#ifndef __PROGRAMMED_BURN_H__
#define __PROGRAMMED_BURN_H__

#include <vector>

class Domain;
class IoData;
class ProgrammedBurnData;
class VarFcn;

template<class Scalar> class Vec;
template<class Scalar, int dim> class SVec;
template<class Scalar> class DistVec;
template<class Scalar, int dim> class DistSVec;

class ProgrammedBurn {

 public:

  ProgrammedBurn(IoData&, Domain *);
  ProgrammedBurn(IoData&, SVec<double, 1> *);

  ~ProgrammedBurn();

  static int countBurnableFluids(IoData&);

  template<int dim>
  void setFluidIds(double, DistVec<int>&, DistSVec<double, dim>&);

  template<int dim>
  void setFluidIds(double, Vec<int>&, SVec<double, dim>&);

  bool isDetonationInterface(int, int, int&) const;

  void getDetonationNormal(int, int, int, double[3], double[3]);

  template<int dim>
  void setCurrentTime(double, VarFcn *, DistSVec<double, dim>&, DistVec<int>&, DistVec<int>&);

  template<int dim>
  void setCurrentTime(double, VarFcn *, SVec<double, dim>&, Vec<int>&, Vec<int>&);

  bool isBurnedEOS(int, int&) const;
  bool isUnburnedEOS(int, int&) const;

  int getBurnedEOS(int) const;
  int getUnburnedEOS(int) const;

  bool nodeInside(int, int, int);
  bool nodeInside(int, int);

  bool numberOfBurns() {
    return myBurns.size();
  }
  bool isIgnited(int tag) const {
    return myBurns[tag].ignited;
  }
  bool isFinished(int tag) const {
    return myBurns[tag].finished;
  }

  // Static methods: compute assorted values associated with a programmed burn
  static void computeChapmanJouguetStateJWL(double, double, double, double, double, double, double,
                                            double, double&, double&, double&, double&);

  static void computeChapmanJouguetStatePG(double, double, double, double, double&, double&,
                                           double&, double&);


 private:

  struct Burn {
    Burn() : x0subdom(-1), x0id(-1), ignited(false) {}
    ProgrammedBurnData *pgData;
    double x0[3];
    int x0subdom, x0id;
    bool ignited;
    bool finished;
  };

  template<int dim>
  void setCJInitialState(Burn&, VarFcn *, DistSVec<double, dim>&, DistVec<int>&, DistVec<int>&);

  template<int dim>
  void setCJInitialState(Burn&, VarFcn *, SVec<double, dim>&, Vec<int>&, Vec<int>&);

  void computeNearestNode(const double[3], double[3], int&, int&);

  std::vector<Burn> myBurns;
  double lastTime;
  Domain *domain;
  SVec<double, 1> *nodeSet0;
};

#endif

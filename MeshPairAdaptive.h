#ifndef _MESH_PAIR_H_
#define _MESH_PAIR_H_

#include <Elem.h>
#include <Node.h>
#include <Vector3DMPI.h>
#include <SubDomain.h>
#include <DistVector.h>
#include <vector>
#include <tuple>
#include <string>
#include <set>

#include <IoData.h>
#include <Domain.h>
#include <VarFcn.h>

class DistRTree;

//--------------------------- AMR MESH PAIR CLASS ------------------------------

template<int dim>
class MeshPairAdaptive {

  IoData& ioData;
  Communicator *com;
  VarFcn *varFcn;

  Domain *domain;
  Domain *domain1;
  Domain *domain2;
  std::vector<Domain *> domains; // pointers to cached domains
  std::vector<int> domainNums; // indices of cached domains

  DistSVec<double,dim> *snapshot1;
  DistSVec<double,dim> *snapshot2;

  double innerProduct;

  Domain *domainSubtractState;
  DistSVec<double,dim> *subtractState;
  DistRTree *distRTreeSubtractState;

public:

  static constexpr double sixth = 1.0 / 6.0;

  // constructors
  MeshPairAdaptive(IoData&, Domain *, Communicator *, VarFcn *, Domain *, DistSVec<double,dim> *);
  ~MeshPairAdaptive();

  // methods
  void delAllSupermeshTags();
  void delSupermeshTags(Domain *);
  void replaceFirstMesh(int);
  void replaceSecondMesh(int);
  void replaceFirstSnap(DistSVec<double,dim> *, bool = false);
  void replaceSecondSnap(DistSVec<double,dim> *, bool = false);
  void subtractStateFromSnapshot(Domain *, DistSVec<double,dim> *);
  void printElementStatisticsAdaptive();
  void computeInnerProduct();
  void computeLocalInnerProduct(Domain *, DistSVec<double,dim> *, Domain *, DistSVec<double,dim> *);
  void computeGlobalInnerProduct(Domain *, DistSVec<double,dim> *, Domain *, DistSVec<double,dim> *);
  void iterator(Domain *, SubDomain *, int&, int&, bool *);
  double computeTetL2(Elem *, int, NodeSet&, DistSVec<double,dim> *, Elem *, int, NodeSet&, DistSVec<double,dim> *);
  double computeTetL2(Vec3DMPI *, Elem *, int, NodeSet&, DistSVec<double,dim> *);
  double computeTetL2(Elem *, int, NodeSet&, DistSVec<double,dim> *, Vec3DMPI *);
  double computeDet(double[3][3]);
  Vec3D computeCentroid (const Vec3DMPI *);
  double computeVolume (const Vec3DMPI *);
  void computeBarycentricCoordinatesJacobianInverted(Vec3DMPI *, double[3][3], int);
  double getInnerProduct();
};

//------------------------------------------------------------------------------

#endif

#ifndef _DENSE_MATRIX_OPS_H_
#define _DENSE_MATRIX_OPS_H_

#include <Vector3D.h>

#include <cstdio>
#include <cstdlib>
#include <cmath>
#include <complex>

#ifdef USE_EIGEN3
// #define USE_EIGEN3_DenseMatrixOp
   #define USE_EIGEN3_DenseMatrixOp_NoMixedPrecision
// #define USE_EIGEN3_VectorOp
// #define USE_EIGEN3_DenseMatrixLu
#endif

#include <Eigen/Core>
#include <Eigen/LU>
#include <Eigen/Cholesky>

//------------------------------------------------------------------------------
// this class performs matrix-matrix and matrix-vector multiplications of the form
// c = a*b, or c += a*b, or c -= a*b, or c = a^T*b, or c += a^T*b or c -= a^T*b
// notice the different type treatments for application to matrix vs. a vector:
//   (1) in apply to matrix, b and c are required to be the same type
//   (2) in apply to vector, a and c are required to be the same type
// warning: b and c must be different
//          a and c must also be different when either USE_EIGEN3_DenseMatrixOp or USE_EIGEN3_DenseMatrixOp_NoMixedPrecision is defined
template<int dim, int dim2>
class DenseMatrixOp {

 public:
  template<class Scalar, class Scalar2>
  static void applyToDenseMatrix(Scalar a[dim2], Scalar b[dim2], Scalar2 c[dim2]) {
    const Eigen::Map<const Eigen::Matrix<Scalar,dim,dim,(dim==1?Eigen::ColMajor:Eigen::RowMajor)>> A(a,dim,dim);
    const Eigen::Map<const Eigen::Matrix<Scalar,dim,dim,(dim==1?Eigen::ColMajor:Eigen::RowMajor)>> B(b,dim,dim);
    Eigen::Map<Eigen::Matrix<Scalar2,dim,dim,(dim==1?Eigen::ColMajor:Eigen::RowMajor)>> C(c,dim,dim);
    C.noalias() = (A*B).template cast<Scalar2>();
  }

  template<class Scalar, class Scalar2>
  static void applyAndAddToDenseMatrix(Scalar a[dim2], Scalar b[dim2], Scalar2 c[dim2]) {
    const Eigen::Map<const Eigen::Matrix<Scalar,dim,dim,(dim==1?Eigen::ColMajor:Eigen::RowMajor)>> A(a,dim,dim);
    const Eigen::Map<const Eigen::Matrix<Scalar,dim,dim,(dim==1?Eigen::ColMajor:Eigen::RowMajor)>> B(b,dim,dim);
    Eigen::Map<Eigen::Matrix<Scalar2,dim,dim,(dim==1?Eigen::ColMajor:Eigen::RowMajor)>> C(c,dim,dim);
    C.noalias() += (A*B).template cast<Scalar2>();
  }

  template<class Scalar, class Scalar2>
  static void applyAndAddTransToDenseMatrix(Scalar a[dim2], Scalar b[dim2], Scalar2 c[dim2]) {
    const Eigen::Map<const Eigen::Matrix<Scalar,dim,dim,(dim==1?Eigen::ColMajor:Eigen::RowMajor)>> A(a,dim,dim);
    const Eigen::Map<const Eigen::Matrix<Scalar,dim,dim,(dim==1?Eigen::ColMajor:Eigen::RowMajor)>> B(b,dim,dim);
    Eigen::Map<Eigen::Matrix<Scalar2,dim,dim,(dim==1?Eigen::ColMajor:Eigen::RowMajor)>> C(c,dim,dim);
    C.noalias() += (A*B).template cast<Scalar2>().transpose();
  }

  template<class Scalar, class Scalar2>
  static void applyAndSubToDenseMatrix(Scalar a[dim2], Scalar b[dim2], Scalar2 c[dim2]) {
    const Eigen::Map<const Eigen::Matrix<Scalar,dim,dim,(dim==1?Eigen::ColMajor:Eigen::RowMajor)>> A(a,dim,dim);
    const Eigen::Map<const Eigen::Matrix<Scalar,dim,dim,(dim==1?Eigen::ColMajor:Eigen::RowMajor)>> B(b,dim,dim);
    Eigen::Map<Eigen::Matrix<Scalar2,dim,dim,(dim==1?Eigen::ColMajor:Eigen::RowMajor)>> C(c,dim,dim);
    C.noalias() -= (A*B).template cast<Scalar2>();
  }

  template<class Scalar, class Scalar2>
  static void applyTransToDenseMatrix(Scalar a[dim2], Scalar b[dim2], Scalar2 c[dim2]) {
    const Eigen::Map<const Eigen::Matrix<Scalar,dim,dim,(dim==1?Eigen::ColMajor:Eigen::RowMajor)>> A(a,dim,dim);
    const Eigen::Map<const Eigen::Matrix<Scalar,dim,dim,(dim==1?Eigen::ColMajor:Eigen::RowMajor)>> B(b,dim,dim);
    Eigen::Map<Eigen::Matrix<Scalar2,dim,dim,(dim==1?Eigen::ColMajor:Eigen::RowMajor)>> C(c,dim,dim);
    C.noalias() = (A.transpose()*B).template cast<Scalar2>();
  }

  template<class Scalar, class Scalar2>
  static void applyTransAndAddToDenseMatrix(Scalar a[dim2], Scalar b[dim2], Scalar2 c[dim2]) {
    const Eigen::Map<const Eigen::Matrix<Scalar,dim,dim,(dim==1?Eigen::ColMajor:Eigen::RowMajor)>> A(a,dim,dim);
    const Eigen::Map<const Eigen::Matrix<Scalar,dim,dim,(dim==1?Eigen::ColMajor:Eigen::RowMajor)>> B(b,dim,dim);
    Eigen::Map<Eigen::Matrix<Scalar2,dim,dim,(dim==1?Eigen::ColMajor:Eigen::RowMajor)>> C(c,dim,dim);
    C.noalias() += (A.transpose()*B).template cast<Scalar2>();
  }

  template<class Scalar, class Scalar2>
  static void applyTransAndSubToDenseMatrix(Scalar a[dim2], Scalar b[dim2], Scalar2 c[dim2]) {
    const Eigen::Map<const Eigen::Matrix<Scalar,dim,dim,(dim==1?Eigen::ColMajor:Eigen::RowMajor)>> A(a,dim,dim);
    const Eigen::Map<const Eigen::Matrix<Scalar,dim,dim,(dim==1?Eigen::ColMajor:Eigen::RowMajor)>> B(b,dim,dim);
    Eigen::Map<Eigen::Matrix<Scalar2,dim,dim,(dim==1?Eigen::ColMajor:Eigen::RowMajor)>> C(c,dim,dim);
    C.noalias() -= (A.transpose()*B).template cast<Scalar2>();
  }

  template<class Scalar, class Scalar2>
  static void applyToVector(Scalar a[dim2], Scalar2 b[dim], Scalar2 c[dim]) {
    const Eigen::Map<const Eigen::Matrix<Scalar,dim,dim,(dim==1?Eigen::ColMajor:Eigen::RowMajor)>> A(a,dim,dim);
    const Eigen::Map<const Eigen::Matrix<Scalar2,dim,1>> B(b,dim,1);
    Eigen::Map<Eigen::Matrix<Scalar2,dim,1>> C(c,dim,1);
    C.noalias() = A.template cast<Scalar2>()*B;
  }

  template<class Scalar, class Scalar2>
  static void applyAndAddToVector(Scalar a[dim2], Scalar2 b[dim], Scalar2 c[dim]) {
    const Eigen::Map<const Eigen::Matrix<Scalar,dim,dim,(dim==1?Eigen::ColMajor:Eigen::RowMajor)>> A(a,dim,dim);
    const Eigen::Map<const Eigen::Matrix<Scalar2,dim,1>> B(b,dim,1);
    Eigen::Map<Eigen::Matrix<Scalar2,dim,1>> C(c,dim,1);
    C.noalias() += A.template cast<Scalar2>()*B;
  }

  template<class Scalar, class Scalar2>
  static void applyAndSubToVector(Scalar a[dim2], Scalar2 b[dim], Scalar2 c[dim]) {
    const Eigen::Map<const Eigen::Matrix<Scalar,dim,dim,(dim==1?Eigen::ColMajor:Eigen::RowMajor)>> A(a,dim,dim);
    const Eigen::Map<const Eigen::Matrix<Scalar2,dim,1>> B(b,dim,1);
    Eigen::Map<Eigen::Matrix<Scalar2,dim,1>> C(c,dim,1);
    C.noalias() -= A.template cast<Scalar2>()*B;
  }

  template<class Scalar, class Scalar2>
  static void applyTransToVector(Scalar a[dim2], Scalar2 b[dim], Scalar2 c[dim]) {
    const Eigen::Map<const Eigen::Matrix<Scalar,dim,dim,(dim==1?Eigen::ColMajor:Eigen::RowMajor)>> A(a,dim,dim);
    const Eigen::Map<const Eigen::Matrix<Scalar2,dim,1>> B(b,dim,1);
    Eigen::Map<Eigen::Matrix<Scalar2,dim,1>> C(c,dim,1);
    C.noalias() = A.template cast<Scalar2>().transpose()*B;
  }

  template<class Scalar, class Scalar2>
  static void applyTransAndAddToVector(Scalar a[dim2], Scalar2 b[dim], Scalar2 c[dim]) {
    const Eigen::Map<const Eigen::Matrix<Scalar,dim,dim,(dim==1?Eigen::ColMajor:Eigen::RowMajor)>> A(a,dim,dim);
    const Eigen::Map<const Eigen::Matrix<Scalar2,dim,1>> B(b,dim,1);
    Eigen::Map<Eigen::Matrix<Scalar2,dim,1>> C(c,dim,1);
    C.noalias() += A.template cast<Scalar2>().transpose()*B;
  }

  template<class Scalar, class Scalar2>
  static void applyTransAndSubToVector(Scalar a[dim2], Scalar2 b[dim], Scalar2 c[dim]) {
    const Eigen::Map<const Eigen::Matrix<Scalar,dim,dim,(dim==1?Eigen::ColMajor:Eigen::RowMajor)>> A(a,dim,dim);
    const Eigen::Map<const Eigen::Matrix<Scalar2,dim,1>> B(b,dim,1);
    Eigen::Map<Eigen::Matrix<Scalar2,dim,1>> C(c,dim,1);
    C.noalias() -= A.template cast<Scalar2>().transpose()*B;
  }

  template<class Scalar, class Scalar2>
  static void transpose(Scalar a[dim2], Scalar2 b[dim2]) {
    const Eigen::Map<const Eigen::Matrix<Scalar,dim,dim,(dim==1?Eigen::ColMajor:Eigen::RowMajor)>> A(a,dim,dim);
    Eigen::Map<Eigen::Matrix<Scalar2,dim,dim,(dim==1?Eigen::ColMajor:Eigen::RowMajor)>> B(b,dim,dim);
    B.noalias() = A.template cast<Scalar2>().transpose();
  }

};

//------------------------------------------------------------------------------

template<int dim, int dim2, int dim3>
class RectangularDenseMatrixOp {

 public:
  template<class Scalar, class Scalar2>
  static void applyAndAddToVector(Scalar a[dim3], Scalar2 b[dim], Scalar2 c[dim2]) {
    const Eigen::Map<const Eigen::Matrix<Scalar,dim2,dim,(dim==1?Eigen::ColMajor:Eigen::RowMajor)>> A(a,dim2,dim);
    const Eigen::Map<const Eigen::Matrix<Scalar2,dim,1>> B(b,dim,1);
    Eigen::Map<Eigen::Matrix<Scalar2,dim2,1>> C(c,dim2,1);
    C.noalias() += A.template cast<Scalar2>()*B;
  }

  template<class Scalar, class Scalar2>
  static void applyAndSubFromVector(Scalar a[dim3], Scalar2 b[dim], Scalar2 c[dim2]) {
    const Eigen::Map<const Eigen::Matrix<Scalar,dim2,dim,(dim==1?Eigen::ColMajor:Eigen::RowMajor)>> A(a,dim2,dim);
    const Eigen::Map<const Eigen::Matrix<Scalar2,dim,1>> B(b,dim,1);
    Eigen::Map<Eigen::Matrix<Scalar2,dim2,1>> C(c,dim2,1);
    C.noalias() -= A.template cast<Scalar2>()*B;
  }


  template<class Scalar, class Scalar2>
  static void applyTransAndAddToVector(Scalar a[dim3], Scalar2 b[dim2], Scalar2 c[dim]) {
    const Eigen::Map<const Eigen::Matrix<Scalar,dim2,dim,(dim==1?Eigen::ColMajor:Eigen::RowMajor)>> A(a,dim2,dim);
    const Eigen::Map<const Eigen::Matrix<Scalar2,dim2,1>> B(b,dim2,1);
    Eigen::Map<Eigen::Matrix<Scalar2,dim,1>> C(c,dim,1);
    C.noalias() += A.template cast<Scalar2>().transpose()*B;
  }

  template<class Scalar, class Scalar2>
  static void applyTransAndSubFromVector(Scalar a[dim3], Scalar2 b[dim2], Scalar2 c[dim]) {
    const Eigen::Map<const Eigen::Matrix<Scalar,dim2,dim,(dim==1?Eigen::ColMajor:Eigen::RowMajor)>> A(a,dim2,dim);
    const Eigen::Map<const Eigen::Matrix<Scalar2,dim2,1>> B(b,dim2,1);
    Eigen::Map<Eigen::Matrix<Scalar2,dim,1>> C(c,dim,1);
    C.noalias() -= A.template cast<Scalar2>().transpose()*B;
  }

};

//------------------------------------------------------------------------------

template<int dim>
class VectorOp {

 public:

  template<class Scalar>
  static void sum(Scalar a[dim], Scalar b[dim], Scalar c[dim]) {
    const Eigen::Map<const Eigen::Matrix<Scalar,dim,1>> A(a,dim,1), B(b,dim,1);
    Eigen::Map<Eigen::Matrix<Scalar,dim,1>> C(c,dim,1);
    C.noalias() = A + B;
  }

  template<class Scalar>
  static void diff(Scalar a[dim], Scalar b[dim], Scalar c[dim]) {
    const Eigen::Map<const Eigen::Matrix<Scalar,dim,1>> A(a,dim,1), B(b,dim,1);
    Eigen::Map<Eigen::Matrix<Scalar,dim,1>> C(c,dim,1);
    C.noalias() = A - B;
  }

  template<class Scalar>
  static void add(Scalar b[dim], Scalar c[dim]) {
    const Eigen::Map<const Eigen::Matrix<Scalar,dim,1>> B(b,dim,1);
    Eigen::Map<Eigen::Matrix<Scalar,dim,1>> C(c,dim,1);
    C.noalias() += B;
  }

  template<class Scalar>
  static void sub(Scalar b[dim], Scalar c[dim]) {
    const Eigen::Map<const Eigen::Matrix<Scalar,dim,1>> B(b,dim,1);
    Eigen::Map<Eigen::Matrix<Scalar,dim,1>> C(c,dim,1);
    C.noalias() -= B;
  }

  template<class Scalar>
  static void mult(const Scalar& a1, const Scalar& a2, const Scalar& a3, Scalar b1[dim],
                   Scalar b2[dim], Scalar b3[dim], Scalar c[dim]) {
    const Eigen::Map<const Eigen::Matrix<Scalar,dim,1>> B1(b1,dim,1), B2(b2,dim,1), B3(b3,dim,1);
    Eigen::Map<Eigen::Matrix<Scalar,dim,1>> C(c,dim,1);
    C.noalias() = a1*B1 + a2*B2 + a3*B3;
  }

  template<class Scalar>
  static void mult(const Scalar& a1, const Scalar& a2, const Scalar& a3, std::complex<Scalar> b1[dim],
                   std::complex<Scalar> b2[dim], std::complex<Scalar> b3[dim], std::complex<Scalar> c[dim]) {
    const Eigen::Map<const Eigen::Matrix<std::complex<Scalar>,dim,1>> B1(b1,dim,1), B2(b2,dim,1), B3(b3,dim,1);
    Eigen::Map<Eigen::Matrix<std::complex<Scalar>,dim,1>> C(c,dim,1);
    C.noalias() = a1*B1 + a2*B2 + a3*B3;
  }

  template<class Scalar>
  static void copy(Scalar b[dim], Scalar c[dim]) {
    const Eigen::Map<const Eigen::Matrix<Scalar,dim,1>> B(b,dim,1);
    Eigen::Map<Eigen::Matrix<Scalar,dim,1>> C(c,dim,1);
    C.noalias() = B;
  }

};

//------------------------------------------------------------------------------

#define DENSEMATRIXTIMESDENSEMATRIX1(op, a, b, c) \
  c[0] op a[0]*b[0];

#define DENSEMATRIXTRANSTIMESDENSEMATRIX1(op, a, b, c) \
  c[0] op a[0]*b[0];

#define DENSEMATRIXTIMESDENSEMATRIX1TRANS(op, a, b, c) \
  c[0] op a[0]*b[0];

#define DENSEMATRIXTIMESDENSEMATRIX2(op, a, b, c) \
  c[0] op a[0]*b[0]+a[1]*b[2]; \
  c[1] op a[0]*b[1]+a[1]*b[3]; \
  c[2] op a[2]*b[0]+a[3]*b[2]; \
  c[3] op a[2]*b[1]+a[3]*b[3];

#define DENSEMATRIXTRANSTIMESDENSEMATRIX2(op, a, b, c) \
  c[0] op a[0]*b[0]+a[2]*b[2]; \
  c[1] op a[0]*b[1]+a[2]*b[3]; \
  c[2] op a[1]*b[0]+a[3]*b[2]; \
  c[3] op a[1]*b[1]+a[3]*b[3];

#define DENSEMATRIXTIMESDENSEMATRIX2TRANS(op, a, b, c) \
  c[0] op a[0]*b[0]+a[1]*b[2]; \
  c[2] op a[0]*b[1]+a[1]*b[3]; \
  c[1] op a[2]*b[0]+a[3]*b[2]; \
  c[3] op a[2]*b[1]+a[3]*b[3];

#define DENSEMATRIXTIMESDENSEMATRIX3(op, a, b, c) \
  c[0] op a[0]*b[0]+a[1]*b[3]+a[2]*b[6]; \
  c[1] op a[0]*b[1]+a[1]*b[4]+a[2]*b[7]; \
  c[2] op a[0]*b[2]+a[1]*b[5]+a[2]*b[8]; \
  c[3] op a[3]*b[0]+a[4]*b[3]+a[5]*b[6]; \
  c[4] op a[3]*b[1]+a[4]*b[4]+a[5]*b[7]; \
  c[5] op a[3]*b[2]+a[4]*b[5]+a[5]*b[8]; \
  c[6] op a[6]*b[0]+a[7]*b[3]+a[8]*b[6]; \
  c[7] op a[6]*b[1]+a[7]*b[4]+a[8]*b[7]; \
  c[8] op a[6]*b[2]+a[7]*b[5]+a[8]*b[8];

#define DENSEMATRIXTRANSTIMESDENSEMATRIX3(op, a, b, c) \
  c[0] op a[0]*b[0]+a[3]*b[3]+a[6]*b[6]; \
  c[1] op a[0]*b[1]+a[3]*b[4]+a[6]*b[7]; \
  c[2] op a[0]*b[2]+a[3]*b[5]+a[6]*b[8]; \
  c[3] op a[1]*b[0]+a[4]*b[3]+a[7]*b[6]; \
  c[4] op a[1]*b[1]+a[4]*b[4]+a[7]*b[7]; \
  c[5] op a[1]*b[2]+a[4]*b[5]+a[7]*b[8]; \
  c[6] op a[2]*b[0]+a[5]*b[3]+a[8]*b[6]; \
  c[7] op a[2]*b[1]+a[5]*b[4]+a[8]*b[7]; \
  c[8] op a[2]*b[2]+a[5]*b[5]+a[8]*b[8];

#define DENSEMATRIXTIMESDENSEMATRIX3TRANS(op, a, b, c) \
  c[0] op a[0]*b[0]+a[1]*b[3]+a[2]*b[6]; \
  c[3] op a[0]*b[1]+a[1]*b[4]+a[2]*b[7]; \
  c[6] op a[0]*b[2]+a[1]*b[5]+a[2]*b[8]; \
  c[1] op a[3]*b[0]+a[4]*b[3]+a[5]*b[6]; \
  c[4] op a[3]*b[1]+a[4]*b[4]+a[5]*b[7]; \
  c[7] op a[3]*b[2]+a[4]*b[5]+a[5]*b[8]; \
  c[1] op a[6]*b[0]+a[7]*b[3]+a[8]*b[6]; \
  c[5] op a[6]*b[1]+a[7]*b[4]+a[8]*b[7]; \
  c[8] op a[6]*b[2]+a[7]*b[5]+a[8]*b[8];

#define DENSEMATRIXTIMESDENSEMATRIX4(op, a, b, c) \
  c[0] op a[0]*b[0]+a[1]*b[4]+a[2]*b[8]+a[3]*b[12]; \
  c[1] op a[0]*b[1]+a[1]*b[5]+a[2]*b[9]+a[3]*b[13]; \
  c[2] op a[0]*b[2]+a[1]*b[6]+a[2]*b[10]+a[3]*b[14]; \
  c[3] op a[0]*b[3]+a[1]*b[7]+a[2]*b[11]+a[3]*b[15]; \
  c[4] op a[4]*b[0]+a[5]*b[4]+a[6]*b[8]+a[7]*b[12]; \
  c[5] op a[4]*b[1]+a[5]*b[5]+a[6]*b[9]+a[7]*b[13]; \
  c[6] op a[4]*b[2]+a[5]*b[6]+a[6]*b[10]+a[7]*b[14]; \
  c[7] op a[4]*b[3]+a[5]*b[7]+a[6]*b[11]+a[7]*b[15]; \
  c[8] op a[8]*b[0]+a[9]*b[4]+a[10]*b[8]+a[11]*b[12]; \
  c[9] op a[8]*b[1]+a[9]*b[5]+a[10]*b[9]+a[11]*b[13]; \
  c[10] op a[8]*b[2]+a[9]*b[6]+a[10]*b[10]+a[11]*b[14]; \
  c[11] op a[8]*b[3]+a[9]*b[7]+a[10]*b[11]+a[11]*b[15]; \
  c[12] op a[12]*b[0]+a[13]*b[4]+a[14]*b[8]+a[15]*b[12]; \
  c[13] op a[12]*b[1]+a[13]*b[5]+a[14]*b[9]+a[15]*b[13]; \
  c[14] op a[12]*b[2]+a[13]*b[6]+a[14]*b[10]+a[15]*b[14]; \
  c[15] op a[12]*b[3]+a[13]*b[7]+a[14]*b[11]+a[15]*b[15];

#define DENSEMATRIXTRANSTIMESDENSEMATRIX4(op, a, b, c) \
  c[0] op a[0]*b[0]+a[4]*b[4]+a[8]*b[8]+a[12]*b[12]; \
  c[1] op a[0]*b[1]+a[4]*b[5]+a[8]*b[9]+a[12]*b[13]; \
  c[2] op a[0]*b[2]+a[4]*b[6]+a[8]*b[10]+a[12]*b[14]; \
  c[3] op a[0]*b[3]+a[4]*b[7]+a[8]*b[11]+a[12]*b[15]; \
  c[4] op a[1]*b[0]+a[5]*b[4]+a[9]*b[8]+a[13]*b[12]; \
  c[5] op a[1]*b[1]+a[5]*b[5]+a[9]*b[9]+a[13]*b[13]; \
  c[6] op a[1]*b[2]+a[5]*b[6]+a[9]*b[10]+a[13]*b[14]; \
  c[7] op a[1]*b[3]+a[5]*b[7]+a[9]*b[11]+a[13]*b[15]; \
  c[8] op a[2]*b[0]+a[6]*b[4]+a[10]*b[8]+a[14]*b[12]; \
  c[9] op a[2]*b[1]+a[6]*b[5]+a[10]*b[9]+a[14]*b[13]; \
  c[10] op a[2]*b[2]+a[6]*b[6]+a[10]*b[10]+a[14]*b[14]; \
  c[11] op a[2]*b[3]+a[6]*b[7]+a[10]*b[11]+a[14]*b[15]; \
  c[12] op a[3]*b[0]+a[7]*b[4]+a[11]*b[8]+a[15]*b[12]; \
  c[13] op a[3]*b[1]+a[7]*b[5]+a[11]*b[9]+a[15]*b[13]; \
  c[14] op a[3]*b[2]+a[7]*b[6]+a[11]*b[10]+a[15]*b[14]; \
  c[15] op a[3]*b[3]+a[7]*b[7]+a[11]*b[11]+a[15]*b[15];

#define DENSEMATRIXTIMESDENSEMATRIX4TRANS(op, a, b, c) \
  c[0] op a[0]*b[0]+a[1]*b[4]+a[2]*b[8]+a[3]*b[12]; \
  c[4] op a[0]*b[1]+a[1]*b[5]+a[2]*b[9]+a[3]*b[13]; \
  c[8] op a[0]*b[2]+a[1]*b[6]+a[2]*b[10]+a[3]*b[14]; \
  c[12] op a[0]*b[3]+a[1]*b[7]+a[2]*b[11]+a[3]*b[15]; \
  c[1] op a[4]*b[0]+a[5]*b[4]+a[6]*b[8]+a[7]*b[12]; \
  c[5] op a[4]*b[1]+a[5]*b[5]+a[6]*b[9]+a[7]*b[13]; \
  c[9] op a[4]*b[2]+a[5]*b[6]+a[6]*b[10]+a[7]*b[14]; \
  c[13] op a[4]*b[3]+a[5]*b[7]+a[6]*b[11]+a[7]*b[15]; \
  c[2] op a[8]*b[0]+a[9]*b[4]+a[10]*b[8]+a[11]*b[12]; \
  c[6] op a[8]*b[1]+a[9]*b[5]+a[10]*b[9]+a[11]*b[13]; \
  c[10] op a[8]*b[2]+a[9]*b[6]+a[10]*b[10]+a[11]*b[14]; \
  c[14] op a[8]*b[3]+a[9]*b[7]+a[10]*b[11]+a[11]*b[15]; \
  c[3] op a[12]*b[0]+a[13]*b[4]+a[14]*b[8]+a[15]*b[12]; \
  c[7] op a[12]*b[1]+a[13]*b[5]+a[14]*b[9]+a[15]*b[13]; \
  c[11] op a[12]*b[2]+a[13]*b[6]+a[14]*b[10]+a[15]*b[14]; \
  c[15] op a[12]*b[3]+a[13]*b[7]+a[14]*b[11]+a[15]*b[15];

#define DENSEMATRIXTIMESDENSEMATRIX5(op, a, b, c) \
  c[0] op a[0]*b[0]+a[1]*b[5]+a[2]*b[10]+a[3]*b[15]+a[4]*b[20]; \
  c[1] op a[0]*b[1]+a[1]*b[6]+a[2]*b[11]+a[3]*b[16]+a[4]*b[21]; \
  c[2] op a[0]*b[2]+a[1]*b[7]+a[2]*b[12]+a[3]*b[17]+a[4]*b[22]; \
  c[3] op a[0]*b[3]+a[1]*b[8]+a[2]*b[13]+a[3]*b[18]+a[4]*b[23]; \
  c[4] op a[0]*b[4]+a[1]*b[9]+a[2]*b[14]+a[3]*b[19]+a[4]*b[24]; \
  c[5] op a[5]*b[0]+a[6]*b[5]+a[7]*b[10]+a[8]*b[15]+a[9]*b[20]; \
  c[6] op a[5]*b[1]+a[6]*b[6]+a[7]*b[11]+a[8]*b[16]+a[9]*b[21]; \
  c[7] op a[5]*b[2]+a[6]*b[7]+a[7]*b[12]+a[8]*b[17]+a[9]*b[22]; \
  c[8] op a[5]*b[3]+a[6]*b[8]+a[7]*b[13]+a[8]*b[18]+a[9]*b[23]; \
  c[9] op a[5]*b[4]+a[6]*b[9]+a[7]*b[14]+a[8]*b[19]+a[9]*b[24]; \
  c[10] op a[10]*b[0]+a[11]*b[5]+a[12]*b[10]+a[13]*b[15]+a[14]*b[20]; \
  c[11] op a[10]*b[1]+a[11]*b[6]+a[12]*b[11]+a[13]*b[16]+a[14]*b[21]; \
  c[12] op a[10]*b[2]+a[11]*b[7]+a[12]*b[12]+a[13]*b[17]+a[14]*b[22]; \
  c[13] op a[10]*b[3]+a[11]*b[8]+a[12]*b[13]+a[13]*b[18]+a[14]*b[23]; \
  c[14] op a[10]*b[4]+a[11]*b[9]+a[12]*b[14]+a[13]*b[19]+a[14]*b[24]; \
  c[15] op a[15]*b[0]+a[16]*b[5]+a[17]*b[10]+a[18]*b[15]+a[19]*b[20]; \
  c[16] op a[15]*b[1]+a[16]*b[6]+a[17]*b[11]+a[18]*b[16]+a[19]*b[21]; \
  c[17] op a[15]*b[2]+a[16]*b[7]+a[17]*b[12]+a[18]*b[17]+a[19]*b[22]; \
  c[18] op a[15]*b[3]+a[16]*b[8]+a[17]*b[13]+a[18]*b[18]+a[19]*b[23]; \
  c[19] op a[15]*b[4]+a[16]*b[9]+a[17]*b[14]+a[18]*b[19]+a[19]*b[24]; \
  c[20] op a[20]*b[0]+a[21]*b[5]+a[22]*b[10]+a[23]*b[15]+a[24]*b[20]; \
  c[21] op a[20]*b[1]+a[21]*b[6]+a[22]*b[11]+a[23]*b[16]+a[24]*b[21]; \
  c[22] op a[20]*b[2]+a[21]*b[7]+a[22]*b[12]+a[23]*b[17]+a[24]*b[22]; \
  c[23] op a[20]*b[3]+a[21]*b[8]+a[22]*b[13]+a[23]*b[18]+a[24]*b[23]; \
  c[24] op a[20]*b[4]+a[21]*b[9]+a[22]*b[14]+a[23]*b[19]+a[24]*b[24];

#define DENSEMATRIXTRANSTIMESDENSEMATRIX5(op, a, b, c) \
  c[0] op a[0]*b[0]+a[5]*b[5]+a[10]*b[10]+a[15]*b[15]+a[20]*b[20]; \
  c[1] op a[0]*b[1]+a[5]*b[6]+a[10]*b[11]+a[15]*b[16]+a[20]*b[21]; \
  c[2] op a[0]*b[2]+a[5]*b[7]+a[10]*b[12]+a[15]*b[17]+a[20]*b[22]; \
  c[3] op a[0]*b[3]+a[5]*b[8]+a[10]*b[13]+a[15]*b[18]+a[20]*b[23]; \
  c[4] op a[0]*b[4]+a[5]*b[9]+a[10]*b[14]+a[15]*b[19]+a[20]*b[24]; \
  c[5] op a[1]*b[0]+a[6]*b[5]+a[11]*b[10]+a[16]*b[15]+a[21]*b[20]; \
  c[6] op a[1]*b[1]+a[6]*b[6]+a[11]*b[11]+a[16]*b[16]+a[21]*b[21]; \
  c[7] op a[1]*b[2]+a[6]*b[7]+a[11]*b[12]+a[16]*b[17]+a[21]*b[22]; \
  c[8] op a[1]*b[3]+a[6]*b[8]+a[11]*b[13]+a[16]*b[18]+a[21]*b[23]; \
  c[9] op a[1]*b[4]+a[6]*b[9]+a[11]*b[14]+a[16]*b[19]+a[21]*b[24]; \
  c[10] op a[2]*b[0]+a[7]*b[5]+a[12]*b[10]+a[17]*b[15]+a[22]*b[20]; \
  c[11] op a[2]*b[1]+a[7]*b[6]+a[12]*b[11]+a[17]*b[16]+a[22]*b[21]; \
  c[12] op a[2]*b[2]+a[7]*b[7]+a[12]*b[12]+a[17]*b[17]+a[22]*b[22]; \
  c[13] op a[2]*b[3]+a[7]*b[8]+a[12]*b[13]+a[17]*b[18]+a[22]*b[23]; \
  c[14] op a[2]*b[4]+a[7]*b[9]+a[12]*b[14]+a[17]*b[19]+a[22]*b[24]; \
  c[15] op a[3]*b[0]+a[8]*b[5]+a[13]*b[10]+a[18]*b[15]+a[23]*b[20]; \
  c[16] op a[3]*b[1]+a[8]*b[6]+a[13]*b[11]+a[18]*b[16]+a[23]*b[21]; \
  c[17] op a[3]*b[2]+a[8]*b[7]+a[13]*b[12]+a[18]*b[17]+a[23]*b[22]; \
  c[18] op a[3]*b[3]+a[8]*b[8]+a[13]*b[13]+a[18]*b[18]+a[23]*b[23]; \
  c[19] op a[3]*b[4]+a[8]*b[9]+a[13]*b[14]+a[18]*b[19]+a[23]*b[24]; \
  c[20] op a[4]*b[0]+a[9]*b[5]+a[14]*b[10]+a[19]*b[15]+a[24]*b[20]; \
  c[21] op a[4]*b[1]+a[9]*b[6]+a[14]*b[11]+a[19]*b[16]+a[24]*b[21]; \
  c[22] op a[4]*b[2]+a[9]*b[7]+a[14]*b[12]+a[19]*b[17]+a[24]*b[22]; \
  c[23] op a[4]*b[3]+a[9]*b[8]+a[14]*b[13]+a[19]*b[18]+a[24]*b[23]; \
  c[24] op a[4]*b[4]+a[9]*b[9]+a[14]*b[14]+a[19]*b[19]+a[24]*b[24];

#define DENSEMATRIXTIMESDENSEMATRIX5TRANS(op, a, b, c) \
  c[0] op a[0]*b[0]+a[1]*b[5]+a[2]*b[10]+a[3]*b[15]+a[4]*b[20]; \
  c[5] op a[0]*b[1]+a[1]*b[6]+a[2]*b[11]+a[3]*b[16]+a[4]*b[21]; \
  c[10] op a[0]*b[2]+a[1]*b[7]+a[2]*b[12]+a[3]*b[17]+a[4]*b[22]; \
  c[15] op a[0]*b[3]+a[1]*b[8]+a[2]*b[13]+a[3]*b[18]+a[4]*b[23]; \
  c[20] op a[0]*b[4]+a[1]*b[9]+a[2]*b[14]+a[3]*b[19]+a[4]*b[24]; \
  c[1] op a[5]*b[0]+a[6]*b[5]+a[7]*b[10]+a[8]*b[15]+a[9]*b[20]; \
  c[6] op a[5]*b[1]+a[6]*b[6]+a[7]*b[11]+a[8]*b[16]+a[9]*b[21]; \
  c[11] op a[5]*b[2]+a[6]*b[7]+a[7]*b[12]+a[8]*b[17]+a[9]*b[22]; \
  c[16] op a[5]*b[3]+a[6]*b[8]+a[7]*b[13]+a[8]*b[18]+a[9]*b[23]; \
  c[21] op a[5]*b[4]+a[6]*b[9]+a[7]*b[14]+a[8]*b[19]+a[9]*b[24]; \
  c[2] op a[10]*b[0]+a[11]*b[5]+a[12]*b[10]+a[13]*b[15]+a[14]*b[20]; \
  c[7] op a[10]*b[1]+a[11]*b[6]+a[12]*b[11]+a[13]*b[16]+a[14]*b[21]; \
  c[12] op a[10]*b[2]+a[11]*b[7]+a[12]*b[12]+a[13]*b[17]+a[14]*b[22]; \
  c[17] op a[10]*b[3]+a[11]*b[8]+a[12]*b[13]+a[13]*b[18]+a[14]*b[23]; \
  c[22] op a[10]*b[4]+a[11]*b[9]+a[12]*b[14]+a[13]*b[19]+a[14]*b[24]; \
  c[3] op a[15]*b[0]+a[16]*b[5]+a[17]*b[10]+a[18]*b[15]+a[19]*b[20]; \
  c[8] op a[15]*b[1]+a[16]*b[6]+a[17]*b[11]+a[18]*b[16]+a[19]*b[21]; \
  c[13] op a[15]*b[2]+a[16]*b[7]+a[17]*b[12]+a[18]*b[17]+a[19]*b[22]; \
  c[18] op a[15]*b[3]+a[16]*b[8]+a[17]*b[13]+a[18]*b[18]+a[19]*b[23]; \
  c[23] op a[15]*b[4]+a[16]*b[9]+a[17]*b[14]+a[18]*b[19]+a[19]*b[24]; \
  c[4] op a[20]*b[0]+a[21]*b[5]+a[22]*b[10]+a[23]*b[15]+a[24]*b[20]; \
  c[9] op a[20]*b[1]+a[21]*b[6]+a[22]*b[11]+a[23]*b[16]+a[24]*b[21]; \
  c[14] op a[20]*b[2]+a[21]*b[7]+a[22]*b[12]+a[23]*b[17]+a[24]*b[22]; \
  c[19] op a[20]*b[3]+a[21]*b[8]+a[22]*b[13]+a[23]*b[18]+a[24]*b[23]; \
  c[24] op a[20]*b[4]+a[21]*b[9]+a[22]*b[14]+a[23]*b[19]+a[24]*b[24];

#define DENSEMATRIXTIMESDENSEMATRIX6(op, a, b, c) \
  c[0] op a[0]*b[0]+a[1]*b[6]+a[2]*b[12]+a[3]*b[18]+a[4]*b[24]+a[5]*b[30]; \
  c[1] op a[0]*b[1]+a[1]*b[7]+a[2]*b[13]+a[3]*b[19]+a[4]*b[25]+a[5]*b[31]; \
  c[2] op a[0]*b[2]+a[1]*b[8]+a[2]*b[14]+a[3]*b[20]+a[4]*b[26]+a[5]*b[32]; \
  c[3] op a[0]*b[3]+a[1]*b[9]+a[2]*b[15]+a[3]*b[21]+a[4]*b[27]+a[5]*b[33]; \
  c[4] op a[0]*b[4]+a[1]*b[10]+a[2]*b[16]+a[3]*b[22]+a[4]*b[28]+a[5]*b[34]; \
  c[5] op a[0]*b[5]+a[1]*b[11]+a[2]*b[17]+a[3]*b[23]+a[4]*b[29]+a[5]*b[35]; \
  c[6] op a[6]*b[0]+a[7]*b[6]+a[8]*b[12]+a[9]*b[18]+a[10]*b[24]+a[11]*b[30]; \
  c[7] op a[6]*b[1]+a[7]*b[7]+a[8]*b[13]+a[9]*b[19]+a[10]*b[25]+a[11]*b[31]; \
  c[8] op a[6]*b[2]+a[7]*b[8]+a[8]*b[14]+a[9]*b[20]+a[10]*b[26]+a[11]*b[32]; \
  c[9] op a[6]*b[3]+a[7]*b[9]+a[8]*b[15]+a[9]*b[21]+a[10]*b[27]+a[11]*b[33]; \
  c[10] op a[6]*b[4]+a[7]*b[10]+a[8]*b[16]+a[9]*b[22]+a[10]*b[28]+a[11]*b[34]; \
  c[11] op a[6]*b[5]+a[7]*b[11]+a[8]*b[17]+a[9]*b[23]+a[10]*b[29]+a[11]*b[35]; \
  c[12] op a[12]*b[0]+a[13]*b[6]+a[14]*b[12]+a[15]*b[18]+a[16]*b[24]+a[17]*b[30]; \
  c[13] op a[12]*b[1]+a[13]*b[7]+a[14]*b[13]+a[15]*b[19]+a[16]*b[25]+a[17]*b[31]; \
  c[14] op a[12]*b[2]+a[13]*b[8]+a[14]*b[14]+a[15]*b[20]+a[16]*b[26]+a[17]*b[32]; \
  c[15] op a[12]*b[3]+a[13]*b[9]+a[14]*b[15]+a[15]*b[21]+a[16]*b[27]+a[17]*b[33]; \
  c[16] op a[12]*b[4]+a[13]*b[10]+a[14]*b[16]+a[15]*b[22]+a[16]*b[28]+a[17]*b[34]; \
  c[17] op a[12]*b[5]+a[13]*b[11]+a[14]*b[17]+a[15]*b[23]+a[16]*b[29]+a[17]*b[35]; \
  c[18] op a[18]*b[0]+a[19]*b[6]+a[20]*b[12]+a[21]*b[18]+a[22]*b[24]+a[23]*b[30]; \
  c[19] op a[18]*b[1]+a[19]*b[7]+a[20]*b[13]+a[21]*b[19]+a[22]*b[25]+a[23]*b[31]; \
  c[20] op a[18]*b[2]+a[19]*b[8]+a[20]*b[14]+a[21]*b[20]+a[22]*b[26]+a[23]*b[32]; \
  c[21] op a[18]*b[3]+a[19]*b[9]+a[20]*b[15]+a[21]*b[21]+a[22]*b[27]+a[23]*b[33]; \
  c[22] op a[18]*b[4]+a[19]*b[10]+a[20]*b[16]+a[21]*b[22]+a[22]*b[28]+a[23]*b[34]; \
  c[23] op a[18]*b[5]+a[19]*b[11]+a[20]*b[17]+a[21]*b[23]+a[22]*b[29]+a[23]*b[35]; \
  c[24] op a[24]*b[0]+a[25]*b[6]+a[26]*b[12]+a[27]*b[18]+a[28]*b[24]+a[29]*b[30]; \
  c[25] op a[24]*b[1]+a[25]*b[7]+a[26]*b[13]+a[27]*b[19]+a[28]*b[25]+a[29]*b[31]; \
  c[26] op a[24]*b[2]+a[25]*b[8]+a[26]*b[14]+a[27]*b[20]+a[28]*b[26]+a[29]*b[32]; \
  c[27] op a[24]*b[3]+a[25]*b[9]+a[26]*b[15]+a[27]*b[21]+a[28]*b[27]+a[29]*b[33]; \
  c[28] op a[24]*b[4]+a[25]*b[10]+a[26]*b[16]+a[27]*b[22]+a[28]*b[28]+a[29]*b[34]; \
  c[29] op a[24]*b[5]+a[25]*b[11]+a[26]*b[17]+a[27]*b[23]+a[28]*b[29]+a[29]*b[35]; \
  c[30] op a[30]*b[0]+a[31]*b[6]+a[32]*b[12]+a[33]*b[18]+a[34]*b[24]+a[35]*b[30]; \
  c[31] op a[30]*b[1]+a[31]*b[7]+a[32]*b[13]+a[33]*b[19]+a[34]*b[25]+a[35]*b[31]; \
  c[32] op a[30]*b[2]+a[31]*b[8]+a[32]*b[14]+a[33]*b[20]+a[34]*b[26]+a[35]*b[32]; \
  c[33] op a[30]*b[3]+a[31]*b[9]+a[32]*b[15]+a[33]*b[21]+a[34]*b[27]+a[35]*b[33]; \
  c[34] op a[30]*b[4]+a[31]*b[10]+a[32]*b[16]+a[33]*b[22]+a[34]*b[28]+a[35]*b[34]; \
  c[35] op a[30]*b[5]+a[31]*b[11]+a[32]*b[17]+a[33]*b[23]+a[34]*b[29]+a[35]*b[35];

#define DENSEMATRIXTRANSTIMESDENSEMATRIX6(op, a, b, c) \
  c[0] op a[0]*b[0]+a[6]*b[6]+a[12]*b[12]+a[18]*b[18]+a[24]*b[24]+a[30]*b[30]; \
  c[1] op a[0]*b[1]+a[6]*b[7]+a[12]*b[13]+a[18]*b[19]+a[24]*b[25]+a[30]*b[31]; \
  c[2] op a[0]*b[2]+a[6]*b[8]+a[12]*b[14]+a[18]*b[20]+a[24]*b[26]+a[30]*b[32]; \
  c[3] op a[0]*b[3]+a[6]*b[9]+a[12]*b[15]+a[18]*b[21]+a[24]*b[27]+a[30]*b[33]; \
  c[4] op a[0]*b[4]+a[6]*b[10]+a[12]*b[16]+a[18]*b[22]+a[24]*b[28]+a[30]*b[34]; \
  c[5] op a[0]*b[5]+a[6]*b[11]+a[12]*b[17]+a[18]*b[23]+a[24]*b[29]+a[30]*b[35]; \
  c[6] op a[1]*b[0]+a[7]*b[6]+a[13]*b[12]+a[19]*b[18]+a[25]*b[24]+a[31]*b[30]; \
  c[7] op a[1]*b[1]+a[7]*b[7]+a[13]*b[13]+a[19]*b[19]+a[25]*b[25]+a[31]*b[31]; \
  c[8] op a[1]*b[2]+a[7]*b[8]+a[13]*b[14]+a[19]*b[20]+a[25]*b[26]+a[31]*b[32]; \
  c[9] op a[1]*b[3]+a[7]*b[9]+a[13]*b[15]+a[19]*b[21]+a[25]*b[27]+a[31]*b[33]; \
  c[10] op a[1]*b[4]+a[7]*b[10]+a[13]*b[16]+a[19]*b[22]+a[25]*b[28]+a[31]*b[34]; \
  c[11] op a[1]*b[5]+a[7]*b[11]+a[13]*b[17]+a[19]*b[23]+a[25]*b[29]+a[31]*b[35]; \
  c[12] op a[2]*b[0]+a[8]*b[6]+a[14]*b[12]+a[20]*b[18]+a[26]*b[24]+a[32]*b[30]; \
  c[13] op a[2]*b[1]+a[8]*b[7]+a[14]*b[13]+a[20]*b[19]+a[26]*b[25]+a[32]*b[31]; \
  c[14] op a[2]*b[2]+a[8]*b[8]+a[14]*b[14]+a[20]*b[20]+a[26]*b[26]+a[32]*b[32]; \
  c[15] op a[2]*b[3]+a[8]*b[9]+a[14]*b[15]+a[20]*b[21]+a[26]*b[27]+a[32]*b[33]; \
  c[16] op a[2]*b[4]+a[8]*b[10]+a[14]*b[16]+a[20]*b[22]+a[26]*b[28]+a[32]*b[34]; \
  c[17] op a[2]*b[5]+a[8]*b[11]+a[14]*b[17]+a[20]*b[23]+a[26]*b[29]+a[32]*b[35]; \
  c[18] op a[3]*b[0]+a[9]*b[6]+a[15]*b[12]+a[21]*b[18]+a[27]*b[24]+a[33]*b[30]; \
  c[19] op a[3]*b[1]+a[9]*b[7]+a[15]*b[13]+a[21]*b[19]+a[27]*b[25]+a[33]*b[31]; \
  c[20] op a[3]*b[2]+a[9]*b[8]+a[15]*b[14]+a[21]*b[20]+a[27]*b[26]+a[33]*b[32]; \
  c[21] op a[3]*b[3]+a[9]*b[9]+a[15]*b[15]+a[21]*b[21]+a[27]*b[27]+a[33]*b[33]; \
  c[22] op a[3]*b[4]+a[9]*b[10]+a[15]*b[16]+a[21]*b[22]+a[27]*b[28]+a[33]*b[34]; \
  c[23] op a[3]*b[5]+a[9]*b[11]+a[15]*b[17]+a[21]*b[23]+a[27]*b[29]+a[33]*b[35]; \
  c[24] op a[4]*b[0]+a[10]*b[6]+a[16]*b[12]+a[22]*b[18]+a[28]*b[24]+a[34]*b[30]; \
  c[25] op a[4]*b[1]+a[10]*b[7]+a[16]*b[13]+a[22]*b[19]+a[28]*b[25]+a[34]*b[31]; \
  c[26] op a[4]*b[2]+a[10]*b[8]+a[16]*b[14]+a[22]*b[20]+a[28]*b[26]+a[34]*b[32]; \
  c[27] op a[4]*b[3]+a[10]*b[9]+a[16]*b[15]+a[22]*b[21]+a[28]*b[27]+a[34]*b[33]; \
  c[28] op a[4]*b[4]+a[10]*b[10]+a[16]*b[16]+a[22]*b[22]+a[28]*b[28]+a[34]*b[34]; \
  c[29] op a[4]*b[5]+a[10]*b[11]+a[16]*b[17]+a[22]*b[23]+a[28]*b[29]+a[34]*b[35]; \
  c[30] op a[5]*b[0]+a[11]*b[6]+a[17]*b[12]+a[23]*b[18]+a[29]*b[24]+a[35]*b[30]; \
  c[31] op a[5]*b[1]+a[11]*b[7]+a[17]*b[13]+a[23]*b[19]+a[29]*b[25]+a[35]*b[31]; \
  c[32] op a[5]*b[2]+a[11]*b[8]+a[17]*b[14]+a[23]*b[20]+a[29]*b[26]+a[35]*b[32]; \
  c[33] op a[5]*b[3]+a[11]*b[9]+a[17]*b[15]+a[23]*b[21]+a[29]*b[27]+a[35]*b[33]; \
  c[34] op a[5]*b[4]+a[11]*b[10]+a[17]*b[16]+a[23]*b[22]+a[29]*b[28]+a[35]*b[34]; \
  c[35] op a[5]*b[5]+a[11]*b[11]+a[17]*b[17]+a[23]*b[23]+a[29]*b[29]+a[35]*b[35];

#define DENSEMATRIXTIMESDENSEMATRIX6TRANS(op, a, b, c) \
  c[0] op a[0]*b[0]+a[1]*b[6]+a[2]*b[12]+a[3]*b[18]+a[4]*b[24]+a[5]*b[30]; \
  c[6] op a[0]*b[1]+a[1]*b[7]+a[2]*b[13]+a[3]*b[19]+a[4]*b[25]+a[5]*b[31]; \
  c[12] op a[0]*b[2]+a[1]*b[8]+a[2]*b[14]+a[3]*b[20]+a[4]*b[26]+a[5]*b[32]; \
  c[18] op a[0]*b[3]+a[1]*b[9]+a[2]*b[15]+a[3]*b[21]+a[4]*b[27]+a[5]*b[33]; \
  c[24] op a[0]*b[4]+a[1]*b[10]+a[2]*b[16]+a[3]*b[22]+a[4]*b[28]+a[5]*b[34]; \
  c[30] op a[0]*b[5]+a[1]*b[11]+a[2]*b[17]+a[3]*b[23]+a[4]*b[29]+a[5]*b[35]; \
  c[1] op a[6]*b[0]+a[7]*b[6]+a[8]*b[12]+a[9]*b[18]+a[10]*b[24]+a[11]*b[30]; \
  c[7] op a[6]*b[1]+a[7]*b[7]+a[8]*b[13]+a[9]*b[19]+a[10]*b[25]+a[11]*b[31]; \
  c[13] op a[6]*b[2]+a[7]*b[8]+a[8]*b[14]+a[9]*b[20]+a[10]*b[26]+a[11]*b[32]; \
  c[19] op a[6]*b[3]+a[7]*b[9]+a[8]*b[15]+a[9]*b[21]+a[10]*b[27]+a[11]*b[33]; \
  c[25] op a[6]*b[4]+a[7]*b[10]+a[8]*b[16]+a[9]*b[22]+a[10]*b[28]+a[11]*b[34]; \
  c[31] op a[6]*b[5]+a[7]*b[11]+a[8]*b[17]+a[9]*b[23]+a[10]*b[29]+a[11]*b[35]; \
  c[2] op a[12]*b[0]+a[13]*b[6]+a[14]*b[12]+a[15]*b[18]+a[16]*b[24]+a[17]*b[30]; \
  c[8] op a[12]*b[1]+a[13]*b[7]+a[14]*b[13]+a[15]*b[19]+a[16]*b[25]+a[17]*b[31]; \
  c[14] op a[12]*b[2]+a[13]*b[8]+a[14]*b[14]+a[15]*b[20]+a[16]*b[26]+a[17]*b[32]; \
  c[20] op a[12]*b[3]+a[13]*b[9]+a[14]*b[15]+a[15]*b[21]+a[16]*b[27]+a[17]*b[33]; \
  c[26] op a[12]*b[4]+a[13]*b[10]+a[14]*b[16]+a[15]*b[22]+a[16]*b[28]+a[17]*b[34]; \
  c[32] op a[12]*b[5]+a[13]*b[11]+a[14]*b[17]+a[15]*b[23]+a[16]*b[29]+a[17]*b[35]; \
  c[3] op a[18]*b[0]+a[19]*b[6]+a[20]*b[12]+a[21]*b[18]+a[22]*b[24]+a[23]*b[30]; \
  c[9] op a[18]*b[1]+a[19]*b[7]+a[20]*b[13]+a[21]*b[19]+a[22]*b[25]+a[23]*b[31]; \
  c[15] op a[18]*b[2]+a[19]*b[8]+a[20]*b[14]+a[21]*b[20]+a[22]*b[26]+a[23]*b[32]; \
  c[21] op a[18]*b[3]+a[19]*b[9]+a[20]*b[15]+a[21]*b[21]+a[22]*b[27]+a[23]*b[33]; \
  c[27] op a[18]*b[4]+a[19]*b[10]+a[20]*b[16]+a[21]*b[22]+a[22]*b[28]+a[23]*b[34]; \
  c[33] op a[18]*b[5]+a[19]*b[11]+a[20]*b[17]+a[21]*b[23]+a[22]*b[29]+a[23]*b[35]; \
  c[4] op a[24]*b[0]+a[25]*b[6]+a[26]*b[12]+a[27]*b[18]+a[28]*b[24]+a[29]*b[30]; \
  c[10] op a[24]*b[1]+a[25]*b[7]+a[26]*b[13]+a[27]*b[19]+a[28]*b[25]+a[29]*b[31]; \
  c[16] op a[24]*b[2]+a[25]*b[8]+a[26]*b[14]+a[27]*b[20]+a[28]*b[26]+a[29]*b[32]; \
  c[22] op a[24]*b[3]+a[25]*b[9]+a[26]*b[15]+a[27]*b[21]+a[28]*b[27]+a[29]*b[33]; \
  c[28] op a[24]*b[4]+a[25]*b[10]+a[26]*b[16]+a[27]*b[22]+a[28]*b[28]+a[29]*b[34]; \
  c[34] op a[24]*b[5]+a[25]*b[11]+a[26]*b[17]+a[27]*b[23]+a[28]*b[29]+a[29]*b[35]; \
  c[5] op a[30]*b[0]+a[31]*b[6]+a[32]*b[12]+a[33]*b[18]+a[34]*b[24]+a[35]*b[30]; \
  c[11] op a[30]*b[1]+a[31]*b[7]+a[32]*b[13]+a[33]*b[19]+a[34]*b[25]+a[35]*b[31]; \
  c[17] op a[30]*b[2]+a[31]*b[8]+a[32]*b[14]+a[33]*b[20]+a[34]*b[26]+a[35]*b[32]; \
  c[23] op a[30]*b[3]+a[31]*b[9]+a[32]*b[15]+a[33]*b[21]+a[34]*b[27]+a[35]*b[33]; \
  c[29] op a[30]*b[4]+a[31]*b[10]+a[32]*b[16]+a[33]*b[22]+a[34]*b[28]+a[35]*b[34]; \
  c[35] op a[30]*b[5]+a[31]*b[11]+a[32]*b[17]+a[33]*b[23]+a[34]*b[29]+a[35]*b[35];

#define DENSEMATRIXTIMESDENSEMATRIX7(op, a, b, c) \
  c[0] op a[0]*b[0]+a[1]*b[7]+a[2]*b[14]+a[3]*b[21]+a[4]*b[28]+a[5]*b[35]+a[6]*b[42]; \
  c[1] op a[0]*b[1]+a[1]*b[8]+a[2]*b[15]+a[3]*b[22]+a[4]*b[29]+a[5]*b[36]+a[6]*b[43]; \
  c[2] op a[0]*b[2]+a[1]*b[9]+a[2]*b[16]+a[3]*b[23]+a[4]*b[30]+a[5]*b[37]+a[6]*b[44]; \
  c[3] op a[0]*b[3]+a[1]*b[10]+a[2]*b[17]+a[3]*b[24]+a[4]*b[31]+a[5]*b[38]+a[6]*b[45]; \
  c[4] op a[0]*b[4]+a[1]*b[11]+a[2]*b[18]+a[3]*b[25]+a[4]*b[32]+a[5]*b[39]+a[6]*b[46]; \
  c[5] op a[0]*b[5]+a[1]*b[12]+a[2]*b[19]+a[3]*b[26]+a[4]*b[33]+a[5]*b[40]+a[6]*b[47]; \
  c[6] op a[0]*b[6]+a[1]*b[13]+a[2]*b[20]+a[3]*b[27]+a[4]*b[34]+a[5]*b[41]+a[6]*b[48]; \
  c[7] op a[7]*b[0]+a[8]*b[7]+a[9]*b[14]+a[10]*b[21]+a[11]*b[28]+a[12]*b[35]+a[13]*b[42]; \
  c[8] op a[7]*b[1]+a[8]*b[8]+a[9]*b[15]+a[10]*b[22]+a[11]*b[29]+a[12]*b[36]+a[13]*b[43]; \
  c[9] op a[7]*b[2]+a[8]*b[9]+a[9]*b[16]+a[10]*b[23]+a[11]*b[30]+a[12]*b[37]+a[13]*b[44]; \
  c[10] op a[7]*b[3]+a[8]*b[10]+a[9]*b[17]+a[10]*b[24]+a[11]*b[31]+a[12]*b[38]+a[13]*b[45]; \
  c[11] op a[7]*b[4]+a[8]*b[11]+a[9]*b[18]+a[10]*b[25]+a[11]*b[32]+a[12]*b[39]+a[13]*b[46]; \
  c[12] op a[7]*b[5]+a[8]*b[12]+a[9]*b[19]+a[10]*b[26]+a[11]*b[33]+a[12]*b[40]+a[13]*b[47]; \
  c[13] op a[7]*b[6]+a[8]*b[13]+a[9]*b[20]+a[10]*b[27]+a[11]*b[34]+a[12]*b[41]+a[13]*b[48]; \
  c[14] op a[14]*b[0]+a[15]*b[7]+a[16]*b[14]+a[17]*b[21]+a[18]*b[28]+a[19]*b[35]+a[20]*b[42]; \
  c[15] op a[14]*b[1]+a[15]*b[8]+a[16]*b[15]+a[17]*b[22]+a[18]*b[29]+a[19]*b[36]+a[20]*b[43]; \
  c[16] op a[14]*b[2]+a[15]*b[9]+a[16]*b[16]+a[17]*b[23]+a[18]*b[30]+a[19]*b[37]+a[20]*b[44]; \
  c[17] op a[14]*b[3]+a[15]*b[10]+a[16]*b[17]+a[17]*b[24]+a[18]*b[31]+a[19]*b[38]+a[20]*b[45]; \
  c[18] op a[14]*b[4]+a[15]*b[11]+a[16]*b[18]+a[17]*b[25]+a[18]*b[32]+a[19]*b[39]+a[20]*b[46]; \
  c[19] op a[14]*b[5]+a[15]*b[12]+a[16]*b[19]+a[17]*b[26]+a[18]*b[33]+a[19]*b[40]+a[20]*b[47]; \
  c[20] op a[14]*b[6]+a[15]*b[13]+a[16]*b[20]+a[17]*b[27]+a[18]*b[34]+a[19]*b[41]+a[20]*b[48]; \
  c[21] op a[21]*b[0]+a[22]*b[7]+a[23]*b[14]+a[24]*b[21]+a[25]*b[28]+a[26]*b[35]+a[27]*b[42]; \
  c[22] op a[21]*b[1]+a[22]*b[8]+a[23]*b[15]+a[24]*b[22]+a[25]*b[29]+a[26]*b[36]+a[27]*b[43]; \
  c[23] op a[21]*b[2]+a[22]*b[9]+a[23]*b[16]+a[24]*b[23]+a[25]*b[30]+a[26]*b[37]+a[27]*b[44]; \
  c[24] op a[21]*b[3]+a[22]*b[10]+a[23]*b[17]+a[24]*b[24]+a[25]*b[31]+a[26]*b[38]+a[27]*b[45]; \
  c[25] op a[21]*b[4]+a[22]*b[11]+a[23]*b[18]+a[24]*b[25]+a[25]*b[32]+a[26]*b[39]+a[27]*b[46]; \
  c[26] op a[21]*b[5]+a[22]*b[12]+a[23]*b[19]+a[24]*b[26]+a[25]*b[33]+a[26]*b[40]+a[27]*b[47]; \
  c[27] op a[21]*b[6]+a[22]*b[13]+a[23]*b[20]+a[24]*b[27]+a[25]*b[34]+a[26]*b[41]+a[27]*b[48]; \
  c[28] op a[28]*b[0]+a[29]*b[7]+a[30]*b[14]+a[31]*b[21]+a[32]*b[28]+a[33]*b[35]+a[34]*b[42]; \
  c[29] op a[28]*b[1]+a[29]*b[8]+a[30]*b[15]+a[31]*b[22]+a[32]*b[29]+a[33]*b[36]+a[34]*b[43]; \
  c[30] op a[28]*b[2]+a[29]*b[9]+a[30]*b[16]+a[31]*b[23]+a[32]*b[30]+a[33]*b[37]+a[34]*b[44]; \
  c[31] op a[28]*b[3]+a[29]*b[10]+a[30]*b[17]+a[31]*b[24]+a[32]*b[31]+a[33]*b[38]+a[34]*b[45]; \
  c[32] op a[28]*b[4]+a[29]*b[11]+a[30]*b[18]+a[31]*b[25]+a[32]*b[32]+a[33]*b[39]+a[34]*b[46]; \
  c[33] op a[28]*b[5]+a[29]*b[12]+a[30]*b[19]+a[31]*b[26]+a[32]*b[33]+a[33]*b[40]+a[34]*b[47]; \
  c[34] op a[28]*b[6]+a[29]*b[13]+a[30]*b[20]+a[31]*b[27]+a[32]*b[34]+a[33]*b[41]+a[34]*b[48]; \
  c[35] op a[35]*b[0]+a[36]*b[7]+a[37]*b[14]+a[38]*b[21]+a[39]*b[28]+a[40]*b[35]+a[41]*b[42]; \
  c[36] op a[35]*b[1]+a[36]*b[8]+a[37]*b[15]+a[38]*b[22]+a[39]*b[29]+a[40]*b[36]+a[41]*b[43]; \
  c[37] op a[35]*b[2]+a[36]*b[9]+a[37]*b[16]+a[38]*b[23]+a[39]*b[30]+a[40]*b[37]+a[41]*b[44]; \
  c[38] op a[35]*b[3]+a[36]*b[10]+a[37]*b[17]+a[38]*b[24]+a[39]*b[31]+a[40]*b[38]+a[41]*b[45]; \
  c[39] op a[35]*b[4]+a[36]*b[11]+a[37]*b[18]+a[38]*b[25]+a[39]*b[32]+a[40]*b[39]+a[41]*b[46]; \
  c[40] op a[35]*b[5]+a[36]*b[12]+a[37]*b[19]+a[38]*b[26]+a[39]*b[33]+a[40]*b[40]+a[41]*b[47]; \
  c[41] op a[35]*b[6]+a[36]*b[13]+a[37]*b[20]+a[38]*b[27]+a[39]*b[34]+a[40]*b[41]+a[41]*b[48]; \
  c[42] op a[42]*b[0]+a[43]*b[7]+a[44]*b[14]+a[45]*b[21]+a[46]*b[28]+a[47]*b[35]+a[48]*b[42]; \
  c[43] op a[42]*b[1]+a[43]*b[8]+a[44]*b[15]+a[45]*b[22]+a[46]*b[29]+a[47]*b[36]+a[48]*b[43]; \
  c[44] op a[42]*b[2]+a[43]*b[9]+a[44]*b[16]+a[45]*b[23]+a[46]*b[30]+a[47]*b[37]+a[48]*b[44]; \
  c[45] op a[42]*b[3]+a[43]*b[10]+a[44]*b[17]+a[45]*b[24]+a[46]*b[31]+a[47]*b[38]+a[48]*b[45]; \
  c[46] op a[42]*b[4]+a[43]*b[11]+a[44]*b[18]+a[45]*b[25]+a[46]*b[32]+a[47]*b[39]+a[48]*b[46]; \
  c[47] op a[42]*b[5]+a[43]*b[12]+a[44]*b[19]+a[45]*b[26]+a[46]*b[33]+a[47]*b[40]+a[48]*b[47]; \
  c[48] op a[42]*b[6]+a[43]*b[13]+a[44]*b[20]+a[45]*b[27]+a[46]*b[34]+a[47]*b[41]+a[48]*b[48];

#define DENSEMATRIXTRANSTIMESDENSEMATRIX7(op, a, b, c) \
  c[0] op a[0]*b[0]+a[7]*b[7]+a[14]*b[14]+a[21]*b[21]+a[28]*b[28]+a[35]*b[35]+a[42]*b[42]; \
  c[1] op a[0]*b[1]+a[7]*b[8]+a[14]*b[15]+a[21]*b[22]+a[28]*b[29]+a[35]*b[36]+a[42]*b[43]; \
  c[2] op a[0]*b[2]+a[7]*b[9]+a[14]*b[16]+a[21]*b[23]+a[28]*b[30]+a[35]*b[37]+a[42]*b[44]; \
  c[3] op a[0]*b[3]+a[7]*b[10]+a[14]*b[17]+a[21]*b[24]+a[28]*b[31]+a[35]*b[38]+a[42]*b[45]; \
  c[4] op a[0]*b[4]+a[7]*b[11]+a[14]*b[18]+a[21]*b[25]+a[28]*b[32]+a[35]*b[39]+a[42]*b[46]; \
  c[5] op a[0]*b[5]+a[7]*b[12]+a[14]*b[19]+a[21]*b[26]+a[28]*b[33]+a[35]*b[40]+a[42]*b[47]; \
  c[6] op a[0]*b[6]+a[7]*b[13]+a[14]*b[20]+a[21]*b[27]+a[28]*b[34]+a[35]*b[41]+a[42]*b[48]; \
  c[7] op a[1]*b[0]+a[8]*b[7]+a[15]*b[14]+a[22]*b[21]+a[29]*b[28]+a[36]*b[35]+a[43]*b[42]; \
  c[8] op a[1]*b[1]+a[8]*b[8]+a[15]*b[15]+a[22]*b[22]+a[29]*b[29]+a[36]*b[36]+a[43]*b[43]; \
  c[9] op a[1]*b[2]+a[8]*b[9]+a[15]*b[16]+a[22]*b[23]+a[29]*b[30]+a[36]*b[37]+a[43]*b[44]; \
  c[10] op a[1]*b[3]+a[8]*b[10]+a[15]*b[17]+a[22]*b[24]+a[29]*b[31]+a[36]*b[38]+a[43]*b[45]; \
  c[11] op a[1]*b[4]+a[8]*b[11]+a[15]*b[18]+a[22]*b[25]+a[29]*b[32]+a[36]*b[39]+a[43]*b[46]; \
  c[12] op a[1]*b[5]+a[8]*b[12]+a[15]*b[19]+a[22]*b[26]+a[29]*b[33]+a[36]*b[40]+a[43]*b[47]; \
  c[13] op a[1]*b[6]+a[8]*b[13]+a[15]*b[20]+a[22]*b[27]+a[29]*b[34]+a[36]*b[41]+a[43]*b[48]; \
  c[14] op a[2]*b[0]+a[9]*b[7]+a[16]*b[14]+a[23]*b[21]+a[30]*b[28]+a[37]*b[35]+a[44]*b[42]; \
  c[15] op a[2]*b[1]+a[9]*b[8]+a[16]*b[15]+a[23]*b[22]+a[30]*b[29]+a[37]*b[36]+a[44]*b[43]; \
  c[16] op a[2]*b[2]+a[9]*b[9]+a[16]*b[16]+a[23]*b[23]+a[30]*b[30]+a[37]*b[37]+a[44]*b[44]; \
  c[17] op a[2]*b[3]+a[9]*b[10]+a[16]*b[17]+a[23]*b[24]+a[30]*b[31]+a[37]*b[38]+a[44]*b[45]; \
  c[18] op a[2]*b[4]+a[9]*b[11]+a[16]*b[18]+a[23]*b[25]+a[30]*b[32]+a[37]*b[39]+a[44]*b[46]; \
  c[19] op a[2]*b[5]+a[9]*b[12]+a[16]*b[19]+a[23]*b[26]+a[30]*b[33]+a[37]*b[40]+a[44]*b[47]; \
  c[20] op a[2]*b[6]+a[9]*b[13]+a[16]*b[20]+a[23]*b[27]+a[30]*b[34]+a[37]*b[41]+a[44]*b[48]; \
  c[21] op a[3]*b[0]+a[10]*b[7]+a[17]*b[14]+a[24]*b[21]+a[31]*b[28]+a[38]*b[35]+a[45]*b[42]; \
  c[22] op a[3]*b[1]+a[10]*b[8]+a[17]*b[15]+a[24]*b[22]+a[31]*b[29]+a[38]*b[36]+a[45]*b[43]; \
  c[23] op a[3]*b[2]+a[10]*b[9]+a[17]*b[16]+a[24]*b[23]+a[31]*b[30]+a[38]*b[37]+a[45]*b[44]; \
  c[24] op a[3]*b[3]+a[10]*b[10]+a[17]*b[17]+a[24]*b[24]+a[31]*b[31]+a[38]*b[38]+a[45]*b[45]; \
  c[25] op a[3]*b[4]+a[10]*b[11]+a[17]*b[18]+a[24]*b[25]+a[31]*b[32]+a[38]*b[39]+a[45]*b[46]; \
  c[26] op a[3]*b[5]+a[10]*b[12]+a[17]*b[19]+a[24]*b[26]+a[31]*b[33]+a[38]*b[40]+a[45]*b[47]; \
  c[27] op a[3]*b[6]+a[10]*b[13]+a[17]*b[20]+a[24]*b[27]+a[31]*b[34]+a[38]*b[41]+a[45]*b[48]; \
  c[28] op a[4]*b[0]+a[11]*b[7]+a[18]*b[14]+a[25]*b[21]+a[32]*b[28]+a[39]*b[35]+a[46]*b[42]; \
  c[29] op a[4]*b[1]+a[11]*b[8]+a[18]*b[15]+a[25]*b[22]+a[32]*b[29]+a[39]*b[36]+a[46]*b[43]; \
  c[30] op a[4]*b[2]+a[11]*b[9]+a[18]*b[16]+a[25]*b[23]+a[32]*b[30]+a[39]*b[37]+a[46]*b[44]; \
  c[31] op a[4]*b[3]+a[11]*b[10]+a[18]*b[17]+a[25]*b[24]+a[32]*b[31]+a[39]*b[38]+a[46]*b[45]; \
  c[32] op a[4]*b[4]+a[11]*b[11]+a[18]*b[18]+a[25]*b[25]+a[32]*b[32]+a[39]*b[39]+a[46]*b[46]; \
  c[33] op a[4]*b[5]+a[11]*b[12]+a[18]*b[19]+a[25]*b[26]+a[32]*b[33]+a[39]*b[40]+a[46]*b[47]; \
  c[34] op a[4]*b[6]+a[11]*b[13]+a[18]*b[20]+a[25]*b[27]+a[32]*b[34]+a[39]*b[41]+a[46]*b[48]; \
  c[35] op a[5]*b[0]+a[12]*b[7]+a[19]*b[14]+a[26]*b[21]+a[33]*b[28]+a[40]*b[35]+a[47]*b[42]; \
  c[36] op a[5]*b[1]+a[12]*b[8]+a[19]*b[15]+a[26]*b[22]+a[33]*b[29]+a[40]*b[36]+a[47]*b[43]; \
  c[37] op a[5]*b[2]+a[12]*b[9]+a[19]*b[16]+a[26]*b[23]+a[33]*b[30]+a[40]*b[37]+a[47]*b[44]; \
  c[38] op a[5]*b[3]+a[12]*b[10]+a[19]*b[17]+a[26]*b[24]+a[33]*b[31]+a[40]*b[38]+a[47]*b[45]; \
  c[39] op a[5]*b[4]+a[12]*b[11]+a[19]*b[18]+a[26]*b[25]+a[33]*b[32]+a[40]*b[39]+a[47]*b[46]; \
  c[40] op a[5]*b[5]+a[12]*b[12]+a[19]*b[19]+a[26]*b[26]+a[33]*b[33]+a[40]*b[40]+a[47]*b[47]; \
  c[41] op a[5]*b[6]+a[12]*b[13]+a[19]*b[20]+a[26]*b[27]+a[33]*b[34]+a[40]*b[41]+a[47]*b[48]; \
  c[42] op a[6]*b[0]+a[13]*b[7]+a[20]*b[14]+a[27]*b[21]+a[34]*b[28]+a[41]*b[35]+a[48]*b[42]; \
  c[43] op a[6]*b[1]+a[13]*b[8]+a[20]*b[15]+a[27]*b[22]+a[34]*b[29]+a[41]*b[36]+a[48]*b[43]; \
  c[44] op a[6]*b[2]+a[13]*b[9]+a[20]*b[16]+a[27]*b[23]+a[34]*b[30]+a[41]*b[37]+a[48]*b[44]; \
  c[45] op a[6]*b[3]+a[13]*b[10]+a[20]*b[17]+a[27]*b[24]+a[34]*b[31]+a[41]*b[38]+a[48]*b[45]; \
  c[46] op a[6]*b[4]+a[13]*b[11]+a[20]*b[18]+a[27]*b[25]+a[34]*b[32]+a[41]*b[39]+a[48]*b[46]; \
  c[47] op a[6]*b[5]+a[13]*b[12]+a[20]*b[19]+a[27]*b[26]+a[34]*b[33]+a[41]*b[40]+a[48]*b[47]; \
  c[48] op a[6]*b[6]+a[13]*b[13]+a[20]*b[20]+a[27]*b[27]+a[34]*b[34]+a[41]*b[41]+a[48]*b[48];

#define DENSEMATRIXTIMESDENSEMATRIX7TRANS(op, a, b, c) \
  c[0] op a[0]*b[0]+a[1]*b[7]+a[2]*b[14]+a[3]*b[21]+a[4]*b[28]+a[5]*b[35]+a[6]*b[42]; \
  c[7] op a[0]*b[1]+a[1]*b[8]+a[2]*b[15]+a[3]*b[22]+a[4]*b[29]+a[5]*b[36]+a[6]*b[43]; \
  c[14] op a[0]*b[2]+a[1]*b[9]+a[2]*b[16]+a[3]*b[23]+a[4]*b[30]+a[5]*b[37]+a[6]*b[44]; \
  c[21] op a[0]*b[3]+a[1]*b[10]+a[2]*b[17]+a[3]*b[24]+a[4]*b[31]+a[5]*b[38]+a[6]*b[45]; \
  c[28] op a[0]*b[4]+a[1]*b[11]+a[2]*b[18]+a[3]*b[25]+a[4]*b[32]+a[5]*b[39]+a[6]*b[46]; \
  c[35] op a[0]*b[5]+a[1]*b[12]+a[2]*b[19]+a[3]*b[26]+a[4]*b[33]+a[5]*b[40]+a[6]*b[47]; \
  c[42] op a[0]*b[6]+a[1]*b[13]+a[2]*b[20]+a[3]*b[27]+a[4]*b[34]+a[5]*b[41]+a[6]*b[48]; \
  c[1] op a[7]*b[0]+a[8]*b[7]+a[9]*b[14]+a[10]*b[21]+a[11]*b[28]+a[12]*b[35]+a[13]*b[42]; \
  c[8] op a[7]*b[1]+a[8]*b[8]+a[9]*b[15]+a[10]*b[22]+a[11]*b[29]+a[12]*b[36]+a[13]*b[43]; \
  c[15] op a[7]*b[2]+a[8]*b[9]+a[9]*b[16]+a[10]*b[23]+a[11]*b[30]+a[12]*b[37]+a[13]*b[44]; \
  c[22] op a[7]*b[3]+a[8]*b[10]+a[9]*b[17]+a[10]*b[24]+a[11]*b[31]+a[12]*b[38]+a[13]*b[45]; \
  c[29] op a[7]*b[4]+a[8]*b[11]+a[9]*b[18]+a[10]*b[25]+a[11]*b[32]+a[12]*b[39]+a[13]*b[46]; \
  c[36] op a[7]*b[5]+a[8]*b[12]+a[9]*b[19]+a[10]*b[26]+a[11]*b[33]+a[12]*b[40]+a[13]*b[47]; \
  c[43] op a[7]*b[6]+a[8]*b[13]+a[9]*b[20]+a[10]*b[27]+a[11]*b[34]+a[12]*b[41]+a[13]*b[48]; \
  c[2] op a[14]*b[0]+a[15]*b[7]+a[16]*b[14]+a[17]*b[21]+a[18]*b[28]+a[19]*b[35]+a[20]*b[42]; \
  c[9] op a[14]*b[1]+a[15]*b[8]+a[16]*b[15]+a[17]*b[22]+a[18]*b[29]+a[19]*b[36]+a[20]*b[43]; \
  c[16] op a[14]*b[2]+a[15]*b[9]+a[16]*b[16]+a[17]*b[23]+a[18]*b[30]+a[19]*b[37]+a[20]*b[44]; \
  c[23] op a[14]*b[3]+a[15]*b[10]+a[16]*b[17]+a[17]*b[24]+a[18]*b[31]+a[19]*b[38]+a[20]*b[45]; \
  c[30] op a[14]*b[4]+a[15]*b[11]+a[16]*b[18]+a[17]*b[25]+a[18]*b[32]+a[19]*b[39]+a[20]*b[46]; \
  c[37] op a[14]*b[5]+a[15]*b[12]+a[16]*b[19]+a[17]*b[26]+a[18]*b[33]+a[19]*b[40]+a[20]*b[47]; \
  c[44] op a[14]*b[6]+a[15]*b[13]+a[16]*b[20]+a[17]*b[27]+a[18]*b[34]+a[19]*b[41]+a[20]*b[48]; \
  c[3] op a[21]*b[0]+a[22]*b[7]+a[23]*b[14]+a[24]*b[21]+a[25]*b[28]+a[26]*b[35]+a[27]*b[42]; \
  c[10] op a[21]*b[1]+a[22]*b[8]+a[23]*b[15]+a[24]*b[22]+a[25]*b[29]+a[26]*b[36]+a[27]*b[43]; \
  c[17] op a[21]*b[2]+a[22]*b[9]+a[23]*b[16]+a[24]*b[23]+a[25]*b[30]+a[26]*b[37]+a[27]*b[44]; \
  c[24] op a[21]*b[3]+a[22]*b[10]+a[23]*b[17]+a[24]*b[24]+a[25]*b[31]+a[26]*b[38]+a[27]*b[45]; \
  c[31] op a[21]*b[4]+a[22]*b[11]+a[23]*b[18]+a[24]*b[25]+a[25]*b[32]+a[26]*b[39]+a[27]*b[46]; \
  c[38] op a[21]*b[5]+a[22]*b[12]+a[23]*b[19]+a[24]*b[26]+a[25]*b[33]+a[26]*b[40]+a[27]*b[47]; \
  c[45] op a[21]*b[6]+a[22]*b[13]+a[23]*b[20]+a[24]*b[27]+a[25]*b[34]+a[26]*b[41]+a[27]*b[48]; \
  c[4] op a[28]*b[0]+a[29]*b[7]+a[30]*b[14]+a[31]*b[21]+a[32]*b[28]+a[33]*b[35]+a[34]*b[42]; \
  c[11] op a[28]*b[1]+a[29]*b[8]+a[30]*b[15]+a[31]*b[22]+a[32]*b[29]+a[33]*b[36]+a[34]*b[43]; \
  c[18] op a[28]*b[2]+a[29]*b[9]+a[30]*b[16]+a[31]*b[23]+a[32]*b[30]+a[33]*b[37]+a[34]*b[44]; \
  c[25] op a[28]*b[3]+a[29]*b[10]+a[30]*b[17]+a[31]*b[24]+a[32]*b[31]+a[33]*b[38]+a[34]*b[45]; \
  c[32] op a[28]*b[4]+a[29]*b[11]+a[30]*b[18]+a[31]*b[25]+a[32]*b[32]+a[33]*b[39]+a[34]*b[46]; \
  c[39] op a[28]*b[5]+a[29]*b[12]+a[30]*b[19]+a[31]*b[26]+a[32]*b[33]+a[33]*b[40]+a[34]*b[47]; \
  c[46] op a[28]*b[6]+a[29]*b[13]+a[30]*b[20]+a[31]*b[27]+a[32]*b[34]+a[33]*b[41]+a[34]*b[48]; \
  c[5] op a[35]*b[0]+a[36]*b[7]+a[37]*b[14]+a[38]*b[21]+a[39]*b[28]+a[40]*b[35]+a[41]*b[42]; \
  c[12] op a[35]*b[1]+a[36]*b[8]+a[37]*b[15]+a[38]*b[22]+a[39]*b[29]+a[40]*b[36]+a[41]*b[43]; \
  c[19] op a[35]*b[2]+a[36]*b[9]+a[37]*b[16]+a[38]*b[23]+a[39]*b[30]+a[40]*b[37]+a[41]*b[44]; \
  c[26] op a[35]*b[3]+a[36]*b[10]+a[37]*b[17]+a[38]*b[24]+a[39]*b[31]+a[40]*b[38]+a[41]*b[45]; \
  c[33] op a[35]*b[4]+a[36]*b[11]+a[37]*b[18]+a[38]*b[25]+a[39]*b[32]+a[40]*b[39]+a[41]*b[46]; \
  c[40] op a[35]*b[5]+a[36]*b[12]+a[37]*b[19]+a[38]*b[26]+a[39]*b[33]+a[40]*b[40]+a[41]*b[47]; \
  c[47] op a[35]*b[6]+a[36]*b[13]+a[37]*b[20]+a[38]*b[27]+a[39]*b[34]+a[40]*b[41]+a[41]*b[48]; \
  c[6] op a[42]*b[0]+a[43]*b[7]+a[44]*b[14]+a[45]*b[21]+a[46]*b[28]+a[47]*b[35]+a[48]*b[42]; \
  c[13] op a[42]*b[1]+a[43]*b[8]+a[44]*b[15]+a[45]*b[22]+a[46]*b[29]+a[47]*b[36]+a[48]*b[43]; \
  c[20] op a[42]*b[2]+a[43]*b[9]+a[44]*b[16]+a[45]*b[23]+a[46]*b[30]+a[47]*b[37]+a[48]*b[44]; \
  c[27] op a[42]*b[3]+a[43]*b[10]+a[44]*b[17]+a[45]*b[24]+a[46]*b[31]+a[47]*b[38]+a[48]*b[45]; \
  c[34] op a[42]*b[4]+a[43]*b[11]+a[44]*b[18]+a[45]*b[25]+a[46]*b[32]+a[47]*b[39]+a[48]*b[46]; \
  c[41] op a[42]*b[5]+a[43]*b[12]+a[44]*b[19]+a[45]*b[26]+a[46]*b[33]+a[47]*b[40]+a[48]*b[47]; \
  c[48] op a[42]*b[6]+a[43]*b[13]+a[44]*b[20]+a[45]*b[27]+a[46]*b[34]+a[47]*b[41]+a[48]*b[48];

//------------------------------------------------------------------------------

#define DENSEMATRIXTIMESVECTOR1(op, a, b, c) \
  c[0] op a[0]*b[0];

#define DENSEMATRIXTRANSTIMESVECTOR1(op, a, b, c) \
  c[0] op a[0]*b[0];

#define DENSEMATRIXTIMESVECTOR2(op, a, b, c) \
  c[0] op a[0]*b[0]+a[1]*b[1]; \
  c[1] op a[2]*b[0]+a[3]*b[1];

#define DENSEMATRIXTRANSTIMESVECTOR2(op, a, b, c) \
  c[0] op a[0]*b[0]+a[2]*b[1]; \
  c[1] op a[1]*b[0]+a[3]*b[1];

#define DENSEMATRIXTIMESVECTOR3(op, a, b, c) \
  c[0] op a[0]*b[0]+a[1]*b[1]+a[2]*b[2]; \
  c[1] op a[3]*b[0]+a[4]*b[1]+a[5]*b[2]; \
  c[2] op a[6]*b[0]+a[7]*b[1]+a[8]*b[2];

#define DENSEMATRIXTRANSTIMESVECTOR3(op, a, b, c) \
  c[0] op a[0]*b[0]+a[3]*b[1]+a[6]*b[2]; \
  c[1] op a[1]*b[0]+a[4]*b[1]+a[7]*b[2]; \
  c[2] op a[2]*b[0]+a[5]*b[1]+a[8]*b[2];

#define DENSEMATRIXTIMESVECTOR4(op, a, b, c) \
  c[0] op a[0]*b[0]+a[1]*b[1]+a[2]*b[2]+a[3]*b[3]; \
  c[1] op a[4]*b[0]+a[5]*b[1]+a[6]*b[2]+a[7]*b[3]; \
  c[2] op a[8]*b[0]+a[9]*b[1]+a[10]*b[2]+a[11]*b[3]; \
  c[3] op a[12]*b[0]+a[13]*b[1]+a[14]*b[2]+a[15]*b[3];

#define DENSEMATRIXTRANSTIMESVECTOR4(op, a, b, c) \
  c[0] op a[0]*b[0]+a[4]*b[1]+a[8]*b[2]+a[12]*b[3]; \
  c[1] op a[1]*b[0]+a[5]*b[1]+a[9]*b[2]+a[13]*b[3]; \
  c[2] op a[2]*b[0]+a[6]*b[1]+a[10]*b[2]+a[14]*b[3]; \
  c[3] op a[3]*b[0]+a[7]*b[1]+a[11]*b[2]+a[15]*b[3];

#define DENSEMATRIXTIMESVECTOR5(op, a, b, c) \
  c[0] op a[0]*b[0]+a[1]*b[1]+a[2]*b[2]+a[3]*b[3]+a[4]*b[4]; \
  c[1] op a[5]*b[0]+a[6]*b[1]+a[7]*b[2]+a[8]*b[3]+a[9]*b[4]; \
  c[2] op a[10]*b[0]+a[11]*b[1]+a[12]*b[2]+a[13]*b[3]+a[14]*b[4]; \
  c[3] op a[15]*b[0]+a[16]*b[1]+a[17]*b[2]+a[18]*b[3]+a[19]*b[4]; \
  c[4] op a[20]*b[0]+a[21]*b[1]+a[22]*b[2]+a[23]*b[3]+a[24]*b[4];

#define DENSEMATRIXTRANSTIMESVECTOR5(op, a, b, c) \
  c[0] op a[0]*b[0]+a[5]*b[1]+a[10]*b[2]+a[15]*b[3]+a[20]*b[4]; \
  c[1] op a[1]*b[0]+a[6]*b[1]+a[11]*b[2]+a[16]*b[3]+a[21]*b[4]; \
  c[2] op a[2]*b[0]+a[7]*b[1]+a[12]*b[2]+a[17]*b[3]+a[22]*b[4]; \
  c[3] op a[3]*b[0]+a[8]*b[1]+a[13]*b[2]+a[18]*b[3]+a[23]*b[4]; \
  c[4] op a[4]*b[0]+a[9]*b[1]+a[14]*b[2]+a[19]*b[3]+a[24]*b[4];

#define DENSEMATRIXTIMESVECTOR6(op, a, b, c) \
  c[0] op a[0]*b[0]+a[1]*b[1]+a[2]*b[2]+a[3]*b[3]+a[4]*b[4]+a[5]*b[5]; \
  c[1] op a[6]*b[0]+a[7]*b[1]+a[8]*b[2]+a[9]*b[3]+a[10]*b[4]+a[11]*b[5]; \
  c[2] op a[12]*b[0]+a[13]*b[1]+a[14]*b[2]+a[15]*b[3]+a[16]*b[4]+a[17]*b[5]; \
  c[3] op a[18]*b[0]+a[19]*b[1]+a[20]*b[2]+a[21]*b[3]+a[22]*b[4]+a[23]*b[5]; \
  c[4] op a[24]*b[0]+a[25]*b[1]+a[26]*b[2]+a[27]*b[3]+a[28]*b[4]+a[29]*b[5]; \
  c[5] op a[30]*b[0]+a[31]*b[1]+a[32]*b[2]+a[33]*b[3]+a[34]*b[4]+a[35]*b[5];

#define DENSEMATRIXTRANSTIMESVECTOR6(op, a, b, c) \
  c[0] op a[0]*b[0]+a[6]*b[1]+a[12]*b[2]+a[18]*b[3]+a[24]*b[4]+a[30]*b[5]; \
  c[1] op a[1]*b[0]+a[7]*b[1]+a[13]*b[2]+a[19]*b[3]+a[25]*b[4]+a[31]*b[5]; \
  c[2] op a[2]*b[0]+a[8]*b[1]+a[14]*b[2]+a[20]*b[3]+a[26]*b[4]+a[32]*b[5]; \
  c[3] op a[3]*b[0]+a[9]*b[1]+a[15]*b[2]+a[21]*b[3]+a[27]*b[4]+a[33]*b[5]; \
  c[4] op a[4]*b[0]+a[10]*b[1]+a[16]*b[2]+a[22]*b[3]+a[28]*b[4]+a[34]*b[5]; \
  c[5] op a[5]*b[0]+a[11]*b[1]+a[17]*b[2]+a[23]*b[3]+a[29]*b[4]+a[35]*b[5];

#define DENSEMATRIXTIMESVECTOR7(op, a, b, c) \
  c[0] op a[0]*b[0]+a[1]*b[1]+a[2]*b[2]+a[3]*b[3]+a[4]*b[4]+a[5]*b[5]+a[6]*b[6]; \
  c[1] op a[7]*b[0]+a[8]*b[1]+a[9]*b[2]+a[10]*b[3]+a[11]*b[4]+a[12]*b[5]+a[13]*b[6]; \
  c[2] op a[14]*b[0]+a[15]*b[1]+a[16]*b[2]+a[17]*b[3]+a[18]*b[4]+a[19]*b[5]+a[20]*b[6]; \
  c[3] op a[21]*b[0]+a[22]*b[1]+a[23]*b[2]+a[24]*b[3]+a[25]*b[4]+a[26]*b[5]+a[27]*b[6]; \
  c[4] op a[28]*b[0]+a[29]*b[1]+a[30]*b[2]+a[31]*b[3]+a[32]*b[4]+a[33]*b[5]+a[34]*b[6]; \
  c[5] op a[35]*b[0]+a[36]*b[1]+a[37]*b[2]+a[38]*b[3]+a[39]*b[4]+a[40]*b[5]+a[41]*b[6]; \
  c[6] op a[42]*b[0]+a[43]*b[1]+a[44]*b[2]+a[45]*b[3]+a[46]*b[4]+a[47]*b[5]+a[48]*b[6];

#define DENSEMATRIXTRANSTIMESVECTOR7(op, a, b, c) \
  c[0] op a[0]*b[0]+a[7]*b[1]+a[14]*b[2]+a[21]*b[3]+a[28]*b[4]+a[35]*b[5]+a[42]*b[6]; \
  c[1] op a[1]*b[0]+a[8]*b[1]+a[15]*b[2]+a[22]*b[3]+a[29]*b[4]+a[36]*b[5]+a[43]*b[6]; \
  c[2] op a[2]*b[0]+a[9]*b[1]+a[16]*b[2]+a[23]*b[3]+a[30]*b[4]+a[37]*b[5]+a[44]*b[6]; \
  c[3] op a[3]*b[0]+a[10]*b[1]+a[17]*b[2]+a[24]*b[3]+a[31]*b[4]+a[38]*b[5]+a[45]*b[6]; \
  c[4] op a[4]*b[0]+a[11]*b[1]+a[18]*b[2]+a[25]*b[3]+a[32]*b[4]+a[39]*b[5]+a[46]*b[6]; \
  c[5] op a[5]*b[0]+a[12]*b[1]+a[19]*b[2]+a[26]*b[3]+a[33]*b[4]+a[40]*b[5]+a[47]*b[6]; \
  c[6] op a[6]*b[0]+a[13]*b[1]+a[20]*b[2]+a[27]*b[3]+a[34]*b[4]+a[41]*b[5]+a[48]*b[6];

//------------------------------------------------------------------------------

#ifndef USE_EIGEN3_DenseMatrixOp
template<>
class DenseMatrixOp<1, 1> {

 public:
#ifdef USE_EIGEN3_DenseMatrixOp_NoMixedPrecision
  template<class Scalar>
  static void applyToDenseMatrix(Scalar a[1], Scalar b[1], Scalar c[1]) {
    const Eigen::Map<const Eigen::Matrix<Scalar,1,1,Eigen::ColMajor>> A(a,1,1);
    const Eigen::Map<const Eigen::Matrix<Scalar,1,1,Eigen::ColMajor>> B(b,1,1);
    Eigen::Map<Eigen::Matrix<Scalar,1,1,Eigen::ColMajor>> C(c,1,1);
    C.noalias() = A*B;
  }

  template<class Scalar>
  static void applyAndAddToDenseMatrix(Scalar a[1], Scalar b[1], Scalar c[1]) {
    const Eigen::Map<const Eigen::Matrix<Scalar,1,1,Eigen::ColMajor>> A(a,1,1);
    const Eigen::Map<const Eigen::Matrix<Scalar,1,1,Eigen::ColMajor>> B(b,1,1);
    Eigen::Map<Eigen::Matrix<Scalar,1,1,Eigen::ColMajor>> C(c,1,1);
    C.noalias() += A*B;
  }

  template<class Scalar>
  static void applyAndAddTransToDenseMatrix(Scalar a[1], Scalar b[1], Scalar c[1]) {
    const Eigen::Map<const Eigen::Matrix<Scalar,1,1,Eigen::ColMajor>> A(a,1,1);
    const Eigen::Map<const Eigen::Matrix<Scalar,1,1,Eigen::ColMajor>> B(b,1,1);
    Eigen::Map<Eigen::Matrix<Scalar,1,1,Eigen::ColMajor>> C(c,1,1);
    C.noalias() += (A*B).transpose();
  }

  template<class Scalar>
  static void applyAndSubToDenseMatrix(Scalar a[1], Scalar b[1], Scalar c[1]) {
    const Eigen::Map<const Eigen::Matrix<Scalar,1,1,Eigen::ColMajor>> A(a,1,1);
    const Eigen::Map<const Eigen::Matrix<Scalar,1,1,Eigen::ColMajor>> B(b,1,1);
    Eigen::Map<Eigen::Matrix<Scalar,1,1,Eigen::ColMajor>> C(c,1,1);
    C.noalias() -= A*B;
  }

  template<class Scalar>
  static void applyTransToDenseMatrix(Scalar a[1], Scalar b[1], Scalar c[1]) {
    const Eigen::Map<const Eigen::Matrix<Scalar,1,1,Eigen::ColMajor>> A(a,1,1);
    const Eigen::Map<const Eigen::Matrix<Scalar,1,1,Eigen::ColMajor>> B(b,1,1);
    Eigen::Map<Eigen::Matrix<Scalar,1,1,Eigen::ColMajor>> C(c,1,1);
    C.noalias() = A.transpose()*B;
  }

  template<class Scalar>
  static void applyTransAndAddToDenseMatrix(Scalar a[1], Scalar b[1], Scalar c[1]) {
    const Eigen::Map<const Eigen::Matrix<Scalar,1,1,Eigen::ColMajor>> A(a,1,1);
    const Eigen::Map<const Eigen::Matrix<Scalar,1,1,Eigen::ColMajor>> B(b,1,1);
    Eigen::Map<Eigen::Matrix<Scalar,1,1,Eigen::ColMajor>> C(c,1,1);
    C.noalias() += A.transpose()*B;
  }

  template<class Scalar>
  static void applyTransAndSubToDenseMatrix(Scalar a[1], Scalar b[1], Scalar c[1]) {
    const Eigen::Map<const Eigen::Matrix<Scalar,1,1,Eigen::ColMajor>> A(a,1,1);
    const Eigen::Map<const Eigen::Matrix<Scalar,1,1,Eigen::ColMajor>> B(b,1,1);
    Eigen::Map<Eigen::Matrix<Scalar,1,1,Eigen::ColMajor>> C(c,1,1);
    C.noalias() -= A.transpose()*B;
  }

  template<class Scalar>
  static void applyToVector(Scalar a[1], Scalar b[1], Scalar c[1]) {
    const Eigen::Map<const Eigen::Matrix<Scalar,1,1,Eigen::ColMajor>> A(a,1,1);
    const Eigen::Map<const Eigen::Matrix<Scalar,1,1>> B(b,1,1);
    Eigen::Map<Eigen::Matrix<Scalar,1,1>> C(c,1,1);
    C.noalias() = A*B;
  }

  template<class Scalar>
  static void applyAndAddToVector(Scalar a[1], Scalar b[1], Scalar c[1]) {
    const Eigen::Map<const Eigen::Matrix<Scalar,1,1,Eigen::ColMajor>> A(a,1,1);
    const Eigen::Map<const Eigen::Matrix<Scalar,1,1>> B(b,1,1);
    Eigen::Map<Eigen::Matrix<Scalar,1,1>> C(c,1,1);
    C.noalias() += A*B;
  }

  template<class Scalar>
  static void applyAndSubToVector(Scalar a[1], Scalar b[1], Scalar c[1]) {
    const Eigen::Map<const Eigen::Matrix<Scalar,1,1,Eigen::ColMajor>> A(a,1,1);
    const Eigen::Map<const Eigen::Matrix<Scalar,1,1>> B(b,1,1);
    Eigen::Map<Eigen::Matrix<Scalar,1,1>> C(c,1,1);
    C.noalias() -= A*B;
  }

  template<class Scalar>
  static void applyTransToVector(Scalar a[1], Scalar b[1], Scalar c[1]) {
    const Eigen::Map<const Eigen::Matrix<Scalar,1,1,Eigen::ColMajor>> A(a,1,1);
    const Eigen::Map<const Eigen::Matrix<Scalar,1,1>> B(b,1,1);
    Eigen::Map<Eigen::Matrix<Scalar,1,1>> C(c,1,1);
    C.noalias() = A.transpose()*B;
  }

  template<class Scalar>
  static void applyTransAndAddToVector(Scalar a[1], Scalar b[1], Scalar c[1]) {
    const Eigen::Map<const Eigen::Matrix<Scalar,1,1,Eigen::ColMajor>> A(a,1,1);
    const Eigen::Map<const Eigen::Matrix<Scalar,1,1>> B(b,1,1);
    Eigen::Map<Eigen::Matrix<Scalar,1,1>> C(c,1,1);
    C.noalias() += A.transpose()*B;
  }

  template<class Scalar>
  static void applyTransAndSubToVector(Scalar a[1], Scalar b[1], Scalar c[1]) {
    const Eigen::Map<const Eigen::Matrix<Scalar,1,1,Eigen::ColMajor>> A(a,1,1);
    const Eigen::Map<const Eigen::Matrix<Scalar,1,1>> B(b,1,1);
    Eigen::Map<Eigen::Matrix<Scalar,1,1>> C(c,1,1);
    C.noalias() -= A.transpose()*B;
  }

  template<class Scalar>
  static void transpose(Scalar a[1], Scalar b[1]) {
    const Eigen::Map<const Eigen::Matrix<Scalar,1,1,Eigen::ColMajor>> A(a,1,1);
    Eigen::Map<Eigen::Matrix<Scalar,1,1,Eigen::ColMajor>> B(b,1,1);
    B.noalias() = A.transpose();
  }
#endif

  template<class Scalar, class Scalar2>
  static void applyToDenseMatrix(Scalar a[1], Scalar b[1], Scalar2 c[1]) {
    DENSEMATRIXTIMESDENSEMATRIX1( =, a, b, c);
  }

  template<class Scalar, class Scalar2>
  static void applyAndAddToDenseMatrix(Scalar a[1], Scalar b[1], Scalar2 c[1]) {
    DENSEMATRIXTIMESDENSEMATRIX1( +=, a, b, c);
  }

  template<class Scalar, class Scalar2>
  static void applyAndAddTransToDenseMatrix(Scalar a[1], Scalar b[1], Scalar2 c[1]) {
    DENSEMATRIXTIMESDENSEMATRIX1TRANS( +=, a, b, c);
  }

  template<class Scalar, class Scalar2>
  static void applyAndSubToDenseMatrix(Scalar a[1], Scalar b[1], Scalar2 c[1]) {
    DENSEMATRIXTIMESDENSEMATRIX1( -=, a, b, c);
  }

  template<class Scalar, class Scalar2>
  static void applyTransToDenseMatrix(Scalar a[1], Scalar b[1], Scalar2 c[1]) {
    DENSEMATRIXTRANSTIMESDENSEMATRIX1( =, a, b, c);
  }

  template<class Scalar, class Scalar2>
  static void applyTransAndAddToDenseMatrix(Scalar a[1], Scalar b[1], Scalar2 c[1]) {
    DENSEMATRIXTRANSTIMESDENSEMATRIX1( +=, a, b, c);
  }

  template<class Scalar, class Scalar2>
  static void applyTransAndSubToDenseMatrix(Scalar a[1], Scalar b[1], Scalar2 c[1]) {
    DENSEMATRIXTRANSTIMESDENSEMATRIX1( -=, a, b, c);
  }

  template<class Scalar, class Scalar2>
  static void applyToVector(Scalar a[1], Scalar2 b[1], Scalar2 c[1]) {
    DENSEMATRIXTIMESVECTOR1( =, a, b, c);
  }

  template<class Scalar, class Scalar2>
  static void applyAndAddToVector(Scalar a[1], Scalar2 b[1], Scalar2 c[1]) {
    DENSEMATRIXTIMESVECTOR1( +=, a, b, c);
  }

  template<class Scalar, class Scalar2>
  static void applyAndSubToVector(Scalar a[1], Scalar2 b[1], Scalar2 c[1]) {
    DENSEMATRIXTIMESVECTOR1( -=, a, b, c);
  }

  template<class Scalar, class Scalar2>
  static void applyTransToVector(Scalar a[1], Scalar2 b[1], Scalar2 c[1]) {
    DENSEMATRIXTRANSTIMESVECTOR1( =, a, b, c);
  }

  template<class Scalar, class Scalar2>
  static void applyTransAndAddToVector(Scalar a[1], Scalar2 b[1], Scalar2 c[1]) {
    DENSEMATRIXTRANSTIMESVECTOR1( +=, a, b, c);
  }

  template<class Scalar, class Scalar2>
  static void applyTransAndSubToVector(Scalar a[1], Scalar2 b[1], Scalar2 c[1]) {
    DENSEMATRIXTRANSTIMESVECTOR1( -=, a, b, c);
  }

  template<class Scalar, class Scalar2>
  static void transpose(Scalar a[1], Scalar2 b[1]) {
    b[0] = a[0];
  }

};

//------------------------------------------------------------------------------

template<>
class DenseMatrixOp<2, 4> {

 public:

#ifdef USE_EIGEN3_DenseMatrixOp_NoMixedPrecision
  template<class Scalar>
  static void applyToDenseMatrix(Scalar a[4], Scalar b[4], Scalar c[4]) {
    const Eigen::Map<const Eigen::Matrix<Scalar,2,2,Eigen::RowMajor>> A(a,2,2);
    const Eigen::Map<const Eigen::Matrix<Scalar,2,2,Eigen::RowMajor>> B(b,2,2);
    Eigen::Map<Eigen::Matrix<Scalar,2,2,Eigen::RowMajor>> C(c,2,2);
    C.noalias() = A*B;
  }

  template<class Scalar>
  static void applyAndAddToDenseMatrix(Scalar a[4], Scalar b[4], Scalar c[4]) {
    const Eigen::Map<const Eigen::Matrix<Scalar,2,2,Eigen::RowMajor>> A(a,2,2);
    const Eigen::Map<const Eigen::Matrix<Scalar,2,2,Eigen::RowMajor>> B(b,2,2);
    Eigen::Map<Eigen::Matrix<Scalar,2,2,Eigen::RowMajor>> C(c,2,2);
    C.noalias() += A*B;
  }

  template<class Scalar>
  static void applyAndAddTransToDenseMatrix(Scalar a[4], Scalar b[4], Scalar c[4]) {
    const Eigen::Map<const Eigen::Matrix<Scalar,2,2,Eigen::RowMajor>> A(a,2,2);
    const Eigen::Map<const Eigen::Matrix<Scalar,2,2,Eigen::RowMajor>> B(b,2,2);
    Eigen::Map<Eigen::Matrix<Scalar,2,2,Eigen::RowMajor>> C(c,2,2);
    C.noalias() += (A*B).transpose();
  }

  template<class Scalar>
  static void applyAndSubToDenseMatrix(Scalar a[4], Scalar b[4], Scalar c[4]) {
    const Eigen::Map<const Eigen::Matrix<Scalar,2,2,Eigen::RowMajor>> A(a,2,2);
    const Eigen::Map<const Eigen::Matrix<Scalar,2,2,Eigen::RowMajor>> B(b,2,2);
    Eigen::Map<Eigen::Matrix<Scalar,2,2,Eigen::RowMajor>> C(c,2,2);
    C.noalias() -= A*B;
  }

  template<class Scalar>
  static void applyTransToDenseMatrix(Scalar a[4], Scalar b[4], Scalar c[4]) {
    const Eigen::Map<const Eigen::Matrix<Scalar,2,2,Eigen::RowMajor>> A(a,2,2);
    const Eigen::Map<const Eigen::Matrix<Scalar,2,2,Eigen::RowMajor>> B(b,2,2);
    Eigen::Map<Eigen::Matrix<Scalar,2,2,Eigen::RowMajor>> C(c,2,2);
    C.noalias() = A.transpose()*B;
  }

  template<class Scalar>
  static void applyTransAndAddToDenseMatrix(Scalar a[4], Scalar b[4], Scalar c[4]) {
    const Eigen::Map<const Eigen::Matrix<Scalar,2,2,Eigen::RowMajor>> A(a,2,2);
    const Eigen::Map<const Eigen::Matrix<Scalar,2,2,Eigen::RowMajor>> B(b,2,2);
    Eigen::Map<Eigen::Matrix<Scalar,2,2,Eigen::RowMajor>> C(c,2,2);
    C.noalias() += A.transpose()*B;
  }

  template<class Scalar>
  static void applyTransAndSubToDenseMatrix(Scalar a[4], Scalar b[4], Scalar c[4]) {
    const Eigen::Map<const Eigen::Matrix<Scalar,2,2,Eigen::RowMajor>> A(a,2,2);
    const Eigen::Map<const Eigen::Matrix<Scalar,2,2,Eigen::RowMajor>> B(b,2,2);
    Eigen::Map<Eigen::Matrix<Scalar,2,2,Eigen::RowMajor>> C(c,2,2);
    C.noalias() -= A.transpose()*B;
  }

  template<class Scalar>
  static void applyToVector(Scalar a[4], Scalar b[2], Scalar c[2]) {
    const Eigen::Map<const Eigen::Matrix<Scalar,2,2,Eigen::RowMajor>> A(a,2,2);
    const Eigen::Map<const Eigen::Matrix<Scalar,2,1>> B(b,2,1);
    Eigen::Map<Eigen::Matrix<Scalar,2,1>> C(c,2,1);
    C.noalias() = A*B;
  }

  template<class Scalar>
  static void applyAndAddToVector(Scalar a[4], Scalar b[2], Scalar c[2]) {
    const Eigen::Map<const Eigen::Matrix<Scalar,2,2,Eigen::RowMajor>> A(a,2,2);
    const Eigen::Map<const Eigen::Matrix<Scalar,2,1>> B(b,2,1);
    Eigen::Map<Eigen::Matrix<Scalar,2,1>> C(c,2,1);
    C.noalias() += A*B;
  }

  template<class Scalar>
  static void applyAndSubToVector(Scalar a[4], Scalar b[2], Scalar c[2]) {
    const Eigen::Map<const Eigen::Matrix<Scalar,2,2,Eigen::RowMajor>> A(a,2,2);
    const Eigen::Map<const Eigen::Matrix<Scalar,2,1>> B(b,2,1);
    Eigen::Map<Eigen::Matrix<Scalar,2,1>> C(c,2,1);
    C.noalias() -= A*B;
  }

  template<class Scalar>
  static void applyTransToVector(Scalar a[4], Scalar b[2], Scalar c[2]) {
    const Eigen::Map<const Eigen::Matrix<Scalar,2,2,Eigen::RowMajor>> A(a,2,2);
    const Eigen::Map<const Eigen::Matrix<Scalar,2,1>> B(b,2,1);
    Eigen::Map<Eigen::Matrix<Scalar,2,1>> C(c,2,1);
    C.noalias() = A.transpose()*B;
  }

  template<class Scalar>
  static void applyTransAndAddToVector(Scalar a[4], Scalar b[2], Scalar c[2]) {
    const Eigen::Map<const Eigen::Matrix<Scalar,2,2,Eigen::RowMajor>> A(a,2,2);
    const Eigen::Map<const Eigen::Matrix<Scalar,2,1>> B(b,2,1);
    Eigen::Map<Eigen::Matrix<Scalar,2,1>> C(c,2,1);
    C.noalias() += A.transpose()*B;
  }

  template<class Scalar>
  static void applyTransAndSubToVector(Scalar a[4], Scalar b[2], Scalar c[2]) {
    const Eigen::Map<const Eigen::Matrix<Scalar,2,2,Eigen::RowMajor>> A(a,2,2);
    const Eigen::Map<const Eigen::Matrix<Scalar,2,1>> B(b,2,1);
    Eigen::Map<Eigen::Matrix<Scalar,2,1>> C(c,2,1);
    C.noalias() -= A.transpose()*B;
  }

  template<class Scalar>
  static void transpose(Scalar a[4], Scalar b[4]) {
    const Eigen::Map<const Eigen::Matrix<Scalar,2,2,Eigen::RowMajor>> A(a,2,2);
    Eigen::Map<Eigen::Matrix<Scalar,2,2,Eigen::RowMajor>> B(b,2,2);
    B.noalias() = A.transpose();
  }
#endif

  template<class Scalar, class Scalar2>
  static void applyToDenseMatrix(Scalar a[4], Scalar b[4], Scalar2 c[4]) {
    DENSEMATRIXTIMESDENSEMATRIX2( =, a, b, c);
  }

  template<class Scalar, class Scalar2>
  static void applyAndAddToDenseMatrix(Scalar a[4], Scalar b[4], Scalar2 c[4]) {
    DENSEMATRIXTIMESDENSEMATRIX2( +=, a, b, c);
  }

  template<class Scalar, class Scalar2>
  static void applyAndAddTransToDenseMatrix(Scalar a[4], Scalar b[4], Scalar2 c[4]) {
    DENSEMATRIXTIMESDENSEMATRIX2TRANS( +=, a, b, c);
  }

  template<class Scalar, class Scalar2>
  static void applyAndSubToDenseMatrix(Scalar a[4], Scalar b[4], Scalar2 c[4]) {
    DENSEMATRIXTIMESDENSEMATRIX2( -=, a, b, c);
  }

  template<class Scalar, class Scalar2>
  static void applyTransToDenseMatrix(Scalar a[4], Scalar b[4], Scalar2 c[4]) {
    DENSEMATRIXTRANSTIMESDENSEMATRIX2( =, a, b, c);
  }

  template<class Scalar, class Scalar2>
  static void applyTransAndAddToDenseMatrix(Scalar a[4], Scalar b[4], Scalar2 c[4]) {
    DENSEMATRIXTRANSTIMESDENSEMATRIX2( +=, a, b, c);
  }

  template<class Scalar, class Scalar2>
  static void applyTransAndSubToDenseMatrix(Scalar a[4], Scalar b[4], Scalar2 c[4]) {
    DENSEMATRIXTRANSTIMESDENSEMATRIX2( -=, a, b, c);
  }

  template<class Scalar, class Scalar2>
  static void applyToVector(Scalar a[4], Scalar2 b[2], Scalar2 c[2]) {
    DENSEMATRIXTIMESVECTOR2( =, a, b, c);
  }

  template<class Scalar, class Scalar2>
  static void applyAndAddToVector(Scalar a[4], Scalar2 b[2], Scalar2 c[2]) {
    DENSEMATRIXTIMESVECTOR2( +=, a, b, c);
  }

  template<class Scalar, class Scalar2>
  static void applyAndSubToVector(Scalar a[4], Scalar2 b[2], Scalar2 c[2]) {
    DENSEMATRIXTIMESVECTOR2( -=, a, b, c);
  }

  template<class Scalar, class Scalar2>
  static void applyTransToVector(Scalar a[4], Scalar2 b[2], Scalar2 c[2]) {
    DENSEMATRIXTRANSTIMESVECTOR2( =, a, b, c);
  }

  template<class Scalar, class Scalar2>
  static void applyTransAndAddToVector(Scalar a[4], Scalar2 b[2], Scalar2 c[2]) {
    DENSEMATRIXTRANSTIMESVECTOR2( +=, a, b, c);
  }

  template<class Scalar, class Scalar2>
  static void applyTransAndSubToVector(Scalar a[4], Scalar2 b[2], Scalar2 c[2]) {
    DENSEMATRIXTRANSTIMESVECTOR2( -=, a, b, c);
  }

  template<class Scalar, class Scalar2>
  static void transpose(Scalar a[4], Scalar2 b[4]) {
    b[0] = a[0];
    b[1] = a[2];
    b[2] = a[1];
    b[3] = a[3];
  }

};

//------------------------------------------------------------------------------

template<>
class DenseMatrixOp<3, 9> {

 public:

#ifdef USE_EIGEN3_DenseMatrixOp_NoMixedPrecision
  template<class Scalar>
  static void applyToDenseMatrix(Scalar a[9], Scalar b[9], Scalar c[9]) {
    const Eigen::Map<const Eigen::Matrix<Scalar,3,3,Eigen::RowMajor>> A(a,3,3);
    const Eigen::Map<const Eigen::Matrix<Scalar,3,3,Eigen::RowMajor>> B(b,3,3);
    Eigen::Map<Eigen::Matrix<Scalar,3,3,Eigen::RowMajor>> C(c,3,3);
    C.noalias() = A*B;
  }

  template<class Scalar>
  static void applyAndAddToDenseMatrix(Scalar a[9], Scalar b[9], Scalar c[9]) {
    const Eigen::Map<const Eigen::Matrix<Scalar,3,3,Eigen::RowMajor>> A(a,3,3);
    const Eigen::Map<const Eigen::Matrix<Scalar,3,3,Eigen::RowMajor>> B(b,3,3);
    Eigen::Map<Eigen::Matrix<Scalar,3,3,Eigen::RowMajor>> C(c,3,3);
    C.noalias() += A*B;
  }

  template<class Scalar>
  static void applyAndAddTransToDenseMatrix(Scalar a[9], Scalar b[9], Scalar c[9]) {
    const Eigen::Map<const Eigen::Matrix<Scalar,3,3,Eigen::RowMajor>> A(a,3,3);
    const Eigen::Map<const Eigen::Matrix<Scalar,3,3,Eigen::RowMajor>> B(b,3,3);
    Eigen::Map<Eigen::Matrix<Scalar,3,3,Eigen::RowMajor>> C(c,3,3);
    C.noalias() += (A*B).transpose();
  }

  template<class Scalar>
  static void applyAndSubToDenseMatrix(Scalar a[9], Scalar b[9], Scalar c[9]) {
    const Eigen::Map<const Eigen::Matrix<Scalar,3,3,Eigen::RowMajor>> A(a,3,3);
    const Eigen::Map<const Eigen::Matrix<Scalar,3,3,Eigen::RowMajor>> B(b,3,3);
    Eigen::Map<Eigen::Matrix<Scalar,3,3,Eigen::RowMajor>> C(c,3,3);
    C.noalias() -= A*B;
  }

  template<class Scalar>
  static void applyTransToDenseMatrix(Scalar a[9], Scalar b[9], Scalar c[9]) {
    const Eigen::Map<const Eigen::Matrix<Scalar,3,3,Eigen::RowMajor>> A(a,3,3);
    const Eigen::Map<const Eigen::Matrix<Scalar,3,3,Eigen::RowMajor>> B(b,3,3);
    Eigen::Map<Eigen::Matrix<Scalar,3,3,Eigen::RowMajor>> C(c,3,3);
    C.noalias() = A.transpose()*B;
  }

  template<class Scalar>
  static void applyTransAndAddToDenseMatrix(Scalar a[9], Scalar b[9], Scalar c[9]) {
    const Eigen::Map<const Eigen::Matrix<Scalar,3,3,Eigen::RowMajor>> A(a,3,3);
    const Eigen::Map<const Eigen::Matrix<Scalar,3,3,Eigen::RowMajor>> B(b,3,3);
    Eigen::Map<Eigen::Matrix<Scalar,3,3,Eigen::RowMajor>> C(c,3,3);
    C.noalias() += A.transpose()*B;
  }

  template<class Scalar>
  static void applyTransAndSubToDenseMatrix(Scalar a[9], Scalar b[9], Scalar c[9]) {
    const Eigen::Map<const Eigen::Matrix<Scalar,3,3,Eigen::RowMajor>> A(a,3,3);
    const Eigen::Map<const Eigen::Matrix<Scalar,3,3,Eigen::RowMajor>> B(b,3,3);
    Eigen::Map<Eigen::Matrix<Scalar,3,3,Eigen::RowMajor>> C(c,3,3);
    C.noalias() -= A.transpose()*B;
  }

  template<class Scalar>
  static void applyToVector(Scalar a[9], Scalar b[3], Scalar c[3]) {
    const Eigen::Map<const Eigen::Matrix<Scalar,3,3,Eigen::RowMajor>> A(a,3,3);
    const Eigen::Map<const Eigen::Matrix<Scalar,3,1>> B(b,3,1);
    Eigen::Map<Eigen::Matrix<Scalar,3,1>> C(c,3,1);
    C.noalias() = A*B;
  }

  template<class Scalar>
  static void applyAndAddToVector(Scalar a[9], Scalar b[3], Scalar c[3]) {
    const Eigen::Map<const Eigen::Matrix<Scalar,3,3,Eigen::RowMajor>> A(a,3,3);
    const Eigen::Map<const Eigen::Matrix<Scalar,3,1>> B(b,3,1);
    Eigen::Map<Eigen::Matrix<Scalar,3,1>> C(c,3,1);
    C.noalias() += A*B;
  }

  template<class Scalar>
  static void applyAndSubToVector(Scalar a[9], Scalar b[3], Scalar c[3]) {
    const Eigen::Map<const Eigen::Matrix<Scalar,3,3,Eigen::RowMajor>> A(a,3,3);
    const Eigen::Map<const Eigen::Matrix<Scalar,3,1>> B(b,3,1);
    Eigen::Map<Eigen::Matrix<Scalar,3,1>> C(c,3,1);
    C.noalias() -= A*B;
  }

  template<class Scalar>
  static void applyTransToVector(Scalar a[9], Scalar b[3], Scalar c[3]) {
    const Eigen::Map<const Eigen::Matrix<Scalar,3,3,Eigen::RowMajor>> A(a,3,3);
    const Eigen::Map<const Eigen::Matrix<Scalar,3,1>> B(b,3,1);
    Eigen::Map<Eigen::Matrix<Scalar,3,1>> C(c,3,1);
    C.noalias() = A.transpose()*B;
  }

  template<class Scalar>
  static void applyTransAndAddToVector(Scalar a[9], Scalar b[3], Scalar c[3]) {
    const Eigen::Map<const Eigen::Matrix<Scalar,3,3,Eigen::RowMajor>> A(a,3,3);
    const Eigen::Map<const Eigen::Matrix<Scalar,3,1>> B(b,3,1);
    Eigen::Map<Eigen::Matrix<Scalar,3,1>> C(c,3,1);
    C.noalias() += A.transpose()*B;
  }

  template<class Scalar>
  static void applyTransAndSubToVector(Scalar a[9], Scalar b[3], Scalar c[3]) {
    const Eigen::Map<const Eigen::Matrix<Scalar,3,3,Eigen::RowMajor>> A(a,3,3);
    const Eigen::Map<const Eigen::Matrix<Scalar,3,1>> B(b,3,1);
    Eigen::Map<Eigen::Matrix<Scalar,3,1>> C(c,3,1);
    C.noalias() -= A.transpose()*B;
  }

  template<class Scalar>
  static void transpose(Scalar a[9], Scalar b[9]) {
    const Eigen::Map<const Eigen::Matrix<Scalar,3,3,Eigen::RowMajor>> A(a,3,3);
    Eigen::Map<Eigen::Matrix<Scalar,3,3,Eigen::RowMajor>> B(b,3,3);
    B.noalias() = A.transpose();
  }
#endif

  template<class Scalar, class Scalar2>
  static void applyToDenseMatrix(Scalar a[9], Scalar b[9], Scalar2 c[9]) {
    DENSEMATRIXTIMESDENSEMATRIX3( =, a, b, c);
  }

  template<class Scalar, class Scalar2>
  static void applyAndAddToDenseMatrix(Scalar a[9], Scalar b[9], Scalar2 c[9]) {
    DENSEMATRIXTIMESDENSEMATRIX3( +=, a, b, c);
  }

  template<class Scalar, class Scalar2>
  static void applyAndAddTransToDenseMatrix(Scalar a[9], Scalar b[9], Scalar2 c[9]) {
    DENSEMATRIXTIMESDENSEMATRIX3TRANS( +=, a, b, c);
  }

  template<class Scalar, class Scalar2>
  static void applyAndSubToDenseMatrix(Scalar a[9], Scalar b[9], Scalar2 c[9]) {
    DENSEMATRIXTIMESDENSEMATRIX3( -=, a, b, c);
  }

  template<class Scalar, class Scalar2>
  static void applyTransToDenseMatrix(Scalar a[9], Scalar b[9], Scalar2 c[9]) {
    DENSEMATRIXTRANSTIMESDENSEMATRIX3( =, a, b, c);
  }

  template<class Scalar, class Scalar2>
  static void applyTransAndAddToDenseMatrix(Scalar a[9], Scalar b[9], Scalar2 c[9]) {
    DENSEMATRIXTRANSTIMESDENSEMATRIX3( +=, a, b, c);
  }

  template<class Scalar, class Scalar2>
  static void applyTransAndSubToDenseMatrix(Scalar a[9], Scalar b[9], Scalar2 c[9]) {
    DENSEMATRIXTRANSTIMESDENSEMATRIX3( -=, a, b, c);
  }

  template<class Scalar, class Scalar2>
  static void applyToVector(Scalar a[9], Scalar2 b[3], Scalar2 c[3]) {
    DENSEMATRIXTIMESVECTOR3( =, a, b, c);
  }

  template<class Scalar, class Scalar2>
  static void applyAndAddToVector(Scalar a[9], Scalar2 b[3], Scalar2 c[3]) {
    DENSEMATRIXTIMESVECTOR3( +=, a, b, c);
  }

  template<class Scalar, class Scalar2>
  static void applyAndSubToVector(Scalar a[9], Scalar2 b[3], Scalar2 c[3]) {
    DENSEMATRIXTIMESVECTOR3( -=, a, b, c);
  }

  template<class Scalar, class Scalar2>
  static void applyTransToVector(Scalar a[9], Scalar2 b[3], Scalar2 c[3]) {
    DENSEMATRIXTRANSTIMESVECTOR3( =, a, b, c);
  }

  template<class Scalar, class Scalar2>
  static void applyTransAndAddToVector(Scalar a[9], Scalar2 b[3], Scalar2 c[3]) {
    DENSEMATRIXTRANSTIMESVECTOR3( +=, a, b, c);
  }

  template<class Scalar, class Scalar2>
  static void applyTransAndSubToVector(Scalar a[9], Scalar2 b[3], Scalar2 c[3]) {
    DENSEMATRIXTRANSTIMESVECTOR3( -=, a, b, c);
  }

  template<class Scalar, class Scalar2>
  static void transpose(Scalar a[9], Scalar2 b[9]) {
    b[0] = a[0];
    b[1] = a[3];
    b[2] = a[6];
    b[3] = a[1];
    b[4] = a[4];
    b[5] = a[7];
    b[6] = a[2];
    b[7] = a[5];
    b[8] = a[8];
  }

};

//------------------------------------------------------------------------------

template<>
class DenseMatrixOp<4, 16> {

 public:

#ifdef USE_EIGEN3_DenseMatrixOp_NoMixedPrecision
  template<class Scalar>
  static void applyToDenseMatrix(Scalar a[16], Scalar b[16], Scalar c[16]) {
    const Eigen::Map<const Eigen::Matrix<Scalar,4,4,Eigen::RowMajor>> A(a,4,4);
    const Eigen::Map<const Eigen::Matrix<Scalar,4,4,Eigen::RowMajor>> B(b,4,4);
    Eigen::Map<Eigen::Matrix<Scalar,4,4,Eigen::RowMajor>> C(c,4,4);
    C.noalias() = A*B;
  }

  template<class Scalar>
  static void applyAndAddToDenseMatrix(Scalar a[16], Scalar b[16], Scalar c[16]) {
    const Eigen::Map<const Eigen::Matrix<Scalar,4,4,Eigen::RowMajor>> A(a,4,4);
    const Eigen::Map<const Eigen::Matrix<Scalar,4,4,Eigen::RowMajor>> B(b,4,4);
    Eigen::Map<Eigen::Matrix<Scalar,4,4,Eigen::RowMajor>> C(c,4,4);
    C.noalias() += A*B;
  }

  template<class Scalar>
  static void applyAndAddTransToDenseMatrix(Scalar a[16], Scalar b[16], Scalar c[16]) {
    const Eigen::Map<const Eigen::Matrix<Scalar,4,4,Eigen::RowMajor>> A(a,4,4);
    const Eigen::Map<const Eigen::Matrix<Scalar,4,4,Eigen::RowMajor>> B(b,4,4);
    Eigen::Map<Eigen::Matrix<Scalar,4,4,Eigen::RowMajor>> C(c,4,4);
    C.noalias() += (A*B).transpose();
  }

  template<class Scalar>
  static void applyAndSubToDenseMatrix(Scalar a[16], Scalar b[16], Scalar c[16]) {
    const Eigen::Map<const Eigen::Matrix<Scalar,4,4,Eigen::RowMajor>> A(a,4,4);
    const Eigen::Map<const Eigen::Matrix<Scalar,4,4,Eigen::RowMajor>> B(b,4,4);
    Eigen::Map<Eigen::Matrix<Scalar,4,4,Eigen::RowMajor>> C(c,4,4);
    C.noalias() -= A*B;
  }

  template<class Scalar>
  static void applyTransToDenseMatrix(Scalar a[16], Scalar b[16], Scalar c[16]) {
    const Eigen::Map<const Eigen::Matrix<Scalar,4,4,Eigen::RowMajor>> A(a,4,4);
    const Eigen::Map<const Eigen::Matrix<Scalar,4,4,Eigen::RowMajor>> B(b,4,4);
    Eigen::Map<Eigen::Matrix<Scalar,4,4,Eigen::RowMajor>> C(c,4,4);
    C.noalias() = A.transpose()*B;
  }

  template<class Scalar>
  static void applyTransAndAddToDenseMatrix(Scalar a[16], Scalar b[16], Scalar c[16]) {
    const Eigen::Map<const Eigen::Matrix<Scalar,4,4,Eigen::RowMajor>> A(a,4,4);
    const Eigen::Map<const Eigen::Matrix<Scalar,4,4,Eigen::RowMajor>> B(b,4,4);
    Eigen::Map<Eigen::Matrix<Scalar,4,4,Eigen::RowMajor>> C(c,4,4);
    C.noalias() += A.transpose()*B;
  }

  template<class Scalar>
  static void applyTransAndSubToDenseMatrix(Scalar a[16], Scalar b[16], Scalar c[16]) {
    const Eigen::Map<const Eigen::Matrix<Scalar,4,4,Eigen::RowMajor>> A(a,4,4);
    const Eigen::Map<const Eigen::Matrix<Scalar,4,4,Eigen::RowMajor>> B(b,4,4);
    Eigen::Map<Eigen::Matrix<Scalar,4,4,Eigen::RowMajor>> C(c,4,4);
    C.noalias() -= A.transpose()*B;
  }

  template<class Scalar>
  static void applyToVector(Scalar a[16], Scalar b[4], Scalar c[4]) {
    const Eigen::Map<const Eigen::Matrix<Scalar,4,4,Eigen::RowMajor>> A(a,4,4);
    const Eigen::Map<const Eigen::Matrix<Scalar,4,1>> B(b,4,1);
    Eigen::Map<Eigen::Matrix<Scalar,4,1>> C(c,4,1);
    C.noalias() = A*B;
  }

  template<class Scalar>
  static void applyAndAddToVector(Scalar a[16], Scalar b[4], Scalar c[4]) {
    const Eigen::Map<const Eigen::Matrix<Scalar,4,4,Eigen::RowMajor>> A(a,4,4);
    const Eigen::Map<const Eigen::Matrix<Scalar,4,1>> B(b,4,1);
    Eigen::Map<Eigen::Matrix<Scalar,4,1>> C(c,4,1);
    C.noalias() += A*B;
  }

  template<class Scalar>
  static void applyAndSubToVector(Scalar a[16], Scalar b[4], Scalar c[4]) {
    const Eigen::Map<const Eigen::Matrix<Scalar,4,4,Eigen::RowMajor>> A(a,4,4);
    const Eigen::Map<const Eigen::Matrix<Scalar,4,1>> B(b,4,1);
    Eigen::Map<Eigen::Matrix<Scalar,4,1>> C(c,4,1);
    C.noalias() -= A*B;
  }

  template<class Scalar>
  static void applyTransToVector(Scalar a[16], Scalar b[4], Scalar c[4]) {
    const Eigen::Map<const Eigen::Matrix<Scalar,4,4,Eigen::RowMajor>> A(a,4,4);
    const Eigen::Map<const Eigen::Matrix<Scalar,4,1>> B(b,4,1);
    Eigen::Map<Eigen::Matrix<Scalar,4,1>> C(c,4,1);
    C.noalias() = A.transpose()*B;
  }

  template<class Scalar>
  static void applyTransAndAddToVector(Scalar a[16], Scalar b[4], Scalar c[4]) {
    const Eigen::Map<const Eigen::Matrix<Scalar,4,4,Eigen::RowMajor>> A(a,4,4);
    const Eigen::Map<const Eigen::Matrix<Scalar,4,1>> B(b,4,1);
    Eigen::Map<Eigen::Matrix<Scalar,4,1>> C(c,4,1);
    C.noalias() += A.transpose()*B;
  }

  template<class Scalar>
  static void applyTransAndSubToVector(Scalar a[16], Scalar b[4], Scalar c[4]) {
    const Eigen::Map<const Eigen::Matrix<Scalar,4,4,Eigen::RowMajor>> A(a,4,4);
    const Eigen::Map<const Eigen::Matrix<Scalar,4,1>> B(b,4,1);
    Eigen::Map<Eigen::Matrix<Scalar,4,1>> C(c,4,1);
    C.noalias() -= A.transpose()*B;
  }

  template<class Scalar>
  static void transpose(Scalar a[16], Scalar b[16]) {
    const Eigen::Map<const Eigen::Matrix<Scalar,4,4,Eigen::RowMajor>> A(a,4,4);
    Eigen::Map<Eigen::Matrix<Scalar,4,4,Eigen::RowMajor>> B(b,4,4);
    B.noalias() = A.transpose();
  }
#endif

  template<class Scalar, class Scalar2>
  static void applyToDenseMatrix(Scalar a[16], Scalar b[16], Scalar2 c[16]) {
    DENSEMATRIXTIMESDENSEMATRIX4( =, a, b, c);
  }

  template<class Scalar, class Scalar2>
  static void applyAndAddToDenseMatrix(Scalar a[16], Scalar b[16], Scalar2 c[16]) {
    DENSEMATRIXTIMESDENSEMATRIX4( +=, a, b, c);
  }

  template<class Scalar, class Scalar2>
  static void applyAndAddTransToDenseMatrix(Scalar a[16], Scalar b[16], Scalar2 c[16]) {
    DENSEMATRIXTIMESDENSEMATRIX4TRANS( +=, a, b, c);
  }

  template<class Scalar, class Scalar2>
  static void applyAndSubToDenseMatrix(Scalar a[16], Scalar b[16], Scalar2 c[16]) {
    DENSEMATRIXTIMESDENSEMATRIX4( -=, a, b, c);
  }

  template<class Scalar, class Scalar2>
  static void applyTransToDenseMatrix(Scalar a[16], Scalar b[16], Scalar2 c[16]) {
    DENSEMATRIXTRANSTIMESDENSEMATRIX4( =, a, b, c);
  }

  template<class Scalar, class Scalar2>
  static void applyTransAndAddToDenseMatrix(Scalar a[16], Scalar b[16], Scalar2 c[16]) {
    DENSEMATRIXTRANSTIMESDENSEMATRIX4( +=, a, b, c);
  }

  template<class Scalar, class Scalar2>
  static void applyTransAndSubToDenseMatrix(Scalar a[16], Scalar b[16], Scalar2 c[16]) {
    DENSEMATRIXTRANSTIMESDENSEMATRIX4( -=, a, b, c);
  }

  template<class Scalar, class Scalar2>
  static void applyToVector(Scalar a[16], Scalar2 b[4], Scalar2 c[4]) {
    DENSEMATRIXTIMESVECTOR4( =, a, b, c);
  }

  template<class Scalar, class Scalar2>
  static void applyAndAddToVector(Scalar a[16], Scalar2 b[6], Scalar2 c[6]) {
    DENSEMATRIXTIMESVECTOR4( +=, a, b, c);
  }

  template<class Scalar, class Scalar2>
  static void applyAndSubToVector(Scalar a[16], Scalar2 b[4], Scalar2 c[4]) {
    DENSEMATRIXTIMESVECTOR4( -=, a, b, c);
  }

  template<class Scalar, class Scalar2>
  static void applyTransToVector(Scalar a[16], Scalar2 b[4], Scalar2 c[4]) {
    DENSEMATRIXTRANSTIMESVECTOR4( =, a, b, c);
  }

  template<class Scalar, class Scalar2>
  static void applyTransAndAddToVector(Scalar a[16], Scalar2 b[4], Scalar2 c[4]) {
    DENSEMATRIXTRANSTIMESVECTOR4( +=, a, b, c);
  }

  template<class Scalar, class Scalar2>
  static void applyTransAndSubToVector(Scalar a[16], Scalar2 b[4], Scalar2 c[4]) {
    DENSEMATRIXTRANSTIMESVECTOR4( -=, a, b, c);
  }

  template<class Scalar, class Scalar2>
  static void transpose(Scalar a[16], Scalar2 b[16]) {
    b[0] = a[0];
    b[1] = a[4];
    b[2] = a[8];
    b[3] = a[12];
    b[4] = a[1];
    b[5] = a[5];
    b[6] = a[9];
    b[7] = a[13];
    b[8] = a[2];
    b[9] = a[6];
    b[10] = a[10];
    b[11] = a[14];
    b[12] = a[3];
    b[13] = a[7];
    b[14] = a[11];
    b[15] = a[15];
  }

};

//------------------------------------------------------------------------------

template<>
class DenseMatrixOp<5, 25> {

 public:

#ifdef USE_EIGEN3_DenseMatrixOp_NoMixedPrecision
  template<class Scalar>
  static void applyToDenseMatrix(Scalar a[25], Scalar b[25], Scalar c[25]) {
    const Eigen::Map<const Eigen::Matrix<Scalar,5,5,Eigen::RowMajor>> A(a,5,5);
    const Eigen::Map<const Eigen::Matrix<Scalar,5,5,Eigen::RowMajor>> B(b,5,5);
    Eigen::Map<Eigen::Matrix<Scalar,5,5,Eigen::RowMajor>> C(c,5,5);
    C.noalias() = A*B;
  }

  template<class Scalar>
  static void applyAndAddToDenseMatrix(Scalar a[25], Scalar b[25], Scalar c[25]) {
    const Eigen::Map<const Eigen::Matrix<Scalar,5,5,Eigen::RowMajor>> A(a,5,5);
    const Eigen::Map<const Eigen::Matrix<Scalar,5,5,Eigen::RowMajor>> B(b,5,5);
    Eigen::Map<Eigen::Matrix<Scalar,5,5,Eigen::RowMajor>> C(c,5,5);
    C.noalias() += A*B;
  }

  template<class Scalar>
  static void applyAndAddTransToDenseMatrix(Scalar a[25], Scalar b[25], Scalar c[25]) {
    const Eigen::Map<const Eigen::Matrix<Scalar,5,5,Eigen::RowMajor>> A(a,5,5);
    const Eigen::Map<const Eigen::Matrix<Scalar,5,5,Eigen::RowMajor>> B(b,5,5);
    Eigen::Map<Eigen::Matrix<Scalar,5,5,Eigen::RowMajor>> C(c,5,5);
    C.noalias() += (A*B).transpose();
  }

  template<class Scalar>
  static void applyAndSubToDenseMatrix(Scalar a[25], Scalar b[25], Scalar c[25]) {
    const Eigen::Map<const Eigen::Matrix<Scalar,5,5,Eigen::RowMajor>> A(a,5,5);
    const Eigen::Map<const Eigen::Matrix<Scalar,5,5,Eigen::RowMajor>> B(b,5,5);
    Eigen::Map<Eigen::Matrix<Scalar,5,5,Eigen::RowMajor>> C(c,5,5);
    C.noalias() -= A*B;
  }

  template<class Scalar>
  static void applyTransToDenseMatrix(Scalar a[25], Scalar b[25], Scalar c[25]) {
    const Eigen::Map<const Eigen::Matrix<Scalar,5,5,Eigen::RowMajor>> A(a,5,5);
    const Eigen::Map<const Eigen::Matrix<Scalar,5,5,Eigen::RowMajor>> B(b,5,5);
    Eigen::Map<Eigen::Matrix<Scalar,5,5,Eigen::RowMajor>> C(c,5,5);
    C.noalias() = A.transpose()*B;
  }

  template<class Scalar>
  static void applyTransAndAddToDenseMatrix(Scalar a[25], Scalar b[25], Scalar c[25]) {
    const Eigen::Map<const Eigen::Matrix<Scalar,5,5,Eigen::RowMajor>> A(a,5,5);
    const Eigen::Map<const Eigen::Matrix<Scalar,5,5,Eigen::RowMajor>> B(b,5,5);
    Eigen::Map<Eigen::Matrix<Scalar,5,5,Eigen::RowMajor>> C(c,5,5);
    C.noalias() += A.transpose()*B;
  }

  template<class Scalar>
  static void applyTransAndSubToDenseMatrix(Scalar a[25], Scalar b[25], Scalar c[25]) {
    const Eigen::Map<const Eigen::Matrix<Scalar,5,5,Eigen::RowMajor>> A(a,5,5);
    const Eigen::Map<const Eigen::Matrix<Scalar,5,5,Eigen::RowMajor>> B(b,5,5);
    Eigen::Map<Eigen::Matrix<Scalar,5,5,Eigen::RowMajor>> C(c,5,5);
    C.noalias() -= A.transpose()*B;
  }

  template<class Scalar>
  static void applyToVector(Scalar a[25], Scalar b[5], Scalar c[5]) {
    const Eigen::Map<const Eigen::Matrix<Scalar,5,5,Eigen::RowMajor>> A(a,5,5);
    const Eigen::Map<const Eigen::Matrix<Scalar,5,1>> B(b,5,1);
    Eigen::Map<Eigen::Matrix<Scalar,5,1>> C(c,5,1);
    C.noalias() = A*B;
  }

  template<class Scalar>
  static void applyAndAddToVector(Scalar a[25], Scalar b[5], Scalar c[5]) {
    const Eigen::Map<const Eigen::Matrix<Scalar,5,5,Eigen::RowMajor>> A(a,5,5);
    const Eigen::Map<const Eigen::Matrix<Scalar,5,1>> B(b,5,1);
    Eigen::Map<Eigen::Matrix<Scalar,5,1>> C(c,5,1);
    C.noalias() += A*B;
  }

  template<class Scalar>
  static void applyAndSubToVector(Scalar a[25], Scalar b[5], Scalar c[5]) {
    const Eigen::Map<const Eigen::Matrix<Scalar,5,5,Eigen::RowMajor>> A(a,5,5);
    const Eigen::Map<const Eigen::Matrix<Scalar,5,1>> B(b,5,1);
    Eigen::Map<Eigen::Matrix<Scalar,5,1>> C(c,5,1);
    C.noalias() -= A*B;
  }

  template<class Scalar>
  static void applyTransToVector(Scalar a[25], Scalar b[5], Scalar c[5]) {
    const Eigen::Map<const Eigen::Matrix<Scalar,5,5,Eigen::RowMajor>> A(a,5,5);
    const Eigen::Map<const Eigen::Matrix<Scalar,5,1>> B(b,5,1);
    Eigen::Map<Eigen::Matrix<Scalar,5,1>> C(c,5,1);
    C.noalias() = A.transpose()*B;
  }

  template<class Scalar>
  static void applyTransAndAddToVector(Scalar a[25], Scalar b[5], Scalar c[5]) {
    const Eigen::Map<const Eigen::Matrix<Scalar,5,5,Eigen::RowMajor>> A(a,5,5);
    const Eigen::Map<const Eigen::Matrix<Scalar,5,1>> B(b,5,1);
    Eigen::Map<Eigen::Matrix<Scalar,5,1>> C(c,5,1);
    C.noalias() += A.transpose()*B;
  }

  template<class Scalar>
  static void applyTransAndSubToVector(Scalar a[25], Scalar b[5], Scalar c[5]) {
    const Eigen::Map<const Eigen::Matrix<Scalar,5,5,Eigen::RowMajor>> A(a,5,5);
    const Eigen::Map<const Eigen::Matrix<Scalar,5,1>> B(b,5,1);
    Eigen::Map<Eigen::Matrix<Scalar,5,1>> C(c,5,1);
    C.noalias() -= A.transpose()*B;
  }

  template<class Scalar>
  static void transpose(Scalar a[25], Scalar b[25]) {
    const Eigen::Map<const Eigen::Matrix<Scalar,5,5,Eigen::RowMajor>> A(a,5,5);
    Eigen::Map<Eigen::Matrix<Scalar,5,5,Eigen::RowMajor>> B(b,5,5);
    B.noalias() = A.transpose();
  }
#endif

  template<class Scalar, class Scalar2>
  static void applyToDenseMatrix(Scalar a[25], Scalar b[25], Scalar2 c[25]) {
    DENSEMATRIXTIMESDENSEMATRIX5( =, a, b, c);
  }

  template<class Scalar, class Scalar2>
  static void applyAndAddToDenseMatrix(Scalar a[25], Scalar b[25], Scalar2 c[25]) {
    DENSEMATRIXTIMESDENSEMATRIX5( +=, a, b, c);
  }

  template<class Scalar, class Scalar2>
  static void applyAndAddTransToDenseMatrix(Scalar a[25], Scalar b[25], Scalar2 c[25]) {
    DENSEMATRIXTIMESDENSEMATRIX5TRANS( +=, a, b, c);
  }

  template<class Scalar, class Scalar2>
  static void applyAndSubToDenseMatrix(Scalar a[25], Scalar b[25], Scalar2 c[25]) {
    DENSEMATRIXTIMESDENSEMATRIX5( -=, a, b, c);
  }

  template<class Scalar, class Scalar2>
  static void applyTransToDenseMatrix(Scalar a[25], Scalar b[25], Scalar2 c[25]) {
    DENSEMATRIXTRANSTIMESDENSEMATRIX5( =, a, b, c);
  }

  template<class Scalar, class Scalar2>
  static void applyTransAndAddToDenseMatrix(Scalar a[25], Scalar b[25], Scalar2 c[25]) {
    DENSEMATRIXTRANSTIMESDENSEMATRIX5( +=, a, b, c);
  }

  template<class Scalar, class Scalar2>
  static void applyTransAndSubToDenseMatrix(Scalar a[25], Scalar b[25], Scalar2 c[25]) {
    DENSEMATRIXTRANSTIMESDENSEMATRIX5( -=, a, b, c);
  }

  template<class Scalar, class Scalar2>
  static void applyToVector(Scalar a[25], Scalar2 b[5], Scalar2 c[5]) {
    DENSEMATRIXTIMESVECTOR5( =, a, b, c);
  }

  template<class Scalar, class Scalar2>
  static void applyAndAddToVector(Scalar a[25], Scalar2 b[5], Scalar2 c[5]) {
    DENSEMATRIXTIMESVECTOR5( +=, a, b, c);
  }

  template<class Scalar, class Scalar2>
  static void applyAndSubToVector(Scalar a[25], Scalar2 b[5], Scalar2 c[5]) {
    DENSEMATRIXTIMESVECTOR5( -=, a, b, c);
  }

  template<class Scalar, class Scalar2>
  static void applyTransToVector(Scalar a[25], Scalar2 b[5], Scalar2 c[5]) {
    DENSEMATRIXTRANSTIMESVECTOR5( =, a, b, c);
  }

  template<class Scalar, class Scalar2>
  static void applyTransAndAddToVector(Scalar a[25], Scalar2 b[5], Scalar2 c[5]) {
    DENSEMATRIXTRANSTIMESVECTOR5( +=, a, b, c);
  }

  template<class Scalar, class Scalar2>
  static void applyTransAndSubToVector(Scalar a[25], Scalar2 b[5], Scalar2 c[5]) {
    DENSEMATRIXTRANSTIMESVECTOR5( -=, a, b, c);
  }

  template<class Scalar, class Scalar2>
  static void transpose(Scalar a[25], Scalar2 b[25]) {
    b[0] = a[0];
    b[1] = a[5];
    b[2] = a[10];
    b[3] = a[15];
    b[4] = a[20];
    b[5] = a[1];
    b[6] = a[6];
    b[7] = a[11];
    b[8] = a[16];
    b[9] = a[21];
    b[10] = a[2];
    b[11] = a[7];
    b[12] = a[12];
    b[13] = a[17];
    b[14] = a[22];
    b[15] = a[3];
    b[16] = a[8];
    b[17] = a[13];
    b[18] = a[18];
    b[19] = a[23];
    b[20] = a[4];
    b[21] = a[9];
    b[22] = a[14];
    b[23] = a[19];
    b[24] = a[24];
  }

};

//------------------------------------------------------------------------------

template<>
class DenseMatrixOp<6, 36> {

 public:

#ifdef USE_EIGEN3_DenseMatrixOp_NoMixedPrecision
  template<class Scalar>
  static void applyToDenseMatrix(Scalar a[36], Scalar b[36], Scalar c[36]) {
    const Eigen::Map<const Eigen::Matrix<Scalar,6,6,Eigen::RowMajor>> A(a,6,6);
    const Eigen::Map<const Eigen::Matrix<Scalar,6,6,Eigen::RowMajor>> B(b,6,6);
    Eigen::Map<Eigen::Matrix<Scalar,6,6,Eigen::RowMajor>> C(c,6,6);
    C.noalias() = A*B;
  }

  template<class Scalar>
  static void applyAndAddToDenseMatrix(Scalar a[36], Scalar b[36], Scalar c[36]) {
    const Eigen::Map<const Eigen::Matrix<Scalar,6,6,Eigen::RowMajor>> A(a,6,6);
    const Eigen::Map<const Eigen::Matrix<Scalar,6,6,Eigen::RowMajor>> B(b,6,6);
    Eigen::Map<Eigen::Matrix<Scalar,6,6,Eigen::RowMajor>> C(c,6,6);
    C.noalias() += A*B;
  }

  template<class Scalar>
  static void applyAndAddTransToDenseMatrix(Scalar a[36], Scalar b[36], Scalar c[36]) {
    const Eigen::Map<const Eigen::Matrix<Scalar,6,6,Eigen::RowMajor>> A(a,6,6);
    const Eigen::Map<const Eigen::Matrix<Scalar,6,6,Eigen::RowMajor>> B(b,6,6);
    Eigen::Map<Eigen::Matrix<Scalar,6,6,Eigen::RowMajor>> C(c,6,6);
    C.noalias() += (A*B).transpose();
  }

  template<class Scalar>
  static void applyAndSubToDenseMatrix(Scalar a[36], Scalar b[36], Scalar c[36]) {
    const Eigen::Map<const Eigen::Matrix<Scalar,6,6,Eigen::RowMajor>> A(a,6,6);
    const Eigen::Map<const Eigen::Matrix<Scalar,6,6,Eigen::RowMajor>> B(b,6,6);
    Eigen::Map<Eigen::Matrix<Scalar,6,6,Eigen::RowMajor>> C(c,6,6);
    C.noalias() -= A*B;
  }

  template<class Scalar>
  static void applyTransToDenseMatrix(Scalar a[36], Scalar b[36], Scalar c[36]) {
    const Eigen::Map<const Eigen::Matrix<Scalar,6,6,Eigen::RowMajor>> A(a,6,6);
    const Eigen::Map<const Eigen::Matrix<Scalar,6,6,Eigen::RowMajor>> B(b,6,6);
    Eigen::Map<Eigen::Matrix<Scalar,6,6,Eigen::RowMajor>> C(c,6,6);
    C.noalias() = A.transpose()*B;
  }

  template<class Scalar>
  static void applyTransAndAddToDenseMatrix(Scalar a[36], Scalar b[36], Scalar c[36]) {
    const Eigen::Map<const Eigen::Matrix<Scalar,6,6,Eigen::RowMajor>> A(a,6,6);
    const Eigen::Map<const Eigen::Matrix<Scalar,6,6,Eigen::RowMajor>> B(b,6,6);
    Eigen::Map<Eigen::Matrix<Scalar,6,6,Eigen::RowMajor>> C(c,6,6);
    C.noalias() += A.transpose()*B;
  }

  template<class Scalar>
  static void applyTransAndSubToDenseMatrix(Scalar a[36], Scalar b[36], Scalar c[36]) {
    const Eigen::Map<const Eigen::Matrix<Scalar,6,6,Eigen::RowMajor>> A(a,6,6);
    const Eigen::Map<const Eigen::Matrix<Scalar,6,6,Eigen::RowMajor>> B(b,6,6);
    Eigen::Map<Eigen::Matrix<Scalar,6,6,Eigen::RowMajor>> C(c,6,6);
    C.noalias() -= A.transpose()*B;
  }

  template<class Scalar>
  static void applyToVector(Scalar a[36], Scalar b[6], Scalar c[6]) {
    const Eigen::Map<const Eigen::Matrix<Scalar,6,6,Eigen::RowMajor>> A(a,6,6);
    const Eigen::Map<const Eigen::Matrix<Scalar,6,1>> B(b,6,1);
    Eigen::Map<Eigen::Matrix<Scalar,6,1>> C(c,6,1);
    C.noalias() = A*B;
  }

  template<class Scalar>
  static void applyAndAddToVector(Scalar a[36], Scalar b[6], Scalar c[6]) {
    const Eigen::Map<const Eigen::Matrix<Scalar,6,6,Eigen::RowMajor>> A(a,6,6);
    const Eigen::Map<const Eigen::Matrix<Scalar,6,1>> B(b,6,1);
    Eigen::Map<Eigen::Matrix<Scalar,6,1>> C(c,6,1);
    C.noalias() += A*B;
  }

  template<class Scalar>
  static void applyAndSubToVector(Scalar a[36], Scalar b[6], Scalar c[6]) {
    const Eigen::Map<const Eigen::Matrix<Scalar,6,6,Eigen::RowMajor>> A(a,6,6);
    const Eigen::Map<const Eigen::Matrix<Scalar,6,1>> B(b,6,1);
    Eigen::Map<Eigen::Matrix<Scalar,6,1>> C(c,6,1);
    C.noalias() -= A*B;
  }

  template<class Scalar>
  static void applyTransToVector(Scalar a[36], Scalar b[6], Scalar c[6]) {
    const Eigen::Map<const Eigen::Matrix<Scalar,6,6,Eigen::RowMajor>> A(a,6,6);
    const Eigen::Map<const Eigen::Matrix<Scalar,6,1>> B(b,6,1);
    Eigen::Map<Eigen::Matrix<Scalar,6,1>> C(c,6,1);
    C.noalias() = A.transpose()*B;
  }

  template<class Scalar>
  static void applyTransAndAddToVector(Scalar a[36], Scalar b[6], Scalar c[6]) {
    const Eigen::Map<const Eigen::Matrix<Scalar,6,6,Eigen::RowMajor>> A(a,6,6);
    const Eigen::Map<const Eigen::Matrix<Scalar,6,1>> B(b,6,1);
    Eigen::Map<Eigen::Matrix<Scalar,6,1>> C(c,6,1);
    C.noalias() += A.transpose()*B;
  }

  template<class Scalar>
  static void applyTransAndSubToVector(Scalar a[36], Scalar b[6], Scalar c[6]) {
    const Eigen::Map<const Eigen::Matrix<Scalar,6,6,Eigen::RowMajor>> A(a,6,6);
    const Eigen::Map<const Eigen::Matrix<Scalar,6,1>> B(b,6,1);
    Eigen::Map<Eigen::Matrix<Scalar,6,1>> C(c,6,1);
    C.noalias() -= A.transpose()*B;
  }

  template<class Scalar>
  static void transpose(Scalar a[36], Scalar b[36]) {
    const Eigen::Map<const Eigen::Matrix<Scalar,6,6,Eigen::RowMajor>> A(a,6,6);
    Eigen::Map<Eigen::Matrix<Scalar,6,6,Eigen::RowMajor>> B(b,6,6);
    B.noalias() = A.transpose();
  }
#endif

  template<class Scalar, class Scalar2>
  static void applyToDenseMatrix(Scalar a[36], Scalar b[36], Scalar2 c[36]) {
    DENSEMATRIXTIMESDENSEMATRIX6( =, a, b, c);
  }

  template<class Scalar, class Scalar2>
  static void applyAndAddToDenseMatrix(Scalar a[36], Scalar b[36], Scalar2 c[36]) {
    DENSEMATRIXTIMESDENSEMATRIX6( +=, a, b, c);
  }

  template<class Scalar, class Scalar2>
  static void applyAndAddTransToDenseMatrix(Scalar a[36], Scalar b[36], Scalar2 c[36]) {
    DENSEMATRIXTIMESDENSEMATRIX6TRANS( +=, a, b, c);
  }

  template<class Scalar, class Scalar2>
  static void applyAndSubToDenseMatrix(Scalar a[36], Scalar b[36], Scalar2 c[36]) {
    DENSEMATRIXTIMESDENSEMATRIX6( -=, a, b, c);
  }

  template<class Scalar, class Scalar2>
  static void applyTransToDenseMatrix(Scalar a[36], Scalar b[36], Scalar2 c[36]) {
    DENSEMATRIXTRANSTIMESDENSEMATRIX6( =, a, b, c);
  }

  template<class Scalar, class Scalar2>
  static void applyTransAndAddToDenseMatrix(Scalar a[36], Scalar b[36], Scalar2 c[36]) {
    DENSEMATRIXTRANSTIMESDENSEMATRIX6( +=, a, b, c);
  }

  template<class Scalar, class Scalar2>
  static void applyTransAndSubToDenseMatrix(Scalar a[36], Scalar b[36], Scalar2 c[36]) {
    DENSEMATRIXTRANSTIMESDENSEMATRIX6( -=, a, b, c);
  }

  template<class Scalar, class Scalar2>
  static void applyToVector(Scalar a[36], Scalar2 b[6], Scalar2 c[6]) {
    DENSEMATRIXTIMESVECTOR6( =, a, b, c);
  }

  template<class Scalar, class Scalar2>
  static void applyAndAddToVector(Scalar a[36], Scalar2 b[6], Scalar2 c[6]) {
    DENSEMATRIXTIMESVECTOR6( +=, a, b, c);
  }

  template<class Scalar, class Scalar2>
  static void applyAndSubToVector(Scalar a[36], Scalar2 b[6], Scalar2 c[6]) {
    DENSEMATRIXTIMESVECTOR6( -=, a, b, c);
  }

  template<class Scalar, class Scalar2>
  static void applyTransToVector(Scalar a[36], Scalar2 b[6], Scalar2 c[6]) {
    DENSEMATRIXTRANSTIMESVECTOR6( =, a, b, c);
  }

  template<class Scalar, class Scalar2>
  static void applyTransAndAddToVector(Scalar a[36], Scalar2 b[6], Scalar2 c[6]) {
    DENSEMATRIXTRANSTIMESVECTOR6( +=, a, b, c);
  }

  template<class Scalar, class Scalar2>
  static void applyTransAndSubToVector(Scalar a[36], Scalar2 b[6], Scalar2 c[6]) {
    DENSEMATRIXTRANSTIMESVECTOR6( -=, a, b, c);
  }

  template<class Scalar, class Scalar2>
  static void transpose(Scalar a[36], Scalar2 b[36]) {
    b[0] = a[0];
    b[1] = a[6];
    b[2] = a[12];
    b[3] = a[18];
    b[4] = a[24];
    b[5] = a[30];
    b[6] = a[1];
    b[7] = a[7];
    b[8] = a[13];
    b[9] = a[19];
    b[10] = a[25];
    b[11] = a[31];
    b[12] = a[2];
    b[13] = a[8];
    b[14] = a[14];
    b[15] = a[20];
    b[16] = a[26];
    b[17] = a[32];
    b[18] = a[3];
    b[19] = a[9];
    b[20] = a[15];
    b[21] = a[21];
    b[22] = a[27];
    b[23] = a[33];
    b[24] = a[4];
    b[25] = a[10];
    b[26] = a[16];
    b[27] = a[22];
    b[28] = a[28];
    b[29] = a[34];
    b[30] = a[5];
    b[31] = a[11];
    b[32] = a[17];
    b[33] = a[23];
    b[34] = a[29];
    b[35] = a[35];
  }

};

//------------------------------------------------------------------------------

template<>
class DenseMatrixOp<7, 49> {

 public:

#ifdef USE_EIGEN3_DenseMatrixOp_NoMixedPrecision
  template<class Scalar>
  static void applyToDenseMatrix(Scalar a[49], Scalar b[49], Scalar c[49]) {
    const Eigen::Map<const Eigen::Matrix<Scalar,7,7,Eigen::RowMajor>> A(a,7,7);
    const Eigen::Map<const Eigen::Matrix<Scalar,7,7,Eigen::RowMajor>> B(b,7,7);
    Eigen::Map<Eigen::Matrix<Scalar,7,7,Eigen::RowMajor>> C(c,7,7);
    C.noalias() = A*B;
  }

  template<class Scalar>
  static void applyAndAddToDenseMatrix(Scalar a[49], Scalar b[49], Scalar c[49]) {
    const Eigen::Map<const Eigen::Matrix<Scalar,7,7,Eigen::RowMajor>> A(a,7,7);
    const Eigen::Map<const Eigen::Matrix<Scalar,7,7,Eigen::RowMajor>> B(b,7,7);
    Eigen::Map<Eigen::Matrix<Scalar,7,7,Eigen::RowMajor>> C(c,7,7);
    C.noalias() += A*B;
  }

  template<class Scalar>
  static void applyAndAddTransToDenseMatrix(Scalar a[49], Scalar b[49], Scalar c[49]) {
    const Eigen::Map<const Eigen::Matrix<Scalar,7,7,Eigen::RowMajor>> A(a,7,7);
    const Eigen::Map<const Eigen::Matrix<Scalar,7,7,Eigen::RowMajor>> B(b,7,7);
    Eigen::Map<Eigen::Matrix<Scalar,7,7,Eigen::RowMajor>> C(c,7,7);
    C.noalias() += (A*B).transpose();
  }

  template<class Scalar>
  static void applyAndSubToDenseMatrix(Scalar a[49], Scalar b[49], Scalar c[49]) {
    const Eigen::Map<const Eigen::Matrix<Scalar,7,7,Eigen::RowMajor>> A(a,7,7);
    const Eigen::Map<const Eigen::Matrix<Scalar,7,7,Eigen::RowMajor>> B(b,7,7);
    Eigen::Map<Eigen::Matrix<Scalar,7,7,Eigen::RowMajor>> C(c,7,7);
    C.noalias() -= A*B;
  }

  template<class Scalar>
  static void applyTransToDenseMatrix(Scalar a[49], Scalar b[49], Scalar c[49]) {
    const Eigen::Map<const Eigen::Matrix<Scalar,7,7,Eigen::RowMajor>> A(a,7,7);
    const Eigen::Map<const Eigen::Matrix<Scalar,7,7,Eigen::RowMajor>> B(b,7,7);
    Eigen::Map<Eigen::Matrix<Scalar,7,7,Eigen::RowMajor>> C(c,7,7);
    C.noalias() = A.transpose()*B;
  }

  template<class Scalar>
  static void applyTransAndAddToDenseMatrix(Scalar a[49], Scalar b[49], Scalar c[49]) {
    const Eigen::Map<const Eigen::Matrix<Scalar,7,7,Eigen::RowMajor>> A(a,7,7);
    const Eigen::Map<const Eigen::Matrix<Scalar,7,7,Eigen::RowMajor>> B(b,7,7);
    Eigen::Map<Eigen::Matrix<Scalar,7,7,Eigen::RowMajor>> C(c,7,7);
    C.noalias() += A.transpose()*B;
  }

  template<class Scalar>
  static void applyTransAndSubToDenseMatrix(Scalar a[49], Scalar b[49], Scalar c[49]) {
    const Eigen::Map<const Eigen::Matrix<Scalar,7,7,Eigen::RowMajor>> A(a,7,7);
    const Eigen::Map<const Eigen::Matrix<Scalar,7,7,Eigen::RowMajor>> B(b,7,7);
    Eigen::Map<Eigen::Matrix<Scalar,7,7,Eigen::RowMajor>> C(c,7,7);
    C.noalias() -= A.transpose()*B;
  }

  template<class Scalar>
  static void applyToVector(Scalar a[49], Scalar b[7], Scalar c[7]) {
    const Eigen::Map<const Eigen::Matrix<Scalar,7,7,Eigen::RowMajor>> A(a,7,7);
    const Eigen::Map<const Eigen::Matrix<Scalar,7,1>> B(b,7,1);
    Eigen::Map<Eigen::Matrix<Scalar,7,1>> C(c,7,1);
    C.noalias() = A*B;
  }

  template<class Scalar>
  static void applyAndAddToVector(Scalar a[49], Scalar b[7], Scalar c[7]) {
    const Eigen::Map<const Eigen::Matrix<Scalar,7,7,Eigen::RowMajor>> A(a,7,7);
    const Eigen::Map<const Eigen::Matrix<Scalar,7,1>> B(b,7,1);
    Eigen::Map<Eigen::Matrix<Scalar,7,1>> C(c,7,1);
    C.noalias() += A*B;
  }

  template<class Scalar>
  static void applyAndSubToVector(Scalar a[49], Scalar b[7], Scalar c[7]) {
    const Eigen::Map<const Eigen::Matrix<Scalar,7,7,Eigen::RowMajor>> A(a,7,7);
    const Eigen::Map<const Eigen::Matrix<Scalar,7,1>> B(b,7,1);
    Eigen::Map<Eigen::Matrix<Scalar,7,1>> C(c,7,1);
    C.noalias() -= A*B;
  }

  template<class Scalar>
  static void applyTransToVector(Scalar a[49], Scalar b[7], Scalar c[7]) {
    const Eigen::Map<const Eigen::Matrix<Scalar,7,7,Eigen::RowMajor>> A(a,7,7);
    const Eigen::Map<const Eigen::Matrix<Scalar,7,1>> B(b,7,1);
    Eigen::Map<Eigen::Matrix<Scalar,7,1>> C(c,7,1);
    C.noalias() = A.transpose()*B;
  }

  template<class Scalar>
  static void applyTransAndAddToVector(Scalar a[49], Scalar b[7], Scalar c[7]) {
    const Eigen::Map<const Eigen::Matrix<Scalar,7,7,Eigen::RowMajor>> A(a,7,7);
    const Eigen::Map<const Eigen::Matrix<Scalar,7,1>> B(b,7,1);
    Eigen::Map<Eigen::Matrix<Scalar,7,1>> C(c,7,1);
    C.noalias() += A.transpose()*B;
  }

  template<class Scalar>
  static void applyTransAndSubToVector(Scalar a[49], Scalar b[7], Scalar c[7]) {
    const Eigen::Map<const Eigen::Matrix<Scalar,7,7,Eigen::RowMajor>> A(a,7,7);
    const Eigen::Map<const Eigen::Matrix<Scalar,7,1>> B(b,7,1);
    Eigen::Map<Eigen::Matrix<Scalar,7,1>> C(c,7,1);
    C.noalias() -= A.transpose()*B;
  }

  template<class Scalar>
  static void transpose(Scalar a[49], Scalar b[49]) {
    const Eigen::Map<const Eigen::Matrix<Scalar,7,7,Eigen::RowMajor>> A(a,7,7);
    Eigen::Map<Eigen::Matrix<Scalar,7,7,Eigen::RowMajor>> B(b,7,7);
    B.noalias() = A.transpose();
  }
#endif

  template<class Scalar, class Scalar2>
  static void applyToDenseMatrix(Scalar a[49], Scalar b[49], Scalar2 c[49]) {
    DENSEMATRIXTIMESDENSEMATRIX7( =, a, b, c);
  }

  template<class Scalar, class Scalar2>
  static void applyAndAddToDenseMatrix(Scalar a[49], Scalar b[49], Scalar2 c[49]) {
    DENSEMATRIXTIMESDENSEMATRIX7( +=, a, b, c);
  }

  template<class Scalar, class Scalar2>
  static void applyAndAddTransToDenseMatrix(Scalar a[49], Scalar b[49], Scalar2 c[49]) {
    DENSEMATRIXTIMESDENSEMATRIX7TRANS( +=, a, b, c);
  }

  template<class Scalar, class Scalar2>
  static void applyAndSubToDenseMatrix(Scalar a[49], Scalar b[49], Scalar2 c[49]) {
    DENSEMATRIXTIMESDENSEMATRIX7( -=, a, b, c);
  }

  template<class Scalar, class Scalar2>
  static void applyTransToDenseMatrix(Scalar a[49], Scalar b[49], Scalar2 c[49]) {
    DENSEMATRIXTRANSTIMESDENSEMATRIX7( =, a, b, c);
  }

  template<class Scalar, class Scalar2>
  static void applyTransAndAddToDenseMatrix(Scalar a[49], Scalar b[49], Scalar2 c[49]) {
    DENSEMATRIXTRANSTIMESDENSEMATRIX7( +=, a, b, c);
  }

  template<class Scalar, class Scalar2>
  static void applyTransAndSubToDenseMatrix(Scalar a[49], Scalar b[49], Scalar2 c[49]) {
    DENSEMATRIXTRANSTIMESDENSEMATRIX7( -=, a, b, c);
  }

  template<class Scalar, class Scalar2>
  static void applyToVector(Scalar a[49], Scalar2 b[7], Scalar2 c[7]) {
    DENSEMATRIXTIMESVECTOR7( =, a, b, c);
  }

  template<class Scalar, class Scalar2>
  static void applyAndAddToVector(Scalar a[49], Scalar2 b[7], Scalar2 c[7]) {
    DENSEMATRIXTIMESVECTOR7( +=, a, b, c);
  }

  template<class Scalar, class Scalar2>
  static void applyAndSubToVector(Scalar a[49], Scalar2 b[7], Scalar2 c[7]) {
    DENSEMATRIXTIMESVECTOR7( -=, a, b, c);
  }

  template<class Scalar, class Scalar2>
  static void applyTransToVector(Scalar a[49], Scalar2 b[7], Scalar2 c[7]) {
    DENSEMATRIXTRANSTIMESVECTOR7( =, a, b, c);
  }

  template<class Scalar, class Scalar2>
  static void applyTransAndAddToVector(Scalar a[49], Scalar2 b[7], Scalar2 c[7]) {
    DENSEMATRIXTRANSTIMESVECTOR7( +=, a, b, c);
  }

  template<class Scalar, class Scalar2>
  static void applyTransAndSubToVector(Scalar a[49], Scalar2 b[7], Scalar2 c[7]) {
    DENSEMATRIXTRANSTIMESVECTOR7( -=, a, b, c);
  }

  template<class Scalar, class Scalar2>
  static void transpose(Scalar a[49], Scalar2 b[49]) {
    b[0] = a[0];
    b[1] = a[7];
    b[2] = a[14];
    b[3] = a[21];
    b[4] = a[28];
    b[5] = a[35];
    b[6] = a[42];
    b[7] = a[1];
    b[8] = a[8];
    b[9] = a[15];
    b[10] = a[22];
    b[11] = a[29];
    b[12] = a[36];
    b[13] = a[43];
    b[14] = a[2];
    b[15] = a[9];
    b[16] = a[16];
    b[17] = a[23];
    b[18] = a[30];
    b[19] = a[37];
    b[20] = a[44];
    b[21] = a[3];
    b[22] = a[10];
    b[23] = a[17];
    b[24] = a[24];
    b[25] = a[31];
    b[26] = a[38];
    b[27] = a[45];
    b[28] = a[4];
    b[29] = a[11];
    b[30] = a[18];
    b[31] = a[25];
    b[32] = a[32];
    b[33] = a[39];
    b[34] = a[46];
    b[35] = a[5];
    b[36] = a[12];
    b[37] = a[19];
    b[38] = a[26];
    b[39] = a[33];
    b[40] = a[40];
    b[41] = a[47];
    b[42] = a[6];
    b[43] = a[13];
    b[44] = a[20];
    b[45] = a[27];
    b[46] = a[34];
    b[47] = a[41];
    b[48] = a[48];
  }

};
#endif

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

#ifndef USE_EIGEN3_VectorOp
template<>
class VectorOp<1> {

 public:

  template<class Scalar>
  static void sum(Scalar a[1], Scalar b[1], Scalar c[1]) {
    c[0] = a[0] + b[0];
  }

  template<class Scalar>
  static void diff(Scalar a[1], Scalar b[1], Scalar c[1]) {
    c[0] = a[0] - b[0];
  }

  template<class Scalar>
  static void add(Scalar b[1], Scalar c[1]) {
    c[0] += b[0];
  }

  template<class Scalar>
  static void sub(Scalar b[1], Scalar c[1]) {
    c[0] -= b[0];
  }

  template<class Scalar>
  static void mult(const Scalar& a1, const Scalar& a2, const Scalar& a3, Scalar b1[1],
                   Scalar b2[1], Scalar b3[1], Scalar c[1]) {
    c[0] = a1*b1[0] + a2*b2[0] + a3*b3[0];
  }

  template<class Scalar>
  static void mult(const Scalar& a1, const Scalar& a2, const Scalar& a3, std::complex<Scalar> b1[1],
                   std::complex<Scalar> b2[1], std::complex<Scalar> b3[1], std::complex<Scalar> c[1]) {
    c[0] = a1*b1[0] + a2*b2[0] + a3*b3[0];
  }

};

//------------------------------------------------------------------------------

template<>
class VectorOp<2> {

 public:

  template<class Scalar>
  static void sum(Scalar a[2], Scalar b[2], Scalar c[2]) {
    c[0] = a[0] + b[0];
    c[1] = a[1] + b[1];
  }

  template<class Scalar>
  static void diff(Scalar a[2], Scalar b[2], Scalar c[2]) {
    c[0] = a[0] - b[0];
    c[1] = a[1] - b[1];
  }

  template<class Scalar>
  static void add(Scalar b[2], Scalar c[2]) {
    c[0] += b[0];
    c[1] += b[1];
  }

  template<class Scalar>
  static void sub(Scalar b[2], Scalar c[2]) {
    c[0] -= b[0];
    c[1] -= b[1];
  }

  template<class Scalar>
  static void mult(const Scalar& a1, const Scalar& a2, const Scalar& a3, Scalar b1[2],
                   Scalar b2[2], Scalar b3[2], Scalar c[2]) {
    c[0] = a1*b1[0] + a2*b2[0] + a3*b3[0];
    c[1] = a1*b1[1] + a2*b2[1] + a3*b3[1];
  }

  template<class Scalar>
  static void mult(const Scalar& a1, const Scalar& a2, const Scalar& a3, std::complex<Scalar> b1[2],
                   std::complex<Scalar> b2[2], std::complex<Scalar> b3[2], std::complex<Scalar> c[2]) {
    c[0] = a1*b1[0] + a2*b2[0] + a3*b3[0];
    c[1] = a1*b1[1] + a2*b2[1] + a3*b3[1];
  }

};

//------------------------------------------------------------------------------

template<>
class VectorOp<3> {

 public:

  template<class Scalar>
  static void sum(Scalar a[3], Scalar b[3], Scalar c[3]) {
    c[0] = a[0] + b[0];
    c[1] = a[1] + b[1];
    c[2] = a[2] + b[2];
  }

  template<class Scalar>
  static void diff(Scalar a[3], Scalar b[3], Scalar c[3]) {
    c[0] = a[0] - b[0];
    c[1] = a[1] - b[1];
    c[2] = a[2] - b[2];
  }

  template<class Scalar>
  static void add(Scalar b[3], Scalar c[3]) {
    c[0] += b[0];
    c[1] += b[1];
    c[2] += b[2];
  }

  template<class Scalar>
  static void sub(Scalar b[3], Scalar c[3]) {
    c[0] -= b[0];
    c[1] -= b[1];
    c[2] -= b[2];
  }

  template<class Scalar>
  static void mult(const Scalar& a1, const Scalar& a2, const Scalar& a3, Scalar b1[3],
                   Scalar b2[3], Scalar b3[3], Scalar c[3]) {
    c[0] = a1*b1[0] + a2*b2[0] + a3*b3[0];
    c[1] = a1*b1[1] + a2*b2[1] + a3*b3[1];
    c[2] = a1*b1[2] + a2*b2[2] + a3*b3[2];
  }

  template<class Scalar>
  static void mult(const Scalar& a1, const Scalar& a2, const Scalar& a3, std::complex<Scalar> b1[3],
                   std::complex<Scalar> b2[3], std::complex<Scalar> b3[3], std::complex<Scalar> c[3]) {
    c[0] = a1*b1[0] + a2*b2[0] + a3*b3[0];
    c[1] = a1*b1[1] + a2*b2[1] + a3*b3[1];
    c[2] = a1*b1[2] + a2*b2[2] + a3*b3[2];
  }

};

//------------------------------------------------------------------------------

template<>
class VectorOp<4> {

 public:

  template<class Scalar>
  static void sum(Scalar a[4], Scalar b[4], Scalar c[4]) {
    c[0] = a[0] + b[0];
    c[1] = a[1] + b[1];
    c[2] = a[2] + b[2];
    c[3] = a[3] + b[3];
  }

  template<class Scalar>
  static void diff(Scalar a[4], Scalar b[4], Scalar c[4]) {
    c[0] = a[0] - b[0];
    c[1] = a[1] - b[1];
    c[2] = a[2] - b[2];
    c[3] = a[3] - b[3];
  }

  template<class Scalar>
  static void add(Scalar b[4], Scalar c[4]) {
    c[0] += b[0];
    c[1] += b[1];
    c[2] += b[2];
    c[3] += b[3];
  }

  template<class Scalar>
  static void sub(Scalar b[4], Scalar c[4]) {
    c[0] -= b[0];
    c[1] -= b[1];
    c[2] -= b[2];
    c[3] -= b[3];
  }

  template<class Scalar>
  static void mult(const Scalar& a1, const Scalar& a2, const Scalar& a3, Scalar b1[4],
                   Scalar b2[4], Scalar b3[4], Scalar c[4]) {
    c[0] = a1*b1[0] + a2*b2[0] + a3*b3[0];
    c[1] = a1*b1[1] + a2*b2[1] + a3*b3[1];
    c[2] = a1*b1[2] + a2*b2[2] + a3*b3[2];
    c[3] = a1*b1[3] + a2*b2[3] + a3*b3[3];
  }

  template<class Scalar>
  static void mult(const Scalar& a1, const Scalar& a2, const Scalar& a3, std::complex<Scalar> b1[4],
                   std::complex<Scalar> b2[4], std::complex<Scalar> b3[4], std::complex<Scalar> c[4]) {
    c[0] = a1*b1[0] + a2*b2[0] + a3*b3[0];
    c[1] = a1*b1[1] + a2*b2[1] + a3*b3[1];
    c[2] = a1*b1[2] + a2*b2[2] + a3*b3[2];
    c[3] = a1*b1[3] + a2*b2[3] + a3*b3[3];
  }

};

//------------------------------------------------------------------------------

template<>
class VectorOp<5> {

 public:

  template<class Scalar>
  static void sum(Scalar a[5], Scalar b[5], Scalar c[5]) {
    c[0] = a[0] + b[0];
    c[1] = a[1] + b[1];
    c[2] = a[2] + b[2];
    c[3] = a[3] + b[3];
    c[4] = a[4] + b[4];
  }

  template<class Scalar>
  static void diff(Scalar a[5], Scalar b[5], Scalar c[5]) {
    c[0] = a[0] - b[0];
    c[1] = a[1] - b[1];
    c[2] = a[2] - b[2];
    c[3] = a[3] - b[3];
    c[4] = a[4] - b[4];
  }

  template<class Scalar>
  static void add(Scalar b[5], Scalar c[5]) {
    c[0] += b[0];
    c[1] += b[1];
    c[2] += b[2];
    c[3] += b[3];
    c[4] += b[4];
  }

  template<class Scalar>
  static void sub(Scalar b[5], Scalar c[5]) {
    c[0] -= b[0];
    c[1] -= b[1];
    c[2] -= b[2];
    c[3] -= b[3];
    c[4] -= b[4];
  }

  template<class Scalar>
  static void mult(const Scalar& a1, const Scalar& a2, const Scalar& a3, Scalar b1[5],
                   Scalar b2[5], Scalar b3[5], Scalar c[5]) {
    c[0] = a1*b1[0] + a2*b2[0] + a3*b3[0];
    c[1] = a1*b1[1] + a2*b2[1] + a3*b3[1];
    c[2] = a1*b1[2] + a2*b2[2] + a3*b3[2];
    c[3] = a1*b1[3] + a2*b2[3] + a3*b3[3];
    c[4] = a1*b1[4] + a2*b2[4] + a3*b3[4];
  }

  template<class Scalar>
  static void mult(const Scalar& a1, const Scalar& a2, const Scalar& a3, std::complex<Scalar> b1[5],
                   std::complex<Scalar> b2[5], std::complex<Scalar> b3[5], std::complex<Scalar> c[5]) {
    c[0] = a1*b1[0] + a2*b2[0] + a3*b3[0];
    c[1] = a1*b1[1] + a2*b2[1] + a3*b3[1];
    c[2] = a1*b1[2] + a2*b2[2] + a3*b3[2];
    c[3] = a1*b1[3] + a2*b2[3] + a3*b3[3];
    c[4] = a1*b1[4] + a2*b2[4] + a3*b3[4];
  }

};

//------------------------------------------------------------------------------

template<>
class VectorOp<6> {

 public:

  template<class Scalar>
  static void sum(Scalar a[6], Scalar b[6], Scalar c[6]) {
    c[0] = a[0] + b[0];
    c[1] = a[1] + b[1];
    c[2] = a[2] + b[2];
    c[3] = a[3] + b[3];
    c[4] = a[4] + b[4];
    c[5] = a[5] + b[5];
  }

  template<class Scalar>
  static void diff(Scalar a[6], Scalar b[6], Scalar c[6]) {
    c[0] = a[0] - b[0];
    c[1] = a[1] - b[1];
    c[2] = a[2] - b[2];
    c[3] = a[3] - b[3];
    c[4] = a[4] - b[4];
    c[5] = a[5] - b[5];
  }

  template<class Scalar>
  static void add(Scalar b[6], Scalar c[6]) {
    c[0] += b[0];
    c[1] += b[1];
    c[2] += b[2];
    c[3] += b[3];
    c[4] += b[4];
    c[5] += b[5];
  }

  template<class Scalar>
  static void sub(Scalar b[6], Scalar c[6]) {
    c[0] -= b[0];
    c[1] -= b[1];
    c[2] -= b[2];
    c[3] -= b[3];
    c[4] -= b[4];
    c[5] -= b[5];
  }

  template<class Scalar>
  static void mult(const Scalar& a1, const Scalar& a2, const Scalar& a3, Scalar b1[6],
                   Scalar b2[6], Scalar b3[6], Scalar c[6]) {
    c[0] = a1*b1[0] + a2*b2[0] + a3*b3[0];
    c[1] = a1*b1[1] + a2*b2[1] + a3*b3[1];
    c[2] = a1*b1[2] + a2*b2[2] + a3*b3[2];
    c[3] = a1*b1[3] + a2*b2[3] + a3*b3[3];
    c[4] = a1*b1[4] + a2*b2[4] + a3*b3[4];
    c[5] = a1*b1[5] + a2*b2[5] + a3*b3[5];
  }

  template<class Scalar>
  static void mult(const Scalar& a1, const Scalar& a2, const Scalar& a3, std::complex<Scalar> b1[6],
                   std::complex<Scalar> b2[6], std::complex<Scalar> b3[6], std::complex<Scalar> c[6]) {
    c[0] = a1*b1[0] + a2*b2[0] + a3*b3[0];
    c[1] = a1*b1[1] + a2*b2[1] + a3*b3[1];
    c[2] = a1*b1[2] + a2*b2[2] + a3*b3[2];
    c[3] = a1*b1[3] + a2*b2[3] + a3*b3[3];
    c[4] = a1*b1[4] + a2*b2[4] + a3*b3[4];
    c[5] = a1*b1[5] + a2*b2[5] + a3*b3[5];
  }

};

//------------------------------------------------------------------------------

template<>
class VectorOp<7> {

 public:

  template<class Scalar>
  static void sum(Scalar a[7], Scalar b[7], Scalar c[7]) {
    c[0] = a[0] + b[0];
    c[1] = a[1] + b[1];
    c[2] = a[2] + b[2];
    c[3] = a[3] + b[3];
    c[4] = a[4] + b[4];
    c[5] = a[5] + b[5];
    c[6] = a[6] + b[6];
  }

  template<class Scalar>
  static void diff(Scalar a[7], Scalar b[7], Scalar c[7]) {
    c[0] = a[0] - b[0];
    c[1] = a[1] - b[1];
    c[2] = a[2] - b[2];
    c[3] = a[3] - b[3];
    c[4] = a[4] - b[4];
    c[5] = a[5] - b[5];
    c[6] = a[6] - b[6];
  }

  template<class Scalar>
  static void add(Scalar b[7], Scalar c[7]) {
    c[0] += b[0];
    c[1] += b[1];
    c[2] += b[2];
    c[3] += b[3];
    c[4] += b[4];
    c[5] += b[5];
    c[6] += b[6];
  }

  template<class Scalar>
  static void sub(Scalar b[7], Scalar c[7]) {
    c[0] -= b[0];
    c[1] -= b[1];
    c[2] -= b[2];
    c[3] -= b[3];
    c[4] -= b[4];
    c[5] -= b[5];
    c[6] -= b[6];
  }

  template<class Scalar>
  static void mult(const Scalar& a1, const Scalar& a2, const Scalar& a3, Scalar b1[7],
                   Scalar b2[7], Scalar b3[7], Scalar c[7]) {
    c[0] = a1*b1[0] + a2*b2[0] + a3*b3[0];
    c[1] = a1*b1[1] + a2*b2[1] + a3*b3[1];
    c[2] = a1*b1[2] + a2*b2[2] + a3*b3[2];
    c[3] = a1*b1[3] + a2*b2[3] + a3*b3[3];
    c[4] = a1*b1[4] + a2*b2[4] + a3*b3[4];
    c[5] = a1*b1[5] + a2*b2[5] + a3*b3[5];
    c[6] = a1*b1[6] + a2*b2[6] + a3*b3[6];
  }

  template<class Scalar>
  static void mult(const Scalar& a1, const Scalar& a2, const Scalar& a3, std::complex<Scalar> b1[7],
                   std::complex<Scalar> b2[7], std::complex<Scalar> b3[7], std::complex<Scalar> c[7]) {
    c[0] = a1*b1[0] + a2*b2[0] + a3*b3[0];
    c[1] = a1*b1[1] + a2*b2[1] + a3*b3[1];
    c[2] = a1*b1[2] + a2*b2[2] + a3*b3[2];
    c[3] = a1*b1[3] + a2*b2[3] + a3*b3[3];
    c[4] = a1*b1[4] + a2*b2[4] + a3*b3[4];
    c[5] = a1*b1[5] + a2*b2[5] + a3*b3[5];
    c[6] = a1*b1[6] + a2*b2[6] + a3*b3[6];
  }

};
#endif

//------------------------------------------------------------------------------
// note: the dim argument is provided for the case where Dim = Eigen::Dynamic and
// the non-Eigen methods are used (if USE_EIGEN3_DenseMatrixLu is not defined), in which
// case a partial class specialization is called which doesn't take advantage of template
// parameter based size specification
// note: lu and cholesky methods using Eigen do not affect original matrix A,
// while non-Eigen methods compute factors in place!
template<class Scalar, int Dim>
class DenseMatrixFactor {

  typedef Eigen::Matrix<Scalar,Dim,Dim,(Dim==1?Eigen::ColMajor:Eigen::RowMajor)> MatrixType;
  typedef Eigen::Matrix<Scalar,Dim,1> VectorType;

 public:
  static void ludec(Scalar *a, int *index) {
    const int& n = Dim;
    int i, imax, j, k;
    Scalar vv[n];
    Scalar big, temp;
    Scalar sum, dum;
    Scalar epsilon = 1.0e-20;
    for(i = 0; i < n; i++) {
      big = 0.0;
      for(j = 0; j < n; j++) {
        temp = std::abs(*(a + i * n + j));
        if(std::norm(temp) > std::norm(big)) {
          big = temp;
        }
      }
      if(big == 0.0) {
        fprintf(stderr, "*** Error: Singular Matrix in ludec\n");
        return;
      }
      vv[i] = 1.0 / big;
    }
    for(j = 0; j < n; j++) {
      for(i = 0; i < j; i++) {
        sum = (*(a + n * i + j));
        for(k = 0; k < i; k++) {
          sum -= *(a + n * i + k) * (*(a + k * n + j));
        }
        *(a + i * n + j) = sum;
      }
      big = 0.0;
      for(i = j; i < n; i++) {
        sum = (*(a + i * n + j));
        for(k = 0; k < j; k++) {
          sum -= *(a + i * n + k) * (*(a + k * n + j));
        }
        *(a + i * n + j) = sum;
        dum = vv[i] * std::abs(sum);
        if(std::norm(dum) >= std::norm(big)) {
          big = std::abs(dum);
          imax = i;
        }
      }
      if(j != imax) {
        for(k = 0; k < n; k++) {
          dum = (*(a + imax * n + k));
          *(a + imax * n + k) = (*(a + j * n + k));
          *(a + j * n + k) = dum;
        }
        vv[imax] = vv[j];
      }
      index[j] = imax;
      if(*(a + j * n + j) == 0.0) {
        *(a + j * n + j) = epsilon;
      }
      if(j != n) {
        dum = 1.0 / (*(a + j * n + j));
        for(i = j + 1; i < n; i++) {
          *(a + i * n + j) *= dum;
        }
      }
    }
  }

  static void ludfdbksb(Scalar *a, int *index, Scalar *b) {
    const int& n = Dim;
    int i, ip, j;
    int ii = -1;
    Scalar sum;
    for(i = 0; i < n; i++) {
      ip = index[i];
      sum = b[ip];
      b[ip] = b[i];
      if(ii >= 0)
        for(j = ii; j <= i - 1; j++) {
          sum -= (*(a + i * n + j)) * b[j];
        }
      else if(std::abs(sum) != 0.0) {
        ii = i;
      }
      b[i] = sum;
    }
    for(i = n - 1; i >= 0; i--) {
      sum = b[i];
      for(j = i + 1; j < n; j++) {
        sum -= (*(a + i * n + j)) * b[j];
      }
      b[i] = sum / (*(a + i * n + i));
    }
  }

  static void lltdec(Scalar *a) {
    // stores cholesky factor in lower triangular part of row-major a
    // could be optimized for row-major storage of a
    const int& n = Dim;
    int i, j, k;
    for(i = 0; i < n; ++i) {
      //if(*(a + i * n + i) <= 0.0) {
      //  fprintf(stderr, "*** Error: Cholesky decomposition has negative elements on the diagonal\n");
      //  return;
      //}
      Scalar invD = 1.0 / (*(a + i * n + i));
      for(j = i + 1; j < n; ++j) {
        Scalar c = (*(a + j * n + i)) * invD;
        for(k = j; k < n; ++k) {
          *(a + k * n + j) -= (*(a + k * n + i)) * c;
        }
      }
      for(j = i; j < n; ++j) {
        *(a + j * n + i) *= sqrt(invD);
      }
    }
  }

  static void lltdfdbksb(Scalar *a, Scalar *b) {
    // could be optimized for row-major storage of a
    const int& n = Dim;
    int i, j;
    // forward elimination
    for(i = 0; i < n; ++i) {
      b[i] = b[i] / (*(a + i * n + i));
      for(int j = i + 1; j < n; ++j) {
        b[j] -= (*(a + j * n + i)) * b[i];
      }
    }
    // backward substitution
    for(i = n; i--; ) {
      for(int j = i + 1; j < n; ++j) {
        b[i] -= (*(a + j * n + i)) * b[j];
      }
      b[i] /= *(a + i * n + i);
    }
  }

  static int dgefa(Scalar *a, int *ipvt) {
    // gaussian elimination with partial pivoting
    const int& n = Dim;
    const int nm1 = n - 1;
    int i__2, i__3, kp1, j, k, l, ll, kn, knp1, jn1;
    Scalar t, *ax, *ay, *aa;
    Scalar tmp, max;
    // parameter adjustments
    --ipvt;
    a -= n + 1;
    // function Body
    for(k = 1; k <= nm1; ++k) {
      kp1 = k + 1;
      kn = k * n;
      knp1 = k * n + k;
      // find l = pivot index
      i__2 = n - k + 1;
      aa = &a[knp1];
      max = std::abs(aa[0]);
      l = 1;
      for(ll = 1; ll < i__2; ll++) {
        tmp = std::abs(aa[ll]);
        if(std::norm(tmp) > std::norm(max)) {
          max = tmp;
          l = ll + 1;
        }
      }
      l += k - 1;
      ipvt[k] = l;
      if(a[l + kn] == 0.) {
        return k;
      }
      // interchange if necessary
      if(l != k) {
        t = a[l + kn];
        a[l + kn] = a[knp1];
        a[knp1] = t;
      }
      // compute multipliers
      t = -1. / a[knp1];
      i__2 = n - k;
      aa = &a[1 + knp1];
      for(ll = 0; ll < i__2; ll++) {
        aa[ll] *= t;
      }
      // row elimination with column indexing
      ax = aa;
      for(j = kp1; j <= n; ++j) {
        jn1 = j * n;
        t = a[l + jn1];
        if(l != k) {
          a[l + jn1] = a[k + jn1];
          a[k + jn1] = t;
        }
        i__3 = n - k;
        ay = &a[1 + k + jn1];
        for(ll = 0; ll < i__3; ll++) {
          ay[ll] += t * ax[ll];
        }
      }
    }
    ipvt[n] = n;
    if(a[n + n * n] == 0.) {
      return n;
    }
    return 0;
  }

  static int dgedi(Scalar *a, int *ipvt, Scalar *work) {
    const int& n = Dim;
    int i__2, kb, kp1, nm1, i, j, k, l, ll, kn, knp1, jn1;
    Scalar *aa, *ax, *ay, tmp;
    Scalar t;
    // parameter adjustments
    --work;
    --ipvt;
    a -= n + 1;
    // compute inverse(u)
    for(k = 1; k <= n; ++k) {
      kn = k * n;
      knp1 = kn + k;
      a[knp1] = 1.0 / a[knp1];
      t = -a[knp1];
      i__2 = k - 1;
      aa = &a[1 + kn];
      for(ll = 0; ll < i__2; ll++) {
        aa[ll] *= t;
      }
      kp1 = k + 1;
      if(n < kp1) {
        continue;
      }
      ax = aa;
      for(j = kp1; j <= n; ++j) {
        jn1 = j * n;
        t = a[k + jn1];
        a[k + jn1] = 0.;
        ay = &a[1 + jn1];
        for(ll = 0; ll < k; ll++) {
          ay[ll] += t * ax[ll];
        }
      }
    }
    // form inverse(u)*inverse(l)
    nm1 = n - 1;
    if(nm1 < 1) {
      return 0;
    }
    for(kb = 1; kb <= nm1; ++kb) {
      k = n - kb;
      kn = k * n;
      kp1 = k + 1;
      aa = a + kn;
      for(i = kp1; i <= n; ++i) {
        work[i] = aa[i];
        aa[i] = 0.;
      }
      for(j = kp1; j <= n; ++j) {
        t = work[j];
        ax = &a[j * n + 1];
        ay = &a[kn + 1];
        for(ll = 0; ll < n; ll++) {
          ay[ll] += t * ax[ll];
        }
      }
      l = ipvt[k];
      if(l != k) {
        ax = &a[kn + 1];
        ay = &a[l * n + 1];
        for(ll = 0; ll < n; ll++) {
          tmp = ax[ll];
          ax[ll] = ay[ll];
          ay[ll] = tmp;
        }
      }
    }
    return 0;
  }

#ifndef USE_EIGEN3_DenseMatrixLu
  static void lu(Scalar *a, Scalar *b, const int = Dim) {
    int index[Dim];
    ludec(a, index);
    ludfdbksb(a, index, b);
  }

  static void cholesky(Scalar *a, Scalar *b, const int = Dim) {
    lltdec(a);
    lltdfdbksb(a, b);
  }

  static int invert(Scalar *a, const int = Dim) {
    int ipvt[Dim];
    Scalar work[Dim];
    int ierr = dgefa(a, ipvt);
    if(ierr) {
      return ierr;
    }
    ierr = dgedi(a, ipvt, work);
    return ierr;
  }
#else
  static void lu(Scalar *a, Scalar *b, const int = Dim) {
    Eigen::Map<MatrixType> A(a);
    Eigen::Map<VectorType> B(b);
    B = A.partialPivLu().solve(B).eval();
  }

  static void cholesky(Scalar *a, Scalar *b, const int = Dim) {
    Eigen::Map<MatrixType> A(a);
    Eigen::Map<VectorType> B(b);
    B = A.llt().solve(B).eval();
  }

  static int invert(Scalar *a, const int = Dim) {
    Eigen::Map<MatrixType> A(a);
#ifndef NDEBUG
    Eigen::FullPivLU<MatrixType> lu(A);
    if(lu.isInvertible()) {
      A = lu.inverse().eval();
      return 0;
    }
    else {
      return 1;
    }
#else
    // note: partial piv lu assumes matrix is invertible without checking
    A = A.partialPivLu().inverse().eval();
    return 0;
#endif
  }
#endif

};

//------------------------------------------------------------------------------
// partial specialization for the case Dim==Eigen::Dynamic which always use Eigen
// methods (regardless of USE_EIGEN3_DenseMatrixLu)
template<class Scalar>
class DenseMatrixFactor<Scalar, Eigen::Dynamic> {

  typedef Eigen::Matrix<Scalar,Eigen::Dynamic,Eigen::Dynamic,Eigen::RowMajor> MatrixType;
  typedef Eigen::Matrix<Scalar,Eigen::Dynamic,1> VectorType;

 public:
  static void ludec(Scalar *, int *) {
    fprintf(stderr, "*** Error: use of DenseMatrixFactor::ludec not implemented for Dim = Eigen::Dynamic\n");
    exit(-1);
  }

  static void ludfdbksb(Scalar *, int *, Scalar *) {
    fprintf(stderr, "*** Error: use of DenseMatrixFactor::ludfdbksb not implemented for Dim = Eigen::Dynamic\n");
    exit(-1);
  }

  static void lltdec(Scalar *) {
    fprintf(stderr, "*** Error: use of DenseMatrixFactor::lltdec not implemented for Dim = Eigen::Dynamic\n");
    exit(-1);
  }

  static void lltdfdbksb(Scalar *, Scalar *) {
    fprintf(stderr, "*** Error: use of DenseMatrixFactor::lltdfdbksb not implemented for Dim = Eigen::Dynamic\n");
    exit(-1);
  }

  static int dgefa(Scalar *, int *) {
    fprintf(stderr, "*** Error: use of DenseMatrixFactor::dgefa not implemented for Dim = Eigen::Dynamic\n");
    exit(-1);
    return 1;
  }

  static int dgedi(Scalar *, int *, Scalar *) {
    fprintf(stderr, "*** Error: use of DenseMatrixFactor::dgedi not implemented for Dim = Eigen::Dynamic\n");
    exit(-1);
    return 1;
  }

  static void lu(Scalar *a, Scalar *b, const int dim) {
    Eigen::Map<MatrixType> A(a,dim,dim);
    Eigen::Map<VectorType> B(b,dim,1);
    B = A.partialPivLu().solve(B).eval();
  }

  static void cholesky(Scalar *a, Scalar *b, const int dim) {
    Eigen::Map<MatrixType> A(a,dim,dim);
    Eigen::Map<VectorType> B(b,dim,1);
    B = A.llt().solve(B).eval();
  }

  static int invert(Scalar *a, const int dim) {
    Eigen::Map<MatrixType> A(a,dim,dim);
#ifndef NDEBUG
    Eigen::FullPivLU<MatrixType> lu(A);
    if(lu.isInvertible()) {
      A = lu.inverse().eval();
      return 0;
    }
    else {
      return 1;
    }
#else
    // note: partial piv lu assumes matrix is invertible without checking
    A = A.partialPivLu().inverse().eval();
    return 0;
#endif
  }

};

//------------------------------------------------------------------------------

template<class Scalar, int dim>
inline
void denseMatrixTimesDenseMatrixInPlace(Scalar m1[dim][dim], Scalar m2[dim][dim]) {
#if defined(USE_EIGEN3_DenseMatrixOp) || defined(USE_EIGEN3_DenseMatrixOp_NoMixedPrecision)
  const Eigen::Map<const Eigen::Matrix<Scalar,dim,dim,(dim==1?Eigen::ColMajor:Eigen::RowMajor)>> M1(&m1[0][0]);
  Eigen::Map<Eigen::Matrix<Scalar,dim,dim,(dim==1?Eigen::ColMajor:Eigen::RowMajor)>> M2(&m2[0][0]);
  M2 = M1*M2;
#else
  int i, j;
  Scalar res[dim][dim];
  for(i = 0; i < dim; ++i) {
    for(j = 0; j < dim; ++j) {
      res[i][j] = 0.0;
      for(int k = 0; k < dim; ++k) {
        res[i][j] += m1[i][k] * m2[k][j];
      }
    }
  }
  for(i = 0; i < dim; ++i) {
    for(j = 0; j < dim; ++j) {
      m2[i][j] = res[i][j];
    }
  }
#endif
}

//------------------------------------------------------------------------------

#endif

#ifndef _DIST_TIME_STATE_H_
#define _DIST_TIME_STATE_H_

#include <DistVector.h>
#include <LowMachPrec.h>
#include <NodeData.h>
#include <TimeData.h>

struct DistInfo;
struct ErrorHandler;
struct FluidModelData;
struct InitialConditions;

class DistGeoState;
class Domain;
class FemEquationTerm;
class VarFcn;

template<int dim> class TimeState;
template<class MatrixType> class DistMat;
template<class Scalar, int Dim> class MvpMat;
template<int dim> class SpaceOperator;
template<int dim> class DistEmbeddedBcData;

//------------------------------------------------------------------------------

template<int dim>
class DistTimeState {

 private:
  VarFcn *varFcn;
  FemEquationTerm *fet;
  DistSVec<double, dim> *V;

  bool locAlloc;
  int numLocSub;
  int isPerturbed;
  int isFunction;

  double refTime;
  double viscousCst;
  double embeddedCfl;
  double embeddedCflRef;

  TimeLowMachPrec    tprec;
  SpatialLowMachPrec sprec; // only for computation of irey

  TimeData *data;
  Domain *domain;
  TimeState<dim> **subTimeState;
  ErrorHandler *errorHandler;

  DistVec<double> *dt;      // actual time stepping
  DistVec<double> *dtc;     // actual time stepping constraints, like the interface should not traverse more than one cell in a time-step
  DistVec<double> *idti;    // inverse inviscid time stepping
  DistVec<double> *idtv;    // inverse viscous  time stepping
  DistVec<double> *dtau;    // dual time stepping
  DistVec<double> *irey;
  DistSVec<double, dim> *Un;
  DistSVec<double, dim> *Unm1;
  DistSVec<double, dim> *Unm2;
  DistSVec<double, dim> *U0;
  double errorEstiNorm;
  double dtMin;

  double dt_coeff;
  int dt_coeff_count;
  double dt_glob_ref;

  double checkForRapidlyChangingPressure;
  double checkForRapidlyChangingDensity;
  bool checkForRapidlyChangingValues;

  DistVec<double> *dIrey;
  DistVec<double> *dIdti;
  DistVec<double> *dIdtv;

 public:
  bool allowcflstop;
  bool allowdtstop;

 private:
  void initialize(IoData&, SpaceOperator<dim> *, VarFcn *,
                  DistSVec<double, dim> *, DistInfo&);
  void computeInitialState(InitialConditions&, FluidModelData&, double[dim]);
  void updateDtCoeff();

 public:
  DistTimeState(IoData&, SpaceOperator<dim> *, VarFcn *, Domain *, DistSVec<double, dim> *);
  DistTimeState(const DistTimeState<dim>&, bool, IoData&);
  ~DistTimeState();

  void resize1(NodeData *nodeData);
  void resize2();

  void setup(const char *, DistSVec<double, dim>&,
             DistSVec<double, dim>&, DistSVec<double, 3>&, IoData&, DistVec<int> *);
  void setupUFluidIdInitialConditions(IoData&, DistVec<int>&);
  void setupUVolumesInitialConditions(IoData&);
  void setupUOneDimensionalSolution(IoData&);
  void setupUMultiFluidInitialConditions(IoData&);
  void setupUPerturbedInitialConditions(IoData&, DistSVec<double, dim>&);
  void setupUFunctionInitialConditions(IoData&, DistSVec<double, dim>&, DistSVec<double, 3>&);
  void createSubStates();
  void update(DistSVec<double, dim>&, DistEmbeddedBcData<dim> *);
  void writeToDisk(char *);
  void fsoInitialize();

  // computes time step sizes
  double computeTimeStep(double, double, double&, long int&, double&, DistGeoState&,
                         DistSVec<double, dim>&, DistEmbeddedBcData<dim> *, bool, double = 0.0);

  // computes time step with error estimation
  // see: "Adaptive Time Stepping for Non-Linear Hyperbolic Problems"
  double computeTimeStep(int, double&, long int&);

  // computes irey without calling computeTimeStep (necessary for low-mach preconditioner when 
  // betav is specified to ensure correct spatial residual norm is computed)
  void computeInverseReynolds(IoData&, double, double, DistGeoState&,
                         DistSVec<double, dim>&, DistEmbeddedBcData<dim> *, bool, double = 0.0);

  // calculates the norm of the error estimator
  void calculateErrorEstiNorm(DistSVec<double, dim>&, DistSVec<double, dim>&);

  // minimum time step size is set for error estimation
  void setDtMin(double _dtMin) {
    dtMin = _dtMin;
  }

  void setGlobalTimeStep(double _dt) {
    *dt = _dt;
  }

  void add_dAW_dt(DistGeoState&, DistVec<double>&, DistSVec<double, dim>&, DistSVec<double, dim>&, DistVec<int> *);

  void add_dAW_dtLS(DistGeoState&, DistVec<double>&, DistSVec<double, dimLS>&, DistSVec<double, dimLS>&, DistSVec<double, dimLS>&,
                    DistSVec<double, dimLS>&, DistSVec<double, dimLS>&);

  void add_dAW_dtau(DistVec<double>&, DistSVec<double, dim>&, DistSVec<double, dim>&, DistVec<int> *);

  template<class Scalar, int neq>
  void addToJacobian(DistVec<double>&, DistMat<MvpMat<Scalar, neq>>&, DistVec<int> *, Scalar = 0, double = 1);

  template<class Scalar>
  void addToJacobianLS(DistVec<double>&, DistMat<MvpMat<Scalar, dimLS>>&);

  template<int Dim>
  void multiplyByTimeStep(DistSVec<double, Dim>&);

  void multiplyByPreconditioner(DistSVec<double, dim>&, DistSVec<double, dim>&, DistVec<int> *);

  TimeState<dim>& operator()(int i) const {
    return *subTimeState[i];
  }
  TimeLowMachPrec& getTimeLowMachPrec() {
    return tprec;
  }
  SpatialLowMachPrec& getSpatialLowMachPrec() {
    return sprec;
  }
  DistVec<double> *getInvReynolds() {
    return irey;
  }
  DistVec<double> *getDerivativeOfInvReynolds(DistGeoState&, DistSVec<double, dim>&, DistSVec<double, dim>&,
                                              double, DistEmbeddedBcData<dim> *);
  DistVec<double>& getTimeStep() {
    return *dt;
  }
  DistSVec<double, dim>& getUn() const {
    return *Un;
  }
  DistSVec<double, dim>& getUnm1() const {
    return *Unm1;
  }
  DistSVec<double, dim>& getUnm2() const {
    return *Unm2;
  }
  DistSVec<double, dim>& getU0() const {
    return *U0;
  }
  TimeData& getData() {
    return *data;
  }
  bool existsNm1() const {
    return data->exist_nm1;
  }
  bool existsNm2() const {
    return data->exist_nm2;
  }
  bool exists0() const {
    return data->exist_0;
  }
  bool useNm1() const {
    return data->use_nm1;
  }
  bool useNm2() const {
    return data->use_nm2;
  }
  bool use0() const {
    return data->use_0;
  }

};

//------------------------------------------------------------------------------

#endif

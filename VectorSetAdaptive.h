#ifndef _VECTOR_SET_A_H_
#define _VECTOR_SET_A_H_

#include <iostream>
#include <vector>
#include <limits>
#include <assert.h>

#ifndef _VECSET_A_TMPL_
  #define _VECSET_A_TMPL_
  template<class VecType, class Scalar = double> class VecSetA;
#endif

//------------------------------------------------------------------------------

template<class VecType, class Scalar>
class VecSetA {

  int numVec;
  std::vector<std::pair<VecType *, typename VecType::InfoType>> vecSet;

 public:
  VecSetA(int);
  VecSetA(VecSetA<VecType>&);
  ~VecSetA() {
    for(int i = 0; i < numVec; ++i) {
      delete std::get<0>(vecSet[i]);
    }
  }

  VecType& operator[](int i) {
    return *(std::get<0>(vecSet[i]));
  }

  const VecType& operator[](int i) const {
    return *(std::get<0>(vecSet[i]));
  }

  int len(int i) const {
    return std::get<1>(vecSet[i]);
  }

  int numVectors() const {
    return numVec;
  }

  void print(const char * = "");
  void addVector(const typename VecType::InfoType&);
  void operator=(const Scalar&);
  void operator*=(const Scalar&);

  void computeSparsityMasks();

};

//------------------------------------------------------------------------------

template<class VecType, class Scalar>
VecSetA<VecType, Scalar>::VecSetA(int _numVec) {
  numVec = _numVec;
}

//------------------------------------------------------------------------------

template<class VecType, class Scalar>
VecSetA<VecType, Scalar>::VecSetA(VecSetA<VecType>& vectorSet) {
  numVec = vectorSet.numVectors();
  for(int i = 0; i < numVec; ++i) {
    VecType *tmp = new VecType((*std::get<0>(vectorSet[i]))); // calls VecType copy constructor
    vecSet.push_back(std::make_pair(tmp,std::get<1>(vectorSet[i])));
  }
}

//------------------------------------------------------------------------------

template<class VecType, class Scalar>
void VecSetA<VecType, Scalar>::print(const char *msg) {
  if(msg) {
    std::cout << msg << std::endl;
  }
  for(int i = 0; i < numVec; ++i) {
    std::cout << "vector " << i << ":";
    std::get<0>(vecSet[i])->print();
  }
}

//------------------------------------------------------------------------------

template<class VecType, class Scalar>
void VecSetA<VecType, Scalar>::addVector(const typename VecType::InfoType& _len) {
  VecType *tmp = new VecType(_len);
  vecSet.push_back(std::make_pair(tmp,_len));
  ++numVec;
}

//------------------------------------------------------------------------------

template<class VecType, class Scalar>
void VecSetA<VecType, Scalar>::operator=(const Scalar& x) {
  for(int i = 0; i < numVec; ++i) {
    (*std::get<0>(vecSet[i])) = x;
  }
}

//------------------------------------------------------------------------------
// assumes the VecType class has defined the *=(Scalar) operator (multiply by constant)
template<class VecType, class Scalar>
void VecSetA<VecType, Scalar>::operator*=(const Scalar& x) {
  for(int i = 0; i < numVec; ++i) {
    (*std::get<0>(vecSet[i])) *= x;
  }
}

//------------------------------------------------------------------------------

template<class VecType, class Scalar>
void VecSetA<VecType, Scalar>::computeSparsityMasks() {
  for(int iVec = 0; iVec < numVec; iVec++) {
    std::get<0>(vecSet[iVec])->computeSparsityMask();
  }
}

//------------------------------------------------------------------------------

#endif

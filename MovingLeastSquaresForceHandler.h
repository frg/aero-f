#ifndef _MLSFH_H_
#define _MLSFH_H_

#include <Vector3D.h>
#include <Vector.h>
#include <PostFcn.h>
#include <FSI/DynamicNodalTransfer.h>

#include <vector>
#include <array>

class Domain;
class SubDomain;
class Quadrature;
class IoData;
class PostFcn;
class VarFcn;
class DistGeoState;
class GeoState;
class Elem;

template<class T> class RTree;
template<class T> class CommPattern;
template<class Scalar, int dim> class DistSVec;
template<class Scalar> class DistVec;
template<int dim> class EmbeddedBcData;
template<int dim> class DistEmbeddedBcData;
template<class Scalar, int dim, int dim2> class RectangularSparseMat;
template<int dim> class MatVecProd_dRdX;
template<int dim> struct dRdXoperators;

enum class StructElemCloudDataType {PRESSURE, TEMPERATURE, VELOCITY};
enum statusType {MASTER = 2, SLAVE = 1, INACTIVE = 0};

//------------------------------------------------------------------------------

struct StructElemCloud {

  int NN = 0;
  int NN_included = 0;
  std::vector<int> closeNodeIds; // public so we can push back
  std::vector<std::array<double, 4>> A; // these are all sized as if g eodim = 3
  std::vector<double> Psi;
  std::vector<double> Psi2;
  double C[4][4];
  double D[4][4]; // D is the adjunct matrix
  double pp;
  double dh;
  Vec3D Vwall;
  Vec3D uoffset;
  double T = 0.0;
  double det = 1;
  Vec3D Xp;
  Vec3D normal;
  Vec3D offset;
  double typicalEdgeLength = -1;
  statusType status = statusType::INACTIVE;
  std::vector<statusType> neighborStatus;
  bool hasMasterNeighbor = false;
  int dimMLS = 3; // default
  Elem *elem; // pointer to the elem that contains this quadrature point

  StructElemCloud() {
    for(int i = 0; i < dimMLS+1; ++i) {
      for(int j = 0; j < dimMLS+1; ++j) {
        C[i][j] = 0.0;
      }
    }
  }

  void clearVectors() {
    closeNodeIds.clear();
    A.clear();
    Psi.clear();
    Psi2.clear();
    neighborStatus.clear();
  }

  void printDiagnostics() {
    fprintf(stderr, "det: %e \n", det);
    for(int i = 0; i < dimMLS + 1; i++) {
      fprintf(stderr, "C[%d]: %e %e %e \n", i, C[i][0], C[i][1], C[i][2]);
    }
  }

  void resizeNeighborStatus(int numneighb) {
    neighborStatus.resize(numneighb);
    std::fill(neighborStatus.begin(), neighborStatus.end(), statusType::INACTIVE);
  }

  void accumulateC() {
    for(int i = 0; i != dimMLS+1; i++) {
      for(int j = 0; j != dimMLS+1; j++) {
        C[i][j] = 0.0;
        for(int k = 0; k != NN; k++) {
          C[i][j] += A[k][i]*A[k][j];
        }
      }
    }
  }

  void postProcessC(int nSt, int nq, int globSub, bool printFlag = false) {
    if(dimMLS == 2) {
      det = C[0][0]*C[1][1]*C[2][2] - C[0][0]*C[1][2]*C[2][1]
          - C[0][1]*C[1][0]*C[2][2] + C[0][1]*C[1][2]*C[2][0]
          + C[0][2]*C[1][0]*C[2][1] - C[0][2]*C[1][1]*C[2][0];
      if(std::abs(det) > 10e10) {
        fprintf(stdout, "Determinant is big %e in Force computation for nSt: %d nq: %d least squares is huge, with NN: %d, \n", det, nSt, nq, NN);
        fprintf(stdout, "NN: %d, C: %e %e %e %e %e %e %e %e %e \n", NN, C[0][0], C[0][1], C[0][2], C[1][0], C[1][1], C[1][2], C[2][0], C[2][1], C[2][2]);
        if(NN == 0) fprintf(stdout, "No valid nodes were found (NN), check N layers\n");
        for(int i = 0; i != NN; i++) {
          for(int j = 0; j != 3; j++) {
            fprintf(stdout, "A[%d][%d] = %e \n", i, j, A[i][j]);
          }
        }
      }
      D[0][0] = C[1][1]*C[2][2] - C[1][2]*C[2][1];
      D[0][1] = -C[1][0]*C[2][2] + C[1][2]*C[2][0];
      D[0][2] = C[1][0]*C[2][1] - C[1][1]*C[2][0];
      // C and D should be symmetric -- check this
      D[1][0] = -C[0][1]*C[2][2] + C[0][2]*C[2][1];
      D[1][1] = C[0][0]*C[2][2] - C[0][2]*C[2][0];
      D[1][2] = -C[0][0]*C[2][1] + C[0][1]*C[2][0];
      //
      D[2][0] = C[0][1]*C[1][2] - C[0][2]*C[1][1];
      D[2][1] = -C[0][0]*C[1][2] + C[0][2]*C[1][0];
      D[2][2] = C[0][0]*C[1][1] - C[0][1]*C[1][0];
    }
    if(dimMLS == 3) {
      det = 0;
      for(int i = 0; i != dimMLS + 1; i++) {
        for(int j = 0; j != dimMLS + 1; j++) {
          D[i][j] = det3x3from4x4(i,j);
          if(i == 0) det += D[i][j]*C[i][j];
        }
      }
    }
    if(printFlag || std::abs(det) < 1e-50) {
      fprintf(stderr, "*** Warning: in MLS Force evaluation: embedded surface may be oriented incorrectly, globSub: %d,\n"
                      "           determinant is close to zero %e in Force computation for nSt: %d nq: %d X: %f %f %f\n"
                      "           least squares is zero, with NN: %d, includedNN: %d, typicalEdgeLength: %e normal: %e %e %e\n",
              globSub, det, nSt, nq, Xp[0], Xp[1], Xp[2], NN, NN_included, typicalEdgeLength, normal[0], normal[1], normal[2]);
    }
  }

  double det3x3from4x4(int i, int j) {
    int is[3] = {0, 1, 2};
    int js[3] = {0, 1, 2};
    for(int ii = 3; ii != i; ii -= 1) is[ii-1] += 1;
    for(int ii = 3; ii != j; ii -= 1) js[ii-1] += 1;
    return std::pow(-1.0, i + j)*(C[is[0]][js[0]]*C[is[1]][js[1]]*C[is[2]][js[2]] - C[is[0]][js[0]]*C[is[1]][js[2]]*C[is[2]][js[1]]
          - C[is[0]][js[1]]*C[is[1]][js[0]]*C[is[2]][js[2]] + C[is[0]][js[1]]*C[is[1]][js[2]]*C[is[2]][js[0]]
          + C[is[0]][js[2]]*C[is[1]][js[0]]*C[is[2]][js[1]] - C[is[0]][js[2]]*C[is[1]][js[1]]*C[is[2]][js[0]]);
  }

  void postProcessPsi() {
    Psi.resize(NN);
    for(int i = 0; i != NN; i++) {
      if(dimMLS == 2) Psi[i] = A[i][0]/det*(D[0][0]*A[i][0] + D[0][1]*A[i][1] + D[0][2]*A[i][2]);
      if(dimMLS == 3) Psi[i] = A[i][0]/det*(D[0][0]*A[i][0] + D[0][1]*A[i][1] + D[0][2]*A[i][2] + D[0][3]*A[i][3]);
    }
  }

  void postProcessPsi2() {
    Psi2.resize(NN);
    for(int i = 0; i != NN; i++) {
      if(dimMLS == 2) {
        Psi2[i] = 1/det*A[i][0]*((D[0][0]*A[i][0] + D[0][1]*A[i][1] + D[0][2]*A[i][2])
                  + offset[0]*(D[1][0]*A[i][0] + D[1][1]*A[i][1] + D[1][2]*A[i][2])
                  + offset[1]*(D[2][0]*A[i][0] + D[2][1]*A[i][1] + D[2][2]*A[i][2]));
      }
      if(dimMLS == 3) {
        Psi2[i] = 1/det*A[i][0]*((D[0][0]*A[i][0] + D[0][1]*A[i][1] + D[0][2]*A[i][2] + D[0][3]*A[i][3])
                  + offset[0]*(D[1][0]*A[i][0] + D[1][1]*A[i][1] + D[1][2]*A[i][2] + D[1][3]*A[i][3])
                  + offset[1]*(D[2][0]*A[i][0] + D[2][1]*A[i][1] + D[2][2]*A[i][2] + D[2][3]*A[i][3])
                  + offset[2]*(D[3][0]*A[i][0] + D[3][1]*A[i][1] + D[3][2]*A[i][2] + D[3][3]*A[i][3]));
      }
    }
  }

  void accumulatepps(double *b) {
    for(int i = 0; i != NN; i++) {
      pp += Psi[i]*b[i];
    }
  }

  void accumulateOffsetU(Vec3D *b) {
    for(int i = 0; i != NN; i++) {
      uoffset += Psi2[i]*b[i];
    }
  }

  void accumulateTs(double *b) {
    for(int i = 0; i != NN; i++) T += Psi2[i]*b[i];
  }

};

//------------------------------------------------------------------------------

class MovingLeastSquaresForceHandler {
  public:
  double supportRadiusFactor = 3.0;
  bool smalldet = false;
 protected:
  struct node_id {
    node_id(int _id) : id(_id) {}
    node_id() : id(-1) {}
    int id;
    void computeBoundingBox(SVec<double, 3>& X, double bb[6], double eps) {
      bb[0] = X[id][0] - eps;
      bb[1] = X[id][0] + eps;
      bb[2] = X[id][1] - eps;
      bb[3] = X[id][1] + eps;
      bb[4] = X[id][2] - eps;
      bb[5] = X[id][2] + eps;
    }
    bool overlapsBoundingBox(SVec<double, 3>& X, double bb[6]) {
      const double *loc = X[id];
      return (loc[0] > bb[0] && loc[0] < bb[1] &&
              loc[1] > bb[2] && loc[1] < bb[3] &&
              loc[2] > bb[4] && loc[2] < bb[5]);
    }
  };

  node_id *_node_ids;
  std::vector<node_id*> node_ids;
  SubDomain *subd;
  GeoState *geoState;
  bool hybridNodes; // flag for how the fluxes are being computed
  int nSlaves = 0;  // one for each iSub
  int nMasters = 0; // one for each iSub
  double loftingFactor = 2.0;
  int dimMLS = 3;
  bool viscous = false;
  Quadrature *quad = NULL;

  int numStructElems;
  int (*stElem)[3];

  double eps = 10.0 * std::numeric_limits<double>::epsilon(); // used by myTree to compute bounding boxes

  void constructRTree(SVec<double, 3>& X);

  RTree<node_id> *myTree = NULL;
  RTree<node_id> *getMyTree() {
    return myTree;
  }
  StructElemCloud **structElemCloudSet = NULL; // one for each iSub
  int *iBuffers = NULL;
  void zeroiBuffer();
  Vec<double> *averageEdgeLength;
  bool pressureDistortion = false;

 public:
  MovingLeastSquaresForceHandler(SubDomain *, IoData&, int, int (*)[3], Vec<double> *, GeoState *, bool, bool);
  MovingLeastSquaresForceHandler() {};
  ~MovingLeastSquaresForceHandler();
  void initializeAverageEdgeLength(Vec<int>&);

  template<int dim>
  void computeEmbSurfBasedForceLoadSmoothTypicalEdgeLength(Vec<Vec3D>&, EmbeddedBcData<dim>&, CommPattern<int>&,
                                                           CommPattern<double>&, bool = false);

  void sndMasterQuadraturePointsWithTypicalEdgeLength(CommPattern<int>&, CommPattern<double>&);
  void rcvMasterQuadraturePointsWithTypicalEdgeLength(CommPattern<int>&, CommPattern<double>&);

  template<int dim>
  void computeEmbSurfBasedForceLoadSmoothPreProcess(Vec<Vec3D>&, Vec3D *, EmbeddedBcData<dim>&, bool *,
                                                    CommPattern<int>&, CommPattern<double>&, CommPattern<double> *,
                                                    CommPattern<double> *, CommPattern<double> *, bool = false);

  void sndSlaveQuadraturePoints(CommPattern<int>&);
  void rcvSlaveQuadraturePoints(CommPattern<int>&, CommPattern<double>&);
  void sndStructElemCloudSetMatrixCData(CommPattern<double>&);
  void rcvStructElemCloudSetMatrixCData(CommPattern<double>&);
  void sndStructElemCloudSetMatrixDData(CommPattern<double>&);
  void rcvStructElemCloudSetMatrixDData(CommPattern<double>&);

  template<int dim>
  void computeForceLocal(EmbeddedBcData<dim>&, SVec<double, dim>&, VarFcn *, int, PostFcn::ScalarType* = 0);

  void sndStructElemCloudSetScalarData(CommPattern<double> *, StructElemCloudDataType, bool = true);
  void rcvStructElemCloudSetScalarData(CommPattern<double> *, StructElemCloudDataType, bool = true);
  void computeEmbSurfBasedForceLoadSmoothFinalize(double (*)[3], double, PostFcn *);

  template<int dim>
  void computeEmbSurfBasedForceLoadSmoothDerivative(SVec<double, dim>&, SVec<double, dim>&, PostFcn *, VarFcn *, double (*)[3], int);

  template<int dim>
  void computeEmbSurfBasedForceLoadSmoothDerivativeOperators(SVec<double, dim>&, dRdXoperators<dim>&, PostFcn *, VarFcn *, const Vec3D&,
                                                             Vec<Vec3D>&, int);

  void computeEmbSurfBasedNodalQuantitySmoothFinalize(PostFcn *, double **, PostFcn::ScalarType&, int, double);

  void computeEmbSurfBasedPressureDistortion(double&, double&, double&);

  template<int dim>
  void computeEmbSurfBasedPressureDistortionDerivative(SVec<double, dim>&, SVec<double, dim>&, VarFcn*, double&, const double&, const double&);

  template<int dim>
  void computeEmbSurfBasedPressureDistortionDerivativeOperators(SVec<double, dim>&, dRdXoperators<dim>&, const double&, const double&);

};

//------------------------------------------------------------------------------

class DistMovingLeastSquaresForceHandler {

 protected:
  Domain *dom;
  DistGeoState *geoState;
  CommPattern<double> *uPat;
  CommPattern<double> *ppPat;
  CommPattern<double> *TPat;
  CommPattern<int> *masterPat;
  CommPattern<double> *typicalEdgeLengthPat;
  CommPattern<double> *slaveMasterMatrixPat;
  CommPattern<double> *masterSlaveMatrixPat;
  bool viscous, initialized;
  MovingLeastSquaresForceHandler **mlsfh;
  DistVec<double> *averageEdgeLength;
  bool pressureDistortion = false;
  Vec<Vec3D> *Xs0; // for inlet flow actuator disk // original
  Vec<Vec3D> *Xs;  // for inlet flow actuator disk
  Vec<Vec3D> *dXs; // for inlet flow actuator disk // sensitivities
  char* inletdisksensitivities = nullptr;
  double pbar = -1.0; //average pressure
  double A; // total area used
  int numStructNodes;
  int numStructElems;
  int (*stElem)[3];
  void initializeAverageEdgeLength();

 public:
  template<int dim>
  DistMovingLeastSquaresForceHandler(IoData&, Domain *, DistEmbeddedBcData<dim>&, DistGeoState *, bool * = nullptr);
   ~DistMovingLeastSquaresForceHandler();

  template<int dim>
  void initialize(DistEmbeddedBcData<dim>&);

  template<int dim>
  void reinitializeWithRecycledStructElemClouds(DistEmbeddedBcData<dim>&);

  template<int dim>
  void updateForce(DistSVec<double, dim>&, DistEmbeddedBcData<dim>&, VarFcn *, PostFcn *, double, double (*)[3],
                   int, int, double * = nullptr);

  template<int dim>
  void updateNodalQuantity(DistSVec<double, dim>&, DistEmbeddedBcData<dim>&, VarFcn *, PostFcn *, double ***,
                           std::vector<PostFcn::ScalarType>&);

  template<int dim>
  void updateNodalQuantityOnEmbSurface(DistSVec<double, dim>&, DistEmbeddedBcData<dim>&, VarFcn *, PostFcn *,
                                       double ***, PostFcn::ScalarType&, int);

  template<int dim>
  void computeForce(DistSVec<double, dim>&, DistEmbeddedBcData<dim>&, VarFcn *, bool, PostFcn *, double, double (*)[3],
                    int, int, double * = nullptr);

  template<int dim>
  void computeDerivative(DistSVec<double, dim>&, DistSVec<double, dim> *, DistEmbeddedBcData<dim>&, PostFcn *,
                         VarFcn *, double, double (*)[3], int, int, double, double * = nullptr);

  template<int dim>
  void computeDerivativeStructure(DistSVec<double, dim>&, DistEmbeddedBcData<dim>&, PostFcn *, VarFcn *, double,
                                  double (*)[3], int, int, double, double * = nullptr);

  template<int dim>
  void computeDerivativeOperators(MatVecProd_dRdX<dim>&, DistSVec<double, dim>&, DistEmbeddedBcData<dim>&, PostFcn *,
                                  VarFcn *, const Vec3D&, int = 0);

  void updateSlaveStructElemCloudScalarData();

  bool readPressureDistortionDiskSensitivity(int step);

  void perturbInletDisk(double eps);

  int getNumStructNodes() { return numStructNodes; }

};

//------------------------------------------------------------------------------

#endif

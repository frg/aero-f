#ifndef _SLIP_FCN_H_
#define _SLIP_FCN_H_

#include <IoData.h>

//------------------------------------------------------------------------------
// Pure virtual base class
class SlipFcn {

 public:
  SlipFcn() {}
  virtual ~SlipFcn() {}

  virtual void compute(double *, double[3], double[3], double[3]) = 0;

};

//------------------------------------------------------------------------------

class MaxwellSlipFcn : public SlipFcn {
  double prandtl, gamma, gasC, sigma, zeroconst, reftemperature, refvelocity;

 public:
  MaxwellSlipFcn(IoData& iod) {
    prandtl = iod.eqs.thermalCondModel.prandtl;
    gamma = iod.eqs.fluidModel().gasModel.specificHeatRatio;
    gasC = iod.eqs.fluidModel().gasModel.idealGasConstant;
    sigma = iod.bc.wall.maxwellSlipData.sigma_acc;
    zeroconst = iod.bc.wall.maxwellSlipData.zeroconstant;
    reftemperature = iod.ref.rv.temperature;
    refvelocity = (iod.bc.wall.maxwellSlipData.refVelOn) ? iod.ref.rv.velocity : 1.0;
  }
  ~MaxwellSlipFcn() {}

  void compute(double *Vwall, double qs[3], double ts[3], double gk[3]) {
    double const1, const2;
    const1 = zeroconst * (2.0 - sigma) / sigma * 1.0 / (sqrt(2.0 * gasC * Vwall[4] * reftemperature / M_PI)) * refvelocity;
    const2 = zeroconst * 0.75 * prandtl * (gamma - 1) / (gamma * gasC * Vwall[4] * reftemperature) * refvelocity * refvelocity;

    for(int i = 0; i < 3; i++) {
      // the normal inside AERO-F points out of the fluid, while we need the normal pointing into the fluid
      gk[i] = (-const1 * ts[i] + const2 * qs[i]);
    }
  }

};

//------------------------------------------------------------------------------

#endif

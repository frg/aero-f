#ifndef _VECTOR_H_
#define _VECTOR_H_

#include <NodeData.h>
#include <NodalGrad.h>

#include <cassert>
#include <complex>
#include <iostream>
#include <utility>
#include <Eigen/Core>
#include <Eigen/Geometry>

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

template<class Scalar>
class Vec {
 public:
  typedef int InfoType;
  typedef typename Eigen::NumTraits<Scalar>::Real RealScalarType;

 protected:
  Eigen::Matrix<Scalar,Eigen::Dynamic,1> *locAlloc;
  Eigen::Ref<Eigen::Matrix<Scalar,Eigen::Dynamic,1>> v;

 public:
  Eigen::MatrixXd *IPM; // inner product matrix

  // constructors --------------------------------------------------------------
  Vec();
  Vec(int);
  Vec(int, Scalar *);
  Vec(const Vec<Scalar>&);
  template<typename Derived>
  Vec(const Eigen::MatrixBase<Derived>&);

  // destructor ----------------------------------------------------------------
  ~Vec();

  // assignment operators ------------------------------------------------------
  Vec<Scalar>& operator=(const Scalar&);
  Vec<Scalar>& operator=(const Vec<Scalar>&);
  template<typename Derived>
  Vec<Scalar>& operator=(const Eigen::MatrixBase<Derived>&);
  Vec<Scalar>& setZero();

  // arithmetic operators ------------------------------------------------------
  // note: all of these return expressions
  auto operator+=(const Vec<Scalar>& b) -> decltype(v+=b.v) {
    return v += b.v;
  }

  template<typename Derived>
  auto operator+=(const Eigen::MatrixBase<Derived>& b) -> decltype(v+=b) {
    return v += b;
  }

  auto operator+(const Vec<Scalar>& b) const -> decltype(v+b.v) {
    return v + b.v;
  }

  template<typename Derived>
  auto operator+(const Eigen::MatrixBase<Derived>& b) const -> decltype(v+b) {
    return v + b;
  }

  auto operator-=(const Vec<Scalar>& b) -> decltype(v-=b.v) {
    return v -= b.v;
  }

  template<typename Derived>
  auto operator-=(const Eigen::MatrixBase<Derived>& b) -> decltype(v-=b) {
    return v -= b;
  }

  auto operator-() const -> decltype(-v) {
    return -v;
  }

  auto operator-(const Vec<Scalar>& b) const -> decltype(v-b.v) {
    return v - b.v;
  }

  template<typename Derived>
  auto operator-(const Eigen::MatrixBase<Derived>& b) const -> decltype(v-b) {
    return v - b;
  }

  auto operator*=(const Scalar& s) -> decltype(v*=s) {
    return v *= s;
  }

  template<typename T>
  auto operator*(const T& s) const -> decltype(v*s) {
    return v * s;
  }

  Scalar operator*(const Vec<Scalar>&) const;

  auto operator/=(const Scalar& s) -> decltype(v/=s) {
    return v /= s;
  }

  template<typename T>
  auto operator/(const T& s) const -> decltype(v/s) {
    return v / s;
  }

  auto operator*=(const Vec<Scalar>& b) -> decltype(v.array()*=b.v.array()) {
    return v.array() *= b.v.array();
  }

  auto operator/=(const Vec<Scalar>& b) -> decltype(v.array()/=b.v.array()) {
    return v.array() /= b.v.array();
  }

  // other operators -----------------------------------------------------------
  Scalar& operator[](int i) {
    return v[i];
  }

  const Scalar& operator[](int i) const {
    return v[i];
  }

  // resizing methods ----------------------------------------------------------
  int capacity() const;
  void reserve(int);
  void push_back(const Scalar&);
  void shrink_to_fit();
  void resize(int);
  void resize(int, const Scalar&);
  void resize(int, NodeData&, int = 1, Scalar = 0);
  void resize(int, const Vec<Scalar>&, GenNodeData<4>&, int = 1, Scalar = 0);

  // data access methods -------------------------------------------------------
  const Eigen::Ref<Eigen::Matrix<Scalar,Eigen::Dynamic,1>>& ref() const {
    return v;
  }
  Eigen::Ref<Eigen::Matrix<Scalar,Eigen::Dynamic,1>>& ref() {
    return v;
  }

  Scalar *data() const {
    return const_cast<Scalar*>(v.data());
  }
  Scalar *data() {
    return v.data();
  }

  // other methods -------------------------------------------------------------
  int size() const {
    return int(v.rows());
  }

  double sizeMB() const {
    return static_cast<double>(size() * sizeof(Scalar)) / (1024.0 * 1024.0);
  }

  Scalar min() const {
    return v.minCoeff();
  }

  template<typename Derived>
  Scalar min(Derived *i) const {
    return v.minCoeff(i);
  }

  Scalar absMin() const {
    return v.cwiseAbs().minCoeff();
  }

  template<typename Derived>
  Scalar absMin(Derived *i) const {
    return v.cwiseAbs().minCoeff(i);
  }

  Scalar max() const {
    return v.maxCoeff();
  }

  template<typename Derived>
  Scalar max(Derived *i) const {
    return v.maxCoeff(i);
  }

  Scalar absMax() const {
    return v.cwiseAbs().maxCoeff();
  }

  template<typename Derived>
  Scalar absMax(Derived *i) const {
    return v.cwiseAbs().maxCoeff(i);
  }

  Scalar sum() const {
    return v.sum();
  }

  Scalar absSum() const {
    return v.template lpNorm<1>();
  }

  auto norm() const -> decltype(v.norm()) {
    return std::real(sqrt(*this * *this));
  }

  auto sqNorm() const -> decltype(v.squaredNorm()) {
    return *this * *this;
  }

  auto real() const -> decltype(v.real()) {
    return v.real();
  }

  auto imag() const -> decltype(v.imag()) {
    return v.imag();
  }

  void print() const {
    std::cout << v.transpose() << std::endl;
  }

  void print(std::ostream& ostream, int precision, const char *msg = "") {
    int old_precision = ostream.precision(precision);
    ostream.setf(std::ios::scientific);
    if(*msg) ostream << msg << std::endl;
    ostream << v << std::endl;
    ostream.precision(old_precision);
    ostream.unsetf(std::ios::scientific);
  }

};

//------------------------------------------------------------------------------

template<typename Derived, typename Scalar>
auto operator+(const Eigen::MatrixBase<Derived>& a, const Vec<Scalar>& b) -> decltype(a+b.ref()) {
  return a + b.ref();
}

//------------------------------------------------------------------------------

template<typename Derived, typename Scalar>
auto operator-(const Eigen::MatrixBase<Derived>& a, const Vec<Scalar>& b) -> decltype(a-b.ref()) {
  return a - b.ref();
}

//------------------------------------------------------------------------------

template<typename T, typename Scalar>
auto operator*(const T& a, const Vec<Scalar>& b) -> decltype(a*b.ref()) {
  return a * b.ref();
}

//------------------------------------------------------------------------------

template<typename Scalar>
std::ostream& operator<<(std::ostream& os, const Vec<Scalar>& a) {
  return (os << a.ref());
}

//------------------------------------------------------------------------------

template<class Scalar>
inline
Vec<Scalar>::Vec() :
  locAlloc(new Eigen::Matrix<Scalar,Eigen::Dynamic,1>(0)),
  v(*locAlloc),
  IPM(nullptr) {
}

//------------------------------------------------------------------------------

template<class Scalar>
inline
Vec<Scalar>::Vec(int l) :
  locAlloc(new Eigen::Matrix<Scalar,Eigen::Dynamic,1>(l)),
  v(*locAlloc),
  IPM(nullptr) {
}

//------------------------------------------------------------------------------

template<class Scalar>
inline
Vec<Scalar>::Vec(int l, Scalar *vv) :
  locAlloc(NULL),
  v(Eigen::Map<Eigen::Matrix<Scalar,Eigen::Dynamic,1>>(vv,l,1)),
  IPM(nullptr) {
}

//------------------------------------------------------------------------------

template<class Scalar>
inline
Vec<Scalar>::Vec(const Vec<Scalar>& y) :
  locAlloc(new Eigen::Matrix<Scalar,Eigen::Dynamic,1>(y.size())),
  v(*locAlloc),
  IPM(y.IPM) {
  v = y.v;
}

//------------------------------------------------------------------------------
// This constructor allows you to construct Vec from Eigen expressions
template<class Scalar>
template<typename Derived>
inline
Vec<Scalar>::Vec(const Eigen::MatrixBase<Derived>& b) :
  locAlloc(new Eigen::Matrix<Scalar,Eigen::Dynamic,1>(b)),
  v(*locAlloc),
  IPM(nullptr) {
}

//------------------------------------------------------------------------------

template<class Scalar>
inline
Vec<Scalar>::~Vec() {
  if(locAlloc) delete locAlloc;
}

//------------------------------------------------------------------------------

template<class Scalar>
inline
Vec<Scalar>&
Vec<Scalar>::setZero() {
  v.setZero();
  return *this;
}

//------------------------------------------------------------------------------

template<class Scalar>
inline
Vec<Scalar>&
Vec<Scalar>::operator=(const Scalar& y) {
  v.setConstant(y);
  return *this;
}

//------------------------------------------------------------------------------

template<class Scalar>
inline
Vec<Scalar>&
Vec<Scalar>::operator=(const Vec<Scalar>& y) {
  if(locAlloc && v.rows() != y.size()) {
    *locAlloc = y.v;
    new (&v) Eigen::Ref<Eigen::Matrix<Scalar,Eigen::Dynamic,1>>(*locAlloc);
  }
  else v = y.v;
  IPM = y.IPM;
  return *this;
}

//------------------------------------------------------------------------------
// This method allows you to assign Eigen expressions to Vec
template<class Scalar>
template<typename Derived>
inline
Vec<Scalar>&
Vec<Scalar>::operator=(const Eigen::MatrixBase<Derived>& b) {
  if(locAlloc && v.rows() != b.rows()) {
    *locAlloc = b;
    new (&v) Eigen::Ref<Eigen::Matrix<Scalar,Eigen::Dynamic,1>>(*locAlloc);
  }
  else v = b;
  return *this;
}

//------------------------------------------------------------------------------

template<class Scalar>
inline
Scalar
Vec<Scalar>::operator*(const Vec<Scalar>& y) const {
  if(IPM) {
    return v.dot(*IPM * y.v);
  }
  else {
    return v.dot(y.v);
  }
}

//------------------------------------------------------------------------------

template<class Scalar>
inline
int
Vec<Scalar>::capacity() const {
  assert(locAlloc);
  return locAlloc->size();
}

//------------------------------------------------------------------------------

template<class Scalar>
inline
void
Vec<Scalar>::reserve(int new_cap) {
  assert(locAlloc);
  const int l = v.size();
  locAlloc->conservativeResize(new_cap);
  new (&v) Eigen::Ref<Eigen::Matrix<Scalar,Eigen::Dynamic,1>>(locAlloc->head(l));
}

//------------------------------------------------------------------------------

template<class Scalar>
inline
void
Vec<Scalar>::push_back(const Scalar& value) {
  const int l = v.size();
  if(l >= locAlloc->size()) {
    locAlloc->conservativeResize(l+1);
  }
  new (&v) Eigen::Ref<Eigen::Matrix<Scalar,Eigen::Dynamic,1>>(*locAlloc);
  v[l] = value;
}

//------------------------------------------------------------------------------

template<class Scalar>
inline
void
Vec<Scalar>::shrink_to_fit() {
  assert(locAlloc);
  const int l = v.size();
  locAlloc->conservativeResize(l);
  new (&v) Eigen::Ref<Eigen::Matrix<Scalar,Eigen::Dynamic,1>>(*locAlloc);
}

//------------------------------------------------------------------------------

template<class Scalar>
inline
void
Vec<Scalar>::resize(int l) {
  assert(locAlloc);
  locAlloc->conservativeResize(l);
  new (&v) Eigen::Ref<Eigen::Matrix<Scalar,Eigen::Dynamic,1>>(*locAlloc);
}

//------------------------------------------------------------------------------

template<class Scalar>
inline
void
Vec<Scalar>::resize(int l, const Scalar& initialValue) {
  int k = size();
  resize(l);
  for(int i = k; i < l; ++i) {
    v[i] = initialValue;
  }
}

//------------------------------------------------------------------------------

template<class Scalar>
inline
void
Vec<Scalar>::resize(int l, NodeData& data, int iflag, Scalar spval) {
  resize(size() + int(data.size()));
  switch(iflag) {
    case 0 : { // constant extrapolation
      for(auto& p : data) {
        v[std::get<0>(p)] = v[std::get<1>(p)[0]];
      }
    } break;
    case 1 : default : { // linear interpolation
      for(auto& p : data) {
        v[std::get<0>(p)] = Scalar(std::get<2>(p)[0]*v[std::get<1>(p)[0]] + std::get<2>(p)[1]*v[std::get<1>(p)[1]]);
      }
    } break;
    case 2 : { // use local tag to determine whether to use constant extrapolation or linear interpolation
      for(auto& p : data) {
        switch(std::get<3>(p)) {
          case 0 :
            v[std::get<0>(p)] = v[std::get<1>(p)[0]];
            break;
          case 1 : default :
            v[std::get<0>(p)] = Scalar(std::get<2>(p)[0]*v[std::get<1>(p)[0]] + std::get<2>(p)[1]*v[std::get<1>(p)[1]]);
            break;
        }
      }
    } break;
    case 3 : { // use local tag to determine whether to apply special treatment or regular linear interpolation
      for(auto& p : data) {
        switch(std::get<3>(p)) {
          case -1 :
            v[std::get<0>(p)] = spval;
            break;
          case 0 :
            v[std::get<0>(p)] = std::get<4>(p)*v[std::get<1>(p)[0]];
            break;
          case 1 : default :
            v[std::get<0>(p)] = Scalar(std::get<2>(p)[0]*v[std::get<1>(p)[0]] + std::get<2>(p)[1]*v[std::get<1>(p)[1]]);
            break;
        }
      }
    } break;
    case 4 : { // use local tag to determine whether to use special value or constant extrapolation
      for(auto& p : data) {
        switch(std::get<3>(p)) {
          case -1 :
            v[std::get<0>(p)] = spval;
            break;
          case 0 : default :
            v[std::get<0>(p)] = v[std::get<1>(p)[0]];
            break;
        }
      }
    } break;
  }
  resize(l);
}

//------------------------------------------------------------------------------

template<class Scalar>
inline
void
Vec<Scalar>::resize(int l, const Vec<Scalar>& v0, GenNodeData<4>& data, int iflag, Scalar spval) {
  resize(l);
  switch(iflag) {
    case 0 : { // constant extrapolation
      for(auto& p : data) {
        v[std::get<0>(p)] = v0[std::get<1>(p)[0]];
      }
    } break;
    case 1 : default : { // linear interpolation
      for(auto& p : data) {
        v[std::get<0>(p)] = Scalar(std::get<2>(p)[0]*v0[std::get<1>(p)[0]] + std::get<2>(p)[1]*v0[std::get<1>(p)[1]] +
                                   std::get<2>(p)[2]*v0[std::get<1>(p)[2]] + std::get<2>(p)[3]*v0[std::get<1>(p)[3]]);
      }
    } break;
    case 2 : { // use local tag to determine whether to use constant extrapolation or linear interpolation
      for(auto& p : data) {
        switch(std::get<3>(p)) {
          case 0 :
            v[std::get<0>(p)] = v0[std::get<1>(p)[0]];
            break;
          case 1 : default :
            v[std::get<0>(p)] = Scalar(std::get<2>(p)[0]*v0[std::get<1>(p)[0]] + std::get<2>(p)[1]*v0[std::get<1>(p)[1]] +
                                       std::get<2>(p)[2]*v0[std::get<1>(p)[2]] + std::get<2>(p)[3]*v0[std::get<1>(p)[3]]);
            break;
        }
      }
    } break;
    case 4 : { // use local tag to determine whether to use special value or constant extrapolation
      for(auto& p : data) {
        switch(std::get<3>(p)) {
          case -1 :
            v[std::get<0>(p)] = spval;
            break;
          case 0 : default :
            v[std::get<0>(p)] = v0[std::get<1>(p)[0]];
            break;
        }
      }
    } break;
  }
}

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

template<class Scalar, int Dim>
class SVec {
 public:
  typedef int InfoType;
  typedef typename Eigen::NumTraits<Scalar>::Real RealScalarType;

 protected:
  Eigen::Matrix<Scalar,Eigen::Dynamic,Dim,(Dim==1)?Eigen::ColMajor:Eigen::RowMajor> *locAlloc;
  Eigen::Ref<Eigen::Matrix<Scalar,Eigen::Dynamic,Dim,(Dim==1)?Eigen::ColMajor:Eigen::RowMajor>> v;

 public:
  // constructors --------------------------------------------------------------
  SVec();
  SVec(int);
  SVec(int, int);
  SVec(int, Scalar(*)[(Dim==Eigen::Dynamic)?0:Dim]);
  SVec(int, int, Scalar *);
  SVec(const SVec<Scalar, Dim>&);
  template<typename Derived>
  SVec(const Eigen::MatrixBase<Derived>&);

  // destructor ----------------------------------------------------------------
  ~SVec();

  // assignment operators ------------------------------------------------------
  SVec<Scalar, Dim>& operator=(const Scalar&);
  SVec<Scalar, Dim>& operator=(const SVec<Scalar, Dim>&);
  template<typename Derived>
  SVec<Scalar, Dim>& operator=(const Eigen::MatrixBase<Derived>&);
  SVec<Scalar, Dim>& setZero();

  // arithmetic operators ------------------------------------------------------
  // note: all of these return expressions
  auto operator+=(const SVec<Scalar, Dim>& b) -> decltype(v+=b.v) {
    return v += b.v;
  }

  template<typename Derived>
  auto operator+=(const Eigen::MatrixBase<Derived>& b) -> decltype(v+=b) {
    return v += b;
  }

  auto operator+(const SVec<Scalar, Dim>& b) const -> decltype(v+b.v) {
    return v + b.v;
  }

  template<typename Derived>
  auto operator+(const Eigen::MatrixBase<Derived>& b) const -> decltype(v+b) {
    return v + b;
  }

  auto operator-=(const SVec<Scalar, Dim>& b) -> decltype(v-=b.v) {
    return v -= b.v;
  }

  template<typename Derived>
  auto operator-=(const Eigen::MatrixBase<Derived>& b) -> decltype(v-=b) {
    return v -= b;
  }

  auto operator-() const -> decltype(-v) {
    return -v;
  }

  auto operator-(const SVec<Scalar, Dim>& b) const -> decltype(v-b.v) {
    return v - b.v;
  }

  template<typename Derived>
  auto operator-(const Eigen::MatrixBase<Derived>& b) const -> decltype(v-b) {
    return v - b;
  }

  auto operator*=(const Scalar& s) -> decltype(v*=s) {
    return v *= s;
  }

  template<typename T>
  auto operator*(const T& s) const -> decltype(v*s) {
    return v * s;
  }

  Scalar operator*(const SVec<Scalar, Dim>&) const;

  auto operator/=(const Scalar& s) -> decltype(v/=s) {
    return v /= s;
  }

  template<typename T>
  auto operator/(const T& s) const -> decltype(v/s) {
    return v / s;
  }

  auto operator*=(const Vec<Scalar>& b) -> decltype(v.array().colwise()*=b.ref().array()) {
    return v.array().colwise() *= b.ref().array();
  }

  auto operator/=(const Vec<Scalar>& b) -> decltype(v.array().colwise()/=b.ref().array()) {
    return v.array().colwise() /= b.ref().array();
  }

  // other operators -----------------------------------------------------------
  Scalar *operator[](int i) {
    return const_cast<Scalar*>(v.row(i).data());
  }
  const Scalar *operator[](int i) const {
    return v.row(i).data();
  }
  Scalar& operator()(int i, int j) {
    return v(i,j);
  }
  const Scalar& operator()(int i, int j) const {
    return v(i,j);
  }

  // reshaping methods ---------------------------------------------------------
  auto vector(int i) -> decltype(v.row(i).transpose()) {
    return v.row(i).transpose();
  }
  auto vector(int i) const -> decltype(v.row(i).transpose()) {
    return v.row(i).transpose();
  }

  // resizing methods ----------------------------------------------------------
  int capacity() const;
  void reserve(int);
  void push_back(const Scalar *);
  void shrink_to_fit();
  void resize(int);
  void resize(int, const Scalar&);
  void resize(int, NodeData&, int = 1);
  void resize(int, NodeData&, NodalGrad<Dim, Scalar>&, int = 1);
  void resize(int, const SVec<Scalar, Dim>&, GenNodeData<4>&, int = 1);

  // data access methods -------------------------------------------------------
  const Eigen::Ref<Eigen::Matrix<Scalar,Eigen::Dynamic,Dim,(Dim==1)?Eigen::ColMajor:Eigen::RowMajor>>& ref() const {
    return v;
  }
  Eigen::Ref<Eigen::Matrix<Scalar,Eigen::Dynamic,Dim,(Dim==1)?Eigen::ColMajor:Eigen::RowMajor>>& ref() {
    return v;
  }

  Scalar(*data() const)[(Dim==Eigen::Dynamic)?0:Dim] {
    static_assert(Dim != Eigen::Dynamic, "*** Error in SVec data method #1: Dim == Eigen::Dynamic");
    return reinterpret_cast<Scalar(*)[(Dim==Eigen::Dynamic)?0:Dim]>(const_cast<Scalar*>(v.data()));
  }
  Scalar(*data())[(Dim==Eigen::Dynamic)?0:Dim] {
    static_assert(Dim != Eigen::Dynamic, "*** Error in SVec data method #2: Dim == Eigen::Dynamic");
    return reinterpret_cast<Scalar(*)[(Dim==Eigen::Dynamic)?0:Dim]>(v.data());
  }

  auto row(int i) const -> decltype(v.row(i)) {
    return v.row(i);
  }
  auto row(int i) -> decltype(v.row(i)) {
    return v.row(i);
  }

  auto col(int j) const -> decltype(v.col(j)) {
    return v.col(j);
  }
  auto col(int j) -> decltype(v.col(j)) {
    return v.col(j);
  }

  // other methods -------------------------------------------------------------
  int size() const {
    return int(v.rows());
  }

  int dim() const {
    return int(v.cols());
  }

  double sizeMB() const {
    return static_cast<double>(size() * v.cols() * sizeof(Scalar)) / (1024.0 * 1024.0);
  }

  Scalar min() const {
    return v.minCoeff();
  }

  template<typename Derived>
  Scalar min(Derived *i, Derived *j) const {
    return v.minCoeff(i, j);
  }

  void min(std::vector<Scalar>& vmin) const {
    vmin.resize(v.cols());
    for(int iDim = 0; iDim < v.cols(); iDim++) {
      vmin[iDim] = v.col(iDim).minCoeff();
    }
  }

  Scalar absMin() const {
    return v.cwiseAbs().minCoeff();
  }

  template<typename Derived>
  Scalar absMin(Derived *i, Derived *j) const {
    return v.cwiseAbs().minCoeff(i, j);
  }

  void absMin(std::vector<Scalar>& vmin) const {
    vmin.resize(v.cols());
    for(int idim = 0; idim < v.cols(); idim++) {
      vmin[idim] = v.cwiseAbs().col(idim).minCoeff();
    }
  }

  Scalar max() const {
    return v.maxCoeff();
  }

  template<typename Derived>
  Scalar max(Derived *i, Derived *j) const {
    return v.maxCoeff(i, j);
  }

  void max(std::vector<Scalar>& vmax) const {
    vmax.resize(v.cols());
    for(int iDim = 0; iDim < v.cols(); iDim++) {
      vmax[iDim] = v.col(iDim).maxCoeff();
    }
  }

  Scalar absMax() const {
    return v.cwiseAbs().maxCoeff();
  }

  template<typename Derived>
  Scalar absMax(Derived *i, Derived *j) const {
    return v.cwiseAbs().maxCoeff(i, j);
  }

  void absMax(std::vector<Scalar>& vmax) const {
    vmax.resize(v.cols());
    for(int idim = 0; idim < v.cols(); idim++) {
      vmax[idim] = v.cwiseAbs().col(idim).maxCoeff();
    }
  }

  Scalar sum() const {
    return v.sum();
  }

  void sum(std::vector<Scalar>& vsum) const {
    vsum.resize(v.cols());
    for(int iDim = 0; iDim < v.cols(); iDim++) {
      vsum[iDim] = v.col(iDim).sum();
    }
  }

  Scalar absSum() const {
    return v.template lpNorm<1>();
  }

  auto norm() const -> decltype(v.norm()) {
    return v.norm();
  }

  auto sqNorm() const -> decltype(v.squaredNorm()) {
    return v.squaredNorm();
  }

  auto real() const -> decltype(v.real()) {
    return v.real();
  }

  auto imag() const -> decltype(v.imag()) {
    return v.imag();
  }

  void print() const {
    std::cout << v << std::endl;
  }

};

//------------------------------------------------------------------------------

template<typename Derived, typename Scalar, int Dim>
auto operator+(const Eigen::MatrixBase<Derived>& a, const SVec<Scalar, Dim>& b) -> decltype(a+b.ref()) {
  return a + b.ref();
}

//------------------------------------------------------------------------------

template<typename Derived, typename Scalar, int Dim>
auto operator-(const Eigen::MatrixBase<Derived>& a, const SVec<Scalar, Dim>& b) -> decltype(a-b.ref()) {
  return a - b.ref();
}

//------------------------------------------------------------------------------

template<typename T, typename Scalar, int Dim>
auto operator*(const T& a, const SVec<Scalar, Dim>& b) -> decltype(a*b.ref()) {
  return a * b.ref();
}

//------------------------------------------------------------------------------

template<typename Scalar, int Dim>
std::ostream& operator<<(std::ostream& os, const SVec<Scalar, Dim>& a) {
  return (os << a.ref());
}

//------------------------------------------------------------------------------

template<class Scalar, int Dim>
inline
SVec<Scalar, Dim>::SVec() :
  locAlloc(new Eigen::Matrix<Scalar,Eigen::Dynamic,Dim,(Dim==1)?Eigen::ColMajor:Eigen::RowMajor>(0,(Dim==Eigen::Dynamic)?0:Dim)),
  v(*locAlloc) {
}

//------------------------------------------------------------------------------

template<class Scalar, int Dim>
inline
SVec<Scalar, Dim>::SVec(int l) :
  locAlloc(new Eigen::Matrix<Scalar,Eigen::Dynamic,Dim,(Dim==1)?Eigen::ColMajor:Eigen::RowMajor>(l,Dim)),
  v(*locAlloc) {
  static_assert(Dim != Eigen::Dynamic, "*** Error in SVec constructor #2: Dim == Eigen::Dynamic");
}

//------------------------------------------------------------------------------

template<class Scalar, int Dim>
inline
SVec<Scalar, Dim>::SVec(int l, int m) :
  locAlloc(new Eigen::Matrix<Scalar,Eigen::Dynamic,Dim,(Dim==1)?Eigen::ColMajor:Eigen::RowMajor>(l,m)),
  v(*locAlloc) {
}

//------------------------------------------------------------------------------

template<class Scalar, int Dim>
inline
SVec<Scalar, Dim>::SVec(int l, Scalar(*vv)[(Dim==Eigen::Dynamic)?0:Dim]) :
  locAlloc(NULL),
  v(Eigen::Map<Eigen::Matrix<Scalar,Eigen::Dynamic,Dim,(Dim==1)?Eigen::ColMajor:Eigen::RowMajor>>(vv[0],l,Dim)) {
  static_assert(Dim != Eigen::Dynamic, "*** Error in SVec constructor #4: Dim == Eigen::Dynamic");
}

//------------------------------------------------------------------------------

template<class Scalar, int Dim>
inline
SVec<Scalar, Dim>::SVec(int l, int m, Scalar *vv) :
  locAlloc(NULL),
  v(Eigen::Map<Eigen::Matrix<Scalar,Eigen::Dynamic,Dim,(Dim==1)?Eigen::ColMajor:Eigen::RowMajor>>(vv,l,m)) {
}

//------------------------------------------------------------------------------

template<class Scalar, int Dim>
inline
SVec<Scalar, Dim>::SVec(const SVec<Scalar, Dim>& y) :
  locAlloc(new Eigen::Matrix<Scalar,Eigen::Dynamic,Dim,(Dim==1)?Eigen::ColMajor:Eigen::RowMajor>(y.size(),y.dim())),
  v(*locAlloc) {
  v = y.v;
}

//------------------------------------------------------------------------------
// This constructor allows you to construct SVec from Eigen expressions
template<class Scalar, int Dim>
template<typename Derived>
inline
SVec<Scalar, Dim>::SVec(const Eigen::MatrixBase<Derived>& b) :
  locAlloc(new Eigen::Matrix<Scalar,Eigen::Dynamic,Dim,(Dim==1)?Eigen::ColMajor:Eigen::RowMajor>(b)),
  v(*locAlloc) {
}

//------------------------------------------------------------------------------
template<class Scalar, int Dim>
inline
SVec<Scalar, Dim>::~SVec() {
  if(locAlloc) delete locAlloc;
}

//------------------------------------------------------------------------------

template<class Scalar, int Dim>
inline
SVec<Scalar, Dim>&
SVec<Scalar, Dim>::setZero() {
  v.setZero();
  return *this;
}

//------------------------------------------------------------------------------

template<class Scalar, int Dim>
inline
SVec<Scalar, Dim>&
SVec<Scalar, Dim>::operator=(const Scalar& y) {
  v.setConstant(y);
  return *this;
}

//------------------------------------------------------------------------------

template<class Scalar, int Dim>
inline
SVec<Scalar, Dim>&
SVec<Scalar, Dim>::operator=(const SVec<Scalar, Dim>& y) {
  if(locAlloc && (v.rows() != y.size() || v.cols() != y.dim())) {
    *locAlloc = y.v;
    new (&v) Eigen::Ref<Eigen::Matrix<Scalar,Eigen::Dynamic,Dim,(Dim==1)?Eigen::ColMajor:Eigen::RowMajor>>(*locAlloc);
  }
  else v = y.v;
  return *this;
}

//------------------------------------------------------------------------------
// This method allows you to assign Eigen expressions to SVec
template<class Scalar, int Dim>
template<typename Derived>
inline
SVec<Scalar, Dim>&
SVec<Scalar, Dim>::operator=(const Eigen::MatrixBase<Derived>& b) {
  if(locAlloc && (v.rows() != b.rows() || v.cols() != b.cols())) {
    *locAlloc = b;
    new (&v) Eigen::Ref<Eigen::Matrix<Scalar,Eigen::Dynamic,Dim,(Dim==1)?Eigen::ColMajor:Eigen::RowMajor>>(*locAlloc);
  }
  else v = b;
  return *this;
}

//------------------------------------------------------------------------------

template<class Scalar, int Dim>
inline
Scalar
SVec<Scalar, Dim>::operator*(const SVec<Scalar, Dim>& y) const {
  return (v.cwiseProduct(y.v)).sum();
}

//------------------------------------------------------------------------------

template<class Scalar, int Dim>
inline void conservativeResize(Eigen::Matrix<Scalar,Eigen::Dynamic,Dim,(Dim==1)?Eigen::ColMajor:Eigen::RowMajor> *m, int rows, int cols) {
  m->conservativeResize(rows, cols);
}

template<class Scalar>
inline void conservativeResize(Eigen::Matrix<Scalar,Eigen::Dynamic,1,Eigen::ColMajor> *m, int rows, int) {
  // specialization for SVecs with 1 column: the 1-arg version of Matrix::conservativeResize seems to be much faster for this case
  m->conservativeResize(rows);
}

//------------------------------------------------------------------------------

template<class Scalar, int Dim>
inline
int
SVec<Scalar, Dim>::capacity() const {
  assert(locAlloc);
  return locAlloc->rows();
}

//------------------------------------------------------------------------------

template<class Scalar, int Dim>
inline
void
SVec<Scalar, Dim>::reserve(int new_cap) {
  assert(locAlloc);
  const int l = v.rows();
  conservativeResize(locAlloc, new_cap, v.cols());
  new (&v) Eigen::Ref<Eigen::Matrix<Scalar,Eigen::Dynamic,Dim,(Dim==1)?Eigen::ColMajor:Eigen::RowMajor>>(locAlloc->topRows(l));
}

//------------------------------------------------------------------------------

template<class Scalar, int Dim>
inline
void
SVec<Scalar, Dim>::push_back(const Scalar *value) {
  const int l = v.rows();
  if(l >= locAlloc->rows()) {
    conservativeResize(locAlloc, l+1, v.cols());
  }
  new (&v) Eigen::Ref<Eigen::Matrix<Scalar,Eigen::Dynamic,Dim,(Dim==1)?Eigen::ColMajor:Eigen::RowMajor>>(*locAlloc);
  for(int k = 0; k < v.cols(); ++k) v[l][k] = value[k];
}

//------------------------------------------------------------------------------

template<class Scalar, int Dim>
inline
void
SVec<Scalar, Dim>::shrink_to_fit() {
  assert(locAlloc);
  const int l = v.rows();
  conservativeResize(locAlloc, l, v.cols());
  new (&v) Eigen::Ref<Eigen::Matrix<Scalar,Eigen::Dynamic,Dim,(Dim==1)?Eigen::ColMajor:Eigen::RowMajor>>(*locAlloc);
}

//------------------------------------------------------------------------------

template<class Scalar, int Dim>
inline
void
SVec<Scalar, Dim>::resize(int l) {
  assert(locAlloc);
  conservativeResize(locAlloc, l, v.cols());
  new (&v) Eigen::Ref<Eigen::Matrix<Scalar,Eigen::Dynamic,Dim,(Dim==1)?Eigen::ColMajor:Eigen::RowMajor>>(*locAlloc);
}

//------------------------------------------------------------------------------

template<class Scalar, int Dim>
inline
void
SVec<Scalar, Dim>::resize(int l, const Scalar& initialValue) {
  int k = size();
  resize(l);
  for(int i = k; i < l; ++i) {
    for(int j = 0; j < v.cols(); ++j) {
      v(i,j) = initialValue;
    }
  }
}

//------------------------------------------------------------------------------

template<class Scalar, int Dim>
inline
void
SVec<Scalar, Dim>::resize(int l, NodeData& data, int iflag) {
  resize(size() + int(data.size()));
  switch(iflag) {
    case 0 : { // constant extrapolation
      for(auto& p : data) {
        v.row(std::get<0>(p)) = v.row(std::get<1>(p)[0]);
      }
    } break;
    case 1 : default : { // linear interpolation
      for(auto& p : data) {
        v.row(std::get<0>(p)) = std::get<2>(p)[0]*v.row(std::get<1>(p)[0]) + std::get<2>(p)[1]*v.row(std::get<1>(p)[1]);
      }
    } break;
    case 2 : { // use local tag to determine whether to use constant extrapolation or linear interpolation
      for(auto& p : data) {
        switch(std::get<3>(p)) {
          case 0 :
            v.row(std::get<0>(p)) = v.row(std::get<1>(p)[0]);
            break;
          case 1 : default :
            v.row(std::get<0>(p)) = std::get<2>(p)[0]*v.row(std::get<1>(p)[0]) + std::get<2>(p)[1]*v.row(std::get<1>(p)[1]);
            break;
        }
      }
    } break;
  }
  resize(l);
}

//------------------------------------------------------------------------------

template<class Scalar, int Dim>
inline
void
SVec<Scalar, Dim>::resize(int l, NodeData& data, NodalGrad<Dim, Scalar>& ngrad, int iflag) {
  resize(size() + int(data.size()));
  switch(iflag) {
    case 0 : { // constant extrapolation
      for(auto& p : data) {
        v.row(std::get<0>(p)) = v.row(std::get<1>(p)[0]);
      }
    } break;
    case 1 : default : { // linear interpolation
      for(auto& p : data) {
        v.row(std::get<0>(p)) = std::get<2>(p)[0]*v.row(std::get<1>(p)[0]) + std::get<2>(p)[1]*v.row(std::get<1>(p)[1]);
      }
    } break;
    case 2 : { // use local tag to determine whether to use constant extrapolation or conservative interpolation or linear interpolation
      for(auto& p : data) {
        switch(std::get<3>(p)) {
          case 0 :
            v.row(std::get<0>(p)) = v.row(std::get<1>(p)[0]);
            break;
          case 1 : default :
            // first order term
            v.row(std::get<0>(p)) = std::get<2>(p)[0]*v.row(std::get<1>(p)[0]) + std::get<2>(p)[1]*v.row(std::get<1>(p)[1]);
            // second order term
            for(int iNode = 0; iNode < 2; ++iNode) {
              v.row(std::get<0>(p)) += ngrad.getX().ref().row(std::get<1>(p)[iNode]) * std::get<5>(p)[iNode][0];
              v.row(std::get<0>(p)) += ngrad.getY().ref().row(std::get<1>(p)[iNode]) * std::get<5>(p)[iNode][1];
              v.row(std::get<0>(p)) += ngrad.getZ().ref().row(std::get<1>(p)[iNode]) * std::get<5>(p)[iNode][2];
            }
            break;
          case 2 :
            v.row(std::get<0>(p)) = std::get<2>(p)[0]*v.row(std::get<1>(p)[0]) + std::get<2>(p)[1]*v.row(std::get<1>(p)[1]);
            break;
        }
      }
    } break;
  }
  resize(l);
}

//------------------------------------------------------------------------------

template<class Scalar, int Dim>
inline
void
SVec<Scalar, Dim>::resize(int l, const SVec<Scalar, Dim>& v0, GenNodeData<4>& data, int iflag) {
  resize(l);
  switch(iflag) {
    case 0 : { // constant extrapolation
      for(auto& p : data) {
        v.row(std::get<0>(p)) = v0.row(std::get<1>(p)[0]);
      }
    } break;
    case 1 : default : { // linear interpolation
      for(auto& p : data) {
        v.row(std::get<0>(p)) = std::get<2>(p)[0]*v0.row(std::get<1>(p)[0]) + std::get<2>(p)[1]*v0.row(std::get<1>(p)[1]) +
                                std::get<2>(p)[2]*v0.row(std::get<1>(p)[2]) + std::get<2>(p)[3]*v0.row(std::get<1>(p)[3]);
      }
    } break;
    case 2 : { // use local tag to determine whether to use constant extrapolation or linear interpolation
      for(auto& p : data) {
        switch(std::get<3>(p)) {
          case 0 :
            v.row(std::get<0>(p)) = v0.row(std::get<1>(p)[0]);
            break;
          case 1 : default :
            v.row(std::get<0>(p)) = std::get<2>(p)[0]*v0.row(std::get<1>(p)[0]) + std::get<2>(p)[1]*v0.row(std::get<1>(p)[1]) +
                                    std::get<2>(p)[2]*v0.row(std::get<1>(p)[2]) + std::get<2>(p)[3]*v0.row(std::get<1>(p)[3]);
            break;
        }
      }
    } break;
  }
}

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

namespace Eigen { namespace internal {

template<typename Scalar>
struct scalar_product_traits<Vec<Scalar>, Scalar>
{
  enum { Defined = 1 };
  typedef Vec<Scalar> ReturnType;
};

template<typename Scalar>
struct scalar_product_traits<Scalar, Vec<Scalar>>
{
  enum { Defined = 1 };
  typedef Vec<Scalar> ReturnType;
};

template<typename Scalar>
struct scalar_product_traits<Vec<Scalar>, std::complex<Scalar>>
{
  enum { Defined = 1 };
  typedef Vec<std::complex<Scalar>> ReturnType;
};

template<typename Scalar>
struct scalar_product_traits<std::complex<Scalar>, Vec<Scalar>>
{
  enum { Defined = 1 };
  typedef Vec<std::complex<Scalar>> ReturnType;
};

template<typename Scalar>
struct scalar_product_traits<Vec<std::complex<Scalar>>, Scalar>
{
  enum { Defined = 1 };
  typedef Vec<std::complex<Scalar>> ReturnType;
};

template<typename Scalar>
struct scalar_product_traits<Scalar, Vec<std::complex<Scalar>>>
{
  enum { Defined = 1 };
  typedef Vec<std::complex<Scalar>> ReturnType;
};

template<typename Scalar, int Dim>
struct scalar_product_traits<SVec<Scalar, Dim>, Scalar>
{
  enum { Defined = 1 };
  typedef SVec<Scalar, Dim> ReturnType;
};

template<typename Scalar, int Dim>
struct scalar_product_traits<Scalar, SVec<Scalar, Dim>>
{
  enum { Defined = 1 };
  typedef SVec<Scalar, Dim> ReturnType;
};

template<typename Scalar, int Dim>
struct scalar_product_traits<SVec<Scalar, Dim>, std::complex<Scalar>>
{
  enum { Defined = 1 };
  typedef SVec<std::complex<Scalar>, Dim> ReturnType;
};

template<typename Scalar, int Dim>
struct scalar_product_traits<std::complex<Scalar>, SVec<Scalar, Dim>>
{
  enum { Defined = 1 };
  typedef SVec<std::complex<Scalar>, Dim> ReturnType;
};

template<typename Scalar, int Dim>
struct scalar_product_traits<SVec<std::complex<Scalar>, Dim>, Scalar>
{
  enum { Defined = 1 };
  typedef SVec<std::complex<Scalar>, Dim> ReturnType;
};

template<typename Scalar, int Dim>
struct scalar_product_traits<Scalar, SVec<std::complex<Scalar>, Dim>>
{
  enum { Defined = 1 };
  typedef SVec<std::complex<Scalar>, Dim> ReturnType;
};

}}

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

#endif

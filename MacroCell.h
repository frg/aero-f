#ifndef _MACRO_CELL_H_
#define _MACRO_CELL_H_

#include <vector>
#include <algorithm>

class Connectivity;

template<class Scalar> class Vec;
template<class Scalar, int dim> class SVec;

//------------------------------------------------------------------------------

class MacroCell {

  double volume;
  std::vector<int> subCells;

 public:
  MacroCell() : volume(0.0) {}
  ~MacroCell() {}

  int& operator[](int i) {
    return subCells[i];
  }

  void addCell(int i) {
    subCells.push_back(i);
  }

  bool contains(int i) {
    return (std::find(subCells.begin(), subCells.end(), i) != subCells.end());
  }

  int size() {
    return subCells.size();
  }

  double vol() {
    return volume;
  }

  template<int dim>
  void computeBarValues(Vec<double>&, SVec<double, dim>&, SVec<double, dim>&, const double,
                        const bool = true, const bool = true);

  friend class MacroCellSet;

};

//------------------------------------------------------------------------------

class MacroCellSet {

  std::vector<MacroCell *> macroCells;

 public:
  MacroCellSet(Connectivity *);
  ~MacroCellSet();

  MacroCell& operator[](int i) {
    return *(macroCells[i]);
  }

  int size() {
    return macroCells.size();
  }

  template<int dim>
  void compute(bool, Vec<double>&, SVec<double, dim>&, SVec<double, dim>&, const double,
               const bool = true, const bool = true);

};

//------------------------------------------------------------------------------

#endif

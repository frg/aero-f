#ifndef _IMPLICIT_ROM_TS_DESC_H_
#define _IMPLICIT_ROM_TS_DESC_H_

#include <ImplicitTsDesc.h>
#include <VectorSet.h>
#include <DistMatrix.h>

#include <array>

class IoData;
class Domain;
class NonlinearRom;

template<class VecType> class Clustering;
template<int dim> class ParallelRom;
template<int dim> class NearestNeighborRom;
template<int dim> class EmbeddedPOD;
template<class Scalar, int dim> class DistSVec;
template<class Scalar> class Vec;
template<class Scalar> class DenseMat;
template<class T> class CommPattern;

#ifndef _MATVECPROD_TMPL_
  #define _MATVECPROD_TMPL_
  template<int dim, int neq, class Scalar2 = double> class MatVecProd;
#endif

#ifdef USE_EIGEN3_DenseMat
#include <Eigen/SVD>
#include "ReducedCoordNeuralNet.h"

typedef Eigen::Matrix<double,Eigen::Dynamic,Eigen::Dynamic,Eigen::RowMajor> MatrixType;
typedef Eigen::Matrix<double,Eigen::Dynamic,1,Eigen::ColMajor> VectorType;
#endif

//------------------------------------------------------------------------------

template<int dim>
class ImplicitRomTsDesc : public ImplicitTsDesc<dim> {

 public:
  int currentCluster;

 protected:
  // for ROB
  NonlinearRom *nlrom;
  EmbeddedPOD<dim> *embedpod;
  Clustering<DistSVec<double, dim>> *clustering;
  DistMat<DenseMat<double>> ROB, QROB, GQROB;
  std::vector<int> loadedClusters;
  std::vector<DistMat<DenseMat<double>>> ROBvec, QROBvec; // for new caching strategy
  std::array<CommPattern<double> *, 3> robPat;  // holds CommPatterns for AJ assembly
  DistSVec<double, dim> Uref;
  bool cache;
  int nPod;
  bool quadratic;
  bool useGenMan;
  ReducedCoordNeuralNet* nn;
  double total_memory;
  DenseMat<double> mat;

  // for local ROBs
  std::vector<double> interpolatedICWeights;
  bool fastDistanceComparisons;
  int *basisSizes;
  DenseMat<double> VTV; // basis inner products, for non-orthogonal projections

  // for nonlinear system (Gauss-Newton)
  MatVecProd<dim, dim> *mvp;
  DistMat<DenseMat<double>> AJ;                           // action of Jacobian on ROB
  DistSVec<double, dim>& F;                               // nonlinear function
  Vec<double> From, dQrom, Qrom, Qromn;
  Vec<double> Qrom_nm1, Qrom_nm2, Qrom_n;

  // legacy storage implementation
  bool oldROB;
  VecSet<DistSVec<double, dim>> *ROBv0, *AJv0, *QROBv0, *GQROBv0;
  std::vector<VecSet<DistSVec<double, dim>>*> ROBvecv0, QROBvecv0; // for new caching strategy

  // componentwise scaling
  bool componentScalingFlag;
  std::vector<double> componentScaling;
  DistSVec<double, dim> *Fscaled;

  // for row ROBs
  bool rowROB;
  bool AJSparsityMasksSet;
  Eigen::PermutationMatrix<Eigen::Dynamic> *basisPermMat;
  std::vector<int> basisIndicesLoaded;
  std::vector<int> prevBasisIndicesLoaded;

  NearestNeighborRom<dim> *knnrom;

  DenseMat<double> redJac;
  ParallelRom<dim> *parallelRom;

  int projType, lsSolver;
  bool reuseJac, newJac;

  double weight_C1;
  double weight_C2;
  double weight_C3;
  double weight_eul2;
  double weight_k;
  bool computeStochasticDrawWeights;
  bool useKWeight;

 protected:
  virtual bool isPostproRom() { return false; };

  virtual void computeFunction(DistSVec<double, dim>&, DistSVec<double, dim>&, bool);
  virtual double meritFunction(DistSVec<double, dim>&);
  virtual void solveLinearSystem(int, DistSVec<double, dim>&, DistSVec<double, dim>&);
  void adjustStep(DistSVec<double, dim>&, double);
  void incrementSolution(DistSVec<double, dim>&, DistSVec<double, dim>&, DistSVec<double, dim>&);
  void incrementSolution();
  void resetSolution(DistSVec<double, dim>&, DistSVec<double, dim>&);

  void expandVector(Vec<double>&, DistSVec<double, dim>&, bool = false);

  virtual void computeQuadraticGrad(Vec<double>&);
  virtual void computeGenManGrad(Vec<double>& y);
  virtual void restrictProduct(DistMat<DenseMat<double>>&);
  virtual void applyMaskColumn(const DistVec<bool>&, DistMat<DenseMat<double>>&);
  virtual void projectOntoSubspace(DistMat<DenseMat<double>>&, DistSVec<double, dim>&, Vec<double>&);
  virtual void projectOntoSubspace(DistMat<DenseMat<double>>&, DistMat<DenseMat<double>>&, DenseMat<double>&);
  virtual void projectOntoSubspace(DistMat<DenseMat<double>>&, DenseMat<double>&);

  bool checkLocalRomStatus(DistSVec<double, dim>&);
  virtual void loadCluster(int, bool);
  void projectSwitchStateOntoAffineSubspace(int, DistSVec<double, dim>&);
  void projectSwitchStateOntoAffineSubspaceFast(int, int, Vec<double>&, DistSVec<double, dim>&);
  void solveSwitchStateOntoQuadManifoldFast(int, int, Vec<double>&);
  void solveInterpStateOntoQuadManifoldFast(int, Vec<double>&, Vec<double>&, Vec<double>&);
  void projectState(DistSVec<double, dim>&, Vec<double>&);
  void projectState(DistSVec<double, dim>&, int);

  void formInterpolationWeights();
  bool formInitialCondition(DistSVec<double, dim>&);

  void writeReducedCoordsToDisk(bool, int);
  void writeReducedResidToDisk(bool, int);
  void writeReducedJacToDisk(bool, int);

  void computeComponentScaling(DistSVec<double, dim>&);
  void scaleComponents(DistSVec<double, dim>&);

  void undoRowRobPerm(Vec<double>&);
  void applyRowRobPerm(Vec<double>&);

  void initNearestNeighbor(bool, bool = true);
  void constructNearestNeighborBasis(DistSVec<double, dim>&);

 public:
  ImplicitRomTsDesc(IoData&, GeoSource&, Domain *, bool = true, bool = true);
  virtual ~ImplicitRomTsDesc();

  //TODO: resize functions (throw error since should probably not be called with AMR)

};

//------------------------------------------------------------------------------

#endif

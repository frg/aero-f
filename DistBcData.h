#ifndef _DIST_BC_DATA_H_
#define _DIST_BC_DATA_H_

#include <Face.h>
#include <DistVector.h>

#include <map>

class IoData;
class Communicator;
class Domain;
class SubDomain;
class VarFcn;
class RotationData;
class DistGeoState;

struct SurfaceData;
struct BoundaryData;

template<int dim> class PeriodicBoundaryHandler;
template<int dim> class BcData;
template<int dim> class DistEmbeddedBcData;
template<int dim> class MatVecProd_dRdX;

//------------------------------------------------------------------------------

template<int dim>
class DistBcData {

 protected:
  std::map<int, SurfaceData *>& surfaceMap;
  std::map<int, BoundaryData *>& bcMap;

  Domain *domain;
  DistGeoState *geoState;
  DistInfo& nodeDistInfo, &faceDistInfo;

  int numLocSub;

  Communicator *com;
  SubDomain **subDomain;
  BcData<dim> **subBcData;

  VarFcn *vf;

  PeriodicBoundaryHandler<dim> *pbHandler;

  double gravity; // intensity of the gravity
  double depth;
  double ngravity[3]; // direction of the gravity

  std::vector<bool> setDeltaWF; //first entry: if extra boundary sufrace wf defined, second entry: if first iteration of simulation

  DistSVec<double, 3> Xdot;
  DistVec<double> Temp;

  DistSVec<double, Face::MaxNumNd * dim> Uface;  // boundary state for each node on each face
  DistSVec<double, dim> Unode;
  DistSVec<double, dim> Ufarin;
  DistSVec<double, dim> Ufarout;
  DistSVec<double, dim> Uporouswall;
  DistVec<bool> wallFcnActVector;

  double Uin[dim], Vin[dim];
  double Uout[dim], Vout[dim];

  DistSVec<double, dim*(dim-5)> *dUfaceSA; // jacobian of wall turbulence variable/s w.r.t primitive state
  DistSVec<double, dim*(dim-5)> *dUnodeSA; // nodal counterpart of dUfaceSA

  //---------------------- sensitivity analysis --------------------------------
  DistSVec<double, 3> *dXdot;
  DistVec<double> *dTemp;
  DistSVec<double, Face::MaxNumNd * dim> *dUface;
  DistSVec<double, dim> *dUnode;
  DistSVec<double, dim> *dUfarin;
  DistSVec<double, dim> *dUfarout;
  DistSVec<double, dim> *dUporouswall;

  double dUin[dim];
  double dUout[dim];
  //----------------------------------------------------------------------------

  void setGravity(IoData&, DistSVec<double, 3>&);
  void setBoundaryData(IoData&, DistSVec<double, 3>&);
  void resetDirectStateInlet();
  void updateDistanceToWall(IoData&, DistVec<double>&);
  void setSurfaceData(IoData&);
  void finalize(IoData&);
  void finalizeSA();

 public:
  DistBcData(IoData&, VarFcn *, Domain *, DistGeoState *);
  virtual ~DistBcData();

  void initialize(IoData&);
  void fsoInitialize();

  virtual void resize(IoData&, NodeData*);
  virtual void initializeSA(IoData&, bool, bool, bool) {}
  virtual void computeNodeValue(DistSVec<double, 3>&, DistEmbeddedBcData<dim> *) {}
  virtual void computeDerivativeOfNodeValue(DistSVec<double, 3>&, DistSVec<double, 3>&) {}
  virtual void computeDerivativeOfNodeValue(MatVecProd_dRdX<dim>&, DistGeoState&, bool) {}
  virtual void computeTransposeDerivativeOfNodeValue(MatVecProd_dRdX<dim>&, DistSVec<double, dim>&, DistGeoState&, DistSVec<double, 3>&) {}
  virtual void computeDerivativeOperatorsOfNodeValue(DistSVec<double, 3>&, MatVecProd_dRdX<dim>&) {}
  virtual void computeNodeWallValues(DistSVec<double, 3>&, DistEmbeddedBcData<dim> *) {}

  void update(IoData&, DistSVec<double, 3>&);
  void updatePeriodicBoundaries(DistSVec<double, dim>&, DistSVec<double, dim>&, DistSVec<double, dim>&, DistSVec<double, dim>&);
  void updateMultiPhaseBoundaries(DistVec<int> *);

  BcData<dim>& operator()(int i) const {
    return *subBcData[i];
  }

  DistSVec<double, 3>& getVelocityVector() {
    return Xdot;
  }
  DistVec<double>& getTemperatureVector() {
    return Temp;
  }
  DistSVec<double, dim>& getInletBoundaryVector() {
    return Ufarin;
  }
  DistSVec<double, dim>& getOutletBoundaryVector() {
    return Ufarout;
  }
  double *getInletConservativeState() {
    // note: this should not be used to enforce boundary conditions
    //       as the actual values on the inlet can vary due to gravity for example
    //       currently it is used for RANS freestream clipping
    return Uin;
  }
  double *getInletPrimitiveState() {
    // note: this should not be used to enforce boundary conditions
    //       as the actual values on the inlet can vary due to gravity for example
    //       currently it is used for initializing inactive swept nodes
    return Vin;
  }
  PeriodicBoundaryHandler<dim> *getPeriodicBcHandler() {
    return pbHandler;
  }
  DistSVec<double, 3>& getDerivativeOfVelocityVector() {
    return *dXdot;
  }

  virtual void resetBoundaryConditionsUSStandardAtmosphere(double, IoData&, DistSVec<double, 3>&) {};

};

//------------------------------------------------------------------------------

template<int dim>
class DistBcDataEuler : public DistBcData<dim> {

 public:

  DistBcDataEuler(IoData&, VarFcn *, Domain *, DistGeoState *);
  ~DistBcDataEuler() {}

  void initializeSA(IoData&, bool, bool, bool);

  void resetBoundaryConditionsUSStandardAtmosphere(double, IoData&, DistSVec<double, 3>&);

  void setDerivativeOfBoundaryConditionsGas(IoData&, DistSVec<double, 3>&, DistSVec<double, 3>&,
                                            bool, bool, bool);

};

//------------------------------------------------------------------------------

class DistBcDataSA : public DistBcDataEuler<6> {

  static const int dim = 6;

  DistSVec<double, 2> *tmp;
  DistSVec<double, 1> *dnormsa;
  DistSVec<double, dim> *dtmpsa;

 public:

  DistBcDataSA(IoData&, VarFcn *, Domain *, DistGeoState *);
  ~DistBcDataSA();

  void resize(IoData&, NodeData*);

  void computeNodeValue(DistSVec<double, 3>&, DistEmbeddedBcData<dim> *);

  void computeDerivativeOfNodeValue(DistSVec<double, 3>&, DistSVec<double, 3>&);

  void computeDerivativeOfNodeValue(MatVecProd_dRdX<dim>&, DistGeoState&, bool);

  void computeTransposeDerivativeOfNodeValue(MatVecProd_dRdX<dim>&, DistSVec<double, dim>&, DistGeoState&, DistSVec<double, 3>&);

  void computeDerivativeOperatorsOfNodeValue(DistSVec<double, 3>&, MatVecProd_dRdX<dim>&);

  void computeNodeWallValues(DistSVec<double, 3>&, DistEmbeddedBcData<dim> *);

};

//------------------------------------------------------------------------------

class DistBcDataKE : public DistBcDataEuler<7> {

  static const int dim = 7;

  DistSVec<double, 3> *tmp;
  DistSVec<double, 1> *dnormke;
  DistSVec<double, 2*dim> *dtmpke;

 public:

  DistBcDataKE(IoData&, VarFcn *, Domain *, DistGeoState *);
  ~DistBcDataKE();

  void resize(IoData&, NodeData*);

  void computeNodeValue(DistSVec<double, 3>&, DistEmbeddedBcData<dim> *);

  void computeDerivativeOfNodeValue(DistSVec<double, 3>&, DistSVec<double, 3>&);

  void computeDerivativeOfNodeValue(MatVecProd_dRdX<dim>&, DistGeoState&, bool);

  void computeTransposeDerivativeOfNodeValue(MatVecProd_dRdX<dim>&, DistSVec<double, dim>&, DistGeoState&, DistSVec<double, 3>&);

  void computeDerivativeOperatorsOfNodeValue(DistSVec<double, 3>&, MatVecProd_dRdX<dim>&);

  void computeNodeWallValues(DistSVec<double, 3>&, DistEmbeddedBcData<dim> *);

};

//------------------------------------------------------------------------------

#endif

#ifndef _SUBDOMAIN_H_
#define _SUBDOMAIN_H_

#include <IoData.h>
#include <Node.h>
#include <Edge.h>
#include <Face.h>
#include <Elem.h>
#include <PostFcn.h>
#include <DistInfo.h>
#include <ErrorHandler.h>
#include <NodeData.h>
#include <NodePatch.h>
#include <Types.h>

#include <functional>
#include <map>
#include <set>
#include <vector>
#include <iostream>

class BCondSet;
class VarFcn;
class BcFcn;
class RecFcn;
class EdgeGrad;
class FluxFcn;
class FemEquationTerm;
class MacroCellSet;
class LESTerm;
class DynamicLESTerm;
class SubDTopo;
class TimeData;
class GeoSource;
class GeoState;
class Connectivity;
class Communicator;
class SubMat;
class SubDiagPrec;
class BinFileHandler;
class BCData;
class VolumicForceTerm;
class SpatialLowMachPrec;
class TimeLowMachPrec;
class LevelSetStructure;
class WallFcn;
class MatchNodeSet;
class BCApplier;

struct V6NodeData;
struct Vec3D;
struct Vec3DRTree;

template<int dim, class Scalar> class NodalGrad;
template<int dim> class BcData;
template<class Scalar> class Vec;
template<class Scalar, int dim> class SVec;
template<class Scalar, int neq> class MvpMat;
template<class Scalar, int neq> class SparseMat;
template<class T> class CommPattern;
template<class T> class RTree;
template<int dim> class EmbeddedBcData;
template<int dim> class GhostPoint;
template<int dim> class RestartState;
template<int dim> class LESData;
template<class Scalar> class DenseMat;
template<class Scalar, int dim, int dim2> class RectangularSparseMat;
template<class MatrixType> class DistMat;

template<int dim> struct dRdXoperators;

//------------------------------------------------------------------------------

struct EdgeDef {

  EdgeDef() {}
  EdgeDef(int_t glLeft, int_t glRight, int edgeNum) : glLeft(glLeft), glRight(glRight), edgeNum(edgeNum) { order(); }
  EdgeDef(const EdgeDef& oth) : glLeft(oth.glLeft), glRight(oth.glRight), edgeNum(oth.edgeNum), sign(oth.sign) {}

  int_t glLeft, glRight;
  int edgeNum, sign;

  bool operator<(const EdgeDef& e) const {
    return ((glLeft < e.glLeft) || (glLeft == e.glLeft && glRight < e.glRight));
  }

  void order() {
    if(glLeft < glRight) {
      sign = 1;
    }
    else {
      int_t tmp = glRight;
      glRight = glLeft;
      glLeft = tmp;
      sign = -1;
    }
  }
};

//------------------------------------------------------------------------------
/*
 * \brief SubDomain data
 */
class SubDomain {

  int locSubNum;
  int clusSubNum;
  int globSubNum;
  int numClusNodes;
  int numNodeRanges;

  char suffix[100];

  NodeSet& nodes;
  EdgeSet  edges;
  FaceSet& faces;
  ElemSet& elems;

  std::vector<int_t> locToGlobNodeMap;
  std::vector<int_t> locToGlobFaceMap;
  std::vector<int_t> locToGlobElemMap;

  int numNeighb;
  std::vector<int> neighb;
  std::vector<int> sndChannel;
  std::vector<int> rcvChannel;
  std::vector<std::vector<int>> sharedNodes;
  std::vector<std::vector<EdgeDef>> sharedEdges;
#ifdef CONSISTENT_ORDERING
  std::vector<int> allSharedNodes;
#endif
  int (*nodeRanges)[3];
  std::vector<int> offWallNode;

  BCondSet *mmsBCs;

  Connectivity *NodeToNode;
  Connectivity *NodeToElem;
  Connectivity *ElemToNode;
  Connectivity *ElemToElem;
  Connectivity *NodeToFace;
  Connectivity *FaceToNode;
  Connectivity *NodeToFacet;
  Connectivity *FacetToNode;
  Connectivity *FaceToFacet;
  Connectivity *NodeToEdge;
  Connectivity *EdgeToNode;
  Connectivity *NodeToSubD;

  // for nonlinear HROM restriction
  std::vector<bool> sampleNodeFlag;
  std::vector<int> sampleNodes;
  std::vector<bool> sampleGradientNodeFlag;
  std::vector<int> sampleGradientNodes;
  std::vector<int> sampleEdges;
  std::vector<int> sampleFaces;
  std::vector<int> sampleElems;

  RTree<Elem> *myTree;
  RTree<Vec3DRTree> *nodeTree;
  std::vector<Vec3DRTree *> nodeTreeData;

  ErrorHandler *errorHandler;

 public:
  SubDomain(int, int, int, int, char *, NodeSet *, FaceSet *, ElemSet *,
            int, int *, Connectivity *, std::vector<int_t>&, std::vector<int_t>&,
            std::vector<int_t>&, int, int (*)[3]);
  ~SubDomain();

  void resize();

  // topology
  std::vector<int_t> & getNodeMap() {
    return locToGlobNodeMap;
  }
  std::vector<int_t> & getFaceMap() {
    return locToGlobFaceMap;
  }
  std::vector<int_t> & getElemMap() {
    return locToGlobElemMap;
  }
  enum Tag { NODE, FACE, ELEM };
  std::vector<int_t> & getLocToGlobMap(Tag t) {
    switch(t) {
      default:
      case NODE: return locToGlobNodeMap;
      case FACE: return locToGlobFaceMap;
      case ELEM: return locToGlobElemMap;
    }
  }

  // member access
  int getGlobSubNum() {
    return globSubNum;
  }
  int getLocSubNum() {
    return locSubNum;
  }
  int getNumNeighb() {
    return numNeighb;
  }
  int &getNumNeighbRef() {
    return numNeighb;
  }
  std::vector<int>& getNeighb() {
    return neighb;
  }
  std::vector<int>& getSndChannel() {
    return sndChannel;
  }
  std::vector<int>& getRcvChannel() {
    return rcvChannel;
  }
  std::vector<std::vector<int>>& getSharedNodes() {
    return sharedNodes;
  }
#ifdef CONSISTENT_ORDERING
  std::vector<int>& getAllSharedNodes() {
    return allSharedNodes;
  }
#endif
  std::vector<std::vector<EdgeDef>>& getSharedEdges() {
    return sharedEdges;
  }
  int numNodes() {
    return(nodes.size());
  }
  int numFaces() {
    return(faces.size());
  }
  int numElems() {
    return(elems.size());
  }
  int numEdges() {
    return(edges.size());
  }
  FaceSet& getFaces() {
    return faces;
  }
  ElemSet& getElems() {
    return elems;
  }
  NodeSet& getNodes() {
    return nodes;
  }
  EdgeSet& getEdges() {
    return edges;
  }
  RTree<Elem> *getMyTree() {
    return myTree;
  }
  RTree<Vec3DRTree> *getNodeTree() {
    return nodeTree;
  }
  int *getElemNodeNum(int i) {
    return(elems[i].nodeNum());
  }
  void assignErrorHandler(ErrorHandler *in) {
    errorHandler = in;
  }
  std::vector<bool>& getSampleNodeFlag() {
    return sampleNodeFlag;
  }
  std::vector<int>& getSampleNodes() {
    return sampleNodes;
  }
  std::vector<bool>& getSampleGradientNodeFlag() {
    return sampleGradientNodeFlag;
  }
  std::vector<int>& getSampleGradientNodes() {
    return sampleGradientNodes;
  }
  std::vector<int>& getSampleEdges() {
    return sampleEdges;
  }
  std::vector<int>& getSampleFaces() {
    return sampleFaces;
  }
  std::vector<int>& getSampleElems() {
    return sampleElems;
  }
  void delSupermeshTags() {
    for(std::vector<Elem*>::iterator it = elems.begin(); it != elems.end(); ++it) {
      (*it)->delSupermeshTag();
    }
  }

  Connectivity *createElemBasedConnectivity();
  Connectivity *createNodeToElementConnectivity();
  Connectivity *createElementToElementConnectivity();
  Connectivity *createElementToNodeConnectivity();
  Connectivity *createNodeToFaceConnectivity();
  Connectivity *createFaceToNodeConnectivity();
  Connectivity *createNodeToFacetConnectivity();
  Connectivity *createFacetToNodeConnectivity();
  Connectivity *createFaceToFacetConnectivity();
  Connectivity *createNodeToEdgeConnectivity();
  Connectivity *createEdgeToNodeConnectivity();
  Connectivity *createEdgeBasedConnectivity();
  Connectivity *createNodeToSubDomainConnectivity();
  Connectivity *createFaceToConstantConnectivity();
  Connectivity *createFaceToSelfConnectivity();
  Connectivity *createNodeToConstantConnectivity();
  Connectivity *createConstantToNodeConnectivity();
  Connectivity *createConstantToMovingWallNodeConnectivity();
  Connectivity *createConstantToConstantConnectivity();
  Connectivity *createElementBasedEdgeToNodeConnectivity();
  Connectivity *createElementBasedNodeToEdgeConnectivity();

  Connectivity *getNodeToNode() { if(!NodeToNode) NodeToNode = createEdgeBasedConnectivity();  return NodeToNode; }
  Connectivity *getNodeToSubD() { if(!NodeToSubD) NodeToSubD = createNodeToSubDomainConnectivity();  return NodeToSubD; }
  Connectivity *getNodeToFace() { if(!NodeToFace) NodeToFace = createNodeToFaceConnectivity();  return NodeToFace; }
  Connectivity *getNodeToFacet() { if(!NodeToFacet) NodeToFacet = createNodeToFacetConnectivity();  return NodeToFacet; }
  Connectivity *getNodeToEdge() { if(!NodeToEdge) NodeToEdge = createNodeToEdgeConnectivity();  return NodeToEdge; }
  Connectivity *getNodeToElem() { if(!NodeToElem) NodeToElem = createNodeToElementConnectivity();  return NodeToElem; }
  Connectivity *getElemToNode() { if(!ElemToNode) ElemToNode = createElementToNodeConnectivity();  return ElemToNode; }
  Connectivity *getElemToElem() { if(!ElemToElem) ElemToElem = createElementToElementConnectivity();  return ElemToElem; }
  Connectivity *getFaceToNode() { if(!FaceToNode) FaceToNode = createFaceToNodeConnectivity();  return FaceToNode; }
  Connectivity *getFacetToNode() { if(!FacetToNode) FacetToNode = createFacetToNodeConnectivity();  return FacetToNode; }
  Connectivity *getFaceToFacet() { if(!FaceToFacet) FaceToFacet = createFaceToFacetConnectivity();  return FaceToFacet; }
  Connectivity *getEdgeToNode() { if(!EdgeToNode) EdgeToNode = createEdgeToNodeConnectivity();  return EdgeToNode; }

  void deleteNodeToElem();
  void deleteElemToNode();
  void deleteNodeToFace();
  void deleteNodeToFacet();
  void deleteFaceToNode();
  void deleteFacetToNode();
  void deleteFaceToFacet();
  void deleteNodeToEdge();
  void deleteEdgeToNode();

  // geometry
  void constructRTree(IoData&, GeoState * = nullptr);
  void constructNodeRTree(IoData&, double);
  void reconstructRTree(GeoState * = nullptr);
  int numberEdges();
  double minEdgeLength();
  void setFaceType(int *);
  void setNodeType(int *, CommPattern<int>&, Vec<int>&);
  void completeNodeType(int *, CommPattern<int>&, Vec<int>&);
  void computeOffWallNode(LevelSetStructure *, CommPattern<int>&, bool = false);
  void completeOffWallNode(CommPattern<int>&);
  int setFaceToElementConnectivity();
  void getElementStatistics(int&, int&, int&, int&);
  void getMaxElementStatistics(int_t&, int_t&, int_t&);
  int computeControlVolumes(int, double, Vec<double>&);
  int computeControlVolumes(int, double, SVec<double, 3>&, Vec<double>&);
  void computeDerivativeOfControlVolumes(int, double, SVec<double, 3>&, SVec<double, 3>&, Vec<double>&);
  void computeDerivativeOfControlVolumes(RectangularSparseMat<double, 3, 1>&, SVec<double, 3>&, Vec<double>&);
  void computeTransposeDerivativeOfControlVolumes(RectangularSparseMat<double, 3, 1>&, Vec<double>&, SVec<double, 3>&);
  void computeDerivativeOperatorsOfControlVolumes(SVec<double, 3>&, RectangularSparseMat<double, 3, 1>&);
  void computeFaceEdgeNormals(SVec<double, 6>&);
  void computeEdgeDihedralAngle(double, SVec<double, 6>&, SVec<double, 5>&, Vec<int>&);
  void otRcvData(CommPattern<double>&, SVec<double, 5>&);
  void propagateInfoAlongEdges(double, SVec<double, 5>&);
  void computeNormalsConfig(SVec<double, 3>&, SVec<double, 3>&, Vec<Vec3D>&,
                            Vec<double>&, Vec<Vec3D>&, Vec<double>&);
  void computeDerivativeOfNormals(SVec<double, 3>&, SVec<double, 3>&, Vec<Vec3D>&, Vec<Vec3D>&, Vec<double>&,
                                  Vec<double>&, Vec<Vec3D>&, Vec<Vec3D>&, Vec<double>&, Vec<double>&);
  void computeDerivativeOfNormals(RectangularSparseMat<double, 3, 3>&, RectangularSparseMat<double, 3, 3>&,
                                  SVec<double, 3>&, Vec<Vec3D>&, Vec<Vec3D>&);
  void computeTransposeDerivativeOfNormals(RectangularSparseMat<double, 3, 3>&, RectangularSparseMat<double, 3, 3>&,
                                           Vec<Vec3D>&, Vec<Vec3D>&, SVec<double, 3>&);
  void computeDerivativeOperatorsOfNormals(SVec<double, 3>&, RectangularSparseMat<double, 3, 3>&,
                                           RectangularSparseMat<double, 3, 3>&);
  void computeNormalsGCL1(SVec<double, 3>&, SVec<double, 3>&, SVec<double, 3>&,
                          Vec<Vec3D>&, Vec<double>&, Vec<Vec3D>&, Vec<double>&);
  void computeNormalsEZGCL1(double, SVec<double, 3>&, SVec<double, 3>&, Vec<Vec3D>&,
                            Vec<double>&, Vec<Vec3D>&, Vec<double>&);

  // Get the local node number in the subdomain of the global node <id>
  // Returns -1 if it does not exist.  Warning: This method is O(N)
  int getLocalNodeNum(int) const;

  void finalizeTags(SVec<bool, 2>&);

  void computeSmoothedSensor(SVec<double, 3>&, Vec<double>&, SVec<double, 3>&);

  template<int dim>
  void computeWeightsLeastSquaresEdgePart(SVec<double, 3>&, SVec<double, 6>&, Vec<int>&,
                                          EmbeddedBcData<dim> * = nullptr, int = 0, Vec<int> * = nullptr, SVec<double, 2> * = nullptr,
                                          std::function<double(int,int,double[3],int)> = [](int,int,double[3],int){return 1.;});

  void computeDerivativeOfWeightsLeastSquaresEdgePart(SVec<double, 3>&, SVec<double, 3>&, SVec<double, 6>&, SVec<double, 6>&);

  void computeDerivativeOfWeightsLeastSquaresEdgePart(RectangularSparseMat<double, 3, 6>&, SVec<double, 3>&, SVec<double, 6>&);

  void computeTransposeDerivativeOfWeightsLeastSquaresEdgePart(RectangularSparseMat<double, 3, 6>&, SVec<double, 6>&, SVec<double, 3>&);

  void computeWeightsLeastSquaresNodePart(SVec<double, 6>&, const Vec<int>&, int, bool = true);

  void computeDerivativeOfWeightsLeastSquaresNodePart(SVec<double, 6>&, SVec<double, 6>&);

  void computeDerivativeOfWeightsLeastSquaresNodePart(RectangularSparseMat<double, 6, 6>&, SVec<double, 6>&);

  void computeTransposeDerivativeOfWeightsLeastSquaresNodePart(RectangularSparseMat<double, 6, 6>&, SVec<double, 6>&);

  void compute_dRdX(SVec<double, 3>&, RectangularSparseMat<double, 3, 6>&);

  void compute_dRdR(SVec<double, 6>&, RectangularSparseMat<double, 6, 6>&);

  template<int dim>
  void computeWeightsLeastSquaresEdgePart2(SVec<double, 3>&, const SVec<double, 6>&, SVec<double, 3>&, SVec<double, 3>&, EmbeddedBcData<dim> *,
                                           int, Vec<int> * = nullptr, SVec<double, 2> * = nullptr);

  void computeDerivativeOfWeightsLeastSquaresEdgePart2(SVec<double, 3>&, SVec<double, 3>&, SVec<double, 6>&,
                                                       SVec<double, 6>&, SVec<double, 3>&, SVec<double, 3>&, SVec<double, 3>&, SVec<double, 3>&);

  void computeWeightsGalerkin(SVec<double, 3>&, SVec<double, 3>&, SVec<double, 3>&, SVec<double, 3>&);

  void computeDerivativeOfWeightsGalerkin(SVec<double, 3>&, SVec<double, 3>&, SVec<double, 3>&, SVec<double, 3>&,
                                          SVec<double, 3>&);

  bool findTetrahedron(SVec<double, 3>&, int, int, Vec3D, Vec3D, double, V6NodeData&, bool);
  Connectivity *agglomerate(Connectivity&, int, bool *, Vec<int> *);
  MacroCellSet *agglomerateMesh(int, int, bool *, Vec<int>&);

  void findNodeBoundingBoxes(SVec<double, 3>&, SVec<double, 3>&, SVec<double, 3>&, double * = 0);
  void enlargeNodeBoundingBoxes(SVec<double, 3>&, SVec<double, 3>&);
  void pseudoFastMarchingMethod(Vec<int>&, SVec<double, 3>&, Vec<double>&, int, Vec<int>&, int&, int&, LevelSetStructure&,
                                bool, int, const double, bool, Vec<double> * = nullptr, SVec<std::pair<int, double>, 3> * = nullptr, bool = false);
  void populatedd2WallOperators(Vec<int>&, SVec<double, 3>&, Vec<double>&, LevelSetStructure&, bool, int,
                                const double, SVec<std::pair<int, double>, 3> *, bool = false);

  void changeSurfaceType(std::map<int, SurfaceData *>&, std::map<int, BoundaryData *>&);
  void markFaceBelongsToSurface(Vec<int>&, CommPattern<int>&);
  void completeFaceBelongsToSurface(Vec<int>&, Vec<double>&, std::map<int, SurfaceData *>&, CommPattern<int>&);
  bool isINodeinITet(SVec<double, 3>&, const Vec3D&, int);

  // reduced mesh masking for HPROMs
  void setupSampleMasks();
  void computeH2Mask(Vec<int>&);

  // spatial discretization
  template<int dim>
  void computeTimeStep(FemEquationTerm *, VarFcn *, GeoState&, SVec<double, dim>&, Vec<double>&, Vec<double>&,
                       Vec<double>&, Vec<double>&, double, Vec<double>&, TimeLowMachPrec&, SpatialLowMachPrec&,
                       double, EmbeddedBcData<dim> *);

  template<int dim>
  void computeDerivativeOfTimeStep(FemEquationTerm *, VarFcn *, GeoState&, SVec<double, dim>&, SVec<double, dim>&,
                                   Vec<double>&, Vec<double>&, double, double, TimeLowMachPrec&, SpatialLowMachPrec&,
                                   EmbeddedBcData<dim> *);

  template<int Dim, class Scalar, int dim>
  void computeGradientsLeastSquares(SVec<double, 3>&, SVec<double, 3>&, SVec<Scalar, Dim>&,
                                    SVec<Scalar, Dim>&, SVec<Scalar, Dim>&, SVec<Scalar, Dim>&,
                                    EmbeddedBcData<dim> *, int, Vec<int> *, SVec<double, 2> * = nullptr,
                                    Vec<bool> * = nullptr);

  template<int Dim, int dim>
  void computeDerivativeOfGradientsLeastSquares(SVec<double, 3>&, SVec<double, 3>&, SVec<double, 3>&, SVec<double, 3>&,
                                                SVec<double, Dim>&, SVec<double, Dim>&, SVec<double, Dim>&,
                                                SVec<double, Dim>&, SVec<double, Dim>&,
                                                EmbeddedBcData<dim> *, SVec<double, 2> * = nullptr);

  template<int Dim>
  void computeDerivativeOfGradientsLeastSquares(dRdXoperators<Dim>&, SVec<double, 3>&, SVec<double, 6>&, SVec<double, Dim>&,
                                                SVec<double, Dim>&, SVec<double, Dim>&, SVec<double, Dim>&);

  template<int Dim>
  void computeTransposeDerivativeOfGradientsLeastSquares(dRdXoperators<Dim>&, SVec<double, Dim>&, SVec<double, Dim>&,
                                                         SVec<double, Dim>&, SVec<double, 3>&, SVec<double, 6>&,
                                                         SVec<double, Dim>&);

  template<int Dim>
  void computeDerivativeOperatorsOfGradientsLeastSquares(SVec<double, 3>&, SVec<double, 6>&, SVec<double, Dim>&,
                                                         dRdXoperators<Dim>&);

  template<int Dim, class Scalar>
  void computeGradientsGalerkin(Vec<double>&, SVec<double, 3>&, SVec<double, 3>&,
                                SVec<double, 3>&, SVec<Scalar, Dim>&, SVec<Scalar, Dim>&,
                                SVec<Scalar, Dim>&, SVec<Scalar, Dim>&, Vec<bool> * = nullptr);

  template<int Dim>
  void computeDerivativeOfGradientsGalerkin(Vec<double>&, Vec<double>&, SVec<double, 3>&, SVec<double, 3>&,
                                            SVec<double, 3>&, SVec<double, 3>&, SVec<double, 3>&, SVec<double, 3>&,
                                            SVec<double, Dim>&, SVec<double, Dim>&, SVec<double, Dim>&,
                                            SVec<double, Dim>&, SVec<double, Dim>&);

  template<int Dim, int dim>
  void computeMinMaxStencilValues(SVec<double, Dim>&, SVec<double, Dim>&, SVec<double, Dim>&, EmbeddedBcData<dim> *);

  template<int Dim>
  void computeDerivativeOfMinMaxStencilValues(SVec<double, Dim>&, SVec<double, Dim>&, SVec<double, Dim>&,
                                              SVec<double, Dim>&, SVec<double, Dim>&, SVec<double, Dim>&);

  template<int Dim>
  void computeMaxStencilValue(SVec<double, Dim>&, Vec<double>&, VarFcn *);

  template<int Dim>
  void computeSecondLimiter(SVec<double, Dim>&, Vec<double>&);

  template<int Dim, int _dim>
  void computeMultiDimLimiter_Step1(std::function<double(double&, double&, double&, double&, double&)>,
                                    RecFcn *, SVec<double, 3>&, Vec<double>&, SVec<double, Dim>&, NodalGrad<Dim, double>&,
                                    CommPattern<double>&, CommPattern<int_t>&, CommPattern<double>&, EmbeddedBcData<_dim> *);

  template<int Dim>
  void computeMultiDimLimiter_Step2(NodalGrad<Dim, double>&, CommPattern<double>&, CommPattern<int_t>&, CommPattern<double>&);

  template<int Dim>
  void computeJacobianMultiDimLimiter_Step1(NodalGrad<Dim, double>&, SVec<double, 3>&);

  template<int Dim>
  void computeJacobianMultiDimLimiter_Step2(std::function<double(double&, double&, double&, double&, double&)>,
                                            std::function<double(double&, double&, double&, double&, double&, double&, double&, double&, double&, double&)>,
                                            RecFcn *, SVec<double, 3>&, Vec<double>&, SVec<double, Dim>&, SVec<double, 3>&, NodalGrad<Dim, double>&);

  template<int Dim>
  void computeDerivativeOfMultiDimLimiter(std::function<double(double&, double&, double&, double&, double&)>,
                                          std::function<double(double&, double&, double&, double&, double&, double&, double&, double&, double&, double&)>,
                                          RecFcn *, SVec<double, 3>&, SVec<double, 3>&, Vec<double>&, Vec<double>&,
                                          SVec<double, Dim>&, SVec<double, Dim>&, SVec<double, Dim>&, SVec<double, Dim>&,
                                          SVec<double, Dim>&, SVec<double, Dim>&, SVec<double, Dim>&, SVec<double, Dim>&,
                                          SVec<double, Dim>&, SVec<double, Dim>&, SVec<double, Dim>&, SVec<double, Dim>&,
                                          SVec<double, Dim>&, SVec<double, Dim>&);

  template<int Dim>
  void computeWBAPLimiter_Step1(double, SVec<double, Dim>&, SVec<double, Dim>&, SVec<double, Dim>&, SVec<double, Dim>&,
                                SVec<double, Dim>&, SVec<double, Dim>&, SVec<double, Dim>&, SVec<double, Dim>&,
                                SVec<double, Dim>&);

  template<int Dim>
  void computeWBAPLimiter_Step2(double, SVec<double, Dim>&, SVec<double, Dim>&, SVec<double, Dim>&, SVec<double, Dim>&,
                                SVec<double, Dim>&, SVec<double, Dim>&, SVec<double, Dim>&, SVec<double, Dim>&,
                                SVec<double, Dim>&);

  template<int Dim>
  void computePressureSensor(SVec<double, 3>&, SVec<double, Dim>&, SVec<double, Dim>&, SVec<double, Dim>&,
                             SVec<double, Dim>&, SVec<double, 3>&);

  template<int dim>
  void applyControlVolumesToResidual(TsData::Form, Vec<double>&, SVec<double, dim>&);

  template<int dim>
  void applyControlVolumesToDerivativeOfResidual(TsData::Form, Vec<double>&, Vec<double>&, SVec<double, dim>&,
                                                 SVec<double, dim>&);

  template<class Scalar, int neq>
  void applyControlVolumesToJacobian(TsData::Form, Vec<double>&, MvpMat<Scalar, neq>&);

  template<int dim, class Scalar>
  void multiplyBydVdU(VarFcn *, SVec<double, dim>&, SVec<Scalar, dim>&, SVec<Scalar, dim>&,
                      Vec<int> *, Vec<bool> * = nullptr);

  template<int dim>
  void computeRiemannAverages(GeoState&, SVec<double, dim>&, EmbeddedBcData<dim> *, Vec<int>&);

  template<int dim>
  void assembleRiemannAverages_Step1(EmbeddedBcData<dim> *, CommPattern<double>&, Vec<int>&);

  template<int dim>
  void assembleRiemannAverages_Step2(GeoState&, SVec<double, dim>&, EmbeddedBcData<dim> *,
                                     CommPattern<double>&, Vec<int>&);

  template<int dim>
  void assembleRiemannAverages_Step3(GeoState&, EmbeddedBcData<dim> *, CommPattern<double>&);

  template<int dim>
  void computeGlobalNeighbors(GeoState&, SVec<double, dim>&, EmbeddedBcData<dim> *);

  template<int dim>
  void assembleGlobalNeighbors_Step1(EmbeddedBcData<dim> *, CommPattern<double>&);

  template<int dim>
  void assembleGlobalNeighbors_Step2(GeoState&, SVec<double, dim>&, EmbeddedBcData<dim> *,
                                     CommPattern<double>&);

  template<int dim>
  void assembleGlobalNeighbors_Step3(GeoState&, EmbeddedBcData<dim> *, CommPattern<double>&);

  template<int dim>
  int computeFiniteVolumeTerm(Vec<double>&, FluxFcn **, RecFcn *, BcData<dim>&, GeoState&,
                              SVec<double, dim>&, NodalGrad<dim, double>&, EdgeGrad *,
                              NodalGrad<dimLS, double> *, SVec<double, dim>&, SVec<bool, 2>&,
                              int, int, EmbeddedBcData<dim> *, Vec<bool> * = nullptr);

  template<int dim>
  void computeDerivativeOfFiniteVolumeTerm(Vec<double>&, Vec<double>&, FluxFcn **, RecFcn *, BcData<dim>&,
                                           GeoState&, SVec<double, dim>&, SVec<double, dim>&, NodalGrad<dim, double>&,
                                           EdgeGrad *, NodalGrad<dimLS, double> *, double, SVec<double, dim>&, int,
                                           EmbeddedBcData<dim> *);

  template<int dim>
  void computeDerivativeOfFiniteVolumeTerm(dRdXoperators<dim>&, BcData<dim>&, GeoState&, NodalGrad<dim, double>&,
                                           Vec<double>&, SVec<double, dim>&, SVec<double, dim>&);

  template<int dim>
  void computeTransposeDerivativeOfFiniteVolumeTerm(dRdXoperators<dim>&, BcData<dim>&, GeoState&, SVec<double, dim>&,
                                                    NodalGrad<dim, double>&, SVec<double, 3>&);

  template<int dim>
  void computeDerivativeOperatorsOfFiniteVolumeTerm(FluxFcn **, RecFcn *, BcData<dim>&, GeoState&, SVec<double, dim>&,
                                                    NodalGrad<dim, double>&, EmbeddedBcData<dim> *, dRdXoperators<dim>&);

  template<int dim>
  void computeFiniteVolumeTermLS(RecFcn *, RecFcn *, GeoState&, SVec<double, dim>&,
                                 NodalGrad<dim, double>&, EdgeGrad *, NodalGrad<dimLS, double>&,
                                 EdgeGrad *, SVec<double, dimLS>&,
                                 SVec<double, dimLS>&, EmbeddedBcData<dim> *);

  template<int dim, class Scalar, int neq>
  void computeJacobianFiniteVolumeTerm(Vec<double> *, FluxFcn **, RecFcn *, BcData<dim>&,
                                       GeoState&, SVec<double, dim>&, NodalGrad<dim, double>&, EdgeGrad *,
                                       NodalGrad<dimLS, double> *, MvpMat<Scalar, neq>&, int,
                                       EmbeddedBcData<dim> *, bool *);

  template<int dim, class Scalar>
  void computeJacobianFiniteVolumeTermLS(RecFcn *, RecFcn *, GeoState&, SVec<double, dim>&,
                                         NodalGrad<dim, double>&, EdgeGrad *, NodalGrad<dimLS, double>&,
                                         EdgeGrad *, SVec<double, dimLS>&,
                                         MvpMat<Scalar, dimLS>&, EmbeddedBcData<dim> *, bool *);

  template<int dim>
  void computeGalerkinTerm(FemEquationTerm *, VarFcn *, LESData<dim> *, BcData<dim>&, GeoState&,
                           SVec<double, dim>&, SVec<double, dim>&, EmbeddedBcData<dim> *);

  template<int dim>
  void computeDerivativeOfGalerkinTerm(FemEquationTerm *, VarFcn *, LESData<dim> *, BcData<dim>&,
                                       GeoState&, SVec<double, dim>&, SVec<double, dim>&,
                                       double, SVec<double, dim>&, EmbeddedBcData<dim> *);

  template<int dim>
  void computeDerivativeOfGalerkinTerm(dRdXoperators<dim>&, GeoState&, bool, SVec<double, dim>&);

  template<int dim>
  void computeTransposeDerivativeOfGalerkinTerm(dRdXoperators<dim>&, GeoState&, SVec<double, dim>&, SVec<double, 3>&);

  template<int dim>
  void computeDerivativeOperatorsOfGalerkinTerm(FemEquationTerm *, BcData<dim>&, GeoState&, SVec<double, dim>&,
                                                EmbeddedBcData<dim> *, dRdXoperators<dim>&);

  template<int dim, class Scalar, int neq>
  void computeJacobianGalerkinTerm(FemEquationTerm *, VarFcn *, LESData<dim> *, BcData<dim>&,
                                   GeoState&, SVec<double, dim>&, MvpMat<Scalar, neq>&,
                                   EmbeddedBcData<dim> *);

  template<int dim>
  void computeMaxwellSlipTerm(FemEquationTerm *, BcData<dim>&, GeoState&, SVec<double, dim>&, SVec<double, dim>&);

  template<int dim>
  void computeVolumicForceTerm(VolumicForceTerm *, Vec<double>&,
                               SVec<double, dim>&, SVec<double, dim>&, bool *);

  template<int dim>
  void computeDerivativeOfVolumicForceTerm(VolumicForceTerm *, Vec<double>&, Vec<double>&,
                                           SVec<double, dim>&, SVec<double, dim>&, SVec<double, dim>&, bool *);

  template<int dim, class Scalar, int neq>
  void computeJacobianVolumicForceTerm(VolumicForceTerm *, Vec<double>&,
                                       SVec<double, dim>&, MvpMat<Scalar, neq>&, bool *);

  template<int dim, class Scalar, int neq>
  void populateRecJacobian(VarFcn *, RecFcn *, NodalGrad<dim, double>&, EdgeGrad *,
                           Vec<double>&, SVec<double, dim>&, MvpMat<Scalar, neq>&, EmbeddedBcData<dim> *);

  template<int dim>
  void precomputeRec(RecFcn *, SVec<double, 3>&, NodalGrad<dim, double>&, EdgeGrad *,
                     SVec<double, dim>&, SVec<double, 4 * dim>&);

  template<int dim, class Scalar, int neq, class Scalar2>
  void computeMatVecProdH2(TsData::Form, RecFcn *, SVec<double, 3>&, Vec<double>&,
                           NodalGrad<dim, Scalar2>&, EdgeGrad *,
                           SVec<double, 4 * dim>&, MvpMat<Scalar, neq>&,
                           SVec<Scalar2, dim>&, SVec<Scalar2, neq>&,
                           Vec<bool> * = nullptr);

  template<int dim>
  int checkSolution(VarFcn *, SVec<double, dim>&, EmbeddedBcData<dim> *, int&);

  template<int dim>
  void checkFailSafe(VarFcn *, SVec<double, dim>&, SVec<bool, 2>&, Vec<int> *);

  template<int dim, int neq>
  int clipSolution(TsData::Clipping, BcsWallData::Integration, VarFcn *,
                   double *, bool *, SVec<double, dim>&, int *, int_t *, double *, Vec<int>&, EmbeddedBcData<dim> *);

  template<int dim>
  void getGradP(NodalGrad<dim, double>&, SVec<double, 3>&);

  template<int dim>
  void getDerivativeOfGradP(NodalGrad<dim, double>&, SVec<double, 3>&);

  template<int dim>
  void getDerivativeOfGradP(dRdXoperators<dim>&, NodalGrad<dim, double>&, SVec<double, 3>&);

  template<int dim>
  void getTransposeDerivativeOfGradP(dRdXoperators<dim>&, SVec<double, 3>&, NodalGrad<dim, double>&);

  template<int dim>
  void computeDerivativeOperatorOfGradP(dRdXoperators<dim>&);

  template<int dim>
  void computeDistanceErrorMetric(FemEquationTerm *, GeoState&, SVec<double, dim>&,
                                  Vec<double>&, EmbeddedBcData<dim> *);

  // boundary conditions
  template<int dim>
  void setupUVolumesInitialConditions_Step1(const int, double[dim], SVec<double, dim>&, CommPattern<double>&);

  template<int dim>
  void setupUVolumesInitialConditions_Step2(CommPattern<double>&, SVec<double, dim>&);

  void setupPhiVolumesInitialConditions_Step1(const int, int, SVec<double, dimLS>&, CommPattern<double>&);

  void setupPhiVolumesInitialConditions_Step2(CommPattern<double>&, SVec<double, dimLS>&);

  template<int dim>
  void assignFreeStreamValues2(SVec<double, dim>&, SVec<double, dim>&, SVec<double, Face::MaxNumNd * dim>&);

  template<int dim>
  void assignPorousWallValues(SVec<double, dim>&, SVec<double, Face::MaxNumNd * dim>&);

  template<int dim>
  void setNodeBcValue2(double *, double *, SVec<double, dim>&, Vec<int>&);

  template<int dim>
  void computeFaceBcValue(SVec<double, dim>&, SVec<double, Face::MaxNumNd * dim>&);

  template<int dim1, int dim2>
  void computeNodeBcValue(SVec<double, 3>&, SVec<double, Face::MaxNumNd * dim1>&, SVec<double, dim2>&, EmbeddedBcData<dim1> *);

  template<int dim1, int dim2>
  void computeDerivativeOfNodeBcValue(SVec<double, 3>&, SVec<double, 3>&, SVec<double, Face::MaxNumNd * dim1>&,
                                      SVec<double, Face::MaxNumNd * dim1>&, SVec<double, dim2>&);

  template<int dim1, int dim2>
  void computeDerivativeOfNodeBcValue(dRdXoperators<dim1>&, GeoState&, bool, SVec<double, dim2>&);

  template<int dim1, int dim2>
  void computeTransposeDerivativeOfNodeBcValue(dRdXoperators<dim1>&, SVec<double, dim1>&, GeoState&,
                                               SVec<double, 3>&);

  template<int dim1, int dim2>
  void computeDerivativeOperatorsOfNodeBcValue(SVec<double, 3>&, SVec<double, Face::MaxNumNd * dim1>&,
                                               SVec<double, dim2>&, dRdXoperators<dim1>&);

  template<int dim>
  void computeNodeBcWallValue(SVec<double, 3>&, SVec<double, 1>&, SVec<double, dim*(dim-5)>&, SVec<double, dim*(dim-5)>&,
                              EmbeddedBcData<dim> *);

  template<int dim>
  void computeBCsJacobianWallValues(FemEquationTerm *, BcData<dim>&, GeoState&, SVec<double, dim>&, EmbeddedBcData<dim> *);

  template<int dim>
  void applyBCsToSolutionVector(BcFcn *, BcData<dim>&, SVec<double, dim>&, Vec<int>&, SVec<double, 9>&,
                                LevelSetStructure *);

  template<int dim>
  void applyBCsToSolutionVectorFullMesh(BcFcn *, BcData<dim>&, SVec<double, dim>&, Vec<int>&, SVec<double, 9>&,
                                LevelSetStructure *);

  template<int dim>
  void applyBCsToTurbSolutionVector(BcFcn *, BcData<dim>&, SVec<double, dim>&, Vec<int>&, SVec<double, 9>&,
                                    LevelSetStructure *);

  template<int dim>
  void applyBCsToResidual(BcFcn *, BcData<dim>&, SVec<double, dim>&, SVec<double, dim>&, Vec<int>&, SVec<double, 9>&,
                          LevelSetStructure *);

  template<int dim>
  void applyBCsToDerivativeOfResidual(BcFcn *, BcData<dim>&, SVec<double, dim>&, SVec<double, dim>&, SVec<double, dim>&,
                                      Vec<int>&, SVec<double, 9>&, LevelSetStructure *);

  template<int dim>
  void applyBCsToTransposeDerivativeOfResidual(BcFcn *, BcData<dim>&, SVec<double, dim>&, Vec<int>&, SVec<double, 9>&,
                                               LevelSetStructure *);

  template<int dim>
  void applyBCsToTimeResidual(BcFcn *, BcData<dim>&, SVec<double, dim>&, SVec<double, dim>&, Vec<int>&, SVec<double, 9>&,
                              LevelSetStructure *);

  template<int dim, class Scalar, int neq>
  void applyBCsToJacobian(BcFcn *, BcData<dim>&, SVec<double, dim>&, MvpMat<Scalar, neq>&, Vec<int>&, SVec<double, 9>&,
                          LevelSetStructure *, bool *);

  template<int dim, class Scalar, int neq>
  void applyBCsToJacobianWallValues(BcFcn *, BcData<dim>&, SVec<double, dim>&, MvpMat<Scalar, neq>&, Vec<int>&,
                                    SVec<double, 9>&, LevelSetStructure *LSS, bool *);

  int findFarfieldNode();

  // I/O
  void openFileForWriting(const char *, int, int);

  template<class Scalar>
  double readTagFromFile(const char *, int, int *, int *);

  template<class Scalar>
  void writeTagToFile(const char *, int, double, int);

  template<class Scalar, int Dim>
  void readVectorFromFile(const char *, int, int, SVec<Scalar, Dim>&, Scalar *);

  template<class Scalar>
  void readVectorFromFile(const char *, int, int, Vec<Scalar>&, Scalar *);

  template<class Scalar, int Dim>
  void readVectorFromFile(const char *, SVec<Scalar, Dim>&);

  template<class Scalar, int Dim>
  void writeVectorToFile(const char *, int, SVec<Scalar, Dim>&, Scalar *);

  template<class Scalar>
  void writeVectorToFile(const char *, int, Vec<Scalar>&, Scalar *);

  template<class Scalar, int Dim>
  void writeVectorToFile(const char *, SVec<Scalar, Dim>&);

  template<int dim>
  void computeNodalForce(PostFcn *, BcData<dim>&, GeoState&, SVec<double, dim>&,
                         double, SVec<double, 3>&, SVec<double, 3>&, EmbeddedBcData<dim> *);

  template<int dim>
  void computeDerivativeOfNodalForce(PostFcn *, BcData<dim>&, GeoState&, SVec<double, dim>&,
                                     SVec<double, dim>&, double, bool, SVec<double, 3>&,
                                     SVec<double, 3>&, SVec<double, 3>&, EmbeddedBcData<dim> *);

  template<int dim>
  void computeDerivativeOfNodalForce(dRdXoperators<dim>&, SVec<double, 3>&, SVec<double, dim>&, bool,
                                     SVec<double, 3>&, SVec<double, 3>&);

  template<int dim>
  void computeTransposeDerivativeOfNodalForce(dRdXoperators<dim>&, SVec<double, 3>&, SVec<double, 3>&,
                                              SVec<double, 3>&, SVec<double, dim>&);

  template<int dim>
  void computeDerivativeOperatorsOfNodalForce(PostFcn *, BcData<dim>&, GeoState&, SVec<double, 3>&, SVec<double, dim>&,
                                              double, SVec<double, 3>&, EmbeddedBcData<dim> *,
                                              dRdXoperators<dim>&);

  template<int dim>
  void computeNodalHeatPower(PostFcn *, BcData<dim>&, GeoState&, SVec<double, dim>&, Vec<double>&, EmbeddedBcData<dim> *);

  template<int dim>
  void computeBulkQuantities(std::map<int, int>&, GeoState&, SVec<double, dim>&, double *, double *, double *);

  template<int dim>
  void computeForceAndMoment(std::map<int, int>&, PostFcn *, BcData<dim>&,
                             GeoState&, SVec<double, dim>&, Vec3D&, Vec3D *, Vec3D *,
                             Vec3D *, Vec3D *, SVec<double, 3>&, EmbeddedBcData<dim> *, int);

  template<int dim>
  void computeDerivativeOfForceAndMoment(std::map<int, int>&, PostFcn *, BcData<dim>&,
                                         GeoState&, SVec<double, dim>&, SVec<double, dim>&,
                                         bool, Vec3D&, Vec3D *, Vec3D *, Vec3D *, Vec3D *,
                                         SVec<double, 3>&, SVec<double, 3>&, EmbeddedBcData<dim> *, int);

  template<int dim>
  void computeDerivativeOfForceAndMoment(dRdXoperators<dim>&, SVec<double, 3>&, SVec<double, dim>&, bool, SVec<double, 3>&,
                                         Vec3D *, Vec3D *, Vec3D *, Vec3D *, int);

  template<int dim>
  void computeTransposeDerivativeOfForceAndMoment(dRdXoperators<dim>&, SVec<double, 3>&, SVec<double, 3>&, SVec<double, 3>&,
                                                  SVec<double, 3>&, SVec<double, 3>&, SVec<double, dim>&, SVec<double, 3>&,
                                                  SVec<double, 3>&, int);

  template<int dim>
  void computeDerivativeOperatorsOfForceAndMoment(std::map<int, int>&, PostFcn *, BcData<dim>&, GeoState&,
                                                  SVec<double, dim>&, Vec3D&, SVec<double, 3>&, EmbeddedBcData<dim> *, int,
                                                  dRdXoperators<dim>&);

  template<int dim>
  void computeHeatFluxes(std::map<int, int>&, PostFcn *, BcData<dim>&,
                         GeoState&, SVec<double, dim>&, double *, EmbeddedBcData<dim> *);

  template<int dim>
  void computeFaceScalarQuantity(PostFcn::ScalarType, PostFcn *, BcData<dim>&, GeoState&,
                                 SVec<double, dim>&, SVec<double, 2>&, EmbeddedBcData<dim> *);

  template<int dim>
  void computeNodeScalarQuantity(PostFcn::ScalarType, PostFcn *, SVec<double, dim>&, SVec<double, 3>&,
                                 Vec<double>&, Vec<int> *, SVec<double, dimLS> *, Vec<bool> *, double[3]);

  template<int dim>
  double computeNodeScalarQuantity(PostFcn::ScalarType, PostFcn *, SVec<double, dim>&, SVec<double, 3>&,
                                   Vec<int> *, double[3], int);

  template<int dim>
  void computeDerivativeOfNodeScalarQuantity(PostFcn::ScalarDerivativeType, PostFcn *, bool,
                                             SVec<double, dim>&, SVec<double, dim>&, SVec<double, 3>&,
                                             SVec<double, 3>&, Vec<double>&, Vec<int> *);

  // mesh motion
  void getReferenceMeshPosition(SVec<double, 3>&);

  void computeDisplacement(SVec<double, 3>&, SVec<double, 3>&);

  void getNdAeroLists(int&, int *&, int&, int *&, int&, int *&, Vec<int>&, MatchNodeSet *);

  int *getSlipSurfOwnership(CommPattern<int>&, std::map<int, SurfaceData *>&);

  void createSlipSurfProjection(int *surfaceOwn, CommPattern<int>&, BCApplier *,
                                SurfaceData *slipSurfaces[]);

  int *getMeshMotionDofType(std::map<int, SurfaceData *>&, CommPattern<int>&, Vec<int>&, MatchNodeSet * = 0);

  void completeMeshMotionDofType(int *, CommPattern<int>&);

  template<class Scalar>
  void computeStiffAndForce(DefoMeshMotionData&, SVec<double, 3>&, SVec<double, 3>&,
                            MvpMat<Scalar, 3>&, std::vector<double>&);

  void setBCond(BCondSet *subBC) {
    mmsBCs = subBC;
  }

  // communication
  void markLenNodes(DistInfo& distInfo) {
    distInfo.setLen(locSubNum, nodes.size());
  }
  void markLenEdges(DistInfo& distInfo) {
    distInfo.setLen(locSubNum, edges.size());
  }
  void markLenFaces(DistInfo& distInfo) {
    distInfo.setLen(locSubNum, faces.size());
  }
  void markLenFaceNorms(DistInfo& distInfo) {
    distInfo.setLen(locSubNum, faces.sizeNorms());
  }
  void markLenNull(DistInfo& distInfo) {
    distInfo.setLen(locSubNum, 0);
  }

  void makeMasterFlag(DistInfo&);
  void makeEdgeMasterFlag(DistInfo&, CommPattern<int_t>&);
#ifdef CONSISTENT_ORDERING
  void makeAllSharedNodes();
#endif
  void setChannelNums(SubDTopo&);
  void identifyEdges(CommPattern<int_t>&);
  void sndNormals(CommPattern<double>&, Vec3D *, double * = nullptr);
  void rcvNormals(CommPattern<double>&, Vec3D *, double * = nullptr);
  void sndEdgeInfo(CommPattern<int_t>&);
  void rcvEdgeInfo(CommPattern<int_t>&);

  template<class Scalar>
  void setComLen(int, int, CommPattern<Scalar>&);

  template<class Scalar>
  void setComLenNodes(int, CommPattern<Scalar>&);

  template<class Scalar>
  void setComLenEdges(int, CommPattern<Scalar>&);

  template<class Scalar>
  void sndData(CommPattern<Scalar>&, Vec<Scalar>&);

  template<class Scalar, int Dim>
  void sndData(CommPattern<Scalar>&, SVec<Scalar, Dim>&);

  template<class Scalar, int Dim>
  void sndData(CommPattern<Scalar>&, DenseMat<Scalar>&);

  template<class Scalar>
  void addRcvData(CommPattern<Scalar>&, Vec<Scalar>&);

  template<class Scalar, int Dim>
  void addRcvData(CommPattern<Scalar>&, SVec<Scalar, Dim>&);

  template<class Scalar, int Dim>
  void addRcvData(CommPattern<Scalar>&, DenseMat<Scalar>&);

  template<class Scalar>
  void reconcileSndData(CommPattern<Scalar>&, Vec<Scalar>&, bool *);

  template<class Scalar, int Dim>
  void reconcileSndData(CommPattern<Scalar>&, SVec<Scalar, Dim>&, bool *);

  template<class Scalar>
  void operateRcvData(CommPattern<Scalar>&, Vec<Scalar>&, std::function<bool(Scalar,Scalar)>);

  template<class Scalar, class Scalar2>
  void operateRcvDataWithKey(CommPattern<Scalar>&, CommPattern<Scalar2>&, Vec<Scalar>&, Vec<Scalar2>&,
                             std::function<bool(Scalar,Scalar)>, Scalar2 *);

  template<class Scalar, int Dim>
  void operateRcvData(CommPattern<Scalar>&, SVec<Scalar, Dim>&, std::function<bool(Scalar,Scalar)>);

  template<class Scalar, int Dim>
  void operateRcvData(CommPattern<Scalar>&, SVec<Scalar, Dim>&, std::function<bool(Scalar,Scalar)>,
                      CommPattern<Scalar>&, SVec<Scalar, Dim>&);

  template<class Scalar>
  void insertRcvData(CommPattern<Scalar>&, Vec<std::set<Scalar>>&);

  template<class Scalar, int Dim>
  void masterRcvData(CommPattern<Scalar>&, SVec<Scalar, Dim>&);

  template<class Scalar>
  void minRcvDataAndCountUpdates(CommPattern<Scalar>&, Vec<Scalar>&, Vec<int>&, Vec<int>&, int&, int, int);

  template<class Scalar>
  void operateRcvdd2wallAndCountUpdates(CommPattern<Scalar>&, Vec<Scalar>&, Vec<int>&, Vec<int>&, int&, int, SVec<std::pair<int, double>, 3> *);

  template<class Scalar, int Dim>
  void sndEdgeData(CommPattern<Scalar>&, SVec<Scalar, Dim>&);

  template<class Scalar>
  void sndEdgeData(CommPattern<Scalar>&, Vec<Scalar>&);

  template<class Scalar, int Dim>
  void addRcvEdgeData(CommPattern<Scalar>&, SVec<Scalar, Dim>&);

  template<class Scalar>
  void addRcvEdgeData(CommPattern<Scalar>&, Vec<Scalar>&);

  template<class Scalar, int dim>
  void sndJacBlocks(CommPattern<Scalar>&, MvpMat<Scalar, dim>&);

  template<class Scalar, int dim>
  void addRcvJacBlocks(CommPattern<Scalar>&, MvpMat<Scalar, dim>&);

  template<class Scalar, int dim>
  void sndRecJacBlocks(CommPattern<Scalar>&, MvpMat<Scalar, dim>&);

  template<class Scalar, int dim>
  void addRcvRecJacBlocks(CommPattern<Scalar>&, MvpMat<Scalar, dim>&);

  template<class Scalar, int dim>
  int cntPrecBlocks(int, SparseMat<Scalar, dim>&);

  template<class Scalar, int dim>
  void sndPrecBlocks(CommPattern<Scalar>&, CommPattern<int>&, SparseMat<Scalar, dim>&, const bool);

  template<class Scalar, int dim>
  void addRcvPrecBlocks(CommPattern<Scalar>&, CommPattern<int>&, SparseMat<Scalar, dim>&, const bool);

  // level-set
  void tagInterfaceNodes(int, Vec<int>&, SVec<double, dimLS>&, int);

  void copyCloseNodes(int, int, Vec<int>&, SVec<double, dimLS>&, Vec<double>&);

  void computeDistanceCloseNodes(int, Vec<int>&, SVec<double, 3>&, SVec<double, dimLS>&, Vec<double>&);

  void recomputeDistanceCloseNodes(int, Vec<int>&, SVec<double, 3>&, SVec<double, dimLS>&, Vec<double>&);

  double computeDistanceLevelNodes(int, Vec<int>&, int, SVec<double, 3>&, Vec<double>&, SVec<double, dimLS>&);

  void getSignedDistance(int, Vec<double>&, SVec<double, dimLS>&);

  // embedded
  template<int dim>
  void computeEmbSurfBasedForceLoad(IoData&, SVec<double, 3>&, double (*)[3], int, int (*)[3], Vec<Vec3D>&,
                                    EmbeddedBcData<dim>&, double, SVec<double, dim>&,
                                    PostFcn *, NodalGrad<dim, double>&, VarFcn *, int);
  template<int dim>
  void computeEmbSurfBasedPostProcessForceLoad(IoData&, SVec<double, 3>&, double (*)[3], int, int (*)[3],
                                               Vec<Vec3D>&, Vec3D *, EmbeddedBcData<dim>&,
                                               double, SVec<double, dim>&, PostFcn *,
                                               NodalGrad<dim, double>&, VarFcn *, double&, double *, int,
                                               double *, int *);
  template<int dim>
  void computeWeightsForSweptNodes(SVec<double, dim>&, SVec<double, dim>&, Vec<double>&,
                                   EmbeddedBcData<dim>&, Vec<int>&, Vec<int>&, int);

  template<int dim>
  void populateGhostPoints(SVec<double, 3>&, SVec<double, dim>&, NodalGrad<dim, double>&, VarFcn *,
                           FemEquationTerm *, EmbeddedBcData<dim>&, int);

  template<int dim>
  void assembleGhostPoints_Step1(Vec<GhostPoint<dim>*>&, CommPattern<int>&,
                                 CommPattern<double>&);

  template<int dim>
  void assembleGhostPoints_Step2(CommPattern<int>&, Vec<GhostPoint<dim>*>&,
                                 CommPattern<int>&, CommPattern<double>&);

  template<int dim>
  void assembleGhostPoints_Step3(CommPattern<int>&, Vec<GhostPoint<dim>*>&,
                                 VarFcn *, CommPattern<int>&, CommPattern<double>&, int);

  template<int dim>
  void reduceGhostPoints_Step1(Vec<GhostPoint<dim>*>&, CommPattern<double>&, bool *, int);

  template<int dim>
  void reduceGhostPoints_Step2(Vec<GhostPoint<dim>*>&, CommPattern<double>&, bool *, int);

  template<int dim, class Scalar, int neq>
  void populateGhostJacobian(SVec<double, 3>&, SVec<double, dim>&, FluxFcn **, VarFcn *,
                             MvpMat<Scalar, neq>&, EmbeddedBcData<dim>&);

  void computeInterfaceFluidMeshSize(IoData&, LevelSetStructure&, SVec<double, 3>&, int, int (*)[3],
                                     Vec<Vec3D>&, Vec3D *, double&, double *, double *, int *);

  template<int dim>
  void computeEmbNodeScalarQuantity(IoData&, SVec<double, 3>&, SVec<double, dim>&, PostFcn *, VarFcn *, WallFcn *,
                                    int, int, int (*)[3], Vec<Vec3D>&, Vec3D *, EmbeddedBcData<dim>&,
                                    double, NodalGrad<dim, double>&, double&, double *, double **, std::vector<PostFcn::ScalarType>&,
                                    double *, int *);

  template<int dim>
  void interpolateSolution(SVec<double, 3>&, SVec<double, dim>&, EmbeddedBcData<dim> *, const Vec3D&, int&, double[dim], int&);

  template<int dim>
  double computeYplusEdgeLength(IoData&, SVec<double, 3>&, SVec<double, dim>&, EmbeddedBcData<dim> *, int, int (*)[3],
                                Vec<Vec3D>&, Vec3D *, double&, double *, VarFcn *, WallFcn *, double, RefinementDomainData&);

  template<int dim, class Int>
  void computeElemWeights(EmbeddedBcData<dim> *, int, Int *);

  void setNodeRanges(int, int (*)[3]);
  void setNumClusNodes(int);
  void makeClusterFlag(int (*)[2], Vec<int>&);
  void makeLocToClusNodeMap_Step1(int, const Vec<int>&, Vec<int>&);
  void makeLocToClusNodeMap_Step2(CommPattern<int>&, int (*)[2], Vec<int>&);

  template<int dim, class Obj>
  void integrateFunction(Obj *obj, NodeSet& X, SVec<double, dim>& V, void (Obj::*F)(int node, const Vec3D& loc, double *f),
                         int npt) { elems.integrateFunction(obj, X, V, F, npt); }

  // linearized
  template<int dim>
  void computeForceDerivs(VarFcn *, SVec<double, 3>&, SVec<double, dim>&,
                          SVec<double, dim>&, Vec<double>&, SVec<double, 3> **);

  template<class TRIPLET, int dim>
  void computeForceDerivMat(VarFcn *, SVec<double, 3>&, SVec<double, dim>&,
                            SVec<double, 3> **, std::vector<TRIPLET>&, int);

  template<int dim>
  void padeReconstruction(SVec<double, dim> **, SVec<double, dim> **, int *,
                          double *, double, int, int, int, int);

  template<int dim>
  void extractElementsRelativeToANode(SVec<double, dim> **, double *, int, int);

  template<int dim>
  void snapshotsConstruction(SVec<double, dim> **, std::complex<double> *,
                             int, int, int, int, int, double);

  template<int dim>
  void hardyInterpolationLogMap(SVec<double, dim> ***, SVec<double, dim> **,
                                int, int, int, DenseMat<double>&, DenseMat<double>&);

  template<int dim>
  void extractElementsRelativeToANodeAndAVector(SVec<double, dim> ***, double *,
                                                int, int, int, int);

  void multiPade(std::complex<double> *, int *, double *, std::complex<double> *,
                 std::complex<double> *, int, int, int, double, double, std::complex<double> *,
                 double *);

  int multiPointsFreq(int, int, double *, int, int);

  void multiPointsDeltaFreq(int, double *, int, double *, int *);

  void buildPadeMatrix(std::complex<double> *, int *, int, double *,
                       std::complex<double> *, int, int, int);

  void buildPadeRhs(std::complex<double> *, int *, int, std::complex<double> *,
                    int, int, int);

  void padeSolution(std::complex<double> *, int *, double, int, int, int, double,
                    int, std::complex<double> *, int *, int, double, double);

  void solveLinearSystem(std::complex<double> *, std::complex<double> *, int);

  void buildDeltaFreq(double *, int, double *, int *);

  void extractElementsRelativeToAComponentAndAMode(double *, std::complex<double> *,
                                                   int, int, int, int, int, int, double);

  // sensitivity analysis
  void checkVec(Vec<int>&, SVec<double, 3>&);

  template<int dim, int dim2>
  void create_NodeBaseddRdXoperators(DistMat<RectangularSparseMat<double, dim, dim2>>&);

  template<int dim, int dim2>
  void create_ConstantToNodeBaseddRdXoperators(DistMat<RectangularSparseMat<double, dim, dim2>>&);

  template<int dim, int dim2>
  void create_ConstantToMovingWallNodeBaseddRdXoperators(DistMat<RectangularSparseMat<double, dim, dim2>>&);

  template<int dim, int dim2>
  void create_NodeToConstantBaseddRdXoperators(DistMat<RectangularSparseMat<double, dim, dim2>>&);

  template<int dim, int dim2>
  void create_FaceToConstantBaseddRdXoperators(DistMat<RectangularSparseMat<double, dim, dim2>>&);

  template<int dim, int dim2>
  void create_FaceToSelfBaseddRdXoperators(DistMat<RectangularSparseMat<double, dim, dim2>>&);

  template<int dim, int dim2>
  void create_FaceToFacetBaseddRdXoperators(DistMat<RectangularSparseMat<double, dim, dim2>>&);

  template<int dim, int dim2>
  void create_ConstantToConstantBaseddRdXoperators(DistMat<RectangularSparseMat<double, dim, dim2>>&);

  template<int dim, int dim2>
  void create_NodeToFaceBaseddRdXoperators(DistMat<RectangularSparseMat<double, dim, dim2>>&);

  template<int dim, int dim2>
  void create_NodeToFacetBaseddRdXoperators(DistMat<RectangularSparseMat<double, dim, dim2>>&);

  template<int dim, int dim2>
  void create_NodeToEdgeBaseddRdXoperators(DistMat<RectangularSparseMat<double, dim, dim2>>&);

  template<int dim, int dim2>
  void create_EdgeBaseddRdXoperators(DistMat<RectangularSparseMat<double, dim, dim2>>&);

  template<int dim, int dim2>
  void create_FaceBaseddRdXoperators(DistMat<RectangularSparseMat<double, dim, dim2>>&);

};

//------------------------------------------------------------------------------

#endif

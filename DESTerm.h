#ifndef _DES_TERM_H_
#define _DES_TERM_H_

#include <SpalartAllmarasTerm.h>
#include <IoData.h>

//------------------------------------------------------------------------------
/*
@INPROCEEDINGS{spalart-allmaras-92,
  author = "Strelets, M.",
  title = "Detached Eddy Simulation of Massively Separated Flows",
  month = jan,
  year = 2001,
  booktitle = "39th AIAA Aerospace Sciences Meeting and Exhibit, Reno, NV",
  note = "{AIAA} paper 01-16694"
}
NOTE: This class only provides an overridden definition for computing the
      turbulent length scale, inheriting everything else from the standard SA
      model
*/
class DESTerm : public SATerm {

  static constexpr double fourth = 1.0 / 4.0;

  double cdes;

 public:
  DESTerm(IoData&, ViscoFcn *, VarFcn *);
  ~DESTerm() {}

  double computeLengthScale(double[4], double[4][3]);

  double computeDerivativeOfLengthScale(double[4], double[4], double[4][3], double[4][3]);

  void computeDerivativeOperatorsOfLengthScale(double[4], double[4][3], double[4], double[4][3]);

};

//------------------------------------------------------------------------------

inline
DESTerm::DESTerm(IoData& iod, ViscoFcn *visf, VarFcn *vfn) : SATerm(iod, iod.eqs.tc.tm.des, visf, vfn) {
  cdes = iod.eqs.tc.tm.des.cdes;
}

//------------------------------------------------------------------------------

inline
double DESTerm::computeLengthScale(double d2w[4], double X[4][3]) {
  double maxl, sidel;
  maxl = -1.0;
  for(int i = 0; i < 4; i++) {
    for(int j = i + 1; j < 4; j++) {
      sidel = sqrt((X[i][0] - X[j][0]) * (X[i][0] - X[j][0]) +
                   (X[i][1] - X[j][1]) * (X[i][1] - X[j][1]) +
                   (X[i][2] - X[j][2]) * (X[i][2] - X[j][2]));
      maxl = std::max(maxl, sidel);
    }
  }
  double d2wall = fourth * (d2w[0] + d2w[1] + d2w[2] + d2w[3]);
  return std::min(d2wall, cdes * maxl);
}

//------------------------------------------------------------------------------

inline
double DESTerm::computeDerivativeOfLengthScale(double d2w[4], double dd2w[4], double X[4][3],
                                               double dX[4][3]) {
  int maxi, maxj;
  double maxl, sidel;
  maxl = -1.0;
  for(int i = 0; i < 4; i++) {
    for(int j = i + 1; j < 4; j++) {
      sidel = sqrt((X[i][0] - X[j][0]) * (X[i][0] - X[j][0]) +
                   (X[i][1] - X[j][1]) * (X[i][1] - X[j][1]) +
                   (X[i][2] - X[j][2]) * (X[i][2] - X[j][2]));
      if(maxl < sidel) {
        maxi = i; maxj = j;
      }
      maxl = std::max(maxl, sidel);
    }
  }
  double d2wall = fourth * (d2w[0] + d2w[1] + d2w[2] + d2w[3]);
  double dd2wall = fourth * (dd2w[0] + dd2w[1] + dd2w[2] + dd2w[3]);
  if((cdes * maxl) < d2wall) {
    double dmaxl = ((X[maxi][0] - X[maxj][0]) * (dX[maxi][0] - dX[maxj][0]) +
                    (X[maxi][1] - X[maxj][1]) * (dX[maxi][1] - dX[maxj][1]) +
                    (X[maxi][2] - X[maxj][2]) * (dX[maxi][2] - dX[maxj][2])) / maxl;
    return cdes * dmaxl;
  }
  else {
    double dd2wall = fourth * (dd2w[0] + dd2w[1] + dd2w[2] + dd2w[3]);
    return dd2wall;
  }
}

//------------------------------------------------------------------------------

inline
void DESTerm::computeDerivativeOperatorsOfLengthScale(double d2w[4], double X[4][3], double dd2walldd2w[4],
                                                      double dd2walldX[4][3]) {
  int maxi, maxj;
  double maxl, sidel;
  maxl = -1.0;
  for(int i = 0; i < 4; i++) {
    for(int j = i + 1; j < 4; j++) {
      sidel = sqrt((X[i][0] - X[j][0]) * (X[i][0] - X[j][0]) +
                   (X[i][1] - X[j][1]) * (X[i][1] - X[j][1]) +
                   (X[i][2] - X[j][2]) * (X[i][2] - X[j][2]));
      if(maxl < sidel) {
        maxi = i; maxj = j;
      }
      maxl = std::max(maxl, sidel);
    }
  }
  double d2wall = fourth * (d2w[0] + d2w[1] + d2w[2] + d2w[3]);
  if((cdes * maxl) < d2wall) {
    for(int k = 0; k < 3; ++k) {
      dd2walldX[maxi][k] =  cdes * (X[maxi][k] - X[maxj][k]) / maxl;
      dd2walldX[maxj][k] = -cdes * (X[maxi][k] - X[maxj][k]) / maxl;
    }
  }
  else {
    dd2walldd2w[0] = dd2walldd2w[1] = dd2walldd2w[2] = dd2walldd2w[3] = fourth;
  }
}

//------------------------------------------------------------------------------

#endif

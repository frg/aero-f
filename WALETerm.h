#ifndef _WALE_TERM_H_
#define _WALE_TERM_H_

#include <LESTerm.h>
#include <IoData.h>

//------------------------------------------------------------------------------
/*
 * Subgrid Scale Stress Modelling Based on the Square of the
 * Velocity Gradient Tensor, F. Nicoud and F. Ducros
 * Flow, Turbulence, and Combustion 62, 183-200, 1999
 */
class WALETerm : public LESTerm {

  static constexpr double fourth = 1.0 / 4.0;
  static constexpr double sixth = 1.0 / 6.0;
  static constexpr double twothirds = 2.0 / 3.0;

  double alpha;
  double cw;

 public:
  WALETerm(IoData&);
  ~WALETerm() {}

  double computeEddyViscosity(double *[4], double, double[3][3]);

  double computeDerivativeOfEddyViscosity(double *[4], double *[4], double, double,
                                          double[3][3], double[3][3]);

  void computeJacobianEddyViscosity(double *[4], double, double[3][3],
                                    double[4][3], double[4][5]);

  double computeTKE(double, double);

};

//------------------------------------------------------------------------------

inline
WALETerm::WALETerm(IoData& iod) : LESTerm(iod) {
  cw = iod.eqs.tc.les.wale.c_w;
}

//------------------------------------------------------------------------------

inline
double WALETerm::computeEddyViscosity(double *V[4], double Delta, double duidxj[3][3]) {
  double rho = fourth * (V[0][0] + V[1][0] + V[2][0] + V[3][0]);
  // strain rate tensor
  double S[3][3];
  S[0][0] = duidxj[0][0];
  S[1][1] = duidxj[1][1];
  S[2][2] = duidxj[2][2];
  S[0][1] = 0.5 * (duidxj[0][1] + duidxj[1][0]);
  S[0][2] = 0.5 * (duidxj[0][2] + duidxj[2][0]);
  S[1][2] = 0.5 * (duidxj[1][2] + duidxj[2][1]);
  S[1][0] = S[0][1];
  S[2][0] = S[0][2];
  S[2][1] = S[1][2];
  double S2 = (S[0][0] * S[0][0] + S[0][1] * S[0][1] + S[0][2] * S[0][2] +
               S[1][0] * S[1][0] + S[1][1] * S[1][1] + S[1][2] * S[1][2] +
               S[2][0] * S[2][0] + S[2][1] * S[2][1] + S[2][2] * S[2][2]);
  double sqS2 = S2 * S2;
  // compute S^d (traceless symmetric part of the square of the velocity gradient tensor, see Eq. 10)
  double duidxj2[3][3];
  for(int i = 0; i < 3; ++i) {
    for(int j = 0; j < 3; ++j) {
      duidxj2[i][j] = 0.0;
      for(int k = 0; k < 3; ++k) {
        duidxj2[i][j] += duidxj[i][k] * duidxj[k][j];
      }
    }
  }
  double p = 1/3.*(duidxj2[0][0] + duidxj2[1][1] + duidxj2[2][2]);
  double Sd[3][3];
  Sd[0][0] = duidxj2[0][0] - p;
  Sd[1][1] = duidxj2[1][1] - p;
  Sd[2][2] = duidxj2[2][2] - p;
  Sd[0][1] = 0.5 * (duidxj2[0][1] + duidxj2[1][0]);
  Sd[0][2] = 0.5 * (duidxj2[0][2] + duidxj2[2][0]);
  Sd[1][2] = 0.5 * (duidxj2[1][2] + duidxj2[2][1]);
  Sd[1][0] = Sd[0][1];
  Sd[2][0] = Sd[0][2];
  Sd[2][1] = Sd[1][2];
  // compute S^d_ij S^d_ij without assuming incompressibility (i.e. trace of Sd*Sd)
  double SD = (Sd[0][0] * Sd[0][0] + Sd[0][1] * Sd[0][1] + Sd[0][2] * Sd[0][2] +
               Sd[1][0] * Sd[1][0] + Sd[1][1] * Sd[1][1] + Sd[1][2] * Sd[1][2] +
               Sd[2][0] * Sd[2][0] + Sd[2][1] * Sd[2][1] + Sd[2][2] * Sd[2][2]);

  double num, denom;
  if(SD < 1.0e-10) {
    num = 0.0;
    denom = 1.0;
  }
  else if(S2 < 1.0e-10) {
    double sqrtSD = sqrt(SD);
    num = sqrt(sqrtSD); // (SD)^1.5/(SD)^1.25 = (SD)^0.25
    denom = 1.0;
  }
  else {
    double sqrtSD = sqrt(SD);
    num = SD*sqrtSD;                         // (SD)^1.5
    denom = sqS2*sqrt(S2) + SD*sqrt(sqrtSD); // (S2)^2.5 + (SD)^1.25
  }
  return rho * (cw * Delta) * (cw * Delta) * (num / denom);
}

//------------------------------------------------------------------------------

inline
double WALETerm::computeDerivativeOfEddyViscosity(double *V[4], double *dV[4], double Delta,
                                                  double dDelta, double duidxj[3][3],
                                                  double dduidxj[3][3]) {
  std::cerr << "*** Warning: WALETerm::computeDerivativeOfEddyViscosity is not implemented\n";
  return 0;
}

//------------------------------------------------------------------------------

inline
void WALETerm::computeJacobianEddyViscosity(double *V[4], double Delta, double duidxj[3][3],
                                            double dp1dxj[4][3], double dmut[4][5]) {
  double rho = fourth * (V[0][0] + V[1][0] + V[2][0] + V[3][0]);
  double S[3][3];
  S[0][0] = duidxj[0][0];
  S[1][1] = duidxj[1][1];
  S[2][2] = duidxj[2][2];
  S[0][1] = 0.5 * (duidxj[0][1] + duidxj[1][0]);
  S[0][2] = 0.5 * (duidxj[0][2] + duidxj[2][0]);
  S[1][2] = 0.5 * (duidxj[1][2] + duidxj[2][1]);
  S[1][0] = S[0][1];
  S[2][0] = S[0][2];
  S[2][1] = S[1][2];
  double S2 = (S[0][0] * S[0][0] + S[0][1] * S[0][1] + S[0][2] * S[0][2] +
               S[1][0] * S[1][0] + S[1][1] * S[1][1] + S[1][2] * S[1][2] +
               S[2][0] * S[2][0] + S[2][1] * S[2][1] + S[2][2] * S[2][2]);
  double sqS2 = S2 * S2;
  double duidxj2[3][3];
  for(int i = 0; i < 3; ++i) {
    for(int j = 0; j < 3; ++j) {
      duidxj2[i][j] = 0.0;
      for(int k = 0; k < 3; ++k) {
        duidxj2[i][j] += duidxj[i][k] * duidxj[k][j];
      }
    }
  }
  double p = 1/3.*(duidxj2[0][0] + duidxj2[1][1] + duidxj2[2][2]);
  double Sd[3][3];
  Sd[0][0] = duidxj2[0][0] - p;
  Sd[1][1] = duidxj2[1][1] - p;
  Sd[2][2] = duidxj2[2][2] - p;
  Sd[0][1] = 0.5 * (duidxj2[0][1] + duidxj2[1][0]);
  Sd[0][2] = 0.5 * (duidxj2[0][2] + duidxj2[2][0]);
  Sd[1][2] = 0.5 * (duidxj2[1][2] + duidxj2[2][1]);
  Sd[1][0] = Sd[0][1];
  Sd[2][0] = Sd[0][2];
  Sd[2][1] = Sd[1][2];
  double SD = (Sd[0][0] * Sd[0][0] + Sd[0][1] * Sd[0][1] + Sd[0][2] * Sd[0][2] +
               Sd[1][0] * Sd[1][0] + Sd[1][1] * Sd[1][1] + Sd[1][2] * Sd[1][2] +
               Sd[2][0] * Sd[2][0] + Sd[2][1] * Sd[2][1] + Sd[2][2] * Sd[2][2]);

  if(SD < 1.0e-10) {
    for(int k = 0; k < 4; ++k) {
      for(int i = 0; i < 5; ++i) {
        dmut[k][i] = 0.0;
      }
    }
  }
  else {
    double T[3][3];
    for(int i = 0; i < 3; i++) {
      for(int j = 0; j < 3; j++) {
        T[i][j] = 0.0;
        for(int m = 0; m < 3; m++) {
          T[i][j] += (Sd[i][m] * duidxj[m][j] + duidxj[i][m] * Sd[m][j]);
        }
      }
    }

    double sqrtSD = sqrt(SD);
    double sqrtS2 = sqrt(S2);
    double num = SD*sqrtSD;                     // (SD)^(3/2)
    double de = (S2 < 1e-10) ? 0 : sqS2*sqrtS2; // (S2)^(5/2)
    double nom = SD*sqrt(sqrtSD);               // (SD)^(5/4)
    double denom = de + nom;

    double cdcd = (cw * Delta) * (cw * Delta);
    double toto1 = fourth * cdcd * (num / denom);
    double toto2 = 2 * rho * cdcd * sqrtSD / (4 * denom * denom);
    double toto3 = (S2 < 1e-10) ? 0 : -10 * SD * S2 * sqrtS2;
    double toto4 = 6 * de + nom;

    // note: dmut is the derivative w.r.t the conservative state variables
    for(int k = 0; k < 4; ++k) {
      dmut[k][4] = 0.0;
      dmut[k][3] = toto2 * ((dp1dxj[k][0] * S[2][0] + dp1dxj[k][1] * S[2][1] + dp1dxj[k][2] * S[2][2]) * toto3 +
                   (dp1dxj[k][0] * T[0][2] + dp1dxj[k][1] * T[1][2] + dp1dxj[k][2] * T[2][2]) * toto4) / V[k][0];
      dmut[k][2] = toto2 * ((dp1dxj[k][0] * S[1][0] + dp1dxj[k][1] * S[1][1] + dp1dxj[k][2] * S[1][2]) * toto3 +
                   (dp1dxj[k][0] * T[0][1] + dp1dxj[k][1] * T[1][1] + dp1dxj[k][2] * T[2][1]) * toto4) / V[k][0];
      dmut[k][1] = toto2 * ((dp1dxj[k][0] * S[0][0] + dp1dxj[k][1] * S[0][1] + dp1dxj[k][2] * S[0][2]) * toto3 +
                   (dp1dxj[k][0] * T[0][0] + dp1dxj[k][1] * T[1][0] + dp1dxj[k][2] * T[2][0]) * toto4) / V[k][0];
      dmut[k][0] = toto1 - (dmut[k][1] * V[k][1] + dmut[k][2] * V[k][2] + dmut[k][3] * V[k][3]);
    }
  }
}

//------------------------------------------------------------------------------

inline
double WALETerm::computeTKE(double nut, double Delta) {
  double uTKE = nut / (Delta * Delta * cw * cw);
  double k = (1.0 / 2.0) * uTKE * uTKE;
  return k;
}


//------------------------------------------------------------------------------

#endif

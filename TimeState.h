#ifndef _TIME_STATE_H_
#define _TIME_STATE_H_

#include <Types.h>

#include <vector>

class GeoState;
class TimeData;
class TimeLowMachPrec;
class VarFcn;

template<class Scalar> class Vec;
template<class Scalar, int dim> class SVec;
template<class Scalar, int dim> class MvpMat;

//------------------------------------------------------------------------------

template<int dim>
class TimeState {

  TimeData& data;

  Vec<double>& dt;
  Vec<double>& dtc;
  Vec<double>& idti;
  Vec<double>& idtv;
  Vec<double>& dtau;
  SVec<double, dim>& Un;
  SVec<double, dim>& Unm1;
  SVec<double, dim>& Unm2;

  struct TimeFDCoefs {
    double c_np1, c_n, c_nm1, c_nm2;
  };

  enum DescriptorCase { DESCRIPTOR, HYBRID, NONDESCRIPTOR } descriptorCase;

  void computeTimeFDCoefs(GeoState&, TimeFDCoefs&, Vec<double>&, int);
  void computeDualTimeFDCoefs(TimeFDCoefs&, Vec<double>&, int);

  template<class Scalar, int neq>
  void addToJacobianGasPrecLocal(int, Scalar, double, double, TimeLowMachPrec&,
                                 double, Scalar *);

  template<class Scalar, int neq>
  void addToJacobianLiquidPrecLocal(int, Scalar, VarFcn *, TimeLowMachPrec&,
                                    double, Scalar *, int);

 public:
  TimeState(TimeData&, Vec<double>&, Vec<double>&, Vec<double>&, Vec<double>&, Vec<double>&, SVec<double, dim>&,
            SVec<double, dim>&, SVec<double, dim>&);
  ~TimeState() {}

  void add_dAW_dt(GeoState&, Vec<double>&, SVec<double, dim>&, SVec<double, dim>&, std::vector<int>&);

  void add_dAW_dtLS(GeoState&, Vec<double>&, SVec<double, dimLS>&, SVec<double, dimLS>&,
                    SVec<double, dimLS>&, SVec<double, dimLS>&, SVec<double, dimLS>&, std::vector<int>&);

  void add_Prec_dAW_dt(GeoState&, Vec<double>&, SVec<double, dim>&, SVec<double, dim>&,
                       Vec<double>&, TimeLowMachPrec&, VarFcn *, Vec<int> *, std::vector<int>&);

  void add_dAW_dtau(Vec<double>&, SVec<double, dim>&, SVec<double, dim>&, std::vector<int>&);

  void add_Prec_dAW_dtau(Vec<double>&, SVec<double, dim>&, SVec<double, dim>&,
                         Vec<double>&, TimeLowMachPrec&, VarFcn *, Vec<int> *, std::vector<int>&);

  template<class Scalar, int neq>
  void addToJacobianNoPrec(Vec<double>&, MvpMat<Scalar, neq>&, bool *, std::vector<int>&, Scalar = 0, double = 1);

  template<class Scalar, int neq>
  void addToJacobianPrec(Vec<double>&, MvpMat<Scalar, neq>&, Vec<double>&, TimeLowMachPrec&,
                         VarFcn *, Vec<int> *, bool *, std::vector<int>&, Scalar = 0, double = 1);

};

//------------------------------------------------------------------------------

#endif

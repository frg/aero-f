#ifndef _COMMUNICATOR_H_
#define _COMMUNICATOR_H_

#include <cstdlib>
#include <cstdio>
#include <cstdarg>

#ifdef USE_MPI
  #include <mpi.h>
  //#define USE_MPI_ERRORS_RETURN
#endif

#include <ResizeArray.h>

class Connectivity;
class Timer;

struct RecInfo {
  int cpu, len;
};

extern void initCommunication(int&, char **&);
extern void initCustomMPIdatatypes();
extern void closeCommunication();
#ifdef USE_MPI
extern void exit(int) __attribute__ ((noreturn));
#endif

//------------------------------------------------------------------------------

#ifdef USE_MPI
template<class T>
class CommTrace {

 public:
  static MPI_Datatype MPIType;
  static int multiplicity;

};
#endif

//------------------------------------------------------------------------------
/* class for communicator with other processes */
class Communicator {

  int thisCPU;
  int numCPU;

 public:
#ifdef USE_MPI
  MPI_Comm comm;
  int nPendReq;
  ResizeArray<MPI_Request> pendReq;
  ResizeArray<MPI_Status> reqStatus;
#endif

 private:
  Timer *timer;
  int maxverbose;
  void handleError(int, const char *);

 public:
  Communicator();
#ifdef USE_MPI
  Communicator(MPI_Comm);
#endif
  ~Communicator() {};

  void split(int, int, Communicator **);
  Communicator *merge(bool);  // returns the intra-communicator for the two groups of an inter-communicator
  int remoteSize();
  void barrier();
  void waitForAllReq();
  void printf(int, const char *, ...);
  void fprintf(FILE *, const char *, ...);
  void Iprobe(int, int *);

  template<class Scalar>
  void sendTo(int, int, Scalar *, int);

  template<class Scalar>
  void sendToBlocking(int, int, Scalar *, int);

  template<class Scalar>
  RecInfo recFrom(int, Scalar *, int);

  template<class Scalar>
  RecInfo recFrom(int, int, Scalar *, int);

  template<class Scalar>
  void exchange(int, int, int *, Scalar **, int *, Scalar **, int *);

  template<class Scalar>
  void sendRecv(int, int, Scalar *, int, Scalar *, int);

  template<class Scalar>
  void sendRecvReplace(int, int, Scalar *, int);

  template<class Scalar>
  void allGather(Scalar *, int, Scalar *, int);

  template<class Scalar>
  void allGather(Scalar *, int);

  template<class CustomDatatype>
  void allGather(CustomDatatype *, int, MPI_Datatype);

  template<class Scalar>
  void allGatherv(Scalar *, int, Scalar *, int[], int[]);

  template<class Scalar>
  void allGatherv(Scalar *, int[], int[]);

  template<class Scalar>
  void allToAll(Scalar *, int, Scalar *, int);

  template<class Scalar>
  void allToAllv(Scalar *, int[], int[], Scalar *, int[], int[]);

  template<class Scalar>
  void gather(Scalar *, int, Scalar *, int, int = 0);

  template<class Scalar>
  void gatherv(Scalar *, int, Scalar *, int[], int[], int = 0);

  template<class Scalar>
  void scatter(Scalar *, int, Scalar *, int, int = 0);

  template<class Scalar>
  void scatterv(Scalar *, int[], int[], Scalar *, int, int = 0);

  template<class Scalar>
  void broadcast(int, Scalar *, int = 0);

#ifdef USE_MPI
  template<class Scalar>
  void reduce(int, Scalar *, MPI_Op, int = 0);

  template<class Scalar>
  void reduce(int, Scalar *, Scalar *, MPI_Op, int = 0);

  template<class Scalar>
  void exScan(int, Scalar *, Scalar *, MPI_Op);

  template<class Scalar>
  void globalOp(int, Scalar *, MPI_Op);

  template<class Scalar>
  void globalOp(int, Scalar *, Scalar *, MPI_Op);
#endif

  template<class Scalar>
  void globalMin(int, Scalar *);

  template<class Scalar>
  void globalMin(int, Scalar *, Scalar *);

  template<class Scalar>
  void globalMax(int, Scalar *);

  template<class Scalar>
  void globalMax(int, Scalar *, Scalar *);

  template<class Scalar>
  void globalSum(int, Scalar *);

  template<class Scalar>
  void globalSum(int, Scalar *, Scalar *);

  int size() const {
    return numCPU;
  }
  int cpuNum() const {
    return thisCPU;
  }
  void setTimer(Timer *t) {
    timer = t;
  }
  Timer *getTimer() const {
    return timer;
  }
  void setMaxVerbose(int v) {
    maxverbose = v;
  }
  int getMaxVerbose() {
    return maxverbose;
  }

};

//------------------------------------------------------------------------------
/* memory allocators that can be used for message passing and one-sided
 * communication (using MPI_alloc_mem) */
void *operator new(size_t, Communicator&);
void operator delete(void *, Communicator&);
void *operator new[](size_t, Communicator&);
void operator delete[](void *, Communicator&);

//------------------------------------------------------------------------------

class SubDTopo {

  struct CPair {
    int from, to, cpuID;
    CPair() {}
    CPair(int f, int t, int c) {
      from = f;
      to = t;
      cpuID = c;
    }
    // the following operator is required by the STL sort algorithm
    bool operator<(const CPair& x) const {
      // we want to order first by cpuID (with local coms first)
      // then by origin and then by destination
      return cpuID < x.cpuID || (cpuID == x.cpuID &&
                                 (from < x.from || (from == x.from && to < x.to)));
    }
    bool operator==(const CPair& x) const {
      return cpuID == x.cpuID && from == x.from && to == x.to;
    }
  };

  int numCPU; // number of CPUs
  int cpuNum; // this CPU's number
  int localNumSub;
  int *glSubToLocal;
  int *localSubToGl;
  int *subNumNeighb;
  int *glSubToCPU;

  int *neighbCPU;
  Connectivity *cpuSC;
  Connectivity *cpuRC;
  int numPairs;
  CPair *allPairs;

  void makeLocalPairIndex(Connectivity *);
  void makeCrossConnect(); // builds neighbCPU cpuSC and cpuRC

 public:
  // The constructor only needs the connectivity of the subdomains of
  // this CPU to be correct, the connectivity of other subdomains can be
  // (and is in slave domain) ommited.
  SubDTopo(int, Connectivity *, Connectivity *);
  ~SubDTopo();

  int getChannelID(int, int);
  int numChannels() {
    return numPairs;
  }
  int numNeighbCPUs();
  int crossNeighb(int iCpu) {
    return neighbCPU[iCpu];
  }
  int *crossNeighb() {
    return neighbCPU;
  }
  Connectivity *cpuSndChannels() {
    return cpuSC;
  }
  Connectivity *cpuRcvChannels() {
    return cpuRC;
  }
  int reverseChannel(int);
  int sourceIsLocal(int channel) {
    return glSubToCPU[allPairs[channel].from] == cpuNum;
  }
  bool isSubLocal(int sub) {
    return glSubToCPU[sub] == cpuNum;
  }
  int locSubNum(int sub) {
    return glSubToLocal[sub];
  }
  int *getGlSubToCPU() {
    return glSubToCPU;
  }

};

//------------------------------------------------------------------------------

template<class T>
struct SubRecInfo {

  T *data;
  int len;
  int leadDim;
  int nvec;

};

//------------------------------------------------------------------------------
/*
   CommPattern represent a communication pattern.
     Communication is based on the model that a message from one
     subdomain to another subdomain is made of a number of vectors.
     Such vectors are stored in a matrix form. That matrix may have
     larger columns than the actual message length. This allows a subdomain
     to send different subparts of a given matrix to different subdomains.

   A communication patterns contains the following information:
     - Length of a vector from one subdomain to its neighbors
     - Number of vectors in each message
     - Leading dimension of the matrix containing the vectors
*/
template<class T>
class CommPattern {

 public:
  enum Mode { Share, CopyOnSend };
  enum Symmetry { Sym, NonSym };

  // Buffers for local copy-communication
 protected:
  Mode mode;
  Symmetry sym;
  T *localDBuffer;

  // for non-local communication we need a few more info
  int numCPUs;
  int numNeighbCPUs;
  int *neighbCPUs;
  Connectivity *sndConnect;
  Connectivity *rcvConnect;
  // in cpuForChannel, we use the convention that this CPU is marked as -1
  int *cpuForChannel;
  int *isSend;  // whether this channel is a send or receive channel
  int *reverseChannel; // corresponding reverse channel
  // Buffers for cross-memory communication on a cpu per cpu basis
  int *crossSendLen, *crossRcvLen;
  T **crossSendBuffer;
  T **crossRcvBuffer;

  int numChannels;
  SubRecInfo<T> *sRecInfo;

  Communicator *communicator;

 public:
  CommPattern(SubDTopo *, Communicator *, Mode = Share, Symmetry = Sym);
  ~CommPattern() {
    delete[] cpuForChannel;
    delete[] reverseChannel;
    delete[] isSend;
    delete[] sRecInfo;
    if(crossSendBuffer) {
      if(crossSendBuffer[0]) {
        delete[] crossSendBuffer[0];
      }
      delete[] crossSendBuffer;
    }
    if(crossRcvBuffer) {
      delete[] crossRcvBuffer;
    }
    if(crossSendLen) {
      delete[] crossSendLen;
    }
    if(crossRcvLen) {
      delete[] crossRcvLen;
    }
    if(localDBuffer) {
      delete[] localDBuffer;
    }
  }

#ifdef MEM_TMPL_FUNC
  template<class TB>
  CommPattern(CommPattern<TB>&, Communicator *, Mode = Share, Symmetry = Sym);
#endif

  int numCh() {
    return numChannels;
  }
  void setLen(int, int, int = -1, int = 1);
  int getLen(int ID) const {
    return sRecInfo[ID].len;
  }
  void finalize(); // complete the internal setup
  SubRecInfo<T> recData(int);
  SubRecInfo<T> getSendBuffer(int);
  void exchange();

};

//------------------------------------------------------------------------------

#endif

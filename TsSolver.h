
#ifndef _TS_SOLVER_H_
#define _TS_SOLVER_H_

#include <IoData.h>
#include <ErrorHandler.h>
#include <NodeData.h>
#include <Timer.h>

#include <cassert>
#include <cstdlib>
#include <fstream>
#include <iostream>
#include <stdexcept>

#include <DistGeoState.h>
#include <DistBcData.h>
#include <SpaceOperator.h>
#include <Domain.h>

extern void startNavierStokesSolver(IoData&, GeoSource&, Domain&);

//------------------------------------------------------------------------------
/* Class which handles the algorithmic organization of the solution for all problems */
template<class ProblemDescriptor>
class TsSolver {

  ProblemDescriptor *probDesc;
  double dts; // structural time-step
  int it_nmp; double t_nmp; // iteration and time at start of refinement period
  int its_nmp; // structure iteration at start of refinement period
  typename ProblemDescriptor::SolVecType *U_nmp, *U_nmp1, *U_nmp2; // state at start of refinement period
  typename ProblemDescriptor::SolVecType *U_nmc; // state at end of refinement period
  typename ProblemDescriptor::SolVecType *dU, *dUPrev; // state increments used for residual/hybrid cfl strategies

  int amr_step_lvl, amr_step_cor_lvl;

  int resolve(typename ProblemDescriptor::SolVecType&, IoData&, bool&, bool&, bool&);

 public:
  TsSolver(ProblemDescriptor *);
  ~TsSolver();

  int solve(IoData&);
  int fsaSolve(IoData&);
};

//------------------------------------------------------------------------------

template<class ProblemDescriptor>
TsSolver<ProblemDescriptor>::TsSolver(ProblemDescriptor *prbd) {
  probDesc = prbd;
  U_nmp = U_nmp1 = U_nmp2 = 0;
  U_nmc = 0;
  dU = dUPrev = 0;
  amr_step_lvl = amr_step_cor_lvl = 0;
}

//------------------------------------------------------------------------------

template<class ProblemDescriptor>
TsSolver<ProblemDescriptor>::~TsSolver() {
  if(U_nmp) delete U_nmp;
  if(U_nmp1) delete U_nmp1;
  if(U_nmp2) delete U_nmp2;
  if(U_nmc) delete U_nmc;
  if(dU) delete dU;
  if(dUPrev) delete dUPrev;
}

//------------------------------------------------------------------------------

template<class ProblemDescriptor>
int TsSolver<ProblemDescriptor>::solve(IoData& ioData) {
  bool do_balancing, do_correcting, onthefly_restart;
  int status;
  do {
    typename ProblemDescriptor::SolVecType *U = new typename ProblemDescriptor::SolVecType(probDesc->getVecInfo());

    // initialize solutions and geometry
    probDesc->setupTimeStepping(U);
    if(probDesc->clusterDone) {
      return 0;
    }

    // solve
    do_balancing = false, do_correcting = false, onthefly_restart = false;
    status = resolve(*U, ioData, do_balancing, do_correcting, onthefly_restart);
    delete U;

    if(do_balancing) {
      // repartition mesh using parmetis, move reassigned elements and remap solution
      probDesc->adaptiveRepart(U_nmp, U_nmp1, U_nmp2, dU, dUPrev, amr_step_lvl);
    }

    if((do_balancing || onthefly_restart) && probDesc->getRecomputeResidualAfterBalancingOrOntheflyRestart()) {
      // recompute initial residual for steady state convergence criterion
      probDesc->recomputeRestartResidual(false);
    }

    if(do_correcting) {
      // restore state at start of refinement period
      probDesc->initializeCorrector(it_nmp, t_nmp, *U_nmp, U_nmp1, U_nmp2, U_nmc, its_nmp);
    }

    if(do_balancing || do_correcting || onthefly_restart) {
      // modify ioData for non-standard restart (without use of solution/data/etc files)
      ioData.amr.onthefly_restart = true;

      // continue solving
      probDesc->timer->reset();
    }
  } while(do_balancing || do_correcting || onthefly_restart);

  return status;
}

//------------------------------------------------------------------------------

/******************************************************************************
 * Main fluid sensitivity analysis routine for the case, where a steady       *
 * state solution is provided as input file. The code thus performs SA only.  *
 ******************************************************************************/
template<class ProblemDescriptor>
int TsSolver<ProblemDescriptor>::fsaSolve(IoData& ioData) {

  // Check that an input file for the solution is specified
  if(ioData.input.solutions[0] == 0) {
    probDesc->printf(1, "\n !!! SensitivityAnalysis requires an input solution !!!\n\n");
    exit(1);
  }

  typename ProblemDescriptor::SolVecType U(probDesc->getVecInfo());

  // initialize solutions and geometry
  probDesc->setupTimeStepping(&U);

  bool lastIt = false;
  int it = probDesc->getInitialIteration();
  double t = probDesc->getInitialTime();

  // setup solution output files
  probDesc->setupOutputToDisk(&lastIt, it, t, U);
  // for embedded method: send force (if it>0) and receive disp (from structure)
  probDesc->computePositionVector(&lastIt, it, t);

  if(probDesc->getEmbeddedBcData()) {
    probDesc->computeDistanceToWall(t, U, it, lastIt);
  }
  probDesc->computeMeshMetrics(it);

  probDesc->fsoHandler(U);
  probDesc->printTimer();
  return 0;
}

//------------------------------------------------------------------------------

template<class ProblemDescriptor>
int TsSolver<ProblemDescriptor>::resolve(typename ProblemDescriptor::SolVecType& U,
                                         IoData& ioData, bool& do_balancing, bool& do_correcting, bool& onthefly_restart) {
  int verboseFlag = ioData.problem.verbose;
  bool lastIt = false;
  typename ProblemDescriptor::SolVecType *dUPrevPrev = NULL;
  double angle = -2.0;
  if(ioData.ts.cfl.strategy == CFLData::DIRECTION || ioData.ts.cfl.strategy == CFLData::HYBRID) {
    if(!dU) {
      dU = new typename ProblemDescriptor::SolVecType(probDesc->getVecInfo());
      (*dU) = 0.0;
    }
    if(!dUPrev) {
      dUPrev = new typename ProblemDescriptor::SolVecType(probDesc->getVecInfo());
      (*dUPrev) = 0.0;
    }
    dUPrevPrev = new typename ProblemDescriptor::SolVecType(probDesc->getVecInfo());
    (*dUPrevPrev) = 0.0;
  }
  typename ProblemDescriptor::SolVecType *UPrev = new typename ProblemDescriptor::SolVecType(probDesc->getVecInfo());
  (*UPrev) = 0.0;
  double dt;
  int it = probDesc->getInitialIteration();
  double t = probDesc->getInitialTime();
  probDesc->iterationPtr = &it;
  probDesc->timePtr = &t;
  ErrorHandler *errorHandler = probDesc->getErrorHandler();
  double dXCMnm1[3] = {0.0,0.0,0.0};
  // In all cases, initialize aerodynamic force vector for sending to structure (for restart, AERO-S will ignore this vector)
  if(!ioData.amr.onthefly_restart) {
    probDesc->initializeOutputToStructure(U);
  }
  if(ioData.bc.inlet.varyAltitude == BcsFreeStreamData::TRUE) {
    probDesc->getDeltaXCM(dXCMnm1);
  }
  if(!ioData.amr.onthefly_restart || probDesc->getCorrectorIteration() > 0) {
    // send force to structure (if it>0) and receive displacement in return
    dts = probDesc->computePositionVector(&lastIt, it, t);
  }
  if(!ioData.amr.onthefly_restart || (probDesc->getMeshRefinedSinceLastWallDistance() && probDesc->getCorrectorIteration() == 0
     && (ioData.amr.recompute_wall_distance == 2 || (it == 0 && ioData.amr.recompute_wall_distance == 1)))) {
    // for an embedded viscous simulation with turbulence model, compute the distance to the wall
    if(ioData.problem.type[ProblemData::EMBEDDED_FRAMEWORK]) probDesc->computeDistanceToWall(t, U, it, lastIt);
  }
  // setup solution output files
  probDesc->setupOutputToDisk(&lastIt, it, t, U);
  // initialize predictor-corrector (non-restart only)
  if(ioData.amr.predictor_corrector == AdaptiveMeshRefinementData::ON && it == 0 && !ioData.amr.onthefly_restart) {
    probDesc->initializePredictor(it_nmp, t_nmp, U_nmp, U_nmp1, U_nmp2, its_nmp);
  }
  // send initial aerodynamic forces to AERO-S (ignored for restart in AERO-S)
  if(!ioData.amr.onthefly_restart && ioData.problem.type[ProblemData::UNSTEADY]) {
    probDesc->sendInitialForce();
  }

  // lambda defining one step of adaptive mesh refinement with optional coarsening and/or balancing
  auto amr_step = [&]() {
    if(probDesc->checkForAmrRefinement(it, lastIt)) {
      double t0 = probDesc->timer->getTime();
      if(amr_step_cor_lvl == 0 && amr_step_lvl == 0) probDesc->initializeAmrStep(U, it, U_nmp, U_nmp1, U_nmp2, dU, dUPrev);
      bool meshCoarsened = false;
      if((amr_step_lvl == 0 && probDesc->checkForAmrCoarsening(it)) || (it == 0 && ioData.amr.pre_coarsen())) {
        for(; amr_step_cor_lvl < ioData.amr.num_coarsening_levels() && !do_balancing && !onthefly_restart; amr_step_cor_lvl += int(it>0)) {
          NodeData *nodeData;
          if((meshCoarsened = probDesc->refineMesh(U, nodeData, it, lastIt, t, amr_step_cor_lvl,
                                                   do_balancing, U_nmp, U_nmp1, U_nmp2, dU, dUPrev, 0, onthefly_restart))) {
            if(!do_balancing && !onthefly_restart) {
              UPrev->resize(probDesc->getVecInfo());
              if(ioData.ts.cfl.strategy == CFLData::DIRECTION || ioData.ts.cfl.strategy == CFLData::HYBRID) {
                dU->resize(probDesc->getVecInfo(), nodeData, 2);
                dUPrev->resize(probDesc->getVecInfo(), nodeData, 2);
                dUPrevPrev->resize(probDesc->getVecInfo());
              }
            }
            delete [] nodeData;
          }
          else {
            // no coarsening occurred at this level; no more levels are required
            // unless one of the criteria hasn't even started yet
            if((it == 0) || (it > 0 && amr_step_cor_lvl+1 >= ioData.amr.max_start_coarsening_level())) break;
          }
        }
      }
      if((amr_step_cor_lvl == ioData.amr.num_coarsening_levels() || !meshCoarsened) && !onthefly_restart) {
        bool meshRefined = false;
        for(; amr_step_lvl < ioData.amr.num_levels() && !do_balancing && !onthefly_restart; ++amr_step_lvl, amr_step_cor_lvl += int(it==0)) {
          NodeData *nodeData;
          if((meshRefined = probDesc->refineMesh(U, nodeData, it, lastIt, t, amr_step_lvl,
                                                 do_balancing, U_nmp, U_nmp1, U_nmp2, dU, dUPrev, 1, onthefly_restart))) {
            if(!do_balancing && !onthefly_restart) {
              UPrev->resize(probDesc->getVecInfo());
              if(ioData.ts.cfl.strategy == CFLData::DIRECTION || ioData.ts.cfl.strategy == CFLData::HYBRID) {
                dU->resize(probDesc->getVecInfo(), nodeData, 2);
                dUPrev->resize(probDesc->getVecInfo(), nodeData, 2);
                dUPrevPrev->resize(probDesc->getVecInfo());
              }
            }
            delete [] nodeData;
          }
          else {
            // no refinement occurred at this level; no more levels are required
            if(amr_step_lvl+1 >= ioData.amr.max_start_level()) break;
          }
        }
        if(amr_step_lvl == ioData.amr.num_levels() || !meshRefined) {
          if(amr_step_lvl > 0 || amr_step_cor_lvl > 0) {
            if(probDesc->checkForAmrBalancing(it, lastIt, onthefly_restart)) do_balancing = true;
            if(!do_balancing) {
              double t1 = probDesc->timer->getTime();
              probDesc->resize2();
              probDesc->timer->addResizeTime(t1);
            }
            if(!ioData.problem.type[ProblemData::UNSTEADY]) {
              probDesc->recomputeRestartResidual(do_balancing || onthefly_restart);
            }
          }
          if(it != 0 && ioData.amr.predictor_corrector == AdaptiveMeshRefinementData::ON) {
            if(probDesc->getCorrectorIteration() < ioData.amr.num_corrector_its) do_correcting = true;
            else probDesc->initializePredictor(it_nmp, t_nmp, U_nmp, U_nmp1, U_nmp2, its_nmp);
          }
          probDesc->finalizeAmrStep(it);
          amr_step_cor_lvl = amr_step_lvl = 0;
        }
      }
      probDesc->timer->addAmrTime(t0);
    }
    else if(probDesc->checkForAmrBalancing(it, lastIt, onthefly_restart)) do_balancing = true;
  };

  if((it != 0 || ioData.amr.pre_refine()) && ( (!ioData.amr.onthefly_restart || amr_step_lvl > 0 || amr_step_cor_lvl > 0) || ioData.romOffline.arom.submeshInnerProduct )) amr_step();
  if(!onthefly_restart && ioData.romOffline.arom.onlyAMR) return 0;

  if(!do_balancing && !onthefly_restart) {
    if(it > 0) {
      lastIt = probDesc->checkForLastIteration(it, t, dt, U);
    }
    if(lastIt) {
      probDesc->outputMesh(it, lastIt, t);
      probDesc->outputPositionVectorToDisk(U);
      if(it == 0 && ioData.amr.pre_refine()) probDesc->outputToDisk(&lastIt, it, 0, 0, t, dt, U);
      else {
        if(ioData.problem.type[ProblemData::SENSITIVITY_ANALYSIS]) {
          probDesc->fsoHandler(U);
        }
        probDesc->printTimer();
      }
    }
  }

  // 6 August 2019: Andrew McClellan; altitude (in meters) used for updating the farfield boundaries
  double altitude;
  double dXCM[3] = {0.0,0.0,0.0};
  if(ioData.bc.inlet.varyAltitude == BcsFreeStreamData::TRUE) {
    probDesc->getDeltaXCM(dXCM);
    altitude = ioData.bc.inlet.calculateAltitude(t*ioData.ref.rv.time, dXCM);
    probDesc->getBcData()->resetBoundaryConditionsUSStandardAtmosphere(altitude, ioData, probDesc->getGeoState()->getPositionVector());
  }

  while(!lastIt && !do_balancing && !do_correcting && !onthefly_restart) {
    probDesc->resetOutputToStructure(U);
    int stat = 0;
    int itSc = 0;
    int itNl = 0;
    int itNlLS = 0;
    // initialize remaining time in fluid subcycling
    double dtLeft = dts;
    it++;
    bool repeat = false;
    do { // subcycling
      (*UPrev) = U;
      if(!repeat) probDesc->resetFixesTag(); // reactivate use of nodal gradients that were set to zero
                                             // in previous (pseudo-) time-step due to "FailSafe = On"
      double dtLeftPrev = dtLeft;
      stat = 0;
      itSc++;
      probDesc->setCurrentTime(t, U);
      if(dU && dUPrev && (dUPrev->norm() != 0) && (dU->norm() != 0)) {
        angle = ((*dU) * (*dUPrev)) / (dU->norm() * dUPrev->norm());
      }
      else {
        angle = -2.0;
      }
      dt = probDesc->computeTimeStep(it, dtLeft, U, angle, dt);
      t += dt;
      // estimate mesh position in subcycle
      probDesc->interpolatePositionVector(dt, dtLeft);
      // compute control volumes and velocities
      probDesc->computeMeshMetrics(it);
      // write snapshot vector to disk (at beginning of each step, after dt calculation,
      // corresponding to converged state from previous step)
      //
      bool outputSubcycle = false;
      if(ioData.output.rom.outputsubcycle == ROMOutputData::OUTPUT_SUBCYCLE_TRUE && ioData.output.rom.frequency == 1) {
        outputSubcycle = true;
      }
      if(ioData.amr.predictor_corrector == AdaptiveMeshRefinementData::OFF) {
        if(!repeat && (itSc == 1 || outputSubcycle)) probDesc->outputSnapshotsToDisk(ioData, lastIt, it-1, itSc, t-dt, U);
      }

      // 6 August 2019: Andrew McClellan; move BC update to after mesh interpolation; take into account CM motion
      if(ioData.bc.inlet.varyAltitude == BcsFreeStreamData::TRUE) {
        double alpha = dt/(dt + dtLeft) - 1.0;
        double dXCMtmp[3];
        dXCMtmp[0] = dXCM[0] + alpha * (dXCM[0] - dXCMnm1[0]);
        dXCMtmp[1] = dXCM[1] + alpha * (dXCM[1] - dXCMnm1[1]);
        dXCMtmp[2] = dXCM[2] + alpha * (dXCM[2] - dXCMnm1[2]);
        altitude = ioData.bc.inlet.calculateAltitude(t*ioData.ref.rv.time, dXCMtmp);
        probDesc->getBcData()->resetBoundaryConditionsUSStandardAtmosphere(altitude, ioData, probDesc->getGeoState()->getPositionVector());
      }
      // fluid Solution
      if(dU && dUPrev) {
        *dUPrevPrev = *dUPrev;
        *dUPrev = *dU;
        *dU = -1.0 * U;
      }
      errorHandler->clearError(ErrorHandler::ALL);
      stat = probDesc->solveNonLinearSystem(U, it, t, U_nmc ? U_nmc : nullptr);
      if(U_nmc) {
        delete U_nmc;
        U_nmc = 0;
      }
      // note: errors should always be resolved in probDesc->solveNonLinearSystem
      probDesc->setInRedoTimestep(errorHandler->globalErrors[ErrorHandler::REDO_TIMESTEP]);
      if(errorHandler->globalErrors[ErrorHandler::REDO_TIMESTEP]) {
        errorHandler->globalErrors[ErrorHandler::REDO_TIMESTEP] = 0;
        probDesc->printf(1, "Repeating time-step.\n");
        repeat = true;
        // reset U to its previous state
        U = (*UPrev);
        // reset directions for direction strategy
        if(dU && dUPrev) {
          *dU = *dUPrev;
          *dUPrev = *dUPrevPrev;
        }
        // undo time step and subcycling
        t -= dt;
        itSc--;
        dtLeft = dtLeftPrev;
        continue;
      }
      else {
        repeat = false;
      }
      if(dU && dUPrev) {
        *dU += U;
      }
      if(stat > 0) {
        itNl += stat;
        // compute the current aerodynamic force
        probDesc->updateOutputToStructure(dt, dtLeft, U);
        probDesc->updateStateVectors(U, it);
      }
      else {
        if(ioData.romOffline.arom.computeAROM) {
          return 0;
        }
        probDesc->printf(1, "Aborting due to signal %d\n", stat);
        probDesc->printf(1, "Note: use of CheckSolution may enable continuation beyond this point\n");
        throw std::exception();
      }
    }
    while(repeat || dtLeft != 0.0 || stat < 0);
    lastIt = probDesc->checkForLastIteration(it, t, dt, U);
    // note: for predictor step, output to screen and compute distance to wall if required, but don't write solution to disk
    if(ioData.amr.predictor_corrector == AdaptiveMeshRefinementData::OFF ||
       probDesc->getCorrectorIteration() == ioData.amr.num_corrector_its || lastIt) {
      probDesc->outputMesh(it, lastIt, t);
      probDesc->outputForces(&lastIt, it, itSc, itNl, t, dt, U);
    }
    probDesc->outputToScreen(it, itSc, itNl, t);
    probDesc->getDeltaXCM(dXCMnm1);
    dts = probDesc->computePositionVector(&lastIt, it, t);
    probDesc->getDeltaXCM(dXCM);
    if((ioData.problem.type[ProblemData::EMBEDDED_FRAMEWORK] &&
       (ioData.problem.alltype == ProblemData::_UNSTEADY_AEROELASTIC_ || ioData.problem.alltype == ProblemData::_FORCED_)) &&
       !lastIt) {
      probDesc->computeDistanceToWall(t, U, it, lastIt);
    }
    if(ioData.amr.predictor_corrector == AdaptiveMeshRefinementData::OFF ||
       probDesc->getCorrectorIteration() == ioData.amr.num_corrector_its || lastIt) {
      if(lastIt || (ioData.amr.predictor_corrector == AdaptiveMeshRefinementData::ON && probDesc->getCorrectorIteration() == ioData.amr.num_corrector_its)) {
        probDesc->outputSnapshotsToDisk(ioData, lastIt, it, itSc, t, U);  // write final snapshot to disk for nonlinear ROM
      }
      probDesc->outputToDisk(&lastIt, it, itSc, itNl, t, dt, U);
      probDesc->outputRotationMatrixToDisk(lastIt);
    }
    amr_step();
  }
  if(dUPrevPrev) delete dUPrevPrev;
  delete UPrev;
  return 0;
}

//------------------------------------------------------------------------------

#endif

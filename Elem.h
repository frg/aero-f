#ifndef _ELEM_H_
#define _ELEM_H_

#include <BinFileHandler.h>
#include <BlockAlloc.h>
#include <IoData.h>
#include <MapFace.h>
#include <Types.h>

#include <algorithm>
#include <array>
#include <cstdio>
#include <functional>
#include <iostream>
#include <map>
#include <utility>
#include <vector>

class FemEquationTerm;
class EdgeSet;
class NodeSet;
class VarFcn;
class VolumeData;
class LevelSetStructure;

struct ElementData;
struct Vec3D;
template<class Scalar> struct Vec3DTemplate;

template<class Scalar> class Vec;
template<class Scalar, int dim> class SVec;
template<class Scalar, int dim> class MvpMat;
template<int dim> class EmbeddedBcData;
template<int dim> class LESData;
template<class Scalar, int dim, int dim2> class RectangularSparseMat;
template<int dim> struct dRdXoperators;

//------------------------ TETRAHEDRAL ELEMENT CLASS ---------------------------

class Elem {

public:
  enum Type {TET = 5};

  static const int MaxNumNd = 4;
  static const int MaxNumEd = 6;
  static const int MaxNumFc = 4;

  static constexpr double third = 1.0 / 3.0;
  static constexpr double fourth = 1.0 / 4.0;
  static constexpr double sixth = 1.0 / 6.0;

  Elem() {
    volume_id = 0;
    sTag = -1;
  }

  Elem(const Elem& other) {
    std::memcpy(nodeNums, other.nodeNums, sizeof nodeNums);
    std::memcpy(edgeNums, other.edgeNums, sizeof edgeNums);
    volume_id = other.volume_id;
    tag = other.tag;
    sTag = other.sTag;
  }

  ~Elem() {}

 private:

  int volume_id;
  int sTag;
  std::pair<std::array<int,4>,int> tag;

  int nodeNums[4];
  int edgeNums[6];

  static const int edgeEnds[6][2];
  static const int edgeFaces[6][2];
  static const int faceDefs[4][3];
  static const std::map<std::pair<int,int>, int> edgeMap;

 public:

  //--------------Operators

  int& operator[](int i) {
    return nodeNum(i);
  }

  operator int *() {
    return nodeNum();
  }

  //--------------General methods

  Type type() {
    return Elem::TET;
  }

  int faceNnd(int i) {
    return 3;
  }

  // Number of nodes
  int numNodes() {
    return 4;
  }

  // Number of edges
  int numEdges() {
    return 6;
  }

  // Number of faces
  int numFaces() {
    return 4;
  }

  Elem *clone(BlockAlloc& memElems) {
    return new(memElems) Elem(*this);
  }

  int *nodeNum() {
    return nodeNums;
  }

  int& nodeNum(int i) {
    return nodeNums[i];
  }

  int numNode(int nodenum) {
    for(int j = 0; j < numNodes(); j++) {
      if(nodeNums[j]==nodenum) {
        return j;
      }
    }
    std::cerr << "*** Error: No matching node found in element.\n";
    exit(-1);
  }

  int& edgeNum(int i) {
    return edgeNums[i];
  }

  int edgeEnd(int i, int k) {
    return edgeEnds[i][k];
  }

  int edgeFace(int i, int k) {
    return edgeFaces[i][k];
  }

  int faceDef(int i, int k) {
    return faceDefs[i][k];
  }

  // fills the nd array with the list of nodes
  void nodes(int *nd) {
    for(int j = 0; j < numNodes(); ++j) {
      nd[j] = nodeNum(j);
    }
  }

  // Set element volume ID
  void setVolumeID(int i) {
    volume_id = i;
  }

  // Get element volume ID
  int getVolumeID() const {
    return volume_id;
  }

  // Set supermesh tag
  void setSupermeshTag(const int& b) {
    sTag = b;
  }

  // Get supermesh tag
  const int& getSupermeshTag() const {
    return sTag;
  }

  // Delete supermesh tag
  void delSupermeshTag() {
    sTag = -1;
  }

  // Set refinement tag
  void setRefinementTag(const std::pair<std::array<int,4>, int>& T) {
    tag = T;
  }

  // Get refinement tag
  const std::pair<std::array<int,4>, int>& getRefinementTag() const {
    return tag;
  }

  // Get refinement edge
  int getRefinementEdge() const {
    return edgeNums[edgeMap.find(std::make_pair(tag.first[0], tag.first[3]))->second];
  }

  //--------------Functions in ElemCore.C

  void numberEdges(EdgeSet&);
  void renumberEdges(Vec<int>&);
  int  countNodesOnBoundaries(Vec<bool>&);
  int  setFaceToElementConnectivity(int, Vec<bool>&, MapFaces&, int (*)[2]);
  int  findEdge(int, int);

  int getLongestEdge(std::function<double(int,int)>, double&);
  double computeHeight(SVec<double, 3>&, const Vec3D&);
  template<class PosVecType>
  Vec3D computeCentroid(PosVecType&);
  template<class PosVecType>
  Vec3D computeDualCentroid(PosVecType&, int);
  template<class PosVecType>
  double computeVolume(PosVecType&);
  template<class PosVecType>
  double computeDerivativeOfVolume(PosVecType&, PosVecType&);
  template<class PosVecType>
  void computeDerivativeOperatorsOfVolume(PosVecType&, double[3], double[3], double[3], double[3]);
  template<class PosVecType>
  double computeControlVolumes(PosVecType&, Vec<double>&);
  template<class PosVecType>
  double computeDerivativeOfControlVolumes(PosVecType&, PosVecType&, Vec<double>&);
  template<class PosVecType>
  void computeDerivativeOperatorsOfControlVolumes(PosVecType&, RectangularSparseMat<double, 3, 1>&);

  int  checkVolume(NodeSet&);
  void computeEdgeNormals(NodeSet&, Vec<Vec3D>&);
  void computeEdgeNormalsConfig(SVec<double, 3>&, SVec<double, 3>&, Vec<Vec3D>&, Vec<double>&);
  void computeDerivativeOfEdgeNormals(SVec<double, 3>&, SVec<double, 3>&, Vec<Vec3D>&, Vec<Vec3D>&, Vec<double>&, Vec<double>&);
  void computeDerivativeOperatorsOfEdgeNormals(SVec<double, 3>&, RectangularSparseMat<double, 3, 3>&);
  void computeEdgeNormalsGCL1(SVec<double, 3>&, SVec<double, 3>&, SVec<double, 3>&, Vec<Vec3D>&, Vec<double>&);
  void computeEdgeNormalsEZGCL1(double, SVec<double, 3>&, SVec<double, 3>&, Vec<Vec3D>&, Vec<double>&);

  template<class PosVecType>
  double computeGradientP1Function(PosVecType&, double[4][3]);
  double computeDerivativeOfGradientP1Function(SVec<double, 3>&, SVec<double, 3>&, double[4][3]);
  void   computeDerivativeOperatorOfGradientP1Function(SVec<double, 3>&, double[4][3], double[4][3][4][3]);
  void   computeWeightsGalerkin(SVec<double, 3>&, SVec<double, 3>&, SVec<double, 3>&, SVec<double, 3>&);
  void   computeDerivativeOfWeightsGalerkin(SVec<double, 3>&, SVec<double, 3>&, SVec<double, 3>&, SVec<double, 3>&,
                                            SVec<double, 3>&);

  // Mesh motion functions
  void computeStiffAndForce(double *, double *, SVec<double, 3>&, NodeSet&, double);
  void computeStiffAndForceBallVertex(double *, double *, SVec<double, 3>&, NodeSet&, double);
  void computeStiffAndForceLIN(double *, SVec<double, 3>&, NodeSet&);
  void computeStiffBallVertex(double *, SVec<double, 3>&, NodeSet&, double);
  void computeStiffTorsionSpring(double *, SVec<double, 3>&, double);
  void computeStiffAndForceNL(double *, double *, SVec<double, 3>&, NodeSet&, ElementData&, double *);

  // Spatial location functions
  template<class PosVecType>
  void computeBarycentricCoordinates(PosVecType&, const Vec3D&, double[3]);
  template<class PosVecType>
  void computeBarycentricCoordinatesJacobian(PosVecType&, double[4][3]);
  template<class PosVecType>
  void computeBarycentricCoordinatesJacobianInverted(PosVecType&, double[3][3], int);
  template<class PosVecType>
  bool isPointInside(PosVecType&, const Vec3D&, double[3], double);
  template<class PosVecType>
  void computeBoundingBox(PosVecType&, double[6], double);
  template<class PosVecType>
  bool overlapsBoundingBox(PosVecType&, double[6]);

  // Distance functions
  void   fastMarchingDistanceUpdate(int, Vec<int>&, int, SVec<double, 3>&, Vec<double>&, const double,
                                    SVec<std::pair<int, double>, 3> * = nullptr, bool = false, double = 1.0, double = 0.0);
  template<class Scalar>
  Scalar computeDistancePlusPhi(int, SVec<double, 3>&, Scalar[3], int[3], const double = 1e9, bool = false);
  template<class Scalar>
  void   computeDistancePlusPhiToVertices(Scalar[3], const Vec3DTemplate<Scalar>&, const Vec3DTemplate<Scalar>&, const Vec3DTemplate<Scalar>&, Scalar&);
  template<class Scalar>
  bool   computeDistancePlusPhiToOppFace(Scalar[3], const Vec3DTemplate<Scalar>&, const Vec3DTemplate<Scalar>&, const Vec3DTemplate<Scalar>&, Scalar&);
  template<class Scalar>
  void   computeDistancePlusPhiToEdges(Scalar[3], const Vec3DTemplate<Scalar>&, const Vec3DTemplate<Scalar>&, const Vec3DTemplate<Scalar>&, Scalar&,
                                       const double = 1e9);
  template<class Scalar>
  void   computeDistancePlusPhiToEdge(Scalar, Scalar, const Vec3DTemplate<Scalar>&, const Vec3DTemplate<Scalar>&, Scalar&);
  int    computeDistanceToAll(double[3], const Vec3DTemplate<double>&, const Vec3DTemplate<double>&, const Vec3DTemplate<double>&, double&);
  void   computeDistanceToInterface(int, SVec<double, 3>&, int[4], Vec3D[4], Vec<double>&, Vec<int>&);
  void   recomputeDistanceToInterface(int, SVec<double, 3>&, int[4], Vec3D[4], Vec<double>&, Vec<int>&);

  //--------------Templated functions in Elem.C

  template<class NodeMap>
  void renumberNodes(NodeMap&);

  template<int dim>
  void computeTimeStep(FemEquationTerm *, SVec<double, 3>&, SVec<double, dim>&,
                       Vec<double>&, EmbeddedBcData<dim> *);

  template<int dim>
  void computeDerivativeOfTimeStep(FemEquationTerm *, SVec<double, 3>&, SVec<double, 3>&, SVec<double, dim>&,
                                   SVec<double, dim>&, double, Vec<double>&, EmbeddedBcData<dim> *);

  template<int dim>
  void computeGalerkinTerm(FemEquationTerm *, VarFcn *, LESData<dim> *, Vec<double>&, SVec<double, 3>&, SVec<double, dim>&,
                           SVec<double, dim>&, EmbeddedBcData<dim> *, std::vector<bool>&);

  template<int dim>
  void computeDerivativeOfGalerkinTerm(FemEquationTerm *, VarFcn *, LESData<dim> *, Vec<double>&, Vec<double>&,
                                       SVec<double, 3>&, SVec<double, 3>&, SVec<double, dim>&, SVec<double, dim>&,
                                       double, SVec<double, dim>&, EmbeddedBcData<dim> *, std::vector<bool>&);

  template<int dim>
  void computeDerivativeOperatorsOfGalerkinTerm(FemEquationTerm *, SVec<double, 3>&, Vec<double>&,
                                                SVec<double, dim>&, EmbeddedBcData<dim> *, dRdXoperators<dim>&);

  template<int dim, class Scalar, int neq>
  void computeJacobianGalerkinTerm(FemEquationTerm *, VarFcn *, LESData<dim> *, Vec<double>&, SVec<double, 3>&, SVec<double, dim>&,
                                   MvpMat<Scalar, neq>&, EmbeddedBcData<dim> *, std::vector<bool>&);

  template<int dim>
  void computeFaceGalerkinTerm(int, FemEquationTerm *, LESData<dim> *, int[3], Vec3D&,
                               Vec<double>&, double *, SVec<double, 3>&, SVec<double, dim>&,
                               SVec<double, dim>&, EmbeddedBcData<dim> *, std::vector<bool>&);

  template<int dim>
  void computeDerivativeOfFaceGalerkinTerm(int, FemEquationTerm *, LESData<dim> *, int[3],
                                           Vec3D&, Vec3D&, Vec<double>&, Vec<double>&, double *, double *,
                                           SVec<double, 3>&, SVec<double, 3>&, SVec<double, dim>&,
                                           SVec<double, dim>&, double, SVec<double, dim>&,
                                           EmbeddedBcData<dim> *, std::vector<bool>&);

  template<int dim>
  void computeDerivativeOperatorsOfFaceGalerkinTerm(int, FemEquationTerm *, int[3], Vec3D&, Vec<double>&,
                                                    double *, SVec<double, 3>&, SVec<double, dim>&,
                                                    EmbeddedBcData<dim> *, int, dRdXoperators<dim>&);

  template<int dim, class Scalar, int neq>
  void computeFaceJacobianGalerkinTerm(int, FemEquationTerm *, LESData<dim> *, int[3], Vec3D&,
                                       Vec<double>&, double *, SVec<double, 3>&, SVec<double, dim>&,
                                       MvpMat<Scalar, neq>&, EmbeddedBcData<dim> *, std::vector<bool>&);

  template<int dim>
  void computeMaxwellSlipTerm(int, FemEquationTerm *, int[3], Vec3D&, double *, SVec<double, 3>&,
                              SVec<double, dim>&, SVec<double, dim>&, std::vector<bool>&);

  template<int dim>
  void computeTestFilterAvgs(SVec<double, dim>&, SVec<double, 16>&, SVec<double, 6>&, Vec<double>&,
                             SVec<double, 8>&, SVec<double, 3>&, SVec<double, dim>&, double,
                             EmbeddedBcData<dim> *);
  template<int dim>
  void computeTestFilterAvgsHelper(SVec<double, dim>&, SVec<double, 16>&, SVec<double, 6>&, Vec<double>&,
                                   SVec<double, 8>&, double *[4], const double[4][3], const double,
                                   const double, const int = -1);

  template<int dim>
  void computeDistanceErrorMetric(FemEquationTerm *, Vec<double>&, SVec<double, 3>&,
                                  SVec<double, dim>&, Vec<double>&, EmbeddedBcData<dim> *);

  template<int dim, class Obj>
  void integrateFunction(Obj *, NodeSet&, SVec<double, dim>&, void (Obj::*)(int, const Vec3D&, double *), int);

  // For embedded methods
  template<int dim>
  void isIntersected(EmbeddedBcData<dim> *, bool&, bool&);

  template<int dim>
  void isActiveForMLS(EmbeddedBcData<dim> *, bool&, Vec3D&, Vec3D&, NodeSet&);

  template<int dim>
  void getGhostStates(int, SVec<double, 3>&, EmbeddedBcData<dim> *, double *[4], int&, double[4][dim]);

  template<int dim>
  void getDerivativeOfGhostStates(int, EmbeddedBcData<dim> *, double *[4], double *[4]);

  template<int dim>
  void getGhostStates(const Vec3D&, SVec<double, 3>&, EmbeddedBcData<dim> *, double *[4], int&, double[4][dim]);

  template<int dim>
  void getLocalGhostStates(int, EmbeddedBcData<dim> *, double *[4], int&, double[4][dim], double[4][dim]);

  template<int dim>
  void getDerivativeOfLocalGhostStates(int, EmbeddedBcData<dim> *, double *[4], double[4][dim], double*[4]);

  template<int dim>
  void getLocalGhostStates(const Vec3D&, SVec<double, 3>&, EmbeddedBcData<dim> *, double *[4], int&, double[4][dim], double[4][dim]);

  template<int dim>
  void populateLocalGhostStates(SVec<double, 3>&, EmbeddedBcData<dim> *, double *[4], VarFcn *, double[4][dim], int);

  template<int dim, int dim2>
  void populateLocalGhostJacobian(SVec<double, 3>&, EmbeddedBcData<dim> *, double *[4], VarFcn *, double[4][4][dim2], int);

  bool isWallFcnActive(LevelSetStructure&, int);

  bool isWallFcnActive(LevelSetStructure&);

  template<int dim>
  int getPorousWallFrictionLossParam(EmbeddedBcData<dim> *, int, int&, double&, double&, double&);

  template<int dim>
  void getPorousWallFrictionLoss(int, SVec<double, 3>&, SVec<double, dim>&, EmbeddedBcData<dim> *, double, int, double, double, double, Vec3D&);

  template<int dim>
  void getJacobianPorousWallFrictionLoss(int, SVec<double, 3>&, SVec<double, dim>&, EmbeddedBcData<dim> *, double, double[dim], int, double,
                                         double, double, double[3][dim]);

  template<int dim>
  void getWallFcnStates(int, SVec<double, 3>&, EmbeddedBcData<dim> *, int&, Vec3D&, double[dim], double&, Vec3D * = nullptr, double * = nullptr);

  template<int dim>
  double getMinChiWeight(EmbeddedBcData<dim> *, double * = nullptr);

  template<int dim>
  int computeWeight(EmbeddedBcData<dim> *, int);

  template<int dim>
  int interpolateSolution(SVec<double, dim>&, SVec<double, 3>&, const Vec3D&, double[dim], int&, EmbeddedBcData<dim> *);

  template<int dim>
  bool reflectGhostState(SVec<double, 3>&, const Vec3D&, double *[4], EmbeddedBcData<dim> *, int, int, Vec3D&,
                         double *, double = 1e-10, int = 1);

  template<int dim>
  bool reflectGhostStateDerivative(SVec<double, 3>&, const Vec3D&, const Vec3D&, double *[4], EmbeddedBcData<dim> *, int, int, Vec3D&, Vec3D&,
                                   double *, double = 1e-10, int = 1);

  template<int dim, class Scalar, int neq>
  bool reflectGhostStateJacobian(SVec<double, 3>&, const Vec3D&, double *[4], EmbeddedBcData<dim> *, int, int, Vec3D&,
                                 double, int, MvpMat<Scalar, neq>&, VarFcn *, int = -1, int = -1);

  // For level-set methods
  int findLSIntersectionPoint(int, SVec<double, dimLS>&, SVec<double, 3>&, int[4], Vec3D[4]);

  void findLSIntersectionPointLinear(int, SVec<double, dimLS>&, SVec<double, 3>&, int[4], Vec3D[4], int);

  void computeDistanceCloseNodes(int, Vec<int>&, SVec<double, 3>&, SVec<double, dimLS>&, Vec<double>&);

  void recomputeDistanceCloseNodes(int, Vec<int>&, SVec<double, 3>&, SVec<double, dimLS>&, Vec<double>&);

  void computeDistanceLevelNodes(int, Vec<int>&, int, SVec<double, 3>&, Vec<double>&, SVec<double, dimLS>&);

};


//----------------------------- ELEMSET CLASS ----------------------------------

class ElemSet {

 public:
  typedef std::vector<Elem *> Container;

 private:
  Container elems;

 public:
  BlockAlloc memElems;

  ElemSet(int n = 0) {
    elems.resize(n);
  }

  ~ElemSet() {
    for(auto& elem : elems) elem->~Elem();
    elems.clear();
  }

  Container& getElems() {
    return elems;
  }

  Elem& operator[](int i) const {
    return *elems[i];
  }

  void addElem(int i, Elem *elem) {
    if(i == elems.size()) elems.push_back(elem); else elems[i] = elem;
  }

  void removeElem(int i) {
    elems.erase(elems.begin()+i);
  }

  auto begin() -> decltype(elems.begin()) { return elems.begin(); }

  auto end() -> decltype(elems.end()) { return elems.end(); }

  int size() const {
    return int(elems.size());
  }

  void renumber(const std::vector<int>&);

  int read(BinFileHandler&, int, int (*)[2], int_t *, std::map<int, VolumeData *>&, int, int_t *,
           std::map<int, int>&, int);

  void write(BinFileHandler&, int, BinFileHandler::OffType, int, int (*)[2], const int_t *, Vec<int>&,
             AdaptiveMeshRefinementData::BisectionStrategy, int);

  template<int dim, class Obj>
  void integrateFunction(Obj *obj, NodeSet& X, SVec<double, dim>& V, void (Obj::*F)(int, const Vec3D&, double *), int npt) {
    for(auto& E : elems) E->integrateFunction(obj, X, V, F, npt);
  }
};

//------------------------------------------------------------------------------

#endif

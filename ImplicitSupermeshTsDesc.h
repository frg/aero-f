#ifndef _IMPLICIT_SUPERMESH_TS_DESC_H_
#define _IMPLICIT_SUPERMESH_TS_DESC_H_

#include <ImplicitCoupledTsDesc.h>
#include <set>

template<int dim> class Supermesh;
template<class Scalar, int dim> class DistSVec;
class Vec3D;
class DistRTree;

//------------------------------------------------------------------------------

template<int dim>
class ImplicitSupermeshTsDesc : public ImplicitCoupledTsDesc<dim> {

  Domain *domainReference;
  DistRTree *distRTree;

 public:
  ImplicitSupermeshTsDesc(IoData&, GeoSource&, Domain *, RTree<Vec3D> *, DistRTree *, Domain *);
  virtual ~ImplicitSupermeshTsDesc();

  bool suppressOutput;
  int itSpecified;

  int& getMeshSnapshotIndex() {
    return this->meshSnapshotIndex;
  }

  int& getSnapStep() {
    return this->output->getSnapStep();
  }

  //int solveNonLinearSystem(DistSVec<double, dim>&, int, double);

  bool checkForLastIteration(int, double, double, DistSVec<double, dim>&);

  bool checkForAmrRefinement(int, bool);

  bool checkForAmrBalancing(int, bool, bool = true);

  void outputSnapshotsToDisk(IoData&, bool, int, int, double, DistSVec<double, dim>&);

  void printTimer();

  void adaptiveRepart(DistSVec<double, dim> *, DistSVec<double, dim> *, DistSVec<double, dim> *, DistSVec<double, dim> *,
                      DistSVec<double, dim> *, int);

  void outputMesh(int, int, double);

  void outputToDisk(bool *, int, int, int, double, double, DistSVec<double, dim>&);

};

//------------------------------------------------------------------------------

#endif

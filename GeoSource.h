#ifndef _GEO_SOURCE_H_
#define _GEO_SOURCE_H_

#include <map>
#include <set>
#include <vector>

class IoData;
class MatchNodeSet;
class SubDomain;
class Connectivity;
class Communicator;
class BinFileHandler;
class BCondSet;
class ElemSet;
class FaceSet;
class DistGeoState;
template<class Scalar> class Vec;

//------------------------------------------------------------------------------

class GeoSource {

  char *conName;
  char *geoName;
  char *decName;
  char *mapName;
  char *matchName;

  int numGlobSub, numCPU, numClusters;
  int numLocSub, cpuNum, numLocThreads;
  std::map<int,int> numSubCluster;

  Connectivity *clusterToSub;
  int (*subToCluster)[2];
  Connectivity *subToSub;
  Connectivity *cpuToSub;
  Connectivity *clusterToCpu;
  int *cpuToThreads;

  MatchNodeSet **matchNodes;

  double oolscale;
  IoData *iod;

  BCondSet *clus_bconds;

 public:

  GeoSource(IoData&, bool = false);
  ~GeoSource();

  void readConnectivityInfo(Communicator *);
  void readCpuToSub();
  int getRangeInfo(BinFileHandler&, int&, int ( *&)[2]);
  SubDomain *getSubDomain(int, int&);

  void openGeoFileForWriting(int, int, const char*);
  void openDecFileForWriting(int, int, const char*);
  void printSubDomainGeoFile(int, SubDomain *, int[3], int[3], int, Vec<int>&, Vec<int>&, const char*, bool = true);
  int getSubDomainDecFileOffset(int, SubDomain *, Vec<int>&);
  void printSubDomainDecFile(int, SubDomain *, int[3], int, int, Vec<int>&, const char*);
  void printConFile(int, const char*);
  void printMapFile(int, const char*);
  void writeWallDistanceToDisk(int, const char*, DistGeoState *);

  void connectSubDomains(const std::vector<std::set<int>>&);
  void disconnectSubDomains(const std::vector<std::set<int>>&);

  int getNumGlobSub() const {
    return numGlobSub;
  }
  int getNumLocSub() const {
    return numLocSub;
  }
  int getNumLocThreads() const {
    return numLocThreads;
  }
  Connectivity *getSubToSub() const {
    return subToSub;
  }
  Connectivity *getCpuToSub() const {
    return cpuToSub;
  }
  Connectivity *getClusterToCpu() const {
    return clusterToCpu;
  }
  Connectivity *getClusterToSub() const {
    return clusterToSub;
  }
  int (*getSubToCluster())[2] {
    return subToCluster;
  }
  int getNumClusters() const {
    return numClusters;
  }
  MatchNodeSet **getMatchNodes() const {
    return matchNodes;
  }
  template<class MapType>
  void distributeBCs(SubDomain *sub, MapType& cl2LocNodeMap);
  template<class MapType>
  void getBC(BCondSet *&subBC, MapType& cl2LocNodeMap);
};

//------------------------------------------------------------------------------

#endif

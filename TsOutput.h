#ifndef _TS_OUTPUT_H_
#define _TS_OUTPUT_H_

#include <NodeData.h>
#include <PostFcn.h>
#include <Types.h>
#include <Vector3D.h>
#include <DenseMatrix.h>
#include <string>
#include <vector>
#include <set>

class IoData;
class RefVal;
class Domain;
class Communicator;
class Timer;

template<class Scalar> class Vec;
template<class Scalar> class DistVec;
template<class Scalar, int dim> class DistSVec;
template<int dim> class PostOperator;
template<int dim> class DistTimeState;
template<int dim> class DistEmbeddedBcData;
template<int dim> class SpaceOperator;
template<int dim> class DistBcData;

//------------------------------------------------------------------------------

template<int dim>
class TsOutput {

  RefVal *refVal;
  PostOperator<dim> *postOp;
  Domain *domain;
  Communicator *com;

  bool steady;
  bool exactforces;
  int it0;
  double time0;
  int stepOffset;
  int frequency;
  int forces_frequency;
  int snap_frequency; // for nonlinear ROM snapshots
  int snap_step;      // for nonlinear ROM snapshots
  int w, p; // width and precision of floating point ascii output
  double frequency_dt, prtout;
  int numFluidPhases; // excludes "ghost" solids
  double length;
  double surface;
  double surfaceBQ;
  bool isFavre;
  bool appendMode;
  static int counter;
  // x00 is the initial value of the point about which moments are computed
  // x0 is the updated value (if desired; requires Corotational/Rigid flag)
  Vec3D x00;
  Vec3D x0;

  double sscale[PostFcn::SSIZE];
  double vscale[PostFcn::VSIZE];
  double tnscale[PostFcn::TNSIZE];
  double avsscale[PostFcn::AVSSIZE];
  double avvscale[PostFcn::AVVSIZE];
  double avtnscale[PostFcn::AVTNSIZE];
  double hsscale[PostFcn::HSSIZE];
  double tscale;
  double xscale;

  char *solutions[PostFcn::SOSIZE];
  char *scalars[PostFcn::SSIZE];
  char *vectors[PostFcn::VSIZE];
  char *tensors[PostFcn::TNSIZE];
  char *tensorsdev[PostFcn::TNSIZE];
  char *avscalars[PostFcn::AVSSIZE];
  char *avvectors[PostFcn::AVVSIZE];
  char *avtensors[PostFcn::AVTNSIZE];
  char *avtensorsdev[PostFcn::AVTNSIZE];
  char *hscalars[PostFcn::HSSIZE];

  bool forceAlgorithmBoth = false;
  char *bulkQuantities;
  char *forces;
  char *tavforces;
  char *hydrostaticforces;
  char *hydrodynamicforces;
  char *lift;
  char *ilift;
  char *vlift;
  char *tavlift;
  char *hydrostaticlift;
  char *hydrodynamiclift;
  char *residuals;
  char *materialvolumes;
  char *materialconservationscalars;
  char *conservation;
  char *cputiming;

  // for embedded framework
  char *embeddedsurfaceDisp;
  char *embeddedsurfaceVel;
  char *embeddedsurfaceCp;
  char *embeddedsurfaceCf;
  char *embeddedsurfaceTemp;
  char *embeddedsurfaceMach;
  char *embeddedsurfaceHeatFlux;
  char *embeddedsurfaceDeltaPlus;
  char *sumPsiMag;
  char *embeddedsurfaceDelta;
  char *embeddedsurfaceUtau;
  char *embeddedsurfaceDensity;
  char *inletDiskPressure;
  char *inletDiskPressureDistortion;
  char *inletDiskTotalPressureDistortion;

  // for sensitivity analysis
  DistVec<double> *Qs_match;
  DistVec<double> *Qs_match_opt;
  char *fullOptPressureName;
  bool optPressureDimensional;
  bool switchOpt;
  DistSVec<double, dim> *Uref;
  double Uref_norm;
  double dSscale[PostFcn::DSSIZE];
  double dVscale[PostFcn::DVSIZE];
  char *dScalars[PostFcn::DSSIZE];
  char *dVectors[PostFcn::DVSIZE];
  char *dSolutions;
  char *dMatchPressure;
  char *dForces;
  char *dModalForces;
  char *dLiftDrag;
  char *dLiftx;
  char *dLifty;
  char *dLiftz;
  char *dMomentx;
  char *dMomenty;
  char *dMomentz;
  char *dPressureDistortion;
  char *dFluxNorm;

  // for nonlinear ROM
  char *statesnapshots;
  char *statesnapshotshistUnm1;
  char *statesnapshotshistUnm2;
  char *snapshotinvrey;
  char *snapshottimesteps;
  char *snapshottimestepdata;
  char *statesnapshotsfreestream;
  char *reducedcoords;
  char *neighborinds;
  char *reducedresid;
  char *reducedjac;
  char *surfaceposition;
  char *surfacevelocity;
  char *sweptset;
  char *globalsweptset;

  Vec3D *TavF, *TavM, *TavL, *TavCp, *TavPTM;
  double *TavRhos, *TavRhoVels;

  double tprevf, tprevl, tprevbq, tinit;
  double tprevAvgS, tinitAvgS, tprevAvgV, tinitAvgV, tprevAvgT, tinitAvgT;
  double tener, tenerold;

  FILE **fpBulkQuantities;
  FILE **fpForces;
  FILE **fpLift;
  FILE **fpInviscidLift;
  FILE **fpViscousLift;
  FILE **fpTavForces;
  FILE **fpTavLift;
  FILE **fpHydroStaticForces;
  FILE **fpHydroDynamicForces;
  FILE **fpHydroStaticLift;
  FILE **fpHydroDynamicLift;
  FILE *fpResiduals;
  FILE *fpMatVolumes;
  FILE *fpMaterialConservationScalars;
  FILE *fpConservationErr;
  FILE *fpCpuTiming;

  // for embedded framework
  FILE *fpEmbeddedSurfaceDisp;
  FILE *fpEmbeddedSurfaceVel;
  FILE *fpEmbeddedSurfaceCp;
  FILE *fpEmbeddedSurfaceCf;
  FILE *fpEmbeddedROMPOS;
  FILE *fpEmbeddedROMVEL;
  FILE *fpSwept;
  FILE *fpEmbeddedSurfaceTemp;
  FILE *fpEmbeddedSurfaceMach;
  FILE *fpEmbeddedSurfaceHeatFlux;
  FILE *fpEmbeddedSurfaceDeltaPlus;
  FILE *fpSumPsiMag;
  FILE *fpEmbeddedSurfaceDelta;
  FILE *fpEmbeddedSurfaceUtau;
  FILE *fpEmbeddedSurfaceDensity;
  FILE *fpInletDiskPressure;
  FILE *fpInletDiskPressureDistortion;
  FILE *fpInletDiskTotalPressureDistortion;

  // for sensitivity analysis
  FILE *fpdMatchPressure;
  FILE *fpdForces;
  FILE *fpdModalForces;
  FILE *fpdLiftDrag;
  FILE *fpdLiftx;
  FILE *fpdLifty;
  FILE *fpdLiftz;
  FILE *fpdMomentx;
  FILE *fpdMomenty;
  FILE *fpdMomentz;
  FILE *fpdPressureDistortion;
  FILE *fpdFluxNorm;
  bool printHeader;

  // for nonlinear ROM
  FILE *fpSnapshotTimestepData;
  FILE *fpReducedCoords;
  FILE *fpNeighborInds;
  FILE *fpReducedResid;
  FILE *fpReducedJac;

  DistSVec<double, dim> *Qsol;    // state vector buffer
  DistVec<double> *Qs;            // scalar buffer
  DistSVec<double, 3> *Qv;        // vector buffer
  DistSVec<double, 3> *Qv2;       // vector buffer
  DistSVec<double, 6> *Qt;        // tensor buffer
  DistSVec<double, 9> *Qhs;       // hessian scalar buffer

  DistVec<double> *AvQs[PostFcn::AVSSIZE];
  DistSVec<double, 3> *AvQv[PostFcn::AVVSIZE];
  DistSVec<double, 6> *AvQt[PostFcn::AVTNSIZE];

  char *heatfluxes;
  FILE **fpHeatFluxes;

  struct {
    double *results;
    int numNodes;
    int *subId;
    int *locNodeId;
    int *last;
    int step;
    std::vector<Vec3D> locations;
  } nodal_output;
  char *nodal_scalars[PostFcn::SSIZE];
  char *nodal_vectors[PostFcn::VSIZE];

  struct line_output {
    int numPoints;
    double x0, y0, z0;
    double x1, y1, z1;
    char *scalars[PostFcn::SSIZE];
    char *vectors[PostFcn::VSIZE];
  };
  std::vector<line_output *> line_outputs;

 public:
  TsOutput(IoData&, RefVal *, Domain *, PostOperator<dim> *);
  ~TsOutput();

  bool toWrite(int, bool, double);
  bool toWriteForces(int, bool, double);
  bool toWriteSnaps(int, bool, double);
  int getStep(int, bool, double);

  int getStepOffset() {
    return stepOffset;
  }
  Vec3D& getX0() { return x0; }
  const Vec3D& getX0() const { return x0; }
  Vec3D& getX00() { return x00; }
  const Vec3D& getX00() const { return x00; }

  int& getSnapStep() {
    return snap_step;
  }
  bool& getAppendMode() {
    return appendMode;
  }

  DistVec<double> *getAvQs(int i) { return (i < PostFcn::AVSSIZE) ? AvQs[i] : nullptr; }
  DistSVec<double, 3> *getAvQv(int i) { return (i < PostFcn::AVVSIZE) ? AvQv[i] : nullptr; }
  DistSVec<double, 6> *getAvQt(int i) { return (i < PostFcn::AVTNSIZE) ? AvQt[i] : nullptr; }

  void updatePrtout(double);
  FILE *backupAsciiFile(char *);
  FILE *backupEmbeddedSurfaceAsciiFile(char *);
  void openAsciiFiles(DistEmbeddedBcData<dim> *, int = 0);
  void closeAsciiFiles();
  void closeSensitivityAsciiFiles();


  void writeForcesToDisk(bool, int, int, int, double, double *,
                         DistSVec<double, dim>&, DistSVec<double, 3> *);

  void writeTotalPressureDistortionToDisk(bool, int, int, int, double, DistSVec<double, dim>&);

  void writeHydroForcesToDisk(bool, int, int, int, double, double, double *,
                              DistSVec<double, dim>&, DistSVec<double, 3> *);

  void writeBulkQuantitiesToDisk(IoData&, bool, int, int, int, double, double, double *,
                        DistSVec<double, dim>&);

  void writeLiftsToDisk(IoData&, bool, int, int, int, double, double, double *,
                        DistSVec<double, dim>&, DistSVec<double, 3> *);

  void writeHydroLiftsToDisk(IoData&, bool, int, int, int, double, double, double *,
                             DistSVec<double, dim>&, DistSVec<double, 3> *);

  void writeResidualsToDisk(int, double, double, double);

  void writeMaterialVolumesToDisk(int, double, DistVec<double>&, DistVec<int> * = 0);

  void writeMaterialConservationScalarsToDisk(int, double, DistSVec<double, dim>&,  DistVec<double>&, DistVec<int> * = 0);

  void writeCPUTimingToDisk(bool, int, double, Timer *);

  void writeSweptSetToDisk(bool, DistVec<int> *);

  void writeEmbeddedSurfaceToDisk(bool, int, double, DistEmbeddedBcData<dim> *);

  void writeConservationErrorsToDisk(int, double, int, double **, double **);

  void writeReducedCoordsToDisk(bool, int, Vec<double>&, int);

  void writeNeighborIndsToDisk(bool, int, std::vector<int>&);

  void writeReducedResidToDisk(bool, int, Vec<double>&, int);

  void writeReducedJacToDisk(bool, int, DenseMat<double>&, int);

  void writeBinaryVectorsToDisk(bool, int, double, DistSVec<double, 3>& X, DistSVec<double, dim>&,
                                DistTimeState<dim> *, DistVec<double> *, DistSVec<double, dimLS> *, double[3]);

  void writeEmbeddedVectorsToDisk(bool, int, double, DistSVec<double, dim>&,
                                  DistEmbeddedBcData<dim> *, DistTimeState<dim> *, WallFcn *);

  void writeInletDiskQtyToDisk(bool, int, double, DistSVec<double,dim>&, DistTimeState<dim> *, int);

  void writeAvgVectorsToDisk(bool, int, double, DistSVec<double, 3>&, DistSVec<double, dim>&,
                             DistTimeState<dim> *, DistSVec<double, dimLS> *, double[3]);

  void writeSnapshotVectorsToDisk(IoData&, bool, int, int, DistSVec<double, dim>&, DistTimeState<dim> *,
                                  DistEmbeddedBcData<dim> *, SpaceOperator<dim> *, DistBcData<dim> *, bool);
  
  void writeSuperSnapshotVectorsToDisk(IoData&, bool, int, int, DistSVec<double, dim>&, DistTimeState<dim> *,
                                  DistEmbeddedBcData<dim> *, bool, Domain *);

  void writeProbesToDisk(bool, int, double, DistSVec<double, dim>&,
                         DistTimeState<dim> *, double[3], bool = false);

  void appendMeshOutputIndex(int, int, bool = false);
  void appendMeshSnapshotIndex(int, int);

  void resize(int, int, double, int, int);
  void resize1(NodeData *);
  void resize2();

  bool checkForGradP();

  void writeDerivativeOfMatchPressureToDisk(int, int, DistVec<double>&, DistSVec<double, 3>&, DistSVec<double, dim>&,
                                            DistTimeState<dim> *);
  void writeDerivativeOfFluxNormToDisk(int, int, double, double);
  void writeDerivativeOfLiftDragToDisk(int, int, Vec3D&, Vec3D&);
  void writeDerivativeOfLiftxToDisk(double, double * = nullptr);
  void writeDerivativeOfLiftyToDisk(double, double * = nullptr);
  void writeDerivativeOfLiftzToDisk(double, double * = nullptr);
  void writeDerivativeOfMomentxToDisk(double, double * = nullptr);
  void writeDerivativeOfMomentyToDisk(double, double * = nullptr);
  void writeDerivativeOfMomentzToDisk(double, double * = nullptr);
  void writeDerivativeOfPressureDistortionToDisk(double, double);
  void writeDerivativeOfForcesToDisk(int, int, Vec3D&, Vec3D&, Vec3D&, Vec3D&);
  void writeDerivativeOfModalForcesToDisk(int, int, Vec<double>&, Vec<double>&);

  void writeBinaryDerivativeOfVectorsToDisk(int, int, bool, DistSVec<double, 3>&, DistSVec<double, 3>&,
                                            DistSVec<double, dim>&, DistSVec<double, dim>&, DistTimeState<dim> *);

  // void computeAvgScalars(PostFcn::ScalarType, bool, int, double, DistSVec<double, 3>&, DistSVec<double, dim>&, DistVec<double>&,
  //                        DistTimeState<dim> *,  DistVec<double> *, DistSVec<double, dimLS> *, double[3]);

  // void computeAvgVectors(PostFcn::VectorType, bool, int, double, DistSVec<double, 3>&, DistSVec<double, dim>&, DistSVec<double, 3>&);

  // void computeAvgTensors(PostFcn::TensorType, bool, int, double, DistSVec<double, 3>&, DistSVec<double, dim>&, DistSVec<double, 6>&);

};

template<int dim>
int TsOutput<dim>::counter = 0;

//------------------------------------------------------------------------------

#endif

#ifndef _VISCO_FCN_H_
#define _VISCO_FCN_H_

#include <BcDef.h>
#include <IoData.h>

#include <cmath>

//------------------------------------------------------------------------------

class ViscoFcn {

 protected:
  const double twothirds = 2.0 / 3.0;

  double bulkViscosity;
  double viscosityRatio;
  bool zeroViscosityRatioAtWall;

 public:
  ViscoFcn(IoData& iod) {
    bulkViscosity = iod.eqs.viscosityModel.bulkViscosity;
    viscosityRatio = iod.eqs.viscosityModel.viscosityRatio;
    zeroViscosityRatioAtWall = (iod.eqs.viscosityModel.zeroViscosityRatioAtWall == ViscosityModelData::ON);
  }
  virtual ~ViscoFcn() {}

  virtual void rstVar(IoData& iod) {}

  virtual double compute_mu(double, int) = 0;                                     // returns μ(T;M) where T is the temperature and M is the freestream Mach number
  virtual double compute_dmu(double, int) = 0;                                    // returns ∂μ/∂T
  virtual double compute_muDerivative(double, double, bool, int) = 0;             // returns ∂μ/∂s given ∂T/∂s, ∂M/∂s where s is arbitrary
  virtual void compute_muDerivativeOperators(double, double&, double&, int) = 0;  // computes ∂μ/∂T and ∂μ/∂M

  double compute_lambda(double mu, int code) {
    // returns λ(μ)
    return (code == BC_INTERNAL || !zeroViscosityRatioAtWall) ? bulkViscosity + (viscosityRatio - twothirds) * mu
                                                              : bulkViscosity - twothirds * mu;
  }
  double compute_dlambda(double mu, int code) {
    // returns ∂λ/∂μ
    return (code == BC_INTERNAL || !zeroViscosityRatioAtWall) ? (viscosityRatio - twothirds)
                                                              : -twothirds;
  }
  double compute_lambdaDerivative(double mu, double dmu, int code) {
    // returns ∂λ/∂s given ∂μ/∂s
    return (code == BC_INTERNAL || !zeroViscosityRatioAtWall) ? (viscosityRatio - twothirds) * dmu
                                                              : -twothirds * dmu;
  }
  void compute_lambdaDerivativeOperators(double& dlambdadmu, int code) {
    // computes ∂λ/∂μ and ∂λ/∂M
    dlambdadmu = (code == BC_INTERNAL || !zeroViscosityRatioAtWall) ? (viscosityRatio - twothirds)
                                                                    : -twothirds;
  }

  virtual bool is_constant() = 0;
  virtual bool is_newtonian() = 0;

  static inline double computeStrainRate(double duidxj[3][3]) {
    const double& S00 = duidxj[0][0];
    const double& S11 = duidxj[1][1];
    const double& S22 = duidxj[2][2];
    const double D01 = duidxj[0][1] + duidxj[1][0];
    const double D02 = duidxj[0][2] + duidxj[2][0];
    const double D12 = duidxj[1][2] + duidxj[2][1];
    return sqrt(2 * (S00 * S00 + S11 * S11 + S22 * S22) + D01 * D01 + D02 * D02 + D12 * D12);
  }

};

//------------------------------------------------------------------------------

class ConstantViscoFcn : public ViscoFcn {

 public:
  ConstantViscoFcn(IoData& iod) : ViscoFcn(iod) {}

  double compute_mu(double T, int tag) {
    return 1.0;
  }

  double compute_dmu(double T, int tag) {
    return 0.0;
  }

  double compute_muDerivative(double T, double dT, bool dMach, int tag) {
    return 0.0;
  }

  void compute_muDerivativeOperators(double T, double& dmudT, double& dmudMach, int tag) {
    dmudT = 0.0;
    dmudMach = 0.0;
  }

  bool is_constant() {
    return true;
  }

  bool is_newtonian() {
    return true;
  }

};

//------------------------------------------------------------------------------

class SutherlandViscoFcn : public ViscoFcn {

  double Ts;
  std::vector<double> alpha;
  std::vector<double> dalpha;

 public:
  SutherlandViscoFcn(IoData& iod) : ViscoFcn(iod) {
    Ts = iod.eqs.viscosityModel.sutherlandReferenceTemperature / iod.ref.temperature;
    alpha.resize(iod.eqs.numPhase());
    dalpha.resize(iod.eqs.numPhase());
    for(int i = 0; i < iod.eqs.numPhase(); ++i) {
      if(iod.eqs.fluidModel(i).fluid != FluidModelData::PERFECT_GAS && iod.eqs.fluidModel(i).fluid != FluidModelData::STIFFENED_GAS) {
        std::cerr << "*** Error: Sutherland viscosity model is only available for perfect and stiffened gas equations of state\n";
        exit(-1);
      }
      double gam = iod.eqs.fluidModel(i).gasModel.specificHeatRatio;
      alpha[i] = gam * (gam - 1.0) * iod.ref.mach * iod.ref.mach;
      dalpha[i] = 2.0 * gam * (gam - 1.0) * iod.ref.mach;
    }
  }

  void rstVar(IoData& iod) {
    ViscoFcn::rstVar(iod);
    Ts = iod.eqs.viscosityModel.sutherlandReferenceTemperature / iod.ref.temperature;
    for(int i = 0; i < iod.eqs.numPhase(); ++i) {
      double gam = iod.eqs.fluidModel(i).gasModel.specificHeatRatio;
      alpha[i] = gam * (gam - 1.0) * iod.ref.mach * iod.ref.mach;
      dalpha[i] = 2.0 * gam * (gam - 1.0) * iod.ref.mach;
    }
  }

  double compute_mu(double Tadim, int tag) {
    double T = alpha[tag] * Tadim;
    return T * sqrt(T) * (1.0 + Ts) / (T + Ts);
  }

  double compute_dmu(double Tadim, int tag) {
    double T = alpha[tag] * Tadim;
    return alpha[tag] * (1.0 + Ts) * sqrt(T) * (1.5 * Ts + 0.5 * T) / ((T + Ts) * ( T + Ts));
  }

  double compute_muDerivative(double Tadim, double dTadim, bool dMach, int tag) {
    double dAlpha = dMach ? dalpha[tag] : 0.0;
    double T = alpha[tag] * Tadim;
    double dT = dAlpha * Tadim + alpha[tag] * dTadim;
    return dT * (1.0 + Ts) * sqrt(T) * (1.5 * Ts + 0.5 * T) / ((T + Ts) * (T + Ts));
  }

  void compute_muDerivativeOperators(double Tadim, double& dmudTadim, double& dmudMach, int tag) {
    double T = alpha[tag] * Tadim;
    double dmudT = (1.0 + Ts) * sqrt(T) * (1.5 * Ts + 0.5 * T) / ((T + Ts) * ( T + Ts));
    dmudTadim = dmudT * alpha[tag];
    dmudMach = dmudT * dalpha[tag] * Tadim;
  }

  bool is_constant() {
    return false;
  }

  bool is_newtonian() {
    return true;
  }

};

//------------------------------------------------------------------------------

class PrandtlViscoFcn : public ViscoFcn {

  std::vector<double> alpha;
  std::vector<double> dalpha;

 public:
  PrandtlViscoFcn(IoData& iod) : ViscoFcn(iod) {
    alpha.resize(iod.eqs.numPhase());
    dalpha.resize(iod.eqs.numPhase());
    for(int i = 0; i < iod.eqs.numPhase(); ++i) {
      if(iod.eqs.fluidModel(i).fluid != FluidModelData::PERFECT_GAS && iod.eqs.fluidModel(i).fluid != FluidModelData::STIFFENED_GAS) {
        std::cerr << "*** Error: Prandtl viscosity model is only available for perfect and stiffened gas equations of state\n";
        exit(-1);
      }
      double gam = iod.eqs.fluidModel(i).gasModel.specificHeatRatio;
      alpha[i] = gam * (gam - 1.0) * iod.ref.mach * iod.ref.mach;
      dalpha[i] = 2.0 * gam * (gam - 1.0) * iod.ref.mach;
    }
  }

  void rstVar(IoData& iod) {
    ViscoFcn::rstVar(iod);
    for(int i = 0; i < iod.eqs.numPhase(); ++i) {
      double gam = iod.eqs.fluidModel(i).gasModel.specificHeatRatio;
      alpha[i] = gam * (gam - 1.0) * iod.ref.mach * iod.ref.mach;
      dalpha[i] = 2.0 * gam * (gam - 1.0) * iod.ref.mach;
    }
  }

  double compute_mu(double Tadim, int tag) {
    return alpha[tag] * Tadim;
  }

  double compute_dmu(double Tadim, int tag) {
    return alpha[tag];
  }

  double compute_muDerivative(double Tadim, double dTadim, bool dMach, int tag) {
    double dAlpha = dMach ? dalpha[tag] : 0.0;
    return (dAlpha * Tadim + alpha[tag] * dTadim);
  }

  void compute_muDerivativeOperators(double Tadim, double& dmudTadim, double& dmudMach, int tag) {
    dmudTadim = alpha[tag];
    dmudMach = dalpha[tag] * Tadim;
  }

  bool is_constant() {
    return false;
  }

  bool is_newtonian() {
    return true;
  }

};

//------------------------------------------------------------------------------

class HerschelBulkleyViscoFcn : public ViscoFcn {

  double k;
  double n;
  double tau0;
  double mu0;
  double shearRate0;

 public:
  HerschelBulkleyViscoFcn(IoData& iod) : ViscoFcn(iod) {
    k = iod.eqs.viscosityModel.hb.consistency;
    n = iod.eqs.viscosityModel.hb.flowIndex;
    tau0 = iod.eqs.viscosityModel.hb.yieldStress;
    mu0 = iod.eqs.viscosityModel.hb.yieldingViscosity;
    shearRate0 = std::pow(tau0/mu0, n);
  }

  double compute_mu(double Tadim, int tag) {
    double shearRate = 0; // TODO: this should be a parameter of the method
    if(shearRate <= shearRate0) {
      return mu0;
    }
    else {
      return (tau0 + k * (std::pow(shearRate, n) - shearRate0)) / shearRate;
    }
  }

  double compute_dmu(double Tadim, int tag) {
    return 0; // TODO
  }

  double compute_muDerivative(double Tadim, double dTadim, bool dMach, int tag) {
    return 0; // TODO
  }

  void compute_muDerivativeOperators(double Tadim, double& dmudTadim, double& dmudMach, int tag) {
    dmudTadim = 0; // TODO
    dmudMach = 0; // TODO
  }

  bool is_constant() {
    return false;
  }

  bool is_newtonian() {
    return false;
  }

};

//------------------------------------------------------------------------------

#endif

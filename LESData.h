#ifndef _LES_DATA_H_
#define _LES_DATA_H_

template<class Scalar, int dim> class SVec;

//------------------------------------------------------------------------------

template<int dim>
class LESData {

  SVec<double, 2> *cs;
  SVec<double, dim> *vbar;

 public:
  LESData() : cs(nullptr), vbar(nullptr) {}
  ~LESData() {}

  void setCs(SVec<double, 2> *_cs) {
    cs = _cs;
  }
  void setVBar(SVec<double, dim> *_vbar) {
    vbar = _vbar;
  }
  SVec<double, 2> *getCs() const {
    return cs;
  }
  SVec<double, dim> *getVBar() const {
    return vbar;
  }

};

//------------------------------------------------------------------------------

#endif

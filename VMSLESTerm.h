#ifndef _VMS_LES_TERM_H_
#define _VMS_LES_TERM_H_

#include <LESTerm.h>
#include <IoData.h>

//------------------------------------------------------------------------------

class VMSLESTerm : public LESTerm {

  static constexpr double fourth = 1.0 / 4.0;

  double oogamma1;
  double csprime;
  double *VBar[4];

 public:
  VMSLESTerm(IoData&);
  ~VMSLESTerm() {}

  void setVBar(double *vbar[4]) {
    VBar[0] = vbar[0];
    VBar[1] = vbar[1];
    VBar[2] = vbar[2];
    VBar[3] = vbar[3];
  }

  double computeEddyViscosity(double *[4], double, double[3][3]);

  double computeDerivativeOfEddyViscosity(double *[4], double *[4], double, double,
                                          double[3][3], double[3][3]);

  void computeJacobianEddyViscosity(double *[4], double, double[3][3],
                                    double[4][3], double[4][5]);

  void computePrimeVariables(double *[4], double[4][3], double[4]);

  void computeReynoldsStresses(double *[4], double[4][6]);

  double computeTKE(double, double);

  //TODO: for Jacobian, need to account for dVPrimedU correctly (currently is VBar frozen)

};

//------------------------------------------------------------------------------

inline
VMSLESTerm::VMSLESTerm(IoData& iod) : LESTerm(iod) {
  oogamma1 = 1.0 / (iod.eqs.fluidModel(0).gasModel.specificHeatRatio - 1.0); // XXX
  csprime  = iod.eqs.tc.les.vms.c_s_prime;
}

//------------------------------------------------------------------------------

inline
double VMSLESTerm::computeEddyViscosity(double *V[4], double Delta, double duidxj[3][3]) {
  double rhobar = fourth * (VBar[0][0] + VBar[1][0] + VBar[2][0] + VBar[3][0]);
  double Sprime[3][3];
  Sprime[0][0] = duidxj[0][0];
  Sprime[1][1] = duidxj[1][1];
  Sprime[2][2] = duidxj[2][2];
  Sprime[0][1] = 0.5 * (duidxj[0][1] + duidxj[1][0]);
  Sprime[0][2] = 0.5 * (duidxj[0][2] + duidxj[2][0]);
  Sprime[1][2] = 0.5 * (duidxj[1][2] + duidxj[2][1]);
  Sprime[1][0] = Sprime[0][1];
  Sprime[2][0] = Sprime[0][2];
  Sprime[2][1] = Sprime[1][2];
  double Sprime2 = (Sprime[0][0] * Sprime[0][0] + Sprime[0][1] * Sprime[0][1] + Sprime[0][2] * Sprime[0][2] +
                    Sprime[1][0] * Sprime[1][0] + Sprime[1][1] * Sprime[1][1] + Sprime[1][2] * Sprime[1][2] +
                    Sprime[2][0] * Sprime[2][0] + Sprime[2][1] * Sprime[2][1] + Sprime[2][2] * Sprime[2][2]);
  return rhobar * (csprime * Delta) * (csprime * Delta) * sqrt(2.0 * Sprime2);
}

//------------------------------------------------------------------------------

inline
double VMSLESTerm::computeDerivativeOfEddyViscosity(double *V[4], double *dV[4], double Delta,
                                                    double dDelta, double duidxj[3][3],
                                                    double dduidxj[3][3]) {
  std::cerr << "*** Warning: VMSLESTerm::computeDerivativeOfEddyViscosity is not implemented\n";
  return 0;
}

//------------------------------------------------------------------------------

inline
void VMSLESTerm::computeJacobianEddyViscosity(double *V[4], double Delta, double duidxj[3][3],
                                              double dp1dxj[4][3], double dmut[4][5]) {
  //TODO
  for(int k = 0; k < 4; ++k) {
    for(int i = 0; i < 5; ++i) {
      dmut[k][i] = 0.0;
    }
  }
}

//------------------------------------------------------------------------------

inline
void VMSLESTerm::computePrimeVariables(double *V[4], double uprime[4][3], double Tprime[4]) {
  double u, v, w, T;
  // get primes for node 0
  uprime[0][0] = V[0][1] -  VBar[0][1];
  uprime[0][1] = V[0][2] -  VBar[0][2];
  uprime[0][2] = V[0][3] -  VBar[0][3];
  Tprime[0]    = oogamma1 * (V[0][4] / V[0][0] - VBar[0][4] / VBar[0][0]);
  // get primes for node 1
  uprime[1][0] = V[1][1] -  VBar[1][1];
  uprime[1][1] = V[1][2] -  VBar[1][2];
  uprime[1][2] = V[1][3] -  VBar[1][3];
  Tprime[1]    = oogamma1 * (V[1][4] / V[1][0] - VBar[1][4] / VBar[1][0]);
  // get primes for node 2
  uprime[2][0] = V[2][1] -  VBar[2][1];
  uprime[2][1] = V[2][2] -  VBar[2][2];
  uprime[2][2] = V[2][3] -  VBar[2][3];
  Tprime[2]    = oogamma1 * (V[2][4] / V[2][0] - VBar[2][4] / VBar[2][0]);
  // get primes for node 3
  uprime[3][0] = V[3][1] -  VBar[3][1];
  uprime[3][1] = V[3][2] -  VBar[3][2];
  uprime[3][2] = V[3][3] -  VBar[3][3];
  Tprime[3]    = oogamma1 * (V[3][4] / V[3][0] - VBar[3][4] / VBar[3][0]);
}

//------------------------------------------------------------------------------

inline
void VMSLESTerm::computeReynoldsStresses(double *V[4], double uprime[4][6]) {
  double u, v, w, T;
    // get primes for node 0
  uprime[0][0] = (V[0][1] * V[0][1]) -  (VBar[0][1] * VBar[0][1]);
  uprime[0][1] = (V[0][2] * V[0][2]) -  (VBar[0][2] * VBar[0][2]);
  uprime[0][2] = (V[0][3] * V[0][3]) -  (VBar[0][3] * VBar[0][3]);
  uprime[0][3] = (V[0][1] * V[0][2]) -  (VBar[0][1] * VBar[0][2]);
  uprime[0][4] = (V[0][1] * V[0][3]) -  (VBar[0][1] * VBar[0][3]);
  uprime[0][5] = (V[0][2] * V[0][3]) -  (VBar[0][2] * VBar[0][3]);
   // get primes for node 1
  uprime[0][0] = (V[1][1] * V[1][1]) -  (VBar[1][1] * VBar[1][1]);
  uprime[0][1] = (V[1][2] * V[1][2]) -  (VBar[1][2] * VBar[1][2]);
  uprime[0][2] = (V[1][3] * V[1][3]) -  (VBar[1][3] * VBar[1][3]);
  uprime[0][3] = (V[1][1] * V[1][2]) -  (VBar[1][1] * VBar[1][2]);
  uprime[0][4] = (V[1][1] * V[1][3]) -  (VBar[1][1] * VBar[1][3]);
  uprime[0][5] = (V[1][2] * V[1][3]) -  (VBar[1][2] * VBar[1][3]);
   // get primes for node 2
  uprime[0][0] = (V[2][1] * V[2][1]) -  (VBar[2][1] * VBar[2][1]);
  uprime[0][1] = (V[2][2] * V[2][2]) -  (VBar[2][2] * VBar[2][2]);
  uprime[0][2] = (V[2][3] * V[2][3]) -  (VBar[2][3] * VBar[2][3]);
  uprime[0][3] = (V[2][1] * V[2][2]) -  (VBar[2][1] * VBar[2][2]);
  uprime[0][4] = (V[2][1] * V[2][3]) -  (VBar[2][1] * VBar[2][3]);
  uprime[0][5] = (V[2][2] * V[2][3]) -  (VBar[2][2] * VBar[2][3]);
   // get primes for node 3
  uprime[0][0] = (V[3][1] * V[3][1]) -  (VBar[3][1] * VBar[3][1]);
  uprime[0][1] = (V[3][2] * V[3][2]) -  (VBar[3][2] * VBar[3][2]);
  uprime[0][2] = (V[3][3] * V[3][3]) -  (VBar[3][3] * VBar[3][3]);
  uprime[0][3] = (V[3][1] * V[3][2]) -  (VBar[3][1] * VBar[3][2]);
  uprime[0][4] = (V[3][1] * V[3][3]) -  (VBar[3][1] * VBar[3][3]);
  uprime[0][5] = (V[3][2] * V[3][3]) -  (VBar[3][2] * VBar[3][3]);
}

//------------------------------------------------------------------------------

inline
double VMSLESTerm::computeTKE(double nut, double Delta) {
  double uTKE = nut / (Delta * Delta * csprime * csprime);
  double k = (1.0 / 2.0) * uTKE * uTKE;
  return k;
}

//------------------------------------------------------------------------------

#endif

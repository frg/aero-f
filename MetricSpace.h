#ifndef _METRIC_SPACE_H_
#define _METRIC_SPACE_H_

#include <IoData.h>
#include <Vector.h>
#include <Vector3D.h>

#include <Eigen/Core>

#include <functional>
#include <vector>

class GeometricQuery;
class NodeSet;
template<class Scalar> class Vec;
template<class Scalar, int dim> class SVec;

//------------------------------------------------------------------------------

class QuadratureRule {
public:
  virtual ~QuadratureRule() {}
  virtual double integrate(std::function<double(double)>, double, double) = 0;
};

//------------------------------------------------------------------------------

class GaussRule : public QuadratureRule {
  const GaussRuleData& gaussRuleData;

  static const double gauss_points[14][14];
  static const double gauss_weights[14][14];

public:
  GaussRule(const GaussRuleData&);

  double integrate(std::function<double(double)>, double, double);
};

//------------------------------------------------------------------------------

class TrapezoidalRule : public QuadratureRule {
public:
  TrapezoidalRule();

  double integrate(std::function<double(double)>, double, double);
};

//------------------------------------------------------------------------------

class SimpsonsRule : public QuadratureRule {
public:
  SimpsonsRule();

  double integrate(std::function<double(double)>, double, double);
};

//------------------------------------------------------------------------------

class AdaptiveSimpsonsRule : public QuadratureRule {
  const AdaptiveSimpsonsRuleData& adaptiveSimpsonsRuleData;

public:
  AdaptiveSimpsonsRule(const AdaptiveSimpsonsRuleData&);

  double integrate(std::function<double(double)>, double, double);
};

//------------------------------------------------------------------------------

class MetricSpace {
  const PreferredDirectionData& preferredDirectionData;
  GeometricQuery& query;
  NodeSet& nodes;

  static const double gaussian_kernel[3][3][3];

  QuadratureRule *quadratureRule;

  Vec<double>& dists;    // nodal distances
  SVec<double, 3>& dirs; // nodal preferred directions
  SVec<double, 9>& mets; // nodal metric tensors

public:
  MetricSpace(const PreferredDirectionData&, GeometricQuery&, NodeSet&, Vec<double>&, SVec<double, 3>&, SVec<double, 9>&);
  ~MetricSpace();

  GeometricQuery& getSource() { return query; }

  Vec<double>& getDistances() { return dists; }
  SVec<double, 3>& getDirections() { return dirs; }
  SVec<double, 9>& getMetricTensors() { return mets; }

  double getDistance(int i) { return dists[i]; }
  Eigen::Map<Eigen::Vector3d> getDirection(int i) { return Eigen::Map<Eigen::Vector3d>(dirs[i]); }
  Eigen::Map<Eigen::Matrix3d> getMetricTensor(int i) { return Eigen::Map<Eigen::Matrix3d>(mets[i]); }

  void initializePoints(Vec<double> *, SVec<double, 1> *, SVec<double, 1> *, SVec<double, 1> *, SVec<double, 9> *);
  void reserve(int);
  void addPoint(int, int, int, double);
  Eigen::Matrix3d computeMetricTensor(int, int, double, std::vector<Vec3D> * = nullptr, std::vector<Eigen::Matrix3d> * = nullptr);
  Eigen::Matrix3d interpolateMetricTensor(int, int, double, std::vector<Eigen::Matrix3d> * = nullptr);
  double computeEdgeLength(int, int, std::vector<Vec3D> * = nullptr, std::vector<Eigen::Matrix3d> * = nullptr);
  double computeEdgeMidpoint(int, int);

private:
  Eigen::Matrix3d metric_tensor(const Eigen::Vector3d&);
  Eigen::Matrix3d distance_direction_and_metric_tensor(Vec3D&, double&, Eigen::Vector3d&);
  Eigen::Matrix3d evaluate_metric_tensor(const Vec3D&, const Vec3D&, double);
  Eigen::Matrix3d interpolate_metric_tensor(const Eigen::Matrix3d&, const Eigen::Matrix3d&, double,
                                            PreferredDirectionData::MetricInterpolation metric_interpolation,
                                            PreferredDirectionData::SimultaneousReductionScheme simultaneous_reduction_scheme);
  double integrate_length(const Vec3D&, const Vec3D&, const Eigen::Matrix3d&, const Eigen::Matrix3d&);
};

//------------------------------------------------------------------------------

#endif

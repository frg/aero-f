#ifndef _FLOODFILL_H_
#define _FLOODFILL_H_

#include <set>
#include <map>

#include <Mpi_Utilities.h>

class Communicator;
class Domain;
class SubDomain;
class EdgeSet;

template<class Scalar> class DistVec;
template<class Scalar> class Vec;
template<class Scalar, int dim> class SVec;

using PhysBAM::GLOBAL_SUBD_ID;

class FloodFill {
 private:
  // data useful for distributing the color map -- populated by generateSubDToProcessorMap(...)
  GLOBAL_SUBD_ID nGlobalSubDomains;
  int *subDomainToProcessorMap;
  std::set<std::pair<std::pair<GLOBAL_SUBD_ID, int>, std::pair<GLOBAL_SUBD_ID, int>>> connections;

 public:
  FloodFill() : nGlobalSubDomains(-1), subDomainToProcessorMap(0) {
  }

  ~FloodFill() {
    delete [] subDomainToProcessorMap;
  }

  void generateSubDToProcessorMap(Domain&, Communicator&);

  void generateConnectionsSet(Domain&, Communicator&, DistVec<int>&);

  void unionColors(Domain&, Communicator&, const int, const int *, std::map<std::pair<PhysBAM::GLOBAL_SUBD_ID, int>, int>&);

  static int floodFillSubDomain(SubDomain&, EdgeSet&, const Vec<bool>&, const Vec<bool>&, Vec<int>&);

};

#endif

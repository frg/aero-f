#ifndef _INTERSECTORPHYSBAM_H_
#define _INTERSECTORPHYSBAM_H_

#include <list>
#include <map>
#include <set>

#include <Edge.h>
#include <LevelSet/LevelSetStructure.h>
#include <Types.h>
#include <Vector.h>

#include "PHYSBAM_INTERFACE.h"
#include <PhysBAM_Geometry/Geometry_Particles/GEOMETRY_PARTICLES.h>
#include <PhysBAM_Geometry/Topology/TRIANGLE_MESH.h>

#define MAXLINE 500

using PhysBAM::PhysBAMInterface;
using PhysBAM::ARRAY;
using PhysBAM::PAIR;
using PhysBAM::VECTOR;
using PhysBAM::IntersectionResult;

class Vec3D;
class Communicator;
class Connectivity;
class IntersectorPhysBAM;
class FloodFill;
class SubDomain;
class EdgeSet;
class Timer;
class IoData;
class CrackingSurface;
class NodeSet;
class EmbeddedStructure;
class TriangleSet;

class SymmetryInfo {
 public:
  double xCoordinate;
  double yCoordinate;
  double zCoordinate;
  Vec3D normal;
  SymmetryInfo(double x, double y, double z, Vec3D normal_) {
    xCoordinate = x;
    yCoordinate = y;
    zCoordinate = z;
    normal = normal_;
  }
};

template<class Scalar, int dim> class SVec;

class DistIntersectorPhysBAM : public DistLevelSetStructure {

  friend class IntersectorPhysBAM;
  friend struct LevelSetResult;
  friend struct PartialLevelSetResult;

  typedef std::pair<int, int> iipair;
  typedef std::pair<int, bool> ibpair;
  typedef std::pair<iipair, ibpair> EdgePair;

  using DistLevelSetStructure::status;
  using DistLevelSetStructure::is_swept;
  using DistLevelSetStructure::is_active;
  using DistLevelSetStructure::is_occluded;
  using DistLevelSetStructure::edge_intersects;
  using DistLevelSetStructure::edge_intersects_constraint;
  using DistLevelSetStructure::distance;
  using DistLevelSetStructure::ddistance;
  using DistLevelSetStructure::closest;
  using DistLevelSetStructure::dclosest;

 protected:
  IoData& iod;
  bool myMemory;

  int numStNodes, numStElems;
  int nqPoint; // number of quadrature points
  int totStNodes, totStElems;
  bool gotNewCracking;
  double xMin, xMax, yMin, yMax, zMin, zMax; // a bounding box over the struct body
  DistSVec<double, 3> *boxMax, *boxMin; // fluid node bounding boxes
  DistSVec<double, 3> *expandedBoxMax, *expandedBoxMin; // expanded fluid node bounding boxes for FIVER+ (hybrid ndoes)

  FloodFill *floodFill;

  // closest point to CFD grid-points
  DistVec<int> *trIds;

  // interpolated signed distance to edge midpoint and "mode" which mimics the mode of closest
  DistVec<int> *closest_to_edge_mode;
  DistVec<Vec3D> *closest_to_edge_vel;
  DistVec<Vec3D> *dclosest_to_edge_vel;
  DistVec<int> *min_distance_other_node;
  DistVec<int> *max_distance_other_node;
  DistVec<double> *average_edge_length;
  void initializeAverageEdgeLength();

  // struct node coords
  Vec<Vec3D> Xs;
  Vec<Vec3D> *Xs0;
  Vec<Vec3D> *Xs_n;
  Vec<Vec3D> *Xs_np1;
  Vec3D *dXs; // for sensitivities
  Vec3D *dXsdot; // for errorcontrolled intersector updates
  Vec3D *Xs_last_recompute;
  Vec3D *Xdots_last_recompute;
  int nSb = 0; // number of shape variables considered
  int *wetted_structural_elems;

  std::map<int, BoundaryData::Type> boundaryConditionsMap;
  int *faceID;
  int maxFaceID;

  // embedded constraints information
  enum BoundaryData::Type *structureType;
  int *integration;
  double *wallTemperature;
  int *heatFluxType;
  double *porosity;
  int *porosityUpdateMethod;
  int *poreShape;
  double *poissonRatio;
  double *thickness;
  double *weavingLength;
  int *actuatorDiskMethod;
  double *actuatorDiskPressureJump;
  int *actuatorDiskReconstructionMethod; // method used to compute the velocity at the interface. 1 : vi+Vj/2 2:vi 3 Vi*grad (extrapolation)
  double *outflowConditions;
  double gamma; // used for the actuatorDiskComputaion

  // surface rotation
  int *surfaceID;
  int *rotOwn;

  int (*stElem)[3]; // structure elements (topology)

  Vec3D *Xsdot; // velocity
  Vec3D *Xsdot_n;
  Vec3D *Xsdot_np1;

  CrackingSurface *cracking;
  std::map<int, SymmetryInfo> SymmetryPlaneList; // map of all the symmetry planes in the file to decide which nodes must be inactive

  int numberOfWallNodes;
  int *nodeType;

  double interface_thickness;

  bool hybridNodes;
  bool thinStructure;
  bool unwettedStructureComponents; // flag for if parts of the embedded structure are in the inactive part of the fluid (like an internal rib) and should be ignored

  bool isoThermalwall;
  double Twall;

  bool withViscousTerms;

  DistVec<int> *status0, *status_nmp; // previous node status
  DistVec<bool> *occluded_node0, *is_occluded_nmp; // previous occluded node status

  Vec3D *triNorms;
  double *triAreas;
  double *triAreas0;
  Vec3D *nodalNormal; // memory allocated only if interpolatedNormal == true

  // parameters from input
  bool interpolatedNormal;
  bool weightByAngle;

  Communicator *com;
  IntersectorPhysBAM **intersector;
  PhysBAMInterface<double> *physInterface;
  Domain *domain;

  bool sensitivities = false;
  bool checkGhostPointReflection = false;

  std::unordered_set<int> *scopeStNodes = 0;
  std::unordered_set<int> *scopeStElems = 0;

  TriangleSet *triangleSet;

 public:
  DistIntersectorPhysBAM(IoData&, Communicator *, EmbeddedStructure * = 0);
  ~DistIntersectorPhysBAM();

  void resize(NodeData *);

  void buildSolidNormals(bool = false);
  void getBoundingBox();

  void findActiveNodesUsingFloodFill(DistSVec<double, 3>&, const DistVec<bool>&, const std::list<std::pair<Vec3D, int>>&, bool);
  void findActiveNodesUsingIntersections(DistSVec<double, 3>&, const DistVec<bool>&, const std::list<std::pair<Vec3D, int>>&, bool);
  void findActiveNodes(DistSVec<double, 3>&, const DistVec<bool>&);

  void init(char *, char *, double);
  void init(EmbeddedStructure *);
  void setPorosity();
  void setStructureType();
  void setWallInformation();
  void getSymmetryPlanesInformation();
  void setOutflow();
  void setActuatorDisk();
  void setNodeType();
  void checkInputFileCorrectnessEmbeddedConstraint();
  void makerotationownership();
  void initbc();
  void updatebc();

  EdgePair makeEdgePair(int, int, int);
  bool checkTriangulatedSurface();
  void initializePhysBAM();

  void initialize(Domain *, IoData&, DistSVec<double, 3>&, DistVec<int> * = 0, DistVec<int> * = 0);
  void updateStructure(double *, double *, int, int (*)[3] = 0, bool = false);

  void updateCracking(int (*)[3]);
  void expandScope();
  void updatePhysBAMInterface(Vec<Vec3D>&, int, DistSVec<double, 3>&, const bool, const bool);
  void setXsLastRecompute();
  int recompute(DistSVec<double, 3>&, double, double, double, int, bool = false, bool = false, bool = true);
  int partialRecompute(double, double, double);
  void getMaskVec(DistSVec<double, 3>&, DistVec<bool>&, bool = true);
  void detectWettedStructuralElems();
  void computeClosestDataWithHybridNodes(DistSVec<double, 3>&, DistVec<ARRAY<int>>&);
  void checkReflectedGhostPointHasActiveTet(DistSVec<double, 3>&, DistVec<ARRAY<int>>&);
  void computeQuantitiesForHybridNodes();
  void assembleClosestNodeData(DistSVec<double, 3>&, DistVec<ARRAY<int>>* = nullptr);

  void initializePredictor();
  void initializeCorrector();

  int countActive();
  int countOccluded();
  int countSwept();
  int countActiveSwept();

  void perturbStructureWithdXs(double);
  void perturbMinDistance(double);
  void perturbHybridQuantities(double);
  void setdXs(int, double *, double *, double *);
  void setdXdtAsdXs(double, double, double);
  void computeDerivatives(double, DistSVec<double, 3>&);

  LevelSetStructure& operator()(int) const;

  PhysBAMInterface<double>& getInterface() {
    return *physInterface;
  }
  Vec3D *getSurfaceNorms() {
    return triNorms;
  }
  Vec3D& getSurfaceNorm(int i) {
    return triNorms[i];
  }
  double& getSurfaceArea(int i) {
    return triAreas[i];
  }
  double& getSurfaceArea0(int i) {
    return triAreas0[i];
  }
  double getAreaRatio(int i) {
    double areaRatio;
    if(triAreas[i]<1e-8*triAreas0[i]) {
     fprintf(stderr, "*** Warning: Triangle area is close to zero. Setting areaRatio to 1e-8\n");
     areaRatio = 1e-8;
    }
    else {
      areaRatio = triAreas[i]/triAreas0[i];
    }
    return areaRatio;
  }
  double getCurrentPorosity(int, double, double, int);
  double getCurrentWeavingLength(int, double, int);
  const Vec3D& getNodalNorm(int i) const {
    if(!nodalNormal) {
      fprintf(stderr, "*** Error: nodal normal not initialized!\n");
      exit(-1);
    }
    return nodalNormal[i];
  }
  Vec<Vec3D>& getStructPosition() {
    return Xs;
  }
  Vec3D* getdStructPosition() {
    return dXs;
  }
  Vec<Vec3D>& getStructPosition_0() {
    return (Xs0) ? *Xs0 : Xs;
  }
  Vec<Vec3D>& getStructPosition_n() {
    return (Xs_n) ? *Xs_n : Xs;
  }
  Vec<Vec3D>& getStructPosition_np1() {
    return (Xs_np1) ? *Xs_np1 : Xs;
  }
  void setStatus(DistVec<int>& nodeTag) {
    *status = nodeTag;
  }
  void setStatus0(DistVec<int>& nodeTag) {
    *status0 = nodeTag;
  }
  void setIsOccluded(DistVec<bool>& nodeFlag) {
    *is_occluded = nodeFlag;
  }
  void setIsOccluded0(DistVec<bool>& nodeFlag) {
    *occluded_node0 = nodeFlag;
  }
  void setStructPosition(const Vec<Vec3D>& _Xs) {
    Xs = _Xs;
  }
  void setStructPosition_np1(const Vec<Vec3D>& _Xs_np1) {
    (*Xs_np1) = _Xs_np1;
  }
  int getNumStructNodes() {
    return numStNodes;
  }
  int getNumStructElems() {
    return numStElems;
  }
  int getNumQuadPoints() {
    return nqPoint;
  }
  int (*getStructElems())[3] {
    return stElem;
  }
  enum BoundaryData::Type getElemType(int i) {
    return (faceID && structureType) ? structureType[faceID[i]] : BoundaryData::UNSPECIFIED;
  }
  int *getNodesType() {
    return nodeType;
  }
  Vec3D *getStructVelocity() {
    return Xsdot;
  }
  Vec3D *getStructVelocity_n() {
    return Xsdot_n;
  }
  Vec3D *getStructVelocity_np1() {
    return Xsdot_np1;
  }
  DistVec<int> *getStatus_nmp() const {
    return status_nmp;
  }
  DistVec<bool> *getIsOccluded_nmp() const {
    return is_occluded_nmp;
  }

  int *getSurfaceID() {
    return surfaceID;
  }

  int *getFaceID() {
    return faceID;
  }

  int getSurfaceID(int k) {
    if(!surfaceID) {
      return 0;
    }
    if(k >= 0 && k < numStNodes) {
      return surfaceID[k];
    }
    else {
      fprintf(stderr, "*** Error: SurfaceID requested for invalid point.\n");
      exit(-1);
    }
  }

  int getFaceID(int e) {
    if(!faceID) {
      return 0;
    }
    if(e >=0 && e < numStElems) {
      return faceID[e];
    }
    else {
      fprintf(stderr, "*** Error: FaceID requested for invalid element.\n");
      exit(-1);
    }
  }

  void restrictToGlobalBoundingBox(Vec3D& reflected_point) {
    for(int i = 0; i < 3; i++) {
      reflected_point[i] = reflected_point[i] < bbMin[i] ? bbMin[i] : reflected_point[i] > bbMax[i] ? bbMax[i] : reflected_point[i];
    }
  }

  double project(Vec3D&, int, double&, double&) const;
};

class IntersectorPhysBAM : public LevelSetStructure {

  friend class DistIntersectorPhysBAM;

  using LevelSetStructure::status;
  using LevelSetStructure::is_swept;
  using LevelSetStructure::is_active;
  using LevelSetStructure::is_occluded;
  using LevelSetStructure::edge_intersects;
  using LevelSetStructure::edge_intersects_constraint;
  using LevelSetStructure::distance;
  using LevelSetStructure::ddistance;
  using LevelSetStructure::closest;
  using LevelSetStructure::dclosest;

 public:
  static const int OUTSIDE = -2, UNDECIDED = -1, INSIDE = 0; // INSIDE: inside real fluid, OUTSIDE: not a fluid
  static int OUTSIDECOLOR;
  static int INSIDECOLOR;

  int locIndex, globIndex;
  const std::vector<int_t>& locToGlobNodeMap;

  std::vector<IntersectionResult<double>> CrossingEdgeRes;
  std::vector<IntersectionResult<double>> ReverseCrossingEdgeRes;

  virtual CrackingSurface *getCrackingSurface() {
    return distIntersector.cracking;
  }

  SubDomain& subD;
  EdgeSet& edges;
  DistIntersectorPhysBAM& distIntersector;

  std::unordered_set<int> *package, *package0;
  std::map<int, int> sub2pack;

  Vec<int>& status0; // status at the previous time-step.
  Vec<int>& trIds;
  Vec<bool>& occluded_node0; // occluded node status at the previous time-step.
  int nFirstLayer;
  ARRAY<int> reverse_mapping, forward_mapping;
  ARRAY<VECTOR<double, 3>> xyz;

  Vec<int> *closest_to_edge_mode;
  Vec<Vec3D> *closest_to_edge_vel;
  Vec<Vec3D> *dclosest_to_edge_vel;
  Vec<int> *max_distance_other_node;
  Vec<int> *min_distance_other_node;
  Vec<double> *average_edge_length;

  bool& sensitivities;

 public:
  IntersectorPhysBAM(SubDomain&, Vec<int>&, Vec<int>&, Vec<bool>&, Vec<int> *, Vec<int> *, Vec<int> *, Vec<Vec3D> *, Vec<Vec3D> *,
                     Vec<double> *, DistIntersectorPhysBAM&);
  virtual ~IntersectorPhysBAM();

  void resize();

  void setInactiveNodesSymmetry(SVec<double, 3>&, std::map<int, SymmetryInfo>);

  int hasCloseTriangle(SVec<double, 3>&, SVec<double, 3>&, SVec<double, 3>&, Vec<bool>&, Vec<ARRAY<int>>&);
  void findEdgeClosestPoint(const int, Vec3D&, ARRAY<int>&);
  int findIntersections(Vec<bool>&, Communicator&);
  int computeSweptNodes(Vec<bool>&, Communicator&);
  void findIntersection(int, const Vec3D&, bool, IntersectionResult<double>&);
  void computeMinAndMaxDistances();
  void computeMinAndMaxDistanceDerivatives(Vec<double>&, Vec<double>&);

  void reset(bool, bool = true); // set status0 = status, and reset status and nFirstLayer.
  void printFirstLayer(SubDomain&, NodeSet&, int = 1);

  LevelSetResult getLevelSetDataAtEdgeCenter(double, int, bool);
  PartialLevelSetResult getPartialLevelSetDataAtEdgeCenter(double, int, bool);

  bool InterfacePointDist(const int, double&);

  void getClosestPoint(int, Vec3D&, Vec3D&) const;
  void getClosestPointAndNormal(int, Vec3D&, Vec3D&, Vec3D&) const;
  void getdClosestPoint(int, Vec3D&, Vec3D&) const;

  bool testIsActive(double t, int n) const {
    return (status[n] >= 0 && status[n] != OUTSIDECOLOR);
  }
  int numOfFluids() {
    return distIntersector.numOfFluids();
  }
  bool isNearInterface(double t, int n) const {
    return (*closest)[n].nearInterface();
  }
  bool withCracking() const {
    return distIntersector.cracking ? true : false;
  }

  void nWall_node(int n, Vec3D& nWall) { nWall = (*closest)[n].normal; }
  void dnWall_node(int n, Vec3D& dnWall) { dnWall = (*dclosest)[n].dnormal; }
  void vWall_edge(int l, Vec3D& vWall) { vWall = (*closest_to_edge_vel)[l];};
  void dvWall_edge(int l, Vec3D*& dvWall) { dvWall = dclosest_to_edge_vel ? &(*dclosest_to_edge_vel)[l] : nullptr;};
  
  bool getTwall(double& Tw) {
    Tw = distIntersector.Twall;
    return distIntersector.isoThermalwall;
  }

  double getInterfaceThickness() const {
    return distIntersector.interface_thickness;
  }

  int getSurfaceID(int k) {
    return distIntersector.getSurfaceID(k);
  }

  int getFaceID(int e) {
    return distIntersector.getFaceID(e);
  }

  std::unordered_set<int>& getScope() {
    return distIntersector.getInterface().getScope(locIndex+1);
  }

  bool edgeIntersectsWall2(double, int, double) const;

  // for intersection with specified wall surface only
  bool edgeIntersectsWall(double, int, int) const;
  bool edgeIntersectsWall2(double, int, double, int) const;

  void initializeAverageEdgeLength(Vec<int>&);

 private:
  void addToPackage(const int, const int);
  void findNodeClosestPoint(const int, Vec3D, ARRAY<int>&);
  void findNodeClosestPointDerivativeFiniteDifference(const int, Vec3D&, double);
  double project(Vec3D&, int, double&, double&) const;
  double edgeProject(Vec3D&, Vec3D&, Vec3D&, double&) const;
  double edgeProject(Vec3D&, int, int, double&) const;

};

//-------------------------------------------------------------------------------
/* If node n is at the interface i.e. an adjacent edge intersects the interface
 * return true and update its distance to wall in d2w
 * otherwise return false and do not update d2w
 */
inline
bool IntersectorPhysBAM::InterfacePointDist(const int n, double& d2w) {
  if((*closest)[n].mode < 0) return false;
  d2w = (*distance)[n];
  return true;
}

//-------------------------------------------------------------------------------

#endif

#ifndef _MODAL_H_
#define _MODAL_H_

#include <IoData.h>
#include <DistVector.h>
#include <VectorSet.h>

#include <complex>

#ifdef DO_MODAL
template<class Scalar> class ARluSymStdEig;
#endif

class Communicator;
class Domain;
class DistGeoState;
class IoData;
class TsInput;
class TsRestart;

template<int dim> class DistBcData;
template<int dim, class Scalar> class DistNodalGrad;
template<int dim> class DistTimeState;
template<int dim> class PostOperator;
template<int dim> class SpaceOperator;
template<int dim> class TsOutput;
template<int dim> class ErrorDynamicsBlockVec;
template<int dim> class ErrorDynamicsMatVecProd;
template<int dim> class ErrorDynamicsPrec;
class ErrorDynamicsInfo;
class ControlInterface;
template<class Scalar> class DenseMat;
template<int dim> class MatVecProd_dRdX;
template<int dim> class CtrlFocusedTrainingMatVecProd;

#ifndef _MATVECPROD_TMPL_
  #define _MATVECPROD_TMPL_
  template<int dim, int neq, class Scalar2 = double> class MatVecProd;
#endif

#ifndef _MATVECPROD2_TMPL_
  #define _MATVECPROD2_TMPL_
  template<int dim, int neq, class Scalar2 = double> class MatVecProdFD;
  template<int dim, class Scalar, int neq, class Scalar2 = double> class MatVecProdH1;
  template<int dim, class Scalar, int neq, class Scalar2 = double> class MatVecProdH2;
  template<int dim, class Scalar, class Scalar2 = double> class MatVecProdLS;
#endif

#ifndef _KSPPREC_TMPL_
  #define _KSPPREC_TMPL_
  template<int Dim, class Scalar2 = double> class KspPrec;
#endif

#ifndef _KSPSLVR_TMPL_
  #define _KSPSLVR_TMPL_
  template<class VecType, class MvpOp, class PrecOp, class IoOp, class ScalarT = double> class KspSolver;
#endif

#ifndef _KSPSLVR2_TMPL_
  #define _KSPSLVR2_TMPL_
  template<class VecType, class MatVecProdOp, class PrecOp, class IoOp, class ScalarT = double> class GmresSolver;
  template<class VecType, class MatVecProdOp, class PrecOp, class IoOp, class ScalarT = double> class GcrSolver;
  template<class VecType, class MatVecProdOp, class PrecOp, class IoOp, class ScalarT = double> class AugmentedGmresSolver;
#endif

//-----------------------------------------------------------------------------------

template<int dim>
class ModalSolver {

  Communicator *com;
  Domain& domain;
  int nStrMode;
  int nPadeDeriv;
  IoData *ioData;
  DistBcData<dim> *bcData;
  DistGeoState *geoState;
  DistTimeState<dim> *tState;
  TsInput *tInput;
  TsOutput<dim> *tOutput;
  TsRestart *tRestart;

  SpaceOperator<dim> *spaceOp;
  PostOperator<dim> *postOp;
  MatVecProdH2<dim, double, dim, double> *HOp, *HOp2step1, *HOpstep2, *HOpstep3;
  MatVecProdH2<dim, std::complex<double>, dim, std::complex<double>> *HOpC;

  KspSolver<DistSVec<double, dim>, MatVecProd<dim, dim, double>,
            KspPrec<dim, double>, Communicator> *ksp, *ksp2, *ksp3;

  KspSolver<DistSVec<std::complex<double>, dim>, MatVecProd<dim, dim, std::complex<double>>,
            KspPrec<dim, std::complex<double>>, Communicator, std::complex<double>> *kspComp;

  GcrSolver<DistSVec<std::complex<double>, dim>, MatVecProd<dim, dim, std::complex<double>>,
            KspPrec<dim, std::complex<double>>, Communicator, std::complex<double>> *kspCompGcr;

  KspPrec<dim, double> *pc;
  KspPrec<dim, std::complex<double>> *pcComplex;
  KspData kspData;

  VecSet<DistSVec<double, 3>> mX;
  DistSVec<double, dim> Uref;

  DistSVec<double, 3> Xref;
  double dt;
  double dt0;
  double *K;
  VecSet<DistSVec<double, dim>> DX;
  VecSet<DistSVec<double, dim>> DE;
  DistVec<double> controlVol;
  DistVec<std::complex<double>> controlVolComp;
  double totalEnergy;
  DistSVec<double, 3> *gradP;
  DistSVec<double, dim> *V;

  double r;
  double volCoef;
  double an;
  double anm1;

  int numSamples;
  double dt_sample;
  VecSet<DistSVec<double, dim>> altitudeSamples;
  VecSet<DistSVec<double, dim>> sampleFluxes;
  DistSVec<double, dim> Wref;

  ControlInterface *userSupFunc;
  DenseMat<double> mass;
  DenseMat<double> stiffness;
  DenseMat<double> op0;
  DenseMat<double> Kop0;
  DenseMat<double> op;
  DenseMat<double> Kop;

  MatVecProd_dRdX<dim> *dRdXop;
  DistSVec<double, 3> *dX;

  DenseMat<double> dForcedX;

  int nCtrlSurfMode;
  VecSet<DistSVec<double, 3>> mXcs;
  VecSet<DistSVec<double, dim>> DXcs;
  VecSet<DistSVec<double, dim>> DEcs;
  DenseMat<double> dForcedXcs;
  // computed as X^T*K*X_CS, i.e. the coupling between the control surfaces and the standard structural modes
  DenseMat<double> stiffnessCS;

  int nCtrlInputs;
  DenseMat<double> dForcedCtrl;

  Vec<double> trim;
  DistSVec<double, dim> *dWref_trim;

  // used for weighting individual modes during frequency domain training
  Vec<double> modeGroupAverageNorms;
  Vec<int> numGroupSnaps;
  Vec<int> snapshotGroupID;
  Vec<int> modeGroupID;

 private:
  void readModeGroups(int, int);
  int readSnapshotData(std::vector<std::string>&, std::vector<std::tuple<int, int, int, double, int, int>>&);
  void computeModalFluidOperators(VecSet<DistSVec<double, 3>>&, VecSet<DistSVec<double, dim>>&, VecSet<DistSVec<double, dim>>&, int, bool, double *);

 public:
  static void readModes(IoData *, Domain&, const char *, int&, VecSet<DistSVec<double, 3>>&, bool = true, double ** = nullptr);

  ModalSolver(Communicator *, IoData&, Domain&);
  ~ModalSolver();

  void solve();

  MatVecProdH2<dim, std::complex<double>, dim, std::complex<double>> *getHOpC() { return HOpC; }
  VecSet<DistSVec<double, dim>>& getDX() { return DX; }
  VecSet<DistSVec<double, dim>>& getDE() { return DE; }
  DenseMat<double>& getMass() { return mass; }
  DenseMat<double>& getStiffness() { return stiffness; }
  MatVecProd_dRdX<dim> *getdRdXop() { return dRdXop; }
  DistSVec<double, 3> *getdX() { return dX; }
  VecSet<DistSVec<double, 3>>& getmX() { return mX; }
  PostOperator<dim> *getPostOp() { return postOp; }
  Domain& getDomain() { return domain; }
  Communicator *getCom() { return com; }
  IoData *getIoData() { return ioData; }
  KspPrec<dim, std::complex<double>> *getPcComplex() { return pcComplex; }

 private:
  void preProcess();
  void createPODInTime(VecSet<DistSVec<double, dim>>&, int&);
  void constructPOD();
  void solveInTimeDomain();
  void timeIntegrate(VecSet<DistSVec<double, dim>>&, int, int, double *, double *, int&, double, char * = 0);
  void freqIntegrate(VecSet<DistSVec<double, dim>>&, double, int&, VecSet<DistSVec<std::complex<double>, dim>>&);
  void freqIntegrateMultipleRhs(VecSet<DistSVec<double, dim>>&, double, int&, VecSet<DistSVec<std::complex<double>, dim>>&);
  void freqIntegrateBlock(VecSet<DistSVec<double, dim>>&, double, int&, VecSet<DistSVec<std::complex<double>, dim>>&,
                          VecSet<DistSVec<double, dim>>&, VecSet<DistSVec<double, dim>>&, VecSet<Vec<double>>&,
                          KspSolver<DistSVec<std::complex<double>, dim>, CtrlFocusedTrainingMatVecProd<dim>,
                                    KspPrec<dim, std::complex<double>>, Communicator, std::complex<double>> *);
  void freqIntegrateBlockMultipleRhs(VecSet<DistSVec<double, dim>>&, double, int&, VecSet<DistSVec<std::complex<double>, dim>>&,
                                     VecSet<DistSVec<double, dim>>&, VecSet<DistSVec<double, dim>>&, VecSet<Vec<double>>&,
                                     GcrSolver<DistSVec<std::complex<double>, dim>, CtrlFocusedTrainingMatVecProd<dim>,
                                               KspPrec<dim, std::complex<double>>, Communicator, std::complex<double>> *);
  void constructROM(DenseMat<double>&, DenseMat<double>&, DenseMat<double>&, DenseMat<double>&, DenseMat<double>&, VecSet<DistSVec<double, dim>>&, int);
  void outputROM(DenseMat<double>&, DenseMat<double>&, DenseMat<double>&, DenseMat<double>&, DenseMat<double>&, VecSet<Vec<double>>&);
  void formOutputRom(VecSet<Vec<double>>&, VecSet<DistSVec<double, dim>>&, int);
  void timeIntegrateROM(DenseMat<double>&, DenseMat<double>&, DenseMat<double>&, DenseMat<double>&, DenseMat<double>&, VecSet<DistSVec<double, dim>>&,
                        int, int, double *, double *, double);

  void updateModalValues(double, double *, double *, double *, double *, Vec<double>&, int);
  void computeModalDisp(double, DistSVec<double, 3>&, DistSVec<double, dim>&, double *, double *, double *, double *, double *, Vec<double>&, Vec<double>&, int, double, double *, double *);
  void computeModalDisp(double, Vec<double>&, double *, double *, double *, double *, double *, Vec<double>&, Vec<double>&, VecSet<Vec<double>>&, int, int, double, double *, double *);

  void computePOD(VecSet<DistSVec<double, dim>>&, int&, bool = true);
  void interpolatePOD();

  void readPodVecs(VecSet<DistSVec<double, dim>>&, int&);
  void getPodVecsSize(const char *, int&);
  void readPodVecsInner(const char *, VecSet<DistSVec<double, dim>>&, int&, int&);
  void checkROBType(VecSet<DistSVec<double, dim>>&, int);
  void outputPODVectors(VecSet<DistSVec<double, dim>>&, Vec<double>&, int, bool = true, DenseMat<double> * = nullptr, Vec<int> * = nullptr);
  void ROBInnerProductSchedule(int **, int, int, int);
  int ROBInnerProductSteps(int, int);
  void ROBInnerProducts();
  void modifiedGramSchmidt(VecSet<DistSVec<double, dim>>&, double *, int);
  void computeDampingRatios();
  void evalMatForEvProblem(double, double, VecSet<Vec<std::complex<double>>>&, VecSet<Vec<double>>&, VecSet<Vec<double>>&);
  void computeGAM(double, double, VecSet<Vec<std::complex<double>>>&);
  void multiplyGAM(double, double, std::complex<double> *, Vec<std::complex<double>>&);
  void computeGenAeroForceMat();

  void readAltitudeSamples();
  void calculateSampleFluxes();

  void writeReducedAltitudeSamples(int, VecSet<Vec<double>>&);
  void projectSolutionVector(VecSet<DistSVec<double, dim>>&, int, DistSVec<double, dim>&, double *);
  void writeReducedIC(int, Vec<double>&, double *, double *);
  template<class Scalar>
  void printBlockMatrix(std::vector<std::vector<DenseMat<Scalar>>>&, std::ostream& = std::cerr, int = 6,
                        const char * = "");
  void checkStability(DenseMat<double>&);
  void outputHDM();

  int readSnapshots(VecSet<DistSVec<double, dim>>&, int&);

  void writeForceJacobian(std::string&);

  void errorAnalysisPreProcessing();
  void initializeErrorSourceMatrix(ErrorDynamicsMatVecProd<dim>&, VecSet<ErrorDynamicsBlockVec<dim>>&);
  std::complex<double> computeErrorDynamicsSpectralRadius(ErrorDynamicsInfo&, ErrorDynamicsMatVecProd<dim>&,
                                                          KspSolver<ErrorDynamicsBlockVec<dim>, ErrorDynamicsMatVecProd<dim>, ErrorDynamicsPrec<dim>, Communicator> *);
  void loadMatrixData();
  void setupMatrices();
  void computeSparseForceOperator();
  void computeModalForce(DistSVec<double, dim>&, DistSVec<double, 3>&, Vec<double>&);
  void computeRefModalForce(DistSVec<double, dim>&, Vec<double>&);
  void readInitialDispAndVel(double *, double *);
  void readTrim();
  void writeRefModalForce(const char *, const char *, Vec<double>&, int);
};

#endif

#ifndef _REC_FCN_DESC_H_
#define _REC_FCN_DESC_H_

#include <RecFcn.h>

#include <cstdlib>

#ifdef USE_EIGEN3
#define USE_EIGEN3_RecFcn
#endif

#ifdef USE_EIGEN3_RecFcn
#include <Eigen/Core>
#endif

//------------------------------------------------------------------------------

template<int dim>
class RecFcnConstant : public RecFcn {

 public:

  RecFcnConstant() : RecFcn(0.0, dim) {
    static_assert(dim != Eigen::Dynamic, "*** Error: in RecFcnConstant constructor Dim == Eigen::Dynamic");
  }
  RecFcnConstant(int _dim) : RecFcn(0.0, _dim) {}

  bool isConstant() {
    return true;
  }

  void compute(double *, double *, double *, double *, double *, double *, double *);

  void computeJacobian(double *, double *, double *, double *,
                       double *, double *, double *, double *, double *);

  void compute(double *, double *, double *, double *, double *, double *,
               double *, double *, double *, double *);

  void precompute(double *, double *, double *, double *,
                  double *, double *, double *, double *, double *) {}

  void compute(std::complex<double> *, std::complex<double> *, std::complex<double> *,
               std::complex<double> *, double *, double *, double *, double *,
               std::complex<double> *, std::complex<double> *);

  void computeDerivative(double *, double *, double *, double *, double *, double *,
                         double *, double *, double *, double *, double *);

  void computeDerivativeOperators(double *, double *, double *, double *, double *,
                                  double *, double *, double *, double *, double *, double *);

};

//------------------------------------------------------------------------------

template<int dim>
class RecFcnLinear : public RecFcn {

 public:

  RecFcnLinear(double b) : RecFcn(b, dim) {
    static_assert(dim != Eigen::Dynamic, "*** Error: in RecFcnLinear constructor Dim == Eigen::Dynamic");
  }
  RecFcnLinear(double b, int _dim) : RecFcn(b, _dim) {}

  void compute(double *, double *, double *, double *, double *, double *, double *);

  void computeJacobian(double *, double *, double *, double *,
                       double *, double *, double *, double *, double *);

  void compute(double *, double *, double *, double *, double *, double *,
               double *, double *, double *, double *);

  void precompute(double *, double *, double *, double *,
                  double *, double *, double *, double *, double *) {}

  void compute(std::complex<double> *, std::complex<double> *, std::complex<double> *,
               std::complex<double> *, double *, double *, double *, double *,
               std::complex<double> *, std::complex<double> *);

  void computeDerivative(double *, double *, double *, double *, double *, double *,
                         double *, double *, double *, double *, double *);

  void computeDerivativeOperators(double *, double *, double *, double *, double *,
                                  double *, double *, double *, double *, double *, double *);

};

//------------------------------------------------------------------------------

template<int dim>
class RecFcnVanAlbada : public RecFcn {

 public:
  RecFcnVanAlbada(double b, double k) : RecFcn(b, dim, k) {
    static_assert(dim != Eigen::Dynamic, "*** Error: in RecFcnVanAlbada constructor Dim == Eigen::Dynamic");
  }
  RecFcnVanAlbada(double b, int _dim, double k) : RecFcn(b, _dim, k) {}

  void compute(double *, double *, double *, double *, double *, double *, double *);

  void computeJacobian(double *, double *, double *, double *,
                       double *, double *, double *, double *, double *);

  void compute(double *, double *, double *, double *, double *, double *,
               double *, double *, double *, double *);

  void precompute(double *, double *, double *, double *,
                  double *, double *, double *, double *, double *);

  void compute(std::complex<double> *, std::complex<double> *, std::complex<double> *,
               std::complex<double> *, double *, double *, double *, double *,
               std::complex<double> *, std::complex<double> *);

  void computeDerivative(double *, double *, double *, double *, double *, double *,
                         double *, double *, double *, double *, double *);

  void computeDerivativeOperators(double *, double *, double *, double *, double *,
                                  double *, double *, double *, double *, double *, double *);

};

//------------------------------------------------------------------------------

template<int dim>
class RecFcnLinearConstant : public RecFcn {

 public:
  RecFcnLinearConstant(double b) : RecFcn(b, dim) {
    static_assert(dim != Eigen::Dynamic, "*** Error: in RecFcnLinearConstant constructor Dim == Eigen::Dynamic");
  }
  RecFcnLinearConstant(double b, int _dim) : RecFcn(b, _dim) {}

  void compute(double *, double *, double *, double *, double *, double *, double *);

  void computeJacobian(double *, double *, double *, double *,
                       double *, double *, double *, double *, double *);

  void compute(double *, double *, double *, double *, double *, double *,
               double *, double *, double *, double *);

  void precompute(double *, double *, double *, double *,
                  double *, double *, double *, double *, double *) {}

  void computeDerivative(double *, double *, double *, double *, double *, double *,
                         double *, double *, double *, double *, double *);

  void computeDerivativeOperators(double *, double *, double *, double *, double *,
                                  double *, double *, double *, double *, double *, double *);

};

//------------------------------------------------------------------------------

template<int dim>
class RecFcnVanAlbadaConstant : public RecFcn {

 public:
  RecFcnVanAlbadaConstant(double b, double k) : RecFcn(b, dim, k) {
    static_assert(dim != Eigen::Dynamic, "*** Error: in RecFcnVanAlbadaConstant constructor Dim == Eigen::Dynamic");
  }

  void compute(double *, double *, double *, double *, double *, double *, double *);

  void computeJacobian(double *, double *, double *, double *,
                       double *, double *, double *, double *, double *);

  void compute(double *, double *, double *, double *, double *, double *,
               double *, double *, double *, double *);

  void precompute(double *, double *, double *, double *,
                  double *, double *, double *, double *, double *);

  void computeDerivative(double *, double *, double *, double *, double *, double *,
                         double *, double *, double *, double *, double *);

  void computeDerivativeOperators(double *, double *, double *, double *, double *,
                                  double *, double *, double *, double *, double *, double *);
};

//------------------------------------------------------------------------------

template<int dim>
class RecFcnLinearVanAlbada : public RecFcn {

 public:
  RecFcnLinearVanAlbada(double b, double k) : RecFcn(b, dim, k) {
    static_assert(dim != Eigen::Dynamic, "*** Error: in RecFcnLinearVanAlbada constructor Dim == Eigen::Dynamic");
  }

  void compute(double *, double *, double *, double *, double *, double *, double *);

  void computeJacobian(double *, double *, double *, double *,
                       double *, double *, double *, double *, double *);

  void compute(double *, double *, double *, double *, double *, double *,
               double *, double *, double *, double *);

  void precompute(double *, double *, double *, double *,
                  double *, double *, double *, double *, double *);

  void computeDerivative(double *, double *, double *, double *, double *, double *,
                         double *, double *, double *, double *, double *);

  void computeDerivativeOperators(double *, double *, double *, double *, double *,
                                  double *, double *, double *, double *, double *, double *);
};

//------------------------------------------------------------------------------

template<int dim>
inline
void RecFcnConstant<dim>::compute(double *Vi, double *ddVij, double *Vj, double *ddVji,
                                  double *Vij, double *Vji, double *dx) {
#ifdef USE_EIGEN3_RecFcn
  const Eigen::Map<const Eigen::Matrix<double,dim,1>> vi(Vi, RecFcn::_dim, 1), vj(Vj, RecFcn::_dim, 1);
  Eigen::Map<Eigen::Matrix<double,dim,1>> vij(Vij, RecFcn::_dim, 1), vji(Vji, RecFcn::_dim, 1);
  vij = vi;
  vji = vj;
#else
  for(int k = 0; k < RecFcn::_dim; ++k) {
    constant(Vi[k], Vj[k], Vij[k], Vji[k]);
  }
#endif
}

//------------------------------------------------------------------------------
#ifndef USE_EIGEN3_RecFcn
template<>
inline
void RecFcnConstant<5>::compute(double *Vi, double *ddVij, double *Vj, double *ddVji,
                                double *Vij, double *Vji, double *dx) {
  constant(Vi[0], Vj[0], Vij[0], Vji[0]);
  constant(Vi[1], Vj[1], Vij[1], Vji[1]);
  constant(Vi[2], Vj[2], Vij[2], Vji[2]);
  constant(Vi[3], Vj[3], Vij[3], Vji[3]);
  constant(Vi[4], Vj[4], Vij[4], Vji[4]);
}

//------------------------------------------------------------------------------

template<>
inline
void RecFcnConstant<6>::compute(double *Vi, double *ddVij, double *Vj, double *ddVji,
                                double *Vij, double *Vji, double *dx) {
  constant(Vi[0], Vj[0], Vij[0], Vji[0]);
  constant(Vi[1], Vj[1], Vij[1], Vji[1]);
  constant(Vi[2], Vj[2], Vij[2], Vji[2]);
  constant(Vi[3], Vj[3], Vij[3], Vji[3]);
  constant(Vi[4], Vj[4], Vij[4], Vji[4]);
  constant(Vi[5], Vj[5], Vij[5], Vji[5]);
}

//------------------------------------------------------------------------------

template<>
inline
void RecFcnConstant<7>::compute(double *Vi, double *ddVij, double *Vj, double *ddVji,
                                double *Vij, double *Vji, double *dx) {
  constant(Vi[0], Vj[0], Vij[0], Vji[0]);
  constant(Vi[1], Vj[1], Vij[1], Vji[1]);
  constant(Vi[2], Vj[2], Vij[2], Vji[2]);
  constant(Vi[3], Vj[3], Vij[3], Vji[3]);
  constant(Vi[4], Vj[4], Vij[4], Vji[4]);
  constant(Vi[5], Vj[5], Vij[5], Vji[5]);
  constant(Vi[6], Vj[6], Vij[6], Vji[6]);
}
#endif
//------------------------------------------------------------------------------

template<int dim>
inline
void RecFcnConstant<dim>::computeJacobian(double *Vi, double *ddVij, double *Vj, double *ddVji,
                                          double *dVijdVi, double *dVijdVj, double *dVjidVi, double *dVjidVj, double *dx) {
#ifdef USE_EIGEN3_RecFcn
  Eigen::Map<Eigen::Matrix<double,dim,1>> dviji(dVijdVi, RecFcn::_dim, 1), dvijj(dVijdVj, RecFcn::_dim, 1),
                                          dvjii(dVjidVi, RecFcn::_dim, 1), dvjij(dVjidVj, RecFcn::_dim, 1);
  dviji.setZero();
  dvijj.setZero();
  dvjii.setZero();
  dvjij.setZero();
#else
  memset(dVijdVi, 0, sizeof(double) * RecFcn::_dim);
  memset(dVijdVj, 0, sizeof(double) * RecFcn::_dim);
  memset(dVjidVi, 0, sizeof(double) * RecFcn::_dim);
  memset(dVjidVj, 0, sizeof(double) * RecFcn::_dim);
#endif
}

//------------------------------------------------------------------------------

template<int dim>
inline
void RecFcnConstant<dim>::compute(double *pi, double *ddpij, double *pj, double *ddpji,
                                  double *aij, double *aji, double *bij, double *bji,
                                  double *pij, double *pji) {
  compute(pi, ddpij, pj, ddpji, pij, pji, nullptr);
}

//------------------------------------------------------------------------------

template<int dim>
inline
void RecFcnConstant<dim>::compute(std::complex<double> *pi, std::complex<double> *ddpij, std::complex<double> *pj,
                                  std::complex<double> *ddpji, double *aij, double *aji, double *bij, double *bji,
                                  std::complex<double> *pij, std::complex<double> *pji) {
#ifdef USE_EIGEN3_RecFcn
  const Eigen::Map<const Eigen::Matrix<std::complex<double>,dim,1>> vi(pi, RecFcn::_dim, 1), vj(pj, RecFcn::_dim, 1);
  Eigen::Map<Eigen::Matrix<std::complex<double>,dim,1>> vij(pij, RecFcn::_dim, 1), vji(pji, RecFcn::_dim, 1);
  vij = vi;
  vji = vj;
#else
  for(int k = 0; k < RecFcn::_dim; ++k) {
    constant(pi[k], pj[k], pij[k], pji[k]);
  }
#endif
}

//------------------------------------------------------------------------------

template<int dim>
inline
void RecFcnConstant<dim>::computeDerivative(double *Vi, double *dVi, double *ddVij, double *dddVij,
                                            double *Vj, double *dVj, double *ddVji, double *dddVji,
                                            double *dVij, double *dVji, double *dx) {
#ifdef USE_EIGEN3_RecFcn
  const Eigen::Map<const Eigen::Matrix<double,dim,1>> dvi(dVi, RecFcn::_dim, 1), dvj(dVj, RecFcn::_dim, 1);
  Eigen::Map<Eigen::Matrix<double,dim,1>> dvij(dVij, RecFcn::_dim, 1), dvji(dVji, RecFcn::_dim, 1);
  dvij = dvi;
  dvji = dvj;
#else
  for(int k = 0; k < RecFcn::_dim; ++k) {
    constantDerivative(dVi[k], dVj[k], dVij[k], dVji[k]);
  }
#endif
}

//------------------------------------------------------------------------------

template<int dim>
inline
void RecFcnConstant<dim>::computeDerivativeOperators(double *Vi, double *ddVij, double *Vj, double *ddVji,
                                                     double *dVijdVi, double *dVijdVj, double *dVijdddVij,
                                                     double *dVjidVi, double *dVjidVj, double *dVjidddVji,
                                                     double *dx) {
  for(int k = 0; k < RecFcn::_dim; ++k) {
    constantDerivativeOperators(dVijdVi[k], dVjidVj[k]);
  }
}

//------------------------------------------------------------------------------

template<int dim>
inline
void RecFcnLinear<dim>::compute(double *Vi, double *ddVij, double *Vj, double *ddVji,
                                double *Vij, double *Vji, double *dx) {
#ifdef USE_EIGEN3_RecFcn
  const Eigen::Map<const Eigen::Matrix<double,dim,1>> vi(Vi, RecFcn::_dim, 1), ddvij(ddVij, RecFcn::_dim, 1),
                                                      vj(Vj, RecFcn::_dim, 1), ddvji(ddVji, RecFcn::_dim, 1);
  Eigen::Map<Eigen::Matrix<double,dim,1>> vij(Vij, RecFcn::_dim, 1), vji(Vji, RecFcn::_dim, 1);
  const Eigen::Matrix<double,dim,1> dV = beta1 * (vj - vi);
  vij = vi + dV + beta * ddvij;
  vji = vj - dV - beta * ddvji;
#else
  for(int k = 0; k < RecFcn::_dim; ++k) {
    linear(Vi[k], ddVij[k], Vj[k], ddVji[k], Vij[k], Vji[k]);
  }
#endif
}

//------------------------------------------------------------------------------
#ifndef USE_EIGEN3_RecFcn
template<>
inline
void RecFcnLinear<5>::compute(double *Vi, double *ddVij, double *Vj, double *ddVji,
                              double *Vij, double *Vji, double *dx) {
  linear(Vi[0], ddVij[0], Vj[0], ddVji[0], Vij[0], Vji[0]);
  linear(Vi[1], ddVij[1], Vj[1], ddVji[1], Vij[1], Vji[1]);
  linear(Vi[2], ddVij[2], Vj[2], ddVji[2], Vij[2], Vji[2]);
  linear(Vi[3], ddVij[3], Vj[3], ddVji[3], Vij[3], Vji[3]);
  linear(Vi[4], ddVij[4], Vj[4], ddVji[4], Vij[4], Vji[4]);
}

//------------------------------------------------------------------------------

template<>
inline
void RecFcnLinear<6>::compute(double *Vi, double *ddVij, double *Vj, double *ddVji,
                              double *Vij, double *Vji, double *dx) {
  linear(Vi[0], ddVij[0], Vj[0], ddVji[0], Vij[0], Vji[0]);
  linear(Vi[1], ddVij[1], Vj[1], ddVji[1], Vij[1], Vji[1]);
  linear(Vi[2], ddVij[2], Vj[2], ddVji[2], Vij[2], Vji[2]);
  linear(Vi[3], ddVij[3], Vj[3], ddVji[3], Vij[3], Vji[3]);
  linear(Vi[4], ddVij[4], Vj[4], ddVji[4], Vij[4], Vji[4]);
  linear(Vi[5], ddVij[5], Vj[5], ddVji[5], Vij[5], Vji[5]);
}

//------------------------------------------------------------------------------

template<>
inline
void RecFcnLinear<7>::compute(double *Vi, double *ddVij, double *Vj, double *ddVji,
                              double *Vij, double *Vji, double *dx) {
  linear(Vi[0], ddVij[0], Vj[0], ddVji[0], Vij[0], Vji[0]);
  linear(Vi[1], ddVij[1], Vj[1], ddVji[1], Vij[1], Vji[1]);
  linear(Vi[2], ddVij[2], Vj[2], ddVji[2], Vij[2], Vji[2]);
  linear(Vi[3], ddVij[3], Vj[3], ddVji[3], Vij[3], Vji[3]);
  linear(Vi[4], ddVij[4], Vj[4], ddVji[4], Vij[4], Vji[4]);
  linear(Vi[5], ddVij[5], Vj[5], ddVji[5], Vij[5], Vji[5]);
  linear(Vi[6], ddVij[6], Vj[6], ddVji[6], Vij[6], Vji[6]);
}
#endif
//------------------------------------------------------------------------------

template<int dim>
inline
void RecFcnLinear<dim>::computeJacobian(double *Vi, double *ddVij, double *Vj, double *ddVji,
                                        double *dVijdVi, double *dVijdVj, double *dVjidVi, double *dVjidVj, double *dx) {
#ifdef USE_EIGEN3_RecFcn
  Eigen::Map<Eigen::Matrix<double,dim,1>> dviji(dVijdVi, RecFcn::_dim, 1), dvijj(dVijdVj, RecFcn::_dim, 1),
                                          dvjii(dVjidVi, RecFcn::_dim, 1), dvjij(dVjidVj, RecFcn::_dim, 1);
  dviji.setConstant(beta);
  dvijj.setConstant(beta1);
  dvjii.setConstant(beta1);
  dvjij.setConstant(-beta);
#else
  for(int k = 0; k < RecFcn::_dim; ++k) {
    linearjac(dVijdVi[k], dVijdVj[k], dVjidVi[k], dVjidVj[k]);
  }
#endif
}

//------------------------------------------------------------------------------
#ifndef USE_EIGEN3_RecFcn
template<>
inline
void RecFcnLinear<5>::computeJacobian(double *Vi, double *ddVij, double *Vj, double *ddVji,
                                      double *dVijdVi, double *dVijdVj, double *dVjidVi, double *dVjidVj, double *dx) {
  linearjac(dVijdVi[0], dVijdVj[0], dVjidVi[0], dVjidVj[0]);
  linearjac(dVijdVi[1], dVijdVj[1], dVjidVi[1], dVjidVj[1]);
  linearjac(dVijdVi[2], dVijdVj[2], dVjidVi[2], dVjidVj[2]);
  linearjac(dVijdVi[3], dVijdVj[3], dVjidVi[3], dVjidVj[3]);
  linearjac(dVijdVi[4], dVijdVj[4], dVjidVi[4], dVjidVj[4]);
}

//------------------------------------------------------------------------------

template<>
inline
void RecFcnLinear<6>::computeJacobian(double *Vi, double *ddVij, double *Vj, double *ddVji,
                                      double *dVijdVi, double *dVijdVj, double *dVjidVi, double *dVjidVj, double *dx) {
  linearjac(dVijdVi[0], dVijdVj[0], dVjidVi[0], dVjidVj[0]);
  linearjac(dVijdVi[1], dVijdVj[1], dVjidVi[1], dVjidVj[1]);
  linearjac(dVijdVi[2], dVijdVj[2], dVjidVi[2], dVjidVj[2]);
  linearjac(dVijdVi[3], dVijdVj[3], dVjidVi[3], dVjidVj[3]);
  linearjac(dVijdVi[4], dVijdVj[4], dVjidVi[4], dVjidVj[4]);
  linearjac(dVijdVi[5], dVijdVj[5], dVjidVi[5], dVjidVj[5]);
}

//------------------------------------------------------------------------------

template<>
inline
void RecFcnLinear<7>::computeJacobian(double *Vi, double *ddVij, double *Vj, double *ddVji,
                                      double *dVijdVi, double *dVijdVj, double *dVjidVi, double *dVjidVj, double *dx) {
  linearjac(dVijdVi[0], dVijdVj[0], dVjidVi[0], dVjidVj[0]);
  linearjac(dVijdVi[1], dVijdVj[1], dVjidVi[1], dVjidVj[1]);
  linearjac(dVijdVi[2], dVijdVj[2], dVjidVi[2], dVjidVj[2]);
  linearjac(dVijdVi[3], dVijdVj[3], dVjidVi[3], dVjidVj[3]);
  linearjac(dVijdVi[4], dVijdVj[4], dVjidVi[4], dVjidVj[4]);
  linearjac(dVijdVi[5], dVijdVj[5], dVjidVi[5], dVjidVj[5]);
  linearjac(dVijdVi[6], dVijdVj[6], dVjidVi[6], dVjidVj[6]);
}
#endif
//------------------------------------------------------------------------------

template<int dim>
inline
void RecFcnLinear<dim>::compute(double *pi, double *ddpij, double *pj, double *ddpji,
                                double *aij, double *aji, double *bij, double *bji,
                                double *pij, double *pji) {
  compute(pi, ddpij, pj, ddpji, pij, pji, nullptr);
}

//------------------------------------------------------------------------------

template<int dim>
inline
void RecFcnLinear<dim>::compute(std::complex<double> *pi, std::complex<double> *ddpij, std::complex<double> *pj,
                                std::complex<double> *ddpji, double *aij, double *aji, double *bij, double *bji,
                                std::complex<double> *pij, std::complex<double> *pji) {
#ifdef USE_EIGEN3_RecFcn
  const Eigen::Map<const Eigen::Matrix<std::complex<double>,dim,1>> vi(pi, RecFcn::_dim, 1), ddvij(ddpij, RecFcn::_dim, 1),
                                                                    vj(pj, RecFcn::_dim, 1), ddvji(ddpji, RecFcn::_dim, 1);
  Eigen::Map<Eigen::Matrix<std::complex<double>,dim,1>> vij(pij, RecFcn::_dim, 1), vji(pji, RecFcn::_dim, 1);
  const Eigen::Matrix<std::complex<double>,dim,1> dV = beta1 * (vj - vi);
  vij = vi + dV + beta * ddvij;
  vji = vj - dV - beta * ddvji;
#else
  for(int k = 0; k < RecFcn::_dim; ++k) {
    linear(pi[k], ddpij[k], pj[k], ddpji[k], pij[k], pji[k]);
  }
#endif
}

//------------------------------------------------------------------------------

template<int dim>
inline
void RecFcnLinear<dim>::computeDerivative(double *Vi, double *dVi, double *ddVij, double *dddVij,
                                          double *Vj, double *dVj, double *ddVji, double *dddVji,
                                          double *dVij, double *dVji, double *dx) {
#ifdef USE_EIGEN3_RecFcn
  const Eigen::Map<const Eigen::Matrix<double,dim,1>> dvi(dVi, RecFcn::_dim, 1), dddvij(dddVij, RecFcn::_dim, 1),
                                                      dvj(dVj, RecFcn::_dim, 1), dddvji(dddVji, RecFcn::_dim, 1);
  Eigen::Map<Eigen::Matrix<double,dim,1>> dvij(dVij, RecFcn::_dim, 1), dvji(dVji, RecFcn::_dim, 1);
  const Eigen::Matrix<double,dim,1> ddV = beta1 * (dvj - dvi);
  dvij = dvi + ddV + beta * dddvij;
  dvji = dvj - ddV - beta * dddvji;
#else
  for(int k = 0; k < RecFcn::_dim; ++k) {
    linearDerivative(dVi[k], dddVij[k], dVj[k], dddVji[k], dVij[k], dVji[k]);
  }
#endif
}

//------------------------------------------------------------------------------

template<int dim>
inline
void RecFcnLinear<dim>::computeDerivativeOperators(double *Vi, double *ddVij, double *Vj, double *ddVji,
                                                   double *dVijdVi, double *dVijdVj, double *dVijdddVij,
                                                   double *dVjidVi, double *dVjidVj, double *dVjidddVji,
                                                   double *dx) {
  for(int k = 0; k < RecFcn::_dim; ++k) {
    linearDerivativeOperators(dVijdVi[k], dVijdVj[k], dVijdddVij[k], dVjidVi[k], dVjidVj[k], dVjidddVji[k]);
  }
}

//------------------------------------------------------------------------------

template<int dim>
inline
void RecFcnVanAlbada<dim>::compute(double *Vi, double *ddVij, double *Vj, double *ddVji,
                                   double *Vij, double *Vji, double *dx) {
  for(int k = 0; k < RecFcn::_dim; ++k) {
    vanalbada(Vi[k], ddVij[k], Vj[k], ddVji[k], Vij[k], Vji[k], dx);
  }
}

//------------------------------------------------------------------------------

template<>
inline
void RecFcnVanAlbada<5>::compute(double *Vi, double *ddVij, double *Vj, double *ddVji,
                                 double *Vij, double *Vji, double *dx) {
  vanalbada(Vi[0], ddVij[0], Vj[0], ddVji[0], Vij[0], Vji[0], dx);
  vanalbada(Vi[1], ddVij[1], Vj[1], ddVji[1], Vij[1], Vji[1], dx);
  vanalbada(Vi[2], ddVij[2], Vj[2], ddVji[2], Vij[2], Vji[2], dx);
  vanalbada(Vi[3], ddVij[3], Vj[3], ddVji[3], Vij[3], Vji[3], dx);
  vanalbada(Vi[4], ddVij[4], Vj[4], ddVji[4], Vij[4], Vji[4], dx);
}

//------------------------------------------------------------------------------

template<>
inline
void RecFcnVanAlbada<6>::compute(double *Vi, double *ddVij, double *Vj, double *ddVji,
                                 double *Vij, double *Vji, double *dx) {
  vanalbada(Vi[0], ddVij[0], Vj[0], ddVji[0], Vij[0], Vji[0], dx);
  vanalbada(Vi[1], ddVij[1], Vj[1], ddVji[1], Vij[1], Vji[1], dx);
  vanalbada(Vi[2], ddVij[2], Vj[2], ddVji[2], Vij[2], Vji[2], dx);
  vanalbada(Vi[3], ddVij[3], Vj[3], ddVji[3], Vij[3], Vji[3], dx);
  vanalbada(Vi[4], ddVij[4], Vj[4], ddVji[4], Vij[4], Vji[4], dx);
  vanalbada(Vi[5], ddVij[5], Vj[5], ddVji[5], Vij[5], Vji[5], dx);
}

//------------------------------------------------------------------------------

template<>
inline
void RecFcnVanAlbada<7>::compute(double *Vi, double *ddVij, double *Vj, double *ddVji,
                                 double *Vij, double *Vji, double *dx) {
  vanalbada(Vi[0], ddVij[0], Vj[0], ddVji[0], Vij[0], Vji[0], dx);
  vanalbada(Vi[1], ddVij[1], Vj[1], ddVji[1], Vij[1], Vji[1], dx);
  vanalbada(Vi[2], ddVij[2], Vj[2], ddVji[2], Vij[2], Vji[2], dx);
  vanalbada(Vi[3], ddVij[3], Vj[3], ddVji[3], Vij[3], Vji[3], dx);
  vanalbada(Vi[4], ddVij[4], Vj[4], ddVji[4], Vij[4], Vji[4], dx);
  vanalbada(Vi[5], ddVij[5], Vj[5], ddVji[5], Vij[5], Vji[5], dx);
  vanalbada(Vi[6], ddVij[6], Vj[6], ddVji[6], Vij[6], Vji[6], dx);
}

//------------------------------------------------------------------------------

template<int dim>
inline
void RecFcnVanAlbada<dim>::computeJacobian(double *Vi, double *ddVij, double *Vj, double *ddVji,
                                           double *dVijdVi, double *dVijdVj, double *dVjidVi, double *dVjidVj, double *dx) {
  for(int k = 0; k < RecFcn::_dim; ++k) {
    vanalbadajac(Vi[k], ddVij[k], Vj[k], ddVji[k], dVijdVi[k], dVijdVj[k], dVjidVi[k], dVjidVj[k], dx);
  }
}

//------------------------------------------------------------------------------

template<>
inline
void RecFcnVanAlbada<5>::computeJacobian(double *Vi, double *ddVij, double *Vj, double *ddVji,
                                         double *dVijdVi, double *dVijdVj, double *dVjidVi, double *dVjidVj, double *dx) {
  vanalbadajac(Vi[0], ddVij[0], Vj[0], ddVji[0], dVijdVi[0], dVijdVj[0], dVjidVi[0], dVjidVj[0], dx);
  vanalbadajac(Vi[1], ddVij[1], Vj[1], ddVji[1], dVijdVi[1], dVijdVj[1], dVjidVi[1], dVjidVj[1], dx);
  vanalbadajac(Vi[2], ddVij[2], Vj[2], ddVji[2], dVijdVi[2], dVijdVj[2], dVjidVi[2], dVjidVj[2], dx);
  vanalbadajac(Vi[3], ddVij[3], Vj[3], ddVji[3], dVijdVi[3], dVijdVj[3], dVjidVi[3], dVjidVj[3], dx);
  vanalbadajac(Vi[4], ddVij[4], Vj[4], ddVji[4], dVijdVi[4], dVijdVj[4], dVjidVi[4], dVjidVj[4], dx);
}

//------------------------------------------------------------------------------

template<>
inline
void RecFcnVanAlbada<6>::computeJacobian(double *Vi, double *ddVij, double *Vj, double *ddVji,
                                         double *dVijdVi, double *dVijdVj, double *dVjidVi, double *dVjidVj, double *dx) {
  vanalbadajac(Vi[0], ddVij[0], Vj[0], ddVji[0], dVijdVi[0], dVijdVj[0], dVjidVi[0], dVjidVj[0], dx);
  vanalbadajac(Vi[1], ddVij[1], Vj[1], ddVji[1], dVijdVi[1], dVijdVj[1], dVjidVi[1], dVjidVj[1], dx);
  vanalbadajac(Vi[2], ddVij[2], Vj[2], ddVji[2], dVijdVi[2], dVijdVj[2], dVjidVi[2], dVjidVj[2], dx);
  vanalbadajac(Vi[3], ddVij[3], Vj[3], ddVji[3], dVijdVi[3], dVijdVj[3], dVjidVi[3], dVjidVj[3], dx);
  vanalbadajac(Vi[4], ddVij[4], Vj[4], ddVji[4], dVijdVi[4], dVijdVj[4], dVjidVi[4], dVjidVj[4], dx);
  vanalbadajac(Vi[5], ddVij[5], Vj[5], ddVji[5], dVijdVi[5], dVijdVj[5], dVjidVi[5], dVjidVj[5], dx);
}

//------------------------------------------------------------------------------

template<>
inline
void RecFcnVanAlbada<7>::computeJacobian(double *Vi, double *ddVij, double *Vj, double *ddVji,
                                         double *dVijdVi, double *dVijdVj, double *dVjidVi, double *dVjidVj, double *dx) {
  vanalbadajac(Vi[0], ddVij[0], Vj[0], ddVji[0], dVijdVi[0], dVijdVj[0], dVjidVi[0], dVjidVj[0], dx);
  vanalbadajac(Vi[1], ddVij[1], Vj[1], ddVji[1], dVijdVi[1], dVijdVj[1], dVjidVi[1], dVjidVj[1], dx);
  vanalbadajac(Vi[2], ddVij[2], Vj[2], ddVji[2], dVijdVi[2], dVijdVj[2], dVjidVi[2], dVjidVj[2], dx);
  vanalbadajac(Vi[3], ddVij[3], Vj[3], ddVji[3], dVijdVi[3], dVijdVj[3], dVjidVi[3], dVjidVj[3], dx);
  vanalbadajac(Vi[4], ddVij[4], Vj[4], ddVji[4], dVijdVi[4], dVijdVj[4], dVjidVi[4], dVjidVj[4], dx);
  vanalbadajac(Vi[5], ddVij[5], Vj[5], ddVji[5], dVijdVi[5], dVijdVj[5], dVjidVi[5], dVjidVj[5], dx);
  vanalbadajac(Vi[6], ddVij[6], Vj[6], ddVji[6], dVijdVi[6], dVijdVj[6], dVjidVi[6], dVjidVj[6], dx);
}

//------------------------------------------------------------------------------

template<int dim>
inline
void RecFcnVanAlbada<dim>::compute(double *pi, double *ddpij, double *pj, double *ddpji,
                                   double *aij, double *aji, double *bij, double *bji,
                                   double *pij, double *pji) {
#ifdef USE_EIGEN3_RecFcn
  const Eigen::Map<const Eigen::Matrix<double,dim,1>> Pi(pi, RecFcn::_dim, 1), ddPij(ddpij, RecFcn::_dim, 1),
                                                      Pj(pj, RecFcn::_dim, 1), ddPji(ddpji, RecFcn::_dim, 1),
                                                      Aij(aij, RecFcn::_dim, 1), Aji(aji, RecFcn::_dim, 1),
                                                      Bij(bij, RecFcn::_dim, 1), Bji(bji, RecFcn::_dim, 1);
  Eigen::Map<Eigen::Matrix<double,dim,1>> Pij(pij, RecFcn::_dim, 1), Pji(pji, RecFcn::_dim, 1);
  const Eigen::Matrix<double,dim,1> dP = Pj - Pi;
  Pij = Pi + Aij.cwiseProduct(dP) + Bij.cwiseProduct(ddPij);
  Pji = Pj - Aji.cwiseProduct(dP) - Bji.cwiseProduct(ddPji);
#else
  for(int k = 0; k < RecFcn::_dim; ++k) {
    vanalbada(pi[k], ddpij[k], pj[k], ddpji[k], aij[k], aji[k], bij[k], bji[k], pij[k], pji[k]);
  }
#endif
}

//------------------------------------------------------------------------------
#ifndef USE_EIGEN3_RecFcn
template<>
inline
void RecFcnVanAlbada<5>::compute(double *pi, double *ddpij, double *pj, double *ddpji,
                                 double *aij, double *aji, double *bij, double *bji,
                                 double *pij, double *pji) {
  vanalbada(pi[0], ddpij[0], pj[0], ddpji[0], aij[0], aji[0], bij[0], bji[0], pij[0], pji[0]);
  vanalbada(pi[1], ddpij[1], pj[1], ddpji[1], aij[1], aji[1], bij[1], bji[1], pij[1], pji[1]);
  vanalbada(pi[2], ddpij[2], pj[2], ddpji[2], aij[2], aji[2], bij[2], bji[2], pij[2], pji[2]);
  vanalbada(pi[3], ddpij[3], pj[3], ddpji[3], aij[3], aji[3], bij[3], bji[3], pij[3], pji[3]);
  vanalbada(pi[4], ddpij[4], pj[4], ddpji[4], aij[4], aji[4], bij[4], bji[4], pij[4], pji[4]);
}

//------------------------------------------------------------------------------

template<>
inline
void RecFcnVanAlbada<6>::compute(double *pi, double *ddpij, double *pj, double *ddpji,
                                 double *aij, double *aji, double *bij, double *bji,
                                 double *pij, double *pji) {
  vanalbada(pi[0], ddpij[0], pj[0], ddpji[0], aij[0], aji[0], bij[0], bji[0], pij[0], pji[0]);
  vanalbada(pi[1], ddpij[1], pj[1], ddpji[1], aij[1], aji[1], bij[1], bji[1], pij[1], pji[1]);
  vanalbada(pi[2], ddpij[2], pj[2], ddpji[2], aij[2], aji[2], bij[2], bji[2], pij[2], pji[2]);
  vanalbada(pi[3], ddpij[3], pj[3], ddpji[3], aij[3], aji[3], bij[3], bji[3], pij[3], pji[3]);
  vanalbada(pi[4], ddpij[4], pj[4], ddpji[4], aij[4], aji[4], bij[4], bji[4], pij[4], pji[4]);
  vanalbada(pi[5], ddpij[5], pj[5], ddpji[5], aij[5], aji[5], bij[5], bji[5], pij[5], pji[5]);
}

//------------------------------------------------------------------------------

template<>
inline
void RecFcnVanAlbada<7>::compute(double *pi, double *ddpij, double *pj, double *ddpji,
                                 double *aij, double *aji, double *bij, double *bji,
                                 double *pij, double *pji) {
  vanalbada(pi[0], ddpij[0], pj[0], ddpji[0], aij[0], aji[0], bij[0], bji[0], pij[0], pji[0]);
  vanalbada(pi[1], ddpij[1], pj[1], ddpji[1], aij[1], aji[1], bij[1], bji[1], pij[1], pji[1]);
  vanalbada(pi[2], ddpij[2], pj[2], ddpji[2], aij[2], aji[2], bij[2], bji[2], pij[2], pji[2]);
  vanalbada(pi[3], ddpij[3], pj[3], ddpji[3], aij[3], aji[3], bij[3], bji[3], pij[3], pji[3]);
  vanalbada(pi[4], ddpij[4], pj[4], ddpji[4], aij[4], aji[4], bij[4], bji[4], pij[4], pji[4]);
  vanalbada(pi[5], ddpij[5], pj[5], ddpji[5], aij[5], aji[5], bij[5], bji[5], pij[5], pji[5]);
  vanalbada(pi[6], ddpij[6], pj[6], ddpji[6], aij[6], aji[6], bij[6], bji[6], pij[6], pji[6]);
}
#endif
//------------------------------------------------------------------------------

template<int dim>
inline
void RecFcnVanAlbada<dim>::compute(std::complex<double> *pi, std::complex<double> *ddpij, std::complex<double> *pj,
                                   std::complex<double> *ddpji, double *aij, double *aji, double *bij, double *bji,
                                   std::complex<double> *pij, std::complex<double> *pji) {
#ifdef USE_EIGEN3_RecFcn
  const Eigen::Map<const Eigen::Matrix<std::complex<double>,dim,1>> Pi(pi, RecFcn::_dim, 1), ddPij(ddpij, RecFcn::_dim, 1),
                                                                    Pj(pj, RecFcn::_dim, 1), ddPji(ddpji, RecFcn::_dim, 1);
  const Eigen::Map<const Eigen::Matrix<double,dim,1>> Aij(aij, RecFcn::_dim, 1), Aji(aji, RecFcn::_dim, 1),
                                                      Bij(bij, RecFcn::_dim, 1), Bji(bji, RecFcn::_dim, 1);
  Eigen::Map<Eigen::Matrix<std::complex<double>,dim,1>> Pij(pij, RecFcn::_dim, 1), Pji(pji, RecFcn::_dim, 1);
  const Eigen::Matrix<std::complex<double>,dim,1> dP = Pj - Pi;
  Pij = Pi + Aij.cwiseProduct(dP) + Bij.cwiseProduct(ddPij);
  Pji = Pj - Aji.cwiseProduct(dP) - Bji.cwiseProduct(ddPji);
#else
  for(int k = 0; k < RecFcn::_dim; ++k) {
    vanalbada(pi[k], ddpij[k], pj[k], ddpji[k], aij[k], aji[k], bij[k], bji[k], pij[k], pji[k]);
  }
#endif
}

//------------------------------------------------------------------------------


template<int dim>
inline
void RecFcnVanAlbada<dim>::precompute(double *Vi, double *ddVij, double *Vj, double *ddVji,
                                      double *aij, double *aji, double *bij, double *bji, double *dx) {
  for(int k = 0; k < RecFcn::_dim; ++k) {
    prevanalbada(Vi[k], ddVij[k], Vj[k], ddVji[k], aij[k], aji[k], bij[k], bji[k], dx);
  }
}

//------------------------------------------------------------------------------

template<>
inline
void RecFcnVanAlbada<5>::precompute(double *Vi, double *ddVij, double *Vj, double *ddVji,
                                    double *aij, double *aji, double *bij, double *bji, double *dx) {
  prevanalbada(Vi[0], ddVij[0], Vj[0], ddVji[0], aij[0], aji[0], bij[0], bji[0], dx);
  prevanalbada(Vi[1], ddVij[1], Vj[1], ddVji[1], aij[1], aji[1], bij[1], bji[1], dx);
  prevanalbada(Vi[2], ddVij[2], Vj[2], ddVji[2], aij[2], aji[2], bij[2], bji[2], dx);
  prevanalbada(Vi[3], ddVij[3], Vj[3], ddVji[3], aij[3], aji[3], bij[3], bji[3], dx);
  prevanalbada(Vi[4], ddVij[4], Vj[4], ddVji[4], aij[4], aji[4], bij[4], bji[4], dx);
}

//------------------------------------------------------------------------------

template<>
inline
void RecFcnVanAlbada<6>::precompute(double *Vi, double *ddVij, double *Vj, double *ddVji,
                                    double *aij, double *aji, double *bij, double *bji, double *dx) {
  prevanalbada(Vi[0], ddVij[0], Vj[0], ddVji[0], aij[0], aji[0], bij[0], bji[0], dx);
  prevanalbada(Vi[1], ddVij[1], Vj[1], ddVji[1], aij[1], aji[1], bij[1], bji[1], dx);
  prevanalbada(Vi[2], ddVij[2], Vj[2], ddVji[2], aij[2], aji[2], bij[2], bji[2], dx);
  prevanalbada(Vi[3], ddVij[3], Vj[3], ddVji[3], aij[3], aji[3], bij[3], bji[3], dx);
  prevanalbada(Vi[4], ddVij[4], Vj[4], ddVji[4], aij[4], aji[4], bij[4], bji[4], dx);
  prevanalbada(Vi[5], ddVij[5], Vj[5], ddVji[5], aij[5], aji[5], bij[5], bji[5], dx);
}

//------------------------------------------------------------------------------

template<>
inline
void RecFcnVanAlbada<7>::precompute(double *Vi, double *ddVij, double *Vj, double *ddVji,
                                    double *aij, double *aji, double *bij, double *bji, double *dx) {
  prevanalbada(Vi[0], ddVij[0], Vj[0], ddVji[0], aij[0], aji[0], bij[0], bji[0], dx);
  prevanalbada(Vi[1], ddVij[1], Vj[1], ddVji[1], aij[1], aji[1], bij[1], bji[1], dx);
  prevanalbada(Vi[2], ddVij[2], Vj[2], ddVji[2], aij[2], aji[2], bij[2], bji[2], dx);
  prevanalbada(Vi[3], ddVij[3], Vj[3], ddVji[3], aij[3], aji[3], bij[3], bji[3], dx);
  prevanalbada(Vi[4], ddVij[4], Vj[4], ddVji[4], aij[4], aji[4], bij[4], bji[4], dx);
  prevanalbada(Vi[5], ddVij[5], Vj[5], ddVji[5], aij[5], aji[5], bij[5], bji[5], dx);
  prevanalbada(Vi[6], ddVij[6], Vj[6], ddVji[6], aij[6], aji[6], bij[6], bji[6], dx);
}

//------------------------------------------------------------------------------

template<int dim>
inline
void RecFcnVanAlbada<dim>::computeDerivative(double *Vi, double *dVi, double *ddVij, double *dddVij,
                                             double *Vj, double *dVj, double *ddVji, double *dddVji,
                                             double *dVij, double *dVji, double *dx) {
  for(int k = 0; k < RecFcn::_dim; ++k) {
    vanalbadaDerivative(Vi[k], dVi[k], ddVij[k], dddVij[k], Vj[k], dVj[k], ddVji[k], dddVji[k], dVij[k], dVji[k], dx);
  }
}

//------------------------------------------------------------------------------

template<int dim>
inline
void RecFcnVanAlbada<dim>::computeDerivativeOperators(double *Vi, double *ddVij, double *Vj, double *ddVji,
                                                      double *dVijdVi, double *dVijdVj, double *dVijdddVij,
                                                      double *dVjidVi, double *dVjidVj, double *dVjidddVji,
                                                      double *dx) {
  for(int k = 0; k < RecFcn::_dim; ++k) {
    vanalbadaDerivativeOperators(Vi[k], ddVij[k], Vj[k], ddVji[k], dVijdVi[k], dVijdVj[k], dVijdddVij[k],
                                 dVjidVi[k], dVjidVj[k], dVjidddVji[k], dx);
  }
}

//------------------------------------------------------------------------------

template<int dim>
inline
void RecFcnLinearConstant<dim>::compute(double *Vi, double *ddVij, double *Vj, double *ddVji,
                                        double *Vij, double *Vji, double *dx) {
  fprintf(stderr, "*** Error: RecFcnLinearConstant<%d>::compute is not overloaded\n", dim);
  exit(1);
}

//------------------------------------------------------------------------------

template<>
inline
void RecFcnLinearConstant<6>::compute(double *Vi, double *ddVij, double *Vj, double *ddVji,
                                      double *Vij, double *Vji, double *dx) {
  linear(Vi[0], ddVij[0], Vj[0], ddVji[0], Vij[0], Vji[0]);
  linear(Vi[1], ddVij[1], Vj[1], ddVji[1], Vij[1], Vji[1]);
  linear(Vi[2], ddVij[2], Vj[2], ddVji[2], Vij[2], Vji[2]);
  linear(Vi[3], ddVij[3], Vj[3], ddVji[3], Vij[3], Vji[3]);
  linear(Vi[4], ddVij[4], Vj[4], ddVji[4], Vij[4], Vji[4]);
  constant(Vi[5], Vj[5], Vij[5], Vji[5]);
}

//------------------------------------------------------------------------------

template<>
inline
void RecFcnLinearConstant<7>::compute(double *Vi, double *ddVij, double *Vj, double *ddVji,
                                      double *Vij, double *Vji, double *dx) {
  linear(Vi[0], ddVij[0], Vj[0], ddVji[0], Vij[0], Vji[0]);
  linear(Vi[1], ddVij[1], Vj[1], ddVji[1], Vij[1], Vji[1]);
  linear(Vi[2], ddVij[2], Vj[2], ddVji[2], Vij[2], Vji[2]);
  linear(Vi[3], ddVij[3], Vj[3], ddVji[3], Vij[3], Vji[3]);
  linear(Vi[4], ddVij[4], Vj[4], ddVji[4], Vij[4], Vji[4]);
  constant(Vi[5], Vj[5], Vij[5], Vji[5]);
  constant(Vi[6], Vj[6], Vij[6], Vji[6]);
}

//------------------------------------------------------------------------------

template<int dim>
inline
void RecFcnLinearConstant<dim>::computeJacobian(double *Vi, double *ddVij, double *Vj, double *ddVji,
                                                double *dVijdVi, double *dVijdVj, double *dVjidVi, double *dVjidVj, double *dx) {
  fprintf(stderr, "*** Error: RecFcnLinearConstant<%d>::computeJacobian is not overloaded\n", dim);
  exit(1);
}

//------------------------------------------------------------------------------

template<>
inline
void RecFcnLinearConstant<6>::computeJacobian(double *Vi, double *ddVij, double *Vj, double *ddVji,
                                              double *dVijdVi, double *dVijdVj, double *dVjidVi, double *dVjidVj, double *dx) {
  linearjac(dVijdVi[0], dVijdVj[0], dVjidVi[0], dVjidVj[0]);
  linearjac(dVijdVi[1], dVijdVj[1], dVjidVi[1], dVjidVj[1]);
  linearjac(dVijdVi[2], dVijdVj[2], dVjidVi[2], dVjidVj[2]);
  linearjac(dVijdVi[3], dVijdVj[3], dVjidVi[3], dVjidVj[3]);
  linearjac(dVijdVi[4], dVijdVj[4], dVjidVi[4], dVjidVj[4]);
  dVijdVi[5] = dVijdVj[5] = dVjidVi[5] = dVjidVj[5] = 0;
}

//------------------------------------------------------------------------------

template<>
inline
void RecFcnLinearConstant<7>::computeJacobian(double *Vi, double *ddVij, double *Vj, double *ddVji,
                                              double *dVijdVi, double *dVijdVj, double *dVjidVi, double *dVjidVj, double *dx) {
  linearjac(dVijdVi[0], dVijdVj[0], dVjidVi[0], dVjidVj[0]);
  linearjac(dVijdVi[1], dVijdVj[1], dVjidVi[1], dVjidVj[1]);
  linearjac(dVijdVi[2], dVijdVj[2], dVjidVi[2], dVjidVj[2]);
  linearjac(dVijdVi[3], dVijdVj[3], dVjidVi[3], dVjidVj[3]);
  linearjac(dVijdVi[4], dVijdVj[4], dVjidVi[4], dVjidVj[4]);
  dVijdVi[5] = dVijdVj[5] = dVjidVi[5] = dVjidVj[5] = 0;
  dVijdVi[6] = dVijdVj[6] = dVjidVi[6] = dVjidVj[6] = 0;
}

//------------------------------------------------------------------------------

template<int dim>
inline
void RecFcnLinearConstant<dim>::compute(double *pi, double *ddpij, double *pj, double *ddpji,
                                        double *aij, double *aji, double *bij, double *bji,
                                        double *pij, double *pji) {
  compute(pi, ddpij, pj, ddpji, pij, pji, nullptr);
}

//------------------------------------------------------------------------------

template<int dim>
inline
void RecFcnLinearConstant<dim>::computeDerivative(double *Vi, double *dVi, double *ddVij, double *dddVij,
                                                  double *Vj, double *dVj, double *ddVji, double *dddVji,
                                                  double *dVij, double *dVji, double *dx) {
  fprintf(stderr, "*** Error: RecFcnLinearConstant<%d>::computeDerivative is not overloaded\n", dim);
  exit(1);
}

//------------------------------------------------------------------------------

template<>
inline
void RecFcnLinearConstant<6>::computeDerivative(double *Vi, double *dVi, double *ddVij, double *dddVij,
                                                double *Vj, double *dVj, double *ddVji, double *dddVji,
                                                double *dVij, double *dVji, double *dx) {
  linearDerivative(dVi[0], dddVij[0], dVj[0], dddVji[0], dVij[0], dVji[0]);
  linearDerivative(dVi[1], dddVij[1], dVj[1], dddVji[1], dVij[1], dVji[1]);
  linearDerivative(dVi[2], dddVij[2], dVj[2], dddVji[2], dVij[2], dVji[2]);
  linearDerivative(dVi[3], dddVij[3], dVj[3], dddVji[3], dVij[3], dVji[3]);
  linearDerivative(dVi[4], dddVij[4], dVj[4], dddVji[4], dVij[4], dVji[4]);
  constantDerivative(dVi[5], dVj[5], dVij[5], dVji[5]);
}

//------------------------------------------------------------------------------

template<>
inline
void RecFcnLinearConstant<7>::computeDerivative(double *Vi, double *dVi, double *ddVij, double *dddVij,
                                                double *Vj, double *dVj, double *ddVji, double *dddVji,
                                                double *dVij, double *dVji, double *dx) {
  linearDerivative(dVi[0], dddVij[0], dVj[0], dddVji[0], dVij[0], dVji[0]);
  linearDerivative(dVi[1], dddVij[1], dVj[1], dddVji[1], dVij[1], dVji[1]);
  linearDerivative(dVi[2], dddVij[2], dVj[2], dddVji[2], dVij[2], dVji[2]);
  linearDerivative(dVi[3], dddVij[3], dVj[3], dddVji[3], dVij[3], dVji[3]);
  linearDerivative(dVi[4], dddVij[4], dVj[4], dddVji[4], dVij[4], dVji[4]);
  constantDerivative(dVi[5], dVj[5], dVij[5], dVji[5]);
  constantDerivative(dVi[6], dVj[6], dVij[6], dVji[6]);
}

//------------------------------------------------------------------------------

template<int dim>
inline
void RecFcnLinearConstant<dim>::computeDerivativeOperators(double *Vi, double *ddVij, double *Vj, double *ddVji,
                                                           double *dVijdVi, double *dVijdVj, double *dVijdddVij,
                                                           double *dVjidVi, double *dVjidVj, double *dVjidddVji,
                                                           double *dx) {
  fprintf(stderr, "*** Error: RecFcnLinearConstant<%d>::computeDerivativeOperators is not overloaded\n", dim);
  exit(1);
}

//------------------------------------------------------------------------------

template<>
inline
void RecFcnLinearConstant<6>::computeDerivativeOperators(double *Vi, double *ddVij, double *Vj, double *ddVji,
                                                         double *dVijdVi, double *dVijdVj, double *dVijdddVij,
                                                         double *dVjidVi, double *dVjidVj, double *dVjidddVji,
                                                         double *dx) {
  linearDerivativeOperators(dVijdVi[0], dVijdVj[0], dVijdddVij[0], dVjidVi[0], dVjidVj[0], dVjidddVji[0]);
  linearDerivativeOperators(dVijdVi[1], dVijdVj[1], dVijdddVij[1], dVjidVi[1], dVjidVj[1], dVjidddVji[1]);
  linearDerivativeOperators(dVijdVi[2], dVijdVj[2], dVijdddVij[2], dVjidVi[2], dVjidVj[2], dVjidddVji[2]);
  linearDerivativeOperators(dVijdVi[3], dVijdVj[3], dVijdddVij[3], dVjidVi[3], dVjidVj[3], dVjidddVji[3]);
  linearDerivativeOperators(dVijdVi[4], dVijdVj[4], dVijdddVij[4], dVjidVi[4], dVjidVj[4], dVjidddVji[4]);
  constantDerivativeOperators(dVijdVi[5], dVjidVj[5]);
}

//------------------------------------------------------------------------------

template<>
inline
void RecFcnLinearConstant<7>::computeDerivativeOperators(double *Vi, double *ddVij, double *Vj, double *ddVji,
                                                         double *dVijdVi, double *dVijdVj, double *dVijdddVij,
                                                         double *dVjidVi, double *dVjidVj, double *dVjidddVji,
                                                         double *dx) {
  linearDerivativeOperators(dVijdVi[0], dVijdVj[0], dVijdddVij[0], dVjidVi[0], dVjidVj[0], dVjidddVji[0]);
  linearDerivativeOperators(dVijdVi[1], dVijdVj[1], dVijdddVij[1], dVjidVi[1], dVjidVj[1], dVjidddVji[1]);
  linearDerivativeOperators(dVijdVi[2], dVijdVj[2], dVijdddVij[2], dVjidVi[2], dVjidVj[2], dVjidddVji[2]);
  linearDerivativeOperators(dVijdVi[3], dVijdVj[3], dVijdddVij[3], dVjidVi[3], dVjidVj[3], dVjidddVji[3]);
  linearDerivativeOperators(dVijdVi[4], dVijdVj[4], dVijdddVij[4], dVjidVi[4], dVjidVj[4], dVjidddVji[4]);
  constantDerivativeOperators(dVijdVi[5], dVjidVj[5]);
  constantDerivativeOperators(dVijdVi[6], dVjidVj[6]);
}

//------------------------------------------------------------------------------

template<int dim>
inline
void RecFcnVanAlbadaConstant<dim>::compute(double *Vi, double *ddVij, double *Vj, double *ddVji,
                                           double *Vij, double *Vji, double *dx) {
  fprintf(stderr, "*** Error: RecFcnVanAlbadaConstant<%d>::compute is not overloaded\n", dim);
  exit(1);
}

//------------------------------------------------------------------------------

template<>
inline
void RecFcnVanAlbadaConstant<6>::compute(double *Vi, double *ddVij, double *Vj, double *ddVji,
                                         double *Vij, double *Vji, double *dx) {
  vanalbada(Vi[0], ddVij[0], Vj[0], ddVji[0], Vij[0], Vji[0], dx);
  vanalbada(Vi[1], ddVij[1], Vj[1], ddVji[1], Vij[1], Vji[1], dx);
  vanalbada(Vi[2], ddVij[2], Vj[2], ddVji[2], Vij[2], Vji[2], dx);
  vanalbada(Vi[3], ddVij[3], Vj[3], ddVji[3], Vij[3], Vji[3], dx);
  vanalbada(Vi[4], ddVij[4], Vj[4], ddVji[4], Vij[4], Vji[4], dx);
  constant(Vi[5], Vj[5], Vij[5], Vji[5]);
}

//------------------------------------------------------------------------------

template<>
inline
void RecFcnVanAlbadaConstant<7>::compute(double *Vi, double *ddVij, double *Vj, double *ddVji,
                                         double *Vij, double *Vji, double *dx) {
  vanalbada(Vi[0], ddVij[0], Vj[0], ddVji[0], Vij[0], Vji[0], dx);
  vanalbada(Vi[1], ddVij[1], Vj[1], ddVji[1], Vij[1], Vji[1], dx);
  vanalbada(Vi[2], ddVij[2], Vj[2], ddVji[2], Vij[2], Vji[2], dx);
  vanalbada(Vi[3], ddVij[3], Vj[3], ddVji[3], Vij[3], Vji[3], dx);
  vanalbada(Vi[4], ddVij[4], Vj[4], ddVji[4], Vij[4], Vji[4], dx);
  constant(Vi[5], Vj[5], Vij[5], Vji[5]);
  constant(Vi[6], Vj[6], Vij[6], Vji[6]);
}

//------------------------------------------------------------------------------

template<int dim>
inline
void RecFcnVanAlbadaConstant<dim>::computeJacobian(double *Vi, double *ddVij, double *Vj, double *ddVji,
                                                   double *dVijdVi, double *dVijdVj, double *dVjidVi, double *dVjidVj, double *dx) {
  fprintf(stderr, "*** Error: RecFcnVanAlbadaConstant<%d>::computeJacobian is not overloaded\n", dim);
  exit(1);
}

//------------------------------------------------------------------------------

template<>
inline
void RecFcnVanAlbadaConstant<6>::computeJacobian(double *Vi, double *ddVij, double *Vj, double *ddVji,
                                                 double *dVijdVi, double *dVijdVj, double *dVjidVi, double *dVjidVj, double *dx) {
  vanalbadajac(Vi[0], ddVij[0], Vj[0], ddVji[0], dVijdVi[0], dVijdVj[0], dVjidVi[0], dVjidVj[0], dx);
  vanalbadajac(Vi[1], ddVij[1], Vj[1], ddVji[1], dVijdVi[1], dVijdVj[1], dVjidVi[1], dVjidVj[1], dx);
  vanalbadajac(Vi[2], ddVij[2], Vj[2], ddVji[2], dVijdVi[2], dVijdVj[2], dVjidVi[2], dVjidVj[2], dx);
  vanalbadajac(Vi[3], ddVij[3], Vj[3], ddVji[3], dVijdVi[3], dVijdVj[3], dVjidVi[3], dVjidVj[3], dx);
  vanalbadajac(Vi[4], ddVij[4], Vj[4], ddVji[4], dVijdVi[4], dVijdVj[4], dVjidVi[4], dVjidVj[4], dx);
  dVijdVi[5] = dVijdVj[5] = dVjidVi[5] = dVjidVj[5] = 0;
}

//------------------------------------------------------------------------------

template<>
inline
void RecFcnVanAlbadaConstant<7>::computeJacobian(double *Vi, double *ddVij, double *Vj, double *ddVji,
                                                 double *dVijdVi, double *dVijdVj, double *dVjidVi, double *dVjidVj, double *dx) {
  vanalbadajac(Vi[0], ddVij[0], Vj[0], ddVji[0], dVijdVi[0], dVijdVj[0], dVjidVi[0], dVjidVj[0], dx);
  vanalbadajac(Vi[1], ddVij[1], Vj[1], ddVji[1], dVijdVi[1], dVijdVj[1], dVjidVi[1], dVjidVj[1], dx);
  vanalbadajac(Vi[2], ddVij[2], Vj[2], ddVji[2], dVijdVi[2], dVijdVj[2], dVjidVi[2], dVjidVj[2], dx);
  vanalbadajac(Vi[3], ddVij[3], Vj[3], ddVji[3], dVijdVi[3], dVijdVj[3], dVjidVi[3], dVjidVj[3], dx);
  vanalbadajac(Vi[4], ddVij[4], Vj[4], ddVji[4], dVijdVi[4], dVijdVj[4], dVjidVi[4], dVjidVj[4], dx);
  dVijdVi[5] = dVijdVj[5] = dVjidVi[5] = dVjidVj[5] = 0;
  dVijdVi[6] = dVijdVj[6] = dVjidVi[6] = dVjidVj[6] = 0;
}

//------------------------------------------------------------------------------

template<int dim>
inline
void RecFcnVanAlbadaConstant<dim>::compute(double *pi, double *ddpij, double *pj, double *ddpji,
                                           double *aij, double *aji, double *bij, double *bji,
                                           double *pij, double *pji) {
  fprintf(stderr, "*** Error: RecFcnVanAlbadaConstant<%d>::compute is not overloaded\n", dim);
  exit(1);
}

//------------------------------------------------------------------------------

template<>
inline
void RecFcnVanAlbadaConstant<6>::compute(double *pi, double *ddpij, double *pj, double *ddpji,
                                         double *aij, double *aji, double *bij, double *bji,
                                         double *pij, double *pji) {
  vanalbada(pi[0], ddpij[0], pj[0], ddpji[0], aij[0], aji[0], bij[0], bji[0], pij[0], pji[0]);
  vanalbada(pi[1], ddpij[1], pj[1], ddpji[1], aij[1], aji[1], bij[1], bji[1], pij[1], pji[1]);
  vanalbada(pi[2], ddpij[2], pj[2], ddpji[2], aij[2], aji[2], bij[2], bji[2], pij[2], pji[2]);
  vanalbada(pi[3], ddpij[3], pj[3], ddpji[3], aij[3], aji[3], bij[3], bji[3], pij[3], pji[3]);
  vanalbada(pi[4], ddpij[4], pj[4], ddpji[4], aij[4], aji[4], bij[4], bji[4], pij[4], pji[4]);
  constant(pi[5], pj[5], pij[5], pji[5]);
}

//------------------------------------------------------------------------------

template<>
inline
void RecFcnVanAlbadaConstant<7>::compute(double *pi, double *ddpij, double *pj, double *ddpji,
                                         double *aij, double *aji, double *bij, double *bji,
                                         double *pij, double *pji) {
  vanalbada(pi[0], ddpij[0], pj[0], ddpji[0], aij[0], aji[0], bij[0], bji[0], pij[0], pji[0]);
  vanalbada(pi[1], ddpij[1], pj[1], ddpji[1], aij[1], aji[1], bij[1], bji[1], pij[1], pji[1]);
  vanalbada(pi[2], ddpij[2], pj[2], ddpji[2], aij[2], aji[2], bij[2], bji[2], pij[2], pji[2]);
  vanalbada(pi[3], ddpij[3], pj[3], ddpji[3], aij[3], aji[3], bij[3], bji[3], pij[3], pji[3]);
  vanalbada(pi[4], ddpij[4], pj[4], ddpji[4], aij[4], aji[4], bij[4], bji[4], pij[4], pji[4]);
  constant(pi[5], pj[5], pij[5], pji[5]);
  constant(pi[6], pj[6], pij[6], pji[6]);
}

//------------------------------------------------------------------------------

template<int dim>
inline
void RecFcnVanAlbadaConstant<dim>::precompute(double *Vi, double *ddVij, double *Vj, double *ddVji,
                                              double *aij, double *aji, double *bij, double *bji, double *dx) {
  fprintf(stderr, "*** Error: RecFcnVanAlbadaConstant<%d>::precompute is not overloaded\n", dim);
  exit(1);
}

//------------------------------------------------------------------------------

template<>
inline
void RecFcnVanAlbadaConstant<6>::precompute(double *Vi, double *ddVij, double *Vj, double *ddVji,
                                            double *aij, double *aji, double *bij, double *bji, double *dx) {
  prevanalbada(Vi[0], ddVij[0], Vj[0], ddVji[0], aij[0], aji[0], bij[0], bji[0], dx);
  prevanalbada(Vi[1], ddVij[1], Vj[1], ddVji[1], aij[1], aji[1], bij[1], bji[1], dx);
  prevanalbada(Vi[2], ddVij[2], Vj[2], ddVji[2], aij[2], aji[2], bij[2], bji[2], dx);
  prevanalbada(Vi[3], ddVij[3], Vj[3], ddVji[3], aij[3], aji[3], bij[3], bji[3], dx);
  prevanalbada(Vi[4], ddVij[4], Vj[4], ddVji[4], aij[4], aji[4], bij[4], bji[4], dx);
}

//------------------------------------------------------------------------------

template<>
inline
void RecFcnVanAlbadaConstant<7>::precompute(double *Vi, double *ddVij, double *Vj, double *ddVji,
                                            double *aij, double *aji, double *bij, double *bji, double *dx) {
  prevanalbada(Vi[0], ddVij[0], Vj[0], ddVji[0], aij[0], aji[0], bij[0], bji[0], dx);
  prevanalbada(Vi[1], ddVij[1], Vj[1], ddVji[1], aij[1], aji[1], bij[1], bji[1], dx);
  prevanalbada(Vi[2], ddVij[2], Vj[2], ddVji[2], aij[2], aji[2], bij[2], bji[2], dx);
  prevanalbada(Vi[3], ddVij[3], Vj[3], ddVji[3], aij[3], aji[3], bij[3], bji[3], dx);
  prevanalbada(Vi[4], ddVij[4], Vj[4], ddVji[4], aij[4], aji[4], bij[4], bji[4], dx);
}

//------------------------------------------------------------------------------

template<int dim>
inline
void RecFcnVanAlbadaConstant<dim>::computeDerivative(double *Vi, double *dVi, double *ddVij, double *dddVij,
                                                     double *Vj, double *dVj, double *ddVji, double *dddVji,
                                                     double *dVij, double *dVji, double *dx) {
  fprintf(stderr, "*** Error: RecFcnVanAlbadaConstant<%d>::computeDerivative is not overloaded\n", dim);
  exit(1);
}

//------------------------------------------------------------------------------

template<>
inline
void RecFcnVanAlbadaConstant<6>::computeDerivative(double *Vi, double *dVi, double *ddVij, double *dddVij,
                                                   double *Vj, double *dVj, double *ddVji, double *dddVji,
                                                   double *dVij, double *dVji, double *dx) {
  vanalbadaDerivative(Vi[0], dVi[0], ddVij[0], dddVij[0], Vj[0], dVj[0], ddVji[0], dddVji[0], dVij[0], dVji[0], dx);
  vanalbadaDerivative(Vi[1], dVi[1], ddVij[1], dddVij[1], Vj[1], dVj[1], ddVji[1], dddVji[1], dVij[1], dVji[1], dx);
  vanalbadaDerivative(Vi[2], dVi[2], ddVij[2], dddVij[2], Vj[2], dVj[2], ddVji[2], dddVji[2], dVij[2], dVji[2], dx);
  vanalbadaDerivative(Vi[3], dVi[3], ddVij[3], dddVij[3], Vj[3], dVj[3], ddVji[3], dddVji[3], dVij[3], dVji[3], dx);
  vanalbadaDerivative(Vi[4], dVi[4], ddVij[4], dddVij[4], Vj[4], dVj[4], ddVji[4], dddVji[4], dVij[4], dVji[4], dx);
  constantDerivative(dVi[5], dVj[5], dVij[5], dVji[5]);
}

//------------------------------------------------------------------------------

template<>
inline
void RecFcnVanAlbadaConstant<7>::computeDerivative(double *Vi, double *dVi, double *ddVij, double *dddVij, double *Vj, double *dVj,
                                                   double *ddVji, double *dddVji, double *dVij, double *dVji, double *dx) {
  vanalbadaDerivative(Vi[0], dVi[0], ddVij[0], dddVij[0], Vj[0], dVj[0], ddVji[0], dddVji[0], dVij[0], dVji[0], dx);
  vanalbadaDerivative(Vi[1], dVi[1], ddVij[1], dddVij[1], Vj[1], dVj[1], ddVji[1], dddVji[1], dVij[1], dVji[1], dx);
  vanalbadaDerivative(Vi[2], dVi[2], ddVij[2], dddVij[2], Vj[2], dVj[2], ddVji[2], dddVji[2], dVij[2], dVji[2], dx);
  vanalbadaDerivative(Vi[3], dVi[3], ddVij[3], dddVij[3], Vj[3], dVj[3], ddVji[3], dddVji[3], dVij[3], dVji[3], dx);
  vanalbadaDerivative(Vi[4], dVi[4], ddVij[4], dddVij[4], Vj[4], dVj[4], ddVji[4], dddVji[4], dVij[4], dVji[4], dx);
  constantDerivative(dVi[5], dVj[5], dVij[5], dVji[5]);
  constantDerivative(dVi[6], dVj[6], dVij[6], dVji[6]);
}

//------------------------------------------------------------------------------

template<int dim>
inline
void RecFcnVanAlbadaConstant<dim>::computeDerivativeOperators(double *Vi, double *ddVij, double *Vj, double *ddVji,
                                                              double *dVijdVi, double *dVijdVj, double *dVijdddVij,
                                                              double *dVjidVi, double *dVjidVj, double *dVjidddVji,
                                                              double *dx) {
  fprintf(stderr, "*** Error: RecFcnVanAlbadaConstant<%d>::computeDerivativeOperators is not overloaded\n", dim);
  exit(1);
}

//------------------------------------------------------------------------------

template<>
inline
void RecFcnVanAlbadaConstant<6>::computeDerivativeOperators(double *Vi, double *ddVij, double *Vj, double *ddVji,
                                                            double *dVijdVi, double *dVijdVj, double *dVijdddVij,
                                                            double *dVjidVi, double *dVjidVj, double *dVjidddVji,
                                                            double *dx) {
  vanalbadaDerivativeOperators(Vi[0], ddVij[0], Vj[0], ddVji[0], dVijdVi[0], dVijdVj[0], dVijdddVij[0], dVjidVi[0], dVjidVj[0], dVjidddVji[0], dx);
  vanalbadaDerivativeOperators(Vi[1], ddVij[1], Vj[1], ddVji[1], dVijdVi[1], dVijdVj[1], dVijdddVij[1], dVjidVi[1], dVjidVj[1], dVjidddVji[1], dx);
  vanalbadaDerivativeOperators(Vi[2], ddVij[2], Vj[2], ddVji[2], dVijdVi[2], dVijdVj[2], dVijdddVij[2], dVjidVi[2], dVjidVj[2], dVjidddVji[2], dx);
  vanalbadaDerivativeOperators(Vi[3], ddVij[3], Vj[3], ddVji[3], dVijdVi[3], dVijdVj[3], dVijdddVij[3], dVjidVi[3], dVjidVj[3], dVjidddVji[3], dx);
  vanalbadaDerivativeOperators(Vi[4], ddVij[4], Vj[4], ddVji[4], dVijdVi[4], dVijdVj[4], dVijdddVij[4], dVjidVi[4], dVjidVj[4], dVjidddVji[4], dx);
  constantDerivativeOperators(dVijdVi[5], dVjidVj[5]);
}

//------------------------------------------------------------------------------

template<>
inline
void RecFcnVanAlbadaConstant<7>::computeDerivativeOperators(double *Vi, double *ddVij, double *Vj, double *ddVji,
                                                            double *dVijdVi, double *dVijdVj, double *dVijdddVij,
                                                            double *dVjidVi, double *dVjidVj, double *dVjidddVji,
                                                            double *dx) {
  vanalbadaDerivativeOperators(Vi[0], ddVij[0], Vj[0], ddVji[0], dVijdVi[0], dVijdVj[0], dVijdddVij[0], dVjidVi[0], dVjidVj[0], dVjidddVji[0], dx);
  vanalbadaDerivativeOperators(Vi[1], ddVij[1], Vj[1], ddVji[1], dVijdVi[1], dVijdVj[1], dVijdddVij[1], dVjidVi[1], dVjidVj[1], dVjidddVji[1], dx);
  vanalbadaDerivativeOperators(Vi[2], ddVij[2], Vj[2], ddVji[2], dVijdVi[2], dVijdVj[2], dVijdddVij[2], dVjidVi[2], dVjidVj[2], dVjidddVji[2], dx);
  vanalbadaDerivativeOperators(Vi[3], ddVij[3], Vj[3], ddVji[3], dVijdVi[3], dVijdVj[3], dVijdddVij[3], dVjidVi[3], dVjidVj[3], dVjidddVji[3], dx);
  vanalbadaDerivativeOperators(Vi[4], ddVij[4], Vj[4], ddVji[4], dVijdVi[4], dVijdVj[4], dVijdddVij[4], dVjidVi[4], dVjidVj[4], dVjidddVji[4], dx);
  constantDerivativeOperators(dVijdVi[5], dVjidVj[5]);
  constantDerivativeOperators(dVijdVi[6], dVjidVj[6]);
}

//------------------------------------------------------------------------------

template<int dim>
inline
void RecFcnLinearVanAlbada<dim>::compute(double *Vi, double *ddVij, double *Vj, double *ddVji,
                                         double *Vij, double *Vji, double *dx) {
  fprintf(stderr, "*** Error: RecFcnLinearVanAlbada<%d>::compute is not overloaded\n", dim);
  exit(1);
}

//------------------------------------------------------------------------------

template<>
inline
void RecFcnLinearVanAlbada<6>::compute(double *Vi, double *ddVij, double *Vj, double *ddVji,
                                       double *Vij, double *Vji, double *dx) {
  linear(Vi[0], ddVij[0], Vj[0], ddVji[0], Vij[0], Vji[0]);
  linear(Vi[1], ddVij[1], Vj[1], ddVji[1], Vij[1], Vji[1]);
  linear(Vi[2], ddVij[2], Vj[2], ddVji[2], Vij[2], Vji[2]);
  linear(Vi[3], ddVij[3], Vj[3], ddVji[3], Vij[3], Vji[3]);
  linear(Vi[4], ddVij[4], Vj[4], ddVji[4], Vij[4], Vji[4]);
  vanalbada(Vi[5], ddVij[5], Vj[5], ddVji[5], Vij[5], Vji[5], dx);
}

//------------------------------------------------------------------------------

template<>
inline
void RecFcnLinearVanAlbada<7>::compute(double *Vi, double *ddVij, double *Vj, double *ddVji,
                                       double *Vij, double *Vji, double *dx) {
  linear(Vi[0], ddVij[0], Vj[0], ddVji[0], Vij[0], Vji[0]);
  linear(Vi[1], ddVij[1], Vj[1], ddVji[1], Vij[1], Vji[1]);
  linear(Vi[2], ddVij[2], Vj[2], ddVji[2], Vij[2], Vji[2]);
  linear(Vi[3], ddVij[3], Vj[3], ddVji[3], Vij[3], Vji[3]);
  linear(Vi[4], ddVij[4], Vj[4], ddVji[4], Vij[4], Vji[4]);
  vanalbada(Vi[5], ddVij[5], Vj[5], ddVji[5], Vij[5], Vji[5], dx);
  vanalbada(Vi[6], ddVij[6], Vj[6], ddVji[6], Vij[6], Vji[6], dx);
}

//------------------------------------------------------------------------------

template<int dim>
inline
void RecFcnLinearVanAlbada<dim>::computeJacobian(double *Vi, double *ddVij, double *Vj, double *ddVji,
                                                 double *dVijdVi, double *dVijdVj, double *dVjidVi, double *dVjidVj, double *dx) {
  fprintf(stderr, "*** Error: RecFcnLinearVanAlbada<%d>::computeJacobian is not overloaded\n", dim);
  exit(1);
}

//------------------------------------------------------------------------------

template<>
inline
void RecFcnLinearVanAlbada<6>::computeJacobian(double *Vi, double *ddVij, double *Vj, double *ddVji,
                                               double *dVijdVi, double *dVijdVj, double *dVjidVi, double *dVjidVj, double *dx) {
  linearjac(dVijdVi[0], dVijdVj[0], dVjidVi[0], dVjidVj[0]);
  linearjac(dVijdVi[1], dVijdVj[1], dVjidVi[1], dVjidVj[1]);
  linearjac(dVijdVi[2], dVijdVj[2], dVjidVi[2], dVjidVj[2]);
  linearjac(dVijdVi[3], dVijdVj[3], dVjidVi[3], dVjidVj[3]);
  linearjac(dVijdVi[4], dVijdVj[4], dVjidVi[4], dVjidVj[4]);
  vanalbadajac(Vi[5], ddVij[5], Vj[5], ddVji[5], dVijdVi[5], dVijdVj[5], dVjidVi[5], dVjidVj[5], dx);
}

//------------------------------------------------------------------------------

template<>
inline
void RecFcnLinearVanAlbada<7>::computeJacobian(double *Vi, double *ddVij, double *Vj, double *ddVji,
                                               double *dVijdVi, double *dVijdVj, double *dVjidVi, double *dVjidVj, double *dx) {
  linearjac(dVijdVi[0], dVijdVj[0], dVjidVi[0], dVjidVj[0]);
  linearjac(dVijdVi[1], dVijdVj[1], dVjidVi[1], dVjidVj[1]);
  linearjac(dVijdVi[2], dVijdVj[2], dVjidVi[2], dVjidVj[2]);
  linearjac(dVijdVi[3], dVijdVj[3], dVjidVi[3], dVjidVj[3]);
  linearjac(dVijdVi[4], dVijdVj[4], dVjidVi[4], dVjidVj[4]);
  vanalbadajac(Vi[5], ddVij[5], Vj[5], ddVji[5], dVijdVi[5], dVijdVj[5], dVjidVi[5], dVjidVj[5], dx);
  vanalbadajac(Vi[6], ddVij[6], Vj[6], ddVji[6], dVijdVi[6], dVijdVj[6], dVjidVi[6], dVjidVj[6], dx);
}

//------------------------------------------------------------------------------

template<int dim>
inline
void RecFcnLinearVanAlbada<dim>::compute(double *pi, double *ddpij, double *pj, double *ddpji,
                                         double *aij, double *aji, double *bij, double *bji,
                                         double *pij, double *pji) {
  fprintf(stderr, "*** Error: RecFcnLinearVanAlbada<%d>::compute is not overloaded\n", dim);
  exit(1);
}

//------------------------------------------------------------------------------

template<>
inline
void RecFcnLinearVanAlbada<6>::compute(double *pi, double *ddpij, double *pj, double *ddpji,
                                       double *aij, double *aji, double *bij, double *bji,
                                       double *pij, double *pji) {
  linear(pi[0], ddpij[0], pj[0], ddpji[0], pij[0], pji[0]);
  linear(pi[1], ddpij[1], pj[1], ddpji[1], pij[1], pji[1]);
  linear(pi[2], ddpij[2], pj[2], ddpji[2], pij[2], pji[2]);
  linear(pi[3], ddpij[3], pj[3], ddpji[3], pij[3], pji[3]);
  linear(pi[4], ddpij[4], pj[4], ddpji[4], pij[4], pji[4]);
  vanalbada(pi[5], ddpij[5], pj[5], ddpji[5], aij[5], aji[5], bij[5], bji[5], pij[5], pji[5]);
}

//------------------------------------------------------------------------------

template<>
inline
void RecFcnLinearVanAlbada<7>::compute(double *pi, double *ddpij, double *pj, double *ddpji,
                                       double *aij, double *aji, double *bij, double *bji,
                                       double *pij, double *pji) {
  linear(pi[0], ddpij[0], pj[0], ddpji[0], pij[0], pji[0]);
  linear(pi[1], ddpij[1], pj[1], ddpji[1], pij[1], pji[1]);
  linear(pi[2], ddpij[2], pj[2], ddpji[2], pij[2], pji[2]);
  linear(pi[3], ddpij[3], pj[3], ddpji[3], pij[3], pji[3]);
  linear(pi[4], ddpij[4], pj[4], ddpji[4], pij[4], pji[4]);
  vanalbada(pi[5], ddpij[5], pj[5], ddpji[5], aij[5], aji[5], bij[5], bji[5], pij[5], pji[5]);
  vanalbada(pi[6], ddpij[6], pj[6], ddpji[6], aij[6], aji[6], bij[6], bji[6], pij[6], pji[6]);
}

//------------------------------------------------------------------------------

template<int dim>
inline
void RecFcnLinearVanAlbada<dim>::precompute(double *Vi, double *ddVij, double *Vj, double *ddVji,
                                            double *aij, double *aji, double *bij, double *bji, double *dx) {
  fprintf(stderr, "*** Error: RecFcnLinearVanAlbada<%d>::precompute is not overloaded\n", dim);
  exit(1);
}

//------------------------------------------------------------------------------

template<>
inline
void RecFcnLinearVanAlbada<6>::precompute(double *Vi, double *ddVij, double *Vj, double *ddVji,
                                          double *aij, double *aji, double *bij, double *bji, double *dx) {
  prevanalbada(Vi[5], ddVij[5], Vj[5], ddVji[5], aij[5], aji[5], bij[5], bji[5], dx);
}

//------------------------------------------------------------------------------

template<>
inline
void RecFcnLinearVanAlbada<7>::precompute(double *Vi, double *ddVij, double *Vj, double *ddVji,
                                          double *aij, double *aji, double *bij, double *bji, double *dx) {
  prevanalbada(Vi[5], ddVij[5], Vj[5], ddVji[5], aij[5], aji[5], bij[5], bji[5], dx);
  prevanalbada(Vi[6], ddVij[6], Vj[6], ddVji[6], aij[6], aji[6], bij[6], bji[6], dx);
}

//------------------------------------------------------------------------------

template<int dim>
inline
void RecFcnLinearVanAlbada<dim>::computeDerivative(double *Vi, double *dVi, double *ddVij, double *dddVij, double *Vj,
                                                   double *dVj, double *ddVji, double *dddVji, double *dVij, double *dVji, double *dx) {
  fprintf(stderr, "*** Error: RecFcnLinearVanAlbada<%d>::computeDerivative is not overloaded\n", dim);
  exit(1);
}

//------------------------------------------------------------------------------

template<>
inline
void RecFcnLinearVanAlbada<6>::computeDerivative(double *Vi, double *dVi, double *ddVij, double *dddVij, double *Vj,
                                                 double *dVj, double *ddVji, double *dddVji, double *dVij, double *dVji, double *dx) {
  linearDerivative(dVi[0], dddVij[0], dVj[0], dddVji[0], dVij[0], dVji[0]);
  linearDerivative(dVi[1], dddVij[1], dVj[1], dddVji[1], dVij[1], dVji[1]);
  linearDerivative(dVi[2], dddVij[2], dVj[2], dddVji[2], dVij[2], dVji[2]);
  linearDerivative(dVi[3], dddVij[3], dVj[3], dddVji[3], dVij[3], dVji[3]);
  linearDerivative(dVi[4], dddVij[4], dVj[4], dddVji[4], dVij[4], dVji[4]);
  vanalbadaDerivative(Vi[5], dVi[5], ddVij[5], dddVij[5], Vj[5], dVj[5], ddVji[5], dddVji[5], dVij[5], dVji[5], dx);
}

//------------------------------------------------------------------------------

template<>
inline
void RecFcnLinearVanAlbada<7>::computeDerivative(double *Vi, double *dVi, double *ddVij, double *dddVij, double *Vj,
                                                 double *dVj, double *ddVji, double *dddVji, double *dVij, double *dVji, double *dx) {
  linearDerivative(dVi[0], dddVij[0], dVj[0], dddVji[0], dVij[0], dVji[0]);
  linearDerivative(dVi[1], dddVij[1], dVj[1], dddVji[1], dVij[1], dVji[1]);
  linearDerivative(dVi[2], dddVij[2], dVj[2], dddVji[2], dVij[2], dVji[2]);
  linearDerivative(dVi[3], dddVij[3], dVj[3], dddVji[3], dVij[3], dVji[3]);
  linearDerivative(dVi[4], dddVij[4], dVj[4], dddVji[4], dVij[4], dVji[4]);
  vanalbadaDerivative(Vi[5], dVi[5], ddVij[5], dddVij[5], Vj[5], dVj[5], ddVji[5], dddVji[5], dVij[5], dVji[5], dx);
  vanalbadaDerivative(Vi[6], dVi[6], ddVij[6], dddVij[6], Vj[6], dVj[6], ddVji[6], dddVji[6], dVij[6], dVji[6], dx);
}

//------------------------------------------------------------------------------

template<int dim>
inline
void RecFcnLinearVanAlbada<dim>::computeDerivativeOperators(double *Vi, double *ddVij, double *Vj, double *ddVji,
                                                            double *dVijdVi, double *dVijdVj, double *dVijdddVij,
                                                            double *dVjidVi, double *dVjidVj, double *dVjidddVji,
                                                            double *dx) {
  fprintf(stderr, "*** Error: RecFcnLinearVanAlbada<%d>::computeDerivativeOperators is not overloaded\n", dim);
  exit(1);
}

//------------------------------------------------------------------------------

template<>
inline
void RecFcnLinearVanAlbada<6>::computeDerivativeOperators(double *Vi, double *ddVij, double *Vj, double *ddVji,
                                                          double *dVijdVi, double *dVijdVj, double *dVijdddVij,
                                                          double *dVjidVi, double *dVjidVj, double *dVjidddVji,
                                                          double *dx) {
  linearDerivativeOperators(dVijdVi[0], dVijdVj[0], dVijdddVij[0], dVjidVi[0], dVjidVj[0], dVjidddVji[0]);
  linearDerivativeOperators(dVijdVi[1], dVijdVj[1], dVijdddVij[1], dVjidVi[1], dVjidVj[1], dVjidddVji[1]);
  linearDerivativeOperators(dVijdVi[2], dVijdVj[2], dVijdddVij[2], dVjidVi[2], dVjidVj[2], dVjidddVji[2]);
  linearDerivativeOperators(dVijdVi[3], dVijdVj[3], dVijdddVij[3], dVjidVi[3], dVjidVj[3], dVjidddVji[3]);
  linearDerivativeOperators(dVijdVi[4], dVijdVj[4], dVijdddVij[4], dVjidVi[4], dVjidVj[4], dVjidddVji[4]);
  vanalbadaDerivativeOperators(Vi[5], ddVij[5], Vj[5], ddVji[5], dVijdVi[5], dVijdVj[5], dVijdddVij[5], dVjidVi[5], dVjidVj[5], dVjidddVji[5], dx);
}

//------------------------------------------------------------------------------

template<>
inline
void RecFcnLinearVanAlbada<7>::computeDerivativeOperators(double *Vi, double *ddVij, double *Vj, double *ddVji,
                                                          double *dVijdVi, double *dVijdVj, double *dVijdddVij,
                                                          double *dVjidVi, double *dVjidVj, double *dVjidddVji,
                                                          double *dx) {
  linearDerivativeOperators(dVijdVi[0], dVijdVj[0], dVijdddVij[0], dVjidVi[0], dVjidVj[0], dVjidddVji[0]);
  linearDerivativeOperators(dVijdVi[1], dVijdVj[1], dVijdddVij[1], dVjidVi[1], dVjidVj[1], dVjidddVji[1]);
  linearDerivativeOperators(dVijdVi[2], dVijdVj[2], dVijdddVij[2], dVjidVi[2], dVjidVj[2], dVjidddVji[2]);
  linearDerivativeOperators(dVijdVi[3], dVijdVj[3], dVijdddVij[3], dVjidVi[3], dVjidVj[3], dVjidddVji[3]);
  linearDerivativeOperators(dVijdVi[4], dVijdVj[4], dVijdddVij[4], dVjidVi[4], dVjidVj[4], dVjidddVji[4]);
  vanalbadaDerivativeOperators(Vi[5], ddVij[5], Vj[5], ddVji[5], dVijdVi[5], dVijdVj[5], dVijdddVij[5], dVjidVi[5], dVjidVj[5], dVjidddVji[5], dx);
  vanalbadaDerivativeOperators(Vi[6], ddVij[6], Vj[6], ddVji[6], dVijdVi[6], dVijdVj[6], dVijdddVij[6], dVjidVi[6], dVjidVj[6], dVjidddVji[6], dx);
}

//------------------------------------------------------------------------------

#endif

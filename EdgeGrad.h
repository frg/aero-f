#ifndef _EDGE_GRAD_H_
#define _EDGE_GRAD_H_

#include <iostream>
#include <unordered_map>
#include <complex>

class IoData;
class Elem;
class ElemSet;

template<class Scalar> class Vec;
template<class Scalar, int Dim> class SVec;

struct Vec3D;

//------------------------------------------------------------------------------

struct V6NodeData {
  int tet, face;
  double r, t;
  bool remote;
  V6NodeData() {
    tet = -1;
    face = -1;
    r = 0.0;
    t = 0.0;
    remote = false;
  }
};

//------------------------------------------------------------------------------

class EdgeGrad {

  double beta;
  double xiu;
  double xic;

  typedef V6NodeData (*V6NodeDataOf2)[2];
  V6NodeDataOf2 v6data;

  std::unordered_map<int, double *> v6remote;  // map from index (l, dir) (as 2 * l + dir) to remote v6 gradients
                                               // (block size 2 * dim for ddV_u and ddV_f)

  template<class PosVecType, int Dim>
  void addUpwindGradient(Elem&, PosVecType&, SVec<double, Dim>&, Vec3D&, double *, double *, const int);

  void addUpwindGradient(double *, double *, double *, const int);

  template<int Dim>
  void addFaceGradient(V6NodeData&, Elem&, SVec<double, Dim>&, SVec<double, Dim>&, SVec<double, Dim>&,
                       Vec3D&, double *, const int);

  void addFaceGradient(double *, double *, const int);

 public:
  EdgeGrad(IoData&);
  ~EdgeGrad();

  V6NodeDataOf2& getV6NodeData() {
    return v6data;
  }
  std::unordered_map<int, double *>& getV6RemoteData() {
    return v6remote;
  }

  template<class PosVecType, int Dim>
  void compute(int, int, int, ElemSet&, PosVecType&, SVec<double, Dim>&, SVec<double, Dim>&, SVec<double, Dim>&,
               SVec<double, Dim>&, Vec<bool> *, Vec3D&, double *, double *);

  template<class PosVecType, int Dim>
  void computeDerivative(int, int, int, ElemSet&, PosVecType&, PosVecType&, SVec<double, Dim>&, SVec<double, Dim>&,
                         SVec<double, Dim>&, SVec<double, Dim>&, SVec<double, Dim>&, SVec<double, Dim>&,
                         SVec<double, Dim>&, SVec<double, Dim>&, double *, double *);

  template<class PosVecType, int Dim>
  void compute(int, int, int, ElemSet&, PosVecType&, SVec<std::complex<double>, Dim>&, SVec<std::complex<double>, Dim>&,
               SVec<std::complex<double>, Dim>&, SVec<std::complex<double>, Dim>&, Vec<bool> *, Vec3D&,
               std::complex<double> *, std::complex<double> *) {
    std::cerr << "*** Error: EdgeGrad::compute is not implemented for complex\n";
    exit(-1);
  }

  template<class PosVecType, int Dim>
  void precomputeUpwindGradient(Elem&, PosVecType&, SVec<double, Dim>&, Vec3D&, double *, const int);

  template<int Dim>
  void precomputeFaceGradient(V6NodeData&, Elem&, SVec<double, Dim>&, SVec<double, Dim>&, SVec<double, Dim>&,
                              Vec3D&, double *, const int);

};

//------------------------------------------------------------------------------

#endif

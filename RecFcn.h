#ifndef _REC_FCN_H_
#define _REC_FCN_H_

#include <cmath>
#include <algorithm>
#include <complex>
#include <iostream>

//------------------------------------------------------------------------------

struct RecFcnBase {

  virtual void compute(double *, double *, double *, double *, double *, double *,
                       double *) = 0;

  virtual void computeJacobian(double *, double *, double *, double *,
                               double *, double *, double *, double *,
                               double *) = 0;

  virtual void compute(double *, double *, double *, double *, double *, double *,
                       double *, double *, double *, double *) = 0;

  virtual void precompute(double *, double *, double *, double *, double *, double *,
                          double *, double *, double *) = 0;

  virtual void compute(std::complex<double> *, std::complex<double> *, std::complex<double> *,
                       std::complex<double> *, double *, double *, double *, double *,
                       std::complex<double> *, std::complex<double> *) {
    std::cerr << "*** Error: RecFcnBase::compute is not implemented for std::complex<double>\n";
    exit(-1);
  }

  virtual void computeDerivative(double *, double *, double *, double *, double *, double *,
                                 double *, double *, double *, double *, double *) {
    std::cerr << "*** Error: RecFcnBase::computeDerivative is not implemented\n";
    exit(-1);
  }

   virtual void computeDerivativeOperators(double *, double *, double *, double *, double *,
                                           double *, double *, double *, double *, double *, double *) {
     std::cerr << "*** Error: RecFcnBase::computeDerivativeOperators is not implemented\n";
     exit(-1);
   }

};

//------------------------------------------------------------------------------

class RecFcn : virtual public RecFcnBase {

 protected:
  double beta;
  double beta1;
  double beta4;
  int _dim;
  double K;

  RecFcn(double b, int d) {
    beta = b;
    beta1 = 0.5 - beta;
    beta4 = 1.0 - 4.0 * beta;
    _dim = d;
    K = -1;
  }
  RecFcn(double b, int d, double k) {
    beta = b;
    beta1 = 0.5 - beta;
    beta4 = 1.0 - 4.0 * beta;
    _dim = d;
    K = k;
  }

 public:
  virtual ~RecFcn() {}

  virtual bool isConstant() {
    return false;
  }

 protected:
  template<class Scalar>
  void constant(Scalar, Scalar, Scalar&, Scalar&);

  void constantDerivative(double, double, double&, double&);

  void constantDerivativeOperators(double&, double&);

  template<class Scalar>
  void linear(Scalar, Scalar, Scalar, Scalar, Scalar&, Scalar&);

  void linearDerivative(double, double, double, double, double&, double&);

  void linearDerivativeOperators(double&, double&, double&, double&, double&, double&);

  void linearjac(double&, double&, double&, double&);

  void vanalbada(double, double, double, double, double&, double&, double *);

  void vanalbadajac(double, double, double, double, double&, double&, double&, double&, double *);

  template<class Scalar>
  void vanalbada(Scalar, Scalar, Scalar, Scalar, double, double, double, double, Scalar&, Scalar&);

  void prevanalbada(double, double, double, double, double&, double&, double&, double&, double *);

  void vanalbadaDerivative(double, double, double, double, double, double, double, double, double&, double&, double *);

  void vanalbadaDerivativeOperators(double, double, double, double, double&, double&, double&, double&, double&, double&, double *);

};

//------------------------------------------------------------------------------

template<class Scalar>
inline
void RecFcn::constant(Scalar Vi, Scalar Vj, Scalar& Vij, Scalar& Vji) {
  Vij = Vi;
  Vji = Vj;
}

//------------------------------------------------------------------------------

inline
void RecFcn::constantDerivative(double dVi, double dVj, double& dVij, double& dVji) {
  dVij = dVi;
  dVji = dVj;
}

//------------------------------------------------------------------------------

inline
void RecFcn::constantDerivativeOperators(double& dVijdVi, double& dVjidVj) {
  dVijdVi = 1;
  dVjidVj = 1;
}

//------------------------------------------------------------------------------

template<class Scalar>
inline
void RecFcn::linear(Scalar Vi, Scalar ddVij, Scalar Vj, Scalar ddVji,
                    Scalar& Vij, Scalar& Vji) {
  Scalar dV = beta1 * (Vj - Vi);
  Vij = Vi + dV + beta * ddVij;
  Vji = Vj - dV - beta * ddVji;
}

//------------------------------------------------------------------------------

inline
void RecFcn::linearDerivative(double dVi, double dddVij, double dVj, double dddVji,
                              double& dVij, double& dVji) {
  // ∂[dV]/∂s = (0.5-β)*(∂[Vj]/∂s - ∂[Vi]/∂s)
  double ddV = beta1 * (dVj - dVi);

  // ∂[Vij]/∂s = ∂[Vi]/∂s + ∂[dV]/∂s + β*∂[ddVij]/∂s
  //           = ∂[Vi]/∂s + (0.5-β)*(∂[Vj]/∂s - ∂[Vi]/∂s) + β*∂[ddVij]/∂s
  //           = (0.5+β)*∂[Vi]/∂s + (0.5-β)*∂[Vj]/∂s + β*∂[ddVij]/∂s

  dVij = dVi + ddV + beta * dddVij;
  // ∂[Vji]/∂s = ∂[Vj]/∂s - ∂[dV]/∂s - β*∂[ddVji]/∂s
  //           = ∂[Vj]/∂s - (0.5-β)*(∂[Vj]/∂s - ∂[Vi]/∂s) - β*∂[ddVji]/∂s
  //           = (0.5+β)*∂[Vj]/∂s + (0.5-β)*∂[Vi]/∂s - β*∂[ddVji]/∂s
  dVji = dVj - ddV - beta * dddVji;
}

//------------------------------------------------------------------------------

inline
void RecFcn::linearDerivativeOperators(double& dVijdVi, double& dVijdVj, double& dVijdddVij, double& dVjidVi,
                                       double& dVjidVj, double& dVjidddVji) {
  dVijdVi = 1 - beta1;
  dVijdVj = beta1;
  dVijdddVij = beta;
  dVjidVi = beta1;
  dVjidVj = 1 - beta1;
  dVjidddVji = -beta;
}

//------------------------------------------------------------------------------

inline
void RecFcn::linearjac(double& dVijdVi, double& dVijdVj, double& dVjidVi, double& dVjidVj) {
  // ∂[Vij]/∂[Vi] = (0.5+β)*∂[Vi]/∂[Vi] + (0.5-β)*∂[Vj]/∂[Vi] + β*∂[ddVij]/∂[Vi]
  //              = (0.5+β) + β*∂[ddVij]/∂[Vi]
  // ∂[Vij]/∂[Vj] = (0.5-β) + β*∂[ddVij]/∂[Vj]
  dVijdVi = beta;   // nodal grad coefficient
  dVijdVj = beta1;  // = 1 - dVij/dVi (without nodal grad part)

  // ∂[Vji]/∂[Vj] = (0.5+β)*∂[Vj]/∂[Vj] + (0.5-β)*∂[Vi]/∂[Vj] - β*∂[ddVji]/∂[Vj]
  //              = (0.5+β) - β*∂[ddVji]/∂[Vj]
  // ∂[Vji]/∂[Vi] = (0.5-β) - β*∂[ddVji]/∂[Vi]
  dVjidVi = beta1;  // = 1 - dVji/dVj (without nodal grad part)
  dVjidVj = -beta;  // nodal grad coefficient
}

//------------------------------------------------------------------------------
// NOTE: the implementation of the van Albada limiter follows the expressions
//       in Lesoinne, Sarkis, Hetmaniuk, and Farhat (2001). This expression (with the 4*beta)
//       IS correct and generates the right asymptotic behavior versus the linear
//       reconstruction case, so don't be tempted to assume there is a bug and
//       replace all 4*beta terms with 2*beta!
inline
void RecFcn::vanalbada(double Vi, double ddVij, double Vj, double ddVji,
                       double& Vij, double& Vji, double *dx) {
  auto f = [this](double& eps2, double& a, double& b) {
    double a2 = a * a;
    double b2 = b * b;
    if((K > 0) || (a * b > 0.0)) {
      return (a * (b2 + eps2) + b * (a2 + eps2)) / (a2 + b2 + 2.0 * eps2);
    }
    else if(a * b == 0.0) {
      return 0.5 * eps2 * (a + b) / (a2 + b2 + 2.0 * eps2);
    }
    else {
      return 0.0;
    }
  };

  double dVji = Vj - Vi;
  double dV   = beta4 * dVji;
  double dVi  = dV + 4.0 * beta * ddVij;
  double dVj  = dV + 4.0 * beta * ddVji;
  double eps2 = (K > 0) ? std::pow(K*std::sqrt(dx[0]*dx[0] + dx[1]*dx[1] + dx[2]*dx[2]), 3) : 1.e-16;
  Vij = Vi + 0.5 * f(eps2, dVi, dVji);
  Vji = Vj - 0.5 * f(eps2, dVj, dVji);
}

//------------------------------------------------------------------------------

inline
void RecFcn::vanalbadajac(double Vi, double ddVij, double Vj, double ddVji,
                          double& dVijdVi, double& dVijdVj, double& dVjidVi, double& dVjidVj, double *dx) {
  auto df = [this](double& eps2, double& a, double& b) {
    double a2 = a * a;
    double b2 = b * b;
    if((K > 0) || (a * b > 0.0)) {
      return (b2 + eps2) * (b2 - a2 + 2.*eps2 + 2.*a * b) / ((a2 + b2 + 2.*eps2) * (a2 + b2 + 2.*eps2));
    }
    else if(a * b == 0.0) {
      return 0.5 * (b2 + eps2) * (b2 - a2 + 2.*eps2) / ((a2 + b2 + 2.*eps2) * (a2 + b2 + 2.*eps2));
    }
    else {
      return 0.0;
    }
  };
  double dVji = Vj - Vi;
  double dV   = beta4 * dVji;
  double dVi  = dV + 4.0 * beta * ddVij;
  double dVj  = dV + 4.0 * beta * ddVji;
  double eps2 = (K > 0) ? std::pow(K*std::sqrt(dx[0]*dx[0] + dx[1]*dx[1] + dx[2]*dx[2]), 3) : 1.e-16;
  double lai  = df(eps2, dVji, dVi);
  double lbi  = df(eps2, dVi, dVji);
  double laj  = df(eps2, dVji, dVj);
  double lbj  = df(eps2, dVj, dVji);
  dVijdVi = 2.0 * beta * lbi;           // this is the nodal grad coefficient of dVij/dVi
  dVijdVj = 0.5 * (lai + beta4 * lbi);  // = 1 - dVij/dVi (without nodal grad part)
  dVjidVi = 0.5 * (laj + beta4 * lbj);  // = 1 - dVji/dVj (without nodal grad part)
  dVjidVj = -2.0 * beta * lbj;          // this is the nodal grad coefficient of dVji/dVj
}

//------------------------------------------------------------------------------
// precomputed van Albada limiter
template<class Scalar>
inline
void RecFcn::vanalbada(Scalar pi, Scalar ddpij, Scalar pj, Scalar ddpji,
                       double aij, double aji, double bij, double bji,
                       Scalar& pij, Scalar& pji) {
  Scalar dpji = pj - pi;
  pij = pi + aij * dpji + bij * ddpij;
  pji = pj - aji * dpji - bji * ddpji;
}

//------------------------------------------------------------------------------

inline
void RecFcn::prevanalbada(double Vi, double ddVij, double Vj, double ddVji,
                          double& aij, double& aji, double& bij, double& bji, double *dx) {
  auto df = [this](double& eps2, double& a, double& b) {
    double a2 = a * a;
    double b2 = b * b;
    if((K > 0) || (a * b > 0.0)) {
      return (b2 + eps2) * (b2 - a2 + 2.*eps2 + 2.*a * b) / ((a2 + b2 + 2.*eps2) * (a2 + b2 + 2.*eps2));
    }
    else if(a * b == 0.0) {
      return 0.5 * (b2 + eps2) * (b2 - a2 + 2.*eps2) / ((a2 + b2 + 2.*eps2) * (a2 + b2 + 2.*eps2));
    }
    else {
      return 0.0;
    }
  };
  double dVji = Vj - Vi;
  double dV   = beta4 * dVji;
  double dVi  = dV + 4.0 * beta * ddVij;
  double dVj  = dV + 4.0 * beta * ddVji;
  double eps2 = (K > 0) ? std::pow(K*std::sqrt(dx[0]*dx[0] + dx[1]*dx[1] + dx[2]*dx[2]), 3) : 1.e-16;
  double lai  = df(eps2, dVji, dVi);
  double lbi  = df(eps2, dVi, dVji);
  double laj  = df(eps2, dVji, dVj);
  double lbj  = df(eps2, dVj, dVji);
  aij = 0.5 * (lai + beta4 * lbi);
  bij = 2.0 * beta * lbi;
  aji = 0.5 * (laj + beta4 * lbj);
  bji = 2.0 * beta * lbj;
}

//------------------------------------------------------------------------------

inline
void RecFcn::vanalbadaDerivative(double Vi, double dVi, double ddVij, double dddVij, double Vj,
                                 double dVj, double ddVji, double dddVji, double& dVij, double& dVji, double *dx) {
  auto df = [this](double eps2, double a, double da, double b, double db) {
    double a2 = a * a;
    double da2 = 2.0 * a * da;
    double b2 = b * b;
    double db2 = 2.0 * b * db;
    if((K > 0) || (a * b > 0.0)) {
      return ((da * (b2 + eps2) + a * db2 + db * (a2 + eps2) + b * da2) * (a2 + b2 + 2.0 * eps2) -
              (a * (b2 + eps2) + b * (a2 + eps2)) * (da2 + db2)) /
             ((a2 + b2 + 2.0 * eps2) * (a2 + b2 + 2.0 * eps2));
    }
    else if(a * b == 0.0) {
      return 0.5 * eps2 * ((da + db) * (a2 + b2 + 2.0 * eps2) - (a + b) * (da2 + db2)) /
            ((a2 + b2 + 2.0 * eps2) * (a2 + b2 + 2.0 * eps2));
    }
    else {
      return 0.0;
    }
  };

  double d_Vji  = Vj - Vi;
  double d_V    = beta4 * d_Vji;
  double dd_Vji = dVj - dVi;
  double dd_V   = beta4 * dd_Vji;
  double d_Vi   = d_V + 4.0 * beta * ddVij;
  double d_Vj   = d_V + 4.0 * beta * ddVji;
  double dd_Vi  = dd_V + 4.0 * beta * dddVij;
  double dd_Vj  = dd_V + 4.0 * beta * dddVji;
  double eps2 = (K > 0) ? std::pow(K*std::sqrt(dx[0]*dx[0] + dx[1]*dx[1] + dx[2]*dx[2]), 3) : 1.e-16;
  dVij = dVi + 0.5 * df(eps2, d_Vi, dd_Vi, d_Vji, dd_Vji);
  dVji = dVj - 0.5 * df(eps2, d_Vj, dd_Vj, d_Vji, dd_Vji);
}

//------------------------------------------------------------------------------

inline
void RecFcn::vanalbadaDerivativeOperators(double Vi, double ddVij, double Vj, double ddVji, double& dVijdVi,
                                          double& dVijdVj, double& dVijdddVij, double& dVjidVi, double& dVjidVj,
                                          double& dVjidddVji, double *dx) {
  auto dops = [this](double eps2, double a, double b, double& dda, double& ddb) {
    double a2 = a * a;
    double b2 = b * b;
    if((K > 0) || (a * b > 0.0)) {
      double c1 = (a2 + b2 + 2.0 * eps2);
      double c2 = (a * (b2 + eps2) + b * (a2 + eps2));
      double c3 = 1.0 / ((a2 + b2 + 2.0 * eps2) * (a2 + b2 + 2.0 * eps2));
      dda = c3 * (c1 * (b2 + eps2 + b * 2.0 * a) - c2 * 2.0 * a);
      ddb = c3 * (c1 * (a * 2.0 * b + a2 + eps2) - c2 * 2.0 * b);
    }
    else if(a * b == 0.0) {
      double c4 = 0.5 * eps2 / ((a2 + b2 + 2.0 * eps2) * (a2 + b2 + 2.0 * eps2));
      double c5 = (a2 + b2 + 2.0 * eps2);
      dda = (c4 * c5 - c4 * (a + b) * 2.0 * a);
      ddb = (c4 * c5 - c4 * (a + b) * 2.0 * b);
    }
    else {
      dda = 0.0;
      ddb = 0.0;
    }
  };

  double d_Vji = Vj - Vi;
  double d_Vi = beta4 * (Vj - Vi) + 4.0 * beta * ddVij;
  double d_Vj = beta4 * (Vj - Vi) + 4.0 * beta * ddVji;
  double dd_VjidVi = -1.0;
  double dd_VjidVj =  1.0;
  double dd_VidVi = -beta4;
  double dd_VidVj =  beta4;
  double dd_VidddVij = 4.0 * beta;
  double dd_VjdVi = -beta4;
  double dd_VjdVj =  beta4;
  double dd_VjdddVji = 4.0 * beta;
  dVijdVi = 1.0;
  dVjidVj = 1.0;
  double dVijdd_Vi, dVijdd_Vji, dVjidd_Vj, dVjidd_Vji;
  double eps2 = (K > 0) ? std::pow(K*std::sqrt(dx[0]*dx[0] + dx[1]*dx[1] + dx[2]*dx[2]), 3) : 1.e-16;
  dops(eps2, d_Vi, d_Vji, dVijdd_Vi, dVijdd_Vji);
  dVijdVi += 0.5 * (dVijdd_Vi * dd_VidVi + dVijdd_Vji * dd_VjidVi);
  dVijdVj =  0.5 * (dVijdd_Vi * dd_VidVj + dVijdd_Vji * dd_VjidVj);
  dVijdddVij = 0.5 * dVijdd_Vi * dd_VidddVij;
  dops(eps2, d_Vj, d_Vji, dVjidd_Vj, dVjidd_Vji);
  dVjidVi = -0.5 * (dVjidd_Vj * dd_VjdVi + dVjidd_Vji * dd_VjidVi);
  dVjidVj += -0.5 * (dVjidd_Vj * dd_VjdVj + dVjidd_Vji * dd_VjidVj);
  dVjidddVji = -0.5 * dVjidd_Vj * dd_VjdddVji;
}

//------------------------------------------------------------------------------

#endif

#ifndef _IMPLICIT_COUPLED_TS_DESC_H_
#define _IMPLICIT_COUPLED_TS_DESC_H_

#include <ImplicitTsDesc.h>

class IoData;
class Domain;
class GeoSource;

template<class Scalar, int dim> class DistSVec;

#ifndef _MATVECPROD_TMPL_
  #define _MATVECPROD_TMPL_
  template<int dim, int neq, class Scalar2 = double> class MatVecProd;
#endif

#ifndef _KSPPREC_TMPL_
  #define _KSPPREC_TMPL_
  template<int Dim, class Scalar2 = double> class KspPrec;
#endif

#ifndef _KSPSLVR_TMPL_
  #define _KSPSLVR_TMPL_
  template<class VecType, class MvpOp, class PrecOp, class IoOp, class ScalarT = double> class KspSolver;
#endif

//------------------------------------------------------------------------------

template<int dim>
class ImplicitCoupledTsDesc : public ImplicitTsDesc<dim> {

 protected:
  MatVecProd<dim, dim> *mvp;
  KspPrec<dim> *pc;
  KspSolver<DistSVec<double, dim>, MatVecProd<dim, dim>, KspPrec<dim>, Communicator> *ksp;
  MatVecProd<dim, dim, PrecScalar> *tmvp;

 public:
  ImplicitCoupledTsDesc(IoData&, GeoSource&, Domain *);
  ~ImplicitCoupledTsDesc();

  void resize1(NodeData *, DistSVec<double, dim>&);
  void resize2();

  void computeJacobian(DistSVec<double, dim>&, DistSVec<double, dim>&);
  void setOperators(DistSVec<double, dim>&);
  void solveLinearSystem(int, DistSVec<double, dim>&, DistSVec<double, dim>&);

};

//------------------------------------------------------------------------------

#endif

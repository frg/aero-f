#ifndef _VAR_FCN_BASE_H_
#define _VAR_FCN_BASE_H_

#include <Vector3D.h>
#include <IoData.h>
#include <Types.h>

#include <cmath>
#include <iostream>
#include <string>
#include <complex>

//--------------------------------------------------------------------------
// This class is the base class for the VarFcnEOS classes where EOS can be
// a stiffened gas, a Tait EOS or a JWL EOS.
// Only elementary functions are declared and/or defined here.
// All arguments must be pertinent to only a single grid node or a single
// state, since it is assumed that the EOS that must be used at this point
// is known.
// As a base class, it is also assumed that the Euler equations are
// considered, meaning there are a priori only 5 variables that
// define each state. Thus for flows with more than 5 variables,
// some transformation operators must be overwritten in
// the appropriate class.
//
// lay-out of the base class is:
//  - 1 -  Transformation Operators
//  - 2 -  General Functions
//  - 3 -  Equations of State Parameters
//  - 4 -  EOS related functions
//--------------------------------------------------------------------------

class VarFcnBase {

 public:

  const char **pname;
  enum Type { PERFECTGAS = 0, STIFFENEDGAS = 1, TAIT = 2, JWL = 3 } type;

  double rhomin, pmin, nutmin;
  bool verif_clipping;

  VarFcnBase(FluidModelData& data) {
    rhomin = data.rhomin;
    pmin = data.pmin;
    nutmin = data.nutmin;
    verif_clipping = data.verif_clipping;
  }
  virtual ~VarFcnBase() {}

  virtual void rstVar(IoData&, FluidModelData& data) {
    pmin = data.pmin;
  }

  virtual bool equal(VarFcnBase *oth) {
    return false;
  }

  //----- Transformation Operators -----//
  virtual void conservativeToPrimitive(double *, double *) = 0;
  virtual void primitiveToConservative(double *, double *) = 0;
  virtual int conservativeToPrimitiveVerification(int_t glob, double *U, double *V) {
    conservativeToPrimitive(U, V);
    return verification(glob, U, V);
  }
  virtual void computedVdU(double *, double *) = 0;
  virtual void computedUdV(double *, double *) = 0;

  virtual void multiplyBydVdU(double *, double *, double *);
  virtual void multiplyBydVdU(double *, std::complex<double> *, std::complex<double> *);
  virtual void postMultiplyBydUdV(double *, double *, double *);
  virtual void postMultiplyBydVdU(double *, double *, double *);
  virtual void preMultiplyBydUdV(double *, double *, double *);
  virtual void postMultiplyBydVdU(double *, double *, std::complex<double> *);
  virtual void postMultiplyBydVdU(double *, double *, float *);

  virtual void postMultiplyBydVdUmean(double *V, double *mat, double *res) {
    fprintf(stderr, "*** Error: postMultiplyBydVdUmean function not defined\n");
    exit(1);
  }
  virtual void postMultiplyBydVdUturb(double *V, double *mat, double *res) {
    fprintf(stderr, "*** Error: postMultiplyBydVdUturb function not defined\n");
    exit(1);
  }
  virtual void postMultiplyBydUdVmean(double *V, double *mat, double *res) {
    fprintf(stderr, "*** Error: postMultiplyBydUdVmean function not defined\n");
    exit(1);
  }
  virtual void postMultiplyBydUdVturb(double *V, double *mat, double *res) {
    fprintf(stderr, "*** Error: postMultiplyBydUdVturb function not defined\n");
    exit(1);
  }

  virtual void extrapolatePrimitive(double un, double c, double *Vb, double *Vinter, double *V) {
    fprintf(stderr, "*** Error: extrapolatePrimitive function not defined\n");
    exit(1);
  }
  virtual void extrapolateCharacteristic(double n[3], double un, double c, double *Vb, double *dV) {
    fprintf(stderr, "*** Error: extrapolateCharacteristic function not defined\n");
    exit(1);
  }
  virtual void primitiveToCharacteristicVariations(double n[3], double *V, double *dV, double *dW) {
    fprintf(stderr, "*** Error: primitiveToCharacteristic function not defined\n");
    exit(1);
  }
  virtual void characteristicToPrimitiveVariations(double n[3], double *V, double *dW, double *dV) {
    fprintf(stderr, "*** Error: characteristicToPrimitive function not defined\n");
    exit(-1);
  }

  virtual void primitiveToConservativeDerivative(double *V, double *dV, double *U, double *dU, double dMach) {
    fprintf(stderr, "*** Error: primitiveToConservativeDerivative function not defined\n");
    exit(1);
  }
  virtual void conservativeToPrimitiveDerivative(double *U, double *dU, double *V, double *dV, double dMach) {
    fprintf(stderr, "*** Error: conservativeToPrimitiveDerivative function not defined\n");
    exit(1);
  }
  virtual void computeConservativeToPrimitiveDerivativeOperators(double *U, double *V, double *dVdU, double *dVdPstiff) {
    fprintf(stderr, "*** Error: computeConservativeToPrimitiveDerivativeOperators function not defined\n");
    exit(1);
  }
  virtual void computePrimitiveToConservativeDerivativeOperators(double *U, double *V, double *dUdV, double *dUdPstiff) {
    fprintf(stderr, "*** Error: computePrimitiveToConservativeDerivativeOperators function not defined\n");
    exit(1);
  }

  //----- General Functions -----//
  virtual int getType() const {
    return type;
  }
  virtual bool doVerification() const {
    return (rhomin != -1.e9 || pmin != -1.e9 || nutmin != -1.e9);
  }
  virtual int verification(int_t glob, double *U, double *V) {
    fprintf(stderr, "*** Error: verification function not defined\n");
    exit(1);
  }

  virtual double getDensity(double *V) const {
    return V[0];
  }
  virtual Vec3D getVelocity(double *V) const {
    return Vec3D(V[1], V[2], V[3]);
  }
  virtual Vec3D getWtVelocity(double *V, double meshVel[3]) const {
    return Vec3D(V[1] - meshVel[0], V[2] - meshVel[1], V[3] - meshVel[2]);
  }
  virtual double getVelocityX(double *V) const {
    return V[1];
  }
  virtual double getVelocityY(double *V) const {
    return V[2];
  }
  virtual double getVelocityZ(double *V) const {
    return V[3];
  }
  virtual double getPressure(double *V) const {
    fprintf(stderr, "*** Error: getPressure function not defined\n");
    exit(1);
  }
  virtual void computedPdV(double *dPdV) const {
    fprintf(stderr, "*** Error: computedPdV function not defined\n");
    exit(1);
  }

  virtual void setDensity(double density, double *V) {
    V[0] = density;
  }
  virtual void setPressure(double p, double *V) {
    fprintf(stderr, "*** Error: setPressure function not defined\n");
    exit(1);
  }
  virtual void setDensity(double *V, double *Vorig) {
    V[0] = Vorig[0];
  }
  virtual void setPressure(double *V, double *Vorig) {
    fprintf(stderr, "*** Error: setPressure function not defined\n");
    exit(1);
  }

  // check that the Euler equations are still hyperbolic
  virtual double checkPressure(double *V) const {
    fprintf(stderr, "*** Error: checkPressure function not defined\n");
    exit(1);
  }
  virtual bool checkReconstructedValues(double *V, int nodeNum, int otherNodeNum,
                                        int failsafe, int_t *locToGlobNodeMap) const {
    bool error = false;
    if(V[0] <= 0.0) {
      error = true;
      const int_t& glNodeNum = locToGlobNodeMap[nodeNum];
      const int_t& otherGlNodeNum = locToGlobNodeMap[otherNodeNum];
      double b = 0.;
      double a = 10 / b;
      if(failsafe)
        fprintf(stderr, "*** Warning: negative density (%e) for node %s after reconstruction 1 on edge %s -> %s\n",
                V[0], std::to_string(glNodeNum).c_str(), std::to_string(glNodeNum).c_str(),
                std::to_string(otherGlNodeNum).c_str());
      else
        fprintf(stderr, "*** Error: negative density (%e) for node %s after reconstruction 2 on edge %s -> %s\n",
                V[0], std::to_string(glNodeNum).c_str(), std::to_string(glNodeNum).c_str(),
                std::to_string(otherGlNodeNum).c_str());
    }
    double pressureCheck = checkPressure(V);
    if(pressureCheck <= 0.0) {
      error = true;
      const int_t& glNodeNum = locToGlobNodeMap[nodeNum];
      const int_t& otherGlNodeNum = locToGlobNodeMap[otherNodeNum];
      if(failsafe)
        fprintf(stderr, "*** Warning: negative pressure (%e) for node %s (rho = %e) after reconstruction 3 on edge %s -> %s\n",
                pressureCheck, std::to_string(glNodeNum).c_str(), V[0], std::to_string(glNodeNum).c_str(),
                std::to_string(otherGlNodeNum).c_str());
      else
        fprintf(stderr, "*** Error: negative pressure (%e) for node %s (rho = %e) after reconstruction 4 on edge %s -> %s\n",
                pressureCheck, std::to_string(glNodeNum).c_str(), V[0], std::to_string(glNodeNum).c_str(),
                std::to_string(otherGlNodeNum).c_str());
    }
    return error;
  }

  virtual double computeTemperature(double *V) const {
    fprintf(stderr, "*** Error: computeTemperature function not defined\n");
    exit(1);
  }
  virtual double computedTdp(double rho, double p) const {
    fprintf(stderr, "*** Error: computeTemperature function not defined\n");
    exit(1);
  }
  virtual double computedTdrho(double rho, double p) const {
    fprintf(stderr, "*** Error: computeTemperature function not defined\n");
    exit(1);
  }
  virtual double computedrhodT(double rho, double p) const {
    fprintf(stderr, "*** Error: computeTemperature function not defined\n");
    exit(1);
  }
  virtual double computedpdT(double rho, double p) const {
    fprintf(stderr, "*** Error: computeTemperature function not defined\n");
    exit(1);
  }
  virtual double computedrhodp(double rho, double p) const {
    fprintf(stderr, "*** Error: computeTemperature function not defined\n");
    exit(1);
  }
  virtual void computeTemperatureGradient(double *V, double *Tg) const {
    fprintf(stderr, "*** Error: computeTemperatureGradient function not defined\n");
    exit(1);
  }
  virtual void computeTemperatureHessian(double *V, double& Trr, double& Trp,
                                         double& Tpp) const {
    fprintf(stderr, "*** Error: computeTemperatureHessian function not defined\n");
    exit(1);
  }
  virtual void getV4FromTemperature(double *V, double T) const {
    fprintf(stderr, "*** Error: getV4FromTemperature function not defined\n");
    exit(1);
  }
  virtual void getdV4fromdVg(double *Vg, double *dVg, double& dV4) const {
    fprintf(stderr, "*** Error: getdV4FromdTemperature function not defined\n");
    exit(1);
  }
  virtual double computeRhoEnergy(double *V) const {
    fprintf(stderr, "*** Error: computeRhoEnergy function not defined\n");
    exit(1);
  }
  virtual double computeRhoEnergy(double rho, double p, double T, double vel) const {
    fprintf(stderr, "*** Error: computeRhoEnergy function not defined\n");
    exit(1);
  }
  virtual double computeRhoEpsilon(double *V) const { // this function computes the internal energy (=rho*e-0.5*rho*u^2)
    fprintf(stderr, "*** Error: computeRhoEpsilon function not defined\n");
    exit(1);
  }

  double getVelocitySquare(double *V) const {
    return V[1] * V[1] + V[2] * V[2] + V[3] * V[3];
  }
  double getWtVelocitySquare(double *V, double meshVel[3]) const {
    return (V[1] - meshVel[0]) * (V[1] - meshVel[0]) + (V[2] - meshVel[1]) * (V[2] - meshVel[1]) + (V[3] - meshVel[2]) * (V[3] - meshVel[2]);
  }
  double getVelocityNorm(double *V) const {
    return sqrt(getVelocitySquare(V));
  }
  double getWtVelocityNorm(double *V, double meshVel[3]) const {
    return sqrt(getWtVelocitySquare(V, meshVel));
  }
  double computeMachNumber(double *V) const {
    return getVelocityNorm(V) / computeSoundSpeed(V);
  }
  double computeWtMachNumber(double *V, double meshVel[3]) const {
    return getWtVelocityNorm(V, meshVel) / computeSoundSpeed(V);
  }
  virtual double computeSoundSpeed(double *V) const {
    fprintf(stderr, "*** Error: computeSoundSpeed function not defined\n");
    exit(1);
  }
  virtual double computeSoundSpeed(double density, double entropy) const {
    fprintf(stderr, "*** Error: computeSoundSpeed(entropy) function not defined\n");
    exit(1);
  }
  virtual double computeSoundSpeed2(double density, double pressure) const {
    fprintf(stderr, "*** Error: computeSoundSpeed2 function not defined\n");
    exit(1);
  }
  virtual double computeEntropy(double density, double pressure) const {
    fprintf(stderr, "*** Error: computeEntropy function not defined\n");
    exit(1);
  }
  virtual double computeDensity(double pressure, double temperature) const {
    fprintf(stderr, "*** Error: computeDensity function not defined\n");
    exit(1);
  }
  virtual double computeIsentropicPressure(double entropy, double density) const {
    fprintf(stderr, "*** Error: computeIsentropicEntropy function not defined\n");
    exit(1);
  }
  virtual double computeTotalPressure(double machr, double *V) const {
    fprintf(stderr, "*** Error: computeTotalPressure function not defined\n");
    exit(1);
  }

  double getTurbulentNuTilde(double *V) const {
    return V[5];
  }
  double getTurbulentKineticEnergy(double *V) const {
    return V[5];
  }
  double getTurbulentDissipationRate(double *V) const {
    return V[6];
  }

  Vec3D getDerivativeOfVelocity(double *dV) const {
    return Vec3D(dV[1], dV[2], dV[3]);
  }
  double getDerivativeOfVelocityNorm(double *V, double *dV) const {
    return (V[1] * dV[1] + V[2] * dV[2] + V[3] * dV[3]) / sqrt(V[1] * V[1] + V[2] * V[2] + V[3] * V[3]);
  }
  virtual double computeDerivativeOfTemperature(double *V, double *dV) const {
    return 0.0;
  }
  virtual void computeDerivativeOperatorsOfTemperature(double *V, double *dTdV) const {
    fprintf(stderr, "*** Error: computeDerivativeOperatorsOfTemperature function not defined\n");
    exit(-1);
  }
  virtual void computeJacobianTemperature(double *V, double *dTdU) const {
    fprintf(stderr, "*** Error: computeJacobianOfTemperature function not defined\n");
    exit(-1);
  }
  virtual double computeDerivativeOfMachNumber(double *V, double *dV, double dMach) const {
    return 0.0;
  }
  virtual double computeDerivativeOfSoundSpeed(double *V, double *dV, double dMach) const {
    return 0.0;
  }
  virtual double computeDerivativeOfTotalPressure(double machr, double dmachr, double *V, double *dV, double dmach) const {
    return 0.0;
  }
  virtual double getDerivativeOfPressureConstant() const {
    return 0.0;
  }

  virtual double specificHeatCstPressure() const {
    fprintf(stderr, "*** Error: specificHeatCstPressure function not defined\n");
    exit(1);
  }
  virtual double computePressureCoefficient(double *V, double pinfty,
                                            double mach, bool dimFlag) const {
    fprintf(stderr, "*** Error: computePressureCoefficient function not defined\n");
    exit(1);
  }

  //----- Equation of State Parameters -----//
  /* PERFECT/STIFFENED GAS EOS */
  virtual double getGamma() const {
    fprintf(stderr, "*** Error: getGamma function not defined\n");
    exit(1);
  }
  virtual double getGamma1() const {
    fprintf(stderr, "*** Error: getGamma1 function not defined\n");
    exit(1);
  }
  virtual double getPressureConstant() const {
    fprintf(stderr, "*** Error: getPressureConstant function not defined\n");
    exit(1);
  }
  /* TAIT EOS */
  virtual double getCv() const {
    fprintf(stderr, "*** Error: getCv function not defined\n");
    exit(1);
  }
  virtual double getAlphaWater() const {
    fprintf(stderr, "*** Error: getAlphaWater function not defined\n");
    exit(1);
  }
  virtual double getBetaWater() const {
    fprintf(stderr, "*** Error: getBetaWater function not defined\n");
    exit(1);
  }
  virtual double getPrefWater() const {
    fprintf(stderr, "*** Error: getPrefWater function not defined\n");
    exit(1);
  }
  /* JWL EOS */
  virtual double getOmega() const {
    fprintf(stderr, "*** Error: getOmega function not defined\n");
    exit(1);
  }
  virtual double getOmegap1() const {
    fprintf(stderr, "*** Error: getOmegap1 function not defined\n");
    exit(1);
  }
  virtual double getInvOmega() const {
    fprintf(stderr, "*** Error: getInvOmega function not defined\n");
    exit(1);
  }
  virtual double getInvOmegap1() const {
    fprintf(stderr, "*** Error: getInvOmegap1 function not defined\n");
    exit(1);
  }
  virtual double getA1() const {
    fprintf(stderr, "*** Error: getA1 function not defined\n");
    exit(1);
  }
  virtual double getA2() const {
    fprintf(stderr, "*** Error: getA2 function not defined\n");
    exit(1);
  }
  virtual double getR1() const {
    fprintf(stderr, "*** Error: getR1 function not defined\n");
    exit(1);
  }
  virtual double getR2() const {
    fprintf(stderr, "*** Error: getR2 function not defined\n");
    exit(1);
  }
  virtual double getRhoref() const {
    fprintf(stderr, "*** Error: getRhoref function not defined\n");
    exit(1);
  }
  virtual double getR1r() const {
    fprintf(stderr, "*** Error: getR1r function not defined\n");
    exit(1);
  }
  virtual double getR2r() const {
    fprintf(stderr, "*** Error: getR2r function not defined\n");
    exit(1);
  }

  virtual bool isBurnable() const {
    return false;
  }

  //----- EOS related functions -----//
  virtual double computeExponentials(double density) const {
    fprintf(stderr, "*** Error: computeExponentials function not defined\n");
    exit(1);
  }
  virtual double computeDerivativeOfExponentials(double density) const {
    fprintf(stderr, "*** Error: computeDerivativeOfExponentials function not defined\n");
    exit(1);
  }
  virtual double computeExponentials2(double density) const {
    fprintf(stderr, "*** Error: computeExponentials2 function not defined\n");
    exit(1);
  }
  virtual double computeDerivativeOfExponentials2(double density) const {
    fprintf(stderr, "*** Error: computeDerivativeOfExponentials2 function not defined\n");
    exit(1);
  }
  virtual double computeFrho(double *V) const {
    fprintf(stderr, "*** Error: computeFrho function not defined\n");
    exit(1);
  }
  virtual double computeFrho(double density) const {
    fprintf(stderr, "*** Error: computeFrho function not defined\n");
    exit(1);
  }
  virtual double computeFrhop(double *V) const {
    fprintf(stderr, "*** Error: computeFrhop function not defined\n");
    exit(1);
  }
  virtual double computeFrhop(double density) const {
    fprintf(stderr, "*** Error: computeFrhop function not defined\n");
    exit(1);
  }

};

//------------------------------------------------------------------------------

inline
void VarFcnBase::multiplyBydVdU(double *V, double *vec, double *res) {
  double dVdU[25];
  computedVdU(V, dVdU);
  res[0] = dVdU[0] * vec[0];
  res[1] = dVdU[5] * vec[0] + dVdU[6] * vec[1];
  res[2] = dVdU[10] * vec[0] + dVdU[12] * vec[2];
  res[3] = dVdU[15] * vec[0] + dVdU[18] * vec[3];
  res[4] = dVdU[20] * vec[0] + dVdU[21] * vec[1] + dVdU[22] * vec[2] + dVdU[23] * vec[3] + dVdU[24] * vec[4];
}

//------------------------------------------------------------------------------

inline
void VarFcnBase::multiplyBydVdU(double *V, std::complex<double> *vec, std::complex<double> *res) {
  double dVdU[25];
  computedVdU(V, dVdU);
  res[0] = dVdU[0] * vec[0];
  res[1] = dVdU[5] * vec[0] + dVdU[6] * vec[1];
  res[2] = dVdU[10] * vec[0] + dVdU[12] * vec[2];
  res[3] = dVdU[15] * vec[0] + dVdU[18] * vec[3];
  res[4] = dVdU[20] * vec[0] + dVdU[21] * vec[1] + dVdU[22] * vec[2] + dVdU[23] * vec[3] + dVdU[24] * vec[4];
}

//------------------------------------------------------------------------------

inline
void VarFcnBase::postMultiplyBydUdV(double *V, double *mat, double *res) {
  double dUdV[25];
  computedUdV(V, dUdV);
  res[0] = mat[0] * dUdV[0] + mat[1] * dUdV[5] + mat[2] * dUdV[10] + mat[3] * dUdV[15] + mat[4] * dUdV[20];
  res[1] = mat[1] * dUdV[6] + mat[4] * dUdV[21];
  res[2] = mat[2] * dUdV[12] + mat[4] * dUdV[22];
  res[3] = mat[3] * dUdV[18] + mat[4] * dUdV[23];
  res[4] = mat[4] * dUdV[24];
  res[5] = mat[5] * dUdV[0] + mat[6] * dUdV[5] + mat[7] * dUdV[10] + mat[8] * dUdV[15] + mat[9] * dUdV[20];
  res[6] = mat[6] * dUdV[6] + mat[9] * dUdV[21];
  res[7] = mat[7] * dUdV[12] + mat[9] * dUdV[22];
  res[8] = mat[8] * dUdV[18] + mat[9] * dUdV[23];
  res[9] = mat[9] * dUdV[24];
  res[10] = mat[10] * dUdV[0] + mat[11] * dUdV[5] + mat[12] * dUdV[10] + mat[13] * dUdV[15] + mat[14] * dUdV[20];
  res[11] = mat[11] * dUdV[6] + mat[14] * dUdV[21];
  res[12] = mat[12] * dUdV[12] + mat[14] * dUdV[22];
  res[13] = mat[13] * dUdV[18] + mat[14] * dUdV[23];
  res[14] = mat[14] * dUdV[24];
  res[15] = mat[15] * dUdV[0] + mat[16] * dUdV[5] + mat[17] * dUdV[10] + mat[18] * dUdV[15] + mat[19] * dUdV[20];
  res[16] = mat[16] * dUdV[6] + mat[19] * dUdV[21];
  res[17] = mat[17] * dUdV[12] + mat[19] * dUdV[22];
  res[18] = mat[18] * dUdV[18] + mat[19] * dUdV[23];
  res[19] = mat[19] * dUdV[24];
  res[20] = mat[20] * dUdV[0] + mat[21] * dUdV[5] + mat[22] * dUdV[10] + mat[23] * dUdV[15] + mat[24] * dUdV[20];
  res[21] = mat[21] * dUdV[6] + mat[24] * dUdV[21];
  res[22] = mat[22] * dUdV[12] + mat[24] * dUdV[22];
  res[23] = mat[23] * dUdV[18] + mat[24] * dUdV[23];
  res[24] = mat[24] * dUdV[24];
}

//------------------------------------------------------------------------------

inline
void VarFcnBase::postMultiplyBydVdU(double *V, double *mat, double *res) {
  double dVdU[25];
  computedVdU(V, dVdU);
  res[0] = mat[0] * dVdU[0] + mat[1] * dVdU[5] + mat[2] * dVdU[10] + mat[3] * dVdU[15] + mat[4] * dVdU[20];
  res[1] = mat[1] * dVdU[6] + mat[4] * dVdU[21];
  res[2] = mat[2] * dVdU[12] + mat[4] * dVdU[22];
  res[3] = mat[3] * dVdU[18] + mat[4] * dVdU[23];
  res[4] = mat[4] * dVdU[24];
  res[5] = mat[5] * dVdU[0] + mat[6] * dVdU[5] + mat[7] * dVdU[10] + mat[8] * dVdU[15] + mat[9] * dVdU[20];
  res[6] = mat[6] * dVdU[6] + mat[9] * dVdU[21];
  res[7] = mat[7] * dVdU[12] + mat[9] * dVdU[22];
  res[8] = mat[8] * dVdU[18] + mat[9] * dVdU[23];
  res[9] = mat[9] * dVdU[24];
  res[10] = mat[10] * dVdU[0] + mat[11] * dVdU[5] + mat[12] * dVdU[10] + mat[13] * dVdU[15] + mat[14] * dVdU[20];
  res[11] = mat[11] * dVdU[6] + mat[14] * dVdU[21];
  res[12] = mat[12] * dVdU[12] + mat[14] * dVdU[22];
  res[13] = mat[13] * dVdU[18] + mat[14] * dVdU[23];
  res[14] = mat[14] * dVdU[24];
  res[15] = mat[15] * dVdU[0] + mat[16] * dVdU[5] + mat[17] * dVdU[10] + mat[18] * dVdU[15] + mat[19] * dVdU[20];
  res[16] = mat[16] * dVdU[6] + mat[19] * dVdU[21];
  res[17] = mat[17] * dVdU[12] + mat[19] * dVdU[22];
  res[18] = mat[18] * dVdU[18] + mat[19] * dVdU[23];
  res[19] = mat[19] * dVdU[24];
  res[20] = mat[20] * dVdU[0] + mat[21] * dVdU[5] + mat[22] * dVdU[10] + mat[23] * dVdU[15] + mat[24] * dVdU[20];
  res[21] = mat[21] * dVdU[6] + mat[24] * dVdU[21];
  res[22] = mat[22] * dVdU[12] + mat[24] * dVdU[22];
  res[23] = mat[23] * dVdU[18] + mat[24] * dVdU[23];
  res[24] = mat[24] * dVdU[24];
}

//------------------------------------------------------------------------------

inline
void VarFcnBase::postMultiplyBydVdU(double *V, double *mat, std::complex<double> *res) {
  double dVdU[25];
  computedVdU(V, dVdU);
  res[0] = mat[0] * dVdU[0] + mat[1] * dVdU[5] + mat[2] * dVdU[10] + mat[3] * dVdU[15] + mat[4] * dVdU[20];
  res[1] = mat[1] * dVdU[6] + mat[4] * dVdU[21];
  res[2] = mat[2] * dVdU[12] + mat[4] * dVdU[22];
  res[3] = mat[3] * dVdU[18] + mat[4] * dVdU[23];
  res[4] = mat[4] * dVdU[24];
  res[5] = mat[5] * dVdU[0] + mat[6] * dVdU[5] + mat[7] * dVdU[10] + mat[8] * dVdU[15] + mat[9] * dVdU[20];
  res[6] = mat[6] * dVdU[6] + mat[9] * dVdU[21];
  res[7] = mat[7] * dVdU[12] + mat[9] * dVdU[22];
  res[8] = mat[8] * dVdU[18] + mat[9] * dVdU[23];
  res[9] = mat[9] * dVdU[24];
  res[10] = mat[10] * dVdU[0] + mat[11] * dVdU[5] + mat[12] * dVdU[10] + mat[13] * dVdU[15] + mat[14] * dVdU[20];
  res[11] = mat[11] * dVdU[6] + mat[14] * dVdU[21];
  res[12] = mat[12] * dVdU[12] + mat[14] * dVdU[22];
  res[13] = mat[13] * dVdU[18] + mat[14] * dVdU[23];
  res[14] = mat[14] * dVdU[24];
  res[15] = mat[15] * dVdU[0] + mat[16] * dVdU[5] + mat[17] * dVdU[10] + mat[18] * dVdU[15] + mat[19] * dVdU[20];
  res[16] = mat[16] * dVdU[6] + mat[19] * dVdU[21];
  res[17] = mat[17] * dVdU[12] + mat[19] * dVdU[22];
  res[18] = mat[18] * dVdU[18] + mat[19] * dVdU[23];
  res[19] = mat[19] * dVdU[24];
  res[20] = mat[20] * dVdU[0] + mat[21] * dVdU[5] + mat[22] * dVdU[10] + mat[23] * dVdU[15] + mat[24] * dVdU[20];
  res[21] = mat[21] * dVdU[6] + mat[24] * dVdU[21];
  res[22] = mat[22] * dVdU[12] + mat[24] * dVdU[22];
  res[23] = mat[23] * dVdU[18] + mat[24] * dVdU[23];
  res[24] = mat[24] * dVdU[24];
}

//------------------------------------------------------------------------------

inline
void VarFcnBase::postMultiplyBydVdU(double *V, double *mat, float *res) {
  double dVdU[25];
  computedVdU(V, dVdU);
  res[0] = mat[0] * dVdU[0] + mat[1] * dVdU[5] + mat[2] * dVdU[10] + mat[3] * dVdU[15] + mat[4] * dVdU[20];
  res[1] = mat[1] * dVdU[6] + mat[4] * dVdU[21];
  res[2] = mat[2] * dVdU[12] + mat[4] * dVdU[22];
  res[3] = mat[3] * dVdU[18] + mat[4] * dVdU[23];
  res[4] = mat[4] * dVdU[24];
  res[5] = mat[5] * dVdU[0] + mat[6] * dVdU[5] + mat[7] * dVdU[10] + mat[8] * dVdU[15] + mat[9] * dVdU[20];
  res[6] = mat[6] * dVdU[6] + mat[9] * dVdU[21];
  res[7] = mat[7] * dVdU[12] + mat[9] * dVdU[22];
  res[8] = mat[8] * dVdU[18] + mat[9] * dVdU[23];
  res[9] = mat[9] * dVdU[24];
  res[10] = mat[10] * dVdU[0] + mat[11] * dVdU[5] + mat[12] * dVdU[10] + mat[13] * dVdU[15] + mat[14] * dVdU[20];
  res[11] = mat[11] * dVdU[6] + mat[14] * dVdU[21];
  res[12] = mat[12] * dVdU[12] + mat[14] * dVdU[22];
  res[13] = mat[13] * dVdU[18] + mat[14] * dVdU[23];
  res[14] = mat[14] * dVdU[24];
  res[15] = mat[15] * dVdU[0] + mat[16] * dVdU[5] + mat[17] * dVdU[10] + mat[18] * dVdU[15] + mat[19] * dVdU[20];
  res[16] = mat[16] * dVdU[6] + mat[19] * dVdU[21];
  res[17] = mat[17] * dVdU[12] + mat[19] * dVdU[22];
  res[18] = mat[18] * dVdU[18] + mat[19] * dVdU[23];
  res[19] = mat[19] * dVdU[24];
  res[20] = mat[20] * dVdU[0] + mat[21] * dVdU[5] + mat[22] * dVdU[10] + mat[23] * dVdU[15] + mat[24] * dVdU[20];
  res[21] = mat[21] * dVdU[6] + mat[24] * dVdU[21];
  res[22] = mat[22] * dVdU[12] + mat[24] * dVdU[22];
  res[23] = mat[23] * dVdU[18] + mat[24] * dVdU[23];
  res[24] = mat[24] * dVdU[24];
}

//------------------------------------------------------------------------------

inline
void VarFcnBase::preMultiplyBydUdV(double *V, double *mat, double *res) {
  double dUdV[25];
  computedUdV(V, dUdV);
  res[0] = dUdV[0] * mat[0];
  res[1] = dUdV[0] * mat[1];
  res[2] = dUdV[0] * mat[2];
  res[3] = dUdV[0] * mat[3];
  res[4] = dUdV[0] * mat[4];
  res[5] = dUdV[5] * mat[0] + dUdV[6] * mat[5];
  res[6] = dUdV[5] * mat[1] + dUdV[6] * mat[6];
  res[7] = dUdV[5] * mat[2] + dUdV[6] * mat[7];
  res[8] = dUdV[5] * mat[3] + dUdV[6] * mat[8];
  res[9] = dUdV[5] * mat[4] + dUdV[6] * mat[9];
  res[10] = dUdV[10] * mat[0] + dUdV[12] * mat[10];
  res[11] = dUdV[10] * mat[1] + dUdV[12] * mat[11];
  res[12] = dUdV[10] * mat[2] + dUdV[12] * mat[12];
  res[13] = dUdV[10] * mat[3] + dUdV[12] * mat[13];
  res[14] = dUdV[10] * mat[4] + dUdV[12] * mat[14];
  res[15] = dUdV[15] * mat[0] + dUdV[18] * mat[15];
  res[16] = dUdV[15] * mat[1] + dUdV[18] * mat[16];
  res[17] = dUdV[15] * mat[2] + dUdV[18] * mat[17];
  res[18] = dUdV[15] * mat[3] + dUdV[18] * mat[18];
  res[19] = dUdV[15] * mat[4] + dUdV[18] * mat[19];
  res[20] = dUdV[20] * mat[0] + dUdV[21] * mat[5] + dUdV[22] * mat[10] + dUdV[23] * mat[15] + dUdV[24] * mat[20];
  res[21] = dUdV[20] * mat[1] + dUdV[21] * mat[6] + dUdV[22] * mat[11] + dUdV[23] * mat[16] + dUdV[24] * mat[21];
  res[22] = dUdV[20] * mat[2] + dUdV[21] * mat[7] + dUdV[22] * mat[12] + dUdV[23] * mat[17] + dUdV[24] * mat[22];
  res[23] = dUdV[20] * mat[3] + dUdV[21] * mat[8] + dUdV[22] * mat[13] + dUdV[23] * mat[18] + dUdV[24] * mat[23];
  res[24] = dUdV[20] * mat[4] + dUdV[21] * mat[9] + dUdV[22] * mat[14] + dUdV[23] * mat[19] + dUdV[24] * mat[24];
}

//------------------------------------------------------------------------------

#endif

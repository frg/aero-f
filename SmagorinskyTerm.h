#ifndef _SMAGORINSKY_TERM_H_
#define _SMAGORINSKY_TERM_H_

#include <LESTerm.h>
#include <IoData.h>

#include <limits>

//------------------------------------------------------------------------------

class SmagorinskyTerm : public LESTerm {

  static constexpr double fourth = 1.0 / 4.0;

  double cs;

 public:
  SmagorinskyTerm(IoData&);
  ~SmagorinskyTerm() {}

  double computeEddyViscosity(double *[4], double, double[3][3]);

  double computeDerivativeOfEddyViscosity(double *[4], double *[4], double, double,
                                          double[3][3], double[3][3]);

  void computeJacobianEddyViscosity(double *[4], double, double[3][3],
                                    double[4][3], double[4][5]);
  
  double computeTKE(double, double);

};

//------------------------------------------------------------------------------

inline
SmagorinskyTerm::SmagorinskyTerm(IoData& iod) : LESTerm(iod) {
  cs = iod.eqs.tc.les.sma.c_s;
}

//------------------------------------------------------------------------------

inline
double SmagorinskyTerm::computeEddyViscosity(double *V[4], double Delta, double duidxj[3][3]) {
  double rho = fourth * (V[0][0] + V[1][0] + V[2][0] + V[3][0]);
  double S[3][3];
  S[0][0] = duidxj[0][0];
  S[1][1] = duidxj[1][1];
  S[2][2] = duidxj[2][2];
  S[0][1] = 0.5 * (duidxj[0][1] + duidxj[1][0]);
  S[0][2] = 0.5 * (duidxj[0][2] + duidxj[2][0]);
  S[1][2] = 0.5 * (duidxj[1][2] + duidxj[2][1]);
  S[1][0] = S[0][1];
  S[2][0] = S[0][2];
  S[2][1] = S[1][2];
  double S2 = (S[0][0] * S[0][0] + S[0][1] * S[0][1] + S[0][2] * S[0][2] +
               S[1][0] * S[1][0] + S[1][1] * S[1][1] + S[1][2] * S[1][2] +
               S[2][0] * S[2][0] + S[2][1] * S[2][1] + S[2][2] * S[2][2]);
  return rho * (cs * Delta) * (cs * Delta) * sqrt(2.0 * S2);
}

//------------------------------------------------------------------------------

inline
double SmagorinskyTerm::computeDerivativeOfEddyViscosity(double *V[4], double *dV[4], double Delta,
                                                         double dDelta, double duidxj[3][3],
                                                         double dduidxj[3][3]) {
  std::cerr << "*** Warning: SmagorinskyTerm::computeDerivativeOfEddyViscosity is not implemented\n";
  return 0;
}

//------------------------------------------------------------------------------

inline
void SmagorinskyTerm::computeJacobianEddyViscosity(double *V[4], double Delta, double duidxj[3][3],
                                                   double dp1dxj[4][3], double dmut[4][5]) {
  double rho = fourth * (V[0][0] + V[1][0] + V[2][0] + V[3][0]);
  double S[3][3];
  S[0][0] = duidxj[0][0];
  S[1][1] = duidxj[1][1];
  S[2][2] = duidxj[2][2];
  S[0][1] = 0.5 * (duidxj[0][1] + duidxj[1][0]);
  S[0][2] = 0.5 * (duidxj[0][2] + duidxj[2][0]);
  S[1][2] = 0.5 * (duidxj[1][2] + duidxj[2][1]);
  S[1][0] = S[0][1];
  S[2][0] = S[0][2];
  S[2][1] = S[1][2];
  double S2 = (S[0][0] * S[0][0] + S[0][1] * S[0][1] + S[0][2] * S[0][2] +
               S[1][0] * S[1][0] + S[1][1] * S[1][1] + S[1][2] * S[1][2] +
               S[2][0] * S[2][0] + S[2][1] * S[2][1] + S[2][2] * S[2][2]);
  double cdcd = (cs * Delta) * (cs * Delta);
  double s2s2 = sqrt(2.0 * S2);
  if(s2s2 < std::numeric_limits<double>::epsilon()) {
    for(int k = 0; k < 4; ++k) {
      for(int i = 0; i < 5; ++i) {
        dmut[k][i] = 0.0;
      }
    }
  }
  else {
    double toto1 = fourth * cdcd * s2s2;
    double toto2 = 2 * rho * cdcd / s2s2;
    // note: dmut is the derivative w.r.t the conservative state variables
    for(int k = 0; k < 4; ++k) {
      dmut[k][4] = 0.0;
      dmut[k][3] = toto2 * (dp1dxj[k][0] * S[2][0] + dp1dxj[k][1] * S[2][1] + dp1dxj[k][2] * S[2][2]) / V[k][0];
      dmut[k][2] = toto2 * (dp1dxj[k][0] * S[1][0] + dp1dxj[k][1] * S[1][1] + dp1dxj[k][2] * S[1][2]) / V[k][0];
      dmut[k][1] = toto2 * (dp1dxj[k][0] * S[0][0] + dp1dxj[k][1] * S[0][1] + dp1dxj[k][2] * S[0][2]) / V[k][0];
      dmut[k][0] = toto1 - (dmut[k][1] * V[k][1] + dmut[k][2] * V[k][2] + dmut[k][3] * V[k][3]);
    }
  }
}
//------------------------------------------------------------------------------

inline
double SmagorinskyTerm::computeTKE(double nut, double Delta) {
  double uTKE = nut / (Delta * Delta * cs * cs);
  double k = (1.0 / 2.0) * uTKE * uTKE;
  return k;
}

//------------------------------------------------------------------------------

#endif

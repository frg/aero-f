#include <ReinitializeDistanceToWall.h>

#include <DistGeoState.h>
#include <Domain.h>
#include <GeometricQuery.h>
#include <SubDomain.h>
#include <LevelSet/LevelSetStructure.h>

#include <algorithm>
#include <iomanip>
#include <limits>

//------------------------------------------------------------------------------

ReinitializeDistanceToWall::ReinitializeDistanceToWall(IoData& ioData, Domain& domain, int _faceID, bool _updateGeoState)
  : iod(ioData), dom(domain), d2wall(domain.getNodeDistInfo()),
    tag(domain.getNodeDistInfo()), sortedNodes(domain.getNodeDistInfo()),
    faceID(_faceID), updateGeoState(_updateGeoState), dd2wall_doppnode(nullptr),
    triangleSet(nullptr) {
  nSortedNodes     = new int[domain.getNumLocSub()];
  firstCheckedNode = new int[domain.getNumLocSub()];
  if(iod.eqs.tc.tm.d2wall.frequencyadaptation == WallDistanceMethodData::OFF)
    usePredictors = false;
  else {
    usePredictors = true;
    predictorTime.resize(3, -1.); // initialize to -1
    d2wnm1 = new DistVec<double>(domain.getNodeDistInfo());
    d2wnm2 = new DistVec<double>(domain.getNodeDistInfo());
    sensitivSA = new DistVec<double>(domain.getNodeDistInfo());
    predictorTag = new DistVec<int>(domain.getNodeDistInfo());
  }
  if(iod.eqs.tc.tm.d2wall.computeinactive == WallDistanceMethodData::NO) {
    computeInactive = false;
  }
  else {
    computeInactive = true;
  }
  if(iod.eqs.tc.tm.d2wall.type == WallDistanceMethodData::CLOSESTPOINT) {
    triangleSet = new TriangleSet(faceID);
  }
  if(iod.problem.type[ProblemData::SENSITIVITY_ANALYSIS] && iod.sa.scFlag != SensitivityAnalysisData::FINITEDIFFERENCE) {
    dd2wall = new DistVec<double>(domain.getNodeDistInfo());
    *dd2wall = 0.0;
  }
  else {
    dd2wall_doppnode = nullptr;
    dd2wall = nullptr;
  }
  // copied from distGeoState.C, TODO, store bb in distgeostate and re-use here
  double bbMin[3] = {std::numeric_limits<double>::infinity(),
                     std::numeric_limits<double>::infinity(),
                     std::numeric_limits<double>::infinity()};
  double bbMax[3] = {-std::numeric_limits<double>::infinity(),
                     -std::numeric_limits<double>::infinity(),
                     -std::numeric_limits<double>::infinity()};
  #pragma omp parallel for
  for(int iSub = 0; iSub < domain.getNumLocSub(); ++iSub) {
    NodeSet& nodes = domain.getSubDomain()[iSub]->getNodes(); // TODO consider mesh motion
    for(int i = 0; i < nodes.size(); ++i) {
      for(int j = 0; j < 3; ++j) {
        bbMin[j] = std::min(bbMin[j], nodes[i][j]);
        bbMax[j] = std::max(bbMax[j], nodes[i][j]);
      }
    }
  }
  dom.getCommunicator()->globalMin(3, bbMin);
  dom.getCommunicator()->globalMax(3, bbMax);
  d2wallmax = 2*sqrt(std::pow(bbMax[0] - bbMin[0],2) + std::pow(bbMax[1] - bbMin[1],2) + std::pow(bbMax[2] - bbMin[2],2));
}

//------------------------------------------------------------------------------

ReinitializeDistanceToWall::~ReinitializeDistanceToWall() {
  delete[] nSortedNodes;
  delete[] firstCheckedNode;
  if(usePredictors) {
    delete d2wnm1;
    delete d2wnm2;
    delete sensitivSA;
    delete predictorTag;
  }
  if(dd2wall_doppnode) delete dd2wall_doppnode;
  if(dd2wall) delete dd2wall;
  if(triangleSet) delete triangleSet;
}

//------------------------------------------------------------------------------

void ReinitializeDistanceToWall::resize(NodeData *nodeData) {
  tag.resize(dom.getNodeDistInfo());
  sortedNodes.resize(dom.getNodeDistInfo());
  if(usePredictors) {
    d2wall.resize(dom.getNodeDistInfo(), nodeData);
    d2wnm1->resize(dom.getNodeDistInfo(), nodeData);
    d2wnm2->resize(dom.getNodeDistInfo(), nodeData);
    predictorTag->resize(dom.getNodeDistInfo());
    sensitivSA->resize(dom.getNodeDistInfo());
  }
  else {
    d2wall.resize(dom.getNodeDistInfo());
  }
}

//------------------------------------------------------------------------------

int ReinitializeDistanceToWall::ComputeWallFunction(DistLevelSetStructure& LSS,
                                                    DistGeoState& distGeoState,
                                                    const double t, bool forceUpdate) {
  int update;
  if(!usePredictors || forceUpdate)
    update = 1;
  else
    update = UpdateDistanceError(LSS, distGeoState, t);
  if(update > 0) {
    if(usePredictors && t != predictorTime[0]) {
      // store with previous exact updates
      predictorTime[2] = predictorTime[1];
      predictorTime[1] = predictorTime[0];
      predictorTime[0] = t;
      (*d2wnm2) = (*d2wnm1);
      (*d2wnm1) = d2wall;
      nSkip = 0;
    }
    // compute distance function
    switch(iod.eqs.tc.tm.d2wall.type) {
      case(WallDistanceMethodData::NONITERATIVE) :
        PseudoFastMarchingMethod(LSS, distGeoState.getPositionVector());
        break;
      case(WallDistanceMethodData::ITERATIVE) :
        IterativeMethodUpdate(LSS, distGeoState.getPositionVector());
        break;
      case(WallDistanceMethodData::CLOSESTPOINT) :
        ClosestPointMethod(LSS, distGeoState.getPositionVector());
        break;
      default :
        fprintf(stderr, "*** Error: Unknown wall distance method\n");
        exit(1);
        break;
    }
    if(!computeInactive) {
      #pragma omp parallel for
      for(int iSub = 0; iSub < dom.getNumLocSub(); ++iSub) {
        Vec<double>& locd2wall = d2wall(iSub);
        LevelSetStructure& locLSS = LSS(iSub);
        for(int k = 0; k < locd2wall.size(); k++) {
          if(!locLSS.isActive(0.0, k)) locd2wall[k] = 0;
        }
      }
    }
    else if(iod.eqs.tc.tm.d2wall.inactivesign == WallDistanceMethodData::NEGATIVE) {
      #pragma omp parallel for
      for(int iSub = 0; iSub < dom.getNumLocSub(); ++iSub) {
        Vec<double>& locd2wall = d2wall(iSub);
        LevelSetStructure& locLSS = LSS(iSub);
        for(int k = 0; k < locd2wall.size(); k++) {
          if(!locLSS.isActive(0.0, k)) locd2wall[k] *= -1;
        }
      }
    }
    if(updateGeoState) distGeoState.updateDistanceToWall(d2wall);
  }
  else { // usePredictors == true
    nSkip++;
  }
  return update;
}

//------------------------------------------------------------------------------

void ReinitializeDistanceToWall::ComputeDerivativeOperatorsOfWallFunction(DistLevelSetStructure& LSS,
                                                                          DistGeoState& distGeoState) {
  if(!dd2wall_doppnode) dd2wall_doppnode = new DistSVec<std::pair<int, double>, 3>(dom.getNodeDistInfo());
  *dd2wall_doppnode = std::pair<int,double>(-1, 0.0);
  if(iod.eqs.tc.tm.d2wall.type == WallDistanceMethodData::NONITERATIVE) {
    dom.getCommunicator()->fprintf(stderr, "*** Warning: Analytical derivatives for non-iterative wall distance method not implemented\n");
  }
  bool hybridNodes = (iod.embed.definitionActiveInactive == EmbeddedFrameworkData::CONTROLVOLUME);
  tag = -1;
  for(int iSub = 0; iSub < dom.getNumLocSub(); ++iSub) {
    dom.getSubDomain()[iSub]->populatedd2WallOperators(tag(iSub), distGeoState.getPositionVector()(iSub), d2wall(iSub),
                                                       LSS(iSub), computeInactive, faceID, d2wallmax, &(*dd2wall_doppnode)(iSub), hybridNodes);
  }
}

//------------------------------------------------------------------------------

void ReinitializeDistanceToWall::ComputeDerivativeOfWallFunction(DistLevelSetStructure& LSS,
                                                                 DistGeoState& distGeoState) {
  if(iod.eqs.tc.tm.d2wall.sensitivities == WallDistanceMethodData::FINITEDIFFERENCE) {
    int findStatus = 2;
    bool fill_scope = true;
    bool retry = false;
    DistSVec<double, 3>& Xc = distGeoState.getPositionVector();
    double eps = 1e-7;
    LSS.perturbStructureWithdXs(eps);
    LSS.recompute(Xc, 0.0, 0.0, 0.0, findStatus, retry, fill_scope);
    ComputeWallFunction(LSS, distGeoState, 0.0, false);
    DistVec<double> d2wall_p(d2wall);
    LSS.perturbStructureWithdXs(0.0);
    LSS.recompute(Xc, 0.0, 0.0, 0.0, findStatus, retry, fill_scope);
    ComputeWallFunction(LSS, distGeoState, 0.0, false);
    *dd2wall = (d2wall_p - d2wall) / eps;
  }
  else {
    // compute distance function
    if(!dd2wall_doppnode) ComputeDerivativeOperatorsOfWallFunction(LSS, distGeoState);
    if(iod.eqs.tc.tm.d2wall.type == WallDistanceMethodData::NONITERATIVE) {
      dom.getCommunicator()->fprintf(stderr, "*** Warning: Analytical derivatives for non-iterative wall distance method not implemented\n");
    }
    // assumes a converged distance field is already calculated
    IterativeMethodUpdate(LSS, distGeoState.getPositionVector(), MarchingMethodObjective::DD2WALL);
  }
  if(updateGeoState) distGeoState.getdDistanceToWall() = *dd2wall;
}

//------------------------------------------------------------------------------

void ReinitializeDistanceToWall::PseudoFastMarchingMethod(DistLevelSetStructure& LSS, DistSVec<double, 3>& X) {
  // the following is an adaptation of the fast marching method
  d2wall = d2wallmax;
  tag = -1;
  int nSub = dom.getNumLocSub();
  int isDone = 0;
  int level = 1; // Level 0 (inactive nodes) and 1 (embedded surface neighbors)
  bool hybridNodes = (iod.embed.definitionActiveInactive == EmbeddedFrameworkData::CONTROLVOLUME);
  while(isDone == 0) { // tag every level
    dom.pseudoFastMarchingMethod(tag, X, d2wall, level, sortedNodes,
                                 nSortedNodes, firstCheckedNode, LSS,
                                 computeInactive, faceID, d2wallmax, nullptr, hybridNodes);
    isDone = 1;
    for(int iSub = 0; iSub < nSub; ++iSub) {
      if(nSortedNodes[iSub] != firstCheckedNode[iSub]) {
        isDone = 0;
        break;
      }
    }
    dom.getCommunicator()->globalMin(1, &isDone);
    ++level;
  }
}

//------------------------------------------------------------------------------

void ReinitializeDistanceToWall::ClosestPointMethod(DistLevelSetStructure& LSS, DistSVec<double, 3>& X) {
  int numStructElems = LSS.getNumStructElems();
  int (*stElem)[3] = LSS.getStructElems();
#ifdef CONSISTENT_WALL_DISTANCE
  Vec<Vec3D>& Xstruct = LSS.getStructPosition();
#else
  Vec<Vec3D>& Xstruct = LSS.getStructPosition_np1();
#endif
  int *faceID = LSS.getFaceID();
  triangleSet->updateAxisAlignedBoundingBoxTree(numStructElems, stElem, Xstruct, faceID);
  #pragma omp parallel for
  for(int iSub = 0; iSub < dom.getNumLocSub(); ++iSub) {
    Vec<double>& ld2wall = d2wall(iSub);
    SVec<double, 3>& x = X(iSub);
    for(int i = 0; i < ld2wall.size(); ++i) {
      Eigen::Vector3d P(x[i][0], x[i][1], x[i][2]);
      ld2wall[i] = triangleSet->distance(P);
    }
  }
}

//------------------------------------------------------------------------------

void ReinitializeDistanceToWall::IterativeMethodUpdate(DistLevelSetStructure& LSS, DistSVec<double, 3>& X, MarchingMethodObjective objective) {
  int nSub = dom.getNumLocSub();
  DistVec<double> resnm1(dom.getNodeDistInfo());
  double resn;
  // initialization (levels 0, 1)
  if(objective == MarchingMethodObjective::D2WALL) d2wall = d2wallmax;
  tag = -1;
  int level = 1, it = 0, checkConverged = 0;
  bool hybridNodes = (iod.embed.definitionActiveInactive == EmbeddedFrameworkData::CONTROLVOLUME);
  switch(objective) {
    case MarchingMethodObjective::DD2WALL:
      dom.pseudoFastMarchingMethod(tag, X, d2wall, level, sortedNodes,
                                   nSortedNodes, firstCheckedNode, LSS,
                                   computeInactive, faceID, d2wallmax, dd2wall, hybridNodes);
      break;
    default:
      dom.pseudoFastMarchingMethod(tag, X, d2wall, level, sortedNodes,
                                   nSortedNodes, firstCheckedNode, LSS,
                                   computeInactive, faceID, d2wallmax, nullptr, hybridNodes);
  }
  // store inactive and embedded surface neighbors for fixing (tag = 0)
  bool *visited = new bool[nSub];
  DistVec<int> tag0(dom.getNodeDistInfo());
  tag0 = -1;
  for(int iSub = 0; iSub < nSub; ++iSub) {
    if(nSortedNodes[iSub] > 0) {
      visited[iSub] = true;
      for(int i = 0; i < d2wall(iSub).size(); i++) {
        if(tag(iSub)[i] > -1) tag0(iSub)[i] = 0;
      }
    }
    else {
      visited[iSub] = false;
    }
  }
  while(it < iod.eqs.tc.tm.d2wall.maxIts) {
    // sweep subdomains
    if(objective == MarchingMethodObjective::DD2WALL) resnm1 = *dd2wall;
    else resnm1 = d2wall;

    for(int iSub = 0; iSub < nSub; ++iSub) {
      level = 1;  // active list already populated in comm, tags reset
      while(firstCheckedNode[iSub] < nSortedNodes[iSub]) {
        ++level;
        switch(objective) {
          case MarchingMethodObjective::DD2WALL:
            (dom.getSubDomain())[iSub]->pseudoFastMarchingMethod(tag(iSub), X(iSub), d2wall(iSub), level,
                                                                 sortedNodes(iSub), nSortedNodes[iSub],
                                                                 firstCheckedNode[iSub], LSS(iSub),
                                                                 computeInactive, faceID, d2wallmax, (it != 0),
                                                                 &(*dd2wall)(iSub), &(*dd2wall_doppnode)(iSub),
                                                                 hybridNodes);
            break;
          default:
            (dom.getSubDomain())[iSub]->pseudoFastMarchingMethod(tag(iSub), X(iSub), d2wall(iSub), level,
                                                                 sortedNodes(iSub), nSortedNodes[iSub],
                                                                 firstCheckedNode[iSub], LSS(iSub),
                                                                 computeInactive, faceID, d2wallmax, (it != 0),
                                                                 nullptr, nullptr, hybridNodes);
        }
      }
    }
    if(!checkConverged) {
      checkConverged = 1;
      for(int iSub = 0; iSub < nSub; ++iSub) {
        visited[iSub] = visited[iSub] || (nSortedNodes[iSub] > 0);
        if(!visited[iSub] && (d2wall(iSub).size() > 0)) checkConverged = 0; // there exists an unvisited subdomain
      }
      dom.getCommunicator()->globalMin(1, &checkConverged);
    }
    DistVec<double> *_d2wall = (objective == MarchingMethodObjective::DD2WALL) ? dd2wall : &d2wall;
    if(checkConverged) {
      double res[2] = {0.0, 0.0};
      for(int iSub = 0; iSub < nSub; ++iSub) {
        for(int i = 0; i < (*_d2wall)(iSub).size(); i++) {
          if(LSS(iSub).isActive(0.0, i) || computeInactive) {
            res[0] += ((*_d2wall)(iSub)[i] - resnm1(iSub)[i]) * ((*_d2wall)(iSub)[i] - resnm1(iSub)[i]);
            res[1] += resnm1(iSub)[i] * resnm1(iSub)[i];
          }
        }
      }
      dom.getCommunicator()->globalSum(2, res);
      resn = sqrt(res[0]) / sqrt(res[1]);
      if(resn < iod.eqs.tc.tm.d2wall.eps) break;
    }
    // reset tags, nSortedNodes and firstCheckedNode for next iteration
    tag = tag0;
    for(int iSub = 0; iSub < nSub; ++iSub) {
      firstCheckedNode[iSub] = 0;
      nSortedNodes[iSub] = 0;
    }
    if(objective == MarchingMethodObjective::DD2WALL) dom.pseudoFastMarchingMethodCommDD2Wall(tag, *_d2wall, resnm1, sortedNodes, nSortedNodes, dd2wall_doppnode);
    else dom.pseudoFastMarchingMethodComm(tag, *_d2wall, resnm1, sortedNodes, nSortedNodes);
    it++;
  }
  if(it == iod.eqs.tc.tm.d2wall.maxIts) {
    if(!checkConverged) {
      if(objective == MarchingMethodObjective::D2WALL) {
        dom.getCommunicator()->fprintf(stderr,
                                       "*** Warning: iterative distance to wall reached maximum %d iterations but solution did\n"
                                       "             not reach entire domain, you may wish to increase MaxIts or use another method!\n", it);
      }
      else if(objective == MarchingMethodObjective::DD2WALL) {
        dom.getCommunicator()->fprintf(stderr,
                                       "*** Warning: iterative derivative of distance to wall reached maximum %d iterations but solution did\n"
                                       "             not reach entire domain, you may wish to increase MaxIts or use FD wall distance!\n", it);
      }
    }
    else {
      dom.getCommunicator()->fprintf(stderr,
                                     "*** Warning: iterative%s distance to wall reached maximum %d iterations (final residual = %e, target = %e)\n",
                                     objective == MarchingMethodObjective::DD2WALL ? " derivative of" : "", it, resn, iod.eqs.tc.tm.d2wall.eps);
    }
  }
  delete[] visited;
}

//------------------------------------------------------------------------------

int ReinitializeDistanceToWall::UpdateDistanceError(DistLevelSetStructure& LSS,
                                                    DistGeoState& distGeoState,
                                                    const double t) {
  int update = 0;
  if(predictorTime[1] < 0.0)
    return 1;
  else if(predictorTime[2] < 0.0 || nSkip > iod.eqs.tc.tm.d2wall.maxSkip)
    return 2;
  // compute distance predictions
  double dtnm2 = predictorTime[1]-predictorTime[2];
  double dtnm1 = predictorTime[0]-predictorTime[1];
  double dtn = t-predictorTime[0];
  double dtnodtnm1 = dtn/dtnm1;
  double dtnodtnm2 = dtn/dtnm2;
  double dtnm1odtnm2 = dtnm1/dtnm2;
  double dtnpdtnm1odtnm1pdtnm2 = (dtn+dtnm1)/(dtnm1+dtnm2);
  double d2wp1, d2wp, delta, r, phi;
  double deltaSA = 0.0, deltaD2W = 0.0;
  switch(iod.eqs.tc.tm.d2wall.frequencyadaptation) {
    case(WallDistanceMethodData::BOTH) : {
      for(int iSub = 0; iSub < dom.getNumLocSub(); ++iSub) {
        for(int k = 0; k < d2wall(iSub).size(); k++) {
          if((*predictorTag)(iSub)[k] > 1) {
            d2wp = (1.0+dtnodtnm1*(1.0+dtnpdtnm1odtnm1pdtnm2)) * d2wall(iSub)[k]
              - dtnodtnm1*(dtnodtnm2+dtnm1odtnm2+1.0) * (*d2wnm1)(iSub)[k]
              + dtnodtnm2*dtnpdtnm1odtnm1pdtnm2 * (*d2wnm2)(iSub)[k];
            d2wp1 = (1.0+dtnodtnm1) * d2wall(iSub)[k] - dtnodtnm1 * (*d2wnm1)(iSub)[k];
            r = ((*d2wnm1)(iSub)[k]-(*d2wnm2)(iSub)[k] + 1.0e-8)
              / (d2wall(iSub)[k]-(*d2wnm1)(iSub)[k] + 1.0e-8) * (dtnm1 / dtnm2);
            phi = std::max(2.0*r / (r*r + 1.0), 0.0);  // limit in case of strong gradients
            delta = (1.0-phi)*d2wp1+phi*d2wp-d2wall(iSub)[k];
            deltaD2W += delta*delta;
            delta *= (*sensitivSA)(iSub)[k];
            deltaSA += delta*delta;
          }
          else if((*predictorTag)(iSub)[k] < 1 && LSS(iSub).isActive(0.0,k)) {
            (distGeoState.getDistanceToWall())(iSub)[k] = LSS(iSub).distToInterface(0.0,k);
            (*predictorTag)(iSub)[k] = 1;
          }
        }
      }
      double deltanorm[4] = {deltaSA, scaleSA, deltaD2W, scaleD2W};
      dom.getCommunicator()->globalSum(4,deltanorm);
      if(sqrt(deltanorm[0])/sqrt(deltanorm[1]) > iod.eqs.tc.tm.d2wall.epsSA
        || sqrt(deltanorm[2])/sqrt(deltanorm[3]) > iod.eqs.tc.tm.d2wall.epsD2W)
        update = 2;
      break;
    }
    case(WallDistanceMethodData::SA) : {
      for(int iSub = 0; iSub < dom.getNumLocSub(); ++iSub) {
        for(int k = 0; k < d2wall(iSub).size(); k++) {
          if((*predictorTag)(iSub)[k] > 1) {
            d2wp = (1.0+dtnodtnm1*(1.0+dtnpdtnm1odtnm1pdtnm2)) * d2wall(iSub)[k]
              - dtnodtnm1*(dtnodtnm2+dtnm1odtnm2+1.0) * (*d2wnm1)(iSub)[k]
              + dtnodtnm2*dtnpdtnm1odtnm1pdtnm2 * (*d2wnm2)(iSub)[k];
            d2wp1 = (1.0+dtnodtnm1) * d2wall(iSub)[k] - dtnodtnm1 * (*d2wnm1)(iSub)[k];
            r = ((*d2wnm1)(iSub)[k]-(*d2wnm2)(iSub)[k] + 1.0e-8)
              / (d2wall(iSub)[k]-(*d2wnm1)(iSub)[k] + 1.0e-8) * (dtnm1 / dtnm2);
            phi = std::max(2.0*r / (r*r + 1.0), 0.0);  // limit in case of strong gradients
            delta = (*sensitivSA)(iSub)[k]*((1.0-phi)*d2wp1+phi*d2wp-d2wall(iSub)[k]);
            deltaSA += delta*delta;
          }
          else if((*predictorTag)(iSub)[k] < 1 && LSS(iSub).isActive(0.0,k)) {
            (distGeoState.getDistanceToWall())(iSub)[k] = LSS(iSub).distToInterface(0.0,k);
            (*predictorTag)(iSub)[k] = 1;
          }
        }
      }
      double deltanorm[2] = {deltaSA, scaleSA};
      dom.getCommunicator()->globalSum(2,deltanorm);
      if(sqrt(deltanorm[0])/sqrt(deltanorm[1]) > iod.eqs.tc.tm.d2wall.epsSA) {
        update = 2;
      }
      break;
    }
    case(WallDistanceMethodData::D2W) : {
      for(int iSub = 0; iSub < dom.getNumLocSub(); ++iSub) {
        for(int k = 0; k < d2wall(iSub).size(); k++) {
          if((*predictorTag)(iSub)[k] > 1) {
            d2wp = (1.0+dtnodtnm1*(1.0+dtnpdtnm1odtnm1pdtnm2)) * d2wall(iSub)[k]
              - dtnodtnm1*(dtnodtnm2+dtnm1odtnm2+1.0) * (*d2wnm1)(iSub)[k]
              + dtnodtnm2*dtnpdtnm1odtnm1pdtnm2 * (*d2wnm2)(iSub)[k];
            d2wp1 = (1.0+dtnodtnm1) * d2wall(iSub)[k] - dtnodtnm1 * (*d2wnm1)(iSub)[k];
            r = ((*d2wnm1)(iSub)[k]-(*d2wnm2)(iSub)[k] + 1.0e-8)
              / (d2wall(iSub)[k]-(*d2wnm1)(iSub)[k] + 1.0e-8) * (dtnm1 / dtnm2);
            phi = std::max(2.0*r / (r*r + 1.0), 0.0);  // limit in case of strong gradients
            delta = (1.0-phi)*d2wp1+phi*d2wp-d2wall(iSub)[k];
            deltaD2W += delta*delta;
          }
          else if((*predictorTag)(iSub)[k] < 1 && LSS(iSub).isActive(0.0,k)) {
            (distGeoState.getDistanceToWall())(iSub)[k] = LSS(iSub).distToInterface(0.0,k);
            (*predictorTag)(iSub)[k] = 1;
          }
        }
      }
      double deltanorm[2] = {deltaD2W, scaleD2W};
      dom.getCommunicator()->globalSum(2,deltanorm);
      if(sqrt(deltanorm[0])/sqrt(deltanorm[1]) > iod.eqs.tc.tm.d2wall.epsD2W) {
        update = 2;
      }
      break;
    }
    default :
      break;  // nothing to do here
  }
  return update;
}

//------------------------------------------------------------------------------

#include <ReinitializeDistanceToWall.C>
#include <InstantiationHelper.h>

#define ReinitializeDistanceToWall_INSTANTIATION_HELPER(dim) \
  template \
  void ReinitializeDistanceToWall::ReinitializePredictors(DistSVec<double, dim>& R);

DIM_INSTANTIATION_HELPER(ReinitializeDistanceToWall_INSTANTIATION_HELPER)

//------------------------------------------------------------------------------

#ifndef SCBLACS_H_
#define SCBLACS_H_

#include <LinkF77.h>

#ifdef __cplusplus
extern "C" {
    void F77NAME(blacs_pinfo)(int *mypnum, int *nprocs);
    void F77NAME(blacs_get)(int *context, int *what, int *val);
    void F77NAME(blacs_gridinit)(int *context, char *order, int *nprow, int *npcol);
    void F77NAME(blacs_gridinfo)(int *context, int *nprow, int *npcol, int *myrow, int *mycol);
    void F77NAME(blacs_gridexit)(int *context);
    void F77NAME(blacs_barrier)(int *context, char *scope);
    void F77NAME(blacs_exit)(int *);
    void F77NAME(igsum2d)(int *context, char *scope, char *top, int *m, int *n, int *A, int *lda, int *rdest, int *cdest);
    void F77NAME(dgsum2d)(int *context, char *scope, char *top, int *m, int *n, double *A, int *lda, int *rdest, int *cdest);
    void F77NAME(igamx2d)(int *context, char *scope, char *top, int *m, int *n, int *A, int *lda, int *ra, int *ca,
                           int *rcflag, int *rdest, int *cdest);
    void F77NAME(igamn2d)(int *context, char *scope, char *top, int *m, int *n, int *A, int *lda, int *ra, int *ca,
                           int *rcflag, int *rdest, int *cdest);
    void F77NAME(dgebs2d)(int *context, char *scope, char *top, int *m, int *n, double *A, int *lda);
    void F77NAME(dgebr2d)(int *context, char *scope, char *top, int *m, int *n, double *A, int *lda, int *rsrc, int *csrc);
    void F77NAME(igebs2d)(int *context, char *scope, char *top, int *m, int *n, int *A, int *lda);
    void F77NAME(igebr2d)(int *context, char *scope, char *top, int *m, int *n, int *A, int *lda, int *rsrc, int *csrc);
    int F77NAME(blacs_pnum)(int *context, int *iprow, int *jpcol);
}
#endif

#endif

#ifndef SCPBLAS_H_
#define SCPBLAS_H_

#include <LinkF77.h>

/*extern "C" {
#ifdef USE_MKL
#include <mkl_pblas.h>
#include <mkl_blacs.h>
#else
#include <pblas.h>
#include <PBpblas.h>
#include <PBblacs.h>
#endif
}*/

// Need to replace this with an appropriate blacs header file.
// Adjusted for 0 based indexing
#define DLEN_ 9
#define DTYPE_ 0
#define CTXT_ 1
#define M_ 2
// Manually change when compiling with PyTorch to #define AFN_ 3 -- JB
#define N_ 3
#define MB_ 4
#define NB_ 5
#define RSRC_ 6
#define CSRC_ 7
#define LLD_ 8


#ifdef __cplusplus
extern "C" {
    int  F77NAME(numroc)(const int *n, const int *nb, const int *myrow, const int *zero, const int *nprow);
    void F77NAME(descinit)(int *descb, const int *n, const int *uno, const int *mb, const int *nb, const int *mm, const int *nn, const int *context, const int *lld, int *info);
    void F77NAME(desc_convert)(int *desc_in, int *desc_out, int *info);
    void F77NAME(pdelset)( double *b, int *i, int *uno, int *descb, double *alpha );
    void F77NAME(pdelget)( char *, char *, double *, double *, int *, int *, int *);
    void F77NAME(pielset)( int *b, int *i, int *uno, int *descb, int *alpha );
    void F77NAME(pielget)( char *, char *, int *, int *, int *, int *, int *);
    void F77NAME(pdgesv)(int *, int *, double *, int *, int *, int *, int *, double *, int *, int *, int *, int *);
    int  F77NAME(indxl2g)(const int *, const int *, const int *, const int *, const int *);
    int  F77NAME(indxg2l)(const int *, const int *, const int *, const int *, const int *);
    int  F77NAME(indxg2p)(const int *, const int *, const int *, const int *, const int *);
    void F77NAME(pdlaprnt)( int *, int *, double *, int *, int *, int *, int *, int *, char**, int *, double *);
    void F77NAME(pdlapiv)(char *, char *, char *, int *, int *, double *, int *, int *, int *, int *, int *, int *, int *, int *);
    void F77NAME(fpdlapiv)(char *, char *, char *, int *, int *, double *, int *, int *, int *, int *, int *, int *, int *, int *);
    void F77NAME(fpslapiv)(char *, char *, char *, int *, int *, float *, int *, int *, int *, int *, int *, int *, int *, int *);
    void F77NAME(infog2l)(int *, int *, int*, int *, int *, int*, int *, int *, int*, int*, int*);
    void F77NAME(pdgeqrf)(int *, int *, double *, int *, int *, int *, double *, double *, int *, int *);
    void F77NAME(pdgels)(char *, int *, int *, int *, double *, int *, int *, int *,
                          double *, int *, int *, int *, double *, int *, int *);
    void F77NAME(pdormqr)(char *, char *, int *, int *, int *, double *, int *, int *, int *, double *,
                           double *, int *, int *, int *, double *, int *, int *);
    void F77NAME(pdtrtrs)(char *, char *, char *, int *, int *, double *, int *, int *, int *,
                           double *, int *, int *, int *, int *);
    void F77NAME(pdlacp2)(char *, int *, int *, double *, int *, int *, int *, double *, int *, int *, int *);
    int  F77NAME(ilcm)(int *, int *);
    int  F77NAME(pdgemr2d)(int *, int *, double *, int *, int *, int *, double *, int *, int *, int *, int *);
    double F77NAME(pdlange)(char *, int *, int *, double *, int *, int *, int *, double *);
    void F77NAME(pdlaqge)(int *, int *, double *, int *, int *, int *, double *, double *, double *, double *, double *, char *);
    void F77NAME(pdscal)(int *, double *, double *, int *, int *, int *, int *);
    void F77NAME(pdnrm2)(int *, double *, double *, int *, int *, int *, int *);
    void F77NAME(pdtrsv)(char *, char *, char *, int *, double *, int *, int *, int *, double *, int *, int *, int *, int *);
    void F77NAME(pdgemv)(char *, int *, int *, double *, double *, int *, int *, int *, double *, int *, int *, int *,
                          int *, double *, double *, int *, int *, int *, int *);
    void F77NAME(pdcopy)(int *, double *, int *, int *, int *, int *, double *, int *, int *, int *, int *);
    void F77NAME(pdgeadd)(char *, int *, int *, double *, double *, int *, int *, int *, double *, double *, int *, int *, int *);
    void F77NAME(pddot)(int *, double *, double *, int *, int *, int *, int *, double *, int *, int *, int *, int *);
    void F77NAME(pdgemm)(char *, char *, int *, int *, int *, double *, double *, int *, int *, int *, double *,
                          int *, int *, int *, double *, double *, int *, int *, int *);
    void F77NAME(dlasrt2)(char *, int *, double *, int *, int *);
    void F77NAME(ilasrt2)(char *, int *, int *, int *, int *);
    void F77NAME(pdpotrf)(char *, int *, double *, int *, int *, int *, int *);
    void F77NAME(pdpotrs)(char *, int *, int *, double *, int *, int *, int *, double *, int *, int *, int *, int *);
    void F77NAME(pdgesvd)(char *, char *, int *, int *, double *, int *, int *, int *, double *, double *, int *, int *, int *,
                           double *, int *, int *, int *, double *, int *, int *);
    // PDGESVD(JOBU,JOBVT,M,N,A,IA,JA,DESCA,S,U,IU,JU,DESCU,VT,IVT,JVT,DESCVT,WORK,LWORK,INFO)
}
#endif

#endif

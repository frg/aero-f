#ifndef _THERMAL_COND_FCN_H_
#define _THERMAL_COND_FCN_H_

#include <IoData.h>
#include <ViscoFcn.h>
#include <VarFcn.h>

//------------------------------------------------------------------------------
// Pure virtual base class
class ThermalCondFcn {

 public:
  ThermalCondFcn() {}
  virtual ~ThermalCondFcn() {}

  virtual double compute(double, int) = 0;

  virtual double compute_dkappa(double, int) = 0;

  virtual double computeDerivative(double, double, bool, int) = 0;

  virtual void computeDerivativeOperators(double, double&, double&, int) = 0;

  virtual bool is_constant() = 0;

};

//------------------------------------------------------------------------------

class ConstantThermalCondFcn : public ThermalCondFcn {

  // non-dimensional value of thermal conductivity coefficient
  // (non-dimensionalized by reference viscosity x reference Cv, already done in IoDataCore.C)
  double thermal_conductivity_coefficient;

 public:
  ConstantThermalCondFcn(IoData& iod) {
    thermal_conductivity_coefficient = iod.eqs.thermalCondModel.conductivity;
  }
  ~ConstantThermalCondFcn() {}

  double compute(double Tadim, int tag) {
    return thermal_conductivity_coefficient;
  }

  double compute_dkappa(double Tadim, int tag) {
    return 0;
  }

  double computeDerivative(double Tadim, double dTadim, bool dMach, int tag) {
    return 0.0;
  }

  void computeDerivativeOperators(double Tadim, double& dkappadTadim, double& dkappadMach, int tag) {
    dkappadTadim = 0.0;
    dkappadMach = 0.0;
  }

  bool is_constant() {
    return true;
  }

};

//------------------------------------------------------------------------------
// assumption: constant Prandtl number (as well as constant cp)
class ConstantPrandtlThermalCondFcn : public ThermalCondFcn {

  double ooPrandtl;
  double ooTurbPrandtl;

  ViscoFcn *viscoFcn;
  VarFcn *varFcn;

 public:
  ConstantPrandtlThermalCondFcn(IoData& iod, ViscoFcn *visf, VarFcn *vfn)
    : varFcn(vfn), viscoFcn(visf) {
    ooPrandtl = 1.0 / iod.eqs.thermalCondModel.prandtl;
    ooTurbPrandtl = 1.0 / iod.eqs.tc.prandtlTurbulent;
  }
  ~ConstantPrandtlThermalCondFcn() {
    viscoFcn = 0;
    varFcn = 0;
  }

  double compute(double Tadim, int tag) {
    return ooPrandtl * varFcn->specificHeatCstPressure(tag) * viscoFcn->compute_mu(Tadim, tag);
  }

  double turbulentConductivity(double mut, int tag) {
    return ooTurbPrandtl * varFcn->specificHeatCstPressure(tag) * mut;
  }

  double compute_dkappa(double Tadim, int tag) {
    return ooPrandtl * varFcn->specificHeatCstPressure(tag) * viscoFcn->compute_dmu(Tadim, tag);
  }

  double computeDerivative(double Tadim, double dTadim, bool dMach, int tag) {
    return ooPrandtl * varFcn->specificHeatCstPressure(tag) * viscoFcn->compute_muDerivative(Tadim, dTadim, dMach, tag);
  }

  void computeDerivativeOperators(double Tadim, double& dkappadTadim, double& dkappadMach, int tag) {
    double dmudTadim(0), dmudMach(0);
    viscoFcn->compute_muDerivativeOperators(Tadim, dmudTadim, dmudMach, tag);
    dkappadTadim = ooPrandtl * varFcn->specificHeatCstPressure(tag) * dmudTadim;
    dkappadMach = ooPrandtl * varFcn->specificHeatCstPressure(tag) * dmudMach;
  }

  double turbulentConductivityDerivative(double dmut, int tag) {
    return ooTurbPrandtl * varFcn->specificHeatCstPressure(tag) * dmut;
  }

  void turbulentConductivityDerivativeOperators(double mut, int tag, double& dkappatdmut) {
    dkappatdmut = ooTurbPrandtl * varFcn->specificHeatCstPressure(tag);
  }

  bool is_constant() {
    return viscoFcn->is_constant();
  }

};

//------------------------------------------------------------------------------

#endif

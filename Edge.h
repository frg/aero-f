#ifndef _EDGE_H_
#define _EDGE_H_

#include <Types.h>

#include <algorithm>
#include <map>
#include <vector>
#include <set>
#include <utility>
#include <cstdio>
#include <cassert>

class RecFcn;
class FluxFcn;
class ElemSet;
class EdgeGrad;
class GeoState;
class TimeLowMachPrec;
class LevelSetStructure;
class VarFcn;

template<int dim, class Scalar> class NodalGrad;
template<class Scalar> class Vec;
template<class Scalar, int dim> class SVec;
template<class Scalar, int dim> class MvpMat;
template<int dim> class EmbeddedBcData;
template<int dim> struct dRdXoperators;

//------------------------------------------------------------------------------

class EdgeSet {

 public:
  typedef std::vector<std::pair<int,int>> PtrContainer;
  typedef std::vector<bool> MasterFlagContainer;
  typedef std::map<std::pair<int,int>, int> MapContainer;

 private:
  MapContainer *mp;
  PtrContainer ptr;
  MasterFlagContainer masterFlag;

 public:
  EdgeSet();
  ~EdgeSet();

  int find(int, int);

  void push_back(const std::pair<int,int>& edge) {
    ptr.push_back(edge);
  }

  void assign(int i, const std::pair<int,int>& edge) {
    assert(i < ptr.size());
    ptr[i] = edge;
  }

  auto begin() -> decltype(ptr.begin()) { return ptr.begin(); }
  auto end() -> decltype(ptr.end()) { return ptr.end(); }

  void createPointers(Vec<int>&);
  void clearMap();

  void erase(const std::set<int>&);
  void renumber(const std::vector<int>&);

  void setMasterFlag(std::vector<bool>& flag) {
    masterFlag.resize(flag.size());
    std::copy(flag.begin(), flag.end(), masterFlag.begin());
  }

  MasterFlagContainer& getMasterFlag() {
    return masterFlag;
  }

  const PtrContainer& getPtr() {
    return ptr;
  }

  int size() const {
    return int(ptr.empty() ? mp->size() : ptr.size());
  }

  template<int dim>
  void computeTimeStep(VarFcn *, GeoState&, SVec<double, dim>&,
                       Vec<double>&, Vec<double>&, TimeLowMachPrec&, double, EmbeddedBcData<dim> *);

  template<int dim>
  void computeDerivativeOfTimeStep(VarFcn *, GeoState&, SVec<double, dim>&, SVec<double, dim>&,
                                   Vec<double>&, Vec<double>&, double, TimeLowMachPrec&, EmbeddedBcData<dim> *);

  template<int dim>
  int computeFiniteVolumeTerm(std::vector<int_t>&, Vec<double>&, FluxFcn **, RecFcn *, ElemSet&, GeoState&,
                              SVec<double, dim>&, NodalGrad<dim, double>&, EdgeGrad *, NodalGrad<dimLS, double> *,
                              SVec<double, dim>&, SVec<bool, 2>&, int, int, EmbeddedBcData<dim> *, int&,
                              std::vector<int>&, std::vector<bool>&, Vec<bool> * = nullptr);

  template<int dim>
  void computeDerivativeOfFiniteVolumeTerm(Vec<double>&, Vec<double>&, FluxFcn **, RecFcn *, ElemSet&, GeoState&,
                                           SVec<double, dim>&, SVec<double, dim>&, NodalGrad<dim, double>&, EdgeGrad *,
                                           double, SVec<double, dim>&, EmbeddedBcData<dim> *);

  template<int dim>
  void computeDerivativeOfFiniteVolumeTerm(dRdXoperators<dim>&, GeoState&, NodalGrad<dim, double>&, Vec<double>&, SVec<double, dim>&,
                                           SVec<double, dim>&);

  template<int dim>
  void computeTransposeDerivativeOfFiniteVolumeTerm(dRdXoperators<dim>& dRdXop, SVec<double, dim>&, NodalGrad<dim, double>&,
                                                    GeoState&, SVec<double, 3>&);

  template<int dim>
  void computeDerivativeOperatorsOfFiniteVolumeTerm(FluxFcn **, RecFcn *, GeoState&, SVec<double, dim>&, NodalGrad<dim, double>&,
                                                    EmbeddedBcData<dim> *, dRdXoperators<dim>&);

  template<int dim, class Scalar, int neq>
  void computeJacobianFiniteVolumeTerm(std::vector<int_t>&, FluxFcn **, ElemSet&, GeoState&, Vec<double> *,
                                       SVec<double, dim>&, RecFcn *, NodalGrad<dim, double>&, EdgeGrad *, NodalGrad<dimLS, double> *,
                                       MvpMat<Scalar, neq>&, int, EmbeddedBcData<dim> *, bool *, std::vector<int>&,
                                       std::vector<bool>&);

  template<int dim>
  void computeFiniteVolumeTermLS(RecFcn *, RecFcn *, ElemSet&, GeoState&, SVec<double, dim>&,
                                 NodalGrad<dim, double>&, EdgeGrad *, NodalGrad<dimLS, double>&, EdgeGrad *,
                                 SVec<double, dimLS>&, SVec<double, dimLS>&, EmbeddedBcData<dim> *);

  template<int dim, class Scalar>
  void computeJacobianFiniteVolumeTermLS(RecFcn *, RecFcn *, ElemSet&, GeoState&, SVec<double, dim>&,
                                         NodalGrad<dim, double>&, EdgeGrad *, NodalGrad<dimLS, double>&, EdgeGrad *,
                                         SVec<double, dimLS>&, MvpMat<Scalar, dimLS>&, EmbeddedBcData<dim> *, bool *);

  void tagInterfaceNodes(Vec<int>&, LevelSetStructure&);

  void tagInterfaceNodes(int, Vec<int>&, SVec<double, dimLS>&);

  void pseudoFastMarchingMethodInitialization(Vec<int>&, Vec<double>&, Vec<int>&,
                                              int&, LevelSetStructure&, bool, int, Vec<double> * = nullptr);

  void pseudoFastMarchingMethodInitializationDerivativeOperators(Vec<int>&, LevelSetStructure&, bool, int, SVec<std::pair<int, double>, 3>&);


  int checkReconstructedValues(int, int, double *, double *, VarFcn *, int_t *,
                               int, SVec<bool, 2>&, int, int, bool, bool);

};

//------------------------------------------------------------------------------

#endif

#ifndef _DIST_NODAL_GRAD_H_
#define _DIST_NODAL_GRAD_H_

#include <IoData.h>
#include <NodeData.h>
#include <Types.h>

#include <functional>

class RecFcn;
class Communicator;
class Domain;
class DistGeoState;

template<int dim> class DistEmbeddedBcData;
template<class Scalar> class DistVec;
template<class Scalar, int Dim> class DistSVec;
template<int dim> class MatVecProd_dRdX;
template<int Dim, class Scalar> class NodalGrad;

//------------------------------------------------------------------------------

template<int Dim, class Scalar>
class DistNodalGrad {

  SchemeData& schemeData;
  SchemeFixData& fixes;
  Communicator *com;
  Domain *domain;

  int failSafe;
  int lastConfig;
  int numLocSub;
  int iteration;

  int_t max_node_pre_amr;

  DistVec<bool> *tag;
  DistVec<bool> *backuptag;
  DistVec<int> *count;  // used for zeroing embedded surface neighbors

  DistSVec<double, Dim> *Vmin;
  DistSVec<double, Dim> *Vmax;
  DistSVec<double, Dim> *phi;
  DistSVec<double, Dim> *dphi_ii; // ∂[phi_i]/∂Vi (used for H2 matrix-vector product and preconditioner)
  DistSVec<double, Dim> *dphi_ij; // ∂[phi_i]/∂Vj (used for H2 matrix-vector product and preconditioner)
  DistSVec<double, Dim> *dphi_ji; // ∂[phi_j]/∂Vi (used for H2 matrix-vector product and preconditioner)
  DistSVec<int_t, Dim> *phi_loc;
  DistSVec<double, 5*Dim> *Vrec;

  DistSVec<double, 3> *sensor;
  DistVec<double> *sigma;

  DistSVec<double, 3> *wii;
  DistSVec<double, 3> *wij;
  DistSVec<double, 3> *wji;
  DistSVec<double, 2> *weights; // used for weighted least-squares

  DistSVec<Scalar, Dim> *ddx, *ddx_copy;
  DistSVec<Scalar, Dim> *ddy, *ddy_copy;
  DistSVec<Scalar, Dim> *ddz, *ddz_copy;

  NodalGrad<Dim, Scalar> **subNodalGrad;

  int lastConfigSA;
  DistSVec<double, Dim> *dVmin;
  DistSVec<double, Dim> *dVmax;
  DistSVec<double, Dim> *dphi;
  DistSVec<double, 3> *dwii;
  DistSVec<double, 3> *dwij;
  DistSVec<double, 3> *dwji;
  DistSVec<Scalar, Dim> *dddx;
  DistSVec<Scalar, Dim> *dddy;
  DistSVec<Scalar, Dim> *dddz;

  double spheres[SchemeFixData::num][4];
  double boxes[SchemeFixData::num][2][3];
  double cones[SchemeFixData::num][2][4];
  int nspheres, nboxes, ncones;

  // for gradient limiting
  std::function<double(double&, double&, double&, double&, double&)> limiter;
  double limitBarth(const double&, double&, double&, double&, double&, double&);
  double limitVenkat(const double&, double&, double&, double&, double&, double&);
  double limitMichalak(const double&, double&, double&, double&, double&, double&);

  // for sensitivity analysis
  std::function<double(double&, double&, double&, double&, double&, double&, double&, double&, double&, double&)> limiterDerivative;
  double computeDerivativeOfBarthFcn(const double&, double&, double&, double&, double&, double&, double&, double&, double&, double&, double&);
  double computeDerivativeOfVenkatFcn(const double&, double&, double&, double&, double&, double&, double&, double&, double&, double&, double&);
  double computeDerivativeOfMichalakFcn(const double&, double&, double&, double&, double&, double&, double&, double&, double&, double&, double&);

 public:
  DistNodalGrad(InputData&, SchemeData&, SchemeFixData&, int, Domain *, bool = false, int = Dim);
  ~DistNodalGrad();

  void resize1(NodeData *);
  void resize2();

  NodalGrad<Dim, Scalar>& operator()(int i) const {
    return *subNodalGrad[i];
  }
  DistSVec<Scalar, Dim>& getX() {
    return *ddx;
  }
  DistSVec<Scalar, Dim>& getY() {
    return *ddy;
  }
  DistSVec<Scalar, Dim>& getZ() {
    return *ddz;
  }
  DistSVec<Scalar, Dim>& getXderivative() const {
    return *dddx;
  }
  DistSVec<Scalar, Dim>& getYderivative() const {
    return *dddy;
  }
  DistSVec<Scalar, Dim>& getZderivative() const {
    return *dddz;
  }
  DistVec<bool> *getTag() {
    return tag;
  }
  DistSVec<double, Dim> *getVmin() {
    return Vmin;
  }
  DistSVec<double, Dim> *getVmax() {
    return Vmax;
  }
  DistSVec<double, Dim> *getPhi() {
    return phi;
  }
  DistSVec<double, Dim> *getdPhi_ii() {
    return dphi_ii;
  }
  DistSVec<double, Dim> *getdPhi_ij() {
    return dphi_ij;
  }
  DistSVec<double, Dim> *getdPhi_ji() {
    return dphi_ji;
  }
  DistSVec<int_t, Dim> *getPhiLoc() {
    return phi_loc;
  }
  DistSVec<double, 5*Dim> *getVrec() {
    return Vrec;
  }
  void resetLastConfig() {
    lastConfig = -1;
  }
  SchemeData& getSchemeData() {
    return schemeData;
  }

  void updateFixes();

  template<int dim>
  void computeWeights(DistSVec<double, 3>&, DistEmbeddedBcData<dim> *, int = 0);

  template<int dim>
  void computeDerivativeOfWeights(DistSVec<double, 3>&, DistSVec<double, 3>&, DistEmbeddedBcData<dim> *);

  void computeDerivativeOfWeights(MatVecProd_dRdX<Dim>&, DistSVec<double, 3>&, DistSVec<double, 6>&);

  void computeTransposeDerivativeOfWeights(MatVecProd_dRdX<Dim>&, DistSVec<double, 6>&, DistSVec<double, 3>&);

  void computeDerivativeOfWeightsOperators(DistSVec<double, 3>&, MatVecProd_dRdX<Dim>&);

  template<int dim>
  void compute(int, DistSVec<Scalar, Dim>&, DistEmbeddedBcData<dim> *, DistGeoState&, int = 0,
               DistVec<bool> * = nullptr);

  template<int dim>
  void computeDerivative(int, DistSVec<Scalar, Dim>&, DistSVec<Scalar, Dim>&, DistEmbeddedBcData<dim> *,
                         DistGeoState&, int = 0);

  void computeDerivative(MatVecProd_dRdX<Dim>&, DistSVec<Scalar, Dim>&, DistGeoState&);

  void computeTransposeDerivative(MatVecProd_dRdX<Dim>&, DistSVec<Scalar, Dim>&, DistSVec<double, 3>&);

  void computeDerivativeOperators(DistGeoState&, DistSVec<Scalar, Dim>&, MatVecProd_dRdX<Dim>&);

  template<int dim>
  void limit(RecFcn *, VarFcn *, DistSVec<double, 3>&, DistVec<double>&, DistSVec<Scalar, Dim>&, DistEmbeddedBcData<dim> *);

  void limitJacobian(RecFcn *, VarFcn *, DistSVec<double, 3>&, DistVec<double>&, DistSVec<Scalar, Dim>&);

  void limitDerivative(RecFcn *, DistSVec<double, 3>&, DistSVec<double, 3>&, DistVec<double>&, DistVec<double>&,
                       DistSVec<Scalar, Dim>&, DistSVec<Scalar, Dim>&);

  void computeTag();

  void fix(DistSVec<bool, 2>&);

  void resetTag();

  void fsoInitialize();

  void initializeDerivatives();

  void writeLimiterPhiToDisk(char *);

  void writeFixesToDisk(char *);

};

//------------------------------------------------------------------------------

#endif

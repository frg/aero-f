#ifndef _ROEJAC7_H_
#define _ROEJAC7_H_

#include <algorithm>
#include <cmath>
#include <limits>
#include <Eigen/Core>
#include <AutoDiff/Function.h>
#include <AutoDiff/SpaceDerivatives.h>

template<typename Scalar>
class RoeFlux7Function : public VectorValuedFunction<5, 5, Scalar, 11, 0, double> {
  const Eigen::Array<double, 11, 1>& sconst;
  const Eigen::Array<int, 0, 1>& iconst;

 public:
  RoeFlux7Function(const Eigen::Array<double, 11, 1>& _sconst, const Eigen::Array<int, 0, 1>& _iconst)
    : sconst(_sconst), iconst(_iconst) {}

  Eigen::Matrix<Scalar, 5, 1> operator()(const Eigen::Matrix<Scalar, 5, 1>& ug, Scalar) {
    const double& gam = sconst[0];
    const double& pstiff = sconst[1];
    const double *enormal = &sconst[2];
    const double& evitno = sconst[5];
    const double *ud = &sconst[6];
    Eigen::Matrix<Scalar, 5, 1> phi;

    using std::sqrt;

    // -----------------------------------------------------------------------
    // This routine computes the Flux of Roe taken at the vectors Ug, Ud
    // normal is the normal of the boundary concerned by the flux.
    // phi stores the resulting flux.
    // gam is the ratio of cp/cv
    // -----------------------------------------------------------------------

    // Initialisation

    double gam1 = gam - 1.;
    double invgam1 = 1. / gam1;

    double rnorm = sqrt(enormal[0] * enormal[0] + enormal[1] * enormal[1] + enormal[2] * enormal[2]);
    double invnorm = 1. / rnorm;

    double normal[3] = { enormal[0] * invnorm, enormal[1] * invnorm, enormal[2] * invnorm };
    double vitno = evitno * invnorm;
    Scalar vitg2 = (ug[1] * ug[1] + ug[2] * ug[2] + ug[3] * ug[3]) * .5;
    double vitd2 = (ud[1] * ud[1] + ud[2] * ud[2] + ud[3] * ud[3]) * .5;

    // Computation of the Roe-averaged state

    Scalar squsr1 = sqrt(ug[0]);
    double squsr2 = sqrt(ud[0]);

    Scalar ener1 = (ug[4] + gam * pstiff) * invgam1 + ug[0] * vitg2;
    double ener2 = (ud[4] + gam * pstiff) * invgam1 + ud[0] * vitd2;

    Scalar usro = 1. / (squsr1 + squsr2);

    Scalar uar2 = (squsr1 * ug[1] + squsr2 * ud[1]) * usro;
    Scalar uar3 = (squsr1 * ug[2] + squsr2 * ud[2]) * usro;
    Scalar uar4 = (squsr1 * ug[3] + squsr2 * ud[3]) * usro;
    Scalar uar5 = ((ener1 + ug[4]) / squsr1 + (ener2 + ud[4]) / squsr2) * usro;

    Scalar qir = (uar2 * uar2 + uar3 * uar3 + uar4 * uar4) * .5;
    Scalar cr2 = gam1 * (uar5 - qir);
    Scalar cr = sqrt(cr2);

    Scalar vdotnt = normal[0] * uar2 + normal[1] * uar3 + normal[2] * uar4;

    if(vdotnt - vitno > 0.) {
      // Computation of the centered terms
      Scalar vdotn = ug[1] * normal[0] + ug[2] * normal[1] + ug[3] * normal[2];
      Scalar h = gam * (ug[4] + pstiff) + gam1 * ug[0] * vitg2;
      h /= gam1 * ug[0];
      phi[0] = ug[0] * (vdotn - vitno);
      phi[1] = phi[0] * ug[1] + ug[4] * normal[0];
      phi[2] = phi[0] * ug[2] + ug[4] * normal[1];
      phi[3] = phi[0] * ug[3] + ug[4] * normal[2];
      phi[4] = phi[0] * h + ug[4] * vitno;

      // Computation of the dissipation term
      Scalar vp = vdotnt - cr - vitno;
      if(vp < 0.) {
        Scalar dif1 = -ug[0] + ud[0];
        Scalar dif2 = -ug[0] * ug[1] + ud[0] * ud[1];
        Scalar dif3 = -ug[0] * ug[2] + ud[0] * ud[2];
        Scalar dif4 = -ug[0] * ug[3] + ud[0] * ud[3];
        Scalar dif5 = -ener1 + ener2;

        Scalar flur = -vp * (cr * (vdotnt * dif1 - normal[0] * dif2 -
                      normal[1] * dif3 - normal[2] * dif4) + gam1 *
                      (qir * dif1 - uar2 * dif2 - uar3 * dif3 - uar4 * dif4 + dif5));

        cr2 = .5 / cr2;
        Scalar crflur = cr * -flur;

        phi[0] -= flur * cr2;
        phi[1] -= (normal[0] * crflur + uar2 * flur) * cr2;
        phi[2] -= (normal[1] * crflur + uar3 * flur) * cr2;
        phi[3] -= (normal[2] * crflur + uar4 * flur) * cr2;
        phi[4] -= (vdotnt * crflur + uar5 * flur) * cr2;
      }
    }
    else {
      // Computation of the centered terms
      double vdotn = ud[1] * normal[0] + ud[2] * normal[1] + ud[3] * normal[2];
      double h = gam * (ud[4] + pstiff) + gam1 * ud[0] * vitd2;
      h /= gam1 * ud[0];
      phi[0] = ud[0] * (vdotn - vitno);
      phi[1] = phi[0] * ud[1] + ud[4] * normal[0];
      phi[2] = phi[0] * ud[2] + ud[4] * normal[1];
      phi[3] = phi[0] * ud[3] + ud[4] * normal[2];
      phi[4] = phi[0] * h + ud[4] * vitno;

      // Computation of the dissipation term
      Scalar vp = vdotnt + cr - vitno;
      if(vp > 0.) {
        Scalar dif1 = -ug[0] + ud[0];
        Scalar dif2 = -ug[0] * ug[1] + ud[0] * ud[1];
        Scalar dif3 = -ug[0] * ug[2] + ud[0] * ud[2];
        Scalar dif4 = -ug[0] * ug[3] + ud[0] * ud[3];
        Scalar dif5 = -ener1 + ener2;

        Scalar flur = vp * (-cr * (vdotnt * dif1 - normal[0] * dif2 -
                      normal[1] * dif3 - normal[2] * dif4) + gam1 *
                      (qir * dif1 - uar2 * dif2 - uar3 * dif3 - uar4 * dif4 + dif5));

        cr2 = .5 / cr2;
        Scalar crflur = cr * flur;

        phi[0] -= flur * cr2;
        phi[1] -= (normal[0] * crflur + uar2 * flur) * cr2;
        phi[2] -= (normal[1] * crflur + uar3 * flur) * cr2;
        phi[3] -= (normal[2] * crflur + uar4 * flur) * cr2;
        phi[4] -= (vdotnt * crflur + uar5 * flur) * cr2;
      }
    }

    // Final phi
    return rnorm * phi;
  }
};

void roejac7(int type, double gam, double pstiff, double enormal[3],
             double evitno, double *ug, double *ud, double *jac) {
  Eigen::Array<int, 0, 1> iconst;
  Eigen::Array<double, 11, 1> sconst;
  sconst << gam, pstiff, enormal[0], enormal[1], enormal[2], evitno,
            ud[0], ud[1], ud[2], ud[3], ud[4];
  Eigen::Matrix<double, 5, 1> v = Eigen::Map<Eigen::Matrix<double, 5, 1>>(ug);
  Jacobian<double, RoeFlux7Function> df5dv(sconst, iconst);
  Eigen::Map<Eigen::Matrix<double, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor>> J(jac, 5 + type, 5 + type);
  switch(type) {
    case 0 :
      J = df5dv(v, 0.);
      break;
    case 1 :
      J.topLeftCorner(5, 5) = df5dv(v, 0.);
      J.bottomLeftCorner(1, 5).setZero();
      J.topRightCorner(5, 1).setZero();
      J.bottomRightCorner(1, 1).setZero();
      break;
    case 2 :
      J.topLeftCorner(5, 5) = df5dv(v, 0.);
      J.bottomLeftCorner(2, 5).setZero();
      J.topRightCorner(5, 2).setZero();
      J.bottomRightCorner(2, 2).setZero();
      break;
  }
}

#endif

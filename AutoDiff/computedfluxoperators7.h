#ifndef _COMPUTEDFLUXOPERATORS7_H_
#define _COMPUTEDFLUXOPERATORS7_H_

#include <algorithm>
#include <cmath>
#include <limits>
#include <Eigen/Core>
#include <AutoDiff/Function.h>
#include <AutoDiff/SpaceDerivatives.h>

template<typename Scalar>
class RoeFlux7Function2 : public VectorValuedFunction<15, 5, Scalar, 1, 0, double> {
  const Eigen::Array<double, 1, 1>& sconst;
  const Eigen::Array<int, 0, 1>& iconst;

 public:
  RoeFlux7Function2(const Eigen::Array<double, 1, 1>& _sconst, const Eigen::Array<int, 0, 1>& _iconst)
    : sconst(_sconst), iconst(_iconst) {}

  Eigen::Matrix<Scalar, 5, 1> operator()(const Eigen::Matrix<Scalar, 15, 1>& q, Scalar) {
    const double& gam = sconst[0];
    const Scalar& pstiff = q[0];
    const Scalar *enormal = &q[1];
    const Scalar& evitno = q[4];
    const Scalar *ug = &q[5];
    const Scalar *ud = &q[10];
    Eigen::Matrix<Scalar, 5, 1> phi;

    using std::sqrt;

    // -----------------------------------------------------------------------
    // This routine computes the Flux of Roe taken at the vectors Ug, Ud
    // normal is the normal of the boundary concerned by the flux.
    // phi stores the resulting flux.
    // gam is the ratio of cp/cv
    // -----------------------------------------------------------------------

    // Initialisation

    double gam1 = gam - 1.;
    double invgam1 = 1. / gam1;

    Scalar rnorm = sqrt(enormal[0] * enormal[0] + enormal[1] * enormal[1] + enormal[2] * enormal[2]);
    Scalar invnorm = 1. / rnorm;

    Scalar normal[3] = { enormal[0] * invnorm, enormal[1] * invnorm, enormal[2] * invnorm };
    Scalar vitno = evitno * invnorm;
    Scalar vitg2 = (ug[1] * ug[1] + ug[2] * ug[2] + ug[3] * ug[3]) * .5;
    Scalar vitd2 = (ud[1] * ud[1] + ud[2] * ud[2] + ud[3] * ud[3]) * .5;

    // Computation of the Roe-averaged state

    Scalar squsr1 = sqrt(ug[0]);
    Scalar squsr2 = sqrt(ud[0]);

    Scalar ener1 = (ug[4] + gam * pstiff) * invgam1 + ug[0] * vitg2;
    Scalar ener2 = (ud[4] + gam * pstiff) * invgam1 + ud[0] * vitd2;

    Scalar usro = 1. / (squsr1 + squsr2);

    Scalar uar2 = (squsr1 * ug[1] + squsr2 * ud[1]) * usro;
    Scalar uar3 = (squsr1 * ug[2] + squsr2 * ud[2]) * usro;
    Scalar uar4 = (squsr1 * ug[3] + squsr2 * ud[3]) * usro;
    Scalar uar5 = ((ener1 + ug[4]) / squsr1 + (ener2 + ud[4]) / squsr2) * usro;

    Scalar qir = (uar2 * uar2 + uar3 * uar3 + uar4 * uar4) * .5;
    Scalar cr2 = gam1 * (uar5 - qir);
    Scalar cr = sqrt(cr2);

    Scalar vdotnt = normal[0] * uar2 + normal[1] * uar3 + normal[2] * uar4;

    if(vdotnt - vitno > 0.) {
      // Computation of the centered terms
      Scalar vdotn = ug[1] * normal[0] + ug[2] * normal[1] + ug[3] * normal[2];
      Scalar h = gam * (ug[4] + pstiff) + gam1 * ug[0] * vitg2;
      h /= gam1 * ug[0];
      phi[0] = ug[0] * (vdotn - vitno);
      phi[1] = phi[0] * ug[1] + ug[4] * normal[0];
      phi[2] = phi[0] * ug[2] + ug[4] * normal[1];
      phi[3] = phi[0] * ug[3] + ug[4] * normal[2];
      phi[4] = phi[0] * h + ug[4] * vitno;

      // Computation of the dissipation term
      Scalar vp = vdotnt - cr - vitno;
      if(vp < 0.) {
        Scalar dif1 = -ug[0] + ud[0];
        Scalar dif2 = -ug[0] * ug[1] + ud[0] * ud[1];
        Scalar dif3 = -ug[0] * ug[2] + ud[0] * ud[2];
        Scalar dif4 = -ug[0] * ug[3] + ud[0] * ud[3];
        Scalar dif5 = -ener1 + ener2;

        Scalar flur = -vp * (cr * (vdotnt * dif1 - normal[0] * dif2 -
                      normal[1] * dif3 - normal[2] * dif4) + gam1 *
                      (qir * dif1 - uar2 * dif2 - uar3 * dif3 - uar4 * dif4 + dif5));

        cr2 = .5 / cr2;
        Scalar crflur = cr * -flur;

        phi[0] -= flur * cr2;
        phi[1] -= (normal[0] * crflur + uar2 * flur) * cr2;
        phi[2] -= (normal[1] * crflur + uar3 * flur) * cr2;
        phi[3] -= (normal[2] * crflur + uar4 * flur) * cr2;
        phi[4] -= (vdotnt * crflur + uar5 * flur) * cr2;
      }
    }
    else {
      // Computation of the centered terms
      Scalar vdotn = ud[1] * normal[0] + ud[2] * normal[1] + ud[3] * normal[2];
      Scalar h = gam * (ud[4] + pstiff) + gam1 * ud[0] * vitd2;
      h /= gam1 * ud[0];
      phi[0] = ud[0] * (vdotn - vitno);
      phi[1] = phi[0] * ud[1] + ud[4] * normal[0];
      phi[2] = phi[0] * ud[2] + ud[4] * normal[1];
      phi[3] = phi[0] * ud[3] + ud[4] * normal[2];
      phi[4] = phi[0] * h + ud[4] * vitno;

      // Computation of the dissipation term
      Scalar vp = vdotnt + cr - vitno;
      if(vp > 0.) {
        Scalar dif1 = -ug[0] + ud[0];
        Scalar dif2 = -ug[0] * ug[1] + ud[0] * ud[1];
        Scalar dif3 = -ug[0] * ug[2] + ud[0] * ud[2];
        Scalar dif4 = -ug[0] * ug[3] + ud[0] * ud[3];
        Scalar dif5 = -ener1 + ener2;

        Scalar flur = vp * (-cr * (vdotnt * dif1 - normal[0] * dif2 -
                      normal[1] * dif3 - normal[2] * dif4) + gam1 *
                      (qir * dif1 - uar2 * dif2 - uar3 * dif3 - uar4 * dif4 + dif5));

        cr2 = .5 / cr2;
        Scalar crflur = cr * flur;

        phi[0] -= flur * cr2;
        phi[1] -= (normal[0] * crflur + uar2 * flur) * cr2;
        phi[2] -= (normal[1] * crflur + uar3 * flur) * cr2;
        phi[3] -= (normal[2] * crflur + uar4 * flur) * cr2;
        phi[4] -= (vdotnt * crflur + uar5 * flur) * cr2;
      }
    }

    // Final phi
    return rnorm * phi;
  }
};

void computedfluxoperators7(int type, double gam, double pstiff, double enormal[3],
                            double evitno, double *ug, double *ud, double *dphidenormal,
                            double *dphidevitno, double *dphidug, double *dphidud,
                            double *dphidpstiff) {
  Eigen::Array<int, 0, 1> iconst;
  Eigen::Array<double, 1, 1> sconst;
  sconst << gam;
  Eigen::Matrix<double, 15, 1> v;
  v << pstiff, enormal[0], enormal[1], enormal[2], evitno,
       Eigen::Map<Eigen::Matrix<double, 5, 1>>(ug),
       Eigen::Map<Eigen::Matrix<double, 5, 1>>(ud);
  Jacobian<double, RoeFlux7Function2> df5dv(sconst, iconst);
  Eigen::Matrix<double, 5, 15> J = df5dv(v, 0.);
  Eigen::Map<Eigen::Matrix<double, 5, 1>> d1(dphidpstiff);                   d1 = J.col(0);
  Eigen::Map<Eigen::Matrix<double, 5, 3, Eigen::RowMajor>> d2(dphidenormal); d2 = J.block<5, 3>(0, 1);
  Eigen::Map<Eigen::Matrix<double, 5, 1>> d3(dphidevitno);                   d3 = J.col(4);
  Eigen::Map<Eigen::Matrix<double, 5, 5, Eigen::RowMajor>> d4(dphidug);      d4 = J.block<5, 5>(0, 5);
  Eigen::Map<Eigen::Matrix<double, 5, 5, Eigen::RowMajor>> d5(dphidud);      d5 = J.block<5, 5>(0, 10);
}

void gxroeflux7(int type, double gam, double pstiff, double dpstiff, double *enormal, double *denormal,
                double evitno, double devitno, double *ug, double *dug, double *ud, double *dud,
                double *phi, double *dphi) {
  Eigen::Array<int, 0, 1> iconst;
  Eigen::Array<double, 1, 1> sconst;
  sconst << gam;
  typedef Eigen::AutoDiffScalar<Eigen::Matrix<double, 1, 1>> Scalar;
  Eigen::Matrix<double, 15, 1> v, dv;
  v << pstiff, enormal[0], enormal[1], enormal[2], evitno,
       Eigen::Map<Eigen::Matrix<double, 5, 1>>(ug),
       Eigen::Map<Eigen::Matrix<double, 5, 1>>(ud);
  dv << dpstiff, denormal[0], denormal[1], denormal[2], devitno,
        Eigen::Map<Eigen::Matrix<double, 5, 1>>(dug),
        Eigen::Map<Eigen::Matrix<double, 5, 1>>(dud);
  Eigen::Matrix<Scalar, 15, 1> v_;
  for(int i = 0; i < 15; ++i) {
    v_[i].value() = v[i];
    v_[i].derivatives()[0] = dv[i];
  }
  RoeFlux7Function2<Scalar> f(sconst, iconst);
  Eigen::Matrix<Scalar, 5, 1> phi_ = f(v_, Scalar(0.));
  for(int i = 0; i < 5; ++i) {
    phi[i] = phi_[i].value();
    dphi[i] = phi_[i].derivatives()[0];
  }
}

#endif

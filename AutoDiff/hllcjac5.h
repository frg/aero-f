#ifndef _HLLCJAC5_H_
#define _HLLCJAC5_H_

#include <algorithm>
#include <cmath>
#include <limits>
#include <Eigen/Core>
#include <AutoDiff/Function.h>
#include <AutoDiff/SpaceDerivatives.h>

template<typename Scalar>
class HLLCFlux5Function : public VectorValuedFunction<5, 5, Scalar, 18, 1, double> {
  const Eigen::Array<double, 18, 1>& sconst;
  const Eigen::Array<int, 1, 1>& iconst;

 public:
  HLLCFlux5Function(const Eigen::Array<double, 18, 1>& _sconst, const Eigen::Array<int, 1, 1>& _iconst)
    : sconst(_sconst), iconst(_iconst) {}

  Eigen::Matrix<Scalar, 5, 1> operator()(const Eigen::Matrix<Scalar, 5, 1>& ug, Scalar) {
    const double& gamma = sconst[0];
    const double& gam = sconst[1];
    const double& pstiff = sconst[2];
    const double *enormal = &sconst[3];
    const double& evitno = sconst[6];
    const double& mach = sconst[7];
    const double& k1 = sconst[8];
    const double& cmach = sconst[9];
    const double& shockreducer = sconst[10];
    const double& irey = sconst[11];
    const double& length = sconst[12];
    const double *ud = &sconst[13];
    const int& prec = iconst[0];
    Eigen::Matrix<Scalar, 5, 1> phi;

    // System generated locals
    Scalar d__1;

    // Builtin functions
    using std::sqrt;
    using std::abs;

    // Local variables
    double gam1, rnorm, invnorm, normal[3], vitno, solrgt[7], crgt, vnrgt,
           hrgt;
    Scalar vplftroe, vprgtroe, flu[6], beta, clft, croe, hlft, qroe, slft,
           srgt, beta2, croe2, updir, vnlft, vnroe, vplft, pstar, sstar,
           ustar[6], vprgt, rholft, rhorgt, sollft[7], rhoinv, roemoy[6],
           vnstar, shock, locmach;

    // --------------------------------------------------------------------- 
    // Computes the convective fluxes using the approximate Riemann          
    // solver of HLLC with optional low Mach preconditioning                 
    // Reference: Extension of Harten-Lax-van Leer Scheme                    
    //            for Flows at All Speeds                                    
    //            By H. Luo, J. D. Baum, and R. Lohner                       
    //            (AIAA Journal - Vol. 43, No. 6, June 2005)                 
    // ---------------------------------------------------------------------

    // Initialization
    gam1 = gam - 1.;
    rnorm = sqrt(enormal[0] * enormal[0] + enormal[1] * enormal[1] + enormal[2] * enormal[2]);
    invnorm = 1. / rnorm;
    normal[0] = enormal[0] * invnorm;
    normal[1] = enormal[1] * invnorm;
    normal[2] = enormal[2] * invnorm;
    vitno = evitno * invnorm;

    sollft[0] = ug[0];
    sollft[1] = ug[1];
    sollft[2] = ug[2];
    sollft[3] = ug[3];
    sollft[4] = ug[4];
    sollft[5] = (ug[4] + gam * pstiff) / gam1 + ug[0] * .5 * (ug[1] * ug[1] +
                 ug[2] * ug[2] + ug[3] * ug[3]);

    solrgt[0] = ud[0];
    solrgt[1] = ud[1];
    solrgt[2] = ud[2];
    solrgt[3] = ud[3];
    solrgt[4] = ud[4];
    solrgt[5] = (ud[4] + gam * pstiff) / gam1 + ud[0] * .5 * (ud[1] * ud[1] +
                 ud[2] * ud[2] + ud[3] * ud[3]);

    // Celerity and normal component of the velocity
    clft = sqrt(abs(gam * (sollft[4] + pstiff) / sollft[0]));
    crgt = sqrt(abs(gam * (solrgt[4] + pstiff) / solrgt[0]));
    vnlft = normal[0] * sollft[1] + normal[1] * sollft[2] + normal[2] * sollft[3];
    vnrgt = normal[0] * solrgt[1] + normal[1] * solrgt[2] + normal[2] * solrgt[3];

    // Roe averaging
    rholft = sqrt(sollft[0]);
    rhorgt = sqrt(solrgt[0]);
    hlft = (sollft[5] + sollft[4]) / sollft[0];
    hrgt = (solrgt[5] + solrgt[4]) / solrgt[0];
    rhoinv = 1.f / (rholft + rhorgt);
    rholft *= rhoinv;
    rhorgt *= rhoinv;
    roemoy[1] = rholft * sollft[1] + rhorgt * solrgt[1];
    // u tilde
    roemoy[2] = rholft * sollft[2] + rhorgt * solrgt[2];
    // v tilde
    roemoy[3] = rholft * sollft[3] + rhorgt * solrgt[3];
    // w tilde
    roemoy[4] = rholft * hlft + rhorgt * hrgt;
    // H tilde
    qroe = (roemoy[1] * roemoy[1] + roemoy[2] * roemoy[2] + roemoy[3] * roemoy[3]) * .5;

    // H-q = (1/rho) (E + p) - Ec/rho  ==> p = rho*gam1/gam (H-q)
    // q=.5 |u|^2=Ec/rh
    croe = sqrt(abs(gam1 * (roemoy[4] - qroe)));
    // c tilde
    vnroe = normal[0] * roemoy[1] + normal[1] * roemoy[2] + normal[2] * roemoy[3];

    // Computing the preconditioning coefficient
    croe2 = croe * croe;
    if(prec == 0) {
        beta = 1.;
    }
    else {
        // local Preconditioning (ARL)
        shock = (abs(ug[4] - ud[4])) / (ug[4] + ud[4]) / length;
        locmach = sqrt(qroe * 2. / croe2 + std::numeric_limits<double>::epsilon());
        // Computing MAX
        beta = std::max(Scalar(k1 * locmach), Scalar(mach));
        beta = (sqrt(irey + std::numeric_limits<double>::epsilon()) + 1.) * beta + shockreducer * shock;
        beta = std::min(beta, Scalar(cmach));
    }
    beta2 = beta * beta;

    // Wave speed SLft, Srgt and SStar if necessary, with preconditioning if necessary
    // Computing the preconditioned eigenvalues
    d__1 = (1. - beta2) * vnlft;
    vplft = ((beta2 + 1.) * vnlft - sqrt(d__1 * d__1 + beta2 * 4. * (clft * clft))) * .5;
    d__1 = (1. - beta2) * vnroe;
    vplftroe = ((beta2 + 1.) * vnroe - sqrt(d__1 * d__1 + beta2 * 4. * (croe * croe))) * .5;

    d__1 = (1. - beta2) * vnrgt;
    vprgt = ((beta2 + 1.) * vnrgt + sqrt(d__1 * d__1 + beta2 * 4. * (crgt * crgt))) * .5;
    d__1 = (1. - beta2) * vnroe;
    vprgtroe = ((beta2 + 1.) * vnroe + sqrt(d__1 * d__1 + beta2 * 4. * (croe * croe))) * .5;
    // Computing the signal velocities
    slft = std::min(vplft, vplftroe);
    srgt = std::max(vprgt, vprgtroe);

    // Flft : left flux by splitting
    if(slft - vitno >= 0.) {
        flu[0] = sollft[0] * (vnlft - vitno);
        flu[1] = flu[0] * sollft[1] + sollft[4] * normal[0];
        flu[2] = flu[0] * sollft[2] + sollft[4] * normal[1];
        flu[3] = flu[0] * sollft[3] + sollft[4] * normal[2];
        flu[4] = sollft[5] * (vnlft - vitno) + sollft[4] * vnlft;
        phi[0] = flu[0] * 2.;
        phi[1] = flu[1] * 2.;
        phi[2] = flu[2] * 2.;
        phi[3] = flu[3] * 2.;
        phi[4] = flu[4] * 2.;
    }

    // Frgt : right flux by splitting
    else if(srgt - vitno <= 0.) {
        flu[0] = solrgt[0] * (vnrgt - vitno);
        flu[1] = flu[0] * solrgt[1] + solrgt[4] * normal[0];
        flu[2] = flu[0] * solrgt[2] + solrgt[4] * normal[1];
        flu[3] = flu[0] * solrgt[3] + solrgt[4] * normal[2];
        flu[4] = solrgt[5] * (vnrgt - vitno) + solrgt[4] * vnrgt;
        phi[0] = flu[0] * 2.;
        phi[1] = flu[1] * 2.;
        phi[2] = flu[2] * 2.;
        phi[3] = flu[3] * 2.;
        phi[4] = flu[4] * 2.;
    }
    else {
        // Calcul de SStar, contact wave speed
        sstar = solrgt[4] - sollft[4] + vnlft * sollft[0] * (slft - vnlft) -
                vnrgt * solrgt[0] * (srgt - vnrgt);

        sstar /= sollft[0] * (slft - vnlft) - solrgt[0] * (srgt - vnrgt);

        if(sstar - vitno >= 0.) {
            pstar = sollft[0] * (slft - vnlft) * (sstar - vnlft) + sollft[4];
            ustar[0] = sollft[0] * (slft - vnlft) / (slft - sstar);
            ustar[1] = ustar[0] * (sollft[1] + (sstar - vnlft) * normal[0]);
            ustar[2] = ustar[0] * (sollft[2] + (sstar - vnlft) * normal[1]);
            ustar[3] = ustar[0] * (sollft[3] + (sstar - vnlft) * normal[2]);
            ustar[4] = ustar[0] * (sollft[5] / sollft[0] + (sstar - vnlft) * (
                    sstar + sollft[4] / (sollft[0] * (slft - vnlft))));
            vnstar = (ustar[1] * normal[0] + ustar[2] * normal[1] + ustar[3] *
                    normal[2]) / ustar[0];

            flu[0] = ustar[0] * (vnstar - vitno);
            flu[1] = ustar[1] * (vnstar - vitno) + pstar * normal[0];
            flu[2] = ustar[2] * (vnstar - vitno) + pstar * normal[1];
            flu[3] = ustar[3] * (vnstar - vitno) + pstar * normal[2];
            flu[4] = ustar[4] * (vnstar - vitno) + pstar * vnstar;
            phi[0] = flu[0] * 2.;
            phi[1] = flu[1] * 2.;
            phi[2] = flu[2] * 2.;
            phi[3] = flu[3] * 2.;
            phi[4] = flu[4] * 2.;
        }
        else {
            pstar = solrgt[0] * (srgt - vnrgt) * (sstar - vnrgt) + solrgt[4];
            ustar[0] = solrgt[0] * (srgt - vnrgt) / (srgt - sstar);
            ustar[1] = ustar[0] * (solrgt[1] + (sstar - vnrgt) * normal[0]);
            ustar[2] = ustar[0] * (solrgt[2] + (sstar - vnrgt) * normal[1]);
            ustar[3] = ustar[0] * (solrgt[3] + (sstar - vnrgt) * normal[2]);
            ustar[4] = ustar[0] * (solrgt[5] / solrgt[0] + (sstar - vnrgt) * (
                    sstar + solrgt[4] / (solrgt[0] * (srgt - vnrgt))));
            vnstar = (ustar[1] * normal[0] + ustar[2] * normal[1] + ustar[3] *
                    normal[2]) / ustar[0];
            flu[0] = ustar[0] * (vnstar - vitno);
            flu[1] = ustar[1] * (vnstar - vitno) + pstar * normal[0];
            flu[2] = ustar[2] * (vnstar - vitno) + pstar * normal[1];
            flu[3] = ustar[3] * (vnstar - vitno) + pstar * normal[2];
            flu[4] = ustar[4] * (vnstar - vitno) + pstar * vnstar;
            phi[0] = flu[0] * 2.;
            phi[1] = flu[1] * 2.;
            phi[2] = flu[2] * 2.;
            phi[3] = flu[3] * 2.;
            phi[4] = flu[4] * 2.;
        }
    }

    return (0.5 * rnorm) * phi;
  }
};

void hllcjac5(int type, double gamma, double gam, double pstiff, double enormal[3],
              double evitno, double *ug, double *ud, double *jac, double mach, double k1,
              double cmach, double shockreducer, double irey, double length, int prec) {
  Eigen::Array<int, 1, 1> iconst;
  iconst << prec;
  Eigen::Array<double, 18, 1> sconst;
  sconst << gamma, gam, pstiff, enormal[0], enormal[1], enormal[2], evitno, mach, k1,
            cmach, shockreducer, irey, length, ud[0], ud[1], ud[2], ud[3], ud[4];
  Eigen::Matrix<double, 5, 1> v = Eigen::Map<Eigen::Matrix<double, 5, 1>>(ug);
  Jacobian<double, HLLCFlux5Function> df5dv(sconst, iconst);
  Eigen::Map<Eigen::Matrix<double, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor>> J(jac, 5 + type, 5 + type);
  switch(type) {
    case 0 :
      J = df5dv(v, 0.);
      break;
    case 1 :
      J.topLeftCorner(5, 5) = df5dv(v, 0.);
      J.bottomLeftCorner(1, 5).setZero();
      J.topRightCorner(5, 1).setZero();
      J.bottomRightCorner(1, 1).setZero();
      break;
    case 2 :
      J.topLeftCorner(5, 5) = df5dv(v, 0.);
      J.bottomLeftCorner(2, 5).setZero();
      J.topRightCorner(5, 2).setZero();
      J.bottomRightCorner(2, 2).setZero();
      break;
  }
}

#endif

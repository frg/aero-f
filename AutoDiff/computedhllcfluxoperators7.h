#ifndef _COMPUTEDHLLCFLUXOPERATORS7_H_
#define _COMPUTEDHLLCFLUXOPERATORS7_H_

#include <algorithm>
#include <cmath>
#include <limits>
#include <Eigen/Core>
#include <AutoDiff/Function.h>
#include <AutoDiff/SpaceDerivatives.h>

template<typename Scalar>
class HLLCFlux7Function2 : public VectorValuedFunction<21, 7, Scalar, 6, 1, double> {
  const Eigen::Array<double, 6, 1>& sconst;
  const Eigen::Array<int, 1, 1>& iconst;

 public:
  HLLCFlux7Function2(const Eigen::Array<double, 6, 1>& _sconst, const Eigen::Array<int, 1, 1>& _iconst)
    : sconst(_sconst), iconst(_iconst) {}

  Eigen::Matrix<Scalar, 7, 1> operator()(const Eigen::Matrix<Scalar, 21, 1>& q, Scalar) {
    const double& gamma = sconst[0];
    const double& gam = sconst[1];
    const double& k1 = sconst[2];
    const double& cmach = sconst[3];
    const double& shockreducer = sconst[4];
    const double& length = sconst[5];
    const int& prec = iconst[0];
    const Scalar& pstiff = q[0];
    const Scalar *enormal = &q[1];
    const Scalar& evitno = q[4];
    const Scalar *ug = &q[5];
    const Scalar *ud = &q[12];
    const Scalar& mach = q[19];
    const Scalar& irey = q[20];
    Eigen::Matrix<Scalar, 7, 1> phi;

    // System generated locals
    Scalar d__1;

    // Builtin functions
    using std::sqrt;
    using std::abs;

    // Local variables
    double gam1;
    Scalar rnorm, invnorm, normal[3], vitno, solrgt[7], crgt, vnrgt, hrgt,
           vplftroe, vprgtroe, flu[6], beta, clft, croe, hlft, qroe, slft,
           srgt, beta2, croe2, updir, vnlft, vnroe, vplft, pstar, sstar,
           ustar[6], vprgt, rholft, rhorgt, sollft[7], rhoinv, roemoy[6],
           vnstar, shock, locmach;

    // --------------------------------------------------------------------- 
    // Computes the convective fluxes using the approximate Riemann          
    // solver of HLLC with optional low Mach preconditioning                 
    // Reference: Extension of Harten-Lax-van Leer Scheme                    
    //            for Flows at All Speeds                                    
    //            By H. Luo, J. D. Baum, and R. Lohner                       
    //            (AIAA Journal - Vol. 43, No. 6, June 2005)                 
    // ---------------------------------------------------------------------

    // Initialization
    gam1 = gam - 1.;
    rnorm = sqrt(enormal[0] * enormal[0] + enormal[1] * enormal[1] + enormal[2] * enormal[2]);
    invnorm = 1. / rnorm;
    normal[0] = enormal[0] * invnorm;
    normal[1] = enormal[1] * invnorm;
    normal[2] = enormal[2] * invnorm;
    vitno = evitno * invnorm;

    sollft[0] = ug[0];
    sollft[1] = ug[1];
    sollft[2] = ug[2];
    sollft[3] = ug[3];
    sollft[4] = ug[4];
    sollft[5] = (ug[4] + gam * pstiff) / gam1 + ug[0] * .5 * (ug[1] * ug[1] +
                 ug[2] * ug[2] + ug[3] * ug[3]);

    solrgt[0] = ud[0];
    solrgt[1] = ud[1];
    solrgt[2] = ud[2];
    solrgt[3] = ud[3];
    solrgt[4] = ud[4];
    solrgt[5] = (ud[4] + gam * pstiff) / gam1 + ud[0] * .5 * (ud[1] * ud[1] +
                 ud[2] * ud[2] + ud[3] * ud[3]);

    // Celerity and normal component of the velocity
    clft = sqrt(abs(gam * (sollft[4] + pstiff) / sollft[0]));
    crgt = sqrt(abs(gam * (solrgt[4] + pstiff) / solrgt[0]));
    vnlft = normal[0] * sollft[1] + normal[1] * sollft[2] + normal[2] * sollft[3];
    vnrgt = normal[0] * solrgt[1] + normal[1] * solrgt[2] + normal[2] * solrgt[3];

    // Roe averaging
    rholft = sqrt(sollft[0]);
    rhorgt = sqrt(solrgt[0]);
    hlft = (sollft[5] + sollft[4]) / sollft[0];
    hrgt = (solrgt[5] + solrgt[4]) / solrgt[0];
    rhoinv = 1.f / (rholft + rhorgt);
    rholft *= rhoinv;
    rhorgt *= rhoinv;
    roemoy[1] = rholft * sollft[1] + rhorgt * solrgt[1];
    // u tilde
    roemoy[2] = rholft * sollft[2] + rhorgt * solrgt[2];
    // v tilde
    roemoy[3] = rholft * sollft[3] + rhorgt * solrgt[3];
    // w tilde
    roemoy[4] = rholft * hlft + rhorgt * hrgt;
    // H tilde
    qroe = (roemoy[1] * roemoy[1] + roemoy[2] * roemoy[2] + roemoy[3] * roemoy[3]) * .5;

    // H-q = (1/rho) (E + p) - Ec/rho  ==> p = rho*gam1/gam (H-q)
    // q=.5 |u|^2=Ec/rh
    croe = sqrt(abs(gam1 * (roemoy[4] - qroe)));
    // c tilde
    vnroe = normal[0] * roemoy[1] + normal[1] * roemoy[2] + normal[2] * roemoy[3];

    // Computing the preconditioning coefficient
    croe2 = croe * croe;
    if(prec == 0) {
        beta = 1.;
    }
    else {
        // local Preconditioning (ARL)
        shock = (abs(ug[4] - ud[4])) / (ug[4] + ud[4]) / length;
        locmach = sqrt(qroe * 2. / croe2 + std::numeric_limits<double>::epsilon());
        // Computing MAX
        beta = std::max(Scalar(k1 * locmach), Scalar(mach));
        beta = (sqrt(irey + std::numeric_limits<double>::epsilon()) + 1.) * beta + shockreducer * shock;
        beta = std::min(beta, Scalar(cmach));
    }
    beta2 = beta * beta;

    // Wave speed SLft, Srgt and SStar if necessary, with preconditioning if necessary
    // Computing the preconditioned eigenvalues
    d__1 = (1. - beta2) * vnlft;
    vplft = ((beta2 + 1.) * vnlft - sqrt(d__1 * d__1 + beta2 * 4. * (clft * clft))) * .5;
    d__1 = (1. - beta2) * vnroe;
    vplftroe = ((beta2 + 1.) * vnroe - sqrt(d__1 * d__1 + beta2 * 4. * (croe * croe))) * .5;

    d__1 = (1. - beta2) * vnrgt;
    vprgt = ((beta2 + 1.) * vnrgt + sqrt(d__1 * d__1 + beta2 * 4. * (crgt * crgt))) * .5;
    d__1 = (1. - beta2) * vnroe;
    vprgtroe = ((beta2 + 1.) * vnroe + sqrt(d__1 * d__1 + beta2 * 4. * (croe * croe))) * .5;
    // Computing the signal velocities
    slft = std::min(vplft, vplftroe);
    srgt = std::max(vprgt, vprgtroe);

    // Flft : left flux by splitting
    if(slft - vitno >= 0.) {
        flu[0] = sollft[0] * (vnlft - vitno);
        flu[1] = flu[0] * sollft[1] + sollft[4] * normal[0];
        flu[2] = flu[0] * sollft[2] + sollft[4] * normal[1];
        flu[3] = flu[0] * sollft[3] + sollft[4] * normal[2];
        flu[4] = sollft[5] * (vnlft - vitno) + sollft[4] * vnlft;
        phi[0] = flu[0] * 2.;
        phi[1] = flu[1] * 2.;
        phi[2] = flu[2] * 2.;
        phi[3] = flu[3] * 2.;
        phi[4] = flu[4] * 2.;
    }

    // Frgt : right flux by splitting
    else if(srgt - vitno <= 0.) {
        flu[0] = solrgt[0] * (vnrgt - vitno);
        flu[1] = flu[0] * solrgt[1] + solrgt[4] * normal[0];
        flu[2] = flu[0] * solrgt[2] + solrgt[4] * normal[1];
        flu[3] = flu[0] * solrgt[3] + solrgt[4] * normal[2];
        flu[4] = solrgt[5] * (vnrgt - vitno) + solrgt[4] * vnrgt;
        phi[0] = flu[0] * 2.;
        phi[1] = flu[1] * 2.;
        phi[2] = flu[2] * 2.;
        phi[3] = flu[3] * 2.;
        phi[4] = flu[4] * 2.;
    }
    else {
        // Calcul de SStar, contact wave speed
        sstar = solrgt[4] - sollft[4] + vnlft * sollft[0] * (slft - vnlft) -
                vnrgt * solrgt[0] * (srgt - vnrgt);

        sstar /= sollft[0] * (slft - vnlft) - solrgt[0] * (srgt - vnrgt);

        if(sstar - vitno >= 0.) {
            pstar = sollft[0] * (slft - vnlft) * (sstar - vnlft) + sollft[4];
            ustar[0] = sollft[0] * (slft - vnlft) / (slft - sstar);
            ustar[1] = ustar[0] * (sollft[1] + (sstar - vnlft) * normal[0]);
            ustar[2] = ustar[0] * (sollft[2] + (sstar - vnlft) * normal[1]);
            ustar[3] = ustar[0] * (sollft[3] + (sstar - vnlft) * normal[2]);
            ustar[4] = ustar[0] * (sollft[5] / sollft[0] + (sstar - vnlft) * (
                    sstar + sollft[4] / (sollft[0] * (slft - vnlft))));
            vnstar = (ustar[1] * normal[0] + ustar[2] * normal[1] + ustar[3] *
                    normal[2]) / ustar[0];

            flu[0] = ustar[0] * (vnstar - vitno);
            flu[1] = ustar[1] * (vnstar - vitno) + pstar * normal[0];
            flu[2] = ustar[2] * (vnstar - vitno) + pstar * normal[1];
            flu[3] = ustar[3] * (vnstar - vitno) + pstar * normal[2];
            flu[4] = ustar[4] * (vnstar - vitno) + pstar * vnstar;
            phi[0] = flu[0] * 2.;
            phi[1] = flu[1] * 2.;
            phi[2] = flu[2] * 2.;
            phi[3] = flu[3] * 2.;
            phi[4] = flu[4] * 2.;
        }
        else {
            pstar = solrgt[0] * (srgt - vnrgt) * (sstar - vnrgt) + solrgt[4];
            ustar[0] = solrgt[0] * (srgt - vnrgt) / (srgt - sstar);
            ustar[1] = ustar[0] * (solrgt[1] + (sstar - vnrgt) * normal[0]);
            ustar[2] = ustar[0] * (solrgt[2] + (sstar - vnrgt) * normal[1]);
            ustar[3] = ustar[0] * (solrgt[3] + (sstar - vnrgt) * normal[2]);
            ustar[4] = ustar[0] * (solrgt[5] / solrgt[0] + (sstar - vnrgt) * (
                    sstar + solrgt[4] / (solrgt[0] * (srgt - vnrgt))));
            vnstar = (ustar[1] * normal[0] + ustar[2] * normal[1] + ustar[3] *
                    normal[2]) / ustar[0];
            flu[0] = ustar[0] * (vnstar - vitno);
            flu[1] = ustar[1] * (vnstar - vitno) + pstar * normal[0];
            flu[2] = ustar[2] * (vnstar - vitno) + pstar * normal[1];
            flu[3] = ustar[3] * (vnstar - vitno) + pstar * normal[2];
            flu[4] = ustar[4] * (vnstar - vitno) + pstar * vnstar;
            phi[0] = flu[0] * 2.;
            phi[1] = flu[1] * 2.;
            phi[2] = flu[2] * 2.;
            phi[3] = flu[3] * 2.;
            phi[4] = flu[4] * 2.;
        }
    }

    updir = (phi[0] < 0.) ? -0.5 : 0.5;
    phi[5] = phi[0] * (updir * ug[5] + (1. - updir) * ud[5]);
    phi[6] = phi[0] * (updir * ug[6] + (1. - updir) * ud[6]);

    return (0.5 * rnorm) * phi;
  }
};

void computedhllcfluxoperators7(int type, double gamma, double gam, double pstiff, double enormal[3],
                                double evitno, double *ug, double *ud, double mach, double k1,
                                double cmach, double shockreducer, double irey, double length, int prec,
                                double *dphidenormal, double *dphidevitno, double *dphidug, double *dphidud,
                                double *dphidpstiff) {
  Eigen::Array<int, 1, 1> iconst;
  Eigen::Array<double, 6, 1> sconst;
  iconst << prec;
  sconst << gamma, gam, k1, cmach, shockreducer, length;
  Eigen::Matrix<double, 21, 1> v;
  v << pstiff, enormal[0], enormal[1], enormal[2], evitno,
       Eigen::Map<Eigen::Matrix<double, 7, 1>>(ug),
       Eigen::Map<Eigen::Matrix<double, 7, 1>>(ud),
       mach, irey;
  Jacobian<double, HLLCFlux7Function2> df5dv(sconst, iconst);
  Eigen::Matrix<double, 7, 21> J = df5dv(v, 0.);
  Eigen::Map<Eigen::Matrix<double, 7, 1>> d1(dphidpstiff);                   d1 = J.col(0);
  Eigen::Map<Eigen::Matrix<double, 7, 3, Eigen::RowMajor>> d2(dphidenormal); d2 = J.block<7, 3>(0, 1);
  Eigen::Map<Eigen::Matrix<double, 7, 1>> d3(dphidevitno);                   d3 = J.col(4);
  Eigen::Map<Eigen::Matrix<double, 7, 7, Eigen::RowMajor>> d4(dphidug);      d4 = J.block<7, 7>(0, 5);
  Eigen::Map<Eigen::Matrix<double, 7, 7, Eigen::RowMajor>> d5(dphidud);      d5 = J.block<7, 7>(0, 12);
}

void gxhllcflux7(int type, double gamma, double gam, double pstiff, double dpstiff, double *enormal, double *denormal,
                 double evitno, double devitno, double *ug, double *dug, double *ud, double *dud, double *phi, double *dphi,
                 double mach, double dmach, double k1, double cmach, double shockreducer, double irey, double direy,
                 double length, int prec) {
  Eigen::Array<int, 1, 1> iconst;
  Eigen::Array<double, 6, 1> sconst;
  iconst << prec;
  sconst << gamma, gam, k1, cmach, shockreducer, length;
  typedef Eigen::AutoDiffScalar<Eigen::Matrix<double, 1, 1>> Scalar;
  Eigen::Matrix<double, 21, 1> v, dv;
  v << pstiff, enormal[0], enormal[1], enormal[2], evitno,
       Eigen::Map<Eigen::Matrix<double, 7, 1>>(ug),
       Eigen::Map<Eigen::Matrix<double, 7, 1>>(ud),
       mach, irey;
  dv << dpstiff, denormal[0], denormal[1], denormal[2], devitno,
        Eigen::Map<Eigen::Matrix<double, 7, 1>>(dug),
        Eigen::Map<Eigen::Matrix<double, 7, 1>>(dud),
        dmach, direy;
  Eigen::Matrix<Scalar, 21, 1> v_;
  for(int i = 0; i < 21; ++i) {
    v_[i].value() = v[i];
    v_[i].derivatives()[0] = dv[i];
  }
  HLLCFlux7Function2<Scalar> f(sconst, iconst);
  Eigen::Matrix<Scalar, 7, 1> phi_ = f(v_, Scalar(0.));
  for(int i = 0; i < 7; ++i) {
    phi[i] = phi_[i].value();
    dphi[i] = phi_[i].derivatives()[0];
  }
}

#endif

#ifndef _RESTART_STATE_
#define _RESTART_STATE_

#include <Types.h>
#include <Vector.h>

template<int dim>
class RestartState {
 public:
  SVec<double, dim> *Un;
  SVec<double, dim> *Unm1;
  SVec<double, dim> *Unm2;
  SVec<double, dim> *U0;
  SVec<double, dim> *Unmp;
  SVec<double, dim> *Unmp1;
  SVec<double, dim> *Unmp2;
  SVec<double, dim> *dU;
  SVec<double, dim> *dUPrev;
  SVec<double, 3> *X;
  SVec<double, 3> *Xn;
  SVec<double, 3> *Xnm1;
  SVec<double, 3> *Xnm2;
  SVec<double, 3> *Xsave;
  SVec<double, 3> *Xs;
  Vec<double> *d2wall;
  Vec<double> *d2walln;
  Vec<double> *d2wallnm1;
  Vec<double> *d2wallnm2;
  Vec<double> *d2wallnmp;
  Vec<double> *d2bfwall;
  Vec<int> *status;
  Vec<bool> *is_occluded;
  Vec<bool> *is_active;
  Vec<int> *status_nmp;
  Vec<bool> *is_occluded_nmp;
  Vec<bool> *ntag;
  SVec<double, dim> *limiterphi;
  SVec<double, dimLS> *Phi;
  SVec<double, dimLS> *Phin;
  SVec<double, dimLS> *Phinm1;
  SVec<double, dimLS> *Phinm2;
  Vec<int> *fluidId;
  Vec<int> *fluidIdn;
  Vec<int> *fluidIdnm1;
  Vec<int> *fluidIdnm2;
  SVec<double, 3> *modes;
  Vec<double> *AvQs0, *AvQs1, *AvQs2, *AvQs3, *AvQs4, *AvQs5, *AvQs6, *AvQs7, *AvQs8, *AvQs9,
              *AvQs10, *AvQs11, *AvQs12, *AvQs13, *AvQs14, *AvQs15, *AvQs16, *AvQs17, *AvQs18;
  SVec<double, 3> *AvQv0, *AvQv1, *AvQv2, *AvQv3, *AvQv4, *AvQv5;
  SVec<double, 6> *AvQt0, *AvQt1, *AvQt2;
  Vec<double> *dists;
  SVec<double, 3> *dirs;
  SVec<double, 9> *mets;
  SVec<double, 1> *refScalar0, *refScalar1, *refScalar2, *refScalar3, *refScalar4, *refScalar5, *refScalar6, *refScalar7;
  SVec<double, 1> *ddx0, *ddx1, *ddx2, *ddx3, *ddx4, *ddx5, *ddx6, *ddx7;
  SVec<double, 1> *ddy0, *ddy1, *ddy2, *ddy3, *ddy4, *ddy5, *ddy6, *ddy7;
  SVec<double, 1> *ddz0, *ddz1, *ddz2, *ddz3, *ddz4, *ddz5, *ddz6, *ddz7;

  int getIntPacketSize() const {
    return (status ? 1 : 0) + (is_occluded ? 1 : 0) + (is_active ? 1 : 0)
           + (status_nmp ? 1 : 0) + (is_occluded_nmp ? 1 : 0) + (ntag ? 1 : 0) + (fluidId ? 1 : 0) + (fluidIdn ? 1 : 0)
           + (fluidIdnm1 ? 1 : 0) + (fluidIdnm2 ? 1 : 0);
  }

  int getDblPacketSize() const {
    return dim + (Unm1 ? dim : 0) + (Unm2 ? dim : 0) + (U0 ? dim : 0) + (Unmp ? dim : 0) + (Unmp1 ? dim : 0) + (Unmp2 ? dim : 0)
               + (dU ? dim : 0) + (dUPrev ? dim : 0) + (X ? 3 : 0) + (Xn ? 3 : 0)
               + (Xnm1 ? 3 : 0) + (Xnm2 ? 3 : 0) + (Xsave ? 3 : 0) + (Xs ? 3 : 0) + (d2wall ? 1 : 0) + (d2walln ? 1 : 0) + (d2wallnm1 ? 1 : 0)
               + (d2wallnm2 ? 1 : 0) + (d2wallnmp ? 1 : 0) + (d2bfwall ? 1 : 0) + (limiterphi ? dim : 0) + (Phi ? Phi->dim() : 0)
               + (Phin ? Phin->dim() : 0) + (Phinm1 ? Phinm1->dim() : 0) + (Phinm2 ? Phinm2->dim() : 0)
               + (modes ? 3 : 0) + (AvQs0 ? 1 : 0) + (AvQs1 ? 1 : 0) + (AvQs2 ? 1 : 0)
               + (AvQs3 ? 1 : 0) + (AvQs4 ? 1 : 0) + (AvQs5 ? 1 : 0) + (AvQs6 ? 1 : 0)
               + (AvQs7 ? 1 : 0) + (AvQs8 ? 1 : 0) + (AvQs9 ? 1 : 0) + (AvQs10 ? 1 : 0)
               + (AvQs11 ? 1 : 0) + (AvQs12 ? 1 : 0) + (AvQs13 ? 1 : 0) + (AvQs14 ? 1 : 0)
               + (AvQs15 ? 1 : 0) + (AvQs16 ? 1 : 0) + (AvQs17 ? 1 : 0) + (AvQs18 ? 1 : 0)
               + (AvQv0 ? 3 : 0) + (AvQv1 ? 3 : 0) + (AvQv2 ? 3 : 0) + (AvQv3 ? 3 : 0) + (AvQv4 ? 3 : 0) + (AvQv5 ? 3 : 0)
               + (AvQt0 ? 6 : 0) + (AvQt1 ? 6 : 0) + (AvQt2 ? 6 : 0) + (dists ? 1 : 0) + (dirs ? 3 : 0) + (mets ? 9 : 0)
               + (refScalar0 ? 1 : 0) + (refScalar1 ? 1 : 0) + (refScalar2 ? 1 : 0) + (refScalar3 ? 1 : 0) + (refScalar4 ? 1 : 0)
               + (refScalar5 ? 1 : 0) + (refScalar6 ? 1 : 0) + (refScalar7 ? 1 : 0) + (ddx0 ? 1 : 0) + (ddx1 ? 1 : 0)
               + (ddx2 ? 1 : 0) + (ddx3 ? 1 : 0) + (ddx4 ? 1 : 0) + (ddx5 ? 1 : 0) + (ddx6 ? 1 : 0) + (ddx7 ? 1 : 0)
               + (ddy0 ? 1 : 0) + (ddy1 ? 1 : 0) + (ddy2 ? 1 : 0) + (ddy3 ? 1 : 0) + (ddy4 ? 1 : 0) + (ddy5 ? 1 : 0)
               + (ddy6 ? 1 : 0) + (ddy7 ? 1 : 0) + (ddz0 ? 1 : 0) + (ddz1 ? 1 : 0) + (ddz2 ? 1 : 0) + (ddz3 ? 1 : 0)
               + (ddz4 ? 1 : 0) + (ddz5 ? 1 : 0) + (ddz6 ? 1 : 0) + (ddz7 ? 1 : 0);
  }

  int getIntNodalState(int node_index, int_t *data) const {
    int p = 0;
    if(status) data[p++] = (int_t)(*status)[node_index];
    if(is_occluded) data[p++] = (int_t)(*is_occluded)[node_index];
    if(is_active) data[p++] = (int_t)(*is_active)[node_index];
    if(status_nmp) data[p++] = (int_t)(*status_nmp)[node_index];
    if(is_occluded_nmp) data[p++] = (int_t)(*is_occluded_nmp)[node_index];
    if(ntag) data[p++] = (int_t)(*ntag)[node_index];
    if(fluidId) data[p++] = (int_t)(*fluidId)[node_index];
    if(fluidIdn) data[p++] = (int_t)(*fluidIdn)[node_index];
    if(fluidIdnm1) data[p++] = (int_t)(*fluidIdnm1)[node_index];
    if(fluidIdnm2) data[p++] = (int_t)(*fluidIdnm2)[node_index];
    return p;
  }

  int getDblNodalState(int node_index, double *data) const {
    int p = 0;
    for(int i = 0; i < dim; ++i) data[p++] = (*Un)[node_index][i];
    if(Unm1) for(int i = 0; i < dim; ++i) data[p++] = (*Unm1)[node_index][i];
    if(Unm2) for(int i = 0; i < dim; ++i) data[p++] = (*Unm2)[node_index][i];
    if(U0) for(int i = 0; i < dim; ++i) data[p++] = (*U0)[node_index][i];
    if(Unmp) for(int i = 0; i < dim; ++i) data[p++] = (*Unmp)[node_index][i];
    if(Unmp1) for(int i = 0; i < dim; ++i) data[p++] = (*Unmp1)[node_index][i];
    if(Unmp2) for(int i = 0; i < dim; ++i) data[p++] = (*Unmp2)[node_index][i];
    if(dU) for(int i = 0; i < dim; ++i) data[p++] = (*dU)[node_index][i];
    if(dUPrev) for(int i = 0; i < dim; ++i) data[p++] = (*dUPrev)[node_index][i];
    if(X) for(int i = 0; i < 3; ++i) data[p++] = (*X)[node_index][i];
    if(Xn) for(int i = 0; i < 3; ++i) data[p++] = (*Xn)[node_index][i];
    if(Xnm1) for(int i = 0; i < 3; ++i) data[p++] = (*Xnm1)[node_index][i];
    if(Xnm2) for(int i = 0; i < 3; ++i) data[p++] = (*Xnm2)[node_index][i];
    if(Xsave) for(int i = 0; i < 3; ++i) data[p++] = (*Xsave)[node_index][i];
    if(Xs) for(int i = 0; i < 3; ++i) data[p++] = (*Xs)[node_index][i];
    if(d2wall) data[p++] = (*d2wall)[node_index];
    if(d2walln) data[p++] = (*d2walln)[node_index];
    if(d2wallnm1) data[p++] = (*d2wallnm1)[node_index];
    if(d2wallnm2) data[p++] = (*d2wallnm2)[node_index];
    if(d2wallnmp) data[p++] = (*d2wallnmp)[node_index];
    if(d2bfwall) data[p++] = (*d2bfwall)[node_index];
    if(limiterphi) for(int i = 0; i < dim; ++i) data[p++] = (*limiterphi)[node_index][i];
    if(Phi) for(int i = 0; i < Phi->dim(); ++i) data[p++] = (*Phi)[node_index][i];
    if(Phin) for(int i = 0; i < Phin->dim(); ++i) data[p++] = (*Phin)[node_index][i];
    if(Phinm1) for(int i = 0; i < Phinm1->dim(); ++i) data[p++] = (*Phinm1)[node_index][i];
    if(Phinm2) for(int i = 0; i < Phinm2->dim(); ++i) data[p++] = (*Phinm2)[node_index][i];
    if(modes) for(int i = 0; i < 3; ++i) data[p++] = (*modes)[node_index][i];
    if(AvQs0) data[p++] = (*AvQs0)[node_index];
    if(AvQs1) data[p++] = (*AvQs1)[node_index];
    if(AvQs2) data[p++] = (*AvQs2)[node_index];
    if(AvQs3) data[p++] = (*AvQs3)[node_index];
    if(AvQs4) data[p++] = (*AvQs4)[node_index];
    if(AvQs5) data[p++] = (*AvQs5)[node_index];
    if(AvQs6) data[p++] = (*AvQs6)[node_index];
    if(AvQs7) data[p++] = (*AvQs7)[node_index];
    if(AvQs8) data[p++] = (*AvQs8)[node_index];
    if(AvQs9) data[p++] = (*AvQs9)[node_index];
    if(AvQs10) data[p++] = (*AvQs10)[node_index];
    if(AvQs11) data[p++] = (*AvQs11)[node_index];
    if(AvQs12) data[p++] = (*AvQs12)[node_index];
    if(AvQs13) data[p++] = (*AvQs13)[node_index];
    if(AvQs14) data[p++] = (*AvQs14)[node_index];
    if(AvQs15) data[p++] = (*AvQs15)[node_index];
    if(AvQs16) data[p++] = (*AvQs16)[node_index];
    if(AvQs17) data[p++] = (*AvQs17)[node_index];
    if(AvQs18) data[p++] = (*AvQs18)[node_index];
    if(AvQv0) for(int i = 0; i < 3; ++i) data[p++] = (*AvQv0)[node_index][i];
    if(AvQv1) for(int i = 0; i < 3; ++i) data[p++] = (*AvQv1)[node_index][i];
    if(AvQv2) for(int i = 0; i < 3; ++i) data[p++] = (*AvQv2)[node_index][i];
    if(AvQv3) for(int i = 0; i < 3; ++i) data[p++] = (*AvQv3)[node_index][i];
    if(AvQv4) for(int i = 0; i < 3; ++i) data[p++] = (*AvQv4)[node_index][i];
    if(AvQv5) for(int i = 0; i < 3; ++i) data[p++] = (*AvQv5)[node_index][i];
    if(AvQt0) for(int i = 0; i < 6; ++i) data[p++] = (*AvQt0)[node_index][i];
    if(AvQt1) for(int i = 0; i < 6; ++i) data[p++] = (*AvQt1)[node_index][i];
    if(AvQt2) for(int i = 0; i < 6; ++i) data[p++] = (*AvQt2)[node_index][i];
    if(dists) data[p++] = (*dists)[node_index];
    if(dirs) for(int i = 0; i < 3; ++i) data[p++] = (*dirs)[node_index][i];
    if(mets) for(int i = 0; i < 9; ++i) data[p++] = (*mets)[node_index][i];
    if(refScalar0) data[p++] = (*refScalar0)[node_index][0];
    if(refScalar1) data[p++] = (*refScalar1)[node_index][0];
    if(refScalar2) data[p++] = (*refScalar2)[node_index][0];
    if(refScalar3) data[p++] = (*refScalar3)[node_index][0];
    if(refScalar4) data[p++] = (*refScalar4)[node_index][0];
    if(refScalar5) data[p++] = (*refScalar5)[node_index][0];
    if(refScalar6) data[p++] = (*refScalar6)[node_index][0];
    if(refScalar7) data[p++] = (*refScalar7)[node_index][0];
    if(ddx0) data[p++] = (*ddx0)[node_index][0];
    if(ddx1) data[p++] = (*ddx1)[node_index][0];
    if(ddx2) data[p++] = (*ddx2)[node_index][0];
    if(ddx3) data[p++] = (*ddx3)[node_index][0];
    if(ddx4) data[p++] = (*ddx4)[node_index][0];
    if(ddx5) data[p++] = (*ddx5)[node_index][0];
    if(ddx6) data[p++] = (*ddx6)[node_index][0];
    if(ddx7) data[p++] = (*ddx7)[node_index][0];
    if(ddy0) data[p++] = (*ddy0)[node_index][0];
    if(ddy1) data[p++] = (*ddy1)[node_index][0];
    if(ddy2) data[p++] = (*ddy2)[node_index][0];
    if(ddy3) data[p++] = (*ddy3)[node_index][0];
    if(ddy4) data[p++] = (*ddy4)[node_index][0];
    if(ddy5) data[p++] = (*ddy5)[node_index][0];
    if(ddy6) data[p++] = (*ddy6)[node_index][0];
    if(ddy7) data[p++] = (*ddy7)[node_index][0];
    if(ddz0) data[p++] = (*ddz0)[node_index][0];
    if(ddz1) data[p++] = (*ddz1)[node_index][0];
    if(ddz2) data[p++] = (*ddz2)[node_index][0];
    if(ddz3) data[p++] = (*ddz3)[node_index][0];
    if(ddz4) data[p++] = (*ddz4)[node_index][0];
    if(ddz5) data[p++] = (*ddz5)[node_index][0];
    if(ddz6) data[p++] = (*ddz6)[node_index][0];
    if(ddz7) data[p++] = (*ddz7)[node_index][0];
    return p;
  }

  int setIntNodalState(int node_index, int_t *data) {
    int p = 0;
    if(status) { status->resize(node_index+1); (*status)[node_index] = (int)data[p++]; }
    if(is_occluded) { is_occluded->resize(node_index+1); (*is_occluded)[node_index] = (bool)data[p++]; }
    if(is_active) { is_active->resize(node_index+1); (*is_active)[node_index] = (bool)data[p++]; }
    if(status_nmp) { status_nmp->resize(node_index+1); (*status_nmp)[node_index] = (int)data[p++]; }
    if(is_occluded_nmp) { is_occluded_nmp->resize(node_index+1); (*is_occluded_nmp)[node_index] = (bool)data[p++]; }
    if(ntag) { ntag->resize(node_index+1); (*ntag)[node_index] = (bool)data[p++]; }
    if(fluidId) { fluidId->resize(node_index+1); (*fluidId)[node_index] = (int)data[p++]; }
    if(fluidIdn) { fluidIdn->resize(node_index+1); (*fluidIdn)[node_index] = (int)data[p++]; }
    if(fluidIdnm1) { fluidIdnm1->resize(node_index+1); (*fluidIdnm1)[node_index] = (int)data[p++]; }
    if(fluidIdnm2) { fluidIdnm2->resize(node_index+1); (*fluidIdnm2)[node_index] = (int)data[p++]; }
    return p;
  }

  int setDblNodalState(int node_index, double *data) {
    int p = 0;
    Un->resize(node_index+1); for(int i = 0; i < dim; ++i) (*Un)[node_index][i] = data[p++];
    if(Unm1) { Unm1->resize(node_index+1); for(int i = 0; i < dim; ++i) (*Unm1)[node_index][i] = data[p++]; }
    if(Unm2) { Unm2->resize(node_index+1); for(int i = 0; i < dim; ++i) (*Unm2)[node_index][i] = data[p++]; }
    if(U0) { U0->resize(node_index+1); for(int i = 0; i < dim; ++i) (*U0)[node_index][i] = data[p++]; }
    if(Unmp) { Unmp->resize(node_index+1); for(int i = 0; i < dim; ++i) (*Unmp)[node_index][i] = data[p++]; }
    if(Unmp1) { Unmp1->resize(node_index+1); for(int i = 0; i < dim; ++i) (*Unmp1)[node_index][i] = data[p++]; }
    if(Unmp2) { Unmp2->resize(node_index+1); for(int i = 0; i < dim; ++i) (*Unmp2)[node_index][i] = data[p++]; }
    if(dU) { dU->resize(node_index+1); for(int i = 0; i < dim; ++i) (*dU)[node_index][i] = data[p++]; }
    if(dUPrev) { dUPrev->resize(node_index+1); for(int i = 0; i < dim; ++i) (*dUPrev)[node_index][i] = data[p++]; }
    if(X) { X->resize(node_index+1); for(int i = 0; i < 3; ++i) (*X)[node_index][i] = data[p++]; }
    if(Xn) { Xn->resize(node_index+1); for(int i = 0; i < 3; ++i) (*Xn)[node_index][i] = data[p++]; }
    if(Xnm1) { Xnm1->resize(node_index+1); for(int i = 0; i < 3; ++i) (*Xnm1)[node_index][i] = data[p++]; }
    if(Xnm2) { Xnm2->resize(node_index+1); for(int i = 0; i < 3; ++i) (*Xnm2)[node_index][i] = data[p++]; }
    if(Xsave) { Xsave->resize(node_index+1); for(int i = 0; i < 3; ++i) (*Xsave)[node_index][i] = data[p++]; }
    if(Xs) { Xs->resize(node_index+1); for(int i = 0; i < 3; ++i) (*Xs)[node_index][i] = data[p++]; }
    if(d2wall) { d2wall->resize(node_index+1); (*d2wall)[node_index] = data[p++]; }
    if(d2walln) { d2walln->resize(node_index+1); (*d2walln)[node_index] = data[p++]; }
    if(d2wallnm1) { d2wallnm1->resize(node_index+1); (*d2wallnm1)[node_index] = data[p++]; }
    if(d2wallnm2) { d2wallnm2->resize(node_index+1); (*d2wallnm2)[node_index] = data[p++]; }
    if(d2wallnmp) { d2wallnmp->resize(node_index+1); (*d2wallnmp)[node_index] = data[p++]; }
    if(d2bfwall) { d2bfwall->resize(node_index+1); (*d2bfwall)[node_index] = data[p++]; }
    if(limiterphi) { limiterphi->resize(node_index+1); for(int i = 0; i < dim; ++i) (*limiterphi)[node_index][i] = data[p++]; }
    if(Phi) { Phi->resize(node_index+1); for(int i = 0; i < Phi->dim(); ++i) (*Phi)[node_index][i] = data[p++]; }
    if(Phin) { Phin->resize(node_index+1); for(int i = 0; i < Phin->dim(); ++i) (*Phin)[node_index][i] = data[p++]; }
    if(Phinm1) { Phinm1->resize(node_index+1); for(int i = 0; i < Phinm1->dim(); ++i) (*Phinm1)[node_index][i] = data[p++]; }
    if(Phinm2) { Phinm2->resize(node_index+1); for(int i = 0; i < Phinm2->dim(); ++i) (*Phinm2)[node_index][i] = data[p++]; }
    if(modes) { modes->resize(node_index+1); for(int i = 0; i < 3; ++i) (*modes)[node_index][i] = data[p++]; }
    if(AvQs0) { AvQs0->resize(node_index+1); (*AvQs0)[node_index] = data[p++]; }
    if(AvQs1) { AvQs1->resize(node_index+1); (*AvQs1)[node_index] = data[p++]; }
    if(AvQs2) { AvQs2->resize(node_index+1); (*AvQs2)[node_index] = data[p++]; }
    if(AvQs3) { AvQs3->resize(node_index+1); (*AvQs3)[node_index] = data[p++]; }
    if(AvQs4) { AvQs4->resize(node_index+1); (*AvQs4)[node_index] = data[p++]; }
    if(AvQs5) { AvQs5->resize(node_index+1); (*AvQs5)[node_index] = data[p++]; }
    if(AvQs6) { AvQs6->resize(node_index+1); (*AvQs6)[node_index] = data[p++]; }
    if(AvQs7) { AvQs7->resize(node_index+1); (*AvQs7)[node_index] = data[p++]; }
    if(AvQs8) { AvQs8->resize(node_index+1); (*AvQs8)[node_index] = data[p++]; }
    if(AvQs9) { AvQs9->resize(node_index+1); (*AvQs9)[node_index] = data[p++]; }
    if(AvQs10) { AvQs10->resize(node_index+1); (*AvQs10)[node_index] = data[p++]; }
    if(AvQs11) { AvQs11->resize(node_index+1); (*AvQs11)[node_index] = data[p++]; }
    if(AvQs12) { AvQs12->resize(node_index+1); (*AvQs12)[node_index] = data[p++]; }
    if(AvQs13) { AvQs13->resize(node_index+1); (*AvQs13)[node_index] = data[p++]; }
    if(AvQs14) { AvQs14->resize(node_index+1); (*AvQs14)[node_index] = data[p++]; }
    if(AvQs15) { AvQs15->resize(node_index+1); (*AvQs15)[node_index] = data[p++]; }
    if(AvQs16) { AvQs16->resize(node_index+1); (*AvQs16)[node_index] = data[p++]; }
    if(AvQs17) { AvQs17->resize(node_index+1); (*AvQs17)[node_index] = data[p++]; }
    if(AvQs18) { AvQs18->resize(node_index+1); (*AvQs18)[node_index] = data[p++]; }
    if(AvQv0) { AvQv0->resize(node_index+1); for(int i = 0; i < 3; ++i) (*AvQv0)[node_index][i] = data[p++]; }
    if(AvQv1) { AvQv1->resize(node_index+1); for(int i = 0; i < 3; ++i) (*AvQv1)[node_index][i] = data[p++]; }
    if(AvQv2) { AvQv2->resize(node_index+1); for(int i = 0; i < 3; ++i) (*AvQv2)[node_index][i] = data[p++]; }
    if(AvQv3) { AvQv3->resize(node_index+1); for(int i = 0; i < 3; ++i) (*AvQv3)[node_index][i] = data[p++]; }
    if(AvQv4) { AvQv4->resize(node_index+1); for(int i = 0; i < 3; ++i) (*AvQv4)[node_index][i] = data[p++]; }
    if(AvQv5) { AvQv5->resize(node_index+1); for(int i = 0; i < 3; ++i) (*AvQv5)[node_index][i] = data[p++]; }
    if(AvQt0) { AvQt0->resize(node_index+1); for(int i = 0; i < 6; ++i) (*AvQt0)[node_index][i] = data[p++]; }
    if(AvQt1) { AvQt1->resize(node_index+1); for(int i = 0; i < 6; ++i) (*AvQt1)[node_index][i] = data[p++]; }
    if(AvQt2) { AvQt2->resize(node_index+1); for(int i = 0; i < 6; ++i) (*AvQt2)[node_index][i] = data[p++]; }
    if(dists) { dists->resize(node_index+1); (*dists)[node_index] = data[p++]; }
    if(dirs) { dirs->resize(node_index+1); for(int i = 0; i < 3; ++i) (*dirs)[node_index][i] = data[p++]; }
    if(mets) { mets->resize(node_index+1); for(int i = 0; i < 9; ++i) (*mets)[node_index][i] = data[p++]; }
    if(refScalar0) { refScalar0->resize(node_index+1); (*refScalar0)[node_index][0] = data[p++]; }
    if(refScalar1) { refScalar1->resize(node_index+1); (*refScalar1)[node_index][0] = data[p++]; }
    if(refScalar2) { refScalar2->resize(node_index+1); (*refScalar2)[node_index][0] = data[p++]; }
    if(refScalar3) { refScalar3->resize(node_index+1); (*refScalar3)[node_index][0] = data[p++]; }
    if(refScalar4) { refScalar4->resize(node_index+1); (*refScalar4)[node_index][0] = data[p++]; }
    if(refScalar5) { refScalar5->resize(node_index+1); (*refScalar5)[node_index][0] = data[p++]; }
    if(refScalar6) { refScalar6->resize(node_index+1); (*refScalar6)[node_index][0] = data[p++]; }
    if(refScalar7) { refScalar7->resize(node_index+1); (*refScalar7)[node_index][0] = data[p++]; }
    if(ddx0) { ddx0->resize(node_index+1); (*ddx0)[node_index][0] = data[p++]; }
    if(ddx1) { ddx1->resize(node_index+1); (*ddx1)[node_index][0] = data[p++]; }
    if(ddx2) { ddx2->resize(node_index+1); (*ddx2)[node_index][0] = data[p++]; }
    if(ddx3) { ddx3->resize(node_index+1); (*ddx3)[node_index][0] = data[p++]; }
    if(ddx4) { ddx4->resize(node_index+1); (*ddx4)[node_index][0] = data[p++]; }
    if(ddx5) { ddx5->resize(node_index+1); (*ddx5)[node_index][0] = data[p++]; }
    if(ddx6) { ddx6->resize(node_index+1); (*ddx6)[node_index][0] = data[p++]; }
    if(ddx7) { ddx7->resize(node_index+1); (*ddx7)[node_index][0] = data[p++]; }
    if(ddy0) { ddy0->resize(node_index+1); (*ddy0)[node_index][0] = data[p++]; }
    if(ddy1) { ddy1->resize(node_index+1); (*ddy1)[node_index][0] = data[p++]; }
    if(ddy2) { ddy2->resize(node_index+1); (*ddy2)[node_index][0] = data[p++]; }
    if(ddy3) { ddy3->resize(node_index+1); (*ddy3)[node_index][0] = data[p++]; }
    if(ddy4) { ddy4->resize(node_index+1); (*ddy4)[node_index][0] = data[p++]; }
    if(ddy5) { ddy5->resize(node_index+1); (*ddy5)[node_index][0] = data[p++]; }
    if(ddy6) { ddy6->resize(node_index+1); (*ddy6)[node_index][0] = data[p++]; }
    if(ddy7) { ddy7->resize(node_index+1); (*ddy7)[node_index][0] = data[p++]; }
    if(ddz0) { ddz0->resize(node_index+1); (*ddz0)[node_index][0] = data[p++]; }
    if(ddz1) { ddz1->resize(node_index+1); (*ddz1)[node_index][0] = data[p++]; }
    if(ddz2) { ddz2->resize(node_index+1); (*ddz2)[node_index][0] = data[p++]; }
    if(ddz3) { ddz3->resize(node_index+1); (*ddz3)[node_index][0] = data[p++]; }
    if(ddz4) { ddz4->resize(node_index+1); (*ddz4)[node_index][0] = data[p++]; }
    if(ddz5) { ddz5->resize(node_index+1); (*ddz5)[node_index][0] = data[p++]; }
    if(ddz6) { ddz6->resize(node_index+1); (*ddz6)[node_index][0] = data[p++]; }
    if(ddz7) { ddz7->resize(node_index+1); (*ddz7)[node_index][0] = data[p++]; }
    return p;
  }
};

#endif

#ifndef _CURVATURE_DETECTION_H_
#define _CURVATURE_DETECTION_H_

class Domain;
class SubDomain;
template<class T> class CommPattern;
template<class Scalar> class DistVec;
template<class Scalar, int dim> class DistSVec;

//------------------------------------------------------------------------------

class CurvatureDetection {

  Domain *domain;
  int numLocSub;
  SubDomain **subDomain;
  DistSVec<double, 6> *normals;
  DistSVec<double, 5> *originAndTag;
  CommPattern<double> *vec5;
  CommPattern<double> *vec6;

 public:

  CurvatureDetection(Domain *);
  ~CurvatureDetection();

  void compute(double, int, double, DistVec<bool>&);

};

//------------------------------------------------------------------------------

#endif

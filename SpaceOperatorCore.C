#include <SpaceOperator.C>
#include <InstantiationHelper.h>

#define SpaceOperator_INSTANTIATION_HELPER(dim) \
  template class SpaceOperator<dim>;\
  template \
  void SpaceOperator<dim>::computeJacobianLS(DistSVec<double, dimLS>& Phi, DistMat<MvpMat<double, dimLS>>& A);

DIM_INSTANTIATION_HELPER(SpaceOperator_INSTANTIATION_HELPER)

//------------------------------------------------------------------------------

#define SpaceOperator_INSTANTIATION_HELPER_1(dim,Scalar,neq) \
  template \
  void SpaceOperator<dim>::computeJacobian(DistSVec<double, dim>& U, DistMat<MvpMat<Scalar, neq>>& A, DistTimeState<dim> *timeState);\
  template \
  void SpaceOperator<dim>::applyBCsToJacobian(DistSVec<double, dim>& U, DistMat<MvpMat<Scalar, neq>>& A);\
  template \
  void SpaceOperator<dim>::applyH2(DistSVec<double, dim>& U, DistMat<MvpMat<Scalar, neq>>& A,\
                                   DistSVec<double, dim+dim+dim+dim>& aij, DistSVec<Scalar, dim>& p,\
                                   DistSVec<Scalar, neq>& prod, DistVec<bool> *rowMask,\
                                   DistVec<bool> *prodMask);

DIM_SCALAR_NEQ_INSTANTIATION_HELPER(SpaceOperator_INSTANTIATION_HELPER_1)

//------------------------------------------------------------------------------

#include <BcFcn.h>
#include <BcDef.h>

//------------------------------------------------------------------------------

template<class Scalar, int neq>
void BcFcnEuler::template_applyToDiagonalTerm(int type, double *Vwall, double *U, Scalar *A, double (*e)[3]) {
  // apply strong enforcement of symmetry b.c.
  if(type == BC_SYMMETRY) {
    Scalar Ag[3][neq];
    for(int k = 0; k < neq; ++ k) {
      Ag[0][k] = A[neq * 1 + k];
      Ag[1][k] = A[neq * 2 + k];
      Ag[2][k] = A[neq * 3 + k];
    }
    for(int k = 0; k < neq; ++k) {
      A[neq * 1 + k] = 0.0;
      A[neq * 2 + k] = e[1][0] * Ag[0][k] + e[1][1] * Ag[1][k] + e[1][2] * Ag[2][k];
      A[neq * 3 + k] = e[2][0] * Ag[0][k] + e[2][1] * Ag[1][k] + e[2][2] * Ag[2][k];
    }
    A[neq * 1 + 1] = e[0][0];
    A[neq * 1 + 2] = e[0][1];
    A[neq * 1 + 3] = e[0][2];
  }
  else if(type == BC_DOUBLE_SYMMETRY) {
    Scalar Ag[3][neq];
    for(int k = 0; k < neq; ++ k) {
      Ag[0][k] = A[neq * 1 + k];
      Ag[1][k] = A[neq * 2 + k];
      Ag[2][k] = A[neq * 3 + k];
    }
    for(int k = 0; k < neq; ++k) {
      A[neq * 1 + k] = 0.0;
      A[neq * 2 + k] = 0.0;
      A[neq * 3 + k] = e[2][0] * Ag[0][k] + e[2][1] * Ag[1][k] + e[2][2] * Ag[2][k];
    }
    A[neq * 1 + 1] = e[0][0];
    A[neq * 1 + 2] = e[0][1];
    A[neq * 1 + 3] = e[0][2];
    A[neq * 2 + 1] = e[1][0];
    A[neq * 2 + 2] = e[1][1];
    A[neq * 2 + 3] = e[1][2];
  }
  else if(type == BC_TRIPLE_SYMMETRY) {
    for(int k = 0; k < neq; ++k) {
      A[neq * 1 + k] = 0.0;
      A[neq * 2 + k] = 0.0;
      A[neq * 3 + k] = 0.0;
    }
    A[neq * 1 + 1] = 1.0;
    A[neq * 2 + 2] = 1.0;
    A[neq * 3 + 3] = 1.0;
  }
}

//------------------------------------------------------------------------------

template<class Scalar, int neq>
void BcFcnEuler::template_applyToOffDiagonalTerm(int type, Scalar *A, double (*e)[3]) {
  // apply strong enforcement of symmetry b.c.
  if(type == BC_SYMMETRY) {
    Scalar Ag[3][neq];
    for(int k = 0; k < neq; ++ k) {
      Ag[0][k] = A[neq * 1 + k];
      Ag[1][k] = A[neq * 2 + k];
      Ag[2][k] = A[neq * 3 + k];
    }
    for(int k = 0; k < neq; ++k) {
      A[neq * 1 + k] = 0.0;
      A[neq * 2 + k] = e[1][0] * Ag[0][k] + e[1][1] * Ag[1][k] + e[1][2] * Ag[2][k];
      A[neq * 3 + k] = e[2][0] * Ag[0][k] + e[2][1] * Ag[1][k] + e[2][2] * Ag[2][k];
    }
  }
  else if(type == BC_DOUBLE_SYMMETRY) {
    Scalar Ag[3][neq];
    for(int k = 0; k < neq; ++ k) {
      Ag[0][k] = A[neq * 1 + k];
      Ag[1][k] = A[neq * 2 + k];
      Ag[2][k] = A[neq * 3 + k];
    }
    for(int k = 0; k < neq; ++k) {
      A[neq * 1 + k] = 0.0;
      A[neq * 2 + k] = 0.0;
      A[neq * 3 + k] = e[2][0] * Ag[0][k] + e[2][1] * Ag[1][k] + e[2][2] * Ag[2][k];
    }
  }
  else if(type == BC_TRIPLE_SYMMETRY) {
    for(int k = 0; k < neq; ++k) {
      A[neq * 1 + k] = 0.0;
      A[neq * 2 + k] = 0.0;
      A[neq * 3 + k] = 0.0;
    }
  }
}

//------------------------------------------------------------------------------

template<class Scalar, int neq>
void BcFcnNS::template_applyToDiagonalTerm(int type, double *Vwall, double *U, Scalar *A) {
  if(type == BC_ISOTHERMAL_WALL_MOVING || type == BC_ISOTHERMAL_WALL_FIXED ||
     type == BC_ADIABATIC_WALL_MOVING || type == BC_ADIABATIC_WALL_FIXED) {
    for(int k = 0; k < neq; ++k) {
      A[neq * 1 + k] = 0.0;
      A[neq * 2 + k] = 0.0;
      A[neq * 3 + k] = 0.0;
      if(type == BC_ISOTHERMAL_WALL_MOVING || type == BC_ISOTHERMAL_WALL_FIXED) {
        A[neq * 4 + k] = 0.0;
      }
    }
    A[neq * 1 + 0] = - Vwall[1];
    A[neq * 1 + 1] = 1.0;
    A[neq * 2 + 0] = - Vwall[2];
    A[neq * 2 + 2] = 1.0;
    A[neq * 3 + 0] = - Vwall[3];
    A[neq * 3 + 3] = 1.0;
    if(type == BC_ISOTHERMAL_WALL_MOVING || type == BC_ISOTHERMAL_WALL_FIXED) {
      A[neq * 4 + 0] = U[4] - 2.0 * Vwall[4] * U[0];
      A[neq * 4 + 1] = - U[1];
      A[neq * 4 + 2] = - U[2];
      A[neq * 4 + 3] = - U[3];
      A[neq * 4 + 4] = U[0];
    }
  }
}

//------------------------------------------------------------------------------

template<class Scalar, int neq>
void BcFcnNS::template_applyToOffDiagonalTerm(int type, Scalar *A) {
  if(type == BC_ISOTHERMAL_WALL_MOVING || type == BC_ISOTHERMAL_WALL_FIXED ||
     type == BC_ADIABATIC_WALL_MOVING || type == BC_ADIABATIC_WALL_FIXED) {
    for(int k = 0; k < neq; ++k) {
      A[neq * 1 + k] = 0.0;
      A[neq * 2 + k] = 0.0;
      A[neq * 3 + k] = 0.0;
      if(type == BC_ISOTHERMAL_WALL_MOVING || type == BC_ISOTHERMAL_WALL_FIXED) {
        A[neq * 4 + k] = 0.0;
      }
    }
  }
}

//------------------------------------------------------------------------------

template<class Scalar>
void BcFcnSA::template_applyToDiagonalTerm(int type, double *Vwall, double *U, Scalar *A) {
  const int neq = 6;
  if(type == BC_ISOTHERMAL_WALL_MOVING || type == BC_ISOTHERMAL_WALL_FIXED ||
     type == BC_ADIABATIC_WALL_MOVING || type == BC_ADIABATIC_WALL_FIXED) {
    for(int k = 0; k < neq; ++k) {
      A[neq * 5 + k] = 0.0;
    }
    A[neq * 5 + 0] = - Vwall[5];
    A[neq * 5 + 5] = 1.0;
  }
}

//------------------------------------------------------------------------------

template<class Scalar>
void BcFcnSA::template_applyToWallDiagonalTerm(int type, double *Vwall, double *dVwall, double *U, Scalar *A) {
  const int neq = 6;
  if(type == BC_ISOTHERMAL_WALL_MOVING || type == BC_ISOTHERMAL_WALL_FIXED ||
     type == BC_ADIABATIC_WALL_MOVING || type == BC_ADIABATIC_WALL_FIXED) {
    A[neq * 5 + 0] = - Vwall[5] - U[0] * dVwall[0];
    A[neq * 5 + 1] = - U[0] * dVwall[1];
    A[neq * 5 + 2] = - U[0] * dVwall[2];
    A[neq * 5 + 3] = - U[0] * dVwall[3];
    A[neq * 5 + 4] = - U[0] * dVwall[4];
    A[neq * 5 + 5] = - U[0] * dVwall[5] + 1.0;
  }
}

//------------------------------------------------------------------------------

template<class Scalar>
void BcFcnSA::template_applyToOffDiagonalTerm(int type, Scalar *A) {
  const int neq = 6;
  if(type == BC_ISOTHERMAL_WALL_MOVING || type == BC_ISOTHERMAL_WALL_FIXED ||
     type == BC_ADIABATIC_WALL_MOVING || type == BC_ADIABATIC_WALL_FIXED) {
    for(int k = 0; k < neq; ++k) {
      A[neq * 5 + k] = 0.0;
    }
  }
}

//------------------------------------------------------------------------------

template<class Scalar>
void BcFcnSA::template_applyToTurbDiagonalTerm(int type, double *Vwall, double *U, Scalar *A) {
  const int neq = 6;
  for(int k = 0; k < neq; ++k) {
    A[neq * 5 + k] = 0.0;
  }
  A[neq * 5 + 0] = - Vwall[5];
  A[neq * 5 + 5] = 1.0;
}

//------------------------------------------------------------------------------

template<class Scalar>
void BcFcnSA::template_applyToTurbOffDiagonalTerm(int type, Scalar *A) {
  const int neq = 6;
  for(int k = 0; k < neq; ++k) {
    A[neq * 5 + k] = 0.0;
  }
}

//------------------------------------------------------------------------------

template<class Scalar>
void BcFcnSAturb::template_applyToDiagonalTerm(int type, double *Vwall, double *U, Scalar *A) {
  if(type == BC_ISOTHERMAL_WALL_MOVING || type == BC_ISOTHERMAL_WALL_FIXED ||
     type == BC_ADIABATIC_WALL_MOVING || type == BC_ADIABATIC_WALL_FIXED) {
    A[0] = 1.0;
  }
}

//------------------------------------------------------------------------------

template<class Scalar>
void BcFcnSAturb::template_applyToOffDiagonalTerm(int type, Scalar *A) {
  if(type == BC_ISOTHERMAL_WALL_MOVING || type == BC_ISOTHERMAL_WALL_FIXED ||
     type == BC_ADIABATIC_WALL_MOVING || type == BC_ADIABATIC_WALL_FIXED) {
    A[0] = 0.0;
  }
}

//------------------------------------------------------------------------------

template<class Scalar>
void BcFcnSAturb::template_applyToTurbDiagonalTerm(int type, double *Vwall, double *U, Scalar *A) {
  A[0] = 1.0;
}

//------------------------------------------------------------------------------

template<class Scalar>
void BcFcnSAturb::template_applyToTurbOffDiagonalTerm(int type, Scalar *A) {
  A[0] = 0.0;
}

//------------------------------------------------------------------------------

template<class Scalar>
void BcFcnKE::template_applyToDiagonalTerm(int type, double *Vwall, double *U, Scalar *A) {
  const int neq = 7;
  if(type == BC_ISOTHERMAL_WALL_MOVING || type == BC_ISOTHERMAL_WALL_FIXED ||
     type == BC_ADIABATIC_WALL_MOVING || type == BC_ADIABATIC_WALL_FIXED) {
    for(int k = 0; k < neq; ++k) {
      A[neq * 5 + k] = 0.0;
      A[neq * 6 + k] = 0.0;
    }
    A[neq * 5 + 0] = - Vwall[5];
    A[neq * 5 + 5] = 1.0;
    A[neq * 6 + 0] = - Vwall[6];
    A[neq * 6 + 6] = 1.0;
  }
}

//------------------------------------------------------------------------------

template<class Scalar>
void BcFcnKE::template_applyToWallDiagonalTerm(int type, double *Vwall, double *dVwall, double *U, Scalar *A) {
  const int neq = 7;
  if(type == BC_ISOTHERMAL_WALL_MOVING || type == BC_ISOTHERMAL_WALL_FIXED ||
     type == BC_ADIABATIC_WALL_MOVING || type == BC_ADIABATIC_WALL_FIXED) {
    A[neq * 5 + 0] = - Vwall[5] - U[0] * dVwall[0];
    A[neq * 5 + 1] = - U[0] * dVwall[1];
    A[neq * 5 + 2] = - U[0] * dVwall[2];
    A[neq * 5 + 3] = - U[0] * dVwall[3];
    A[neq * 5 + 4] = - U[0] * dVwall[4];
    A[neq * 5 + 5] = - U[0] * dVwall[5] + 1.0;
    A[neq * 5 + 6] = - U[0] * dVwall[6];
    A[neq * 6 + 0] = - Vwall[6] - U[0] * dVwall[7];
    A[neq * 6 + 1] = - U[0] * dVwall[8];
    A[neq * 6 + 2] = - U[0] * dVwall[9];
    A[neq * 6 + 3] = - U[0] * dVwall[10];
    A[neq * 6 + 4] = - U[0] * dVwall[11];
    A[neq * 6 + 5] = - U[0] * dVwall[12];
    A[neq * 6 + 6] = - U[0] * dVwall[13] + 1.0;
  }
}

//------------------------------------------------------------------------------

template<class Scalar>
void BcFcnKE::template_applyToOffDiagonalTerm(int type, Scalar *A) {
  const int neq = 7;
  if(type == BC_ISOTHERMAL_WALL_MOVING || type == BC_ISOTHERMAL_WALL_FIXED ||
     type == BC_ADIABATIC_WALL_MOVING || type == BC_ADIABATIC_WALL_FIXED) {
    for(int k = 0; k < neq; ++k) {
      A[neq * 5 + k] = 0.0;
      A[neq * 6 + k] = 0.0;
    }
  }
}

//------------------------------------------------------------------------------

template<class Scalar>
void BcFcnKE::template_applyToTurbDiagonalTerm(int type, double *Vwall, double *U, Scalar *A) {
  const int neq = 7;
  for(int k = 0; k < neq; ++k) {
    A[neq * 5 + k] = 0.0;
    A[neq * 6 + k] = 0.0;
  }
  A[neq * 5 + 0] = - Vwall[5];
  A[neq * 5 + 5] = 1.0;
  A[neq * 6 + 0] = - Vwall[6];
  A[neq * 6 + 6] = 1.0;
}

//------------------------------------------------------------------------------

template<class Scalar>
void BcFcnKE::template_applyToTurbOffDiagonalTerm(int type, Scalar *A) {
  const int neq = 7;
  for(int k = 0; k < neq; ++k) {
    A[neq * 5 + k] = 0.0;
    A[neq * 6 + k] = 0.0;
  }
}

//------------------------------------------------------------------------------

template<class Scalar>
void BcFcnKEturb::template_applyToDiagonalTerm(int type, double *Vwall, double *U, Scalar *A) {
  if(type == BC_ISOTHERMAL_WALL_MOVING || type == BC_ISOTHERMAL_WALL_FIXED ||
     type == BC_ADIABATIC_WALL_MOVING || type == BC_ADIABATIC_WALL_FIXED) {
    A[0] = 1.0;
    A[1] = 0.0;
    A[2] = 0.0;
    A[3] = 1.0;
  }
}

//------------------------------------------------------------------------------

template<class Scalar>
void BcFcnKEturb::template_applyToOffDiagonalTerm(int type, Scalar *A) {
  if(type == BC_ISOTHERMAL_WALL_MOVING || type == BC_ISOTHERMAL_WALL_FIXED ||
     type == BC_ADIABATIC_WALL_MOVING || type == BC_ADIABATIC_WALL_FIXED) {
    A[0] = 0.0;
    A[1] = 0.0;
    A[2] = 0.0;
    A[3] = 0.0;
  }
}

//------------------------------------------------------------------------------

template<class Scalar>
void BcFcnKEturb::template_applyToTurbDiagonalTerm(int type, double *Vwall, double *U, Scalar *A) {
  A[0] = 1.0;
  A[1] = 0.0;
  A[2] = 0.0;
  A[3] = 1.0;
}

//------------------------------------------------------------------------------

template<class Scalar>
void BcFcnKEturb::template_applyToTurbOffDiagonalTerm(int type, Scalar *A) {
  A[0] = 0.0;
  A[1] = 0.0;
  A[2] = 0.0;
  A[3] = 0.0;
}

//------------------------------------------------------------------------------

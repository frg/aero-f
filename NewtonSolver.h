#ifndef _NEWTON_SOLVER_H_
#define _NEWTON_SOLVER_H_

#include <TsParameters.h>

#include <algorithm>
#include <cstdlib>
#include <cmath>
#include <cstdio>
#include <cstring>

/*@ARTICLE{brown-saad-90,
  author = "Brown, P. N. and Saad, Y.",
  title = "Hybrid {K}rylov Methods for
           Nonlinear Systems of Equations",
  journal = siamjscistat,
  year = 1990,
  volume = 11,
  number = 3,
  pages = "450--481",
}
@ARTICLE{keyes-venkatakrishnan-96,
  author = "Keyes, D. E. and Venkatakrishnan, V.",
  title = "{N}ewton-{K}rylov-{S}chwarz Methods: Interfacing Sparse Linear
  Solvers with Nonlinear Applications",
  journal = zamm,
  year = 1996,
  volume = 76,
  pages = "147--150",
}
*/

//------------------------------------------------------------------------------

template<class ProblemDescriptor, class KspDataType>
class NewtonSolver {

  ProblemDescriptor *probDesc;
  NewtonData<KspDataType>& newtonData;

  typename ProblemDescriptor::SolVecType F;   // nonlinear function
  typename ProblemDescriptor::SolVecType dQ;  // gradient of F
  typename ProblemDescriptor::SolVecType rhs; // right hand side

  FILE *output;
  bool converged;

 public:
  NewtonSolver(ProblemDescriptor *, NewtonData<KspDataType>&, int);
  ~NewtonSolver() {}

  void resize();

  int solve(typename ProblemDescriptor::SolVecType&, const bool = true);

  typename ProblemDescriptor::SolVecType& getF() {
    return F;
  }
  typename ProblemDescriptor::SolVecType& getdQ() {
    return dQ;
  }
  typename ProblemDescriptor::SolVecType& getrhs() {
    return rhs;
  }
  int& getMaxIts() {
    return newtonData.maxIts;
  }
  double& getEps() {
    return newtonData.eps;
  }
  double& getEpsAbsRes() {
    return newtonData.epsAbsRes;
  }
  double& getEpsAbsInc() {
    return newtonData.epsAbsInc;
  }
  bool getConverged() {
    return converged;
  }

  double res0imposed;

};

//------------------------------------------------------------------------------

template<class ProblemDescriptor, class KspDataType>
NewtonSolver<ProblemDescriptor, KspDataType>::NewtonSolver(ProblemDescriptor *_probDesc,
                                                           NewtonData<KspDataType>& _newtonData, int dim) :
  probDesc(_probDesc), newtonData(_newtonData),
  F(_probDesc->getVecInfo(), dim), dQ(_probDesc->getVecInfo(), dim), rhs(_probDesc->getVecInfo(), dim) {
  // open output file
  if(strcmp(newtonData.output, "") == 0) {
    output = 0;
  }
  else if(strcmp(newtonData.output, "stdout") == 0) {
    output = stdout;
  }
  else if(strcmp(newtonData.output, "stderr") == 0) {
    output = stderr;
  }
  else {
    output = fopen(newtonData.output, "w");
    if(!output) {
      probDesc->getCommunicator()->fprintf(stderr, "*** Error: could not open \'%s\'\n", newtonData.output);
      exit(1);
    }
  }
  res0imposed = -1.0;
}

//------------------------------------------------------------------------------

template<class ProblemDescriptor, class KspDataType>
void
NewtonSolver<ProblemDescriptor, KspDataType>::resize() {
  F.resize(probDesc->getVecInfo());
  dQ.resize(probDesc->getVecInfo());
  rhs.resize(probDesc->getVecInfo());
}

//------------------------------------------------------------------------------

template<class ProblemDescriptor, class KspDataType>
int
NewtonSolver<ProblemDescriptor, KspDataType>::solve(typename ProblemDescriptor::SolVecType& Q,
                                                    const bool firstSolve) {
  int& maxIts = getMaxIts();
  double& eps = getEps();
  double& epsAbsRes = getEpsAbsRes();
  double& epsAbsInc = getEpsAbsInc();
  bool finalRes = (maxIts > 1 || output); // if true, then a final residual evaluation will be done
                                          // when maxIts is reached before terminating the loop.
  int& maxItsLS = newtonData.lineSearch.maxIts;
  double& rho = newtonData.lineSearch.rho; // contraction factor for backtracking
  double& c1 = newtonData.lineSearch.c1;   // sufficient decrease factor for backtracking
  int& fsMaxIts = newtonData.fsMaxIts;
  int& fsMinIts = newtonData.fsMinIts;
  Communicator *com = probDesc->getCommunicator();
  double res, target, res0, restrial, alpha;
  int it, fsIt = 0;
  converged = false;
  for(it = 0; finalRes || it < maxIts; ++it) {
    // compute the nonlinear function value
    probDesc->computeFunction(Q, F, (firstSolve && it == 0));
    // check stopping criteria
    res = probDesc->meritFunction(F);
    if(it == 0) {
      res0 = (res0imposed>0 && firstSolve) ? res0imposed : res;
      res0imposed = -1.0;
      target = eps * res0;
    }
    if(output) {
      com->fprintf(output, "Newton iteration #%d: residual = %e, target = %e\n", it, res, std::max(target,epsAbsRes));
    }
    if((res == 0.0) || (res <= target) || (it > 0 && res <= epsAbsRes && dQ.norm() <= epsAbsInc)) {
      converged = true;
      break;
    }
    if(it == maxIts) {
      break;
    }
    rhs = -1.0 * F;
    // setup Jacobian matrix-vector product
    probDesc->computeJacobian(Q, F);
    // apply preconditioner if available
    probDesc->setOperators(Q);
    probDesc->solveLinearSystem(it, rhs, dQ);
    if(maxItsLS > 0) {
      for(int itLS = 0; itLS < maxItsLS; ++itLS) {
        if(itLS > 0) {
          alpha *= rho;
          if(itLS == 1) {
            probDesc->adjustStep(dQ, rho - 1.0);  // dQ *= (rho - 1);
          }
          else {
            probDesc->adjustStep(dQ, rho);  // dQ *= rho;
          }
        }
        else {
          alpha = 1.0;
        }
        // compute updated residual
        probDesc->incrementSolution(Q, dQ, rhs);  // rhs = Q; Q += dQ;
        probDesc->computeFunction(Q, F, false);
        restrial = probDesc->meritFunction(F);
        if(probDesc->lineSearchCondition(restrial, res, alpha, c1) || dQ.norm() <= epsAbsInc) {
          break;
        }
        if(itLS == maxItsLS - 1 && maxItsLS != 1) {
          com->fprintf(stderr, "*** Warning: Line Search reached %d its\n", maxItsLS);
        }
      }
    }
    else {
      probDesc->incrementSolution(Q, dQ, rhs);  // rhs = Q; Q += dQ;
    }
    // verify that the solution is physical
    if(probDesc->checkSolution(Q)) {
      int failSafe = probDesc->checkFailSafe(Q);
      if(probDesc->getTsParams() && probDesc->getTsParams()->checksol && (!failSafe || fsIt >= fsMinIts)) { // repeat time-step
        probDesc->resetSolution(Q, rhs);  // Q = rhs;
        return -10;
      }
      else if(failSafe && fsIt < fsMaxIts) { // repeat Newton iteration
        com->fprintf(stderr, "*** Warning: Newton solver redoing iteration %d\n", it + 1);
        probDesc->resetSolution(Q, rhs);  // Q = rhs;
        --it;
        ++fsIt;
      }
      else { // exit
        com->fprintf(stderr, "Newton solver failed\n");
        return -3;
      }
    }
  }
  if(!converged && maxIts != 1) {
    com->fprintf(stderr, "*** Warning: Newton solver reached %d its", maxIts);
    com->fprintf(stderr, " (Residual: initial=%.2e, reached=%.2e, target=%.2e)\n", res0, res, target);
  }
  return it;
}

//------------------------------------------------------------------------------

#endif

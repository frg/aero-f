#include <WallFcn.h>

#include <VarFcn.h>
#include <Vector3D.h>
#include <ViscoFcn.h>
#include <BcDef.h>

#include <Eigen/Core>
#include <unsupported/Eigen/AutoDiff>

#include <cmath>

//------------------------------------------------------------------------------
// This function was modified to account for the derivative of the muw with respect to the conservative variables
void WallFcn::computeJacobianSurfaceTermLES(int code, Vec3D& normal, double d2wall[3],
                                            double *Vwall, double *V[3], double (*dRdU)[5][5], int tag) {
}

//------------------------------------------------------------------------------
// This function was modified to account for the derivative of the muw with respect to the conservative variables
template<int dim>
void WallFcn::computeJacobianSurfaceTerm(int code, Vec3D& normal, double d2wall[3],
                                         double *Vwall, double *V[3],
                                         double (*dRdU)[dim][dim], int tag) {
  double delta, dT, rhow, muw, gamw;
  double drhow[3][dim];
  double dTdu0;
  double dTdu1;
  double dTdu2;
  double dTdu3;
  double dTdu4;
  double dutau[3][dim];
  double dadu0;
  double dadu1;
  double dadu2;
  double dadu3;
  double dadu4;
  double dmuw[3][dim];
  Vec3D ddudu0;
  Vec3D ddudu1;
  Vec3D ddudu2;
  Vec3D ddudu3;
  Vec3D ddudu4;
  Vec3D dtdu0;
  Vec3D dtdu1;
  Vec3D dtdu2;
  Vec3D dtdu3;
  Vec3D dtdu4;
  Vec3D du, uw;
  computeFaceValues(d2wall, Vwall, V, delta, du, dT, uw, rhow, muw, gamw, tag);
  double T = dT + Vwall[4]; // note: dT = T - Tw
  for(int i = 0; i < 3; ++i) {
    for(int j = 0; j < dim; ++j) {
      drhow[i][j] = 0.0;
      dutau[i][j] = 0.0;
      dmuw[i][j] = 0.0;
    }
  }
  drhow[0][0] = third;
  drhow[1][0] = third;
  drhow[2][0] = third;
  double norm = sqrt(normal * normal);
  Vec3D n = (1.0 / norm) * normal;
  Vec3D t = computeTangentVector(n, du);
  double dut = du * t;
  double utau = computeFrictionVelocity(t, delta, rhow, du, muw);
  double a = - rhow * utau * utau * norm;
  double Ttau;
  double dTtaudu0;
  double dTtaudu1;
  double dTtaudu2;
  double dTtaudu3;
  double dTtaudu4;
  double dMach = 0.0;
  if(code == BC_ISOTHERMAL_WALL_MOVING || code == BC_ISOTHERMAL_WALL_FIXED) {
    Ttau = computeFrictionTemperature(utau, delta, rhow, dT, muw);
  }
  for(int k = 0; k < 3; ++k) {
    double rho = varFcn->getDensity(V[k], tag);
    double Tk = varFcn->computeTemperature(V[k], tag);
    ddudu0[0] = - third * varFcn->getVelocityX(V[k], tag) / rho;
    ddudu0[1] = - third * varFcn->getVelocityY(V[k], tag) / rho;
    ddudu0[2] = - third * varFcn->getVelocityZ(V[k], tag) / rho;
    ddudu1[0] = third / rho;
    ddudu1[1] = 0.0;
    ddudu1[2] = 0.0;
    ddudu2[0] = 0.0;
    ddudu2[1] = third / rho;
    ddudu2[2] = 0.0;
    ddudu3[0] = 0.0;
    ddudu3[1] = 0.0;
    ddudu3[2] = third / rho;
    ddudu4[0] = 0.0;
    ddudu4[1] = 0.0;
    ddudu4[2] = 0.0;
    dtdu0 = computeDerivativeOfTangentVector(n, du, ddudu0);
    dtdu1 = computeDerivativeOfTangentVector(n, du, ddudu1);
    dtdu2 = computeDerivativeOfTangentVector(n, du, ddudu2);
    dtdu3 = computeDerivativeOfTangentVector(n, du, ddudu3);
    dtdu4 = computeDerivativeOfTangentVector(n, du, ddudu4);
    dTdu0 = - third / V[k][0] * (Tk - 0.5 * (V[k][1] * V[k][1] + V[k][2] * V[k][2] + V[k][3] * V[k][3]));
    dTdu1 = - third / V[k][0] * V[k][1];
    dTdu2 = - third / V[k][0] * V[k][2];
    dTdu3 = - third / V[k][0] * V[k][3];
    dTdu4 = third / V[k][0];
    dmuw[k][0] = viscoFcn->compute_muDerivative(T, dTdu0, dMach, tag);
    dmuw[k][1] = viscoFcn->compute_muDerivative(T, dTdu1, dMach, tag);
    dmuw[k][2] = viscoFcn->compute_muDerivative(T, dTdu2, dMach, tag);
    dmuw[k][3] = viscoFcn->compute_muDerivative(T, dTdu3, dMach, tag);
    dmuw[k][4] = viscoFcn->compute_muDerivative(T, dTdu4, dMach, tag);
    dutau[k][0] = computeDerivativeOfFrictionVelocity(t, dtdu0, delta, 0.0, rhow, drhow[k][0], du, ddudu0, muw, dmuw[k][0], dMach);
    dutau[k][1] = computeDerivativeOfFrictionVelocity(t, dtdu1, delta, 0.0, rhow, drhow[k][1], du, ddudu1, muw, dmuw[k][1], dMach);
    dutau[k][2] = computeDerivativeOfFrictionVelocity(t, dtdu2, delta, 0.0, rhow, drhow[k][2], du, ddudu2, muw, dmuw[k][2], dMach);
    dutau[k][3] = computeDerivativeOfFrictionVelocity(t, dtdu3, delta, 0.0, rhow, drhow[k][3], du, ddudu3, muw, dmuw[k][3], dMach);
    dutau[k][4] = computeDerivativeOfFrictionVelocity(t, dtdu4, delta, 0.0, rhow, drhow[k][4], du, ddudu4, muw, dmuw[k][4], dMach);
    dadu0 = - drhow[k][0] * utau * utau * norm - rhow * 2.0 * utau * dutau[k][0] * norm;
    dadu1 = - drhow[k][1] * utau * utau * norm - rhow * 2.0 * utau * dutau[k][1] * norm;
    dadu2 = - drhow[k][2] * utau * utau * norm - rhow * 2.0 * utau * dutau[k][2] * norm;
    dadu3 = - drhow[k][3] * utau * utau * norm - rhow * 2.0 * utau * dutau[k][3] * norm;
    dadu4 = - drhow[k][4] * utau * utau * norm - rhow * 2.0 * utau * dutau[k][4] * norm;
    dRdU[k][0][0] = 0.0;
    dRdU[k][0][1] = 0.0;
    dRdU[k][0][2] = 0.0;
    dRdU[k][0][3] = 0.0;
    dRdU[k][0][4] = 0.0;
    dRdU[k][1][0] = dadu0 * t[0] + a * dtdu0[0];
    dRdU[k][1][1] = dadu1 * t[0] + a * dtdu1[0];
    dRdU[k][1][2] = dadu2 * t[0] + a * dtdu2[0];
    dRdU[k][1][3] = dadu3 * t[0] + a * dtdu3[0];
    dRdU[k][1][4] = dadu4 * t[0] + a * dtdu4[0];
    dRdU[k][2][0] = dadu0 * t[1] + a * dtdu0[1];
    dRdU[k][2][1] = dadu1 * t[1] + a * dtdu1[1];
    dRdU[k][2][2] = dadu2 * t[1] + a * dtdu2[1];
    dRdU[k][2][3] = dadu3 * t[1] + a * dtdu3[1];
    dRdU[k][2][4] = dadu4 * t[1] + a * dtdu4[1];
    dRdU[k][3][0] = dadu0 * t[2] + a * dtdu0[2];
    dRdU[k][3][1] = dadu1 * t[2] + a * dtdu1[2];
    dRdU[k][3][2] = dadu2 * t[2] + a * dtdu2[2];
    dRdU[k][3][3] = dadu3 * t[2] + a * dtdu3[2];
    dRdU[k][3][4] = dadu4 * t[2] + a * dtdu4[2];
    dRdU[k][4][0] = dadu0 * (uw * t) + a * (uw * dtdu0);
    dRdU[k][4][1] = dadu1 * (uw * t) + a * (uw * dtdu1);
    dRdU[k][4][2] = dadu2 * (uw * t) + a * (uw * dtdu2);
    dRdU[k][4][3] = dadu3 * (uw * t) + a * (uw * dtdu3);
    dRdU[k][4][4] = dadu4 * (uw * t) + a * (uw * dtdu4);
    if(code == BC_ISOTHERMAL_WALL_MOVING || code == BC_ISOTHERMAL_WALL_FIXED) {
      dTtaudu0 = computeDerivativeOfFrictionTemperature(utau, dutau[k][0], delta, 0.0, rhow, drhow[k][0], dT, dTdu0, muw, dmuw[k][0], dMach);
      dTtaudu1 = computeDerivativeOfFrictionTemperature(utau, dutau[k][1], delta, 0.0, rhow, drhow[k][1], dT, dTdu1, muw, dmuw[k][1], dMach);
      dTtaudu2 = computeDerivativeOfFrictionTemperature(utau, dutau[k][2], delta, 0.0, rhow, drhow[k][2], dT, dTdu2, muw, dmuw[k][2], dMach);
      dTtaudu3 = computeDerivativeOfFrictionTemperature(utau, dutau[k][3], delta, 0.0, rhow, drhow[k][3], dT, dTdu3, muw, dmuw[k][3], dMach);
      dTtaudu4 = computeDerivativeOfFrictionTemperature(utau, dutau[k][4], delta, 0.0, rhow, drhow[k][4], dT, dTdu4, muw, dmuw[k][4], dMach);
      dRdU[k][4][0] += gamw * drhow[k][0] * utau * Ttau * norm + gamw * rhow * dutau[k][0] * Ttau * norm + gamw * rhow * utau * dTtaudu0 * norm;
      dRdU[k][4][1] += gamw * drhow[k][1] * utau * Ttau * norm + gamw * rhow * dutau[k][1] * Ttau * norm + gamw * rhow * utau * dTtaudu1 * norm;
      dRdU[k][4][2] += gamw * drhow[k][2] * utau * Ttau * norm + gamw * rhow * dutau[k][2] * Ttau * norm + gamw * rhow * utau * dTtaudu2 * norm;
      dRdU[k][4][3] += gamw * drhow[k][3] * utau * Ttau * norm + gamw * rhow * dutau[k][3] * Ttau * norm + gamw * rhow * utau * dTtaudu3 * norm;
      dRdU[k][4][4] += gamw * drhow[k][4] * utau * Ttau * norm + gamw * rhow * dutau[k][4] * Ttau * norm + gamw * rhow * utau * dTtaudu4 * norm;
    }
  }
}

//------------------------------------------------------------------------------
// This function was included to account for the derivative of the Vwall[5] with respect to the conservative variables
template<int dim>
void WallFcn::computeBCsJacobianWallValues(int code, Vec3D& normal, double d2wall[3],
                                           double *Vwall, double *dVwall, double *V[3], int tag) {
  double delta, dT, rhow, muw, gamw;
  double dTdu0;
  double dTdu1;
  double dTdu2;
  double dTdu3;
  double dTdu4;
  double dMach = 0.0;
  double drhow[3][dim];
  double dutau[3][dim];
  double dmuw[3][dim];
  double ddut[3][dim];
  double dvwall[3][(dim==7)?14:dim];
  Vec3D ddudu0;
  Vec3D ddudu1;
  Vec3D ddudu2;
  Vec3D ddudu3;
  Vec3D ddudu4;
  Vec3D dtdu0;
  Vec3D dtdu1;
  Vec3D dtdu2;
  Vec3D dtdu3;
  Vec3D dtdu4;
  Vec3D du, uw;
  computeFaceValues(d2wall, Vwall, V, delta, du, dT, uw, rhow, muw, gamw, tag);
  double T = dT + Vwall[4]; // note: dT = T - Tw
  for(int i = 0; i < 3; ++i) {
    for(int j = 0; j < dim; ++j) {
      drhow[i][j] = 0.0;
      dutau[i][j] = 0.0;
      dmuw[i][j] = 0.0;
      ddut[i][j] = 0.0;
      dvwall[i][j] = 0.0;
      if(dim == 7) {
        dvwall[i][7+j] = 0.0;
      }
    }
  }
  drhow[0][0] = third;
  drhow[1][0] = third;
  drhow[2][0] = third;
  double norm = sqrt(normal * normal);
  Vec3D n = (1.0 / norm) * normal;
  Vec3D t = computeTangentVector(n, du);
  double dut = du * t;
  double utau = computeFrictionVelocity(t, delta, rhow, du, muw);
  for(int k = 0; k < 3; ++k) {
    double rho = varFcn->getDensity(V[k], tag);
    double Tk = varFcn->computeTemperature(V[k], tag);
    ddudu0[0] = - third * varFcn->getVelocityX(V[k], tag) / rho;
    ddudu0[1] = - third * varFcn->getVelocityY(V[k], tag) / rho;
    ddudu0[2] = - third * varFcn->getVelocityZ(V[k], tag) / rho;
    ddudu1[0] = third / rho;
    ddudu1[1] = 0.0;
    ddudu1[2] = 0.0;
    ddudu2[0] = 0.0;
    ddudu2[1] = third / rho;
    ddudu2[2] = 0.0;
    ddudu3[0] = 0.0;
    ddudu3[1] = 0.0;
    ddudu3[2] = third / rho;
    ddudu4[0] = 0.0;
    ddudu4[1] = 0.0;
    ddudu4[2] = 0.0;
    dtdu0 = computeDerivativeOfTangentVector(n, du, ddudu0);
    dtdu1 = computeDerivativeOfTangentVector(n, du, ddudu1);
    dtdu2 = computeDerivativeOfTangentVector(n, du, ddudu2);
    dtdu3 = computeDerivativeOfTangentVector(n, du, ddudu3);
    dtdu4 = computeDerivativeOfTangentVector(n, du, ddudu4);
    dTdu0 = - third / V[k][0] * (Tk - 0.5 * (V[k][1] * V[k][1] + V[k][2] * V[k][2] + V[k][3] * V[k][3]));
    dTdu1 = - third / V[k][0] * V[k][1];
    dTdu2 = - third / V[k][0] * V[k][2];
    dTdu3 = - third / V[k][0] * V[k][3];
    dTdu4 = third / V[k][0];
    dmuw[k][0] = viscoFcn->compute_muDerivative(T, dTdu0, dMach, tag);
    dmuw[k][1] = viscoFcn->compute_muDerivative(T, dTdu1, dMach, tag);
    dmuw[k][2] = viscoFcn->compute_muDerivative(T, dTdu2, dMach, tag);
    dmuw[k][3] = viscoFcn->compute_muDerivative(T, dTdu3, dMach, tag);
    dmuw[k][4] = viscoFcn->compute_muDerivative(T, dTdu4, dMach, tag);
    dutau[k][0] = computeDerivativeOfFrictionVelocity(t, dtdu0, delta, 0.0, rhow, drhow[k][0], du, ddudu0, muw, dmuw[k][0], dMach);
    dutau[k][1] = computeDerivativeOfFrictionVelocity(t, dtdu1, delta, 0.0, rhow, drhow[k][1], du, ddudu1, muw, dmuw[k][1], dMach);
    dutau[k][2] = computeDerivativeOfFrictionVelocity(t, dtdu2, delta, 0.0, rhow, drhow[k][2], du, ddudu2, muw, dmuw[k][2], dMach);
    dutau[k][3] = computeDerivativeOfFrictionVelocity(t, dtdu3, delta, 0.0, rhow, drhow[k][3], du, ddudu3, muw, dmuw[k][3], dMach);
    dutau[k][4] = computeDerivativeOfFrictionVelocity(t, dtdu4, delta, 0.0, rhow, drhow[k][4], du, ddudu4, muw, dmuw[k][4], dMach);
    ddut[k][0] = ddudu0 * t + du * dtdu0;
    ddut[k][1] = ddudu1 * t + du * dtdu1;
    ddut[k][2] = ddudu2 * t + du * dtdu2;
    ddut[k][3] = ddudu3 * t + du * dtdu3;
    ddut[k][4] = ddudu4 * t + du * dtdu4;
    for(int l = 0; l < dim; ++l) {
      computeDerivativeOfWallValues(utau, dutau[k][l], delta, rhow, drhow[k][l], dut, ddut[k][l], muw, dmuw[k][l], dMach, Vwall, dVwall);
      dvwall[k][l] = dVwall[5];
      if(dim == 7) {
        dvwall[k][7+l] = dVwall[6];
      }
    }
  }
  for(int l = 0; l < (dim==7?14:dim); ++l) {
    dVwall[l] = dvwall[0][l] + dvwall[1][l] + dvwall[2][l];
  }
}

//------------------------------------------------------------------------------

template<int dim>
void WallFcn::computeVolumeTerm(int code, Vec3D& normal, double delta, double *Vwall, int idxf, double *V[4],
                                int tag, double dudxj[3][3], double dTdxj[3], double mu, double lambda,
                                double kappa, double ucg[3], double (*r)[dim]) {
  double rhow = varFcn->getDensity(V[idxf], tag);
  Vec3D uw(Vwall[1], Vwall[2], Vwall[3]);
  Vec3D du = varFcn->getVelocity(V[idxf], tag) - uw;
  double T = varFcn->computeTemperature(V[idxf], tag);
  double muw = viscoFcn->compute_mu(T, tag);
  double norm = normal.norm();
  Vec3D n = (1.0 / norm) * normal;
  Vec3D t = computeTangentVector(n, du);
  double utau = computeFrictionVelocity(t, delta, rhow, du, muw);
  double dudn = reynolds * utau * utau * rhow / muw;

  double dTdn;
  if(code == BC_ISOTHERMAL_WALL_MOVING || code == BC_ISOTHERMAL_WALL_FIXED) {
    double Tw = Vwall[4];
    double dT = T - Tw;
    double Ttau = computeFrictionTemperature(utau, delta, rhow, dT, muw);
    double gamw = varFcn->specificHeatCstPressure(tag);
    dTdn = gamw * rhow * utau * Ttau * norm; // XXX check
  }
  else dTdn = 0;

  // velocity gradient correction
  double a = dudn - ( t[0] * (dudxj[0][0] * n[0] + dudxj[0][1] * n[1] + dudxj[0][2] * n[2]) +
                      t[1] * (dudxj[1][0] * n[0] + dudxj[1][1] * n[1] + dudxj[1][2] * n[2]) +
                      t[2] * (dudxj[2][0] * n[0] + dudxj[2][1] * n[1] + dudxj[2][2] * n[2]) );
  double dudxjc[3][3];
  for(int i = 0; i < 3; ++i) {
    for(int j = 0; j < 3; ++j) {
      dudxjc[i][j] = dudxj[i][j] + a * t[i] * n[j];
    }
  }

  // temperature gradient correction
  double b = dTdn - (n[0] * dTdxj[0] + n[1] * dTdxj[1] + n[2] * dTdxj[2]);
  double dTdxjc[3];
  for(int i = 0; i < 3; ++i) {
    dTdxjc[i] = dTdxj[i] + b * n[i];
  }

  // no eddy viscosity from LES
  double nu = 1.;

  // compute stress tensor
  double div = dudxjc[0][0] + dudxjc[1][1] + dudxjc[2][2];
  double tij[3][3];
  tij[0][0] = lambda * div + 2.0 * mu * dudxjc[0][0];
  tij[1][1] = lambda * div + 2.0 * mu * dudxjc[1][1];
  tij[2][2] = lambda * div + 2.0 * mu * dudxjc[2][2];
  tij[0][1] = mu * (dudxjc[1][0] + dudxjc[0][1]);
  tij[0][2] = mu * (dudxjc[2][0] + dudxjc[0][2]);
  tij[1][2] = mu * (dudxjc[2][1] + dudxjc[1][2]);
  tij[1][0] = tij[0][1];
  tij[2][0] = tij[0][2];
  tij[2][1] = tij[1][2];

  // compute heat flux vector
  double qj[3];
  qj[0] = -kappa * dTdxjc[0];
  qj[1] = -kappa * dTdxjc[1];
  qj[2] = -kappa * dTdxjc[2];

  // compute volume term
  r[0][0] = 0.0;
  r[0][1] = tij[0][0];
  r[0][2] = tij[1][0];
  r[0][3] = tij[2][0];
  r[0][4] = nu*(ucg[0] * tij[0][0] + ucg[1] * tij[1][0] + ucg[2] * tij[2][0]) - qj[0];
  r[1][0] = 0.0;
  r[1][1] = tij[0][1];
  r[1][2] = tij[1][1];
  r[1][3] = tij[2][1];
  r[1][4] = nu*(ucg[0] * tij[0][1] + ucg[1] * tij[1][1] + ucg[2] * tij[2][1]) - qj[1];
  r[2][0] = 0.0;
  r[2][1] = tij[0][2];
  r[2][2] = tij[1][2];
  r[2][3] = tij[2][2];
  r[2][4] = nu*(ucg[0] * tij[0][2] + ucg[1] * tij[1][2] + ucg[2] * tij[2][2]) - qj[2];
}

//------------------------------------------------------------------------------

template<int dim>
void WallFcn::computeJacobianVolumeTerm(int code, Vec3D& normal, double delta, double *Vwall, int idxf, double *V[4], int tag,
                                        double dp1dxj[4][3],  double _mu, double dmu[4][dim],
                                        double _lambda, double dlambda[4][dim], double _kappa, double dkappa[4][dim],
                                        double (*dRdU)[3][dim][dim]) {
  typedef Eigen::AutoDiffScalar<Eigen::Matrix<double, 1, 4*dim>> Scalar;
  Scalar mu; mu.value() = _mu; for(int i=0; i<4; ++i) for(int j=0; j<dim; ++j) mu.derivatives()[dim*i+j] = dmu[i][j];
  Scalar lambda; lambda.value() = _lambda; for(int i=0; i<4; ++i) for(int j=0; j<dim; ++j) lambda.derivatives()[dim*i+j] = dlambda[i][j];
  Scalar kappa; kappa.value() = _kappa; for(int i=0; i<4; ++i) for(int j=0; j<dim; ++j) kappa.derivatives()[dim*i+j] = dkappa[i][j];

  double _rhow = varFcn->getDensity(V[idxf], tag);
  Vec3D uw(Vwall[1], Vwall[2], Vwall[3]);
  Vec3D uf = varFcn->getVelocity(V[idxf], tag);
  Vec3D du = uf - uw;
  double Tw = Vwall[4];
  double Tf = varFcn->computeTemperature(V[idxf], tag);
  double dT = Tf - Tw;
  double dTfdU[dim]; varFcn->computeJacobianTemperature(V[idxf], dTfdU, tag);
  double _muw = viscoFcn->compute_mu(Tf, tag);
  double norm = sqrt(normal * normal);
  Vec3D n = (1.0 / norm) * normal;
  Vec3D _t = computeTangentVector(n, du);
  double _utau = computeFrictionVelocity(_t, delta, _rhow, du, _muw);
  double _Ttau = (code == BC_ISOTHERMAL_WALL_MOVING || code == BC_ISOTHERMAL_WALL_FIXED) ? computeFrictionTemperature(_utau, delta, _rhow, dT, _muw) : 0;

  Scalar rhow; rhow.value() = _rhow; rhow.derivatives().setZero();
  Scalar muw; muw.value() = _muw; muw.derivatives().setZero();
  Scalar t[3]; for(int i=0; i<3; ++i) { t[i].value() = _t[i]; t[i].derivatives().setZero(); }
  Scalar utau; utau.value() = _utau; utau.derivatives().setZero();
  Scalar Ttau; Ttau.value() = _Ttau; Ttau.derivatives().setZero();
  for(int i = 0; i < 5; ++i) {
    double drhowdui = (i == 0) ? 1.0 : 0.0;
    double dmuwdui = viscoFcn->compute_muDerivative(Tf, dTfdU[i], 0, tag);
    Vec3D ddudui;
    for(int j = 0; j < 3; ++j) { if(i == 0) ddudui[j] = -uf[j] / _rhow; else if(i == j+1) ddudui[j] = 1 / _rhow; else ddudui[j] = 0; }
    Vec3D dtdui = computeDerivativeOfTangentVector(n, du, ddudui);
    double dutaudui = computeDerivativeOfFrictionVelocity(_t, dtdui, delta, 0.0, _rhow, drhowdui, du, ddudui, _muw, dmuwdui, 0);
    rhow.derivatives()[dim * idxf + i] = drhowdui;
    muw.derivatives()[dim * idxf + i] = dmuwdui;
    for(int j = 0; j < 3; ++j) t[j].derivatives()[dim * idxf + i] = dtdui[j];
    utau.derivatives()[dim * idxf + i] = dutaudui;
    if(code == BC_ISOTHERMAL_WALL_MOVING || code == BC_ISOTHERMAL_WALL_FIXED) {
      double dTtaudui = computeDerivativeOfFrictionTemperature(_utau, dutaudui, delta, 0.0, _rhow, drhowdui, dT, dTfdU[i], _muw, dmuwdui, 0);
      Ttau.derivatives()[dim * idxf + i] = dTtaudui;
    }
  }
  Scalar dudn = reynolds * utau * utau * rhow / muw;
  Scalar dTdn;
  if(code == BC_ISOTHERMAL_WALL_MOVING || code == BC_ISOTHERMAL_WALL_FIXED) {
    double gamw = varFcn->specificHeatCstPressure(tag);
    dTdn = gamw * rhow * utau * Ttau * norm;
  }
  else { dTdn.value() = 0; dTdn.derivatives().setZero(); }

  // velocity
  Scalar u[4][3];
  for(int i = 0; i < 4; ++i) {
    Vec3D ui = varFcn->getVelocity(V[i], tag);
    double rhoi = varFcn->getDensity(V[i], tag);
    for(int j = 0; j < 3; ++j) {
      u[i][j].value() = ui[j];
      u[i][j].derivatives().setZero();
      u[i][j].derivatives()[dim * i] = -ui[j] / rhoi;
      u[i][j].derivatives()[dim * i + j + 1] = 1. / rhoi;
    }
  }
  Scalar ucg[3];
  for(int i = 0; i < 3; ++i) {
    ucg[i] = 0.25 * (u[0][i] + u[1][i] + u[2][i] + u[3][i]);
  }

  // velocity gradient
  Scalar dudxj[3][3];
  dudxj[0][0] = dp1dxj[0][0] * u[0][0] + dp1dxj[1][0] * u[1][0] + dp1dxj[2][0] * u[2][0] + dp1dxj[3][0] * u[3][0];
  dudxj[0][1] = dp1dxj[0][1] * u[0][0] + dp1dxj[1][1] * u[1][0] + dp1dxj[2][1] * u[2][0] + dp1dxj[3][1] * u[3][0];
  dudxj[0][2] = dp1dxj[0][2] * u[0][0] + dp1dxj[1][2] * u[1][0] + dp1dxj[2][2] * u[2][0] + dp1dxj[3][2] * u[3][0];
  dudxj[1][0] = dp1dxj[0][0] * u[0][1] + dp1dxj[1][0] * u[1][1] + dp1dxj[2][0] * u[2][1] + dp1dxj[3][0] * u[3][1];
  dudxj[1][1] = dp1dxj[0][1] * u[0][1] + dp1dxj[1][1] * u[1][1] + dp1dxj[2][1] * u[2][1] + dp1dxj[3][1] * u[3][1];
  dudxj[1][2] = dp1dxj[0][2] * u[0][1] + dp1dxj[1][2] * u[1][1] + dp1dxj[2][2] * u[2][1] + dp1dxj[3][2] * u[3][1];
  dudxj[2][0] = dp1dxj[0][0] * u[0][2] + dp1dxj[1][0] * u[1][2] + dp1dxj[2][0] * u[2][2] + dp1dxj[3][0] * u[3][2];
  dudxj[2][1] = dp1dxj[0][1] * u[0][2] + dp1dxj[1][1] * u[1][2] + dp1dxj[2][1] * u[2][2] + dp1dxj[3][1] * u[3][2];
  dudxj[2][2] = dp1dxj[0][2] * u[0][2] + dp1dxj[1][2] * u[1][2] + dp1dxj[2][2] * u[2][2] + dp1dxj[3][2] * u[3][2];

  // velocity gradient correction
  Scalar a = dudn - ( t[0] * (dudxj[0][0] * n[0] + dudxj[0][1] * n[1] + dudxj[0][2] * n[2]) +
                      t[1] * (dudxj[1][0] * n[0] + dudxj[1][1] * n[1] + dudxj[1][2] * n[2]) +
                      t[2] * (dudxj[2][0] * n[0] + dudxj[2][1] * n[1] + dudxj[2][2] * n[2]) );
  Scalar dudxjc[3][3];
  for(int i = 0; i < 3; ++i) {
    for(int j = 0; j < 3; ++j) {
      dudxjc[i][j] = dudxj[i][j] + a * t[i] * n[j];
    }
  }

  // temperature
  Scalar T[4];
  for(int i = 0; i < 4; ++i) {
    double Ti = varFcn->computeTemperature(V[i], tag);
    double dTdU[dim]; varFcn->computeJacobianTemperature(V[i], dTdU, tag);
    T[i].value() = Ti;
    T[i].derivatives().setZero();
    for(int j = 0; j < dim; ++j) T[i].derivatives()[dim * i + j] = dTdU[j];
  }

  // temperature gradient
  Scalar dTdxj[3];
  dTdxj[0] = dp1dxj[0][0] * T[0] + dp1dxj[1][0] * T[1] + dp1dxj[2][0] * T[2] + dp1dxj[3][0] * T[3];
  dTdxj[1] = dp1dxj[0][1] * T[0] + dp1dxj[1][1] * T[1] + dp1dxj[2][1] * T[2] + dp1dxj[3][1] * T[3];
  dTdxj[2] = dp1dxj[0][2] * T[0] + dp1dxj[1][2] * T[1] + dp1dxj[2][2] * T[2] + dp1dxj[3][2] * T[3];

  // temperature gradient correction
  Scalar b = dTdn - (n[0] * dTdxj[0] + n[1] * dTdxj[1] + n[2] * dTdxj[2]);
  Scalar dTdxjc[3];
  for(int i = 0; i < 3; ++i) {
    dTdxjc[i] = dTdxj[i] + b * n[i];
  }

  // no eddy viscosity from LES
  Scalar nu = 1.;

  // compute stress tensor
  Scalar div = dudxjc[0][0] + dudxjc[1][1] + dudxjc[2][2];
  Scalar tij[3][3];
  tij[0][0] = lambda * div + 2.0 * mu * dudxjc[0][0];
  tij[1][1] = lambda * div + 2.0 * mu * dudxjc[1][1];
  tij[2][2] = lambda * div + 2.0 * mu * dudxjc[2][2];
  tij[0][1] = mu * (dudxjc[1][0] + dudxjc[0][1]);
  tij[0][2] = mu * (dudxjc[2][0] + dudxjc[0][2]);
  tij[1][2] = mu * (dudxjc[2][1] + dudxjc[1][2]);
  tij[1][0] = tij[0][1];
  tij[2][0] = tij[0][2];
  tij[2][1] = tij[1][2];

  // compute heat flux vector
  Scalar qj[3];
  qj[0] = -kappa * dTdxjc[0];
  qj[1] = -kappa * dTdxjc[1];
  qj[2] = -kappa * dTdxjc[2];

  // compute volume term
  Scalar r[3][5];
  r[0][0] = 0.0;
  r[0][1] = tij[0][0];
  r[0][2] = tij[1][0];
  r[0][3] = tij[2][0];
  r[0][4] = nu * (ucg[0] * tij[0][0] + ucg[1] * tij[1][0] + ucg[2] * tij[2][0]) - qj[0];
  r[1][0] = 0.0;
  r[1][1] = tij[0][1];
  r[1][2] = tij[1][1];
  r[1][3] = tij[2][1];
  r[1][4] = nu * (ucg[0] * tij[0][1] + ucg[1] * tij[1][1] + ucg[2] * tij[2][1]) - qj[1];
  r[2][0] = 0.0;
  r[2][1] = tij[0][2];
  r[2][2] = tij[1][2];
  r[2][3] = tij[2][2];
  r[2][4] = nu * (ucg[0] * tij[0][2] + ucg[1] * tij[1][2] + ucg[2] * tij[2][2]) - qj[2];

  for(int i = 0; i < 4; ++i) {
    for(int j = 0; j < 3; ++j) {
      for(int k = 0; k < 5; ++k) {
        for(int l = 0; l < dim; ++l) {
          dRdU[i][j][k][l] = r[j][k].derivatives()[dim * i + l];
        }
      }
    }
  }
}

//------------------------------------------------------------------------------

template<int dim>
void WallFcn::computeDerivativeOfVolumeTerm(int code, Vec3D& _normal, Vec3D& dnormal, double delta, double ddelta, double *Vwall, int idxf, double *V[4], double *dV[4],
                                int tag, double _dudxj[3][3], double ddudxj[3][3], double _dTdxj[3], double ddTdxj[3], double nu, double _mu, double dmu, double _lambda,
                                double dlambda, double _kappa, double dkappa, double _ucg[3], double ducg[3], double (*dr)[dim], double (*_r)[dim]) {
  typedef Eigen::AutoDiffScalar<Eigen::Matrix<double, 1, 1>> Scalar;
  // unpack into autodiff variables
  Scalar mu; mu.value() = _mu; mu.derivatives()[0] = dmu;
  Scalar lambda; lambda.value() = _lambda; lambda.derivatives()[0] = dlambda;
  Scalar kappa; kappa.value() = _kappa; kappa.derivatives()[0] = dkappa;
  Scalar dudxj[3][3]; for(int i = 0; i < 3; i++) { for(int j = 0; j < 3; ++j) {dudxj[i][j].value() = _dudxj[i][j]; dudxj[i][j].derivatives()[0] = ddudxj[i][j];}}
  Scalar dTdxj[3]; for(int i = 0; i < 3; i++) {dTdxj[i].value() = _dTdxj[i]; dTdxj[i].derivatives()[0] = ddTdxj[i];}
  Scalar ucg[3]; for(int i = 0; i < 3; ++i) { ucg[i].value() = _ucg[i]; ucg[i].derivatives()[0] = ducg[i];}
  Scalar normal[3]; for(int i = 0; i < 3; i++) {normal[i].value() = _normal[i]; normal[i].derivatives()[0] = dnormal[i];}

  double _rhow = varFcn->getDensity(V[idxf], tag);
  double drhow = varFcn->getDensity(dV[idxf], tag);
  Scalar rhow; rhow.value() = _rhow; rhow.derivatives()[0] = drhow;

  Vec3D uw(Vwall[1], Vwall[2], Vwall[3]); // no derivative here
  Vec3D duw(0.0);
  Vec3D uf = varFcn->getVelocity(V[idxf], tag);
  Vec3D duf = varFcn->getVelocity(dV[idxf],tag);
  Vec3D du = uf - uw;
  Vec3D ddu = duf - duw;

  double Tw = Vwall[4];
  double dTw = 0.0;
  double Tf = varFcn->computeTemperature(V[idxf], tag);
  double dTf = varFcn->computeDerivativeOfTemperature(V[idxf], dV[idxf],tag);
  double dT = Tf - Tw;
  double ddT = dTf - dTw;

  double _muw = viscoFcn->compute_mu(Tf, tag);
  double dmuw = viscoFcn->compute_muDerivative(Tf, dTf, 0, tag);
  Scalar muw; muw.value() = _muw; muw.derivatives()[0] = dmuw;

  Scalar norm = sqrt(normal[0] * normal[0] + normal[1] * normal[1] + normal[2] * normal[2]);
  Scalar n[3]; for(int i = 0; i < 3; ++i) n[i] = normal[i] / norm;
  Vec3D n_Vec3D(n[0].value(), n[1].value(),n[2].value());
  Vec3D dn_Vec3D(n[0].derivatives()[0], n[1].derivatives()[0], n[2].derivatives()[0]);
  Vec3D _t = computeTangentVector(n_Vec3D, du);
  Vec3D dt = computeDerivativeOfTangentVector(n_Vec3D, dn_Vec3D, du, ddu);
  Scalar t[3]; for(int i=0; i<3; ++i) { t[i].value() = _t[i]; t[i].derivatives()[0] = dt[i]; }

  double _utau = computeFrictionVelocity(_t, delta, _rhow, du, _muw);
  double dutau = computeDerivativeOfFrictionVelocity(_t, dt, delta, ddelta, rhow.value(), rhow.derivatives()[0], du, ddu, _muw, dmuw, 0);
  Scalar utau; utau.value() = _utau; utau.derivatives()[0] = dutau;

  double _Ttau = (code == BC_ISOTHERMAL_WALL_MOVING || code == BC_ISOTHERMAL_WALL_FIXED) ? computeFrictionTemperature(_utau, delta, _rhow, dT, _muw) : 0;
  double dTtau = 0.0;
  if(code == BC_ISOTHERMAL_WALL_MOVING || code == BC_ISOTHERMAL_WALL_FIXED) {
    dTtau = computeDerivativeOfFrictionTemperature(_utau, dutau, delta, ddelta, _rhow, drhow, dT, ddT, _muw, dmuw, 0);
  }
  Scalar Ttau; Ttau.value() = _Ttau; Ttau.derivatives()[0] = dTtau;

  // set up auto-diff variables
  Scalar dudn = reynolds * utau * utau * rhow / muw;
  Scalar dTdn;
  if(code == BC_ISOTHERMAL_WALL_MOVING || code == BC_ISOTHERMAL_WALL_FIXED) {
    double gamw = varFcn->specificHeatCstPressure(tag);
    dTdn = gamw * rhow * utau * Ttau * norm;
  }
  else { dTdn.value() = 0; dTdn.derivatives().setZero(); }

  // velocity gradient correction
  Scalar a = dudn - ( t[0] * (dudxj[0][0] * n[0] + dudxj[0][1] * n[1] + dudxj[0][2] * n[2]) +
                      t[1] * (dudxj[1][0] * n[0] + dudxj[1][1] * n[1] + dudxj[1][2] * n[2]) +
                      t[2] * (dudxj[2][0] * n[0] + dudxj[2][1] * n[1] + dudxj[2][2] * n[2]) );

  Scalar dudxjc[3][3]; for(int i = 0; i < 3; ++i) { for(int j = 0; j < 3; ++j) {dudxjc[i][j] = dudxj[i][j] + a * t[i] * n[j];}}

  // temperature gradient correction
  Scalar b = dTdn - (n[0] * dTdxj[0] + n[1] * dTdxj[1] + n[2] * dTdxj[2]);
  Scalar dTdxjc[3];
  for(int i = 0; i < 3; ++i) {
    dTdxjc[i] = dTdxj[i] + b * n[i];
  }

  // compute stress tensor
  Scalar div = dudxjc[0][0] + dudxjc[1][1] + dudxjc[2][2];
  Scalar tij[3][3];
  tij[0][0] = lambda * div + 2.0 * mu * dudxjc[0][0];
  tij[1][1] = lambda * div + 2.0 * mu * dudxjc[1][1];
  tij[2][2] = lambda * div + 2.0 * mu * dudxjc[2][2];
  tij[0][1] = mu * (dudxjc[1][0] + dudxjc[0][1]);
  tij[0][2] = mu * (dudxjc[2][0] + dudxjc[0][2]);
  tij[1][2] = mu * (dudxjc[2][1] + dudxjc[1][2]);
  tij[1][0] = tij[0][1];
  tij[2][0] = tij[0][2];
  tij[2][1] = tij[1][2];

  // compute heat flux vector
  Scalar qj[3];
  qj[0] = -kappa * dTdxjc[0];
  qj[1] = -kappa * dTdxjc[1];
  qj[2] = -kappa * dTdxjc[2];

  // compute volume term
  Scalar r[3][5];
  r[0][0] = 0.0;
  r[0][1] = tij[0][0];
  r[0][2] = tij[1][0];
  r[0][3] = tij[2][0];
  r[0][4] = nu * (ucg[0] * tij[0][0] + ucg[1] * tij[1][0] + ucg[2] * tij[2][0]) - qj[0];
  r[1][0] = 0.0;
  r[1][1] = tij[0][1];
  r[1][2] = tij[1][1];
  r[1][3] = tij[2][1];
  r[1][4] = nu * (ucg[0] * tij[0][1] + ucg[1] * tij[1][1] + ucg[2] * tij[2][1]) - qj[1];
  r[2][0] = 0.0;
  r[2][1] = tij[0][2];
  r[2][2] = tij[1][2];
  r[2][3] = tij[2][2];
  r[2][4] = nu * (ucg[0] * tij[0][2] + ucg[1] * tij[1][2] + ucg[2] * tij[2][2]) - qj[2];

  for(int j = 0; j < 3; ++j) {
    for(int k = 0; k < 5; ++k) {
      dr[j][k] = r[j][k].derivatives()[0];
      if(_r) _r[j][k] = r[j][k].value();
    }
  }
}

#ifndef _FLUX_FCN_DESC_JWL_H_
#define _FLUX_FCN_DESC_JWL_H_

#include <FluxFcnDesc.h>
#include <VarFcnJwl.h>

class IoData;

//------------------------------------------------------------------------------

class FluxFcnJwlApprJacRoeEuler3D : public FluxFcnApprJacRoeEuler3D {

 public:
  FluxFcnJwlApprJacRoeEuler3D(int rs, double gg, IoData& ioData, VarFcnJwl *varFcnJwl) :
    FluxFcnApprJacRoeEuler3D(ioData, rs, gg, varFcnJwl) {}
  ~FluxFcnJwlApprJacRoeEuler3D() {
    vf = 0;
  }

  void compute(double, double, double *, double *, double, double *, double *, double *, bool);
  void computeJacobians(double, double, double *, double *, double, double *, double *, double *, double *, FluxFcnBase::Type, bool);

};

//------------------------------------------------------------------------------

class FluxFcnJwlWallEuler3D : public FluxFcnWallEuler3D {

 public:
  FluxFcnJwlWallEuler3D(IoData& ioData, VarFcnJwl *varFcnJwl) :
    FluxFcnWallEuler3D(ioData, varFcnJwl) {}
  ~FluxFcnJwlWallEuler3D() {
    vf = 0;
  }

  void compute(double, double, double *, double *, double, double *, double *, double *, bool);
  void computeJacobian(double, double, double *, double *, double, double *, double *, double *, FluxFcnBase::Type, bool);

};

//------------------------------------------------------------------------------

class FluxFcnJwlGhidagliaEuler3D : public FluxFcnGhidagliaEuler3D {

 public:
  FluxFcnJwlGhidagliaEuler3D(IoData& ioData, VarFcnJwl *varFcnJwl) :
    FluxFcnGhidagliaEuler3D(varFcnJwl) {}
  ~FluxFcnJwlGhidagliaEuler3D() {
    vf = 0;
  }

  void compute(double, double, double *, double *, double, double *, double *, double *, bool);

};

//------------------------------------------------------------------------------

#endif

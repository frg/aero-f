#ifndef _IMPLICIT_POSTPRO_ROM_TS_DESC_H_
#define _IMPLICIT_POSTPRO_ROM_TS_DESC_H_

#include <ImplicitRomTsDesc.h>

template<class Scalar, int dim> class DistSVec;
template<int dim> class EmbeddedPOD;

//------------------------------------------------------------------------------

template<int dim>
class ImplicitPostproRomTsDesc : public ImplicitRomTsDesc<dim> {

  FILE *fpReducedCoords;
  FILE *fpNeighborInds;
  FILE *fpSurfacePosition;
  Vec<Vec3D> *Xs;
  int *numNodes;
  Vec<int> nearestNeighborIndices;

 protected:
  bool isPostproRom() { return true; };

 public:
  ImplicitPostproRomTsDesc(IoData&, GeoSource&, Domain *);
  virtual ~ImplicitPostproRomTsDesc();

  int solveNonLinearSystem(DistSVec<double, dim>&, int, double, DistSVec<double, dim> * = nullptr);
  bool formInitialCondition(DistSVec<double, dim>&);
};

//------------------------------------------------------------------------------

#endif

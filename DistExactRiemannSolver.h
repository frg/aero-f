#ifndef _DIST_EXACT_RIEMANN_H_
#define _DIST_EXACT_RIEMANN_H_

#include <NodeData.h>

class IoData;
class Domain;
class VarFcn;
class SparseGridCluster;

template<class Scalar, int dim> class DistSVec;
template<class Scalar> class DistVec;
template<int dim> class ExactRiemannSolver;

//------------------------------------------------------------------------------

template<int dim>
class DistExactRiemannSolver {

  Domain *dom;
  DistSVec<double, dim> *riemannupdate;
  DistVec<double> *weight;
  ExactRiemannSolver<dim> **subExactRiemannSolver;
  SparseGridCluster *tabulationC;
  DistVec<int> *fluidIdToSet;

 public:
  DistExactRiemannSolver(IoData&, Domain *, VarFcn *);
  ~DistExactRiemannSolver();

  void resize(NodeData *);

  DistSVec<double, dim> *getRiemannUpdate() const {
    return riemannupdate;
  }

  DistVec<double> *getRiemannWeight() const {
    return weight;
  }

  DistVec<int> *getFluidIdToSet() const {
    return fluidIdToSet;
  }

  ExactRiemannSolver<dim>& operator()(int i) const {
    return *subExactRiemannSolver[i];
  }

};

//------------------------------------------------------------------------------

#endif

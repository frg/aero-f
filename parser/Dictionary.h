#ifndef _DICTIONARY
#define _DICTIONARY

#include <map>
#include <string>

class Assigner;

typedef std::map<std::string, int> DictMap;
typedef std::map<int, std::string> TokenMap;

// SysSmbMap is now a structure so that it can be properly deleted.
class SysSmbMap {
 public:
  std::map<int, Assigner *> forest;
  SysSmbMap() {}
  ~SysSmbMap();
};

class Dictionary {
  int nToken;
  DictMap map;
  TokenMap tkm;
 public:
  Dictionary() {
    nToken = 0;
  }
  int token(const char *text) {
    DictMap::iterator it = map.find(text);
    if(it != map.end()) {
      return it->second;
    }
    map[text] = nToken;
    tkm[nToken] = text;
    return nToken++;
  }
  std::string& word(int tk) {
    return tkm[tk];
  }
  ~Dictionary() {}
 private:
};

extern Dictionary *dictionary;
extern SysSmbMap *sysSmb;

int addSysSymbol(const char *, Assigner *);
int findSysToken(const char *);
Assigner *findSysObj(int tk);

#endif

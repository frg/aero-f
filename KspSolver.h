#ifndef _KSP_SOLVER_H_
#define _KSP_SOLVER_H_

#include <DenseMatrix.h>
#include <KspConvCriterion.h>

#include <Eigen/Dense>

#include <cstdio>
#include <functional>
#include <vector>

class Communicator;
class KspData;
class KspConvCriterion;
class Timer;

template<class Scalar> class Vec;
template<class Scalar> class DenseMat;

#ifndef _KSPSLVR_TMPL_
  #define _KSPSLVR_TMPL_
  template<class VecType, class MvpOp, class PrecOp, class IoOp, class ScalarT = double> class KspSolver;
#endif

#ifndef _KSPSLVR2_TMPL_
  #define _KSPSLVR2_TMPL_
  template<class VecType, class MatVecProdOp, class PrecOp, class IoOp, class ScalarT = double> class GmresSolver;
  template<class VecType, class MatVecProdOp, class PrecOp, class IoOp, class ScalarT = double> class GcrSolver;
  template<class VecType, class MatVecProdOp, class PrecOp, class IoOp, class ScalarT = double> class AugmentedGmresSolver;
#endif

//------------------------------------------------------------------------------

template<class VecType, class MatVecProdOp, class PrecOp, class IoOp, class ScalarT>
class KspSolver {

 protected:
  int typePrec;
  double eps;
  double absoluteEps;
  bool checkFinalRes;
  MatVecProdOp *mvpOp;
  void (MatVecProdOp::*apply)(VecType&, VecType&);
  PrecOp *pcOp;
  IoOp *ioOp;
  KspConvCriterion *kspConvCriterion;
  FILE *output;
  Timer *timer;

 public:
  int maxits;

  KspSolver() {}
  KspSolver(KspData&, MatVecProdOp *, PrecOp *, IoOp *, Timer *,
            void (MatVecProdOp::*)(VecType&, VecType&));
  virtual ~KspSolver() {
    if(kspConvCriterion) {
      delete kspConvCriterion;
    }
  }

  virtual void resize(const typename VecType::InfoType&) = 0;

  void setup(int, int, VecType&);

  void setMaxIts(int i) {
    maxits = i;
  }
  void setEps(double e) {
    eps = e;
  }
  void disableOutput() {
    output = NULL;
  }

  virtual int solve(VecType&, VecType&) = 0;

  void printParam() {
    ioOp->fprintf(stderr, " solver params: %d maxits, %e eps\n", maxits, eps);
  }

};

//------------------------------------------------------------------------------

template<class VecType, class MatVecProdOp, class PrecOp, class IoOp>
class RichardsonSolver : public KspSolver<VecType, MatVecProdOp, PrecOp, IoOp> {

  VecType dx, r;

  using KspSolver<VecType, MatVecProdOp, PrecOp, IoOp>::typePrec;
  using KspSolver<VecType, MatVecProdOp, PrecOp, IoOp>::eps;
  using KspSolver<VecType, MatVecProdOp, PrecOp, IoOp>::absoluteEps;
  using KspSolver<VecType, MatVecProdOp, PrecOp, IoOp>::checkFinalRes;
  using KspSolver<VecType, MatVecProdOp, PrecOp, IoOp>::mvpOp;
  using KspSolver<VecType, MatVecProdOp, PrecOp, IoOp>::apply;
  using KspSolver<VecType, MatVecProdOp, PrecOp, IoOp>::pcOp;
  using KspSolver<VecType, MatVecProdOp, PrecOp, IoOp>::ioOp;
  using KspSolver<VecType, MatVecProdOp, PrecOp, IoOp>::kspConvCriterion;
  using KspSolver<VecType, MatVecProdOp, PrecOp, IoOp>::output;
  using KspSolver<VecType, MatVecProdOp, PrecOp, IoOp>::maxits;
  using KspSolver<VecType, MatVecProdOp, PrecOp, IoOp>::timer;

 public:

  RichardsonSolver(const typename VecType::InfoType&, int, KspData&,
                   MatVecProdOp *, PrecOp *, IoOp *, Timer *,
                   void (MatVecProdOp::*)(VecType&, VecType&) = &MatVecProdOp::apply);
  ~RichardsonSolver() {}

  void resize(const typename VecType::InfoType&);

  int solve(VecType&, VecType&);

};

//------------------------------------------------------------------------------

template<class VecType, class MatVecProdOp, class PrecOp, class IoOp>
class CgSolver : public KspSolver<VecType, MatVecProdOp, PrecOp, IoOp> {

  VecType r, Ap, y, p;

  using KspSolver<VecType, MatVecProdOp, PrecOp, IoOp>::typePrec;
  using KspSolver<VecType, MatVecProdOp, PrecOp, IoOp>::eps;
  using KspSolver<VecType, MatVecProdOp, PrecOp, IoOp>::absoluteEps;
  using KspSolver<VecType, MatVecProdOp, PrecOp, IoOp>::checkFinalRes;
  using KspSolver<VecType, MatVecProdOp, PrecOp, IoOp>::mvpOp;
  using KspSolver<VecType, MatVecProdOp, PrecOp, IoOp>::apply;
  using KspSolver<VecType, MatVecProdOp, PrecOp, IoOp>::pcOp;
  using KspSolver<VecType, MatVecProdOp, PrecOp, IoOp>::ioOp;
  using KspSolver<VecType, MatVecProdOp, PrecOp, IoOp>::kspConvCriterion;
  using KspSolver<VecType, MatVecProdOp, PrecOp, IoOp>::output;
  using KspSolver<VecType, MatVecProdOp, PrecOp, IoOp>::maxits;
  using KspSolver<VecType, MatVecProdOp, PrecOp, IoOp>::timer;

 public:
  CgSolver(const typename VecType::InfoType&, int, KspData&,
           MatVecProdOp *, PrecOp *, IoOp *, Timer *,
           void (MatVecProdOp::*)(VecType&, VecType&) = &MatVecProdOp::apply);
  ~CgSolver() {}

  void resize(const typename VecType::InfoType&);

  int solve(VecType&, VecType&);

};

//------------------------------------------------------------------------------

template<class VecType, class MatVecProdOp, class PrecOp, class IoOp, class ScalarT>
class GmresSolver : public KspSolver<VecType, MatVecProdOp, PrecOp, IoOp, ScalarT> {

  int numVec;
  DenseMat<ScalarT> cs, H;
  Vec<ScalarT> g, y;
  VecType w, r;
  std::vector<VecType> V;

  using KspSolver<VecType, MatVecProdOp, PrecOp, IoOp, ScalarT>::typePrec;
  using KspSolver<VecType, MatVecProdOp, PrecOp, IoOp, ScalarT>::eps;
  using KspSolver<VecType, MatVecProdOp, PrecOp, IoOp, ScalarT>::absoluteEps;
  using KspSolver<VecType, MatVecProdOp, PrecOp, IoOp, ScalarT>::checkFinalRes;
  using KspSolver<VecType, MatVecProdOp, PrecOp, IoOp, ScalarT>::mvpOp;
  using KspSolver<VecType, MatVecProdOp, PrecOp, IoOp, ScalarT>::apply;
  using KspSolver<VecType, MatVecProdOp, PrecOp, IoOp, ScalarT>::pcOp;
  using KspSolver<VecType, MatVecProdOp, PrecOp, IoOp, ScalarT>::ioOp;
  using KspSolver<VecType, MatVecProdOp, PrecOp, IoOp, ScalarT>::kspConvCriterion;
  using KspSolver<VecType, MatVecProdOp, PrecOp, IoOp, ScalarT>::output;
  using KspSolver<VecType, MatVecProdOp, PrecOp, IoOp, ScalarT>::maxits;
  using KspSolver<VecType, MatVecProdOp, PrecOp, IoOp, ScalarT>::timer;

 public:
  GmresSolver(const typename VecType::InfoType&, int, KspData&,
              MatVecProdOp *, PrecOp *, IoOp *, Timer *,
              void (MatVecProdOp::*)(VecType&, VecType&) = &MatVecProdOp::apply);
  ~GmresSolver() {}

  void resize(const typename VecType::InfoType&);

  int solve(VecType&, VecType&);

 private:
  void applyPreviousRotations(int, DenseMat<ScalarT>&, DenseMat<ScalarT>&);
  void applyNewRotation(int, DenseMat<ScalarT>&, DenseMat<ScalarT>&, Vec<ScalarT>&);
  void backwardSolve(int, DenseMat<ScalarT>&, Vec<ScalarT>&, Vec<ScalarT>&);

};

//------------------------------------------------------------------------------

template<class VecType, class MatVecProdOp, class PrecOp, class IoOp, class ScalarT>
class AugmentedGmresSolver : public KspSolver<VecType, MatVecProdOp, PrecOp, IoOp, ScalarT> {

  using Mat = Eigen::Matrix<ScalarT, Eigen::Dynamic, Eigen::Dynamic>;
  using Vec = Eigen::Matrix<ScalarT, Eigen::Dynamic, 1>;

  bool multipleLhs;

  int numVec;
  int deflationSize;
  int nThreads = 1;
  Communicator *com;
  // size needed for all the master portion of interface vectors
  int halfSize;
  // matrix space for V
  Mat Vspace;
  // matrix space for H
  Mat Hspace;
  // space for R such that H = P R
  Mat Rspace;
  // set at the end of a solution to the number of valid vectors in Q
  int krylovSize;
  // vector of Givens rotations
  std::vector<Eigen::JacobiRotation<ScalarT>> givens;
  // deflation direction matrix
  Mat U;
  // unit matrix C = M A U, C^H C = I
  Mat C;
  // B = C^H Q
  Mat Bspace;
  // work data
  VecType rd, zd, vd, wd;
  Vec gs, v;

  std::function<void(VecType&)> assemble;

  using KspSolver<VecType, MatVecProdOp, PrecOp, IoOp, ScalarT>::typePrec;
  using KspSolver<VecType, MatVecProdOp, PrecOp, IoOp, ScalarT>::eps;
  using KspSolver<VecType, MatVecProdOp, PrecOp, IoOp, ScalarT>::absoluteEps;
  using KspSolver<VecType, MatVecProdOp, PrecOp, IoOp, ScalarT>::checkFinalRes;
  using KspSolver<VecType, MatVecProdOp, PrecOp, IoOp, ScalarT>::mvpOp;
  using KspSolver<VecType, MatVecProdOp, PrecOp, IoOp, ScalarT>::apply;
  using KspSolver<VecType, MatVecProdOp, PrecOp, IoOp, ScalarT>::pcOp;
  using KspSolver<VecType, MatVecProdOp, PrecOp, IoOp, ScalarT>::ioOp;
  using KspSolver<VecType, MatVecProdOp, PrecOp, IoOp, ScalarT>::kspConvCriterion;
  using KspSolver<VecType, MatVecProdOp, PrecOp, IoOp, ScalarT>::output;
  using KspSolver<VecType, MatVecProdOp, PrecOp, IoOp, ScalarT>::maxits;
  using KspSolver<VecType, MatVecProdOp, PrecOp, IoOp, ScalarT>::timer;

 public:
  AugmentedGmresSolver(const typename VecType::InfoType&, int, KspData&,
                       MatVecProdOp *, PrecOp *, IoOp *, Timer *, std::function<void(VecType&)>,
                       void (MatVecProdOp::*)(VecType&, VecType&) = &MatVecProdOp::apply);
  ~AugmentedGmresSolver() {}

  void resize(const typename VecType::InfoType&);

  int solve(VecType&, VecType&);

 private:
  void buildDeflation(int, int);
  double unifiedNorm(const Eigen::Ref<const Vec>);
  int iterate(double, int, double, int, int&, double&);
  void subspaceSolve(VecType&, VecType&, int);
  void update(VecType&, VecType&);

};

//------------------------------------------------------------------------------

template<class VecType, class MatVecProdOp, class PrecOp, class IoOp, class ScalarT>
class GcrSolver : public KspSolver<VecType, MatVecProdOp, PrecOp, IoOp, ScalarT> {

  int numVec;
  Vec<ScalarT> ApAp;
  VecType w, r, R, AR, temp, w0, x0;
  std::vector<VecType> p, Ap;

  using KspSolver<VecType, MatVecProdOp, PrecOp, IoOp, ScalarT>::typePrec;
  using KspSolver<VecType, MatVecProdOp, PrecOp, IoOp, ScalarT>::eps;
  using KspSolver<VecType, MatVecProdOp, PrecOp, IoOp, ScalarT>::absoluteEps;
  using KspSolver<VecType, MatVecProdOp, PrecOp, IoOp, ScalarT>::checkFinalRes;
  using KspSolver<VecType, MatVecProdOp, PrecOp, IoOp, ScalarT>::mvpOp;
  using KspSolver<VecType, MatVecProdOp, PrecOp, IoOp, ScalarT>::apply;
  using KspSolver<VecType, MatVecProdOp, PrecOp, IoOp, ScalarT>::pcOp;
  using KspSolver<VecType, MatVecProdOp, PrecOp, IoOp, ScalarT>::ioOp;
  using KspSolver<VecType, MatVecProdOp, PrecOp, IoOp, ScalarT>::kspConvCriterion;
  using KspSolver<VecType, MatVecProdOp, PrecOp, IoOp, ScalarT>::output;
  using KspSolver<VecType, MatVecProdOp, PrecOp, IoOp, ScalarT>::maxits;
  using KspSolver<VecType, MatVecProdOp, PrecOp, IoOp, ScalarT>::timer;

 public:
  int numCalcVec;
  GcrSolver(const typename VecType::InfoType&, int, KspData&,
            MatVecProdOp *, PrecOp *, IoOp *, Timer *,
            void (MatVecProdOp::*)(VecType&, VecType&) = &MatVecProdOp::apply);
  ~GcrSolver() {}

  void resize(const typename VecType::InfoType&);

  int solve(VecType&, VecType&);
  int solveMRhs(VecType&, VecType&);

};

//------------------------------------------------------------------------------

#ifdef TEMPLATE_FIX
  #include <KspSolver.C>
#endif

#endif

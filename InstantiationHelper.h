#include <Types.h>
#include <complex>

#include <boost/preprocessor/seq/elem.hpp>
#include <boost/preprocessor/seq/for_each_product.hpp>

//------------------------------------------------------------------------------

#define DIM_SEQ (5)(6)(7)
#define SEG_SEQ ((5,1))((5,2))
#ifdef TYPE_PREC
  #define REAL_SEQ   (double)(TYPE_PREC) 
  #define SCALAR_SEQ (double)(TYPE_PREC)(std::complex<double>)
#else
  #define REAL_SEQ   (double)
  #define SCALAR_SEQ (double)(std::complex<double>)
#endif

//------------------------------------------------------------------------------

#define DIM_INSTANCE(r,product) \
  BOOST_PP_SEQ_ELEM(0, product)(BOOST_PP_SEQ_ELEM(1, product))

#define DIM_INSTANTIATION_HELPER(macro) \
  BOOST_PP_SEQ_FOR_EACH_PRODUCT(DIM_INSTANCE,((macro))(DIM_SEQ))

//------------------------------------------------------------------------------

#define DIM_SCALAR_DIM_INSTANCE(r,product) \
  BOOST_PP_SEQ_ELEM(0, product)(BOOST_PP_SEQ_ELEM(1, product), \
                                BOOST_PP_SEQ_ELEM(2, product), \
                                BOOST_PP_SEQ_ELEM(1, product))
#define DIM_SCALAR_NEQ1_INSTANCE(r,product) \
  BOOST_PP_SEQ_ELEM(0, product)(BOOST_PP_TUPLE_ELEM(2, 0, BOOST_PP_SEQ_ELEM(1, product))+BOOST_PP_TUPLE_ELEM(2, 1, BOOST_PP_SEQ_ELEM(1, product)), \
                                BOOST_PP_SEQ_ELEM(2, product), \
                                BOOST_PP_TUPLE_ELEM(2, 0, BOOST_PP_SEQ_ELEM(1, product)))
#define DIM_SCALAR_NEQ2_INSTANCE(r,product) \
  BOOST_PP_SEQ_ELEM(0, product)(BOOST_PP_TUPLE_ELEM(2, 0, BOOST_PP_SEQ_ELEM(1, product))+BOOST_PP_TUPLE_ELEM(2, 1, BOOST_PP_SEQ_ELEM(1, product)), \
                                BOOST_PP_SEQ_ELEM(2, product), \
                                BOOST_PP_TUPLE_ELEM(2, 1, BOOST_PP_SEQ_ELEM(1, product)))

#define DIM_SCALAR_NEQ_INSTANTIATION_HELPER(macro) \
  BOOST_PP_SEQ_FOR_EACH_PRODUCT(DIM_SCALAR_DIM_INSTANCE, ((macro))(DIM_SEQ)(SCALAR_SEQ)) \
  BOOST_PP_SEQ_FOR_EACH_PRODUCT(DIM_SCALAR_NEQ1_INSTANCE,((macro))(SEG_SEQ)(REAL_SEQ)) \
  BOOST_PP_SEQ_FOR_EACH_PRODUCT(DIM_SCALAR_NEQ2_INSTANCE,((macro))(SEG_SEQ)(REAL_SEQ))

//------------------------------------------------------------------------------

#define DIM_DIM_INSTANCE(r,product) \
  BOOST_PP_SEQ_ELEM(0, product)(BOOST_PP_SEQ_ELEM(1, product), \
                                BOOST_PP_SEQ_ELEM(1, product))
#define DIM_NEQ1_INSTANCE(r,product) \
  BOOST_PP_SEQ_ELEM(0, product)(BOOST_PP_TUPLE_ELEM(2, 0, BOOST_PP_SEQ_ELEM(1, product))+BOOST_PP_TUPLE_ELEM(2, 1, BOOST_PP_SEQ_ELEM(1, product)), \
                                BOOST_PP_TUPLE_ELEM(2, 0, BOOST_PP_SEQ_ELEM(1, product)))
#define DIM_NEQ2_INSTANCE(r,product) \
  BOOST_PP_SEQ_ELEM(0, product)(BOOST_PP_TUPLE_ELEM(2, 0, BOOST_PP_SEQ_ELEM(1, product))+BOOST_PP_TUPLE_ELEM(2, 1, BOOST_PP_SEQ_ELEM(1, product)), \
                                BOOST_PP_TUPLE_ELEM(2, 1, BOOST_PP_SEQ_ELEM(1, product)))

#define DIM_NEQ_INSTANTIATION_HELPER(macro) \
  BOOST_PP_SEQ_FOR_EACH_PRODUCT(DIM_DIM_INSTANCE, ((macro))(DIM_SEQ)) \
  BOOST_PP_SEQ_FOR_EACH_PRODUCT(DIM_NEQ1_INSTANCE,((macro))(SEG_SEQ)) \
  BOOST_PP_SEQ_FOR_EACH_PRODUCT(DIM_NEQ2_INSTANCE,((macro))(SEG_SEQ))

//------------------------------------------------------------------------------

#define NEQ1_NEQ2_INSTANCE(r,product) \
  BOOST_PP_SEQ_ELEM(0, product)(BOOST_PP_TUPLE_ELEM(2, 0, BOOST_PP_SEQ_ELEM(1, product)), \
                                BOOST_PP_TUPLE_ELEM(2, 1, BOOST_PP_SEQ_ELEM(1, product)))

#define NEQ1_NEQ2_INSTANTIATION_HELPER(macro) \
  BOOST_PP_SEQ_FOR_EACH_PRODUCT(NEQ1_NEQ2_INSTANCE,((macro))(SEG_SEQ))

//------------------------------------------------------------------------------

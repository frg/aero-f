#ifndef _MATCH_NODE_H_
#define _MATCH_NODE_H_

class BinFileHandler;

//------------------------------------------------------------------------------

class MatchNodeSet {

 public:
  enum Type {BODYFITTED = 0, EMBEDDED = 1};

 private:
  Type type;
  int numNodes;

  int (*index)[3];
  double (*gap)[3];
  double (*xi)[3];
  double *normgap;

 public:

  MatchNodeSet(const char *, Type);
  MatchNodeSet(Type _type) {
    numNodes = 0;
    index = 0;
    gap = 0;
    xi = 0;
    normgap = 0;
    type = _type;
  }
  MatchNodeSet(int, Type);
  ~MatchNodeSet();

  void read(BinFileHandler&, int, int (*)[2]);
  void autoInit(int);
  template<class NodeMap>
  void renumberNodes(NodeMap&);
  void exportInfo(int, int (*)[3]);
  void setBufferPosition(int, int);
  void getDisplacement(int, double, double, double, bool *, double (*)[2][3], double (*)[3],
                       double (*)[3], double (*)[3], double (*)[3], double *);
  void getDisplacementSensitivity(bool *, double, double (*)[2][3], double (*)[3], double *);
  void getTotDisplacement(int, double, double, double, bool *, double (*)[2][3], double (*)[3],
                          double (*)[3], double *);
  double getTemperature(int, double, double, bool *, double *, double *);

  template<int dim>
  void send(double, double (*)[dim], double (*)[dim]);

  template<int dim>
  void sendWithMasterFlag(double, double (*)[dim], double (*)[dim], bool *);

  double (*getGap(int, int *))[3];

  void updateNumStNodes(int nn) {
    numNodes = nn;
  }
  int size() const {
    return numNodes;
  }
  int subMatchNode(int i) {
    return(index[i][0]); // returns the subdomain node of the ith matched node
  }

};

//------------------------------------------------------------------------------

#endif

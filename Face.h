#ifndef _FACE_H_
#define _FACE_H_

#include <BcDef.h>
#include <BinFileHandler.h>
#include <BlockAlloc.h>
#include <MapFace.h>
#include <PostFcn.h>
#include <Types.h>
#include <Node.h>
#include <Vector.h>

#include <vector>

class VarFcn;
class FluxFcn;
class FemEquationTerm;
class EdgeSet;
class ElemSet;
class GeoState;
class TimeLowMachPrec;
class Connectivity;

struct Vec3D;

template<int dim> class BcData;
template<class Scalar> class Vec;
template<class Scalar, int dim> class SVec;
template<class Scalar, int dim> class MvpMat;
template<class Scalar, int dim> class DistSVec;
template<int dim> class EmbeddedBcData;
template<int dim> class LESData;
template<class Scalar, int dim, int dim2> class RectangularSparseMat;
template<int dim> struct dRdXoperators;

//-------------------------- TRIANGLE FACE CLASS -------------------------------

class Face {

 public:
  enum Type {TRIA = 4};

  static const int MaxNumNd = 3;

  static constexpr double third = 1.0 / 3.0;

  Face() {
    elemNum = -1;
    isWallFcnAct = true;
    deltad2w = 0.0;
  }

  ~Face() {}

 private:
  int elemNum;
  int code; // boundary condition code
  int surface_id;
  int normNum;
  int nodeNums[3];
  int edgeNums[3];
  bool isWallFcnAct;
  double deltad2w;

  static const int edgeEnds[MaxNumNd][2];

 public:
  //--------------Operators
  int operator[](int i) {
    return nodeNum(i);
  }

  operator int *() {
    return nodeNum();
  }

  operator MaxFace() {
    return MaxFace(numNodes(), nodeNum());
  }

  //--------------General methods
  Type type() {
    return Face::TRIA;
  }

  int numNodes() {
    return 3;
  }

  int numEdges() {
    return 3;
  }

  // (The normals for all nodes equal, but the normal velocities are different, thus 3)
  int numNorms() {
    return 3;
  }

  int *nodeNum() {
    return nodeNums;
  }

  int& nodeNum(int i) {
    return nodeNums[i];
  }

  int& edgeNum(int i) {
    return edgeNums[i];
  }

  int edgeEnd(int i, int k) {
    return edgeEnds[i][k];
  }

  int *nodes(int *d = NULL) {
    if(d) {
      for(int i = 0; i < numNodes(); i++) {
        d[i] = nodeNum(i);
      }
    }
    return nodeNum();
  }

  int getCode() {
    return code;
  }

  int getSowerCode() {
    switch(code) {
      case BC_MASSFLOW_OUTLET_MOVING : return BC_OUTLET_MOVING;
      case BC_MASSFLOW_OUTLET_FIXED : return BC_OUTLET_FIXED;
      case BC_MASSFLOW_INLET_MOVING : return BC_INLET_MOVING;
      case BC_MASSFLOW_INLET_FIXED : return BC_INLET_FIXED;
      case BC_DIRECTSTATE_OUTLET_MOVING : return BC_OUTLET_MOVING;
      case BC_DIRECTSTATE_OUTLET_FIXED : return BC_OUTLET_FIXED;
      case BC_DIRECTSTATE_INLET_MOVING : return BC_INLET_MOVING;
      case BC_DIRECTSTATE_INLET_FIXED : return BC_INLET_FIXED;
      case BC_POROUS_WALL_MOVING : return BC_SLIP_WALL_MOVING;
      case BC_POROUS_WALL_FIXED : return BC_SLIP_WALL_FIXED;
      case BC_SYMMETRY : return 6;
      case BC_PERIODIC : return 7;
      default : return code;
    }
  }

  void setCode(int t) {
    code = t;
  }

  int getElementNumber() const {
    return elemNum;
  }

  int getSurfaceID() {
    return surface_id;
  }

  void setSurfaceID(int s) {
    surface_id = s;
  }

  int getNormNum() const {
    return normNum;
  }

  void setNormNum(int n) {
    normNum = n;
  }

  void setIsWallFcnAct(bool n) {
    isWallFcnAct = n;
  }

  bool getIsWallFcnAct() {
    return isWallFcnAct;
  }

  void setDeltaD2W(double n) {
    deltad2w = n;
  }

  double getDeltaD2W() {
    return deltad2w;
  }

  //--------------Functions in FaceCore.C
  void setup(int, int *, int, int surface_id = 0);
  void setCode(int *);
  void setNodeType(int *, Vec<int>&);
  void setElementNumber(int elemNum, int rotDir);
  void tagNodesOnBoundaries(Vec<bool>&);
  void tagEdgesOnBoundaries(Vec<bool>&);
  void reorder();
  void numberEdges(EdgeSet&);
  int  findEdge(int i, int j);
  void computeEdgeNormals(NodeSet&, int_t *, SVec<double, 6>&);
  static double computeVolume(Vec3D&, Vec3D&, Vec3D&, Vec3D&, Vec3D&, Vec3D&);
  static double computeVolume(const Vec3D& xa_n, const Vec3D& xb_n, const Vec3D& xc_n, const Vec3D& xd_n,
                              const Vec3D& xa_np1, const Vec3D& xb_np1, const Vec3D& xc_np1, const Vec3D& xd_np1);
  double computeArea(SVec<double, 3>&);

  // Compute total face normal
  void computeNormal(NodeSet&, Vec3D&);
  void computeNormal(SVec<double, 3>&, Vec3D&);
  void computeNormalAndDerivative(SVec<double, 3>&, SVec<double, 3>&, Vec3D&, Vec3D&);
  void compute_dndX(SVec<double, 3>&, double[3][3][3]);

  // Compute subface normals
  void computeNormal(NodeSet&, Vec<Vec3D>&);
  void computeNormalConfig(SVec<double, 3>&, SVec<double, 3>&, Vec<Vec3D>&,
                           Vec<double>&);
  void computeDerivativeOfNormal(SVec<double, 3>&, SVec<double, 3>&, Vec<Vec3D>&,
                                 Vec<Vec3D>&, Vec<double>&, Vec<double>&);
  void computeDerivativeOperatorsOfNormal(int, SVec<double, 3>&, RectangularSparseMat<double, 3, 3>&);
  void computeNormalGCL1(SVec<double, 3>&, SVec<double, 3>&,
                         SVec<double, 3>&, Vec<Vec3D>&, Vec<double>&);
  void computeNormalEZGCL1(double, SVec<double, 3>&, SVec<double, 3>&,
                           Vec<Vec3D>&, Vec<double>&);

  // Get i-th subface normal/normal velocity
  Vec3D getNormal(Vec<Vec3D>&, int i);
  double getNormalVel(Vec<double>&, int i);

  // Spatial location functions
  void computeBarycentricCoordinates(NodeSet&, const Vec3D&, double[2]);
  bool isPointInside(NodeSet&, const Vec3D&, double[2], double);
  void computeBoundingBox(NodeSet& X, double bb[6]);

  //--------------Templated functions in Face.C
  template<class NodeMap>
  void renumberNodes(NodeMap&);

  template<int dim>
  void assignFreeStreamValues2(SVec<double, dim>&, SVec<double, dim>&, double *);

  template<int dim>
  void assignPorousWallValues(SVec<double, dim>&, double *);

  template<int dim>
  void computeFaceBcValue(SVec<double, dim>&, double *);

  template<int dim1, int dim2>
  void computeNodeBcValue(SVec<double, 3>&, double *, SVec<double, dim2>&, EmbeddedBcData<dim1> *);

  template<int dim1, int dim2>
  void computeDerivativeOfNodeBcValue(SVec<double, 3>&, SVec<double, 3>&, double *, double *, SVec<double, dim2>&);

  template<int dim1, int dim2>
  void computeDerivativeOperatorsOfNodeBcValue(int, SVec<double, 3>&, double *, SVec<double, dim2>&, dRdXoperators<dim1>&);

  template<int dim>
  void computeNodeBcWallValue(SVec<double, 3>&, SVec<double, 1>&, double *, SVec<double, dim*(dim-5)>&, EmbeddedBcData<dim> *);

  template<int dim>
  void computeTimeStep(VarFcn *, Vec<Vec3D>&, Vec<double>&, SVec<double, dim>&,
                       Vec<double>&, Vec<double>&, TimeLowMachPrec&, EmbeddedBcData<dim> *);

  template<int dim>
  void computeDerivativeOfTimeStep(VarFcn *, Vec<Vec3D>&, Vec<Vec3D>&, Vec<double>&, Vec<double>&,
                                   SVec<double, dim>&, SVec<double, dim>&, Vec<double>&, Vec<double>&,
                                   double, TimeLowMachPrec&, EmbeddedBcData<dim> *);

  template<int dim>
  void computeFiniteVolumeTerm(FluxFcn **, Vec<Vec3D>&, Vec<double>&,
                               double *, SVec<double, dim>&, SVec<double, dim>&,
                               EmbeddedBcData<dim> *, std::vector<bool>&, double *);

  template<int dim>
  void computeDerivativeOfFiniteVolumeTerm(FluxFcn **, Vec<Vec3D>&, Vec<Vec3D>&, Vec<double>&, Vec<double>&,
                                           double *, double *, SVec<double, dim>&, SVec<double, dim>&,
                                           double, SVec<double, dim>&, EmbeddedBcData<dim> *);

  template<int dim>
  void computeDerivativeOperatorsOfFiniteVolumeTerm(int, FluxFcn **, Vec<Vec3D>&, Vec<double>&,
                                                    SVec<double, dim>&, double *, EmbeddedBcData<dim> *,
                                                    dRdXoperators<dim>&);

  template<int dim>
  void computeFiniteVolumeTermLS(Vec<Vec3D>&, Vec<double>&, SVec<double, dim>&, Vec<int>&,
                                 SVec<double, dimLS>&, SVec<double, dimLS>&);

  template<int dim, class Scalar, int neq>
  void computeJacobianFiniteVolumeTerm(FluxFcn **, Vec<Vec3D>&, Vec<double>&,
                                       double *, SVec<double, dim>&, MvpMat<Scalar, neq>&,
                                       EmbeddedBcData<dim> *, std::vector<bool>&, double *);

  template<int dim>
  void computeGalerkinTerm(ElemSet&, FemEquationTerm *, LESData<dim> *, Vec<double>&,
                           double *, SVec<double, 3>&, SVec<double, dim>&,
                           SVec<double, dim>&, EmbeddedBcData<dim> *, std::vector<bool>&);

  template<int dim>
  void computeDerivativeOfGalerkinTerm(ElemSet&, FemEquationTerm *, LESData<dim> *,
                                       Vec<double>&, Vec<double>&, double *, double *, SVec<double, 3>&,
                                       SVec<double, 3>&, SVec<double, dim>&, SVec<double, dim>&,
                                       double, SVec<double, dim>&, EmbeddedBcData<dim> *,
                                       std::vector<bool>&);

  template<int dim>
  void computeDerivativeOperatorsOfGalerkinTerm(int, ElemSet&, FemEquationTerm *, Vec<double>&, double *,
                                                SVec<double, 3>&, SVec<double, dim>&, EmbeddedBcData<dim> *,
                                                dRdXoperators<dim>&);

  template<int dim, class Scalar, int neq>
  void computeJacobianGalerkinTerm(ElemSet&, FemEquationTerm *, LESData<dim> *, Vec<double>&,
                                   double *, SVec<double, 3>&, SVec<double, dim>&,
                                   MvpMat<Scalar, neq>&, EmbeddedBcData<dim> *, std::vector<bool>&);

  template<int dim>
  void computeMaxwellSlipTerm(ElemSet&, FemEquationTerm *, double *, SVec<double, 3>&,
                              SVec<double, dim>&, SVec<double, dim>&, std::vector<bool>&);

  template<int dim>
  void computeBCsJacobianWallValues(ElemSet&, FemEquationTerm *, Vec<double>&,
                                    double *, double *, SVec<double, 3>&,
                                    SVec<double, dim>&, EmbeddedBcData<dim> *);

 protected:
  template<int dim>
  void computeForce(ElemSet&, PostFcn *, SVec<double, 3>&, Vec<double>&, double *,
                    SVec<double, dim>&, double *, Vec3D&, Vec3D&, Vec3D&, Vec3D&,
                    SVec<double, 3>&, EmbeddedBcData<dim> *, int = 0);

  template<int dim>
  void computeDerivativeOfForce(ElemSet&, PostFcn *, SVec<double, 3>&, SVec<double, 3>&, Vec<double>&,
                                double *, double *, SVec<double, dim>&, SVec<double, dim>&,
                                bool, double *, Vec3D&, Vec3D&, Vec3D&, Vec3D&,
                                SVec<double, 3>&, SVec<double, 3>&, EmbeddedBcData<dim> *, int = 0);

  template<int dim>
  void computeDerivativeOperatorsOfForce(ElemSet&, PostFcn *, Vec<double>&, double *, SVec<double, 3>&, SVec<double, dim>&, double *,
                                         SVec<double, 3>&, EmbeddedBcData<dim> *, int, double[3][3], double[3][3],
                                         double[3][3], double[3][3], double[3][3], double[3][3], double[3][3],
                                         double[3][3], double[3][3], double[3][3], double[3][3], double[3][3],
                                         double[3][3], double[3][dim], double[3][dim], double[3][dim], double[3][3][3],
                                         double[3][4][3], double[3][4][dim], double[3][3]);

  template<int dim>
  void computeForceTransmitted(ElemSet&, PostFcn *, SVec<double, 3>&, Vec<double>&, double *,
                               SVec<double, dim>&, double *, Vec3D&, Vec3D&, Vec3D&, Vec3D&,
                               SVec<double, 3>&, EmbeddedBcData<dim> *, int = 0);

  template<int dim>
  void computeDerivativeOfForceTransmitted(ElemSet&, PostFcn *, SVec<double, 3>&, SVec<double, 3>&, Vec<double>&,
                                           double *, double *, SVec<double, dim>&, SVec<double, dim>&,
                                           bool, double *, Vec3D&, Vec3D&, Vec3D&, Vec3D&,
                                           SVec<double, 3>&, SVec<double, 3>&, EmbeddedBcData<dim> *, int = 0);

  template<int dim>
  void computeDerivativeOperatorsOfForceTransmitted(ElemSet&, PostFcn *, Vec<double>&, double *, SVec<double, 3>&, SVec<double, dim>&, double *,
                                                    SVec<double, 3>&, EmbeddedBcData<dim> *, int, double[3][3][dim],
                                                    double[3][3][3], double[3][3][3], double[3], double[3][3],
                                                    double[3][3][dim], double[3][3][3], double[3][3][3],
                                                    double[3], double[3][3], double[3][3][dim], double[3][3][3],
                                                    double[3][3][3], double[3], double[3][3], double[3][3][3],
                                                    double[3][4][3], double[3][4][dim], double[3][3]);

 public:
  template<int dim>
  void computeNodalForce(ElemSet&, PostFcn *, SVec<double, 3>&, Vec<double>&, double *,
                         SVec<double, dim>&, double, SVec<double, 3>&, SVec<double, 3>&,
                         EmbeddedBcData<dim> *);

  template<int dim>
  void computeDerivativeOfNodalForce(ElemSet&, PostFcn *, SVec<double, 3>&, SVec<double, 3>&,
                                     Vec<double>&, double *, double *, SVec<double, dim>&,
                                     SVec<double, dim>&, double, bool, SVec<double, 3>&,
                                     SVec<double, 3>&, SVec<double, 3>&, EmbeddedBcData<dim> *);

  template<int dim>
  void computeDerivativeOperatorsOfNodalForce(ElemSet&, PostFcn *, Vec<double>&, double *, SVec<double, 3>&, SVec<double, dim>&,
                                              double, SVec<double, 3>&, EmbeddedBcData<dim> *,
                                              dRdXoperators<dim>&);

  template<int dim>
  void computeNodalHeatPower(ElemSet&, PostFcn *, SVec<double, 3>&, Vec<double>&,
                             double *, SVec<double, dim>&, Vec<double>&, EmbeddedBcData<dim> *);

  template<int dim>
  double computeHeatFluxes(ElemSet&, PostFcn *, SVec<double, 3>&, Vec<double>&, double *,
                           SVec<double, dim>&, EmbeddedBcData<dim> *);

  template<int dim>
  void computeBulkQuantities(SVec<double, 3>&, SVec<double, dim>&, double&, double&, double&); //ej check if & and * functions same

  template<int dim>
  void computeForceAndMoment(ElemSet&, PostFcn *, SVec<double, 3>&, Vec<double>&,
                             double *, SVec<double, dim>&, Vec3D&, Vec3D&,
                             Vec3D&, Vec3D&, Vec3D&, SVec<double, 3>&,
                             EmbeddedBcData<dim> *, int);

  template<int dim>
  void computeDerivativeOfForceAndMoment(ElemSet&, PostFcn *, SVec<double, 3>&, SVec<double, 3>&,
                                         Vec<double>&, double *, double *, SVec<double, dim>&,
                                         SVec<double, dim>&, bool, Vec3D&, Vec3D&, Vec3D&,
                                         Vec3D&, Vec3D&, SVec<double, 3>&, SVec<double, 3>&,
                                         EmbeddedBcData<dim> *, int);

  template<int dim>
  void computeDerivativeOperatorsOfForceAndMoment(ElemSet&, PostFcn *, SVec<double, 3>&,
                                                  Vec<double>&, double *, SVec<double, dim>&, Vec3D&,
                                                  SVec<double, 3>&, EmbeddedBcData<dim> *, int,
                                                  dRdXoperators<dim>&);

  template<int dim>
  void computeScalarQuantity(PostFcn::ScalarType, ElemSet&, PostFcn *, SVec<double, 3>&,
                             Vec<double>&, double *, SVec<double, dim>&,
                             SVec<double, 2>&, EmbeddedBcData<dim> *);

  template<int dim>
  void computeFDerivs(ElemSet&, VarFcn *, SVec<double, 3>&, SVec<double, dim>&, Vec3D[dim], int);

  template<int dim>
  void computeForceDerivs(ElemSet&, VarFcn *, SVec<double, 3>&, SVec<double, dim>&,
                          SVec<double, dim>&, Vec<double>&, SVec<double, 3> **, int);

  template<class TRIPLET, int dim>
  void computeForceDerivMat(ElemSet&, VarFcn *, SVec<double, 3>&, SVec<double, dim>&,
                            SVec<double, 3> **, std::vector<TRIPLET>&, std::vector<int_t>&, int, int);

};

//------------------------------------------------------------------------------

inline
void Face::computeBoundingBox(NodeSet& X, double bb[6]) {
  const Vec3D& a = X[nodeNum(0)];
  const Vec3D& b = X[nodeNum(1)];
  const Vec3D& c = X[nodeNum(2)];
  //const double eps = 10.0 * std::numeric_limits<double>::epsilon();
  // enlarge BB to remove risk of not finding face at cost of false positives (difficulties with flatness)
  const double eps = 1.0e-4 * std::max({(a-b).norm(), (a-c).norm(), (b-c).norm()});  // edge length based tolerance
  bb[0] = std::min({a[0], b[0], c[0]}) - eps;
  bb[1] = std::max({a[0], b[0], c[0]}) + eps;
  bb[2] = std::min({a[1], b[1], c[1]}) - eps;
  bb[3] = std::max({a[1], b[1], c[1]}) + eps;
  bb[4] = std::min({a[2], b[2], c[2]}) - eps;
  bb[5] = std::max({a[2], b[2], c[2]}) + eps;
}

//----------------------------- FACESET CLASS ----------------------------------

class FaceSet {

 public:
  typedef std::vector<Face *> Container;

 private:
  int numFaceNorms;  // Number of face normals to be stored
  Container faces;

 public:
  BlockAlloc memFaces;

  FaceSet(int n = 0) : numFaceNorms(0) {
    faces.resize(n);
  }

  ~FaceSet() {
    for(auto& face : faces) face->~Face();
    faces.clear();
  }

  Container& getFaces() {
    return faces;
  }

  Face& operator[](int i) const {
    return *faces[i];
  }

  void addFace(int i, Face *face) {
    if(i == faces.size()) faces.push_back(face); else faces[i] = face;
  }

  void removeFace(int i) {
    faces.erase(faces.begin()+i);
  }

  auto begin() -> decltype(faces.begin()) { return faces.begin(); }

  auto end() -> decltype(faces.end()) { return faces.end(); }

  int size() const {
    return int(faces.size());
  }

  int &sizeNorms() {
    return numFaceNorms;
  }

  int sizeNorms() const {
    return numFaceNorms;
  }

  Connectivity *createFaceToFacetConnectivity() const;

  Connectivity *createFacetToNodeConnectivity() const;

  void renumber(const std::vector<int>&);

  void setNormNum();

  int read(BinFileHandler&, int, int (*)[2], int_t *, int);

  void write(BinFileHandler&, int, BinFileHandler::OffType, int, int (*)[2], const int_t *, Vec<int>&, int);

  template<int dim>
  void computeDerivativeOfFiniteVolumeTerm(dRdXoperators<dim>&, BcData<dim>&, GeoState&, SVec<double, dim>&);

  template<int dim>
  void computeTransposeDerivativeOfFiniteVolumeTerm(dRdXoperators<dim>&, BcData<dim>&, GeoState&, SVec<double, dim>&);

};

//------------------------------------------------------------------------------

// Get OUTWARD subface normal
inline Vec3D Face::getNormal(Vec<Vec3D>& faceNorm, int i) {
  return faceNorm[normNum + i];
}

//------------------------------------------------------------------------------

inline double Face::getNormalVel(Vec<double>& faceNormVel, int i) {
  return faceNormVel[normNum + i];
}

//------------------------------------------------------------------------------

#endif

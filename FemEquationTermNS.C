#include <FemEquationTermNS.h>

#include <IoData.h>
#include <VarFcn.h>
#include <ViscoFcn.h>
#include <WallFcn.h>
#include <WallFcnODE.h>
#include <ThermalCondFcn.h>
#include <NavierStokesTerm.h>
#include <Vector3D.h>

//------------------------------------------------------------------------------

FemEquationTermNS::FemEquationTermNS(IoData& iod, VarFcn *vf) : FemEquationTerm(iod) {
  ns = new NavierStokesTerm(iod, vf);
  integrationType = iod.bc.wall.integration;
  if(iod.bc.wall.anyWallFcn && iod.bc.wall.integration == BcsWallData::ODE) {
    wallFcn = new WallFcnODE(iod, ns->getVarFcn(), ns->getViscoFcn());
  }
  else if(iod.bc.wall.anyWallFcn) {
    wallFcn = new WallFcn(iod, ns->getVarFcn(), ns->getViscoFcn());
  }
}

//------------------------------------------------------------------------------

FemEquationTermNS::~FemEquationTermNS() {
  if(ns) {
    delete ns;
  }
}

//------------------------------------------------------------------------------

double FemEquationTermNS::computeViscosity(double *V, int tag) {
  double T;
  ns->computeTemperature(V, tag, T);
  return ns->getOoreynolds_mu() * ns->getViscoFcn()->compute_mu(T, tag);
}

//------------------------------------------------------------------------------

void FemEquationTermNS::computeJacobianViscosity(double *V, double *dmudV, int tag) {
  double T;
  ns->computeTemperature(V, tag, T);
  double dTdV[5];
  ns->getVarFcn()->computeJacobianTemperature(V, dTdV, tag);
  double dmudT = ns->getOoreynolds_mu() * ns->getViscoFcn()->compute_dmu(T, tag);
  for(int i = 0; i < 5; ++i) {
    dmudV[i] = dmudT * dTdV[i];
  }
}

//------------------------------------------------------------------------------

void FemEquationTermNS::rstVar(IoData& iod) {
  FemEquationTerm::rstVar(iod);
  ns->rstVar(iod);
}

//------------------------------------------------------------------------------

double FemEquationTermNS::computeViscousTimeStep(double X[3], double *V, int tag) {
  double T;
  ns->computeTemperature(V, tag, T);
  return ns->getOoreynolds_mu() * ns->getViscoFcn()->compute_mu(T, tag) / V[0];
}

//------------------------------------------------------------------------------

double FemEquationTermNS::computeDerivativeOfViscousTimeStep(double X[3], double dX[3], double *V,
                                                             double *dV, double dMach, int tag) {
  double T, dT;
  ns->computeTemperature(V, tag, T);
  ns->computeDerivativeOfTemperature(V, dV, tag, dT);
  double ooreynolds_mu = ns->getOoreynolds_mu();
  double dooreynolds_mu = ns->getdOoreynolds_mu(dMach);
  double mu = ooreynolds_mu * ns->getViscoFcn()->compute_mu(T, tag);
  double dmu = dooreynolds_mu * (mu / ooreynolds_mu) + ooreynolds_mu * ns->getViscoFcn()->compute_muDerivative(T, dT, dMach, tag);
  return (dmu * V[0] - mu * dV[0]) / (V[0] * V[0]);
}

//------------------------------------------------------------------------------

bool FemEquationTermNS::computeVolumeTerm(double tetVol, double dp1dxj[4][3], double X[4][3], double d2w[4],
                                          double *V[4], int tag, int material_id,
                                          int code, Vec3D& n, double *Vwall, int idxf, double d2wf,
                                          double *r, double *S, double *PR, double alpha) {
  bool porousmedia = false;
  double u[4][3], ucg[3];
  ns->computeVelocity(V, u, ucg);
  double dudxj[3][3];
  ns->computeVelocityGradient(dp1dxj, u, dudxj);
  double T[4], Tcg;
  ns->computeTemperature(V, tag, T, Tcg);
  double dTdxj[3];
  ns->computeTemperatureGradient(dp1dxj, T, dTdxj);
  double mu, lambda, kappa;
  ns->computeNonDimensionalLaminarTransportCoefficients(Tcg, tag, mu, lambda, kappa, code);
  double (*R)[5] = reinterpret_cast<double (*)[5]>(r);
  if(Vwall) { // use embedded wall function
    wallFcn->computeVolumeTerm(code, n, d2wf, Vwall, idxf, V, tag, dudxj, dTdxj, mu, lambda, kappa, ucg, R);
    if(alpha < 1.0) {
      double R_temp[4][5];
      ns->computeVolumeTermNS(1., mu, lambda, kappa, ucg, dudxj, dTdxj, R_temp);
      for(int i = 0; i < 4; ++i) {
        for(int j = 0; j < 5; ++j) {
          R[i][j] = alpha * R[i][j] + (1.0 - alpha) * R_temp[i][j];
        }
      }
    }
  }
  else {
    ns->computeVolumeTermNS(1., mu, lambda, kappa, ucg, dudxj, dTdxj, R);
  }
  if(material_id > 0) {
    std::map<int, PorousMedia *>::iterator it = pr->find(material_id);
    if(it != pr->end()) { // if porous media with material_id has been defined in the input file
      porousmedia = pr->computeVolumeTermPorousCore(tetVol, it, ucg, V, PR);
    }
  }
  return porousmedia;
}

//------------------------------------------------------------------------------

bool FemEquationTermNS::computeDerivativeOfVolumeTerm(double tetVol, double dtetVol, double dp1dxj[4][3], double ddp1dxj[4][3],
                                                      double X[4][3], double dX[4][3], double d2w[4], double dd2w[4], double *V[4], double *dV[4],
                                                      double dMach, int tag, int material_id, int code,
                                                      Vec3D& n, Vec3D& dn, double *Vwall, int idxf, double d2wf, double dd2wf,
                                                      double *dr, double *dS, double *dPR, double alpha, double dalpha) {
  bool porousmedia = false;
  double u[4][3], ucg[3];
  ns->computeVelocity(V, u, ucg);
  double du[4][3], ducg[3];
  ns->computeDerivativeOfVelocity(dV, du, ducg);
  double dudxj[3][3];
  ns->computeVelocityGradient(dp1dxj, u, dudxj);
  double ddudxj[3][3];
  ns->computeDerivativeOfVelocityGradient(dp1dxj, ddp1dxj, u, du, ddudxj);
  double T[4], Tcg;
  ns->computeTemperature(V, tag, T, Tcg);
  double dT[4], dTcg;
  ns->computeDerivativeOfTemperature(V, dV, tag, dT, dTcg);
  double dTdxj[3];
  ns->computeTemperatureGradient(dp1dxj, T, dTdxj);
  double ddTdxj[3];
  ns->computeDerivativeOfTemperatureGradient(dp1dxj, ddp1dxj, T, dT, ddTdxj);
  double mu, lambda, kappa;
  ns->computeNonDimensionalLaminarTransportCoefficients(Tcg, tag, mu, lambda, kappa, code);
  double dmu, dlambda, dkappa;
  ns->computeDerivativeOfNonDimensionalLaminarTransportCoefficients(Tcg, dTcg, tag, mu, lambda, kappa, dMach, dmu, dlambda, dkappa, code);
  double (*dR)[5] = reinterpret_cast<double (*)[5]>(dr);
  if(Vwall) {
    double R[4][5];
    wallFcn->computeDerivativeOfVolumeTerm(code, n, dn, d2wf,dd2wf, Vwall, idxf, V, dV, tag, dudxj, ddudxj, dTdxj, ddTdxj, 1.,
      // mu, dmu, lambda, dlambda, kappa, dkappa, ucg, ducg, dR, 0);
      mu, dmu, lambda, dlambda, kappa, dkappa, ucg, ducg, dR, std::abs(dalpha) ? R : nullptr);
    if(alpha < 1.0) {
      double dR_temp[4][5];
      ns->computeDerivativeOfVolumeTermNS(1., 0., mu, dmu, lambda, dlambda, kappa, dkappa, ucg, ducg, dudxj, ddudxj, dTdxj, ddTdxj, dR_temp);
      for(int i = 0; i < 4; ++i) {
        for(int j = 0; j < 5; ++j) {
          dR[i][j] = alpha * dR[i][j] + (1.0 - alpha) * dR_temp[i][j];
        }
      }
      if(std::abs(dalpha) > 0.0) {
        double R_temp[4][5];
        ns->computeVolumeTermNS(1., mu, lambda, kappa, ucg, dudxj, dTdxj, R_temp);
        for(int i = 0; i < 4; ++i) {
          for(int j = 0; j < 5; ++j) {
            dR[i][j] += dalpha * (R[i][j] - R_temp[i][j]);
          }
        }
      }
    }
  }
  else {
    ns->computeDerivativeOfVolumeTermNS(1., 0., mu, dmu, lambda, dlambda, kappa, dkappa, ucg, ducg, dudxj, ddudxj, dTdxj, ddTdxj, dR);
  }
  if(material_id > 0) {
    std::map<int, PorousMedia *>::iterator it = pr->find(material_id);
    if(it != pr->end()) { // if porous media with material_id has been defined in the input file
      porousmedia = pr->computeDerivativeOfVolumeTermPorousCore(tetVol, dtetVol, it, ucg, ducg, V, dV, dPR);
    }
  }
  return porousmedia;
}

//------------------------------------------------------------------------------

void FemEquationTermNS::computeDerivativeOperatorsOfVolumeTerm(double dp1dxj[4][3], double X[4][3], double d2w[4], double *V[4],
                                                               int tag, int code, double drddp1dxj[3][5][4][3],
                                                               double drdMach[3][5], double dsddp1dxj[5][4][3],
                                                               double dsdMach[5], double dsdX[5][4][3]) {
  double u[4][3], ucg[3];
  ns->computeVelocity(V, u, ucg);
  double dudV[4][3][4][4] = {0}, ducgdV[3][4][4] = {0};
  ns->computeDerivativeOperatorsOfVelocity(dudV, ducgdV);
  double T[4], Tcg;
  ns->computeTemperature(V, tag, T, Tcg);
  double dTdV[4][5] = {0}, dTcgdV[4][5] = {0};
  ns->computeDerivativeOperatorsOfTemperature(V, tag, dTdV, dTcgdV);
  double dudxj[3][3];
  ns->computeVelocityGradient(dp1dxj, u, dudxj);
  double ddudxj[3][3] = {0};
  double ddudxjddp1dxj[3][3][4][3] = {0}, ddudxjdu[3][3][4][3] = {0};
  ns->computeDerivativeOperatorsOfVelocityGradient(dp1dxj, u, ddudxjddp1dxj, ddudxjdu);
  double dTdxj[3];
  ns->computeTemperatureGradient(dp1dxj, T, dTdxj);
  double ddTdxjddp1dxj[3][4][3] = {0}, ddTdxjdT[3][4] = {0};
  ns->computeDerivativeOperatorsOfTemperatureGradient(dp1dxj, T, ddTdxjddp1dxj, ddTdxjdT);
  double mu, lambda, kappa;
  ns->computeNonDimensionalLaminarTransportCoefficients(Tcg, tag, mu, lambda, kappa, code);
  double dmudTcg = 0, dmudMach = 0, dlambdadTcg = 0, dlambdadMach = 0, dkappadTcg = 0, dkappadMach = 0;
  ns->computeNonDimensionalLaminarTransportCoefficientsDerivativeOperators(Tcg, tag, dmudTcg, dmudMach,
                                                                           dlambdadTcg, dlambdadMach, dkappadTcg,
                                                                           dkappadMach, code);
  double drdnu[3][5] = {0}, drdmu[3][5] = {0}, drdlambda[3][5] = {0}, drdkappa[3][5] = {0},
         drddudxj[3][5][3][3] = {0}, drddTdxj[3][5][3] = {0};
  ns->computeDerivativeOperatorsOfVolumeTermNS(1, mu, lambda, kappa, ucg, dudxj, dTdxj, drdnu,
                                               drdmu, drdlambda, drdkappa, drddudxj, drddTdxj);
  for(int i = 0; i < 3; ++i) {
    for(int j = 0; j < 5; ++j) {
      drdMach[i][j] += (drdkappa[i][j] * dkappadMach + drdmu[i][j] * dmudMach + drdlambda[i][j] * dlambdadMach);
      for(int k = 0; k < 3; ++k) {
        for(int l = 0; l < 4; ++l) {
          for(int w = 0; w < 3; ++w) {
            drddp1dxj[i][j][l][w] += drddTdxj[i][j][k] * ddTdxjddp1dxj[k][l][w];
          }
        }
        for(int l = 0; l < 3; ++l) {
          for(int m = 0; m < 4; ++m) {
            for(int n = 0; n < 3; ++n) {
              drddp1dxj[i][j][m][n] += drddudxj[i][j][k][l] * ddudxjddp1dxj[k][l][m][n];
            }
          }
        }
      }
    }
  }
}

//------------------------------------------------------------------------------

bool FemEquationTermNS::computeJacobianVolumeTerm(double tetVol, double dp1dxj[4][3], double X[4][3],
                                                  double d2w[4], double *V[4], int tag, int material_id,
                                                  int code, Vec3D& n, double *Vwall, int idxf, double d2wf,
                                                  double *drdu, double *dsdu, double *dpdu, double alpha) {
  bool porousmedia = false;
  double u[4][3], ucg[3];
  ns->computeVelocity(V, u, ucg);
  double dudxj[3][3];
  ns->computeVelocityGradient(dp1dxj, u, dudxj);
  double T[4], Tcg;
  ns->computeTemperature(V, tag, T, Tcg);
  double dTdxj[3];
  ns->computeTemperatureGradient(dp1dxj, T, dTdxj);
  double mu, lambda, kappa;
  ns->computeNonDimensionalLaminarTransportCoefficients(Tcg, tag, mu, lambda, kappa, code);
  double (*dRdU)[3][5][5] = reinterpret_cast<double (*)[3][5][5]>(drdu);
  if(ns->getViscoFcn()->is_constant() && ns->getThermalCondFcn()->is_constant() && !Vwall) {
    ns->computeJacobianVolumeTermNS(dp1dxj, mu, lambda, kappa, V, ucg, dudxj, tag, dRdU);
  }
  else {
    double dTcg[4][5];
    ns->computeJacobianTemperature(V, tag, T, dTcg);
    double dmu[4][5], dlambda[4][5], dkappa[4][5];
    ns->computeJacobianNonDimensionalLaminarTransportCoefficients(Tcg, dTcg, tag, mu, dmu, dlambda, dkappa, code);
    if(Vwall) { // use embedded wall function
      wallFcn->computeJacobianVolumeTerm(code, n, d2wf, Vwall, idxf, V, tag, dp1dxj, mu, dmu, lambda, dlambda,
                                         kappa, dkappa, dRdU);
      if(alpha < 1) {
        double dRdU_temp[4][3][5][5];
        ns->computeJacobianVolumeTermNS(dp1dxj, mu, dmu, lambda, dlambda, kappa, dkappa, V, ucg, dudxj, tag, dTdxj, dRdU_temp);
        for(int i = 0; i < 4; ++i) {
          for(int j = 0; j < 3; ++j) {
            for(int k = 0; k < 5; ++k) {
              for(int l = 0; l < 5; ++l) {
                dRdU[i][j][k][l] = alpha * dRdU[i][j][k][l] + (1.0 - alpha) * dRdU_temp[i][j][k][l];
              }
            }
          }
        }
      }
    }
    else {
      ns->computeJacobianVolumeTermNS(dp1dxj, mu, dmu, lambda, dlambda, kappa, dkappa, V, ucg, dudxj, tag, dTdxj, dRdU);
    }
  }
  if(material_id > 0) {
    std::map<int, PorousMedia *>::iterator it = pr->find(material_id);
    if(it != pr->end()) { // if porous media with material_id has been defined in the input file
      double (*dPdU)[4][5][5] = reinterpret_cast<double (*)[4][5][5]>(dpdu);
      porousmedia = pr->computeJacobianVolumeTermPorousCore(tetVol, it, ucg, V, dPdU);
    }
  }
  return porousmedia;
}

//------------------------------------------------------------------------------

void FemEquationTermNS::computeSurfaceTerm(int code, double d2w[3], double *V[3], double *Vwall,
                                           int tag, Vec3D& n, double *R) {
  wallFcn->computeSurfaceTerm(code, n, d2w, Vwall, V, R, tag);
}

//------------------------------------------------------------------------------

void FemEquationTermNS::computeDerivativeOfSurfaceTerm(int code, double d2w[3], double *V[3], double *dV[3],
                                                       double *Vwall, double *dVwall, int tag, Vec3D& n,
                                                       Vec3D& dn, double dMach, double *dR) {
  wallFcn->computeDerivativeOfSurfaceTerm(code, n, dn, d2w, Vwall, dVwall, V, dV, dMach, dR, tag);
}

//------------------------------------------------------------------------------

void FemEquationTermNS::computeDerivativeOperatorsOfSurfaceTerm(int code, double d2w[3], double *V[3],
                                                                double *Vwall, int tag, Vec3D& n,
                                                                double dRdn[][3], double dRdS[][3],
                                                                double dVwalldn[][3], double dVwalldS[][3]) {
  wallFcn->computeDerivativeOperatorsOfSurfaceTerm(code, n, d2w, Vwall, V, dRdn, dRdS, dVwalldn, dVwalldS, tag);
}

//------------------------------------------------------------------------------

void FemEquationTermNS::computeJacobianSurfaceTerm(int code, double d2w[3], double *V[3], double *Vwall,
                                                   int tag, Vec3D& n, double *drdu) {
  double (*dRdU)[5][5] = reinterpret_cast<double (*)[5][5]>(drdu);
  if(integrationType == BcsWallData::ODE) {
    wallFcn->computeJacobianSurfaceTermLES(code, n, d2w, Vwall, V, dRdU, tag);
  }
  else {
    wallFcn->computeJacobianSurfaceTerm(code, n, d2w, Vwall, V, dRdU, tag);
  }
}

//------------------------------------------------------------------------------

void FemEquationTermNS::computeSurfaceTerm(int code, double dp1dxj[4][3], double *V[4], double *Vwall,
                                           int tag, Vec3D& n, double *R) {
  double u[4][3], ucg[3];
  ns->computeVelocity(V, u, ucg);
  double dudxj[3][3];
  ns->computeVelocityGradient(dp1dxj, u, dudxj);
  double T[4], Tcg;
  ns->computeTemperature(V, tag, T, Tcg);
  double mu, lambda, kappa;
  ns->computeNonDimensionalLaminarTransportCoefficients(Tcg, tag, mu, lambda, kappa, code);
  ns->computeSurfaceTermNS(mu, lambda, Vwall, dudxj, n, R);
}

//------------------------------------------------------------------------------

void FemEquationTermNS::computeDerivativeOfSurfaceTerm(int code, double dp1dxj[4][3], double ddp1dxj[4][3], double *V[4],
                                                       double *dV[4], double *Vwall, double *dVwall, int tag, Vec3D& n,
                                                       Vec3D& dn, double dMach, double *dR) {
  double u[4][3], ucg[3];
  ns->computeVelocity(V, u, ucg);
  double du[4][3], ducg[3];
  ns->computeDerivativeOfVelocity(dV, du, ducg);
  double dudxj[3][3];
  ns->computeVelocityGradient(dp1dxj, u, dudxj);
  double ddudxj[3][3];
  ns->computeDerivativeOfVelocityGradient(dp1dxj, ddp1dxj, u, du, ddudxj);
  double T[4], Tcg;
  ns->computeTemperature(V, tag, T, Tcg);
  double dT[4], dTcg;
  ns->computeDerivativeOfTemperature(V, dV, tag, dT, dTcg);
  double mu, lambda, kappa;
  ns->computeNonDimensionalLaminarTransportCoefficients(Tcg, tag, mu, lambda, kappa, code);
  double dmu, dlambda, dkappa;
  ns->computeDerivativeOfNonDimensionalLaminarTransportCoefficients(Tcg, dTcg, tag, mu, lambda, kappa, dMach, dmu, dlambda, dkappa, code);
  ns->computeDerivativeOfSurfaceTermNS(mu, dmu, lambda, dlambda, Vwall, dVwall, dudxj, ddudxj, n, dn, dR);
}

//------------------------------------------------------------------------------

void FemEquationTermNS::computeDerivativeOperatorsOfSurfaceTerm(int code, double dp1dxj[4][3], double *V[4], double *Vwall,
                                                                int tag, Vec3D& n, double dRddp1dxj[][4][3], double dRdn[][3],
                                                                double dRdMach[]) {
  double u[4][3], ucg[3];
  ns->computeVelocity(V, u, ucg);
  double dudV[4][3][4][4] = {0}, ducgdV[3][4][4] = {0};
  ns->computeDerivativeOperatorsOfVelocity(dudV, ducgdV);
  double dudxj[3][3];
  ns->computeVelocityGradient(dp1dxj, u, dudxj);
  double ddudxj[3][3] = {0};
  double ddudxjddp1dxj[3][3][4][3] = {0}, ddudxjdu[3][3][4][3] = {0};
  ns->computeDerivativeOperatorsOfVelocityGradient(dp1dxj, u, ddudxjddp1dxj, ddudxjdu);
  double T[4], Tcg;
  ns->computeTemperature(V, tag, T, Tcg);
  double dTdV[4][5] = {0}, dTcgdV[4][5] = {0};
  ns->computeDerivativeOperatorsOfTemperature(V, tag, dTdV, dTcgdV);
  double mu, lambda, kappa;
  ns->computeNonDimensionalLaminarTransportCoefficients(Tcg, tag, mu, lambda, kappa, code);
  double dmudTcg, dmudMach, dlambdadTcg, dlambdadMach, dkappadTcg, dkappadMach;
  ns->computeNonDimensionalLaminarTransportCoefficientsDerivativeOperators(Tcg, tag, dmudTcg, dmudMach,
                                                                           dlambdadTcg, dlambdadMach, dkappadTcg,
                                                                           dkappadMach, code);
  double dRdmu[5] = {0}, dRdlambda[5] = {0}, dRdVwall[5][5], dRddudxj[5][3][3];
  ns->computeDerivativeOperatorsOfSurfaceTermNS(mu, lambda, Vwall, dudxj, n, dRdmu, dRdlambda, dRdVwall, dRddudxj, dRdn);
  for(int j = 0; j < 5; ++j) {
    dRdMach[j] += (dRdmu[j] * dmudMach + dRdlambda[j] * dlambdadMach);
    for(int k = 0; k < 3; ++k) {
      for(int l = 0; l < 3; ++l) {
        for(int m = 0; m < 4; ++m) {
          for(int n = 0; n < 3; ++n) {
            dRddp1dxj[j][m][n] += dRddudxj[j][k][l] * ddudxjddp1dxj[k][l][m][n];
          }
        }
      }
    }
  }
}

//------------------------------------------------------------------------------

void FemEquationTermNS::computeJacobianSurfaceTerm(int code, double dp1dxj[4][3], double *V[4], double *Vwall,
                                                   int tag, Vec3D& n, double *drdu) {
  double u[4][3], ucg[3];
  ns->computeVelocity(V, u, ucg);
  double dudxj[3][3];
  ns->computeVelocityGradient(dp1dxj, u, dudxj);
  double T[4], Tcg;
  ns->computeTemperature(V, tag, T, Tcg);
  double mu, lambda, kappa;
  ns->computeNonDimensionalLaminarTransportCoefficients(Tcg, tag, mu, lambda, kappa, code);
  double (*dRdU)[5][5] = reinterpret_cast<double (*)[5][5]>(drdu);
  if(ns->getViscoFcn()->is_constant() && ns->getThermalCondFcn()->is_constant()) {
    ns->computeJacobianSurfaceTermNS(dp1dxj, mu, lambda, V, Vwall, n, dRdU);
  }
  else {
    double dTcg[4][5];
    ns->computeJacobianTemperature(V, tag, T, dTcg);
    double dmu[4][5], dlambda[4][5], dkappa[4][5];
    ns->computeJacobianNonDimensionalLaminarTransportCoefficients(Tcg, dTcg, tag, mu, dmu, dlambda, dkappa, code);
    ns->computeJacobianSurfaceTermNS(dp1dxj, mu, dmu, lambda, dlambda, V, Vwall, dudxj, n, dRdU);
  }
}

//------------------------------------------------------------------------------

void FemEquationTermNS::computeMaxwellSlipSurfaceTerm(int code, double dp1dxj[4][3], double *V[4], double *Vwall,
                                                      int tag, Vec3D& n, double *R) {
  double rho[4], rhocg;
  ns->computeDensity(V, rho, rhocg);
  double u[4][3], ucg[3];
  ns->computeVelocity(V, u, ucg);
  double dudxj[3][3];
  ns->computeVelocityGradient(dp1dxj, u, dudxj);
  double T[4], Tcg;
  ns->computeTemperature(V, tag, T, Tcg);
  double dTdxj[3];
  ns->computeTemperatureGradient(dp1dxj, T, dTdxj);
  double mu, lambda, kappa;
  ns->computeNonDimensionalLaminarTransportCoefficients(Tcg, tag, mu, lambda, kappa, code);
  ns->computeMaxwellSlipSurfaceTermNS(mu, lambda, kappa, Vwall, rhocg, ucg, dudxj, dTdxj, n, R);
}

//------------------------------------------------------------------------------

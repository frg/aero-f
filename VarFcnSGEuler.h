#ifndef _VAR_FCN_SGEULER_H
#define _VAR_FCN_SGEULER_H

#include <VarFcnBase.h>

#include <fstream>
#include <string>

//--------------------------------------------------------------------------
// This class is the VarFcn class for the Stiffened Gas EOS in Euler
// Equations. Only elementary functions are declared and/or defined here.
// All arguments must be pertinent to only a single grid node or a single
// state.
//
// lay-out of the base class is:
//  - 1 -  Transformation Operators
//  - 2 -  General Functions
//  - 3 -  Equations of State Parameters
//  - 4 -  EOS related functions
//
//--------------------------------------------------------------------------
//
// EOS: Pressure = (gam - 1)*Density*e - gam*Pc
// where
//   e  : internal energy per unit mass.
//   Pc : pressure constant.
//
//   Note that the complete EOS is given by the above and h = cp * T
//   where cp is constant. For a perfect gas, this leads to epsilon = cv * T
//   but for a stiffened gas, epsilon = cv * T does not hold.
//   For a stiffened gas, choosing epsilon = cv * T would lead to a non-constant cp...
//
//--------------------------------------------------------------------------

class VarFcnSGEuler : public VarFcnBase {

 protected:
  double gam;
  double gam1;
  double invgam1;
  double Pstiff;
  double dPstiff;

  void computedVdU(double *, double *);
  void computedUdV(double *, double *);

 public:
  // baseClass determines if VarFcnSG is used as a base class
  // for another class (like VarFcnSGSA and VarFcnSGKE)
  VarFcnSGEuler(FluidModelData& data, bool baseClass = false);
  virtual ~VarFcnSGEuler() {
    delete[] pname;
  }

  void rstVar(IoData&, FluidModelData&);

  bool equal(VarFcnBase *oth) {
    if(oth->type == VarFcnBase::STIFFENEDGAS || oth->type == VarFcnBase::PERFECTGAS) {
      VarFcnSGEuler *othsg = static_cast<VarFcnSGEuler *>(oth);
      if(gam == othsg->gam && Pstiff == othsg->Pstiff) {
        return true;
      }
    }
    return false;
  }

  //----- Transformation Operators -----//
  void conservativeToPrimitive(double *, double *);
  void primitiveToConservative(double *, double *);
  void conservativeToPrimitiveDerivative(double *, double *, double *, double *, double);
  void primitiveToConservativeDerivative(double *, double *, double *, double *, double);
  void computeConservativeToPrimitiveDerivativeOperators(double *, double *, double *, double *);
  void computePrimitiveToConservativeDerivativeOperators(double *, double *, double *, double *);

  void extrapolatePrimitive(double, double, double *, double *, double *);
  void extrapolateCharacteristic(double[3], double, double, double *, double *);
  void primitiveToCharacteristicVariations(double[3], double *, double *, double *);
  void characteristicToPrimitiveVariations(double[3], double *, double *, double *);

  //----- General Functions -----//
  int verification(int_t, double *, double *);

  double getPressure(double *V) const {
    return V[4];
  }
  void computedPdV(double *dPdV) const {
    dPdV[0] = dPdV[1] = dPdV[2] = dPdV[3] = 0.0;
    dPdV[4] = 1.0;
  }

  void setPressure(double p, double *V) {
    V[4] = p;
  }
  void setPressure(double *V, double *Vorig) {
    V[4] = Vorig[4];
  }
  double checkPressure(double *V) const final {
    return V[4] + Pstiff;
  }
  double computeTemperature(double *V) const {
#ifndef NDEBUG
    if(std::isnan(1.0 / V[0])) {
      fprintf(stderr, "*** Error: nan encountered in computeTemperature()\n");
    }
#endif
    return invgam1 * (V[4] + Pstiff) / V[0];
  }
  void getV4FromTemperature(double *V, double T) const {
    V[4] = T * V[0] * gam1 - Pstiff;
  }
  void getdV4fromdVg(double *Vg, double *dVg, double& dV4) const { // dVg is where V4 is temperature
    dV4 = (dVg[4] * Vg[0] + Vg[4] * dVg[0]) * gam1;
  }
  double computeRhoEnergy(double *V) const {
    return invgam1 * (V[4] + gam * Pstiff) + 0.5 * V[0] * (V[1] * V[1] + V[2] * V[2] + V[3] * V[3]);
  }
  double computeRhoEnergy(double rho, double p, double T, double vel) const {
    return invgam1 * (p + gam * Pstiff) + 0.5 * rho * vel * vel;
  }
  double computeRhoEpsilon(double *V) const {
    return invgam1 * (V[4] + gam * Pstiff);
  }

  double computeSoundSpeed(double *V) const {
#ifndef NDEBUG
    if(V[4] + Pstiff < 0 || V[0] <= 0) {
      fprintf(stderr, "VarFcnSGEuler::computeSoundSpeed V[4]=%e, Pstiff=%e, V[0]=%e\n", V[4], Pstiff, V[0]);
    }
#endif
    return sqrt(gam * (V[4] + Pstiff) / V[0]);
  }
  double computeSoundSpeed(double density, double entropy) const {
    double c2 = gam * entropy * pow(density, gam1);
    if(c2 > 0) {
      return sqrt(c2);
    }
    return 0.0;
  }
  double computeSoundSpeed2(double density, double pressure) const {
    return gam * (pressure + Pstiff) / density;
  }
  double computeEntropy(double density, double pressure) const {
    return (pressure + Pstiff) / pow(density, gam);
  }
  double computeDensity(double pressure, double temperature) const {
    return invgam1 * (pressure + Pstiff) / temperature;
  }
  double computeIsentropicPressure(double entropy, double density) const {
    return entropy * pow(density, gam) - Pstiff;
  }
  double computeTotalPressure(double machr, double *V) const {
    double mach = computeMachNumber(V);
    double opmach = 1.0 + 0.5 * gam1 * mach * mach;
    return (V[4] + Pstiff) * pow(opmach, gam * invgam1) - Pstiff;
  }

  double computedTdp(double rho, double p) const {
    return invgam1 / rho;
  }
  double computedTdrho(double rho, double p) const {
    return -invgam1 * (p + Pstiff) / rho / rho;
  }
  double computedrhodT(double rho, double p) const {
    double T = invgam1 * (p + Pstiff) / rho;
    return -invgam1 * (p + Pstiff) / T / T;
  }
  double computedpdT(double rho, double p) const {
    return gam1 * rho;
  }
  double computedrhodp(double rho, double p) const {
    return rho / (p + Pstiff);
  }

  double computePressureCoefficient(double *V, double pinfty, double mach, bool dimFlag) const {
    if(dimFlag) {
      return 2.0 * (V[4] - pinfty);
    }
    else {
      return 2.0 * (V[4] - 1.0 / (gam * mach * mach));  // a priori, valid only for Perfect Gas
    }
  }
  // specific heat at constant pressure is gamma for Perfect Gas and Stiffened Gas with h = cp * T
  double specificHeatCstPressure() const {
    return gam;
  }

  double computeDerivativeOfTemperature(double *V, double *dV) const { // TODO dPstiff
    return (invgam1 * dV[4] - computeTemperature(V) * dV[0]) / V[0];
  }

  void computeDerivativeOperatorsOfTemperature(double *V, double *dTdV) const { // TODO dPstiff
    dTdV[0] = -computeTemperature(V) / V[0];
    dTdV[1] = dTdV[2] = dTdV[3] = 0.0;
    dTdV[4] = invgam1 / V[0];
  }

  void computeJacobianTemperature(double *V, double *dTdU) const {
    const double invrho = 1.0 / V[0];
    dTdU[0] = -invrho * (computeTemperature(V) - 0.5 * (V[1] * V[1] + V[2] * V[2] + V[3] * V[3]));
    dTdU[1] = -invrho * V[1];
    dTdU[2] = -invrho * V[2];
    dTdU[3] = -invrho * V[3];
    dTdU[4] = invrho;
  }

  double computeDerivativeOfMachNumber(double *V, double *dV, double dMach) const {
    // Fix when the speed is 0
    double MyMach = computeMachNumber(V);
    if(MyMach == 0.0) {
      return 0.0;
    }
    return 1 / (2.0 * sqrt((V[1] * V[1] + V[2] * V[2] + V[3] * V[3]) * V[0] / (gam * (V[4] + Pstiff)))) * (((2.0 *
           (V[1] * dV[1] + V[2] * dV[2] + V[3] * dV[3]) * V[0] + (V[1] * V[1] + V[2] * V[2] + V[3] * V[3]) * dV[0]) * (V[4] + Pstiff) -
           (V[1] * V[1] + V[2] * V[2] + V[3] * V[3]) * V[0] * (dV[4] + dPstiff * dMach)) / ((V[4] + Pstiff) * (V[4] + Pstiff)));
  }
  double computeDerivativeOfSoundSpeed(double *V, double *dV, double dMach) const {
    return 1.0 / (2.0 * sqrt(gam * (V[4] + Pstiff) / V[0])) * gam * ((dV[4] + dPstiff * dMach) * V[0] - (V[4] + Pstiff) * dV[0]) / (V[0] * V[0]);
  }
  double computeDerivativeOfTotalPressure(double machr, double dmachr, double *V, double *dV, double dMach) const {
    double mach = computeMachNumber(V);
    double dmach = computeDerivativeOfMachNumber(V, dV, dMach);
    double opmach = 1.0 + 0.5 * gam1 * mach * mach;
    double dopmach = gam1 * mach * dmach;
    return dV[4] * pow(opmach, gam * invgam1) + (V[4] + Pstiff) * gam * invgam1 * pow(opmach, (gam * invgam1 - 1)) * dopmach;
  }

  //----- Equation of State Parameters -----//
  double getGamma() const final {
    return gam;
  }
  double getGamma1() const final {
    return gam1;
  }
  double getPressureConstant() const final {
    return Pstiff;
  }
  double getDerivativeOfPressureConstant() const final {
    return dPstiff;
  }

  //----- EOS related functions -----//

};

//------------------------------------------------------------------------------

inline
VarFcnSGEuler::VarFcnSGEuler(FluidModelData& data, bool baseClass) : VarFcnBase(data) {
  if(data.fluid != FluidModelData::PERFECT_GAS && data.fluid != FluidModelData::STIFFENED_GAS) {
    fprintf(stderr, "*** Error: FluidModelData is not of type GAS\n");
    exit(1);
  }
  if(data.gasModel.type == GasModelData::IDEAL) {
    type = PERFECTGAS;
  }
  else if(data.gasModel.type == GasModelData::STIFFENED) {
    type = STIFFENEDGAS;
  }
  else {
    fprintf(stdout, "*** Error: VarFcnSGEuler::type is undefined since data.gasModel.type = %d\n", data.gasModel.type);
  }
  gam = data.gasModel.specificHeatRatio;
  gam1 = gam - 1.0;
  invgam1 = 1.0 / gam1;
  Pstiff = data.gasModel.pressureConstant;
  dPstiff = 0.0;
  if(!baseClass) {
    pname = new const char *[5];
    pname[0] = "density";
    pname[1] = "x-velocity";
    pname[2] = "y-velocity";
    pname[3] = "z-velocity";
    pname[4] = "pressure";
  }
}

//------------------------------------------------------------------------------

inline
void VarFcnSGEuler::rstVar(IoData& iod, FluidModelData& data) {
  Pstiff = data.gasModel.pressureConstant;
  dPstiff = -2.0 * Pstiff / iod.ref.mach;
  VarFcnBase::rstVar(iod, data);
}

//------------------------------------------------------------------------------

inline
void VarFcnSGEuler::conservativeToPrimitive(double *U, double *V) {
  V[0] = U[0];
  double invRho = U[0] > 0.0 ? 1.0 / U[0] : 0.0;
  V[1] = U[1] * invRho;
  V[2] = U[2] * invRho;
  V[3] = U[3] * invRho;
  double vel2 = V[1] * V[1] + V[2] * V[2] + V[3] * V[3];
  V[4] = gam1 * (U[4] - 0.5 * U[0] * vel2) - gam * Pstiff;
}

//------------------------------------------------------------------------------

inline
void VarFcnSGEuler::primitiveToConservative(double *V, double *U) {
  double vel2 = V[1] * V[1] + V[2] * V[2] + V[3] * V[3];
  U[0] = V[0];
  U[1] = V[0] * V[1];
  U[2] = V[0] * V[2];
  U[3] = V[0] * V[3];
  U[4] = (V[4] + gam * Pstiff) * invgam1 + 0.5 * V[0] * vel2;
}

//------------------------------------------------------------------------------

inline
void VarFcnSGEuler::conservativeToPrimitiveDerivative(double *U, double *dU, double *V, double *dV, double dMach) {
  dV[0] = dU[0];
  double invRho = 1.0 / V[0];
  dV[1] = (dU[1] - dV[0] * V[1]) * invRho;
  dV[2] = (dU[2] - dV[0] * V[2]) * invRho;
  dV[3] = (dU[3] - dV[0] * V[3]) * invRho;
  double vel2 = V[1] * V[1] + V[2] * V[2] + V[3] * V[3];
  double dvel2 = 2.0 * V[1] * dV[1] + 2.0 * V[2] * dV[2] + 2.0 * V[3] * dV[3];
  dV[4] = gam1 * (dU[4] - 0.5 * dU[0] * vel2  - 0.5 * U[0] * dvel2) - gam * dPstiff * dMach;
}

//------------------------------------------------------------------------------

inline
void VarFcnSGEuler::primitiveToConservativeDerivative(double *V, double *dV, double *U, double *dU, double dMach) {
  double vel2 = V[1] * V[1] + V[2] * V[2] + V[3] * V[3];
  double dvel2 = 2.0 * V[1] * dV[1] + 2.0 * V[2] * dV[2] + 2.0 * V[3] * dV[3];
  dU[0] = dV[0];
  dU[1] = dV[0] * V[1] + V[0] * dV[1];
  dU[2] = dV[0] * V[2] + V[0] * dV[2];
  dU[3] = dV[0] * V[3] + V[0] * dV[3];
  dU[4] = (dV[4] + gam * dPstiff * dMach) * invgam1 + 0.5 * dV[0] * vel2 + 0.5 * V[0] * dvel2;
}

//------------------------------------------------------------------------------

inline
void VarFcnSGEuler::computeConservativeToPrimitiveDerivativeOperators(double *U, double *V, double *dVdU, double *dVdPstiff) {
  computedVdU(V, dVdU);
  dVdPstiff[4] = -gam;
}

//------------------------------------------------------------------------------

inline
void VarFcnSGEuler::computePrimitiveToConservativeDerivativeOperators(double *U, double *V, double *dUdV, double *dUdPstiff) {
  computedUdV(V, dUdV);
  dUdPstiff[4] = gam * invgam1;
}

//------------------------------------------------------------------------------

inline
void VarFcnSGEuler::extrapolatePrimitive(double un, double c, double *Vb,
                                         double *Vinter, double *V) {
  if(un == 0.0) {
    V[0] = Vb[0];
    V[1] = Vb[1];
    V[2] = Vb[2];
    V[3] = Vb[3];
    V[4] = Vinter[4];
  }
  else {
    if(un < 0.0) { // INLET
      if(-un - c > 0.0) { // SUPERSONIC
        V[0] = Vb[0];
        V[1] = Vb[1];
        V[2] = Vb[2];
        V[3] = Vb[3];
        V[4] = Vb[4];
      }
      else { // SUBSONIC
        V[0] = Vb[0];
        V[1] = Vb[1];
        V[2] = Vb[2];
        V[3] = Vb[3];
        V[4] = Vinter[4];
      }
    }
    else { // OUTLET
      if(un - c > 0.0) { // SUPERSONIC
        V[0] = Vinter[0];
        V[1] = Vinter[1];
        V[2] = Vinter[2];
        V[3] = Vinter[3];
        V[4] = Vinter[4];
      }
      else { // SUBSONIC
        V[0] = Vinter[0];
        V[1] = Vinter[1];
        V[2] = Vinter[2];
        V[3] = Vinter[3];
        V[4] = Vb[4];
      }
    }
  }
}

//------------------------------------------------------------------------------

inline
void VarFcnSGEuler::extrapolateCharacteristic(double n[3], double un, double c,
                                              double *Vb, double *dV) {
  /* routine computes boundary conditions using characteristic methods
   * and assuming that values are small perturbations of values at infinity
   * initially dV contains perturbations of primitive variables to be extrapolated
   * at return, dV contains perturbations of primitive variables at the boundary
   */
  double dVn = dV[1] * n[0] + dV[2] * n[1] + dV[3] * n[2];
  double ooc2 = 1.0 / (c * c);
  double oorhoc = sqrt(ooc2) / Vb[0];
  double coeff1 = dV[0] - ooc2 * dV[4];
  // step 1: primitive to characteristic variations
  double dW[5];
  dW[0] = n[0] * coeff1 + n[2] * dV[2] - n[1] * dV[3];
  dW[1] = n[1] * coeff1 + n[0] * dV[3] - n[2] * dV[1];
  dW[2] = n[2] * coeff1 + n[1] * dV[1] - n[0] * dV[2];
  dW[3] = dVn + oorhoc * dV[4];
  dW[4] = -dVn + oorhoc * dV[4];
  // step 2: choose variations to be extrapolated
  //         if incoming characteristic, then the
  //         characteristic is set to 0.0
  //         else, there is nothing to do
  if(un == 0.0) {
    dW[0] = 0.0;
    dW[1] = 0.0;
    dW[2] = 0.0;
    dW[3] = 0.0;
  }
  else {
    if(un < 0.0) { // INLET
      if(-un - c > 0.0) { // SUPERSONIC
        dW[0] = 0.0;
        dW[1] = 0.0;
        dW[2] = 0.0;
        dW[3] = 0.0;
        dW[4] = 0.0;
      }
      else { // SUBSONIC
        dW[0] = 0.0;
        dW[1] = 0.0;
        dW[2] = 0.0;
        dW[3] = 0.0;
      }
    }
    else { // OUTLET
      if(un - c > 0.0) { // SUPERSONIC
      }
      else { // SUBSONIC
        dW[4] = 0.0;
      }
    }
  }
  // step 3: characteristic to primitive variations
  double sum = dW[3] + dW[4];
  double diff = dW[3] - dW[4];
  dV[0] = dW[0] * n[0] + dW[1] * n[1] + dW[2] * n[2] + 0.5 * Vb[0] * sum / c;
  dV[1] = n[1] * dW[2] - n[2] * dW[1] + 0.5 * n[0] * diff;
  dV[2] = n[2] * dW[0] - n[0] * dW[2] + 0.5 * n[1] * diff;
  dV[3] = n[0] * dW[1] - n[1] * dW[0] + 0.5 * n[2] * diff;
  dV[4] = 0.5 * Vb[0] * c * sum;
}

//------------------------------------------------------------------------------

inline
void VarFcnSGEuler::primitiveToCharacteristicVariations(double n[3], double *V,
                                                        double *dV, double *dW) {
  double dVn = dV[1] * n[0] + dV[2] * n[1] + dV[3] * n[2];
  double ooc2 = V[0] / (gam * (V[4] + Pstiff));
  double oorhoc = sqrt(ooc2) / V[0];
  double coeff1 = dV[0] - ooc2 * dV[4];
  dW[0] = n[0] * coeff1 + n[2] * dV[2] - n[1] * dV[3];
  dW[1] = n[1] * coeff1 + n[0] * dV[3] - n[2] * dV[1];
  dW[2] = n[2] * coeff1 + n[1] * dV[1] - n[0] * dV[2];
  dW[3] = dVn + oorhoc * dV[4];
  dW[4] = -dVn + oorhoc * dV[4];
}

//-----------------------------------------------------------------------------

inline
void VarFcnSGEuler::characteristicToPrimitiveVariations(double n[3], double *V,
                                                        double *dW, double *dV) {
  double sum = dW[3] + dW[4];
  double diff = dW[3] - dW[4];
  double c = sqrt(gam * (V[4] + Pstiff) / V[0]);
  dV[0] = dV[1] * n[0] + dV[2] * n[1] + dV[3] * n[2] + 0.5 * V[0] * sum / c;
  dV[1] = n[1] * dV[2] - n[2] * dV[1] + 0.5 * n[0] * diff;
  dV[2] = n[2] * dV[0] - n[0] * dV[2] + 0.5 * n[1] * diff;
  dV[3] = n[0] * dV[1] - n[1] * dV[0] + 0.5 * n[2] * diff;
  dV[4] = 0.5 * V[0] * c * sum;
}

//------------------------------------------------------------------------------

inline
int VarFcnSGEuler::verification(int_t glob, double *U, double *V) {
  // verification of density and pressure value
  // if pressure/density < pmin/rhomin, set pressure/density to pmin/rhomin
  // and rewrite V and U!!
  int count = 0;
  if(V[0] < rhomin) {
    if(verif_clipping) {
      fprintf(stderr, "clip density[%s] in gas(Euler) from %e to %e\n", std::to_string(glob).c_str(), V[0], rhomin);
    }
    V[0] = rhomin;
    count += (count + 1) % 2;
  }
  if(V[4] < pmin) {
    if(verif_clipping) {
      fprintf(stdout, "clip pressure[%s] in gas(Euler) from %e to %e\n", std::to_string(glob).c_str(), V[4], pmin);
    }
    V[4] = pmin;
    count += 2;
  }
  if(count) {
    primitiveToConservative(V, U); // also modify U
    conservativeToPrimitive(U, V); // due to roundoff resulting pressure can be less than pmin or even negative!
  }
  return count;
}

//------------------------------------------------------------------------------

inline
void VarFcnSGEuler::computedVdU(double *V, double *dVdU) {
  double invrho = 1.0 / V[0];
  dVdU[0]  = 1.0;
  dVdU[5]  = -invrho * V[1];
  dVdU[6]  = invrho;
  dVdU[10] = -invrho * V[2];
  dVdU[12] = invrho;
  dVdU[15] = -invrho * V[3];
  dVdU[18] = invrho;
  dVdU[20] = gam1 * 0.5 * (V[1] * V[1] + V[2] * V[2] + V[3] * V[3]);
  dVdU[21] = -gam1 * V[1];
  dVdU[22] = -gam1 * V[2];
  dVdU[23] = -gam1 * V[3];
  dVdU[24] = gam1;
}

//------------------------------------------------------------------------------

inline
void VarFcnSGEuler::computedUdV(double *V, double *dUdV) {
  dUdV[0]  = 1.0;
  dUdV[5]  = V[1];
  dUdV[6]  = V[0];
  dUdV[10] = V[2];
  dUdV[12] = V[0];
  dUdV[15] = V[3];
  dUdV[18] = V[0];
  dUdV[20] = 0.5 * (V[1] * V[1] + V[2] * V[2] + V[3] * V[3]);
  dUdV[21] = V[0] * V[1];
  dUdV[22] = V[0] * V[2];
  dUdV[23] = V[0] * V[3];
  dUdV[24] = invgam1;
}

//------------------------------------------------------------------------------

#endif

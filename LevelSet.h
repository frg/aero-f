#ifndef _LEVEL_SET_H_
#define _LEVEL_SET_H_

#include <DistVector.h>
#include <IoData.h>
#include <NodeData.h>

struct ClosestPoint;

class Communicator;
class Domain;
class FluidSelector;
class TimeData;
class VarFcn;

// This LevelSet class stores the level-set at the previous time-steps
// and helps compute the next-step level-set as well as the
// reinitialization of the level-set
// Note that the current level-set is not stored here, but is available
// in TsDesc.

template<int dimLS>
class LevelSet {

 private:
  Communicator *com;
  int numLocSub;
  Domain *domain;
  TimeData *data;

  int nLevelSet;
  std::vector<bool> trueLevelSet; // some level-sets take only values -1 and +1 (volumeID initialization)
                                  // and do not need to be advected, since they don't represent a
                                  // fluid-fluid interface. However, other level-sets are distance functions
                                  // to a fluid-fluid interface and must therefore be updated and reinitialized.

  // for reinitialization from input file
  MultiFluidData::CopyCloseNodes copy;  // true if nodes close to interface are unchanged
  int bandlevel;                        // number of node layers to reinitialize
  double conv_eps;
  bool diff;

  // for reinitialization
  DistVec<double> Psi;                  // the steady state solution of Psi will reinitialize the level set
  DistVec<int> Tag;                     // node tags for reinitialization in a narrow band

 public:
  DistSVec<double, dimLS> Phin;
  DistSVec<double, dimLS> Phinm1;
  DistSVec<double, dimLS> Phinm2;

  LevelSet(IoData&, Domain *);
  ~LevelSet();

  void resize(NodeData *);

  // initialization routines
  template<int dim>
  void setup(const char *, DistSVec<double, 3>&, DistSVec<double, dim>&, DistSVec<double, dimLS>&,
             IoData&, FluidSelector *, VarFcn *, DistVec<ClosestPoint> *, DistVec<double> *,
             DistVec<int> *);

  void setupPhiVolumesInitialConditions(IoData&, DistSVec<double, dimLS>&);

  template<int dim>
  void setupPhiOneDimensionalSolution(IoData&, DistSVec<double, dim>&,
                                      DistSVec<double, dimLS>&, FluidSelector *,
                                      VarFcn *);

  void setupPhiMultiFluidInitialConditions(IoData&, DistSVec<double, dimLS>&);

  void setupPhiFluidStructureInitialConditions(IoData&, DistSVec<double, 3>&, DistSVec<double, dimLS>&,
                                               DistVec<ClosestPoint>&, DistVec<double>&, DistVec<int>&,
                                               FluidSelector *);

  // update the members of the class and writing them to the disk
  void checkTrueLevelSetUpdate(DistSVec<double, dimLS>&);
  void update(DistSVec<double, dimLS>&);
  void writeToDisk(char *);

  // reinitialization routines
  void reinitializeLevelSet(DistSVec<double, 3>&, DistSVec<double, dimLS>&, bool = true, int = -1);
  void reinitializeLevelSetFM(DistSVec<double, 3>&, DistSVec<double, dimLS>&, bool, int);

  // switching from conservative (rho*phi) to primitive (phi) and vice-versa
  template<int dim>
  void conservativeToPrimitive(DistSVec<double, dimLS>&, DistSVec<double, dimLS>&,
                               DistSVec<double, dim>&);

  template<int dim>
  void primitiveToConservative(DistSVec<double, dimLS>&, DistSVec<double, dimLS>&,
                               DistSVec<double, dim>&);

  DistSVec<double, dimLS>& getPhinm1() {
    return Phinm1;
  }

};

#endif

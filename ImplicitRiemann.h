#ifndef _IMPLICIT_RIEMANN_H_
#define _IMPLICIT_RIEMANN_H_

class VarFcn;

class ImplicitRiemann {

 public:
  static void computeGasGasJacobian(double, double, double, double, double, double, double, double,
                                    double, double *, double *, double *, double *);

  static void computeTaitTaitJacobian(double, double, double, double, double, double, double,
                                      double, double, double, double, double *, double *, double *,
                                      double *);

  static void computeGasTaitJacobian(double, double, double, double, double, double, double, double,
                                     double, double, double *, double *, double *, double *, double,
                                     bool);

  static void computeJwlJwlJacobian(VarFcn *, int, int, double *, double *, double *, double *,
                                    double *, double *, double *, double *);

  static void computeGasJwlJacobian(VarFcn *, int, int, double *, double *, double *, double *,
                                    double *, double *, double *, double *, double *);

  static void computeTaitJwlJacobian(VarFcn *, int, int, double *, double *, double *, double *,
                                     double *, double *, double *, double *, double *);
 private:
  static double computeJwlIntegral2(VarFcn *, int, double, double, double, double);

  static void computeGasDerivRarefaction2x2(double, double, double, double, double, double[6],
                                            double, double);

  static void computeGasDerivShock2x2(double, double, double, double, double, double[6]);

  static void computeTaitDerivRarefaction2x2(double, double, double, double, double, double,
                                             double[6], double, double);

  static void computeTaitDerivShock2x2(double, double, double, double, double, double, double[6]);

  static void computeJwlDerivRarefaction(VarFcn *, int, double, double, double, double, double[6],
                                         double, double, double * = nullptr);

  static void computeJwlDerivShock(double, double, double, double[6], double, double, double,
                                   double, double, double, double);

  static void computeInterfaceQuantities2x2(double[6], double[6], double *, double *, double *,
                                            double *);
};

#endif

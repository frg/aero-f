#include <EmbeddedCorotSolver.h>
#include <BcDef.h>
#include <Domain.h>
#include <DenseMatrixOps.h>
#include <BCApplier.h>
#include <Node.h>
#include <SubDomain.h>
#include <LevelSet/LevelSetStructure.h>

#include <cstdlib>
#include <cmath>

//------------------------------------------------------------------------------

EmbeddedCorotSolver::EmbeddedCorotSolver(IoData& iod, Domain *dom, DistLevelSetStructure *distLSS, int _masterSurfaceID,
                                         DistSVec<double, 3> *_X0, std::string _corotmatrix)
  : ioData(iod), domain(dom), Xs0(distLSS->getStructPosition_0()), stElem(distLSS->getStructElems()), nodesType(distLSS->getNodesType()),
    surfaceID(distLSS->getSurfaceID()), masterSurfaceID(_masterSurfaceID), X0(_X0), corotmatrix(_corotmatrix) {

  numLocSub = domain->getNumLocSub();
  com = domain->getCommunicator();

  meshMotionBCs = new BCApplier(domain->getNumLocSub(), domain, ioData);
  computeCG(Xs0, cg0);

  cgN[0] = cg0[0];
  cgN[1] = cg0[1];
  cgN[2] = cg0[2];
  double zeroRot[3] = {0.0, 0.0, 0.0};
  computeRotMat(zeroRot, R);
  // look if a symmetry plane was specified in the input file
  n[0] = ioData.dmesh.symmetry.nx;
  n[1] = ioData.dmesh.symmetry.ny;
  n[2] = ioData.dmesh.symmetry.nz;
  double nrm = sqrt(n[0] * n[0] + n[1] * n[1] + n[2] * n[2]);
  SymAxis = EmbeddedCorotSolver::NONE;
  if(nrm != 0.0) {
    n[0] /= nrm;
    n[1] /= nrm;
    n[2] /= nrm;
    if((fabs(n[0]) == 1.0) && (n[1] == 0.0) && (n[2] == 0.0)) {
      SymAxis = EmbeddedCorotSolver::AXIS_X;
    }
    else if((n[0] == 0.0) && (fabs(n[1]) == 1.0) && (n[2] == 0.0)) {
      SymAxis = EmbeddedCorotSolver::AXIS_Y;
    }
    else if((n[0] == 0.0) && (n[1] == 0.0) && (fabs(n[2]) == 1.0)) {
      SymAxis = EmbeddedCorotSolver::AXIS_Z;
    }
    else {
      com->fprintf(stderr, "*** Error: embedded corotational solver only supports a canonical plane as a symmetry plane.\n");
      exit(-1);
    }
  }
  switch(SymAxis) {
    case(EmbeddedCorotSolver::NONE):
      com->fprintf(stderr, " ... No symmetry plane is used in the embedded corotational solver.\n");
      com->fprintf(stderr, "     -> the 3 rotations axis (X,Y,Z) & 3 translation axis (X,Y,Z) are used.\n");
      break;
    case(EmbeddedCorotSolver::AXIS_X):
      com->fprintf(stderr, " ... Symmetry plane of normal X is used in the embedded corotational solver.\n");
      com->fprintf(stderr, "     -> only rotation around axis X & translations in the Y-Z plane are allowed.\n");
      break;
    case(EmbeddedCorotSolver::AXIS_Y):
      com->fprintf(stderr, " ... Symmetry plane of normal Y is used in the embedded corotational solver.\n");
      com->fprintf(stderr, "     -> only rotation around axis Y & translations in the X-Z plane are allowed.\n");
      break;
    case(EmbeddedCorotSolver::AXIS_Z):
      com->fprintf(stderr, " ... Symmetry plane of normal Z is used in the embedded corotational solver.\n");
      com->fprintf(stderr, "     -> only rotation around axis Z & translations in the X-Y plane are allowed.\n");
      break;
  }

  cm0[0] = ioData.dmesh.mass.XCM;
  cm0[1] = ioData.dmesh.mass.YCM;
  cm0[2] = ioData.dmesh.mass.ZCM;
  cmN[0] = ioData.dmesh.mass.XCM;
  cmN[1] = ioData.dmesh.mass.YCM;
  cmN[2] = ioData.dmesh.mass.ZCM;
}

//------------------------------------------------------------------------------

EmbeddedCorotSolver::~EmbeddedCorotSolver() {
  delete meshMotionBCs;
}

//------------------------------------------------------------------------------

void EmbeddedCorotSolver::resize2() {
  delete meshMotionBCs;
  meshMotionBCs = new BCApplier(domain->getNumLocSub(), domain, ioData);
}

//------------------------------------------------------------------------------

inline
void invRotLocVec(double mat[3][3], double v[3]) {
  double c[3];
  for(int j = 0; j < 3; j++) {
    c[j] = mat[0][j] * v[0] + mat[1][j] * v[1] + mat[2][j] * v[2];
  }
  v[0] = c[0];
  v[1] = c[1];
  v[2] = c[2];
}

//------------------------------------------------------------------------------

void EmbeddedCorotSolver::computeRotGradAndJac(Vec<Vec3D>& Xs, double RR[3][3],
                                               double cg1[3], double grad[3], double jac[3][3]) {
  for(int i = 0; i < 3; i++) {
    grad[i] = 0.0;
    for(int j = 0; j < 3; j++) {
      jac[i][j] = 0.0;
    }
  }
  Vec3D rotGrad[3];
  for(int i = 0; i < Xs.size(); i++) {
    if((nodesType && nodesType[i] == BoundaryData::SYMMETRYPLANE) || (masterSurfaceID >= 0 && surfaceID && surfaceID[i] != masterSurfaceID)) continue;
    double rd[3];
    // rotate the local vectors using R(n-1)
    rd[0] = Xs0[i][0] - cg0[0];
    rd[1] = Xs0[i][1] - cg0[1];
    rd[2] = Xs0[i][2] - cg0[2];
    rotLocVec(RR, rd);
    Vec3D eVec;
    // compute freq. used values
    eVec[0] = Xs[i][0] - cg1[0] - rd[0];
    eVec[1] = Xs[i][1] - cg1[1] - rd[1];
    eVec[2] = Xs[i][2] - cg1[2] - rd[2];
    rotGrad[0][0] = -rd[1];
    rotGrad[0][1] = rd[0];
    rotGrad[0][2] = 0;
    rotGrad[1][0] = rd[2];
    rotGrad[1][1] = 0;
    rotGrad[1][2] = -rd[0];
    rotGrad[2][0] = 0;
    rotGrad[2][1] = -rd[2];
    rotGrad[2][2] = rd[1];
    grad[0] += -2 * (eVec * rotGrad[0]);
    grad[1] += -2 * (eVec * rotGrad[1]);
    grad[2] += -2 * (eVec * rotGrad[2]);
    jac[0][0] +=  2 * (rd[0] * eVec[0]
                       + rd[1] * eVec[1]
                       + rotGrad[0][0] * rotGrad[0][0]
                       + rotGrad[0][1] * rotGrad[0][1]);
    jac[0][1] += -2 * (rd[2] * eVec[1]
                       - rotGrad[0][0] * rotGrad[1][0]);
    jac[0][2] += -2 * (rd[2] * eVec[0]
                       - rotGrad[0][1] * rotGrad[2][1]);
    jac[1][1] += 2 * (rd[0] * eVec[0]
                      + rd[2] * eVec[2]
                      + rotGrad[1][0] * rotGrad[1][0]
                      + rotGrad[1][2] * rotGrad[1][2]);
    jac[1][2] += -2 * (rd[1] * eVec[0]
                       -  rotGrad[2][2] * rotGrad[1][2]);
    jac[2][2] += 2 * (rd[2] * eVec[2]
                      + rd[1] * eVec[1]
                      + rotGrad[2][1] * rotGrad[2][1]
                      + rotGrad[2][2] * rotGrad[2][2]);
  }
  // fill in symmetric terms
  jac[1][0] = jac[0][1];
  jac[2][0] = jac[0][2];
  jac[2][1] = jac[1][2];
}

//------------------------------------------------------------------------------

void EmbeddedCorotSolver::rotLocVec(double mat[3][3], double v[3]) {
  double c[3];
  for(int j = 0; j < 3; j++)
    c[j] = mat[j][0] * v[0] +
           mat[j][1] * v[1] +
           mat[j][2] * v[2];
  v[0] = c[0];
  v[1] = c[1];
  v[2] = c[2];
}

//------------------------------------------------------------------------------
// computes delta R
void EmbeddedCorotSolver::computeRotMat(double *angle, double mat[3][3]) {
  // trig functions of angles
  double c1 = cos(angle[0]);
  double s1 = sin(angle[0]);
  double c2 = cos(angle[1]);
  double s2 = sin(angle[1]);
  double c3 = cos(angle[2]);
  double s3 = sin(angle[2]);
  // compute rotation matrix computed as R1.R2.R3
  // where R1 is rotation about z
  //       R2 is rotation about y
  //       R3 is rotation about x
  mat[0][0] = c1 * c2;
  mat[0][1] = c1 * s2 * s3 - c3 * s1;
  mat[0][2] = c1 * c3 * s2 + s1 * s3;
  mat[1][0] = c2 * s1;
  mat[1][1] = c1 * c3 + s1 * s2 * s3;
  mat[1][2] = c3 * s1 * s2 - c1 * s3;
  mat[2][0] = -s2;
  mat[2][1] = c2 * s3;
  mat[2][2] = c2 * c3;
}

//------------------------------------------------------------------------------

void EmbeddedCorotSolver::printRotMat(double mat[3][3]) {
  com->fprintf(stderr, " Rotation matrix = \n");
  for(int i = 0; i < 3; i++) {
    for(int j = 0; j < 3; j++) {
      com->fprintf(stderr, " %e  ", mat[i][j]);
    }
    com->fprintf(stderr, "\n");
  }
}

//------------------------------------------------------------------------------

void EmbeddedCorotSolver::readRotationMatrix() {
  FILE *fp = fopen(corotmatrix.c_str(), "r");
  if(!fp) {
    com->fprintf(stderr, "*** Error: could not open \'%s\'\n", corotmatrix.c_str());
    exit(-1);
  }
  com->fprintf(stdout, "Read solution 0 from \'%s\'\n", corotmatrix.c_str());
  for(int i = 0; i < 3; ++i) {
    int ret = fscanf(fp, "%lf %lf %lf\n", &R[i][0], &R[i][1], &R[i][2]);
  }
  fclose(fp);
}

//------------------------------------------------------------------------------

void EmbeddedCorotSolver::updateCM(double cg1[3]) {
  int i;
  for(i = 0; i < 3; ++i) {
    cmN[i] = cm0[i] - cg0[i];
  }
  rotLocVec(R, cmN);
  for(i = 0; i < 3; ++i) {
    cmN[i] += cg1[i];
  }
}

//------------------------------------------------------------------------------

void EmbeddedCorotSolver::computeCG(Vec<Vec3D>& Xs, double cg[3]) {
  for(int j = 0; j < 3; ++j) {
    cg[j] = 0.0;
  }
  int count = 0;
  for(int i = 0; i < Xs.size(); ++i) {
    if((nodesType && nodesType[i] == BoundaryData::SYMMETRYPLANE) || (masterSurfaceID >= 0 && surfaceID && surfaceID[i] != masterSurfaceID)) continue;
    for(int j = 0; j < 3; ++j) {
      cg[j] += Xs[i][j];
    }
    count++;
  }
  if(count != 0) {
    double invCount = 1.0 / count;
    for(int j = 0; j < 3; ++j) {
      cg[j] *= invCount;
    }
  }
}

//------------------------------------------------------------------------------

void EmbeddedCorotSolver::computeNodeRot(double RR[3][3], DistSVec<double, 3>& X,
                                         double cg00[3], double cg1[3]) {
  #pragma omp parallel for
  for(int iSub = 0; iSub < numLocSub; ++iSub) {
    double (*x)[3]  = X.subData(iSub);
    if(X0) {
      double (*x0)[3] = X0->subData(iSub);
      for(int i = 0; i < X.subSize(iSub); ++i) {
        x[i][0] = x0[i][0] - cg00[0];
        x[i][1] = x0[i][1] - cg00[1];
        x[i][2] = x0[i][2] - cg00[2];
        rotLocVec(RR, x[i]);
        x[i][0] = x[i][0] + cg1[0];
        x[i][1] = x[i][1] + cg1[1];
        x[i][2] = x[i][2] + cg1[2];
      }
    }
    else {
      NodeSet& x0 = domain->getSubDomain()[iSub]->getNodes();
      for(int i = 0; i < X.subSize(iSub); ++i) {
        x[i][0] = x0[i][0] - cg00[0];
        x[i][1] = x0[i][1] - cg00[1];
        x[i][2] = x0[i][2] - cg00[2];
        rotLocVec(RR, x[i]);
        x[i][0] = x[i][0] + cg1[0];
        x[i][1] = x[i][1] + cg1[1];
        x[i][2] = x[i][2] + cg1[2];
      }
    }
  }
}

//------------------------------------------------------------------------------

void EmbeddedCorotSolver::solveDeltaRot(Vec<Vec3D>& Xs, double deltaRot[3][3], double cg1[3]) {
  double jac[3][3], grad[3];
  double dRot[3][3];
  // initialize deltaRot to Identity Matrix
  double zeroRot[3] = {0.0, 0.0, 0.0};
  computeRotMat(zeroRot, deltaRot);
  int maxits = 10;
  double atol = 1.e-12;
  double rtol = 1.e-10;
  double res0, res, target;
  for(int iter = 0; iter < maxits; ++iter) {
    // compute rotation gradients and derivatives
    computeRotGradAndJac(Xs, R, cg1, grad, jac);
    // zero terms depending on the axis of the plane of symmetry
    switch(SymAxis) {
      case EmbeddedCorotSolver::NONE :
        break;
      case EmbeddedCorotSolver::AXIS_X : // grad[2] is rotation angle about X-axis
        grad[0] = grad[1] = 0.0;
        jac[0][1] = jac[1][0] = jac[0][2] = jac[2][0] = 0.0;
        jac[1][2] = jac[2][1] = 0.0;
        jac[0][0] = jac[1][1] = 1.0;
        break;
      case EmbeddedCorotSolver::AXIS_Y :
        grad[0] = grad[2] = 0.0;
        jac[0][1] = jac[1][0] = jac[0][2] = jac[2][0] = 0.0;
        jac[1][2] = jac[2][1] = 0.0;
        jac[0][0] = jac[2][2] = 1.0;
        break;
      case EmbeddedCorotSolver::AXIS_Z :
        grad[1] = grad[2] = 0.0;
        jac[0][1] = jac[1][0] = jac[0][2] = jac[2][0] = 0.0;
        jac[1][2] = jac[2][1] = 0.0;
        jac[1][1] = jac[2][2] = 1.0;
        break;
    }
    res = sqrt(grad[0] * grad[0] + grad[1] * grad[1] + grad[2] * grad[2]);
    if(iter == 0) {
      res0 = res;
      target = rtol * res0;
    }
    if(res <= atol || res <= target) {
      break;
    }
    // if the initial residual is already small
    // rotation results come back in grad
    solveRotMat(jac, grad);
    grad[0] *= -1.0;
    grad[1] *= -1.0;
    grad[2] *= -1.0;
    // update dRot
    computeRotMat(grad, dRot);
    denseMatrixTimesDenseMatrixInPlace(dRot, R);
    denseMatrixTimesDenseMatrixInPlace(dRot, deltaRot);
  }
  if(res > target && res > atol) {
    com->printf(1, "*** Warning: incremental rotation solver reached %d its", maxits);
    com->printf(1, " (initial res = %.2e, final res=%.2e, target=%.2e)\n", res0, res, target);
  }
}

//------------------------------------------------------------------------------

void EmbeddedCorotSolver::solveRotMat(double m[3][3], double v[3]) {
  for(int i = 0; i < 2; i++)
    for(int j = i + 1; j < 3; ++j) {
      double coef = m[j][i] / m[i][i];
      for(int k = i + 1; k < 3; ++k) {
        m[j][k] -= coef * m[i][k];
      }
      v[j] -= coef * v[i];
    }
  for(int i = 2; i >= 0; i--) {
    for(int j = 2; j > i; j--) {
      v[i] -= m[i][j] * v[j];
    }
    v[i] /= m[i][i];
  }
}

//------------------------------------------------------------------------------

void EmbeddedCorotSolver::applyProjector(DistSVec<double, 3>& Xdot) {
  if(meshMotionBCs) {
    meshMotionBCs->applyP(Xdot);
  }
}

//------------------------------------------------------------------------------

void EmbeddedCorotSolver::computeMeanDXForSlidingPlane(DistSVec<double, 3>& dX, double meandX[3]) {
  int size = 0;
  int **DofType = (meshMotionBCs) ? meshMotionBCs->getDofType() : 0;
  if(DofType) {
    double (*sumdX)[3] = reinterpret_cast<double (*)[3]>
                         (alloca(sizeof(double) * 3 * domain->getNumGlobSub()));
    for(int iSub = 0; iSub < domain->getNumGlobSub(); ++iSub)
      for(int j = 0; j < 3; ++j) {
        sumdX[iSub][j] = 0.0;
      }
    #pragma omp parallel for private(i,j) reduction(+: size)
    for(int iSub = 0; iSub < numLocSub; ++iSub) {
      double (*dx)[3] = dX.subData(iSub);
      int (*dofType)[3] = reinterpret_cast<int (*)[3]>(DofType[iSub]);
      bool *flag = dX.getMasterFlag(iSub);
      int locsize = 0;
      double locsumdx[3] = {0.0, 0.0, 0.0};
      for(int i = 0; i < dX.subSize(iSub); i++) {
        if(flag[i]) {
          if(dofType[i][0] == BC_MATCHEDSLIDE ||
             dofType[i][1] == BC_MATCHEDSLIDE ||
             dofType[i][2] == BC_MATCHEDSLIDE) {
            ++locsize;
            for(int j = 0; j < 3; ++j)
              if(dofType[i][j] == BC_MATCHEDSLIDE) {
                locsumdx[j] += dx[i][j];
              }
          }
        }
      }
      size += locsize;
      for(int j = 0; j < 3; ++j) {
        sumdX[domain->getSubMap()[iSub]][j] = locsumdx[j];
      }
    }
    com->globalSum(1, &size);
    com->globalSum(3 * domain->getNumGlobSub(), reinterpret_cast<double *>(sumdX));
    for(int j = 0; j < 3; ++j) {
      meandX[j] = 0.0;
    }
    for(int iSub = 0; iSub < domain->getNumGlobSub(); ++iSub)
      for(int j = 0; j < 3; ++j) {
        meandX[j] += sumdX[iSub][j];
      }
    double invTotN;
    if(size != 0) {
      invTotN = 1.0 / double(size);
    }
    else {
      invTotN = 0.;
    }
    for(int j = 0; j < 3; ++j) {
      meandX[j] *= invTotN;
    }
  }
}

//------------------------------------------------------------------------------

void EmbeddedCorotSolver::solve(Vec<Vec3D>& Xtilde, DistSVec<double, 3>& X) {
  // compute cg(n+1)
  double cg1[3];
  computeCG(Xtilde, cg1);
  switch(SymAxis) {
    case EmbeddedCorotSolver::NONE :
      break;
    case EmbeddedCorotSolver::AXIS_X :
      cg1[0] = cg0[0];
      break;
    case EmbeddedCorotSolver::AXIS_Y :
      cg1[1] = cg0[1];
      break;
    case EmbeddedCorotSolver::AXIS_Z :
      cg1[2] = cg0[2];
      break;
  }
  // solve for the incremental rotations via Newton-Raphson
  double deltaRot[3][3];
  solveDeltaRot(Xtilde, deltaRot, cg1);
  // update node
  computeNodeRot(R, X, cg0, cg1);
  cgN[0] = cg1[0];
  cgN[1] = cg1[1];
  cgN[2] = cg1[2];
  updateCM(cgN);
}

//------------------------------------------------------------------------------

void EmbeddedCorotSolver::setup(Vec<Vec3D>& Xtilde) {
  // compute cg(n+1)
  double cg1[3];
  computeCG(Xtilde, cg1);
  switch(SymAxis) {
    case EmbeddedCorotSolver::NONE :
      break;
    case EmbeddedCorotSolver::AXIS_X :
      cg1[0] = cg0[0];
      break;
    case EmbeddedCorotSolver::AXIS_Y :
      cg1[1] = cg0[1];
      break;
    case EmbeddedCorotSolver::AXIS_Z :
      cg1[2] = cg0[2];
      break;
  }
  if(corotmatrix != "") {
    readRotationMatrix();
  }
  else {
    // solve for the incremental rotations via Newton-Raphson
    double deltaRot[3][3];
    solveDeltaRot(Xtilde, deltaRot, cg1);
  }
  cgN[0] = cg1[0];
  cgN[1] = cg1[1];
  cgN[2] = cg1[2];
  updateCM(cgN);
}

//------------------------------------------------------------------------------

void EmbeddedCorotSolver::updateMomentArm(Vec3D& x00, Vec3D& x0) {
  int i;
  for(i = 0; i < 3; ++i) {
    x0[i] = x00[i] - cg0[i];
  }
  rotLocVec(R, (double *) x0);
  for(i = 0; i < 3; ++i) {
    x0[i] += cgN[i];
  }
}

//------------------------------------------------------------------------------

void EmbeddedCorotSolver::getDeltaXCM(double dXCM[3]) {
  dXCM[0] = cmN[0] - cm0[0];
  dXCM[1] = cmN[1] - cm0[1];
  dXCM[2] = cmN[2] - cm0[2];
}

//------------------------------------------------------------------------------

void EmbeddedCorotSolver::getRotationMatrix(double rotMat[3][3]) {
  rotMat[0][0] = R[0][0];
  rotMat[0][1] = R[0][1];
  rotMat[0][2] = R[0][2];
  rotMat[1][0] = R[1][0];
  rotMat[1][1] = R[1][1];
  rotMat[1][2] = R[1][2];
  rotMat[2][0] = R[2][0];
  rotMat[2][1] = R[2][1];
  rotMat[2][2] = R[2][2];
}

//------------------------------------------------------------------------------

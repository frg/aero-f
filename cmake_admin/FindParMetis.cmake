if (PARMETIS_INCLUDES AND PARMETIS_LIBRARIES)
  set(PARMETIS_FIND_QUIETLY TRUE)
endif (PARMETIS_INCLUDES AND PARMETIS_LIBRARIES)

find_path(PARMETIS_INCLUDES 
  NAMES 
  parmetis.h 
  PATHS 
  $ENV{PARMETISDIR} 
  ${INCLUDE_INSTALL_DIR} 
  PATH_SUFFIXES
  .
  parmetis
  include
)

macro(_parmetis_check_version)
  file(READ "${PARMETIS_INCLUDES}/parmetis.h" _parmetis_version_header)

  string(REGEX MATCH "define[ \t]+PARMETIS_VER_MAJOR[ \t]+([0-9]+)" _parmetis_major_version_match "${_parmetis_version_header}")
  set(PARMETIS_MAJOR_VERSION "${CMAKE_MATCH_1}")
  string(REGEX MATCH "define[ \t]+PARMETIS_VER_MINOR[ \t]+([0-9]+)" _parmetis_minor_version_match "${_parmetis_version_header}")
  set(PARMETIS_MINOR_VERSION "${CMAKE_MATCH_1}")
  string(REGEX MATCH "define[ \t]+PARMETIS_VER_SUBMINOR[ \t]+([0-9]+)" _parmetis_subminor_version_match "${_parmetis_version_header}")
  set(PARMETIS_SUBMINOR_VERSION "${CMAKE_MATCH_1}")
  if(NOT PARMETIS_MAJOR_VERSION)
    message(STATUS "Could not determine ParMetis version. Assuming version 4.0.0")
    set(PARMETIS_VERSION 4.0.0)
  else()
    set(PARMETIS_VERSION ${PARMETIS_MAJOR_VERSION}.${PARMETIS_MINOR_VERSION}.${PARMETIS_SUBMINOR_VERSION})
  endif()
  if(${PARMETIS_VERSION} VERSION_LESS ${ParMetis_FIND_VERSION})
    set(PARMETIS_VERSION_OK FALSE)
  else()
    set(PARMETIS_VERSION_OK TRUE)
  endif()

  if(NOT PARMETIS_VERSION_OK)
    message(STATUS "ParMetis version ${PARMETIS_VERSION} found in ${PARMETIS_INCLUDES}, "
                   "but at least version ${ParMetis_FIND_VERSION} is required")
  endif(NOT PARMETIS_VERSION_OK)
endmacro(_parmetis_check_version)

  if(PARMETIS_INCLUDES AND ParMetis_FIND_VERSION)
    _parmetis_check_version()
  else()
    set(PARMETIS_VERSION_OK TRUE)
  endif()


find_library(PARMETIS_LIBRARIES parmetis PATHS $ENV{PARMETISDIR} ${LIB_INSTALL_DIR} PATH_SUFFIXES lib)

include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(PARMETIS DEFAULT_MSG
                                  PARMETIS_INCLUDES PARMETIS_LIBRARIES PARMETIS_VERSION_OK)

mark_as_advanced(PARMETIS_INCLUDES PARMETIS_LIBRARIES)

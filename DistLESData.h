#ifndef _DIST_LES_DATA_H_
#define _DIST_LES_DATA_H_

#include <vector>
#include <complex>

class IoData;
class Domain;
class SubDomain;
class DistGeoState;
class MacroCellSet;
class FemEquationTermLES;
class FemEquationTermVMSLES;
class VarFcn;

template<int dim> class LESData;
template<int dim> class EmbeddedBcData;
template<int dim> class DistEmbeddedBcData;
template<int dim> class DistBcData;
template<class Scalar> class Vec;
template<class Scalar, int dim> class SVec;
template<class Scalar> class DistVec;
template<class Scalar, int dim> class DistSVec;
template<class MatrixType> class DistMat;
template<class Scalar, int neq> class MvpMat;
template<class T> class CommPattern;

//------------------------------------------------------------------------------
/*
  A base class for LES post-processing and auxillary LES calculations required
  by certain SGS LES models
*/
template<int dim>
class DistLESData {

 protected:
  int numLocSub;
  Domain *domain;
  SubDomain **subDomain;
  std::vector<LESData<dim> *> les;
  FemEquationTermLES *fet;

  void smooth(DistVec<double>&, DistVec<double>&);

 public:
  DistLESData(Domain *, FemEquationTermLES *);
  virtual ~DistLESData() {}

  LESData<dim> *operator()(int iSub) {
    return les[iSub];
  }

  virtual void resize() {}

  virtual bool isVMS() { return false; }

  virtual void compute(VarFcn *, DistBcData<dim>&, DistGeoState&, DistSVec<double, dim>&,
                       DistSVec<double, dim>&, DistEmbeddedBcData<dim> *) {}

  virtual void computeDerivative(VarFcn *, DistBcData<dim>&, DistGeoState&, DistSVec<double, dim>&,
                                 DistSVec<double, dim>&, DistSVec<double, dim>&, DistEmbeddedBcData<dim> *) {}

  virtual void outputMutOMu(DistGeoState&, DistSVec<double, dim>&, DistEmbeddedBcData<dim> *,
                            DistVec<double>&, bool = false);
};

//------------------------------------------------------------------------------

template<int dim>
class DistDynamicLESData : public DistLESData<dim> {

  double gamma;
  DistSVec<double, dim> *VCap;      // test filtered values of the primitive variables
  DistSVec<double, 16> *Mom_Test;   // test filtered values for computing cs from momentum equation
  DistSVec<double, 6> *Sij_Test;    // test filtered value of Sij
  DistVec<double> *modS_Test;       // test filtered modulus of Sij
  DistSVec<double, 8> *Eng_Test;    // test filtered values for computing pt from energy equation
  DistSVec<double, 2> *Cs, *Cstmp;  // Cs and Pt values
  DistVec<int> *Ni;                 // number of tets connected to node i

  void computeInternal(DistGeoState&, DistSVec<double, dim>&, DistEmbeddedBcData<dim> *);

  void computeTetsConnectedToNode();

  void computeCsValues(SVec<double, dim>&, SVec<double, 16>&, SVec<double, 6>&, Vec<double>&,
                       SVec<double, 8>&, SVec<double, 2>&, Vec<int>&, EmbeddedBcData<dim> *);

 public:
  DistDynamicLESData(IoData&, Domain *, FemEquationTermLES *);
  ~DistDynamicLESData();

  void resize();

  void compute(VarFcn *, DistBcData<dim>&, DistGeoState&, DistSVec<double, dim>&, DistSVec<double, dim>&,
               DistEmbeddedBcData<dim> *);

  void computeDerivative(VarFcn *, DistBcData<dim>&, DistGeoState&, DistSVec<double, dim>&,
                         DistSVec<double, dim>&, DistSVec<double, dim>&, DistEmbeddedBcData<dim> *);

  void outputMutOMu(DistGeoState&, DistSVec<double, dim>&, DistEmbeddedBcData<dim> *,
                    DistVec<double>&, bool = false);

  void outputCsValues(DistGeoState&, DistSVec<double, dim>&, DistEmbeddedBcData<dim> *,
                      DistVec<double>&);

};

//------------------------------------------------------------------------------
template<int dim>
class DistVMSLESData : public DistLESData<dim> {

  double gamma;
  int lastConfig;
  int scopeWidth;
  int scopeDepth;
  std::vector<MacroCellSet *> macroCells;
  DistSVec<double, dim> *VBar, *Rvms;  // using VMS-LES with the embedded framework is not supported
                                       // since VBar states are not adjusted for ghost points
  FemEquationTermVMSLES *vfet;  // for VMS-LES, DistLESData::fet is for NS contribution
                                // while vfet handles the VMS-LES SGS model part

  void buildMacroCells();

  void computeVBar(DistGeoState&, DistSVec<double, dim>&, bool = true, bool = true);

 public:
  DistVMSLESData(IoData&, Domain *, FemEquationTermLES *, FemEquationTermVMSLES *);
  ~DistVMSLESData();

  void resize();

  bool isVMS() { return true; }

  void compute(VarFcn *, DistBcData<dim>&, DistGeoState&, DistSVec<double, dim>&,
               DistSVec<double, dim>&, DistEmbeddedBcData<dim> *);

  void computeDerivative(VarFcn *, DistBcData<dim>&, DistGeoState&, DistSVec<double, dim>&,
                         DistSVec<double, dim>&, DistSVec<double, dim>&, DistEmbeddedBcData<dim> *);

  void outputMutOMu(DistGeoState&, DistSVec<double, dim>&, DistEmbeddedBcData<dim> *,
                    DistVec<double>&, bool = false);

  void outputReynoldsStresses(DistGeoState&, DistSVec<double, dim>&, 
                            DistSVec<double, 6>&, bool = true, bool = true);

  void outputVBar(DistGeoState&, DistSVec<double, dim>&, DistSVec<double, dim>&,
                  bool = true, bool = true);

  void outputVPrime(DistGeoState&, DistSVec<double, dim>&, DistSVec<double, dim>&,
                    bool = true, bool = true);

  void outputMacroCellID(DistVec<double>&);

};

//------------------------------------------------------------------------------

#endif

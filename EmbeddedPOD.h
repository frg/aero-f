#ifndef _EMBEDDED_POD_H_
#define _EMBEDDED_POD_H_

#include <limits>
#include <Eigen/SVD>
#include <vector>

class Domain;
class Communicator;
class IoData;
class DistGeoState;
struct Vec3D;
struct AlternatingLeastSquaresData;

template<int dim> class DistEmbeddedBcData;
template<class Scalar> class DistVec;
template<class Scalar, int dim> class DistSVec;
template<class Scalar> class Vec;
template<class Scalar> class DenseMat;
#ifndef _VECSET_TMPL_
  #define _VECSET_TMPL_
  template<class VecType, class Scalar = double> class VecSet;
#endif
template<class MatrixType> class DistMat;

typedef Eigen::Matrix<double,Eigen::Dynamic,Eigen::Dynamic,Eigen::RowMajor> MatrixType;
typedef Eigen::Matrix<double,Eigen::Dynamic,1,Eigen::ColMajor> VectorType;

//------------------------------------------------------------------------------

template<int dim>
class EmbeddedPOD {

  IoData& iod;
  Communicator *com;
  Domain *dom;
  DistEmbeddedBcData<dim> *ebcData;
  DistGeoState *geoState;

 private:
  void ALS(const VecSet<DistSVec<double, dim>>&, VecSet<DistSVec<double, dim>>&, const VecSet<DistVec<bool>>&,
           const AlternatingLeastSquaresData&, int, int);

  void updateColOfV(const VecSet<DistSVec<double, dim>>&, VecSet<DistSVec<double, dim>>&, DenseMat<double>&,
                    const VecSet<DistVec<bool>>&, int, int, int, const AlternatingLeastSquaresData&);
  void updateRowOfU(const VecSet<DistSVec<double, dim>>&, VecSet<DistSVec<double, dim>>&, DenseMat<double>&,
                    const VecSet<DistVec<bool>>&, int, int, int, int, int, const AlternatingLeastSquaresData&);

  // mask operations
  void applyMaskRow(const VecSet<DistVec<bool>>&, DenseMat<double>&, int, int, int, int);
  void applyMaskRow(const VecSet<DistVec<bool>>&, Vec<double>&, int, int, int);
  void getRow(const VecSet<DistSVec<double, dim>>&, Vec<double>&, int, int, int, int);

 public:
  EmbeddedPOD(IoData&, Domain *, DistEmbeddedBcData<dim> *, DistGeoState *);
  ~EmbeddedPOD();

  void applyMaskColumn(const DistVec<bool>&, DistMat<DenseMat<double>>&);
  void applyWeightColumn(const DistVec<bool>&, const DistVec<bool>&, double, DistSVec<double, dim>&);
  void applyWeightColumn(const DistVec<bool>&, const DistVec<bool>&, double, DistMat<DenseMat<double>>&);
  void getMask(VecSet<DistVec<bool>>&, const std::vector<VecSet<Vec<Vec3D>> *>&, std::vector<std::string>&);
  void getMask(DistVec<bool>&, const Vec<Vec3D>&);
  void compress(const VecSet<DistSVec<double, dim>>&, VecSet<DistSVec<double, dim>>&, const VecSet<DistVec<bool>>&,
                const AlternatingLeastSquaresData&);
  void project(DistSVec<double, dim>&, double *, VecSet<DistSVec<double, dim>>&, const Eigen::JacobiSVD<MatrixType>&, int,
               double = std::numeric_limits<double>::epsilon());
  void project(DistSVec<double, dim>&, double *, DistMat<DenseMat<double>>&, const Eigen::JacobiSVD<MatrixType>&, int,
               double = std::numeric_limits<double>::epsilon());
  void reinitialize(const char *, const char * = "");

};

//------------------------------------------------------------------------------

#endif

#ifndef _STIFF_MAT_H_
#define _STIFF_MAT_H_

class BCApplier;
class Domain;

template<class MatrixType> class DistMat;
template<class Scalar, int Dim> class MvpMat;
template<class Scalar, int Dim> class DistSVec;

//------------------------------------------------------------------------------

class StiffMat {

  DistMat<MvpMat<double, 3>> *A;
  BCApplier *BCs;

  int numLocSub;
  Domain *domain;

  void createStiffMat(int);

 public:
  StiffMat(Domain *, BCApplier * = nullptr);
  ~StiffMat();

  DistMat<MvpMat<double, 3>>& getStiffMatrix() {
    return *A;
  }

  BCApplier *&getBCs() {
    return BCs;
  }

  void resize(BCApplier *);

  void apply(DistSVec<double, 3>&, DistSVec<double, 3>&);

  void applyTranspose(DistSVec<double,3>&, DistSVec<double,3>&);

};

//------------------------------------------------------------------------------

#endif

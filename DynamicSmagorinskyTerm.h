#ifndef _DYNAMIC_SMAGORINSKY_TERM_H_
#define _DYNAMIC_SMAGORINSKY_TERM_H_

#include <LESTerm.h>
#include <IoData.h>

#include <limits>

//------------------------------------------------------------------------------

class DynamicSmagorinskyTerm : public LESTerm {

  static constexpr double fourth = 1.0 / 4.0;

  double csmax;
  double PrT, PrTmin, PrTmax;
  double mach;
  double csprime;

 public:
  DynamicSmagorinskyTerm(IoData&);
  ~DynamicSmagorinskyTerm() {}

  void setConstants(double cs[4], double Pr[4]) {
    csprime  = fourth * (cs[0] + cs[1] + cs[2] + cs[3]);
    prandtlT = fourth * (Pr[0] + Pr[1] + Pr[2] + Pr[3]);  // overwrite constant in base class
  }

  double computeEddyViscosity(double *[4], double, double[3][3]);

  double computeDerivativeOfEddyViscosity(double *[4], double *[4], double, double,
                                          double[3][3], double[3][3]);

  void computeJacobianEddyViscosity(double *[4], double, double[3][3],
                                    double[4][3], double[4][5]);

  double computeCsValue(double, double[4][3]);

  double computeTKE(double, double);


};

//------------------------------------------------------------------------------

inline
DynamicSmagorinskyTerm::DynamicSmagorinskyTerm(IoData& iod) : LESTerm(iod) {
  csmax = iod.eqs.tc.les.dles.clip.cs_max;
  PrT   = iod.eqs.tc.prandtlTurbulent;
  PrTmin = iod.eqs.tc.les.dles.clip.pt_min;
  PrTmax = iod.eqs.tc.les.dles.clip.pt_max;
  mach  = iod.bc.inlet.mach;
}

//------------------------------------------------------------------------------

inline
double DynamicSmagorinskyTerm::computeEddyViscosity(double *V[4], double Delta, double duidxj[3][3]) {
  // clipping for csprime = cs * Delta^2 and pt
  if(csprime > csmax * Delta * Delta) {
    csprime = csmax * Delta * Delta;
  }
  else if(csprime < 0.0) {
    csprime = 0.0;
  }
  if(mach < 0.3) {  // for subsonic flow use constant prandtl number
    prandtlT = PrT;
  }
  else {
    if(prandtlT < PrTmin) {
      prandtlT = PrTmin;
    }
    else if(prandtlT > PrTmax) {
      prandtlT = PrTmax;
    }
  }
  if(csprime == 0.0) return 0;
  double rho = fourth * (V[0][0] + V[1][0] + V[2][0] + V[3][0]);
  double S[3][3];
  S[0][0] = duidxj[0][0];
  S[1][1] = duidxj[1][1];
  S[2][2] = duidxj[2][2];
  S[0][1] = 0.5 * (duidxj[0][1] + duidxj[1][0]);
  S[0][2] = 0.5 * (duidxj[0][2] + duidxj[2][0]);
  S[1][2] = 0.5 * (duidxj[1][2] + duidxj[2][1]);
  S[1][0] = S[0][1];
  S[2][0] = S[0][2];
  S[2][1] = S[1][2];
  double S2 = (S[0][0] * S[0][0] + S[0][1] * S[0][1] + S[0][2] * S[0][2] +
               S[1][0] * S[1][0] + S[1][1] * S[1][1] + S[1][2] * S[1][2] +
               S[2][0] * S[2][0] + S[2][1] * S[2][1] + S[2][2] * S[2][2]);
  return rho * csprime * sqrt(2.0 * S2);
}

//------------------------------------------------------------------------------

inline
double DynamicSmagorinskyTerm::computeDerivativeOfEddyViscosity(double *V[4], double *dV[4], double Delta,
                                                                double dDelta, double duidxj[3][3],
                                                                double dduidxj[3][3]) {
  std::cerr << "*** Warning: DynamicSmagorinskyTerm::computeDerivativeOfEddyViscosity is not implemented\n";
  return 0;
}

//------------------------------------------------------------------------------

inline
void DynamicSmagorinskyTerm::computeJacobianEddyViscosity(double *V[4], double Delta, double duidxj[3][3],
                                                          double dp1dxj[4][3], double dmut[4][5]) {
  if(csprime == 0.0 || csprime == csmax * Delta * Delta) {
    for(int k = 0; k < 4; ++k) {
      dmut[k][0] = 0;
      dmut[k][1] = 0;
      dmut[k][2] = 0;
      dmut[k][3] = 0;
      dmut[k][4] = 0.0;
    }
  }
  else {
    double rho = fourth * (V[0][0] + V[1][0] + V[2][0] + V[3][0]);
    double S[3][3];
    S[0][0] = duidxj[0][0];
    S[1][1] = duidxj[1][1];
    S[2][2] = duidxj[2][2];
    S[0][1] = 0.5 * (duidxj[0][1] + duidxj[1][0]);
    S[0][2] = 0.5 * (duidxj[0][2] + duidxj[2][0]);
    S[1][2] = 0.5 * (duidxj[1][2] + duidxj[2][1]);
    S[1][0] = S[0][1];
    S[2][0] = S[0][2];
    S[2][1] = S[1][2];
    double S2 = (S[0][0] * S[0][0] + S[0][1] * S[0][1] + S[0][2] * S[0][2] +
                 S[1][0] * S[1][0] + S[1][1] * S[1][1] + S[1][2] * S[1][2] +
                 S[2][0] * S[2][0] + S[2][1] * S[2][1] + S[2][2] * S[2][2]);
    double s2s2 = sqrt(2.0 * S2);
    if(s2s2 < std::numeric_limits<double>::epsilon()) {
      for(int k = 0; k < 4; ++k) {
        for(int i = 0; i < 5; ++i) {
          dmut[k][i] = 0.0;
        }
      }
    }
    else {
      double toto1 = fourth * csprime * s2s2;
      double toto2 = 2 * rho * csprime / s2s2;
      // note: dmut is the derivative w.r.t the conservative state variables
      for(int k = 0; k < 4; ++k) {
        dmut[k][4] = 0.0;
        dmut[k][3] = toto2 * (dp1dxj[k][0] * S[2][0] + dp1dxj[k][1] * S[2][1] + dp1dxj[k][2] * S[2][2]) / V[k][0];
        dmut[k][2] = toto2 * (dp1dxj[k][0] * S[1][0] + dp1dxj[k][1] * S[1][1] + dp1dxj[k][2] * S[1][2]) / V[k][0];
        dmut[k][1] = toto2 * (dp1dxj[k][0] * S[0][0] + dp1dxj[k][1] * S[0][1] + dp1dxj[k][2] * S[0][2]) / V[k][0];
        dmut[k][0] = toto1 - (dmut[k][1] * V[k][1] + dmut[k][2] * V[k][2] + dmut[k][3] * V[k][3]);
      }
    }
  }
}

//------------------------------------------------------------------------------

inline
double DynamicSmagorinskyTerm::computeCsValue(double tetVol, double X[4][3]) {
  double Delta = computeFilterWidth(tetVol, X);
  return csprime / (Delta * Delta);  // assumes Cs has already been set
}

//------------------------------------------------------------------------------

inline
double DynamicSmagorinskyTerm::computeTKE(double nut, double Delta) {
  double uTKE = nut / csprime;
  double k = (1.0 / 2.0) * uTKE * uTKE;
  return k;
}


//------------------------------------------------------------------------------

#endif

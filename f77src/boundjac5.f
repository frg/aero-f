      SUBROUTINE BOUNDJAC5(type, gam, normal, sig, ua, ub, jac)
c
c     FLUX = A+(Wi)*Wi + A-(Wi)*Winf
c
      IMPLICIT NONE
c     ua is the state in primitive,
c     ub is the facet external boundary value passed in conservative variables
      REAL*8 ua(*), ub(*), vnx, vny, vnz
      REAL*8 normal(3), fgp1, H(5,5), jac(*), jac1
c     Sig is the dot product of the normal with the velocity of the facet
c     times the area of the facet
      REAL*8 sig
c     Plus and minus fluxes
      REAL*8 cap
      REAL*8 u, v, w, ro, usro, vit, pm, c2, c, usc, dcdr, dcdp, dfgp
      REAL*8 v1, v4, v5, vp1, vp4, vp5, dvp1, dvp4, dvp5
      REAL*8 vpp, vpm, dvpp, dvpm, dvppdr, dvpmdr, dvppdp, dvpmdp
      REAL*8 gam, gam1, rhoE, updir
      REAL*8 capd1, capd2, capd3, unx, xn1, xn2, cc, ff, gg, phiro
      REAL*8 dffdu, dffdv, dffdw, dffdr, dffdp
      REAL*8 dggdu, dggdv, dggdw, dggdr, dggdp
      INTEGER type, dim, i, k
c
      dim = 5 + type
c
      gam1 = gam - 1.d0
c
      vnx = normal(1)
      vny = normal(2)
      vnz = normal(3)
c
      cap        = SQRT(vnx*vnx + vny*vny + vnz*vnz)
      capd1      = vnx/cap
      capd2      = vny/cap
      capd3      = vnz/cap
c
      ro         = ua(1)
      usro       = 1.0d0/ro
      u          = ua(2)
      v          = ua(3)
      w          = ua(4)
      vit        = u*u + v*v + w*w
      pm         = ua(5)
      rhoE       = pm/gam1 + 0.5d0*ro*vit
c
      c2         = gam*pm*usro
      c          = SQRT(c2)
      usc        = 1.0d0/c
      dcdr       = -0.5d0*c/ro
      dcdp       = 0.5d0*gam/(ro*c)
c
      unx        = capd1*u + capd2*v + capd3*w
c
      v1         = cap*unx - sig
      v4         = cap*(unx + c) - sig
      v5         = cap*(unx - c) - sig
c
      if (v1.le.0.0d0) then
        vp1  = 0.0d0
        dvp1 = 0.0d0
      else
        vp1 = v1
        dvp1 = cap
      endif
      if (v4.le.0.0d0) then
        vp4  = 0.0d0
        dvp4 = 0.0d0
      else
        vp4  = v4
        dvp4 = cap
      endif
      if (v5.le.0.0d0) then
        vp5  = 0.0d0
        dvp5 = 0.0d0
      else
        vp5  = v5
        dvp5 = cap
      endif
c
      vpp        = vp4 + vp5
      vpm        = vp4 - vp5
      dvpp       = (dvp4 + dvp5)
      dvppdr     = (dvp4 - dvp5)*dcdr
      dvppdp     = (dvp4 - dvp5)*dcdp
      dvpm       = (dvp4 - dvp5)
      dvpmdr     = (dvp4 + dvp5)*dcdr
      dvpmdp     = (dvp4 + dvp5)*dcdp
c
c     fgp(1)       = (gam1*ro*vp1 + 0.5*ro*vpp)/gam
      fgp1       = (gam1*ro*vp1 + 0.5*ro*vpp)/gam
      dfgp       = (gam1*ro*dvp1 + 0.5*ro*dvpp)/gam
      H(1,1)     = (gam1*vp1 + 0.5*(vpp + ro*dvppdr))/gam
      H(2,1)     = dfgp*capd1
      H(3,1)     = dfgp*capd2
      H(4,1)     = dfgp*capd3
      H(5,1)     = 0.5*ro*dvppdp/gam
c
c     fgp(2)     = fgp(1)*u + 0.5d0*pm*usc*capd1*vpm
      H(1,2)     = H(1,1)*u + 0.5d0*pm*capd1*(usc*dvpmdr - dcdr/c2*vpm)
      H(2,2)     = H(2,1)*u + fgp1 + 0.5d0*pm*usc*capd1*dvpm*capd1
      H(3,2)     = H(3,1)*u + 0.5d0*pm*usc*capd1*dvpm*capd2
      H(4,2)     = H(4,1)*u + 0.5d0*pm*usc*capd1*dvpm*capd3
      H(5,2)     = H(5,1)*u + 0.5d0*capd1*(
     &             usc*vpm - pm*dcdp/c2*vpm + pm*usc*dvpmdp)
c
c     fgp(3)     = fgp(1)*v + 0.5d0*pm*usc*capd2*vpm
      H(1,3)     = H(1,1)*v + 0.5d0*pm*capd2*(usc*dvpmdr - dcdr/c2*vpm)
      H(2,3)     = H(2,1)*v + 0.5d0*pm*usc*capd2*dvpm*capd1
      H(3,3)     = H(3,1)*v + fgp1 + 0.5d0*pm*usc*capd2*dvpm*capd2
      H(4,3)     = H(4,1)*v + 0.5d0*pm*usc*capd2*dvpm*capd3
      H(5,3)     = H(5,1)*v + 0.5d0*capd2*(
     &             usc*vpm - pm*dcdp/c2*vpm + pm*usc*dvpmdp)
c
c     fgp(4)     = fgp(1)*w + 0.5d0*pm*usc*capd3*vpm
      H(1,4)     = H(1,1)*w + 0.5d0*pm*capd3*(usc*dvpmdr - dcdr/c2*vpm)
      H(2,4)     = H(2,1)*w + 0.5d0*pm*usc*capd3*dvpm*capd1
      H(3,4)     = H(3,1)*w + 0.5d0*pm*usc*capd3*dvpm*capd2
      H(4,4)     = H(4,1)*w + fgp1 + 0.5d0*pm*usc*capd3*dvpm*capd3
      H(5,4)     = H(5,1)*w + 0.5d0*capd3*(
     &             usc*vpm - pm*dcdp/c2*vpm + pm*usc*dvpmdp)
c
c      fgp(5)     = 0.5d0*((gam1/gam)*ro*vit*(vp1 - 0.5d0*vpp) +
c     &             rhoE*vpp + ro*unx*c*vpm/gam)
      dfgp       = dvp1 - 0.5d0*dvpm
      H(1,5)     = 0.5d0*((gam1/gam)*vit*
     &             (vp1 - 0.5d0*vpp - 0.5d0*ro*dvppdr) +
     &             0.5d0*vit*vpp + rhoE*dvppdr +
     &             unx/gam*(c*vpm + ro*dcdr*vpm + ro*c*dvpmdr))
      H(2,5)     = 0.5d0*((gam1/gam)*ro*(2.0d0*u*(vp1 - 0.5d0*vpp) +
     &             vit*dfgp*capd1) +
     &             ro*u*vpp + rhoE*(dvp4 + dvp5)*capd1 +
     &             ro*c/gam*(capd1*vpm + unx*dvpm*capd1))
      H(3,5)     = 0.5d0*((gam1/gam)*ro*(2.0d0*v*(vp1 - 0.5d0*vpp) +
     &             vit*dfgp*capd2) +
     &             ro*v*vpp + rhoE*(dvp4 + dvp5)*capd2 +
     &             ro*c/gam*(capd2*vpm + unx*dvpm*capd2))
      H(4,5)     = 0.5d0*((gam1/gam)*ro*(2.0d0*w*(vp1 - 0.5d0*vpp) +
     &             vit*dfgp*capd3) +
     &             ro*w*vpp + rhoE*(dvp4 + dvp5)*capd3 +
     &             ro*c/gam*(capd3*vpm + unx*dvpm*capd3))
      H(5,5)     = 0.5d0*(-0.5d0*(gam1/gam)*ro*vit*dvppdp +
     &             vpp/gam1 + rhoE*dvppdp +
     &             ro*unx/gam*(dcdp*vpm + c*dvpmdp))
c
      if (v1.ge.0.0d0) then
        vp1  = 0.0d0
        dvp1 = 0.0d0
      else
        vp1 = v1
        dvp1 = cap
      endif
      if (v4.ge.0.0d0) then
        vp4  = 0.0d0
        dvp4 = 0.0d0
      else
        vp4  = v4
        dvp4 = cap
      endif
      if (v5.ge.0.0d0) then
        vp5  = 0.0d0
        dvp5 = 0.0d0
      else
        vp5  = v5
        dvp5 = cap
      endif
c
      vpp        = vp1 - 0.5d0*(vp4 + vp5)
      vpm        = 0.5d0*(vp5 - vp4)
      dvpp       = dvp1 - 0.5d0*(dvp4 + dvp5)
      dvppdr     = 0.5d0*(dvp5 - dvp4)*dcdr
      dvppdp     = 0.5d0*(dvp5 - dvp4)*dcdp
      dvpm       = 0.5e0*(dvp5 - dvp4)
      dvpmdr     = -0.5d0*(dvp5 + dvp4)*dcdr
      dvpmdp     = -0.5d0*(dvp5 + dvp4)*dcdp
c
      xn1        = gam1*usc*(-0.5d0*vit*ub(1) +
     &             u*ub(2) + v*ub(3) + w*ub(4) - ub(5))
      xn2        = unx*ub(1) -
     &             capd1*ub(2) - capd2*ub(3) - capd3*ub(4)
c
      cc         = 0.5d0*vit + c2/gam1
      ff         = usc*(vpp*xn1 + vpm*xn2)
      gg         = vpm*xn1 + vpp*xn2
      dffdu      = usc*(dvpp*capd1*xn1 +
     &             vpp*gam1*usc*(-ub(1)*u + ub(2)) +
     &             dvpm*capd1*xn2 + vpm*capd1*ub(1))
      dffdv      = usc*(dvpp*capd2*xn1 +
     &             vpp*gam1*usc*(-ub(1)*v + ub(3)) +
     &             dvpm*capd2*xn2 + vpm*capd2*ub(1))
      dffdw      = usc*(dvpp*capd3*xn1 +
     &             vpp*gam1*usc*(-ub(1)*w + ub(4)) +
     &             dvpm*capd3*xn2 + vpm*capd3*ub(1))
      dffdr      = -(vpp*xn1 + vpm*xn2)*dcdr/c2 +
     &             usc*(-vpp*xn1*dcdr/c + dvppdr*xn1 + dvpmdr*xn2)
      dffdp      = -(vpp*xn1 + vpm*xn2)*dcdp/c2 +
     &             usc*(-vpp*xn1*dcdp/c + dvppdp*xn1 + dvpmdp*xn2)
      dggdu      = dvpm*capd1*xn1 + vpm*gam1*usc*(-ub(1)*u + ub(2)) +
     &             dvpp*capd1*xn2 + vpp*capd1*ub(1)
      dggdv      = dvpm*capd2*xn1 + vpm*gam1*usc*(-ub(1)*v + ub(3)) +
     &             dvpp*capd2*xn2 + vpp*capd2*ub(1)
      dggdw      = dvpm*capd3*xn1 + vpm*gam1*usc*(-ub(1)*w + ub(4)) +
     &             dvpp*capd3*xn2 + vpp*capd3*ub(1)
      dggdr      = -vpm*xn1*dcdr/c + dvpmdr*xn1 + dvppdr*xn2
      dggdp      = -vpm*xn1*dcdp/c + dvpmdp*xn1 + dvppdp*xn2
c
c     fgm(1)     = vp1*ub(1) + ff
      H(1,1)     = H(1,1) + dffdr
      H(2,1)     = H(2,1) + dvp1*capd1*ub(1) + dffdu
      H(3,1)     = H(3,1) + dvp1*capd2*ub(1) + dffdv
      H(4,1)     = H(4,1) + dvp1*capd3*ub(1) + dffdw
      H(5,1)     = H(5,1) + dffdp
c
c     fgm(2)     = vp1*ub(2) + u*ff  + capd1*gg
      H(1,2)     = H(1,2) + u*dffdr + capd1*dggdr
      H(2,2)     = H(2,2) + dvp1*capd1*ub(2) + u*dffdu + ff +
     &             capd1*dggdu
      H(3,2)     = H(3,2) + dvp1*capd2*ub(2) + u*dffdv + capd1*dggdv
      H(4,2)     = H(4,2) + dvp1*capd3*ub(2) + u*dffdw + capd1*dggdw
      H(5,2)     = H(5,2) + u*dffdp + capd1*dggdp
c
c     fgm(3)     = vp1*ub(3) + v*ff  + capd2*gg
      H(1,3)     = H(1,3) + v*dffdr + capd2*dggdr
      H(2,3)     = H(2,3) + dvp1*capd1*ub(3) + v*dffdu + capd2*dggdu
      H(3,3)     = H(3,3) + dvp1*capd2*ub(3) + v*dffdv + ff +
     &             capd2*dggdv
      H(4,3)     = H(4,3) + dvp1*capd3*ub(3) + v*dffdw + capd2*dggdw
      H(5,3)     = H(5,3) + v*dffdp + capd2*dggdp
c
c     fgm(4)     = vp1*ub(4) + w*ff  + capd3*gg
      H(1,4)     = H(1,4) + w*dffdr + capd3*dggdr
      H(2,4)     = H(2,4) + dvp1*capd1*ub(4) + w*dffdu + capd3*dggdu
      H(3,4)     = H(3,4) + dvp1*capd2*ub(4) + w*dffdv + capd3*dggdv
      H(4,4)     = H(4,4) + dvp1*capd3*ub(4) + w*dffdw + ff +
     &             capd3*dggdw
      H(5,4)     = H(5,4) + w*dffdp + capd3*dggdp
c
c     fgm(5)     = vp1*ub(5) + cc*ff + unx*gg
      H(1,5)     = H(1,5) + cc*dffdr + 2.0d0*c*dcdr/gam1*ff + unx*dggdr
      H(2,5)     = H(2,5) + dvp1*capd1*ub(5) + cc*dffdu + u*ff +
     &             capd1*gg + unx*dggdu
      H(3,5)     = H(3,5) + dvp1*capd2*ub(5) + cc*dffdv + v*ff +
     &             capd2*gg + unx*dggdv
      H(4,5)     = H(4,5) + dvp1*capd3*ub(5) + cc*dffdw + w*ff +
     &             capd3*gg + unx*dggdw
      H(5,5)     = H(5,5) + cc*dffdp + 2.0d0*c*dcdp/gam1*ff + unx*dggdp
c
c Copy to output flux jacobian
c
      dim = 5 + type
      do i = 1,5
        do k = 1,5
          jac(dim*(i-1) + k) = H(k,i)
        enddo
        do k = 6,dim
          jac(dim*(i-1) + k) = 0.d0
        enddo
      enddo
      if (type.ne.0) then
        phiro = fgp1 + vp1*ub(1) + ff
        updir = 0.5d0 + dsign(0.5d0, phiro)
        jac1  = phiro*updir
        if (type.eq.1) then
          do k = 1,5
            jac(dim*5 + k) = jac(k)*updir*ua(6)
          enddo
          jac(dim*5 + 6) = jac1
        else if (type.eq.2) then
          do k = 1,5
            jac(dim*5 + k) = jac(k)*updir*ua(6)
          enddo
          jac(dim*5 + 6) = jac1
          jac(dim*5 + 7) = 0.0
          do k = 1,5
            jac(dim*6 + k) = jac(k)*updir*ua(7)
          enddo
          jac(dim*6 + 6) = 0.0
          jac(dim*6 + 7) = jac1
        endif
      endif
c
      END

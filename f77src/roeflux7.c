#include <math.h>

void roeflux7_(int *type, const double *gam, const double *pstiff,
               const double *enormal, const double *evitno, double *ug,
               double *ud, double *phi)
{
/* ----------------------------------------------------------------------- */
/* This routine computes the Flux of Roe taken at the vectors Ug, Ud */
/* normal is the normal of the boundary concerned by the flux. */
/* phi stores the resulting flux. */
/* gam is the ratio of cp/cv */
/* ----------------------------------------------------------------------- */

    /* Initialisation */

    double gam1 = *gam - 1.;
    double invgam1 = 1. / gam1;

    double rnorm = sqrt(enormal[0] * enormal[0] + enormal[1] * enormal[1] + enormal[2] * enormal[2]);
    double invnorm = 1. / rnorm;

    double normal[3] = { enormal[0] * invnorm, enormal[1] * invnorm, enormal[2] * invnorm };
    double vitno = *evitno * invnorm;
    double vitg2 = (ug[1] * ug[1] + ug[2] * ug[2] + ug[3] * ug[3]) * .5;
    double vitd2 = (ud[1] * ud[1] + ud[2] * ud[2] + ud[3] * ud[3]) * .5;

    /* Computation of the Roe-averaged state */

    double squsr1 = sqrt(ug[0]);
    double squsr2 = sqrt(ud[0]);

    double ener1 = (ug[4] + *gam * *pstiff) * invgam1 + ug[0] * vitg2;
    double ener2 = (ud[4] + *gam * *pstiff) * invgam1 + ud[0] * vitd2;

    double usro = 1. / (squsr1 + squsr2);

    double uar2 = (squsr1 * ug[1] + squsr2 * ud[1]) * usro;
    double uar3 = (squsr1 * ug[2] + squsr2 * ud[2]) * usro;
    double uar4 = (squsr1 * ug[3] + squsr2 * ud[3]) * usro;
    double uar5 = ((ener1 + ug[4]) / squsr1 + (ener2 + ud[4]) / squsr2) * usro;

    double qir = (uar2 * uar2 + uar3 * uar3 + uar4 * uar4) * .5;
    double cr2 = gam1 * (uar5 - qir);
    double cr = sqrt(cr2);

    double vdotnt = normal[0] * uar2 + normal[1] * uar3 + normal[2] * uar4;

    if(vdotnt - vitno > 0.) {
        /* Computation of the centered terms */
        double vdotn = ug[1] * normal[0] + ug[2] * normal[1] + ug[3] * normal[2];
        double h = *gam * (ug[4] + *pstiff) + gam1 * ug[0] * vitg2;
        h /= gam1 * ug[0];
        phi[0] = ug[0] * (vdotn - vitno);
        phi[1] = phi[0] * ug[1] + ug[4] * normal[0];
        phi[2] = phi[0] * ug[2] + ug[4] * normal[1];
        phi[3] = phi[0] * ug[3] + ug[4] * normal[2];
        phi[4] = phi[0] * h + ug[4] * vitno;

        /* Computation of the dissipation term */
        double vp = vdotnt - cr - vitno;
        if(vp < 0.) {
            double dif1 = -ug[0] + ud[0];
            double dif2 = -ug[0] * ug[1] + ud[0] * ud[1];
            double dif3 = -ug[0] * ug[2] + ud[0] * ud[2];
            double dif4 = -ug[0] * ug[3] + ud[0] * ud[3];
            double dif5 = -ener1 + ener2;

            double flur = -vp * (cr * (vdotnt * dif1 - normal[0] * dif2 -
                          normal[1] * dif3 - normal[2] * dif4) + gam1 *
                          (qir * dif1 - uar2 * dif2 - uar3 * dif3 - uar4 * dif4 + dif5));

            cr2 = .5 / cr2;
            double crflur = cr * -flur;

            phi[0] -= flur * cr2;
            phi[1] -= (normal[0] * crflur + uar2 * flur) * cr2;
            phi[2] -= (normal[1] * crflur + uar3 * flur) * cr2;
            phi[3] -= (normal[2] * crflur + uar4 * flur) * cr2;
            phi[4] -= (vdotnt * crflur + uar5 * flur) * cr2;
        }
    }
    else {
        /* Computation of the centered terms */
        double vdotn = ud[1] * normal[0] + ud[2] * normal[1] + ud[3] * normal[2];
        double h = *gam * (ud[4] + *pstiff) + gam1 * ud[0] * vitd2;
        h /= gam1 * ud[0];
        phi[0] = ud[0] * (vdotn - vitno);
        phi[1] = phi[0] * ud[1] + ud[4] * normal[0];
        phi[2] = phi[0] * ud[2] + ud[4] * normal[1];
        phi[3] = phi[0] * ud[3] + ud[4] * normal[2];
        phi[4] = phi[0] * h + ud[4] * vitno;

        /* Computation of the dissipation term */
        double vp = vdotnt + cr - vitno;
        if(vp > 0.) {
            double dif1 = -ug[0] + ud[0];
            double dif2 = -ug[0] * ug[1] + ud[0] * ud[1];
            double dif3 = -ug[0] * ug[2] + ud[0] * ud[2];
            double dif4 = -ug[0] * ug[3] + ud[0] * ud[3];
            double dif5 = -ener1 + ener2;

            double flur = vp * (-cr * (vdotnt * dif1 - normal[0] * dif2 -
                          normal[1] * dif3 - normal[2] * dif4) + gam1 *
                          (qir * dif1 - uar2 * dif2 - uar3 * dif3 - uar4 * dif4 + dif5));

            cr2 = .5 / cr2;
            double crflur = cr * flur;

            phi[0] -= flur * cr2;
            phi[1] -= (normal[0] * crflur + uar2 * flur) * cr2;
            phi[2] -= (normal[1] * crflur + uar3 * flur) * cr2;
            phi[3] -= (normal[2] * crflur + uar4 * flur) * cr2;
            phi[4] -= (vdotnt * crflur + uar5 * flur) * cr2;
        }
    }

    /* Final phi */
    phi[0] *= rnorm;
    phi[1] *= rnorm;
    phi[2] *= rnorm;
    phi[3] *= rnorm;
    phi[4] *= rnorm;

    /* For one and two equation turbulence models */
    if(*type == 1) {
        if(phi[0] >= 0) phi[5] = phi[0] * ug[5];
        else            phi[5] = phi[0] * ud[5];
    }
    else if(*type == 2) {
        if(phi[0] >= 0) { phi[5] = phi[0] * ug[5]; phi[6] = phi[0] * ug[6]; }
        else            { phi[5] = phi[0] * ud[5]; phi[6] = phi[0] * ud[6]; }
    }
}


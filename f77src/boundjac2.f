      SUBROUTINE BOUNDJAC2(type, gam, normal, sig, ua, ub, jac)
c
c     FLUX = A+(Wi)*Wi + A-(Wi)*Winf
c
      IMPLICIT NONE
c     ua is the state in primitive,
c     ub is the facet external boundary value passed in conservative variables
      REAL*8 ua(*), ub(*), vnx, vny, vnz
      REAL*8 normal(3), jac(*), jac1
c     Sig is the dot product of the normal with the velocity of the facet
c     times the area of the facet
      REAL*8 sig
c     Plus and minus fluxes
      REAL*8 fgp1, fgm1
      REAL*8 cap
      REAL*8 u, v, w, ro, usro, vit, pm, c2, c, usc
      REAL*8 v1, v4, v5, vp1, vp4, vp5, vpp, vpm
      REAL*8 gam, gam1, updir
      REAL*8 capd1, capd2, capd3, unx, xn1, xn2, ff, phiro
      INTEGER type
c
      gam1 = gam - 1.d0
c
      vnx = normal(1)
      vny = normal(2)
      vnz = normal(3)
c
      cap        = SQRT(vnx*vnx+ vny*vny + vnz*vnz)
      capd1      = vnx/cap
      capd2      = vny/cap
      capd3      = vnz/cap
c
      ro         = ua(1)
      usro       = 1.0d0/ro
      u          = ua(2)
      v          = ua(3)
      w          = ua(4)
      vit        = u*u + v*v + w*w
      pm         = ua(5)
c
      c2         = gam*pm*usro
      c          = SQRT(c2)
      usc        = 1.0d0/c
c
      unx        = capd1*u + capd2*v + capd3*w
c
      v1         = cap*unx - sig
      v4         = cap*(unx + c) - sig
      v5         = cap*(unx - c) - sig
c
      vp1        = MAX(v1, 0.0d0)
      vp4        = MAX(v4, 0.0d0)
      vp5        = MAX(v5, 0.0d0)
c
      vpp        = vp4 + vp5
c
      fgp1       = (gam1*ro*vp1 + 0.5*ro*vpp)/gam
c
      vp1        = MIN(v1, 0.0d0)
      vp4        = MIN(v4, 0.0d0)
      vp5        = MIN(v5, 0.0d0)
c
      vpp        = vp1 - 0.5*(vp4 + vp5)
      vpm        = 0.5d0*(vp5 - vp4)
c
      xn1        = gam1*usc*(-0.5d0*vit*ub(1) +
     &             u*ub(2) + v*ub(3) + w*ub(4) - ub(5))
      xn2        = unx*ub(1) -
     &             capd1*ub(2) - capd2*ub(3) - capd3*ub(4)
c
      ff         = usc*(vpp*xn1 + vpm*xn2)
c
      fgm1       = vp1*ub(1) + ff
c
      phiro = fgp1 + fgm1
c
      updir = 0.5d0 + dsign(0.5d0, phiro)
      jac1  = phiro*updir*usro
c
      if (type.eq.1) then
         jac(1) = jac1
      else if (type.eq.2) then
         jac(1) = jac1
         jac(2) = 0.0
         jac(3) = 0.0
         jac(4) = jac(1)
      endif
c
      END

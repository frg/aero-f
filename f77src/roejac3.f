      SUBROUTINE ROEFLUX2(gamma,gam,pstiff,enormal,evitno,
     &                    Ug,Ud,phi1)
c-----------------------------------------------------------------------
c This routine computes the Flux of Roe taken at the vectors Ug, Ud
c normal is the normal of the boundary concerned by the flux.
c phi stores the resulting flux.
c-----------------------------------------------------------------------
      IMPLICIT NONE
      REAL*8 Ug(*), Ud(*), normal(3), enormal(3), evitno, phi1
      REAL*8 energ, enerd
      REAL*8 H, vitno, updir, gamma
      REAL*8 VdotN, rnorm, invnorm
      REAL*8 flur1, flur2, flur3, flur4, flur5
      REAL*8 dif1, dif2, dif3, dif4, dif5
      REAL*8 cr, cr2, qir
      REAL*8 vp1, vp4, vp5
      REAL*8 ener1, ener2
      REAL*8 uar1, uar2, uar3, uar4, uar5
      REAL*8 usro, squsr1, squsr2
      REAL*8 tet1, tet2, tet3
      REAL*8 gam, gam1, vitg2, vitd2, pstiff
      INTEGER type
c
c Initialisation
c
      gam1 = gam - 1.d0
c
      rnorm = DSQRT(enormal(1)*enormal(1) + enormal(2)*enormal(2) +
     &              enormal(3)*enormal(3))

      invnorm = 1.0d0 / rnorm
c
      normal(1) = enormal(1) * invnorm
      normal(2) = enormal(2) * invnorm
      normal(3) = enormal(3) * invnorm

      vitno = evitno * invnorm
c
c Computation of the centered terms
c
      VdotN = Ug(2)*normal(1) + Ug(3)*normal(2) + Ug(4)*normal(3)
      vitg2 = Ug(2)*Ug(2) + Ug(3)*Ug(3) + Ug(4)*Ug(4)
      phi1 = Ug(1)*(VdotN - vitno)
c
      VdotN = Ud(2)*normal(1)+Ud(3)*normal(2)+Ud(4)*normal(3)
      VdotN = VdotN - vitno
      vitd2 = Ud(2)*Ud(2) + Ud(3)*Ud(3) + Ud(4)*Ud(4)
      phi1 = phi1 + Ud(1)*VdotN
c
c Computation of the Roe-averaged state
c

      squsr1                    = DSQRT(Ug(1))
      squsr2                    = DSQRT(Ud(1))
c
      ener1                     = (Ug(5)+gam*pstiff)/gam1 +
     &                            0.5d0*Ug(1)*vitg2
c
      ener2                     = (Ud(5)+gam*pstiff)/gam1 +
     &                            0.5d0*Ud(1)*vitd2
c
      usro                      = 1.d0/(squsr1 + squsr2)
c
      uar1                      = (squsr1*Ug(1) +
     &                                  squsr2*Ud(1))*usro
c
      uar2                      = (squsr1*Ug(2) +
     &                                squsr2*Ud(2))*usro
c
      uar3                      = (squsr1*Ug(3) +
     &                                squsr2*Ud(3))*usro
c
      uar4                      = (squsr1*Ug(4) +
     &                                squsr2*Ud(4))*usro
c
      uar5                      = ((ener1 + Ug(5))/
     &                                squsr1 +
     &                                (ener2 + Ud(5))/
     &                                squsr2)*usro


c
c Computation of the dissipation term
c
      VdotN                     = normal(1)*uar2 + normal(2)*uar3 +
     &                               normal(3)*uar4
c
      qir                       = 0.5d0*(uar2*uar2 + uar3*uar3 +
     &                                    uar4*uar4)
c
      tet1                      = normal(3)*uar3 - normal(2)*uar4
      tet2                      = normal(1)*uar4 - normal(3)*uar2
      tet3                      = normal(2)*uar2 - normal(1)*uar3
c
      cr2                       = gam1*(uar5 - qir)
      cr                        = DSQRT(cr2)
      cr2                       = 1.d0/cr2
c
      energ = (Ug(5)+gam*Pstiff)/gam1 +
     &     0.5d0*Ug(1)*(Ug(2)*Ug(2) + Ug(3)*Ug(3) + Ug(4)*Ug(4))
      enerd = (Ud(5)+gam*Pstiff)/gam1 +
     &     0.5d0*Ud(1)*(Ud(2)*Ud(2) + Ud(3)*Ud(3) + Ud(4)*Ud(4))

      dif1                     = - Ug(1) + Ud(1)
      dif2                     = - Ug(1)*Ug(2) + Ud(1)*Ud(2)
      dif3                     = - Ug(1)*Ug(3) + Ud(1)*Ud(3)
      dif4                     = - Ug(1)*Ug(4) + Ud(1)*Ud(4)
      dif5                     = - energ + enerd
c
      vp1                       = gamma * DABS(VdotN - vitno)
      vp4                       = gamma * DABS((VdotN + cr) - vitno)
      vp5                       = gamma * DABS((VdotN - cr) - vitno)
c
      flur1                     = vp1*
     &              ((normal(1)*(1.0 - gam1*qir*cr2) - tet1)*dif1 +
     &               (normal(1)*gam1*uar2*cr2)*dif2  +
     &               (normal(3)  + (normal(1)*gam1*uar3*cr2))*dif3   +
     &               (-normal(2) + (normal(1)*gam1*uar4*cr2))*dif4   -
     &               (normal(1)*gam1*cr2)*dif5)
c
      flur2                     = vp1*
     &              ((normal(2)*(1.0 - gam1*qir*cr2) - tet2)*dif1 +
     &               (-normal(3) + (normal(2)*gam1*uar2*cr2))*dif2   +
     &               (normal(2)*gam1*uar3*cr2)*dif3  +
     &               (normal(1)  + (normal(2)*gam1*uar4*cr2))*dif4   -
     &               (normal(2)*gam1*cr2)*dif5)
c
      flur3                     = vp1*
     &              ((normal(3)*(1.0 - gam1*qir*cr2) - tet3)*dif1 +
     &               (normal(2)  + (normal(3)*gam1*uar2*cr2))*dif2   +
     &               (-normal(1) + (normal(3)*gam1*uar3*cr2))*dif3   +
     &               (normal(3)*gam1*uar4*cr2)*dif4  -
     &               (normal(3)*gam1*cr2)*dif5)
c
      flur4                     = vp4*
     &              ((-cr*VdotN   + gam1*qir)*dif1  +
     &               ( cr*normal(1) - gam1*uar2)*dif2 +
     &               ( cr*normal(2) - gam1*uar3)*dif3 +
     &               ( cr*normal(3) - gam1*uar4)*dif4 +
     &               gam1*dif5)
c
      flur5                     = vp5*
     &              (( cr*VdotN  + gam1* qir)*dif1 +
     &               (-cr*normal(1) - gam1*uar2)*dif2 +
     &               (-cr*normal(2) - gam1*uar3)*dif3 +
     &               (-cr*normal(3) - gam1*uar4)*dif4 +
     &               gam1*dif5)
c
c Final phi including the numerical viscosity parameter
c
      phi1 =  phi1 - (normal(1)*flur1 +
     &     normal(2)*flur2 + normal(3)*flur3 +
     &     0.5*(flur4 + flur5)*cr2)

c
      phi1 = phi1*0.5d0*rnorm
c
      END

      SUBROUTINE ROEJAC3(type,gamma,gam,pstiff,enormal,evitno,
     &                   Ug,Ud,jacg,jacd)
c
      IMPLICIT NONE
      REAL*8 gam, pstiff, enormal(3), evitno, Ug(*), Ud(*), gamma
      REAL*8 jacg(*), jacd(*), phi1, updir, jacleft, jacright
      INTEGER type
c
      CALL ROEFLUX2(gamma,gam,pstiff,enormal,evitno,
     &              Ug,Ud,phi1)
c
      updir = 0.5d0 + dsign(0.5d0, phi1)
c
      jacleft = phi1 * updir / Ug(1)
      jacright = phi1 * (1.0d0 - updir) / Ud(1)
c
      if (type.eq.1) then
         jacg(1) = jacleft
         jacd(1) = jacright
      else if (type.eq.2) then
         jacg(1) = jacleft
         jacg(2) = 0.0
         jacg(3) = 0.0
         jacg(4) = jacg(1)
c
         jacd(1) = jacright
         jacd(2) = 0.0
         jacd(3) = 0.0
         jacd(4) = jacd(1)
      endif
c
      END

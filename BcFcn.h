#ifndef _BC_FCN_H_
#define _BC_FCN_H_

#include <complex>

class BcsSymmetryData;
class BcsWallData;
class IoData;

//------------------------------------------------------------------------------

class BcFcn {

 public:

  BcFcn() {}
  virtual ~BcFcn() {}

  virtual void applyToSolutionVector(int, double *, double *, bool, double (*)[3]);
  virtual void applyToResidualTerm(int, double *, double *, double *, bool, double (*)[3]);
  virtual void applyToDerivativeOfResidualTerm(int, double *, double *, double *, double *, double *, bool, double (*)[3]);
  virtual void applyToTransposeDerivativeOfResidualTerm(int, double *, bool, double (*)[3]);

  virtual void applyToTurbSolutionVector(int, double *, double *);
  virtual void applyToTurbResidualTerm(int, double *, double *, double *);
  virtual void applyToDerivativeOfTurbResidualTerm(int, double *, double *, double *, double *, double *);

  virtual void applyToDiagonalTerm(int, double *, double *, float *, bool, double (*)[3]);
  virtual void applyToDiagonalTerm(int, double *, double *, double *, bool, double (*)[3]);
  virtual void applyToDiagonalTerm(int, double *, double *, std::complex<double> *, bool, double (*)[3]);
  virtual void applyToOffDiagonalTerm(int, float *, bool, double (*)[3]);
  virtual void applyToOffDiagonalTerm(int, double *, bool, double (*)[3]);
  virtual void applyToOffDiagonalTerm(int, std::complex<double> *, bool, double (*)[3]);

  virtual void applyToTurbDiagonalTerm(int, double *, double *, float *);
  virtual void applyToTurbDiagonalTerm(int, double *, double *, double *);
  virtual void applyToTurbDiagonalTerm(int, double *, double *, std::complex<double> *);
  virtual void applyToTurbOffDiagonalTerm(int, float *);
  virtual void applyToTurbOffDiagonalTerm(int, double *);
  virtual void applyToTurbOffDiagonalTerm(int, std::complex<double> *);

  virtual void applyToWallDiagonalTerm(int, double *, double *, double *, float *);
  virtual void applyToWallDiagonalTerm(int, double *, double *, double *, double *);
  virtual void applyToWallDiagonalTerm(int, double *, double *, double *, std::complex<double> *);
};

//------------------------------------------------------------------------------

class BcFcnEuler : public BcFcn {
  BcsSymmetryData& symmetry;

 public:
  BcFcnEuler(IoData&);
  ~BcFcnEuler() {}

  static void template_applyToSolutionVectorTerm(int, double *, double *, double (*)[3]);

  static void template_applyToResidualTerm(int, double *, double *, double *, double (*)[3]);

  template<class Scalar, int neq>
  static void template_applyToDiagonalTerm(int, double *, double *, Scalar *, double (*)[3]);

  template<class Scalar, int neq>
  static void template_applyToOffDiagonalTerm(int, Scalar *, double (*)[3]);

  void applyToSolutionVector(int, double *, double *, bool, double (*)[3]);
  void applyToResidualTerm(int, double *, double *, double *, bool, double (*)[3]);

  void applyToDiagonalTerm(int, double *, double *, float *, bool, double (*)[3]);
  void applyToDiagonalTerm(int, double *, double *, double *, bool, double (*)[3]);
  void applyToDiagonalTerm(int, double *, double *, std::complex<double> *, bool, double (*)[3]);
  void applyToOffDiagonalTerm(int, float *, bool, double (*)[3]);
  void applyToOffDiagonalTerm(int, double *, bool, double (*)[3]);
  void applyToOffDiagonalTerm(int, std::complex<double> *, bool, double (*)[3]);

};

//------------------------------------------------------------------------------

class BcFcnNS : public BcFcn {
  BcsSymmetryData& symmetry;
  BcsWallData& wall;

 public:
  BcFcnNS(IoData&);
  ~BcFcnNS() {}

  static void template_applyToSolutionVectorTerm(int, double *, double *);

  static void template_applyToResidualTerm(int, double *, double *, double *);

  static void template_applyToDerivativeOfResidualTerm(int, double *, double *, double *, double *, double *);

  static void template_applyToTransposeDerivativeOfResidualTerm(int, double *);

  template<class Scalar, int neq>
  static void template_applyToDiagonalTerm(int, double *, double *, Scalar *);

  template<class Scalar, int neq>
  static void template_applyToOffDiagonalTerm(int, Scalar *);

  void applyToSolutionVector(int, double *, double *, bool, double (*)[3]);
  void applyToResidualTerm(int, double *, double *, double *, bool, double (*)[3]);
  void applyToDerivativeOfResidualTerm(int, double *, double *, double *, double *, double *, bool, double (*)[3]);
  void applyToTransposeDerivativeOfResidualTerm(int, double *, bool, double (*)[3]);

  void applyToDiagonalTerm(int, double *, double *, float *, bool, double (*)[3]);
  void applyToDiagonalTerm(int, double *, double *, double *, bool, double (*)[3]);
  void applyToDiagonalTerm(int, double *, double *, std::complex<double> *, bool, double (*)[3]);
  void applyToOffDiagonalTerm(int, float *, bool, double (*)[3]);
  void applyToOffDiagonalTerm(int, double *, bool, double (*)[3]);
  void applyToOffDiagonalTerm(int, std::complex<double> *, bool, double (*)[3]);

};

//------------------------------------------------------------------------------
/*
@BOOK{white-74,
  author = "White, F. M.",
  title = "Viscous fluid flow",
  publisher = "McGraw-Hill",
  year = 1974,
}
*/
class BcFcnSA : public BcFcn {
  BcsSymmetryData& symmetry;
  BcsWallData& wall;

  template<class Scalar>
  void template_applyToDiagonalTerm(int, double *, double *, Scalar *);

  template<class Scalar>
  void template_applyToOffDiagonalTerm(int, Scalar *);

  template<class Scalar>
  void template_applyToTurbDiagonalTerm(int, double *, double *, Scalar *);

  template<class Scalar>
  void template_applyToTurbOffDiagonalTerm(int, Scalar *);

  template<class Scalar>
  void template_applyToWallDiagonalTerm(int, double *, double *, double *, Scalar *);

 public:

  BcFcnSA(IoData&);
  ~BcFcnSA() {}

  void applyToSolutionVector(int, double *, double *, bool, double (*)[3]);
  void applyToResidualTerm(int, double *, double *, double *, bool, double (*)[3]);
  void applyToDerivativeOfResidualTerm(int, double *, double *, double *, double *, double *, bool, double (*)[3]);
  void applyToTransposeDerivativeOfResidualTerm(int, double *, bool, double (*)[3]);

  void applyToTurbSolutionVector(int, double *, double *);
  void applyToTurbResidualTerm(int, double *, double *, double *);
  void applyToDerivativeOfTurbResidualTerm(int, double *, double *, double *, double *, double *);

  void applyToDiagonalTerm(int, double *, double *, float *, bool, double (*)[3]);
  void applyToDiagonalTerm(int, double *, double *, double *, bool, double (*)[3]);
  void applyToDiagonalTerm(int, double *, double *, std::complex<double> *, bool, double (*)[3]);
  void applyToOffDiagonalTerm(int, float *, bool, double (*)[3]);
  void applyToOffDiagonalTerm(int, double *, bool, double (*)[3]);
  void applyToOffDiagonalTerm(int, std::complex<double> *, bool, double (*)[3]);

  void applyToTurbDiagonalTerm(int, double *, double *, float *);
  void applyToTurbDiagonalTerm(int, double *, double *, double *);
  void applyToTurbDiagonalTerm(int, double *, double *, std::complex<double> *);
  void applyToTurbOffDiagonalTerm(int, float *);
  void applyToTurbOffDiagonalTerm(int, double *);
  void applyToTurbOffDiagonalTerm(int, std::complex<double> *);

  void applyToWallDiagonalTerm(int, double *, double *, double *, float *);
  void applyToWallDiagonalTerm(int, double *, double *, double *, double *);
  void applyToWallDiagonalTerm(int, double *, double *, double *, std::complex<double> *);

};

//------------------------------------------------------------------------------

class BcFcnSAturb : public BcFcn {

  template<class Scalar>
  void template_applyToDiagonalTerm(int, double *, double *, Scalar *);

  template<class Scalar>
  void template_applyToOffDiagonalTerm(int, Scalar *);

  template<class Scalar>
  void template_applyToTurbDiagonalTerm(int, double *, double *, Scalar *);

  template<class Scalar>
  void template_applyToTurbOffDiagonalTerm(int, Scalar *);

 public:
  BcFcnSAturb() {}
  ~BcFcnSAturb() {}

  void applyToDiagonalTerm(int, double *, double *, float *, bool, double (*)[3]);
  void applyToDiagonalTerm(int, double *, double *, double *, bool, double (*)[3]);
  void applyToDiagonalTerm(int, double *, double *, std::complex<double> *, bool, double (*)[3]);
  void applyToOffDiagonalTerm(int, float *, bool, double (*)[3]);
  void applyToOffDiagonalTerm(int, double *, bool, double (*)[3]);
  void applyToOffDiagonalTerm(int, std::complex<double> *, bool, double (*)[3]);

  void applyToTurbDiagonalTerm(int, double *, double *, float *);
  void applyToTurbDiagonalTerm(int, double *, double *, double *);
  void applyToTurbDiagonalTerm(int, double *, double *, std::complex<double> *);
  void applyToTurbOffDiagonalTerm(int, float *);
  void applyToTurbOffDiagonalTerm(int, double *);
  void applyToTurbOffDiagonalTerm(int, std::complex<double> *);

};

//------------------------------------------------------------------------------
/*
@ARTICLE{jaeger-dhatt-92,
  author = "Jaeger, M. and Dhatt, G.",
  title = "An extended k--$\epsilon$ finite element model",
  journal = ijnmf,
  year = 1992,
  volume = 14,
  pages = "1325--1345",
}
*/
class BcFcnKE : public BcFcn {

  BcsSymmetryData& symmetry;
  BcsWallData& wall;

  template<class Scalar>
  void template_applyToDiagonalTerm(int, double *, double *, Scalar *);

  template<class Scalar>
  void template_applyToOffDiagonalTerm(int, Scalar *);

  template<class Scalar>
  void template_applyToTurbDiagonalTerm(int, double *, double *, Scalar *);

  template<class Scalar>
  void template_applyToTurbOffDiagonalTerm(int, Scalar *);

  template<class Scalar>
  void template_applyToWallDiagonalTerm(int, double *, double *, double *, Scalar *);

 public:
  BcFcnKE(IoData&);
  ~BcFcnKE() {}

  void applyToSolutionVector(int, double *, double *, bool, double (*)[3]);
  void applyToResidualTerm(int, double *, double *, double *, bool, double (*)[3]);
  void applyToDerivativeOfResidualTerm(int, double *, double *, double *, double *, double *, bool, double (*)[3]);
  void applyToTransposeDerivativeOfResidualTerm(int, double *, bool, double (*)[3]);

  void applyToTurbSolutionVector(int, double *, double *);
  void applyToTurbResidualTerm(int, double *, double *, double *);
  void applyToDerivativeOfTurbResidualTerm(int, double *, double *, double *, double *, double *);

  void applyToDiagonalTerm(int, double *, double *, float *, bool, double (*)[3]);
  void applyToDiagonalTerm(int, double *, double *, double *, bool, double (*)[3]);
  void applyToDiagonalTerm(int, double *, double *, std::complex<double> *, bool, double (*)[3]);
  void applyToOffDiagonalTerm(int, float *, bool, double (*)[3]);
  void applyToOffDiagonalTerm(int, double *, bool, double (*)[3]);
  void applyToOffDiagonalTerm(int, std::complex<double> *, bool, double (*)[3]);

  void applyToTurbDiagonalTerm(int, double *, double *, float *);
  void applyToTurbDiagonalTerm(int, double *, double *, double *);
  void applyToTurbDiagonalTerm(int, double *, double *, std::complex<double> *);
  void applyToTurbOffDiagonalTerm(int, float *);
  void applyToTurbOffDiagonalTerm(int, double *);
  void applyToTurbOffDiagonalTerm(int, std::complex<double> *);

  void applyToWallDiagonalTerm(int, double *, double *, double *, float *);
  void applyToWallDiagonalTerm(int, double *, double *, double *, double *);
  void applyToWallDiagonalTerm(int, double *, double *, double *, std::complex<double> *);

};

//------------------------------------------------------------------------------

class BcFcnKEturb : public BcFcn {

  template<class Scalar>
  void template_applyToDiagonalTerm(int, double *, double *, Scalar *);

  template<class Scalar>
  void template_applyToOffDiagonalTerm(int, Scalar *);

  template<class Scalar>
  void template_applyToTurbDiagonalTerm(int, double *, double *, Scalar *);

  template<class Scalar>
  void template_applyToTurbOffDiagonalTerm(int, Scalar *);

 public:
  BcFcnKEturb() {}
  ~BcFcnKEturb() {}

  void applyToDiagonalTerm(int, double *, double *, float *, bool, double (*)[3]);
  void applyToDiagonalTerm(int, double *, double *, double *, bool, double (*)[3]);
  void applyToDiagonalTerm(int, double *, double *, std::complex<double> *, bool, double (*)[3]);
  void applyToOffDiagonalTerm(int, float *, bool, double (*)[3]);
  void applyToOffDiagonalTerm(int, double *, bool, double (*)[3]);
  void applyToOffDiagonalTerm(int, std::complex<double> *, bool, double (*)[3]);

  void applyToTurbDiagonalTerm(int, double *, double *, float *);
  void applyToTurbDiagonalTerm(int, double *, double *, double *);
  void applyToTurbDiagonalTerm(int, double *, double *, std::complex<double> *);
  void applyToTurbOffDiagonalTerm(int, float *);
  void applyToTurbOffDiagonalTerm(int, double *);
  void applyToTurbOffDiagonalTerm(int, std::complex<double> *);

};

//------------------------------------------------------------------------------

#endif

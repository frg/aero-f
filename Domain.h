#ifndef _DOMAIN_H_
#define _DOMAIN_H_

#include <IoData.h>
#include <DistInfo.h>
#include <NodeData.h>
#include <Types.h>

#include <functional>
#include <iostream>
#include <map>
#include <set>
#include <complex>

class IoData;
class VarFcn;
class BcFcn;
class RecFcn;
class FemEquationTerm;
class VolumicForceTerm;
class DistEdgeGrad;
class FluxFcn;
class TimeData;
class SubDTopo;
class SubDomain;
class GeoSource;
class DistGeoState;
class PostFcn;
class TimeLowMachPrec;
class SpatialLowMachPrec;
class Timer;
class ErrorHandler;
class Connectivity;
class DistLevelSetStructure;
class WallFcn;
class DistMeshState;
class Communicator;
class BCApplier;
class MatchNodeSet;
class ControlInterface;
class GeoSource;

struct BcsSymmetryData;
struct Vec3D;

template<int dim> class DistBcData;
template<class T> class CommPattern;
template<class Scalar> class DistVec;
template<class Scalar, int Dim> class DistSVec;
template<class Scalar> class Vec;
template<class MatrixType> class DistMat;
template<class Scalar, int Dim> class MvpMat;
template<class Scalar, int Dim> class SparseMat;
template<int dim> class DistEmbeddedBcData;
template<int dim> class GhostPoint;
template<int dim> class DistRestartState;
template<int dim> class DistLESData;
template<class Scalar> class DenseMat;
template<int dim> class MatVecProd_dRdX;
template<class Scalar, int dim, int dim2> class RectangularSparseMat;
template<int dim, class Scalar> class DistNodalGrad;

#ifndef _VECSET_TMPL_
  #define _VECSET_TMPL_
  template<class VecType, class Scalar = double> class VecSet;
#endif

//------------------------------------------------------------------------------
/*  \brief Class of all data for this MPI process
 *  Class containing the geometry of all the subdomains used in the current MPI process
 *
 * The Domain class contains all the SubDomains and offers entry points for all
 * parallel routines, distributing the work to each SubDomain.
 */
class Domain {

  int numLocSub;
  int numGlobSub;
  int *locSubToGlobSub;

  GeoSource *geoSourcePtr;

  SubDomain **subDomain;
  SubDTopo *subTopo;

  DistVec<int> *nodeType;
  DistVec<int> *nodeType2;  //used for hyperreduction decomposition
  DistSVec<double, 9> *nodeFrame;

  DistInfo *nodeDistInfo;
  DistInfo *edgeDistInfo;
  DistInfo *edgeNormDistInfo;
  DistInfo *faceDistInfo;
  DistInfo *faceNormDistInfo;

  std::map<int, CommPattern<double> *> pool;
  std::map<int, CommPattern<std::complex<double>> *> cpool;
  std::map<int, CommPattern<float> *> fpool;
  std::map<int, CommPattern<long int> *> lpool;
  std::map<int, CommPattern<int> *> ipool;
  std::map<int, CommPattern<bool> *> bpool;
  std::map<int, CommPattern<double> *> edge_pool;
  CommPattern<int> *levelPat;
  CommPattern<int> *ghostPat1;
  CommPattern<double> *edgePat;
  CommPattern<double> *scalarEdgePat;
  CommPattern<double> *ghostPat2;
  CommPattern<bool> *fsPat;
  CommPattern<bool> *boolPat;

  Communicator *com;
  Communicator *strCom;
  Communicator *heatCom;
  Communicator *embedCom;
  Communicator *globCom;
  Communicator *globFluidCom;

  Timer *timer;
  Timer *strTimer;
  Timer *heatTimer;
  bool mytimer;

  ErrorHandler *errorHandler;

  DistVec<double> *Delta;
  DistVec<double> *CsDelSq;
  DistVec<double> *PrT;
  DistVec<double> *WCsDelSq;
  DistVec<double> *WPrT;

  DistSVec<bool, 2> *tag;

  int meshID;

 public:
  Domain(int = 1, bool = false);
  ~Domain();

  void cleanup();
  void print();

  int getMeshID() const {
    return meshID; // meshID is only for when multiple HDM or reduced meshes are used, otherwise it is -1.
  }
  void setMeshID(int _meshID) {
    meshID = _meshID;
  }
  void setGeoSource(GeoSource *geoSource) {
    geoSourcePtr = geoSource;
  }
  GeoSource *getGeoSource() const {
    return geoSourcePtr;
  }
  int getNumLocSub() const {
    return numLocSub;
  }
  int getNumGlobSub() const {
    return numGlobSub;
  }
  int *getSubMap() const {
    return locSubToGlobSub;
  }
  SubDomain **getSubDomain() const {
    return subDomain;
  }
  SubDTopo *getSubTopo() const {
    return subTopo;
  }
  SubDTopo *&getSubTopoRef() {
    return subTopo;
  }
  DistVec<int> *getNodeType() const {
    return nodeType;
  }
  DistVec<int> *getNodeType2() const { //for hyperreduction decomposition
    return nodeType2;
  }
  CommPattern<int> *getLevelPat() const {
    return levelPat;
  }
  CommPattern<bool> *getBoolPat() const {
    return boolPat;
  }
  CommPattern<bool> *getFsPat() const {
    return fsPat;
  }
  Communicator *getCommunicator() const {
    return com;
  }
  Communicator *getStrCommunicator() {
    return strCom;
  }
  Communicator *getHeatCommunicator() {
    return heatCom;
  }
  Communicator *getEmbedCommunicator() {
    return embedCom;
  }
  Communicator *getGlobFluidCommunicator() {
    return globFluidCom;
  }
  void setTimer(Timer *);
  Timer *getTimer() const {
    return timer;
  }
  Timer *getStrTimer() const {
    return strTimer;
  }
  Timer *getHeatTimer() const {
    return heatTimer;
  }
  DistInfo& getNodeDistInfo() const {
    return *nodeDistInfo;
  }
  DistInfo& getEdgeDistInfo() const {
    return *edgeDistInfo;
  }
  DistInfo& getEdgeNormDistInfo() const {
    return *edgeNormDistInfo;
  }
  DistInfo& getFaceDistInfo() const {
    return *faceDistInfo;
  }
  DistInfo& getFaceNormDistInfo() const {
    return *faceNormDistInfo;
  }
  ErrorHandler *getErrorHandler() const {
    return errorHandler;
  }

  template<class Scalar, int Dim>
  CommPattern<Scalar> *getCommPat(DistSVec<Scalar, Dim>&);
  template<class Scalar>
  CommPattern<Scalar> *getCommPat(DistVec<Scalar>&);
  template<class Scalar>
  CommPattern<Scalar> *getCommPat(int);
  template<int Dim>
  CommPattern<double> *getEdgeCommPat(DistSVec<double, Dim>&);
  CommPattern<double> *getEdgeCommPat(int);
  CommPattern<double> *getEdgeCommPat(DistVec<double>&);

  void getGeometry(GeoSource&, IoData&);
  void constructRTree(IoData&, DistGeoState * = nullptr);
  void constructNodeRTree(IoData&, double);
  void reconstructRTree(DistGeoState * = nullptr);
  void createRhsPat(int, IoData&);
  void numberEdges();
  double minEdgeLength();
  void makeEdgeInterfaceLists();
  void setFaceType(IoData&);
  void setNodeType(IoData&);
  void computeOffWallNode(IoData&, DistLevelSetStructure *);
  void setFaceToElementConnectivity();
  int getNumNodesMax();
  int getNumElemsMax();
  int getNumNodesMin();
  int getNumElemsMin();
  void printElementStatistics(bool = false);
  void getMaxElementStatistics(int_t[3]);
  int computeControlVolumes(double, DistVec<double>&);
  int computeControlVolumes(double, DistSVec<double, 3>&, DistVec<double>&);
  void computeDerivativeOfControlVolumes(double, DistSVec<double, 3>&, DistSVec<double, 3>&, DistVec<double>&);
  void computeDerivativeOfControlVolumes(DistMat<RectangularSparseMat<double, 3, 1>>&, DistSVec<double, 3>&, DistVec<double>&);
  void computeTransposeDerivativeOfControlVolumes(DistMat<RectangularSparseMat<double, 3, 1>>&, DistVec<double>&, DistSVec<double, 3>&);
  void computeDerivativeOperatorsOfControlVolumes(DistSVec<double, 3>&, DistMat<RectangularSparseMat<double, 3, 1>>&);
  void computeNormalsConfig(DistSVec<double, 3>&, DistSVec<double, 3>&, DistVec<Vec3D>&,
                            DistVec<double>&, DistVec<Vec3D>&, DistVec<double>&, bool = true);
  void computeDerivativeOfNormals(DistSVec<double, 3>&, DistSVec<double, 3>&, DistVec<Vec3D>&, DistVec<Vec3D>&,
                                  DistVec<double>&, DistVec<double>&, DistVec<Vec3D>&, DistVec<Vec3D>&,
                                  DistVec<double>&, DistVec<double>&);
  void computeDerivativeOfNormals(DistMat<RectangularSparseMat<double, 3, 3>>&, DistMat<RectangularSparseMat<double, 3, 3>>&,
                                  DistSVec<double, 3>&, DistVec<Vec3D>&, DistVec<double>&, DistVec<Vec3D>&, DistVec<double>&);
  void computeTransposeDerivativeOfNormals(DistMat<RectangularSparseMat<double, 3, 3>>&, DistMat<RectangularSparseMat<double, 3, 3>>&,
                                           DistVec<Vec3D>&, DistVec<Vec3D>&, DistSVec<double, 3>&);
  void computeDerivativeOperatorsOfNormals(DistSVec<double, 3>&, DistMat<RectangularSparseMat<double, 3, 3>>&,
                                           DistMat<RectangularSparseMat<double, 3, 3>>&);
  void computeNormalsGCL1(DistSVec<double, 3>&, DistSVec<double, 3>&,
                          DistSVec<double, 3>&, DistVec<Vec3D>&, DistVec<double>&,
                          DistVec<Vec3D>&, DistVec<double>&);
  void computeNormalsGCL2(TimeData&, DistVec<Vec3D>&, DistVec<Vec3D>&,
                          DistVec<double>&, DistVec<double>&, DistVec<Vec3D>&,
                          DistVec<Vec3D>&, DistVec<double>&, DistVec<double>&);
  void computeNormalsEZGCL1(double, DistSVec<double, 3>&, DistSVec<double, 3>&, DistVec<Vec3D>&,
                            DistVec<double>&, DistVec<Vec3D>&, DistVec<double>&);
  void computeNormalsEZGCL2(TimeData&, DistVec<double>&, DistVec<double>&,
                            DistVec<double>&, DistVec<double>&);
  void computeNormalsEZGCL3(TimeData&, DistVec<double>&, DistVec<double>&, DistVec<double>&,
                            DistVec<double>&, DistVec<double>&, DistVec<double>&);
  void computeVelocities(DGCLData::Velocities, TimeData&, DistSVec<double, 3>&,
                         DistSVec<double, 3>&, DistSVec<double, 3>&, DistSVec<double, 3>&,
                         DistSVec<double, 3>&);

  template<int dim>
  void computeWeightsLeastSquares(DistSVec<double, 3>&, DistSVec<double, 3>&, DistSVec<double, 3>&,
                                  DistEmbeddedBcData<dim> * = nullptr, int = 0, int = 4, DistSVec<double, 2> * = nullptr,
                                  std::function<double(int,int,double[3],int)> = [](int,int,double[3],int){return 1.;});

  void computeDerivativeOfWeightsLeastSquares(DistSVec<double, 3>&, DistSVec<double, 3>&, DistSVec<double, 3>&,
                                              DistSVec<double, 3>&, DistSVec<double, 3>&, DistSVec<double, 3>&);

  void computeDerivativeOfWeightsLeastSquares(DistMat<RectangularSparseMat<double, 3, 6>>&, DistMat<RectangularSparseMat<double, 6, 6>>&,
                                              DistSVec<double, 3>&, DistSVec<double, 6>&);

  void computeTransposeDerivativeOfWeightsLeastSquares(DistMat<RectangularSparseMat<double, 3, 6>>&, DistMat<RectangularSparseMat<double, 6, 6>>&,
                                                       DistSVec<double, 6>&, DistSVec<double, 3>&);

  void computeDerivativeOperatorsOfWeightsLeastSquares(DistSVec<double, 3>&, DistMat<RectangularSparseMat<double, 3, 6>>&,
                                                       DistMat<RectangularSparseMat<double, 6, 6>>&);

  void computeWeightsGalerkin(DistSVec<double, 3>&, DistSVec<double, 3>&, DistSVec<double, 3>&, DistSVec<double, 3>&);

  void computeDerivativeOfWeightsGalerkin(DistSVec<double, 3>&, DistSVec<double, 3>&, DistSVec<double, 3>&,
                                          DistSVec<double, 3>&, DistSVec<double, 3>&);

  void getReferenceMeshPosition(DistSVec<double, 3>&);
  void getNdAeroLists(int *&, int **&, int *&, int **&, int *&, int **&, MatchNodeSet **);

  void findNodeBoundingBoxes(DistSVec<double, 3>&, DistSVec<double, 3>&, DistSVec<double, 3>&, int, double * = 0);

  template<class Scalar>
  void computeStiffAndForce(DefoMeshMotionData&, DistSVec<double, 3>&,
                            DistSVec<double, 3>&, DistMat<MvpMat<Scalar, 3>>&, std::vector<std::vector<double>>&);

  void pseudoFastMarchingMethod(DistVec<int>&, DistSVec<double, 3>&, DistVec<double>&, int,
                                DistVec<int>&, int *, int *, DistLevelSetStructure&, bool, int,
                                const double, DistVec<double> * = nullptr, bool = false,
                                DistSVec<std::pair<int, double>, 3> * = nullptr);

  void pseudoFastMarchingMethodComm(DistVec<int>&, DistVec<double>&, DistVec<double>&,
                                    DistVec<int>&, int *);

  void pseudoFastMarchingMethodCommDD2Wall(DistVec<int>&, DistVec<double>&, DistVec<double>&,
                                           DistVec<int>&, int *, DistSVec<std::pair<int, double>, 3> *);

  template<int dim>
  void computeTimeStep(double, double, double, double, FemEquationTerm *, VarFcn *, DistGeoState&,
                       DistSVec<double, dim>&, DistVec<double>&, DistVec<double>&,
                       DistVec<double>&, DistVec<double>&, DistVec<double>&, DistVec<double>&,
                       TimeLowMachPrec&, SpatialLowMachPrec&, DistEmbeddedBcData<dim> *, bool, double);

  template<int dim>
  void computeDerivativeOfInvReynolds(FemEquationTerm *, VarFcn *, DistGeoState&, DistSVec<double, dim>&,
                                      DistSVec<double, dim>&, DistVec<double>&, DistVec<double>&,
                                      DistVec<double>&, DistVec<double>&, DistVec<double>&, double,
                                      TimeLowMachPrec&, SpatialLowMachPrec&, DistEmbeddedBcData<dim> *);

  template<int Dim, class Scalar, int dim>
  void computeGradientsLeastSquares(DistSVec<double, 3>&, DistSVec<double, 3>&, DistSVec<Scalar, Dim>&,
                                    DistSVec<Scalar, Dim>&, DistSVec<Scalar, Dim>&, DistSVec<Scalar, Dim>&,
                                    DistEmbeddedBcData<dim> *, int = 0, DistVec<int> * = nullptr,
                                    DistSVec<double, 2> * = nullptr, DistVec<bool> * = nullptr);

  template<int Dim, int dim>
  void computeDerivativeOfGradientsLeastSquares(DistSVec<double, 3>&, DistSVec<double, 3>&, DistSVec<double, 3>&,
                                                DistSVec<double, 3>&, DistSVec<double, Dim>&, DistSVec<double, Dim>&,
                                                DistSVec<double, Dim>&, DistSVec<double, Dim>&, DistSVec<double, Dim>&,
                                                DistEmbeddedBcData<dim> *, DistSVec<double, 2> * = nullptr);

  template<int Dim, int dim>
  void computeDerivativeOfGradientsLeastSquares(DistSVec<double, 3>&, DistSVec<double, 3>&, DistSVec<double, 3>&,
                                                DistSVec<double, 3>&, DistSVec<std::complex<double>, Dim>&,
                                                DistSVec<std::complex<double>, Dim>&, DistSVec<std::complex<double>, Dim>&,
                                                DistSVec<std::complex<double>, Dim>&, DistSVec<std::complex<double>, Dim>&,
                                                DistEmbeddedBcData<dim> *, DistSVec<double, 2> * = nullptr) {
    std::cerr << "*** Error: derivative of complex least-squares gradient is not implemented\n"; exit(-1);
  }

  template<int Dim>
  void computeDerivativeOfGradientsLeastSquares(MatVecProd_dRdX<Dim>&, DistSVec<double, 3>&, DistSVec<double, 6>&, DistSVec<double, Dim>&,
                                                DistSVec<double, Dim>&, DistSVec<double, Dim>&, DistSVec<double, Dim>&);

  template<int Dim>
  void computeDerivativeOfGradientsLeastSquares(MatVecProd_dRdX<Dim>&, DistSVec<double, 3>&, DistSVec<double, 6>&,
                                                DistSVec<std::complex<double>, Dim>&, DistSVec<std::complex<double>, Dim>&,
                                                DistSVec<std::complex<double>, Dim>&, DistSVec<std::complex<double>, Dim>&) {
    std::cerr << "*** Error: derivative of complex least-squares gradient is not implemented\n"; exit(-1);
  }

  template<int Dim>
  void computeTransposeDerivativeOfGradientsLeastSquares(MatVecProd_dRdX<Dim>&, DistSVec<double, Dim>&, DistSVec<double, Dim>&,
                                                         DistSVec<double, Dim>&, DistSVec<double, 3>&, DistSVec<double, 6>&,
                                                         DistSVec<double, Dim>&);

  template<int Dim>
  void computeTransposeDerivativeOfGradientsLeastSquares(MatVecProd_dRdX<Dim>&, DistSVec<std::complex<double>, Dim>&,
                                                         DistSVec<std::complex<double>, Dim>&, DistSVec<std::complex<double>, Dim>&,
                                                         DistSVec<double, 3>&, DistSVec<double, 6>&, DistSVec<std::complex<double>, Dim>&) {
    std::cerr << "*** Error: derivative of complex least-squares gradient is not implemented\n"; exit(-1);
  }

  template<int Dim>
  void computeDerivativeOperatorsOfGradientsLeastSquares(DistSVec<double, 3>&, DistSVec<double, 6>&, DistSVec<double, Dim>&,
                                                         MatVecProd_dRdX<Dim>&, int = 4);

  template<int Dim>
  void computeDerivativeOperatorsOfGradientsLeastSquares(DistSVec<double, 3>&, DistSVec<double, 6>&, DistSVec<std::complex<double>, Dim>&,
                                                         MatVecProd_dRdX<Dim>&, int = 4) {
    std::cerr << "*** Error: derivative of complex least-squares gradient is not implemented\n"; exit(-1);
  }

  template<int Dim, class Scalar>
  void computeGradientsGalerkin(DistVec<double>&, DistSVec<double, 3>&, DistSVec<double, 3>&, DistSVec<double, 3>&,
                                DistSVec<Scalar, Dim>&, DistSVec<Scalar, Dim>&, DistSVec<Scalar, Dim>&,
                                DistSVec<Scalar, Dim>&, DistVec<bool> * = nullptr);

  template<int Dim>
  void computeDerivativeOfGradientsGalerkin(DistVec<double>&, DistVec<double>&, DistSVec<double, 3>&,
                                            DistSVec<double, 3>&, DistSVec<double, 3>&, DistSVec<double, 3>&,
                                            DistSVec<double, 3>&, DistSVec<double, 3>&, DistSVec<double, Dim>&,
                                            DistSVec<double, Dim>&, DistSVec<double, Dim>&, DistSVec<double, Dim>&,
                                            DistSVec<double, Dim>&);

  template<int Dim>
  void computeDerivativeOfGradientsGalerkin(DistVec<double>&, DistVec<double>&, DistSVec<double, 3>&,
                                            DistSVec<double, 3>&, DistSVec<double, 3>&, DistSVec<double, 3>&,
                                            DistSVec<double, 3>&, DistSVec<double, 3>&, DistSVec<std::complex<double>, Dim>&,
                                            DistSVec<std::complex<double>, Dim>&, DistSVec<std::complex<double>, Dim>&,
                                            DistSVec<std::complex<double>, Dim>&, DistSVec<std::complex<double>, Dim>&) {
   std::cerr << "*** Error: derivative of complex Galerkin gradient is not implemented\n"; exit(-1);
  }

  template<int Dim, int dim>
  void computeMultiDimLimiter(std::function<double(double&, double&, double&, double&, double&)>,
                              RecFcn *, VarFcn *, DistSVec<double, 3>&, DistVec<double>&, DistSVec<double, Dim>&,
                              DistNodalGrad<Dim, double>&, SchemeData&, DistEmbeddedBcData<dim> *);

  template<int Dim, int dim>
  void computeMultiDimLimiter(std::function<double(double&, double&, double&, double&, double&)>,
                              RecFcn *, VarFcn *, DistSVec<double, 3>&, DistVec<double>&, DistSVec<std::complex<double>, Dim>&,
                              DistNodalGrad<Dim, std::complex<double>>&, SchemeData&, DistEmbeddedBcData<dim> *) {
    std::cerr << "*** Error: complex multi-dim limiter is not implemented\n"; exit(-1);
  }

  template<int Dim>
  void computeJacobianMultiDimLimiter(std::function<double(double&, double&, double&, double&, double&)>,
                                      std::function<double(double&, double&, double&, double&, double&, double&, double&, double&, double&, double&)>,
                                      RecFcn *, VarFcn *, DistSVec<double, 3>&, DistVec<double>&, DistSVec<double, Dim>&, DistNodalGrad<Dim, double>&,
                                      SchemeData&);

  template<int Dim>
  void computeJacobianMultiDimLimiter(std::function<double(double&, double&, double&, double&, double&)>,
                                      std::function<double(double&, double&, double&, double&, double&, double&, double&, double&, double&, double&)>,
                                      RecFcn *, VarFcn *, DistSVec<double, 3>&, DistVec<double>&, DistSVec<std::complex<double>, Dim>&,
                                      DistNodalGrad<Dim, std::complex<double>>&, SchemeData&) {
    std::cerr << "*** Error: complex multi-dim limiter Jacobian is not implemented\n"; exit(-1);
  }

  template<int Dim>
  void computeDerivativeOfMultiDimLimiter(std::function<double(double&, double&, double&, double&, double&)>,
                                          std::function<double(double&, double&, double&, double&, double&, double&, double&, double&, double&, double&)>,
                                          RecFcn *, DistSVec<double, 3>&, DistSVec<double, 3>&,
                                          DistVec<double>&, DistVec<double>&, DistSVec<double, Dim>&, DistSVec<double, Dim>&,
                                          DistSVec<double, Dim>&, DistSVec<double, Dim>&, DistSVec<double, Dim>&,
                                          DistSVec<double, Dim>&, DistSVec<double, Dim>&, DistSVec<double, Dim>&,
                                          DistSVec<double, Dim>&, DistSVec<double, Dim>&, DistSVec<double, Dim>&,
                                          DistSVec<double, Dim>&, DistSVec<double, Dim>&, DistSVec<double, Dim>&);

  template<int Dim>
  void computeDerivativeOfMultiDimLimiter(std::function<double(double&, double&, double&, double&, double&)>,
                                          std::function<double(double&, double&, double&, double&, double&, double&, double&, double&, double&, double&)>,
                                          RecFcn *, DistSVec<double, 3>&, DistSVec<double, 3>&,
                                          DistVec<double>&, DistVec<double>&, DistSVec<std::complex<double>, Dim>&, DistSVec<std::complex<double>, Dim>&,
                                          DistSVec<std::complex<double>, Dim>&, DistSVec<std::complex<double>, Dim>&, DistSVec<std::complex<double>, Dim>&,
                                          DistSVec<std::complex<double>, Dim>&, DistSVec<std::complex<double>, Dim>&, DistSVec<std::complex<double>, Dim>&,
                                          DistSVec<double, Dim>&, DistSVec<double, Dim>&, DistSVec<double, Dim>&,
                                          DistSVec<double, Dim>&, DistSVec<double, Dim>&, DistSVec<double, Dim>&) {
    std::cerr << "*** Error: complex multi-dim limiter derivative is not implemented\n"; exit(-1);
  }

  template<int Dim>
  void computeWBAPLimiter(double, DistSVec<double, Dim>&, DistSVec<double, Dim>&, DistSVec<double, Dim>&);

  template<int Dim>
  void computeWBAPLimiter(double, DistSVec<std::complex<double>, Dim>&, DistSVec<std::complex<double>, Dim>&,
                          DistSVec<std::complex<double>, Dim>&) {
    std::cerr << "*** Error: complex WBAP limiter is not implemented\n"; exit(-1);
  }

  template<int Dim>
  void computePressureSensor(double, DistSVec<double, 3>&, DistSVec<double, Dim>&, DistSVec<double, Dim>&,
                             DistSVec<double, Dim>&, DistSVec<double, Dim>&,
                             DistSVec<double, 3>&, DistVec<double>&);

  template<int Dim>
  void computePressureSensor(double, DistSVec<double, 3>&, DistSVec<std::complex<double>, Dim>&,
                             DistSVec<std::complex<double>, Dim>&, DistSVec<std::complex<double>, Dim>&,
                             DistSVec<std::complex<double>, Dim>&, DistSVec<double, 3>&, DistVec<double>&) {
    std::cerr << "*** Error: complex p-sensor limiter is not implemented\n"; exit(-1);
  }

  template<int dim>
  void setupUVolumesInitialConditions(const int volid, double UU[dim], DistSVec<double, dim>& U);

  template<int dim>
  void computeRiemannAverages(DistGeoState&, DistSVec<double, dim>&, DistEmbeddedBcData<dim> *);

  template<int dim>
  void computeGlobalNeighbors(DistGeoState&, DistSVec<double, dim>&, DistEmbeddedBcData<dim> *);

  template<int dim>
  void computeFiniteVolumeTerm(DistVec<double>&, FluxFcn **, RecFcn *, VarFcn *, DistBcData<dim>&, DistGeoState&,
                               DistSVec<double, dim>&, DistNodalGrad<dim, double>&, DistEdgeGrad *,
                               DistNodalGrad<dimLS, double> *, DistSVec<double, dim>&, int, int,
                               DistEmbeddedBcData<dim> *, DistVec<bool> * = nullptr);

  template<int dim>
  void computeDerivativeOfFiniteVolumeTerm(DistVec<double>&, DistVec<double>&, FluxFcn **, RecFcn *,
                                           DistBcData<dim>&, DistGeoState&, DistSVec<double, dim>&,
                                           DistSVec<double, dim>&, DistNodalGrad<dim, double>&, DistEdgeGrad *,
                                           DistNodalGrad<dimLS, double> *, double, DistSVec<double, dim>&,
                                           int, DistEmbeddedBcData<dim> *);

  template<int dim>
  void computeDerivativeOfFiniteVolumeTerm(MatVecProd_dRdX<dim>&, DistBcData<dim>&, DistGeoState&,
                                           DistNodalGrad<dim, double>&, DistVec<double>&, DistSVec<double, dim>&,
                                           DistSVec<double, dim>&);

  template<int dim>
  void computeTransposeDerivativeOfFiniteVolumeTerm(MatVecProd_dRdX<dim>&, DistBcData<dim>&, DistGeoState&,
                                                    DistSVec<double, dim>&, DistNodalGrad<dim, double>&, DistSVec<double, 3>&);

  template<int dim>
  void computeDerivativeOperatorsOfFiniteVolumeTerm(FluxFcn **, RecFcn *, DistBcData<dim>&, DistGeoState&,
                                                    DistSVec<double, dim>&, DistNodalGrad<dim, double>&,
                                                    DistEmbeddedBcData<dim> *, MatVecProd_dRdX<dim>&);

  template<int dim>
  void computeFiniteVolumeTermLS(RecFcn *, RecFcn *, DistGeoState&, DistSVec<double, dim>&,
                                 DistNodalGrad<dim, double>&, DistEdgeGrad *, DistNodalGrad<dimLS, double>&,
                                 DistEdgeGrad *, DistSVec<double, dimLS>&,
                                 DistSVec<double, dimLS>&, DistEmbeddedBcData<dim> *);

  template<int dim, class Scalar, int neq>
  void computeJacobianFiniteVolumeTerm(DistVec<double> *, FluxFcn **, RecFcn *,
                                       DistBcData<dim>&, DistGeoState&, DistSVec<double, dim>&,
                                       DistNodalGrad<dim, double>&, DistEdgeGrad *, DistNodalGrad<dimLS, double> *,
                                       DistMat<MvpMat<Scalar, neq>>&, int, DistEmbeddedBcData<dim> *);

  template<int dim, class Scalar>
  void computeJacobianFiniteVolumeTermLS(RecFcn *, RecFcn *, DistGeoState&, DistSVec<double, dim>&,
                                         DistNodalGrad<dim, double>&, DistEdgeGrad *, DistNodalGrad<dimLS, double>&,
                                         DistEdgeGrad *, DistSVec<double, dimLS>&,
                                         DistMat<MvpMat<Scalar, dimLS>>&, DistEmbeddedBcData<dim> *);

  template<int dim>
  void computeGalerkinTerm(FemEquationTerm *, VarFcn *, DistLESData<dim> *, DistBcData<dim>&, DistGeoState&,
                           DistSVec<double, dim>&, DistSVec<double, dim>&, DistEmbeddedBcData<dim> *);

  template<int dim>
  void computeDerivativeOfGalerkinTerm(FemEquationTerm *, VarFcn *, DistLESData<dim> *, DistBcData<dim>&,
                                       DistGeoState&, DistSVec<double, dim>&, DistSVec<double, dim>&,
                                       bool, DistSVec<double, dim>&, DistEmbeddedBcData<dim> *);

  template<int dim>
  void computeDerivativeOfGalerkinTerm(MatVecProd_dRdX<dim>&, DistGeoState&, double, DistSVec<double, dim>&);

  template<int dim>
  void computeTransposeDerivativeOfGalerkinTerm(MatVecProd_dRdX<dim>&, DistGeoState&, DistSVec<double, dim>&,
                                                DistSVec<double, 3>&);

  template<int dim>
  void computeDerivativeOperatorsOfGalerkinTerm(FemEquationTerm *, DistBcData<dim>&, DistGeoState&,
                                                DistSVec<double, dim>&, DistEmbeddedBcData<dim> *,
                                                MatVecProd_dRdX<dim>&);

  template<int dim, class Scalar, int neq>
  void computeJacobianGalerkinTerm(FemEquationTerm *, VarFcn *, DistLESData<dim> *, DistBcData<dim>&,
                                   DistGeoState&, DistSVec<double, dim>&, DistMat<MvpMat<Scalar, neq>>&,
                                   DistEmbeddedBcData<dim> *);

  template<int dim>
  void computeBCsJacobianWallValues(FemEquationTerm *, DistBcData<dim>&, DistGeoState&,
                                    DistSVec<double, dim>&, DistEmbeddedBcData<dim> *);

  template<int dim>
  void computeVolumicForceTerm(VolumicForceTerm *, DistVec<double>&,
                               DistSVec<double, dim>&, DistSVec<double, dim>&);

  template<int dim>
  void computeDerivativeOfVolumicForceTerm(VolumicForceTerm *, DistVec<double>&, DistVec<double>&,
                                           DistSVec<double, dim>&, DistSVec<double, dim>&, DistSVec<double, dim>&);

  template<int dim, class Scalar, int neq>
  void computeJacobianVolumicForceTerm(VolumicForceTerm *, DistVec<double>&,
                                       DistSVec<double, dim>&, DistMat<MvpMat<Scalar, neq>>&);

  template<int dim>
  void precomputeRec(RecFcn *, DistSVec<double, 3>&, DistNodalGrad<dim, double>&, DistEdgeGrad *,
                     DistSVec<double, dim>&, DistSVec<double, 4 * dim>&);

  template<int dim, class Scalar, int neq>
  void populateRecJacobian(VarFcn *, RecFcn *, DistNodalGrad<dim, double>&, DistEdgeGrad *,
                           DistVec<double>&, DistSVec<double, dim>&, DistMat<MvpMat<Scalar, neq>>&, DistEmbeddedBcData<dim> *);

  template<int dim, class Scalar, int neq, class Scalar2>
  void computeMatVecProdH2(TsData::Form, RecFcn *, DistSVec<double, 3>&, DistVec<double>&, DistNodalGrad<dim, Scalar2>&, DistEdgeGrad *,
                           DistSVec<double, 4 * dim>&, DistMat<MvpMat<Scalar, neq>>&,
                           DistSVec<Scalar2, dim>&, DistSVec<Scalar2, neq>&, DistVec<bool> * = nullptr);

  template<int dim, class Scalar>
  void multiplyBydVdU(VarFcn *, DistSVec<double, dim>&, DistSVec<Scalar, dim>&, DistSVec<Scalar, dim>&,
                      DistVec<int> *, DistVec<bool> * = nullptr);

  template<int dim>
  void applyControlVolumesToResidual(TsData::Form, DistVec<double>&, DistSVec<double, dim>&);

  template<int dim>
  void applyControlVolumesToDerivativeOfResidual(TsData::Form, DistVec<double>&, DistVec<double>&, DistSVec<double, dim>&, DistSVec<double, dim>&);

  template<class Scalar, int neq>
  void applyControlVolumesToJacobian(TsData::Form, DistVec<double>&, DistMat<MvpMat<Scalar, neq>>&);

  template<int dim>
  void applyBCsToSolutionVector(BcFcn *, DistBcData<dim>&, DistSVec<double, dim>&, DistLevelSetStructure *);

  template<int dim>
  void applyBCsToSolutionVectorFullMesh(BcFcn *, DistBcData<dim>&, DistSVec<double, dim>&, DistLevelSetStructure *);

  template<int dim>
  void applyBCsToTurbSolutionVector(BcFcn *, DistBcData<dim>&, DistSVec<double, dim>&, DistLevelSetStructure *);

  template<int dim>
  void applyBCsToResidual(BcFcn *, DistBcData<dim>&, DistSVec<double, dim>&, DistSVec<double, dim>&, DistLevelSetStructure *);

  template<int dim>
  void applyBCsToDerivativeOfResidual(BcFcn *, DistBcData<dim>&, DistSVec<double, dim>&, DistSVec<double, dim>&, DistSVec<double, dim>&,
                                      DistLevelSetStructure *);

  template<int dim>
  void applyBCsToTransposeDerivativeOfResidual(BcFcn *, DistBcData<dim>&, DistSVec<double, dim>&, DistLevelSetStructure *);

  template<int dim>
  void applyBCsToTimeResidual(BcFcn *, DistBcData<dim>&, DistSVec<double, dim>&, DistSVec<double, dim>&, DistLevelSetStructure *);

  template<int dim, class Scalar, int neq>
  void applyBCsToJacobian(BcFcn *, DistBcData<dim>&, DistSVec<double, dim>&, DistMat<MvpMat<Scalar, neq>>&, DistLevelSetStructure *);

  template<int dim, class Scalar, int neq>
  void applyBCsToJacobianWallValues(BcFcn *, DistBcData<dim>&, DistSVec<double, dim>&,
                                    DistMat<MvpMat<Scalar, neq>>&, DistLevelSetStructure *);

  template<class Scalar>
  void reconcile(CommPattern<Scalar> *, DistVec<Scalar>&);

  template<int Dim, class Scalar>
  void reconcile(CommPattern<Scalar> *, DistSVec<Scalar, Dim>&);

  template<int Dim, class Scalar>
  void assemble(CommPattern<Scalar> *, DistSVec<Scalar, Dim>&);

  template<int Dim, class Scalar>
  void assembleEdges(CommPattern<Scalar> *, DistSVec<Scalar, Dim>&);

  template<int Dim, class Scalar>
  void assemble(CommPattern<Scalar> *, DistSVec<Scalar, Dim>&, std::function<bool(Scalar,Scalar)>);

  template<int Dim, class Scalar>
  void assembleDerivative(CommPattern<Scalar> *, DistSVec<Scalar, Dim>&, std::function<bool(Scalar,Scalar)>,
                          CommPattern<Scalar> *, DistSVec<Scalar, Dim>&);

  template<class Scalar>
  void assemble(CommPattern<Scalar> *, DistVec<Scalar>&);

  template<class Scalar>
  void assembleEdges(CommPattern<Scalar> *, DistVec<Scalar>&);

  template<class Scalar>
  void assemble(CommPattern<Scalar> *, DistVec<Scalar>&, std::function<bool(Scalar,Scalar)>);

  template<class Scalar, class Scalar2>
  void assembleWithKey(CommPattern<Scalar> *, CommPattern<Scalar2> *, DistVec<Scalar>&, DistVec<Scalar2>&,
                       std::function<bool(Scalar,Scalar)>, Scalar2 * = 0);

  template<class Scalar>
  void assembleIntoSets(CommPattern<Scalar> *, DistVec<Scalar>&, DistVec<std::set<Scalar>>&);

  template<class Scalar, int dim>
  void assembleJacobian(CommPattern<Scalar> *, DistMat<MvpMat<Scalar, dim>>&);

  template<class Scalar, int dim>
  void assembleRecJacobian(CommPattern<Scalar> *, DistMat<MvpMat<Scalar, dim>>&);

  template<class Scalar, int dim>
  void assembleKspPrec(CommPattern<Scalar> *, CommPattern<int> *, DistMat<SparseMat<Scalar, dim>>&, const bool);

  template<class Scalar, int dim>
  void assembleBasis(CommPattern<Scalar> *, DistMat<DenseMat<Scalar>>&);

  template<class Scalar, int Dim>
  bool readVectorFromFile(const char *, int, double *, DistSVec<Scalar, Dim>&, Scalar * = 0, bool = true);

  template<class Scalar>
  bool readVectorFromFile(const char *, int, double *, DistVec<Scalar>&, Scalar * = 0, bool = true);

  template<class Scalar, int Dim>
  void readVectorFromFile(const char *, DistSVec<Scalar, Dim>&);

  template<class Scalar, int Dim>
  void writeVectorToFile(const char *, int, double, DistSVec<Scalar, Dim>&, Scalar * = 0, bool = true);

  template<class Scalar>
  void writeVectorToFile(const char *, int, double, DistVec<Scalar>&, Scalar * = 0, bool = true);

  template<class Scalar, int Dim>
  void writeVectorToFile(const char *, DistSVec<Scalar, Dim>&);

  template<class Scalar, int dim>
  void scaleSolution(DistSVec<Scalar, dim>&, RefVal *);

  template<int dim>
  int checkSolution(VarFcn *, DistSVec<double, dim>&, DistEmbeddedBcData<dim> *, int * = nullptr);

  template<int dim>
  void checkFailSafe(VarFcn *, DistSVec<double, dim>&, DistSVec<bool, 2>&, DistVec<int> *);

  template<int dim, int neq>
  int clipSolution(TsData::Clipping, BcsWallData::Integration,
                   VarFcn *, double *, DistSVec<double, dim>&, DistEmbeddedBcData<dim> *);

  template<int dim>
  void computeForceDerivs(VarFcn *, DistSVec<double, 3>&, DistSVec<double, dim>&,
                          DistSVec<double, dim>&, Vec<double>&, VecSet<DistSVec<double, 3>>&);

  template<class TRIPLET, int dim>
  void computeForceDerivMat(VarFcn *, DistSVec<double, 3>&, DistSVec<double, dim>&,
                            VecSet<DistSVec<double, 3>>&, std::vector<std::vector<TRIPLET>>&);

  template<int dim>
  void padeReconstruction(VecSet<DistSVec<double, dim>>&, VecSet<DistSVec<double, dim>>&,
                          int *, double *, double, int, int, int, int);

  template<int dim>
  void hardyInterpolationLogMap(VecSet<DistSVec<double, dim>, double> **, VecSet<DistSVec<double, dim>, double>&,
                                int, int, int, DenseMat<double>&, DenseMat<double>&);

  template<int dim>
  void getGradP(DistNodalGrad<dim, double>&, DistSVec<double, 3>&);

  template<int dim>
  void getDerivativeOfGradP(DistNodalGrad<dim, double>&, DistSVec<double, 3>&);

  template<int dim>
  void computeDerivativeOperatorOfGradP(MatVecProd_dRdX<dim>&);

  template<int dim>
  void getDerivativeOfGradP(MatVecProd_dRdX<dim>&, DistNodalGrad<dim, double>&, DistSVec<double, 3>&);

  template<int dim>
  void getTransposeDerivativeOfGradP(MatVecProd_dRdX<dim>&, DistSVec<double, 3>&, DistNodalGrad<dim, double>&);

  void computeMaterialVolumes(double *, int, DistVec<double>&, DistVec<int> *);

  template<int dim>
  void computeMaterialConservationScalars(double *, double *, double *, double *, double *, int, DistSVec<double, dim>&,
                                          DistVec<double>&, DistVec<int> *);

  template<int dim>
  void computeEmbSurfBasedForceLoad(IoData&, DistSVec<double, 3>&, double (*)[3], int,
                                    DistEmbeddedBcData<dim>&, double, DistSVec<double, dim>&,
                                    PostFcn *, DistNodalGrad<dim, double> *, VarFcn *, int);

  void computeInterfaceFluidMeshSize(IoData&, DistLevelSetStructure&, DistSVec<double, 3>&, int, int (*)[3], Vec<Vec3D>&,
                                     double&, std::vector<double>&, std::vector<double>&, std::vector<int>&);

  template<int dim>
  bool updateSweptNodes(DistSVec<double, dim>&, DistVec<double>&,
                        DistSVec<double, dim>&, DistEmbeddedBcData<dim>&, DistVec<int>&, int);

  template<int dim>
  void computeWeightsForSweptNodes(DistSVec<double, dim>&, DistVec<double>&,
                                   DistSVec<double, dim>&, DistVec<int>&,
                                   DistVec<int>&, DistEmbeddedBcData<dim>&, int);

  template<int dim>
  void populateGhostPoints(DistSVec<double, 3>&, DistSVec<double, dim>&, DistNodalGrad<dim, double> *,
                           VarFcn *, FemEquationTerm *, DistEmbeddedBcData<dim>&);

  template<int dim, class Scalar, int neq>
  void populateGhostJacobian(DistSVec<double, 3>&, DistSVec<double, dim>&, FluxFcn **, VarFcn *, DistMat<MvpMat<Scalar, neq>>&,
                             DistEmbeddedBcData<dim>&);

  void clearGhost();

  void setupPhiVolumesInitialConditions(const int, int, DistSVec<double, dimLS>&);

  void tagInterfaceNodes(int, DistVec<int>&, DistSVec<double, dimLS>&, int);

  void computeDistanceCloseNodes(int, DistVec<int>&, DistSVec<double, 3>&, DistSVec<double, dimLS>&, DistVec<double>&,
                                 MultiFluidData::CopyCloseNodes);

  void computeDistanceLevelNodes(int, DistVec<int>&, int, DistSVec<double, 3>&, DistVec<double>&, double&, DistSVec<double, dimLS>&,
                                 MultiFluidData::CopyCloseNodes);

  void getSignedDistance(int, DistVec<double>&, DistSVec<double, dimLS>&);

  template<int dim>
  void computeRefinementScalar(HessianCriterionData::SensorType, DistEmbeddedBcData<dim> *,
                               VarFcn *, DistSVec<double, dim>&, DistSVec<double, dimLS> *,
                               DistSVec<double, 1>&, DistSVec<double, 3>&, DistVec<double> *);

  template<int dim>
  void computeRefinementGradients(HessianCriterionData::SensorType, DistEmbeddedBcData<dim> *,
                                  VarFcn *, DistSVec<double, dim>&, DistSVec<double, dimLS> *,
                                  DistSVec<double, 1>&, DistSVec<double, 1>&, DistSVec<double, 1>&,
                                  DistSVec<double, 3>&, DistVec<double> *, LeastSquaresData&);

  template<int dim>
  void computeRefinementHessian(HessianCriterionData::SensorType, DistEmbeddedBcData<dim> *,
                                VarFcn *, DistSVec<double, dim>&, DistSVec<double, dimLS> *,
                                DistSVec<double, 9>&, DistSVec<double, 3>&, DistVec<double> *,
                                LeastSquaresData&);

  template<int dim>
  void computeDistanceErrorMetric(FemEquationTerm *, DistGeoState&, DistSVec<double, dim>&,
                                  DistVec<double>&, DistEmbeddedBcData<dim> *, TsData::Form);

  template<int dim>
  double computeYplusEdgeLength(GeoSource&, IoData&, DistSVec<double, 3>&, DistSVec<double, dim>&, DistVec<double>&,
                                DistEmbeddedBcData<dim> *, DistVec<double> *, VarFcn *,
                                WallFcn *, double&, RefinementDomainData&);

  void resize1(IoData&, int);
  void resize2(IoData&);

  void printGeometry(GeoSource&, int, const char*, DistGeoState * = nullptr, bool = true, bool = true, bool = true, bool = true);

  void assignErrorHandler();

  void checkVec(DistSVec<double, 3>&);

  ControlInterface *getUserSuppliedFunction(IoData&);

  template<int dim>
  void computeNodalForceLinearized(MatVecProd_dRdX<dim>&, DistSVec<double, dim>&, DistSVec<double, dim>&, DistSVec<double, 3>&,
                                   DistSVec<double, 6>&, DistSVec<double, 3>&, DistNodalGrad<dim, double>&, RecFcn *, DistSVec<double, 3>&);

  Domain * readMesh(IoData&, int);
};

//------------------------------------------------------------------------------

#endif

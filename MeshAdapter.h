#ifndef _MESH_ADAPTER_H_
#define _MESH_ADAPTER_H_

#include <SubDomain.h>
#include <Eigen/Core>

#include <map>
#include <vector>

class GeometricQuery;
class MetricSpace;
class TriangleSet;

// note: uncommenting the following line activates an alternative new implementation
//       of the refinement subiteration loop which is more rigorous in its handling
//       of certain corner cases, but also much slower.
//#define SUBITER_IMPL_NEW

// note: uncommenting the following lines activates an alternative new implementation
//       for assigning global numbers to new elements and faces; this also requires
//       a change to the coarsening (see coarsenNodes_Step2g and coarsenNodes_Step2h)
//       which currently only works for the Isotropic bisection strategy since it
//       relies on the element tag to identify the pairings
//#define NEW_ELEM_NUMBERING
//#define NEW_FACE_NUMBERING

// note: uncommenting the following line actives a partial precomputation method used
//       to compute barycentric coordinates in coarsenNodes_Step2i
//#define PRECOMPUTE_BARY

class MeshAdapter {

  SubDomain *subDomain;

  int locSubNum;
  int globSubNum;

  NodeSet& nodes;
  EdgeSet& edges;
  FaceSet& faces;
  ElemSet& elems;

  std::vector<int_t>& locToGlobNodeMap;
  std::vector<int_t>& locToGlobFaceMap;
  std::vector<int_t>& locToGlobElemMap;

  int& numNeighb;
  std::vector<int>& neighb;
  std::vector<int>& sndChannel;
  std::vector<int>& rcvChannel;
  std::vector<std::vector<int>>& sharedNodes;
#ifdef CONSISTENT_ORDERING
  std::vector<int>& allSharedNodes;
#endif
  std::vector<std::vector<EdgeDef>>& sharedEdges;

  MetricSpace *metricSpace;
  std::map<int, MetricSpace*> localMetrics;

 public:
  MeshAdapter(SubDomain *, MetricSpace *, std::map<int, MetricSpace*>);
  ~MeshAdapter();

  std::vector<int_t>& getFaceMap() { return locToGlobFaceMap; }
  std::vector<int_t>& getElemMap() { return locToGlobElemMap; }

  template<int dim>
  void refineMesh_Step1(AdaptiveMeshRefinementData&, SVec<double, dim>&, SVec<double, 3>&,
                        SVec<double, 1> *[], SVec<double, 1> *[], SVec<double, 1> *[],
                        SVec<double, 1> *[], std::set<int>&, std::vector<int>&,
                        int_t, EmbeddedBcData<dim> *, Vec<double> *, Vec<double> *, int,
                        int&, int, VarFcn *, WallFcn *, RTree<Vec3D> *, std::map<int, GeometricQuery*>&,
                        BcsSymmetryData&);
  void refineEdges_Step2a(std::set<int>&, CommPattern<int>&);
  void refineEdges_Step2b(std::set<int>&, CommPattern<int>&);
  void refineEdges_Step2c(AdaptiveMeshRefinementData&, std::vector<set_t<int>>&, const std::set<int>&, std::set<int>&, CommPattern<int>&);
  int  refineEdges_Step2d(std::set<int>&, std::set<int>&, CommPattern<int>&);
  void refineEdges_Step2e(AdaptiveMeshRefinementData&, const std::set<int>&, const std::vector<double>&,
                          std::vector<int>&);
  void refineEdges_Step2f(AdaptiveMeshRefinementData&, const std::vector<int>&, std::vector<set_t<int>>&,
                          const std::set<int>&, std::set<int>&, CommPattern<int>&);
  void refineEdges_Step2g(std::set<int>&, std::set<int>&, std::vector<int>&, int&, CommPattern<int>&);
  void refineEdges_Step2h(std::vector<int>&, int&);
  template<int dim>
  void refineEdges_Step3(AdaptiveMeshRefinementData&, const std::vector<int>&, NodeData&, std::vector<set_t<int>>&, std::vector<set_t<int>>&,
                         std::vector<set_t<int>>&, std::multimap<int,std::pair<int,int>>&, std::multimap<int,std::pair<int,int>>&,
                         EmbeddedBcData<dim> *, SVec<double, 3>&, bool, TriangleSet *, std::set<int>&);
  void coarsenNodes_Step2a(AdaptiveMeshRefinementData&, int_t, const std::vector<int>&, std::set<int>&, CommPattern<int>&);
  void coarsenNodes_Step2b(AdaptiveMeshRefinementData&, std::set<int>&, CommPattern<int>&);
  void coarsenNodes_Step2c(AdaptiveMeshRefinementData&, const std::set<int>&, std::vector<set_t<int>>&, std::set<int>&, CommPattern<int>&, int);
  int coarsenNodes_Step2d(const std::set<int>&, std::set<int>&, CommPattern<int>&);
  void coarsenNodes_Step2e(const std::set<int>&, CommPattern<int>&);
  void coarsenNodes_Step2f(std::set<int>&, CommPattern<int>&, const std::vector<set_t<int>>&, CommPattern<int_t>&);
  void coarsenNodes_Step2g(const std::set<int>&, const std::vector<set_t<int>>&, CommPattern<int_t>&);
  void coarsenNodes_Step2h(const std::vector<set_t<int>>&, std::set<int>&, std::map<int,NodePatch>&, CommPattern<int_t>&);
  void coarsenNodes_Step2i(AdaptiveMeshRefinementData&, std::set<int>&, std::map<int,NodePatch>&, std::vector<set_t<int>>&, CommPattern<int_t>&, CommPattern<int_t>&,
                           CommPattern<double>&, int, int, int&, RTree<Elem> *, NodeSet *, std::vector<std::set<Elem*>>&,
                           std::vector<std::set<int>>&, std::vector<std::set<int>>&);
  template<int dim>
  void coarsenNodes_Step2j(const std::set<int>&, const std::set<int>&, std::map<int,NodePatch>&, const std::vector<set_t<int>>&,
                           CommPattern<int_t>&, CommPattern<int_t>&, CommPattern<double>&, RestartState<dim>, RTree<Elem> *, NodeSet *,
                           RestartState<dim>, const std::vector<std::set<Elem*>>&, const std::vector<std::set<int>>&, const std::vector<std::set<int>>&);
  void coarsenNodes_Step2k(const std::set<int>&, const std::vector<set_t<int>>&, std::map<int,NodePatch>&, CommPattern<int_t>&);
  template<int dim>
  void coarsenNodes_Step3a(AdaptiveMeshRefinementData&, const std::set<int>&, std::set<int>&, std::set<int>&, std::set<int>&, std::vector<set_t<int>>&,
                           std::vector<set_t<int>>&, std::vector<set_t<int>>&, std::set<int>&, std::map<int,NodePatch>&, std::set<int>&,
                           std::set<int>&, CommPattern<int_t>&, CommPattern<double>&, RestartState<dim>, int&, RTree<Elem> *, NodeSet *, ElemSet *,
                           RestartState<dim>, int&, int&, bool, std::set<int>&);
  void insertSharedNodes_Step1a(std::map<int,set_t<int>>&, CommPattern<int>&);
  void insertSharedNodes_Step1b(std::set<int>&, CommPattern<int>&);
  void insertSharedNodes_Step1c(std::map<int,set_t<int>>&, CommPattern<int>&, CommPattern<int>&);
  void insertSharedNodes_Step2(const std::map<int,set_t<int>>&, CommPattern<int>&);
  void insertSharedNodes_Step3(std::set<int>&, CommPattern<int>&, int&, std::set<int> *);
  void eraseSharedNodes_Step1(const std::set<int>&, CommPattern<int>&);
  void eraseSharedNodes_Step2(const std::set<int>&, std::set<int>&, CommPattern<int>&);
  void coarsenNodes_Step3b(const std::set<int>&, const std::set<int>&, const std::set<int>&, const std::set<int>&,
                           int_t&, int_t&, int_t&, NodeData&, const std::set<int>&, const std::set<int>&,
                           const std::set<int>&, std::set<int>&);
  void updateMetricTensor_Step1(std::set<int>&, CommPattern<int>&, CommPattern<double>&);
  void updateMetricTensor_Step2(std::set<int>&, CommPattern<int>&, CommPattern<double>&);
  void updateMetricTensor_Step3(CommPattern<int>&, CommPattern<double>&);
  void assignGlobalNumbers_Step1a(SubDTopo&, int_t[3], std::vector<int>&, int&, int&, int&, std::vector<std::vector<int>>&,
                                  CommPattern<int>&, CommPattern<int_t>&);
  void assignGlobalNumbers_Step1b(std::multimap<int,std::pair<int,int>>&, std::vector<std::vector<int>>&, std::vector<std::vector<int>>&);
  void assignGlobalNodeNumbers_Step2(int&, int&, CommPattern<int>&, const std::vector<std::vector<int>>&);
  void assignGlobalNodeNumbers_Step3(const std::vector<int>&, int, int&, CommPattern<int>&, int&, const std::vector<std::vector<int>>&);
  void assignGlobalNodeNumbers_Step4(const std::vector<int>&, int, int, int_t, CommPattern<int_t>&, const std::vector<std::vector<int>>&);
  void assignGlobalNodeNumbers_Step5(CommPattern<int_t>&, int&, const std::vector<std::vector<int>>&);
  void assignGlobalFaceNumbers_Step2(int&, int&, CommPattern<int_t>&, const std::vector<std::vector<int>>&, const std::vector<std::vector<int>>&,
                                     const std::vector<std::vector<int>>&);
  void assignGlobalFaceNumbers_Step3(int, int&, CommPattern<int_t>&, int&, const std::vector<std::vector<int>>&, const std::vector<std::vector<int>>&,
                                     const std::vector<std::vector<int>>&);
  void assignGlobalFaceNumbers_Step4(int, int, int_t);
  void assignGlobalElemNumbers_Step2(int&, int&, CommPattern<int_t>&, const std::vector<std::vector<int>>&, const std::vector<std::vector<int>>&,
                                     const std::vector<std::vector<int>>&);
  void assignGlobalElemNumbers_Step3(int, int&, CommPattern<int_t>&, int&, const std::vector<std::vector<int>>&, const std::vector<std::vector<int>>&,
                                     const std::vector<std::vector<int>>&);
  void assignGlobalElemNumbers_Step4(int, int, int_t);
  void initializeRefinementTag_Step1(IoData&, Vec<int>&);
  void initializeRefinementTag_Step2(IoData&, Vec<int>&, Vec<int_t>&);
  void initializeRefinementTag_Step3(IoData&, Vec<int_t>&, Vec<int>&);
  void checkGlobalNumbers(int_t, int_t, int_t);
  void moveElems_Step1(const std::set<int>&, const std::multimap<int,int>&, SubDTopo*, CommPattern<int_t>&, CommPattern<double>&, int, int,
                       std::multimap<int,int>& elemToFace);
  template<int dim>
  void moveElems_Step2(const std::set<int>&, const std::multimap<int,int>&, std::set<int>&, std::set<int>&, std::set<int>&,
                       std::vector<set_t<int>>&, SubDTopo*, CommPattern<int_t>&, CommPattern<double>&, std::set<int>&,
                       std::map<std::pair<int,int>,int>&, RestartState<dim>, const std::multimap<int,int>&, bool);
  template<int dim>
  void moveElems_Step3(const std::set<int>&, std::multimap<int,int>&, std::set<int>&, std::vector<set_t<int>>&, SubDTopo*,
                       CommPattern<int_t>&, CommPattern<double>&, std::set<int>&, std::map<std::pair<int,int>,int>&,
                       RestartState<dim>, bool);
  void moveElems_Step4(const std::set<int>&, const std::set<int>&, const std::set<int>&, const std::map<std::pair<int,int>,int>&, NodeData&);
  int countMasterEdges(const std::set<int>&);
  void computeEdgeLengths_Step1(AdaptiveMeshRefinementData&, const std::set<int>& edge_indices_ref, std::vector<double>&, CommPattern<double>&);
  void computeEdgeLengths_Step2(std::vector<double>&, CommPattern<double>&);
  void checkSharedNodes_Step1(CommPattern<double>&);
  void checkSharedNodes_Step2(CommPattern<double>&);
};

//------------------------------------------------------------------------------

#endif

/*
 *    Supermesh.h
 */

#ifndef _SUPERMESH_
#define _SUPERMESH_

#include <Domain.h>
#include <SubDomain.h>
#include <Communicator.h>
#include <Connectivity.h>
#include <Timer.h>

#include <RTree.h>
#include <Vector3D.h>
#include <Elem.h>
#include <Node.h>
#include <DistVector.h>
#include <VectorSet.h>
#include <VectorSetAdaptive.h>
#include <DistMeshAdapter.h>
#include <DistRestartState.h>
#include <DistGeoState.h>
#include <DistEmbeddedBcData.h>
#include <NonlinearRomClustering.h>

#include <map>
#include <set>
#include <utility>
#include <vector>
#include <cstdio>

#include <ImplicitCoupledTsDesc.h>
#include <ImplicitTsDesc.h>
#include <TsSolver.h>

class varFcn;
class WallFcn;
class DistMeshState;

template<int dim>
class Supermesh {

  IoData& ioData;
  Domain *domain;
  Communicator *com;

  RTree<Vec3D> **supernodes;
  RTree<Vec3D> *subnodes;
  std::vector<Vec3D *> supernodesData;
  std::vector<Vec3D *> subnodesData;
  int nClusters;

 public:

  Clustering<Vec<double>> *clustering;

  Supermesh(IoData& _ioData, Domain *_domain, int _nClusters)
  : ioData(_ioData),
    domain(_domain),
    com(_domain->getCommunicator()),
    nClusters(_nClusters) {
      supernodes = new RTree<Vec3D> *[_nClusters];
      for(int iCluster = 0; iCluster < _nClusters; ++iCluster) {
        supernodes[iCluster] = new RTree<Vec3D>(ioData.amr.node_rtree_eps);
      }
      subnodes = new RTree<Vec3D>(ioData.amr.node_rtree_eps);
    }

  ~Supermesh() {
    if(supernodes) {
      for(int iCluster = 0; iCluster < nClusters; ++iCluster) {
        if(supernodes[iCluster]) {
          delete supernodes[iCluster];
        }
      }
      delete [] supernodes;
      supernodes = 0;
    }
    if(subnodes) {
      delete subnodes;
      subnodes = 0;
    }
    for(Vec3D *node : supernodesData) {
      delete node;
    }
    supernodesData.clear();
    for(Vec3D *node : subnodesData) {
      delete node;
    }
    subnodesData.clear();
  }

  //------------------------------------------------------------------------------

  RTree<Vec3D> **getSupernodes() {
    return supernodes;
  }

  //------------------------------------------------------------------------------

  RTree<Vec3D> *getSubnodes() {
    return subnodes;
  }

  //------------------------------------------------------------------------------

  std::vector<Vec3D *>& getSupernodesData() {
    return supernodesData;
  }

  //------------------------------------------------------------------------------

  std::vector<Vec3D *>& getSubnodesData() {
    return subnodesData;
  }

  //------------------------------------------------------------------------------

  void findSupernodes(std::vector<int>& snapToMesh, int iCluster) {
    int thisCPU = com->cpuNum();
    int numCPU = com->size();
    com->fprintf(stdout, "\nFinding supernodes of cluster %d...\n", iCluster);

    for(int iSnap = 0; iSnap < clustering->snapsInCluster[iCluster]; ++iSnap) {
      // read mesh
      int thisMsh = snapToMesh[clustering->clusterSnapshotMap[iCluster][iSnap]];
      Domain* domainAdaptive = domain->readMesh(ioData, thisMsh);

      // create com buffer
      int *numLocNodes = new int[numCPU];
      numLocNodes[thisCPU] = 0;
      for(int iSub = 0; iSub < domainAdaptive->getNumLocSub(); ++iSub) {
        numLocNodes[thisCPU] += domainAdaptive->getSubDomain()[iSub]->numNodes();
      }
      com->allGather<int>(numLocNodes, 1);
      int *offset = new int[numCPU+1];
      offset[0] = 0;
      for(int cpu = 0; cpu < numCPU; ++cpu) offset[cpu+1] = numLocNodes[cpu];
      for(int cpu = 0; cpu < numCPU; ++cpu) offset[cpu+1] += offset[cpu];
      double *buffer = new double[3*offset[numCPU]];

      // populate buffer
      SubDomain **subDomain = domainAdaptive->getSubDomain();
      int nodecnt = offset[thisCPU];
      for(int iSub = 0; iSub < domainAdaptive->getNumLocSub(); ++iSub) {
        for(int iNode = 0; iNode < subDomain[iSub]->numNodes(); ++iNode) {
          buffer[3*nodecnt+0] = subDomain[iSub]->getNodes()[iNode][0];
          buffer[3*nodecnt+1] = subDomain[iSub]->getNodes()[iNode][1];
          buffer[3*nodecnt+2] = subDomain[iSub]->getNodes()[iNode][2];
          nodecnt++;
        }
      }

      // exchange buffer
      int *counts = new int[numCPU];
      int *displs = new int[numCPU];
      for(int cpu = 0; cpu < numCPU; ++cpu) {
        counts[cpu] = 3*numLocNodes[cpu];
        displs[cpu] = 3*offset[cpu];
      }
      com->allGatherv(static_cast<double *>(MPI_IN_PLACE), counts[thisCPU], buffer, counts, displs);
      NodeSet tmpnodes(0);
      double bary[3];
      for(int iNode = 0; iNode < offset[numCPU]; ++iNode) {
        Vec3D tmp(buffer[3*iNode+0], buffer[3*iNode+1], buffer[3*iNode+2]);
        if(supernodes[iCluster]->root) {
          Vec3D *supit = supernodes[iCluster]->template search<NodeSet, &Vec3D::isPointInside>(tmpnodes, tmp, bary, ioData.amr.node_rtree_eps);
          if(!(supit)) {
            Vec3D *tmpcopy = new Vec3D(tmp); // clone for the RTree since it doesn't own its objects
            supernodes[iCluster]->template insert<NodeSet, &Vec3D::computeBoundingBox>(tmpnodes, tmpcopy);
            supernodesData.push_back(tmpcopy); // for later deletion
          }
        }
        else {
          Vec3D *tmpcopy = new Vec3D(tmp); // clone for the RTree since it doesn't own its objects
          supernodes[iCluster]->template insert<NodeSet, &Vec3D::computeBoundingBox>(tmpnodes, tmpcopy);
          supernodesData.push_back(tmpcopy); // for later deletion
        }
      }
      com->fprintf(stdout, "Mesh %d has %d nodes (including duplicates), supernodes so far: %d\n",
                   thisMsh, offset[numCPU], supernodes[iCluster]->getSize());

      // cleanup
      delete [] numLocNodes;
      delete [] offset;
      delete [] counts;
      delete [] displs;
      delete [] buffer;
      delete domainAdaptive;
    }

    // com->fprintf(stdout, "\nChecking supernodes...\n");
    // for(int iSnap = 0; iSnap < numVectors; ++iSnap) {
    //   // read mesh
    //   int thisMsh = snapToMesh[clustering->clusterSnapshotMap[iCluster][iSnap]];
    //   Domain* domainAdaptive = domain->readMesh(ioData, thisMsh);

    //   // search supernodes set for nodes in local subdomain
    //   int numMissing = 0;
    //   NodeSet tmpnodes(0);
    //   double bary[3];
    //   SubDomain **subDomain = domainAdaptive->getSubDomain();
    //   for(int iSub = 0; iSub < domainAdaptive->getNumLocSub(); ++iSub) {
    //     NodeSet& locNodes = subDomain[iSub]->getNodes();
    //     for(auto it = locNodes.begin(); it != locNodes.end(); it++) {
    //       Vec3D *supit = supernodes->search<NodeSet, &Vec3D::isPointInside>(tmpnodes, *it, bary, ioData.amr.node_rtree_eps);
    //       if(!(supit)) {
    //         numMissing++;
    //       }
    //     }
    //   }
    //   com->globalSum(1, &numMissing);
    //   com->fprintf(stdout, "Mesh %d has %d nodes missing from the supernodes set\n",
    //     thisMsh, numMissing);

    //   // cleanup
    //   delete domainAdaptive;
    // }
  }

  //------------------------------------------------------------------------------

  void findSubnodes(std::vector<int>& snapToMesh) {
    int thisCPU = com->cpuNum();
    int numCPU = com->size();
    com->fprintf(stdout, "\nFinding subnodes\n");

    for(int iSnap = 0; iSnap < snapToMesh.size(); ++iSnap) {
      // read mesh
      int thisMsh = snapToMesh[iSnap];
      Domain* domainAdaptive = domain->readMesh(ioData, thisMsh);

      // create com buffer
      int *numLocNodes = new int[numCPU];
      numLocNodes[thisCPU] = 0;
      for(int iSub = 0; iSub < domainAdaptive->getNumLocSub(); ++iSub) {
        numLocNodes[thisCPU] += domainAdaptive->getSubDomain()[iSub]->numNodes();
      }
      com->allGather<int>(numLocNodes, 1);
      int *offset = new int[numCPU+1];
      offset[0] = 0;
      for(int cpu = 0; cpu < numCPU; ++cpu) offset[cpu+1] = numLocNodes[cpu];
      for(int cpu = 0; cpu < numCPU; ++cpu) offset[cpu+1] += offset[cpu];
      double *buffer = new double[3*offset[numCPU]];

      // populate buffer
      SubDomain **subDomain = domainAdaptive->getSubDomain();
      int nodecnt = offset[thisCPU];
      for(int iSub = 0; iSub < domainAdaptive->getNumLocSub(); ++iSub) {
        for(int iNode = 0; iNode < subDomain[iSub]->numNodes(); ++iNode) {
          buffer[3*nodecnt+0] = subDomain[iSub]->getNodes()[iNode][0];
          buffer[3*nodecnt+1] = subDomain[iSub]->getNodes()[iNode][1];
          buffer[3*nodecnt+2] = subDomain[iSub]->getNodes()[iNode][2];
          nodecnt++;
        }
      }

      // exchange buffer
      int *counts = new int[numCPU];
      int *displs = new int[numCPU];
      for(int cpu = 0; cpu < numCPU; ++cpu) {
        counts[cpu] = 3*numLocNodes[cpu];
        displs[cpu] = 3*offset[cpu];
      }
      com->allGatherv(static_cast<double *>(MPI_IN_PLACE), counts[thisCPU], buffer, counts, displs);

      // populate node tree
      NodeSet tmpnodes(0);
      double bary[3];
      RTree<Vec3D> snapnodes(ioData.amr.node_rtree_eps);
      std::vector<Vec3D *> snapnodesData;
      for(int iNode = 0; iNode < offset[numCPU]; ++iNode) {
        Vec3D tmp(buffer[3*iNode+0], buffer[3*iNode+1], buffer[3*iNode+2]);
        if(iSnap == 0) {
          if(subnodes->root) {
            Vec3D *supit = subnodes->template search<NodeSet, &Vec3D::isPointInside>(tmpnodes, tmp, bary, ioData.amr.node_rtree_eps);
            if(!(supit)) {
              Vec3D *tmpcopy = new Vec3D(tmp); // clone for the RTree since it doesn't own its objects
              subnodes->template insert<NodeSet, &Vec3D::computeBoundingBox>(tmpnodes, tmpcopy);
              subnodesData.push_back(tmpcopy); // for later deletion
            }
          }
          else {
            Vec3D *tmpcopy = new Vec3D(tmp); // clone for the RTree since it doesn't own its objects
            subnodes->template insert<NodeSet, &Vec3D::computeBoundingBox>(tmpnodes, tmpcopy);
            subnodesData.push_back(tmpcopy); // for later deletion
          }
        }
        else {
          if(snapnodes.root) {
            Vec3D *supit = snapnodes.template search<NodeSet, &Vec3D::isPointInside>(tmpnodes, tmp, bary, ioData.amr.node_rtree_eps);
            if(!(supit)) {
              Vec3D *tmpcopy = new Vec3D(tmp); // clone for the RTree since it doesn't own its objects
              snapnodes.template insert<NodeSet, &Vec3D::computeBoundingBox>(tmpnodes, tmpcopy);
              snapnodesData.push_back(tmpcopy); // for later deletion
            }
          }
          else {
            Vec3D *tmpcopy = new Vec3D(tmp); // clone for the RTree since it doesn't own its objects
            snapnodes.template insert<NodeSet, &Vec3D::computeBoundingBox>(tmpnodes, tmpcopy);
            snapnodesData.push_back(tmpcopy); // for later deletion
          }
        }
      }

      // find intersection set
      if(iSnap != 0) {
        for(auto it = subnodesData.begin(); it != subnodesData.end();) {
          Vec3D *supit = snapnodes.template search<NodeSet, &Vec3D::isPointInside>(tmpnodes, **it, bary, ioData.amr.node_rtree_eps);
          if(!(supit)) {
            subnodes->template remove<NodeSet, &Vec3D::computeBoundingBox>(tmpnodes, *it);
            it = subnodesData.erase(it);
          }
          else {
            ++it;
          }
        }
      }
      for(Vec3D *node : snapnodesData) {
        delete node;
      }

      com->fprintf(stdout, "Mesh %d has %d nodes (including duplicates), subnodes so far: %d\n",
                   thisMsh, offset[numCPU], subnodes->getSize());

      // cleanup
      delete [] numLocNodes;
      delete [] offset;
      delete [] counts;
      delete [] displs;
      delete [] buffer;
      delete domainAdaptive;
    }

  }

  //------------------------------------------------------------------------------

};

#endif

#ifndef _VECTOR3DTEMPLATE_H_
#define _VECTOR3DTEMPLATE_H_

#include <cstdio>
#include <cmath>
#include <algorithm>

#include <unsupported/Eigen/AutoDiff>

//------------------------------------------------------------------------------

template<class Scalar>
struct Vec3DTemplate {

  Scalar v[3];

  Vec3DTemplate() {
    v[0] = v[1] = v[2] = 0.0;
  }
  Vec3DTemplate(Scalar x[3]) {
    v[0] = x[0];
    v[1] = x[1];
    v[2] = x[2];
  }
  Vec3DTemplate(Scalar x, Scalar y, Scalar z) {
    v[0] = x;
    v[1] = y;
    v[2] = z;
  }
  Vec3DTemplate(const Vec3DTemplate& v2) {
    v[0] = v2.v[0];
    v[1] = v2.v[1];
    v[2] = v2.v[2];
  }
  Vec3DTemplate(Scalar x) {
    v[0] = v[1] = v[2] = x;
  }
  ~Vec3DTemplate() {}

  Vec3DTemplate& operator=(const Scalar);
  Vec3DTemplate& operator=(const Vec3DTemplate&);
  Vec3DTemplate& operator+=(const Vec3DTemplate&);
  Vec3DTemplate& operator+=(const Scalar&);
  Vec3DTemplate& operator-=(const Vec3DTemplate&);
  Vec3DTemplate& operator-=(const Scalar&);
  Vec3DTemplate& operator*=(Scalar);
  Vec3DTemplate& operator/=(Scalar);
  Vec3DTemplate operator/(Scalar) const;
  Vec3DTemplate operator+(const Vec3DTemplate&) const;
  Vec3DTemplate operator-(const Vec3DTemplate&) const;
  Vec3DTemplate operator-() const;
  Vec3DTemplate operator^(const Vec3DTemplate&) const;
  Scalar operator*(const Vec3DTemplate&) const;

  operator Scalar *() {
    return v;
  }
  Scalar& operator[](int i) {
    return v[i];
  }
  Scalar operator[](int i) const {
    return v[i];
  }
  void print(const char *msg = "") {
    fprintf(stdout, "%s(%e %e %e)\n", msg, v[0], v[1], v[2]);
  }
  Scalar norm() const {
    return sqrt(v[0] * v[0] + v[1] * v[1] + v[2] * v[2]);
  }
  Scalar normsq() const {
    return (v[0] * v[0] + v[1] * v[1] + v[2] * v[2]);
  }
};

//------------------------------------------------------------------------------

template<class Scalar>
inline
Vec3DTemplate<Scalar>& Vec3DTemplate<Scalar>::operator=(const Scalar v2) {
  v[0] = v2;
  v[1] = v2;
  v[2] = v2;
  return *this;
}

//------------------------------------------------------------------------------

template<class Scalar>
inline
Vec3DTemplate<Scalar>& Vec3DTemplate<Scalar>::operator=(const Vec3DTemplate<Scalar>& v2) {
  v[0] = v2.v[0];
  v[1] = v2.v[1];
  v[2] = v2.v[2];
  return *this;
}

//------------------------------------------------------------------------------

template<class Scalar>
inline
Vec3DTemplate<Scalar>& Vec3DTemplate<Scalar>::operator+=(const Vec3DTemplate<Scalar>& v2) {
  v[0] += v2.v[0];
  v[1] += v2.v[1];
  v[2] += v2.v[2];
  return *this;
}

//------------------------------------------------------------------------------

template<class Scalar>
inline
Vec3DTemplate<Scalar>& Vec3DTemplate<Scalar>::operator+=(const Scalar& c) {
  v[0] += c;
  v[1] += c;
  v[2] += c;
  return *this;
}

//------------------------------------------------------------------------------

template<class Scalar>
inline
Vec3DTemplate<Scalar>& Vec3DTemplate<Scalar>::operator-=(const Vec3DTemplate<Scalar>& v2) {
  v[0] -= v2.v[0];
  v[1] -= v2.v[1];
  v[2] -= v2.v[2];
  return *this;
}

//------------------------------------------------------------------------------

template<class Scalar>
inline
Vec3DTemplate<Scalar>& Vec3DTemplate<Scalar>::operator-=(const Scalar& c) {
  v[0] -= c;
  v[1] -= c;
  v[2] -= c;
  return *this;
}

//------------------------------------------------------------------------------

template<class Scalar>
inline
Vec3DTemplate<Scalar>& Vec3DTemplate<Scalar>::operator*=(Scalar cst) {
  v[0] *= cst;
  v[1] *= cst;
  v[2] *= cst;
  return *this;
}

//------------------------------------------------------------------------------

template<class Scalar>
inline
Vec3DTemplate<Scalar>& Vec3DTemplate<Scalar>::operator/=(Scalar cst) {
  cst = 1.0 / cst;
  v[0] *= cst;
  v[1] *= cst;
  v[2] *= cst;
  return *this;
}

//------------------------------------------------------------------------------

template<class Scalar>
inline
Vec3DTemplate<Scalar> Vec3DTemplate<Scalar>::operator/(Scalar cst) const {
  Vec3DTemplate<Scalar> res;
  cst = 1.0 / cst;
  res.v[0] = v[0] * cst;
  res.v[1] = v[1] * cst;
  res.v[2] = v[2] * cst;
  return res;
}

//------------------------------------------------------------------------------

template<class Scalar>
inline
Vec3DTemplate<Scalar> Vec3DTemplate<Scalar>::operator+(const Vec3DTemplate<Scalar>& v2) const {
  Vec3DTemplate<Scalar> res;
  res.v[0] = v[0] + v2.v[0];
  res.v[1] = v[1] + v2.v[1];
  res.v[2] = v[2] + v2.v[2];
  return res;
}

//------------------------------------------------------------------------------

template<class Scalar>
inline
Vec3DTemplate<Scalar> Vec3DTemplate<Scalar>::operator-(const Vec3DTemplate<Scalar>& v2) const {
  Vec3DTemplate<Scalar> res;
  res.v[0] = v[0] - v2.v[0];
  res.v[1] = v[1] - v2.v[1];
  res.v[2] = v[2] - v2.v[2];
  return res;
}

//------------------------------------------------------------------------------

template<class Scalar>
inline
Vec3DTemplate<Scalar> Vec3DTemplate<Scalar>::operator-() const {
  Vec3DTemplate<Scalar> res;
  res.v[0] = -v[0];
  res.v[1] = -v[1];
  res.v[2] = -v[2];
  return res;
}

//------------------------------------------------------------------------------
// define vector cross product
template<class Scalar>
inline
Vec3DTemplate<Scalar> Vec3DTemplate<Scalar>::operator^(const Vec3DTemplate<Scalar>& v2) const {
  Vec3DTemplate<Scalar> res;
  res.v[0] = v[1] * v2.v[2] - v[2] * v2.v[1];
  res.v[1] = v[2] * v2.v[0] - v[0] * v2.v[2];
  res.v[2] = v[0] * v2.v[1] - v[1] * v2.v[0];
  return res;
}

//------------------------------------------------------------------------------
// define vector dot product
template<class Scalar>
inline
Scalar Vec3DTemplate<Scalar>::operator*(const Vec3DTemplate<Scalar>& v2) const {
  return v[0] * v2.v[0] + v[1] * v2.v[1] + v[2] * v2.v[2];
}

//------------------------------------------------------------------------------

template<class Scalar>
inline
Vec3DTemplate<Scalar> operator*(Scalar c, const Vec3DTemplate<Scalar>& v) {
  Vec3DTemplate<Scalar> res;
  res.v[0] = c * v.v[0];
  res.v[1] = c * v.v[1];
  res.v[2] = c * v.v[2];
  return res;
}

//------------------------------------------------------------------------------

template<class Scalar>
inline
Vec3DTemplate<Scalar> operator*(const Vec3DTemplate<Scalar>& v, Scalar c) {
  Vec3DTemplate<Scalar> res;
  res.v[0] = c * v.v[0];
  res.v[1] = c * v.v[1];
  res.v[2] = c * v.v[2];
  return res;
}

//------------------------------------------------------------------------------

template<class Scalar>
inline
Vec3DTemplate<Scalar> min(const Vec3DTemplate<Scalar>& a, const Vec3DTemplate<Scalar>& b) {
  return Vec3DTemplate<Scalar>(std::min(a[0], b[0]),
                               std::min(a[1], b[1]),
                               std::min(a[2], b[2]));
}

//------------------------------------------------------------------------------

template<class Scalar>
inline
Vec3DTemplate<Scalar> max(const Vec3DTemplate<Scalar>& a, const Vec3DTemplate<Scalar>& b) {
  return Vec3DTemplate<Scalar>(std::max(a[0], b[0]),
               std::max(a[1], b[1]),
               std::max(a[2], b[2]));
}

//------------------------------------------------------------------------------

#endif

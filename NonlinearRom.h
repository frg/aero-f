#ifndef _NONLINEAR_ROM_H_
#define _NONLINEAR_ROM_H_

#include <VectorSet.h>
#include <ParallelRom.h>

#include <vector>
#include <tuple>
#include <string>

class IoData;
class Domain;
class Communicator;
class Vec3D;

template<int dim> class ImplicitTsDesc;
template<class Scalar> class DistVec;
template<class Scalar, int dim> class DistSVec;
template<class Scalar> class DenseMat;
template<class Scalar> class Vec;
template<class VecType> class Clustering;
template<class MatrixType> class DistMat;
template<int dim> class DistEmbeddedBcData;
template<int dim> class EmbeddedPOD;
template<int dim> class NearestNeighborRom;


//----------- Class for handling I/O for the nonlinear ROM database ------------

class NonlinearRom {

  IoData& ioData;
  Domain *domain;
  Communicator *com;

 protected:
  const int precision = 12;
  const int width = 18;
  void determinePrefixName(const char *, const char *, char *&);
  void determineFileName(const char *, const char *, const char *, char *&);

 public:
  // State snapshot clusters, fast online projections, and distance comparisons
  char *statePrefix;
  char *stateSnapsName;
  char *mapName;
  char *indexName;
  char *connName;
  char *centersName;

  // State basis
  char *stateBasisName;
  char *quadStateBasisName;
  char *stateRefName;
  char *stateAvgName;
  char *stateSingValsName;
  char *quadStateBasisSingValsName;
  char *projErrorName;
  char *projReducedCoordsName;
  char *reducedCoordsNeuralNetName;
  char *rowClustersName;
  char *basisRowClustersName;
  char *quadStateBasisSizesName;

  // Residual basis (for Gappy POD based methods)
  char *residualPrefix;
  char *residualBasisName;
  char *residualSingValsName;

  // Reduced mesh
  char *reducedMeshPrefix;
  char *reducedMeshName;
  char *reducedMeshNodesName;
  char *sampleNodesName;
  char *sampleNodesFullName;
  char *gradientNodesName;
  char *gradientNodesFullName;
  char *sampleWeightsName;

  // Online and HROM (reduced* quantities are written in XPost format vs. binary counterparts)
  char *reducedWallDistName;
  char *reducedStateBasisName;
  char *reducedQuadStateBasisName;
  char *reducedStateRefName;
  char *reducedStateCentersName;
  char *reducedFluidIdName;
  char *reducedSnapshotsName;
  char *reducedGnatMatrixName;
  char *gnatMatrixName;

  void determinePath(char *, int, char *&);

  int determineBasisSize(char *, int, int, double, std::vector<double>&, std::vector<int> *);

 protected:
  int readSnapshotData(const char *, std::vector<std::string>&, std::vector<std::tuple<int, int, int, double>>&, int = -1);
  int readSurfaceSnapshotData(const char *, std::vector<std::string>&, std::vector<std::string>&, std::vector<std::tuple<int, int, int, double>>&, const char *);

 public:
  NonlinearRom(IoData&, Domain *);
  ~NonlinearRom();

  int getNPODVectors(const char *, bool, std::vector<double>&);
  int getNPODVectorsRow(const char *, bool, std::vector<double>&, std::vector<double>&, std::vector<int>&);
  void getPODConstants(const char *, bool, int&, int&, double&, std::vector<double> * = nullptr);
  void readInitialFIDs(std::vector<std::string>&);

  void createClusterDirs(int);
  void writeClusterIndex(int, const int *);
  void writeClusterMap(int, const int *, int **);
  void writeClusterConn(int, const int *, int **);

  void IOClusteredInfoASCII(bool, int, const char *,
                            std::vector<double> * = nullptr,
                            std::vector<std::vector<double>> * = nullptr,
                            std::vector<std::vector<std::vector<double>>> * = nullptr,
                            std::vector<std::vector<std::vector<std::vector<double>>>> * = nullptr);

  void readInputNodes(std::vector<long>&);

  void readInputWeights(std::vector<double>&);

  template<int dim>
  void readSnapshots(const char *, VecSet<DistSVec<double, dim>>&,
                     std::vector<std::pair<std::string, int>> * = nullptr,
                     std::vector<int> * = nullptr);

  template<int dim>
  void writeClusteredSnapshots(int, VecSet<DistSVec<double, dim>>&, int **, int *,
                               std::vector<std::pair<std::string, int>>&);

  template<int dim>
  void readClusteredSnapshots(int, int *, VecSet<DistSVec<double, dim>>&);

  template<int dim>
  void writeClusterCenters(int, int *, VecSet<DistSVec<double, dim>> *);

  template<int dim>
  void readClusterCenters(int, int *&, VecSet<DistSVec<double, dim>> *&);

  template<int dim>
  void referenceSnapshots(int, int, VecSet<DistSVec<double, dim>> *,
                          VecSet<DistSVec<double, dim>>&, DistSVec<double, dim>&);

  template<int dim>
  void readReferenceState(int, DistSVec<double, dim>&);

  template<int dim>
  void writeAverageState(DistSVec<double, dim>&);

  template<int dim>
  void readAverageState(DistSVec<double, dim>&);

  template<int dim>
  void writeBasis(int, const char *, VecSet<DistSVec<double, dim>>&,
                  std::vector<double>&, int, std::vector<int> * = nullptr, Domain * = nullptr);

  template<int dim>
  void readBasis(int, const char *, VecSet<DistSVec<double, dim>>&, bool = false,
                 std::vector<int> * = nullptr, bool = true);

  template<int dim>
  void readBasis(int, const char *, DistMat<DenseMat<double>>&, bool = false,
                 std::vector<int> * = nullptr);

  template<int dim>
  void readCachedOfflineQuantities(Clustering<DistSVec<double, dim>> *, bool = false);

  template<int dim>
  void generateHyperreductionTrainingData(ImplicitTsDesc<dim> *, Clustering<DistSVec<double, dim>> *,
                                          VecSet<DistSVec<double, dim>> *,
                                          std::vector<DenseMat<double>> *,
                                          NearestNeighborRom<dim> * = nullptr);

  void writeReducedMesh(std::vector<int>&, std::vector<double>&, std::vector<int>&,
                        std::vector<int>&, const int, const int, const int, const int, int = -1);

  void writeReducedVector(int, const char *, std::vector<int>&, int = 1);

  template<int dim>
  void writeReducedVector(int, const char *, std::vector<double>&, int = 1);

  void writeSampleWeights(std::vector<double>&, int = -1);

  void readReducedMeshNodes(std::vector<int>&, int = -1);

  void readSampleNodes(std::vector<int>&, std::vector<int>&, int = -1);

  void readSampleWeights(std::vector<double>&, int = -1);

  template<int dim>
  void readGnatMatrix(VecSet<DistSVec<double, dim>>&);

  template<int dim>
  void readMultiSol(VecSet<DistSVec<double, dim>> *, std::vector<std::vector<double>> *);

  int readSurfaceSnapshots(const char *, std::vector<VecSet<Vec<Vec3D>> *>&, std::vector<std::string>&, const char * = nullptr, int = 0);

  int readAppendBinary(std::string);

  void readRowClusterAssignments(int, DistVec<double>&);

  void writeBasisRowClusterAssignments(int, std::vector<int>&);

  void readBasisRowClusterAssignments(int, std::vector<int>&);

  void writeSnapshotInnerProducts(DenseMat<double>&);

  void readSnapshotInnerProducts(DenseMat<double>&);

  void writeNearestNeighborICInfo(const Vec<double>&, const std::vector<int>&);

  void readNearestNeighborICInfo(Vec<double>&, std::vector<int>&);

  void writeMultiVecASCII(char *, std::vector<double> * = nullptr,
                          std::vector<std::vector<double>> * = nullptr,
                          std::vector<std::vector<std::vector<double>>> * = nullptr,
                          std::vector<std::vector<std::vector<std::vector<double>>>> * = nullptr);
  void readMultiVecASCII(char *, std::vector<double> * = nullptr,
                         std::vector<std::vector<double>> * = nullptr,
                         std::vector<std::vector<std::vector<double>>> * = nullptr,
                         std::vector<std::vector<std::vector<std::vector<double>>>> * = nullptr);

  template<int dim>
  void computeQuadraticGrad(Vec<double>&, VecSet<DistSVec<double, dim>>&,
                            VecSet<DistSVec<double, dim>>&,
                            VecSet<DistSVec<double, dim>>&);

  template<int dim>
  void optimizeQuadProjection(Vec<double>&, DistSVec<double, dim>&, Clustering<DistSVec<double, dim>> *, int);

  template<int dim>
  void optimizeQuadProjectionFast(Vec<double>&, DistSVec<double, dim>&, Clustering<DistSVec<double, dim>> *, int, bool = true);

  template<int dim>
  void optimizeNNAugmentedProjectionFast(Vec<double>&, DistSVec<double, dim>&, Clustering<DistSVec<double, dim>> *, int, bool = true);
};

//------------------------------------------------------------------------------

#endif

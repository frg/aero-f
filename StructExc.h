#ifndef _STRUCT_EXC_H_
#define _STRUCT_EXC_H_

class IoData;
class GeoSource;
class MatchNodeSet;
class Domain;
class Communicator;
struct Vec3D;

template<class Scalar> class DistVec;
template<class Scalar, int dim> class DistSVec;
template<class Scalar> class Vec;

//------------------------------------------------------------------------------

class StructExc {

  int algNum;
  int rstrt;
  int smode;
  int sendInitialForce;
  double dt;
  double tmax;

  double oolscale;
  double ootscale;
  double oovscale;
  double ootempscale;
  double fscale;
  double pscale;

  int numLocSub;
  int numStrCPU;
  int (*numStrNodes)[2];

  int recParity;
  int sndParity;
  int bufsize;
  double *buffer;

  MatchNodeSet **matchNodes;

  Communicator *com;
  Communicator *strCom;

 public:

  StructExc(IoData&, MatchNodeSet **, int, Communicator *, Communicator *, int);
  ~StructExc();
  void updateMNS(MatchNodeSet **mns) {
    matchNodes = mns;
  }
  void updateNumStrNodes(int nn);

  void negotiate();
  void negotiateStopping(bool *);
  void getNumParam(int&, int&, double&);
  void sendNumParam(int);
  void getRelResidual(double&);
  double getInfo();
  void getEmbeddedWetSurfaceInfo(int&, bool&, int&, int&);
  void getEmbeddedWetSurface(int, Vec3D *, int, int *, int = 3);
  void getDisplacement(DistSVec<double, 3>&, DistSVec<double, 3>&,
                       DistSVec<double, 3>&, DistSVec<double, 3>&);
  void getDisplacementSensitivity(DistSVec<double, 3>&, DistSVec<double, 3>&, bool = false);
  void getTotDisplacement(DistSVec<double, 3>&, DistSVec<double, 3>&);
  int getSubcyclingInfo();
  void getTemperature(DistVec<double>&);
  void sendForce(DistSVec<double, 3>&, bool = true);
  void getMdFreq(int&, double *&);
  void getMdStrDisp(int, DistSVec<double, 3>&, DistSVec<double, 3>&, DistSVec<double, 3>&);
  void sendHeatPower(DistVec<double>&);
  void sendFluidSuggestedTimestep(double);

  int getAlgorithmNumber() const {
    return algNum;
  }
  int getRestartFrequency() const {
    return rstrt;
  }
  int getSendInitialForce() const {
    return sendInitialForce;
  }
  double getTimeStep() const {
    return dt;
  }
  double getMaxTime() const {
    return tmax;
  }

  // for cracking
  void getInitialCrackingSetup(int&, int&);
  bool getNewCrackingStats(int&, int&, int&);
  void getNewCracking(int, int, int *, double *, int *, int *, int);
  void getInitialPhantomNodes(int, Vec<Vec3D>&, int);
};

//------------------------------------------------------------------------------

#endif

#ifndef _K_EPSILON_TERM_H_
#define _K_EPSILON_TERM_H_

#include <IoData.h>
#include <ThermalCondFcn.h>

#include <Eigen/Core>
#include <unsupported/Eigen/AutoDiff>

class VarFcn;
class ViscoFcn;

//------------------------------------------------------------------------------
/*
@ARTICLE{jones-launder-72,
  author = "Jones, W. P. and Launder, B. E.",
  title = "The Prediction of Laminarization with a Two-Equation
           Turbulence Model",
  journal = "International Journal of Heat and Mass Transfer",
  year = 1972,
  volume = 15,
  pages = "301--314",
}
*/
class KEpsilonTerm {

  static constexpr double fourth = 1.0 / 4.0;

  double ooreynolds_mu;
  double reynolds_mu;
  double dRedMach;

  double sigma_k;
  double sigma_eps;
  double sigma_eps1;
  double sigma_eps2;
  double c_mu;

  ConstantPrandtlThermalCondFcn *turbThermalCondFcn;
  VarFcn *varFcn;

 public:
  KEpsilonTerm(IoData&, ViscoFcn *, VarFcn *);
  ~KEpsilonTerm();

  void rstVar(IoData&);

  ConstantPrandtlThermalCondFcn *getTurbThermalCondFcn() {
    return turbThermalCondFcn;
  }

  void computeRhoKEps(double *, double&, double&);

  void computeDerivativeOfRhoKEps(double *, double *, double&, double&);

  void computeRhoKEps(double *[4], double&, double&);

  void computeDerivativeOfRhoKEps(double *[4], double *[4], double&, double&);

  void computeRhoKEpsDerivativeOperators(double *[4], double[4][7], double[4][7]);

  void computeJacobianRhoKEps(double *[4], double[4][7], double[4][7]);

  void computeTurbulentTransportCoefficients(bool, double, double, int, double&, double&, double&);

  void computeDerivativeOfTurbulentTransportCoefficients(bool, double, double, double, double, int,
                                                         double, double&, double&, double&);

  void computeTurbulentTransportCoefficientsDerivativeOperators(bool, double, double, int, double,
                                                                double&, double&, double&, double&,
                                                                double&, double&, double&, double&,
                                                                double&);
  double computeTurbulentViscosity(double, double);

  double computeDerivativeOfTurbulentViscosity(double, double, double, double, double);

  void computeTurbulentViscosityDerivativeOperators(double, double, double&, double&, double&);

  double computeSecondTurbulentViscosity(double);

  double computeDerivativeOfSecondTurbulentViscosity(double);

  void computeSecondTurbulentViscosityDerivativeOperators(double, double&);

  void computeVolumeTermKE(double[4][3], double[3][3], double, double, double, double,
                           double *[4], double (*)[7], double *);

  void computeDerivativeOfVolumeTermKE(double[4][3], double[4][3], double[3][3], double[3][3], double,
                                       double, double, double, double, double, double, double, double *[4],
                                       double *[4], double, double (*)[7], double *);

  void computeDerivativeOperatorsOfVolumeTermKE(double[4][3], double[3][3], double, double, double, double, double *[4],
                                                double[3][4][3], double[3], double[3], double[3],
                                                double[3][4][3], double[3], double[3], double[3],
                                                double[3][3], double&, double&, double[3][3], double&, double&);

  template<int neq, int shift>
  void computeJacobianVolumeTermKE(double[4][3], double[4][3], double, double, double, double,
                                   double *[4], double (*)[3][neq][neq], double (*)[neq][neq]);

  void computeJacobianVolumeTermKE(double[4][3], double[4][3], double, double[4][7], double, double[4][7],
                                   double, double[4][7], double, double[4][7], double *[4], int,
                                   double (*)[3][7][7], double (*)[7][7]);

};

//------------------------------------------------------------------------------

inline
KEpsilonTerm::KEpsilonTerm(IoData& iod, ViscoFcn *visf, VarFcn *vfn) {
  ooreynolds_mu = 1.0 / iod.ref.reynolds_mu;
  reynolds_mu = iod.ref.reynolds_mu;
  dRedMach = iod.ref.dRe_mudMach;
  sigma_k = iod.eqs.tc.tm.ke.sigma_k;
  sigma_eps = iod.eqs.tc.tm.ke.sigma_eps;
  sigma_eps1 = iod.eqs.tc.tm.ke.sigma_eps1;
  sigma_eps2 = iod.eqs.tc.tm.ke.sigma_eps2;
  c_mu = iod.eqs.tc.tm.ke.c_mu;
  turbThermalCondFcn = new ConstantPrandtlThermalCondFcn(iod, visf, vfn);
  varFcn = vfn;
}

//------------------------------------------------------------------------------

inline
KEpsilonTerm::~KEpsilonTerm() {
  if(turbThermalCondFcn) {
    delete turbThermalCondFcn;
  }
}

//------------------------------------------------------------------------------

inline
void KEpsilonTerm::rstVar(IoData& iod) {
  // reset anything that may depend on any sensitivity analysis variable
  ooreynolds_mu = 1.0 / iod.ref.reynolds_mu;
  reynolds_mu = iod.ref.reynolds_mu;
  dRedMach = iod.ref.dRe_mudMach;
}

//------------------------------------------------------------------------------

inline
void KEpsilonTerm::computeRhoKEps(double *V, double& rhok, double& rhoeps) {
  rhok = V[0] * V[5];
  rhoeps = V[0] * V[6];
}

//------------------------------------------------------------------------------

inline
void KEpsilonTerm::computeDerivativeOfRhoKEps(double *V, double *dV, double& drhok, double& drhoeps) {
  drhok = dV[0] * V[5] + V[0] * dV[5];
  drhoeps = dV[0] * V[6] + V[0] * dV[6];
}

//------------------------------------------------------------------------------

inline
void KEpsilonTerm::computeRhoKEps(double *V[4], double& rhok, double& rhoeps) {
  rhok = fourth * (V[0][0] * V[0][5] + V[1][0] * V[1][5] +
                   V[2][0] * V[2][5] + V[3][0] * V[3][5]);
  rhoeps = fourth * (V[0][0] * V[0][6] + V[1][0] * V[1][6] +
                     V[2][0] * V[2][6] + V[3][0] * V[3][6]);
}

//------------------------------------------------------------------------------

inline
void KEpsilonTerm::computeDerivativeOfRhoKEps(double *V[4], double *dV[4], double& drhok, double& drhoeps) {
  drhok = fourth * (dV[0][0] * V[0][5] + V[0][0] * dV[0][5] + dV[1][0] * V[1][5] + V[1][0] * dV[1][5] +
                    dV[2][0] * V[2][5] + V[2][0] * dV[2][5] + dV[3][0] * V[3][5] + V[3][0] * dV[3][5]);
  drhoeps = fourth * (dV[0][0] * V[0][6] + V[0][0] * dV[0][6] + dV[1][0] * V[1][6] + V[1][0] * dV[1][6] +
                      dV[2][0] * V[2][6] + V[2][0] * dV[2][6] + dV[3][0] * V[3][6] + V[3][0] * dV[3][6]);
}

//------------------------------------------------------------------------------

inline
void KEpsilonTerm::computeRhoKEpsDerivativeOperators(double *V[4], double drhokdV[4][7], double drhoepsdV[4][7]) {
  for(int i = 0; i < 4; ++i) {
    drhokdV[i][0] = fourth * V[i][5];
    drhokdV[i][5] = fourth * V[i][0];
    drhoepsdV[i][0] = fourth * V[i][6];
    drhoepsdV[i][6] = fourth * V[i][0];
  }
}

//------------------------------------------------------------------------------

inline
void KEpsilonTerm::computeJacobianRhoKEps(double *V[4], double drhok[4][7], double drhoeps[4][7]) {
  for(int k = 0; k < 4; ++k) {
    drhok[k][0] = 0.0;  drhoeps[k][0] = 0.0;
    drhok[k][1] = 0.0;  drhoeps[k][1] = 0.0;
    drhok[k][2] = 0.0;  drhoeps[k][2] = 0.0;
    drhok[k][3] = 0.0;  drhoeps[k][3] = 0.0;
    drhok[k][4] = 0.0;  drhoeps[k][4] = 0.0;
    drhok[k][5] = 0.25; drhoeps[k][5] = 0.0;
    drhok[k][6] = 0.0;  drhoeps[k][6] = 0.25;
  }
}

//------------------------------------------------------------------------------

inline
void KEpsilonTerm::computeTurbulentTransportCoefficients(bool inTrip, double rhok, double rhoeps, int tag,
                                                         double& mut, double& lambdat, double& kappat) {
  // Applying the laminar-turbulent trip when necessary
  if(inTrip) {
    mut = computeTurbulentViscosity(rhok, rhoeps);
    lambdat = computeSecondTurbulentViscosity(mut);
    kappat  = turbThermalCondFcn->turbulentConductivity(mut, tag);
  }
  else {
    mut = lambdat = kappat = 0.0;
  }
}

//------------------------------------------------------------------------------

inline
void KEpsilonTerm::computeDerivativeOfTurbulentTransportCoefficients(bool inTrip, double rhok, double drhok,
                                                                     double rhoeps, double drhoeps, int tag, double dMach,
                                                                     double& dmut, double& dlambdat, double& dkappat) {
  // Applying the laminar-turbulent trip when necessary
  if(inTrip) {
    dmut = computeDerivativeOfTurbulentViscosity(rhok, drhok, rhoeps, drhoeps, dMach);
    dlambdat = computeDerivativeOfSecondTurbulentViscosity(dmut);
    dkappat = turbThermalCondFcn->turbulentConductivityDerivative(dmut, tag);
  }
  else {
    dmut = dlambdat = dkappat = 0.;
  }
}

//------------------------------------------------------------------------------

inline
void KEpsilonTerm::computeTurbulentTransportCoefficientsDerivativeOperators(bool inTrip, double rhok, double rhoeps, int tag,
                                                                            double mut, double& dmutdrhok, double& dmutdrhoeps,
                                                                            double& dmutdMach, double& dlambdatdrhok, double& dlambdatdrhoeps,
                                                                            double& dlambdatdMach, double& dkappatdrhok, double& dkappatdrhoeps,
                                                                            double& dkappatdMach) {
  // Applying the laminar-turbulent trip when necessary
  if(inTrip) {
    computeTurbulentViscosityDerivativeOperators(rhok, rhoeps, dmutdrhok, dmutdrhoeps, dmutdMach);
    double dlambdatdmut;
    computeSecondTurbulentViscosityDerivativeOperators(mut, dlambdatdmut);
    dlambdatdrhok = dlambdatdmut * dmutdrhok;
    dlambdatdrhoeps = dlambdatdmut * dmutdrhoeps;
    dlambdatdMach = dlambdatdmut * dmutdMach;
    double dkappatdmut;
    turbThermalCondFcn->turbulentConductivityDerivativeOperators(mut, tag, dkappatdmut);
    dkappatdrhok = dkappatdmut * dmutdrhok;
    dkappatdrhoeps = dkappatdmut * dmutdrhoeps;
    dkappatdMach = dkappatdmut * dmutdMach;
  }
  else {
    dmutdrhok = dmutdrhoeps = dmutdMach = 0;
    dlambdatdrhok = dlambdatdrhoeps = dlambdatdMach = 0;
    dkappatdrhok = dkappatdrhoeps = dkappatdMach = 0;
  }
}

//------------------------------------------------------------------------------

inline
double KEpsilonTerm::computeTurbulentViscosity(double rhok, double rhoeps) {
  return reynolds_mu * c_mu * rhok * rhok / rhoeps;
}

//------------------------------------------------------------------------------

inline
double KEpsilonTerm::computeDerivativeOfTurbulentViscosity(double rhok, double drhok, double rhoeps, double drhoeps, double dMach) {
  return ((dRedMach * dMach * c_mu * rhok * rhok + reynolds_mu * c_mu *
           2.0 * rhok * drhok) * rhoeps - reynolds_mu * c_mu * rhok * rhok * drhoeps) / (rhoeps * rhoeps);
}

//------------------------------------------------------------------------------

inline
void KEpsilonTerm::computeTurbulentViscosityDerivativeOperators(double rhok, double rhoeps, double& dmutdrhok, double& dmutdrhoeps, double& dmutdMach) {
  dmutdrhok = 2 * reynolds_mu * c_mu * rhok / rhoeps;
  dmutdrhoeps = -reynolds_mu * c_mu * rhok * rhok / (rhoeps * rhoeps);
  dmutdMach = dRedMach * c_mu * rhok * rhok / rhoeps;
}

//------------------------------------------------------------------------------

inline
double KEpsilonTerm::computeSecondTurbulentViscosity(double mut) {
  // simple model that remains true when the Stokes' hypothesis is assumed
  return -2.0 * mut / 3.0;
}

//------------------------------------------------------------------------------

inline
double KEpsilonTerm::computeDerivativeOfSecondTurbulentViscosity(double dmut) {
  return -2.0 * dmut / 3.0;
}

//------------------------------------------------------------------------------

inline
void KEpsilonTerm::computeSecondTurbulentViscosityDerivativeOperators(double mut, double& dlambdatdmut) {
  dlambdatdmut = -2.0 / 3.0;
}

//------------------------------------------------------------------------------

inline
void KEpsilonTerm::computeVolumeTermKE(double dp1dxj[4][3], double dudxj[3][3],
                                       double mul, double mut, double rhok, double rhoeps,
                                       double *V[4], double (*R)[7], double *S) {
  double muk = ooreynolds_mu * (mul + sigma_k * mut);
  double mueps = ooreynolds_mu * (mul + sigma_eps * mut);
  R[0][5] = muk * (dp1dxj[0][0] * V[0][5] + dp1dxj[1][0] * V[1][5] +
                   dp1dxj[2][0] * V[2][5] + dp1dxj[3][0] * V[3][5]);
  R[0][6] = mueps * (dp1dxj[0][0] * V[0][6] + dp1dxj[1][0] * V[1][6] +
                     dp1dxj[2][0] * V[2][6] + dp1dxj[3][0] * V[3][6]);
  R[1][5] = muk * (dp1dxj[0][1] * V[0][5] + dp1dxj[1][1] * V[1][5] +
                   dp1dxj[2][1] * V[2][5] + dp1dxj[3][1] * V[3][5]);
  R[1][6] = mueps * (dp1dxj[0][1] * V[0][6] + dp1dxj[1][1] * V[1][6] +
                     dp1dxj[2][1] * V[2][6] + dp1dxj[3][1] * V[3][6]);
  R[2][5] = muk * (dp1dxj[0][2] * V[0][5] + dp1dxj[1][2] * V[1][5] +
                   dp1dxj[2][2] * V[2][5] + dp1dxj[3][2] * V[3][5]);
  R[2][6] = mueps * (dp1dxj[0][2] * V[0][6] + dp1dxj[1][2] * V[1][6] +
                     dp1dxj[2][2] * V[2][6] + dp1dxj[3][2] * V[3][6]);
  double div  = dudxj[0][0] + dudxj[1][1] + dudxj[2][2];
  double div2 = dudxj[0][0] * dudxj[0][0] + dudxj[1][1] * dudxj[1][1] + dudxj[2][2] * dudxj[2][2];
  double a = dudxj[0][1] + dudxj[1][0];
  double b = dudxj[0][2] + dudxj[2][0];
  double c = dudxj[1][2] + dudxj[2][1];
  double prod = ooreynolds_mu * mut * (2.0 * div2 - 2.0 / 3.0 * div * div + a * a + b * b + c * c)
                - 2.0 / 3.0 * rhok * div;
  S[0] = 0.0;
  S[1] = 0.0;
  S[2] = 0.0;
  S[3] = 0.0;
  S[4] = 0.0;
  S[5] = - rhoeps + prod;
  S[6] = (sigma_eps1 * rhoeps * prod - sigma_eps2 * rhoeps * rhoeps) / rhok;
}

//------------------------------------------------------------------------------

inline
void KEpsilonTerm::computeDerivativeOfVolumeTermKE(double _dp1dxj[4][3], double ddp1dxj[4][3],
                                                   double _dudxj[3][3], double ddudxj[3][3],
                                                   double _rhok, double drhok, double _rhoeps,
                                                   double drhoeps, double _mul, double dmul,
                                                   double _mut, double dmut, double *_V[4],
                                                   double *dV[4], double dMach, double (*dR)[7],
                                                   double *dS) {
  typedef Eigen::AutoDiffScalar<Eigen::Matrix<double, 1, 1>> Scalar;
  Scalar dp1dxj[4][3], dudxj[3][3], rhok, rhoeps, mul, mut, *V[4], ooreynolds_mu;
  for(int i=0; i<4; ++i) for(int j=0; j<3; ++j) { dp1dxj[i][j].value() = _dp1dxj[i][j]; dp1dxj[i][j].derivatives()[0] = ddp1dxj[i][j]; }
  for(int i=0; i<3; ++i) for(int j=0; j<3; ++j) { dudxj[i][j].value()  = _dudxj[i][j];  dudxj[i][j].derivatives()[0]  = ddudxj[i][j]; }
  rhok.value() = _rhok; rhok.derivatives()[0] = drhok;
  rhoeps.value() = _rhoeps; rhoeps.derivatives()[0] = drhoeps;
  mul.value() = _mul; mul.derivatives()[0] = dmul;
  mut.value() = _mut; mut.derivatives()[0] = dmut;
  for(int i=0; i<4; ++i) { V[i] = new Scalar[7]; for(int j=0; j<7; ++j) { V[i][j].value() = _V[i][j]; V[i][j].derivatives()[0] = dV[i][j]; }}
  ooreynolds_mu.value() = 1/reynolds_mu; ooreynolds_mu.derivatives()[0] = -dMach * dRedMach / (reynolds_mu * reynolds_mu);

  Scalar muk = ooreynolds_mu * (mul + sigma_k * mut);
  Scalar mueps = ooreynolds_mu * (mul + sigma_eps * mut);
  dR[0][5] = (muk * (dp1dxj[0][0] * V[0][5] + dp1dxj[1][0] * V[1][5] +
                     dp1dxj[2][0] * V[2][5] + dp1dxj[3][0] * V[3][5])).derivatives()[0];
  dR[0][6] = (mueps * (dp1dxj[0][0] * V[0][6] + dp1dxj[1][0] * V[1][6] +
                       dp1dxj[2][0] * V[2][6] + dp1dxj[3][0] * V[3][6])).derivatives()[0];
  dR[1][5] = (muk * (dp1dxj[0][1] * V[0][5] + dp1dxj[1][1] * V[1][5] +
                     dp1dxj[2][1] * V[2][5] + dp1dxj[3][1] * V[3][5])).derivatives()[0];
  dR[1][6] = (mueps * (dp1dxj[0][1] * V[0][6] + dp1dxj[1][1] * V[1][6] +
                       dp1dxj[2][1] * V[2][6] + dp1dxj[3][1] * V[3][6])).derivatives()[0];
  dR[2][5] = (muk * (dp1dxj[0][2] * V[0][5] + dp1dxj[1][2] * V[1][5] +
                     dp1dxj[2][2] * V[2][5] + dp1dxj[3][2] * V[3][5])).derivatives()[0];
  dR[2][6] = (mueps * (dp1dxj[0][2] * V[0][6] + dp1dxj[1][2] * V[1][6] +
                       dp1dxj[2][2] * V[2][6] + dp1dxj[3][2] * V[3][6])).derivatives()[0];
  Scalar div  = dudxj[0][0] + dudxj[1][1] + dudxj[2][2];
  Scalar div2 = dudxj[0][0] * dudxj[0][0] + dudxj[1][1] * dudxj[1][1] + dudxj[2][2] * dudxj[2][2];
  Scalar a = dudxj[0][1] + dudxj[1][0];
  Scalar b = dudxj[0][2] + dudxj[2][0];
  Scalar c = dudxj[1][2] + dudxj[2][1];
  Scalar prod = ooreynolds_mu * mut * (2.0 * div2 - 2.0 / 3.0 * div * div + a * a + b * b + c * c)
                - 2.0 / 3.0 * rhok * div;
  dS[0] = 0;
  dS[1] = 0;
  dS[2] = 0;
  dS[3] = 0;
  dS[4] = 0;
  dS[5] = (-rhoeps + prod).derivatives()[0];
  dS[6] = ((sigma_eps1 * rhoeps * prod - sigma_eps2 * rhoeps * rhoeps) / rhok).derivatives()[0];
}

//------------------------------------------------------------------------------

inline
void KEpsilonTerm::computeDerivativeOperatorsOfVolumeTermKE(double _dp1dxj[4][3], double _dudxj[3][3], double rhok, double rhoeps,
                                                            double _mul, double _mut, double *V[4],
                                                            double dr5ddp1dxj[3][4][3], double dr5dmul[3], double dr5dmut[3], double dr5dMach[3],
                                                            double dr6ddp1dxj[3][4][3], double dr6dmul[3], double dr6dmut[3], double dr6dMach[3],
                                                            double ds5ddudkj[3][3], double& ds5dmut, double& ds5dMach,
                                                            double ds6ddudkj[3][3], double& ds6dmut, double& ds6dMach) {
  // variable         : dp1dxj[4][3], dudxj[3][3], mul, mut, Mach
  // derivatives index: 0-11,         12-20,       21,  22,  23
  typedef Eigen::AutoDiffScalar<Eigen::Matrix<double, 1, 24>> Scalar;

  Scalar dp1dxj[4][3], dudxj[3][3], mul, mut, ooreynolds_mu;
  for(int i=0; i<4; ++i) for(int j=0; j<3; ++j) { dp1dxj[i][j].value() = _dp1dxj[i][j]; dp1dxj[i][j].derivatives().setZero(); dp1dxj[i][j].derivatives()[3*i+j] = 1; }
  for(int i=0; i<3; ++i) for(int j=0; j<3; ++j) { dudxj[i][j].value()  = _dudxj[i][j];  dudxj[i][j].derivatives().setZero();  dudxj[i][j].derivatives()[12+3*i+j] = 1; }
  mul.value() = _mul; mul.derivatives().setZero(); mul.derivatives()[21] = 1;
  mut.value() = _mut; mut.derivatives().setZero(); mut.derivatives()[22] = 1;
  ooreynolds_mu.value() = 1/reynolds_mu; ooreynolds_mu.derivatives().setZero(); ooreynolds_mu.derivatives()[23] = -dRedMach / (reynolds_mu * reynolds_mu);

  Scalar muk = ooreynolds_mu * (mul + sigma_k * mut);
  Scalar mueps = ooreynolds_mu * (mul + sigma_eps * mut);
  Scalar R05 = muk * (dp1dxj[0][0] * V[0][5] + dp1dxj[1][0] * V[1][5] +
                      dp1dxj[2][0] * V[2][5] + dp1dxj[3][0] * V[3][5]);
  Scalar R06 = mueps * (dp1dxj[0][0] * V[0][6] + dp1dxj[1][0] * V[1][6] +
                        dp1dxj[2][0] * V[2][6] + dp1dxj[3][0] * V[3][6]);
  Scalar R15 = muk * (dp1dxj[0][1] * V[0][5] + dp1dxj[1][1] * V[1][5] +
                      dp1dxj[2][1] * V[2][5] + dp1dxj[3][1] * V[3][5]);
  Scalar R16 = mueps * (dp1dxj[0][1] * V[0][6] + dp1dxj[1][1] * V[1][6] +
                        dp1dxj[2][1] * V[2][6] + dp1dxj[3][1] * V[3][6]);
  Scalar R25 = muk * (dp1dxj[0][2] * V[0][5] + dp1dxj[1][2] * V[1][5] +
                      dp1dxj[2][2] * V[2][5] + dp1dxj[3][2] * V[3][5]);
  Scalar R26 = mueps * (dp1dxj[0][2] * V[0][6] + dp1dxj[1][2] * V[1][6] +
                        dp1dxj[2][2] * V[2][6] + dp1dxj[3][2] * V[3][6]);
  Scalar div  = dudxj[0][0] + dudxj[1][1] + dudxj[2][2];
  Scalar div2 = dudxj[0][0] * dudxj[0][0] + dudxj[1][1] * dudxj[1][1] + dudxj[2][2] * dudxj[2][2];
  Scalar a = dudxj[0][1] + dudxj[1][0];
  Scalar b = dudxj[0][2] + dudxj[2][0];
  Scalar c = dudxj[1][2] + dudxj[2][1];
  Scalar prod = ooreynolds_mu * mut * (2.0 * div2 - 2.0 / 3.0 * div * div + a * a + b * b + c * c)
                - 2.0 / 3.0 * rhok * div;
  Scalar S5 = - rhoeps + prod;
  Scalar S6 = (sigma_eps1 * rhoeps * prod - sigma_eps2 * rhoeps * rhoeps) / rhok;

  for(int i = 0; i < 4; ++i) {
    for(int j = 0; j < 3; ++j) {
      dr5ddp1dxj[0][i][j] = R05.derivatives()[3*i+j];
      dr5ddp1dxj[1][i][j] = R15.derivatives()[3*i+j];
      dr5ddp1dxj[2][i][j] = R25.derivatives()[3*i+j];
      dr6ddp1dxj[0][i][j] = R06.derivatives()[3*i+j];
      dr6ddp1dxj[1][i][j] = R16.derivatives()[3*i+j];
      dr6ddp1dxj[2][i][j] = R26.derivatives()[3*i+j];
    }
  }
  dr5dmul[0]  = R05.derivatives()[21]; dr5dmul[1]  = R15.derivatives()[21]; dr5dmul[2]  = R25.derivatives()[21];
  dr6dmul[0]  = R06.derivatives()[21]; dr6dmul[1]  = R16.derivatives()[21]; dr6dmul[2]  = R26.derivatives()[21];
  dr5dmut[0]  = R05.derivatives()[22]; dr5dmut[1]  = R15.derivatives()[22]; dr5dmut[2]  = R25.derivatives()[22];
  dr6dmut[0]  = R06.derivatives()[22]; dr6dmut[1]  = R16.derivatives()[22]; dr6dmut[2]  = R26.derivatives()[22];
  dr5dMach[0] = R05.derivatives()[23]; dr5dMach[1] = R15.derivatives()[23]; dr5dMach[2] = R25.derivatives()[23];
  dr6dMach[0] = R06.derivatives()[23]; dr6dMach[1] = R16.derivatives()[23]; dr6dMach[2] = R26.derivatives()[23];

  for(int i = 0; i < 3; ++i) {
    for(int j = 0; j < 3; ++j) {
      ds5ddudkj[i][j] = S5.derivatives()[12+3*i+j];
      ds6ddudkj[i][j] = S6.derivatives()[12+3*i+j];
    }
  }
  ds5dmut  = S5.derivatives()[22];
  ds5dMach = S5.derivatives()[23];
  ds6dmut  = S6.derivatives()[22];
  ds6dMach = S6.derivatives()[23];
}

//------------------------------------------------------------------------------

template<int neq, int shift>
inline
void KEpsilonTerm::computeJacobianVolumeTermKE(double dp1dxj[4][3], double dudxj[4][3], double mul,
                                               double mut, double rhok, double rhoeps,
                                               double *V[4], double (*dRdU)[3][neq][neq],
                                               double (*dSdU)[neq][neq]) {
  // note: the Jacobian computed in this function is approximate (derivatives of mut w.r.t. rhok/rhoeps
  //       and also derivatives of prod w.r.t. rhou are neglected)
  for(int i = 0; i < 4; ++i) {
    for(int j = 0; j < neq; ++j) {
      for(int k = 0; k < neq; ++k) {
        dSdU[i][j][k] = 0;
      }
    }
  }

  double muk = ooreynolds_mu * (mul + sigma_k * mut);
  double mueps = ooreynolds_mu * (mul + sigma_eps * mut);
  for(int k = 0; k < 4; ++k) {
    double oorho = 1.0 / V[k][0];
    double nuk = muk * oorho;
    double nueps = mueps * oorho;
    dRdU[k][0][0 + shift][0 + shift] = nuk * dp1dxj[k][0];
    dRdU[k][0][0 + shift][1 + shift] = 0.0;
    dRdU[k][0][1 + shift][0 + shift] = 0.0;
    dRdU[k][0][1 + shift][1 + shift] = nueps * dp1dxj[k][0];
    dRdU[k][1][0 + shift][0 + shift] = nuk * dp1dxj[k][1];
    dRdU[k][1][0 + shift][1 + shift] = 0.0;
    dRdU[k][1][1 + shift][0 + shift] = 0.0;
    dRdU[k][1][1 + shift][1 + shift] = nueps * dp1dxj[k][1];
    dRdU[k][2][0 + shift][0 + shift] = nuk * dp1dxj[k][2];
    dRdU[k][2][0 + shift][1 + shift] = 0.0;
    dRdU[k][2][1 + shift][0 + shift] = 0.0;
    dRdU[k][2][1 + shift][1 + shift] = nueps * dp1dxj[k][2];
  }

  double div  = dudxj[0][0] + dudxj[1][1] + dudxj[2][2];
  double div2 = dudxj[0][0] * dudxj[0][0] + dudxj[1][1] * dudxj[1][1] + dudxj[2][2] * dudxj[2][2];
  double a = dudxj[0][1] + dudxj[1][0];
  double b = dudxj[0][2] + dudxj[2][0];
  double c = dudxj[1][2] + dudxj[2][1];
  double prod = ooreynolds_mu * mut * (2.0 * div2 - 2.0 / 3.0 * div * div + a * a + b * b + c * c)
                - 2.0 / 3.0 * rhok * div;

  double s00 = (-2.0 / 3.0) * div;
  double s01 = -1.0;
  double s10 = -(sigma_eps1 * rhoeps * prod - sigma_eps2 * rhoeps * rhoeps) / rhok / rhok +
                 sigma_eps1 * rhoeps * s00 / rhok;
  double s11 = (sigma_eps1 * prod - 2.0 * sigma_eps2 * rhoeps) / rhok;
  s00 *= fourth;
  s01 *= fourth;
  s10 *= fourth;
  s11 *= fourth;
  for(int k = 0; k < 4; ++k) {
    dSdU[k][0 + shift][0 + shift] = s00;
    dSdU[k][0 + shift][1 + shift] = s01;
    dSdU[k][1 + shift][0 + shift] = s10;
    dSdU[k][1 + shift][1 + shift] = s11;
  }
}

//------------------------------------------------------------------------------

inline
void KEpsilonTerm::computeJacobianVolumeTermKE(double dp1dxj[4][3], double _dudxj[4][3], double _mul,
                                               double dmul[4][7], double _mut, double dmut[4][7],
                                               double _rhok, double drhok[4][7], double _rhoeps,
                                               double drhoeps[4][7], double *_V[4], int tag,
                                               double (*dRdU)[3][7][7], double (*dSdU)[7][7]) {
  typedef Eigen::AutoDiffScalar<Eigen::Matrix<double, 28, 1>> Scalar;

  Scalar V[4][7];
  double dVdU[49] = {0};
  for(int i = 0; i < 4; ++i) {
    varFcn->getVarFcnBase(tag)->computedVdU(_V[i], dVdU);
    for(int j = 0; j < 7; ++j) {
      V[i][j].value() = _V[i][j];
      V[i][j].derivatives().setZero();
      for(int k = 0; k < 7; ++k) {
        V[i][j].derivatives()[7*i+k] = dVdU[7*j+k];
      }
    }
  }

  Scalar mul, mut, rhok, rhoeps;
  mul.value() = _mul;
  mut.value() = _mut;
  rhok.value() = _rhok;
  rhoeps.value() = _rhoeps;
  for(int i = 0; i < 4; ++i) {
    for(int j = 0; j < 7; ++j) {
      mul.derivatives()[7*i+j] = dmul[i][j];
      mut.derivatives()[7*i+j] = dmut[i][j];
      rhok.derivatives()[7*i+j] = drhok[i][j];
      rhoeps.derivatives()[7*i+j] = drhoeps[i][j];
    }
  }

  Scalar muk = ooreynolds_mu * (mul + sigma_k * mut);
  Scalar mueps = ooreynolds_mu * (mul + sigma_eps * mut);

  Scalar R5[3], R6[3];
  for(int j = 0; j < 3; ++j) {
    R5[j] = muk * (dp1dxj[0][j] * V[0][5] + dp1dxj[1][j] * V[1][5] +
                   dp1dxj[2][j] * V[2][5] + dp1dxj[3][j] * V[3][5]);
    R6[j] = mueps * (dp1dxj[0][j] * V[0][6] + dp1dxj[1][j] * V[1][6] +
                     dp1dxj[2][j] * V[2][6] + dp1dxj[3][j] * V[3][6]);
  }

  Scalar dudxj[3][3];
  dudxj[0][0] = dp1dxj[0][0] * V[0][1] + dp1dxj[1][0] * V[1][1] +
                dp1dxj[2][0] * V[2][1] + dp1dxj[3][0] * V[3][1];
  dudxj[0][1] = dp1dxj[0][1] * V[0][1] + dp1dxj[1][1] * V[1][1] +
                dp1dxj[2][1] * V[2][1] + dp1dxj[3][1] * V[3][1];
  dudxj[0][2] = dp1dxj[0][2] * V[0][1] + dp1dxj[1][2] * V[1][1] +
                dp1dxj[2][2] * V[2][1] + dp1dxj[3][2] * V[3][1];
  dudxj[1][0] = dp1dxj[0][0] * V[0][2] + dp1dxj[1][0] * V[1][2] +
                dp1dxj[2][0] * V[2][2] + dp1dxj[3][0] * V[3][2];
  dudxj[1][1] = dp1dxj[0][1] * V[0][2] + dp1dxj[1][1] * V[1][2] +
                dp1dxj[2][1] * V[2][2] + dp1dxj[3][1] * V[3][2];
  dudxj[1][2] = dp1dxj[0][2] * V[0][2] + dp1dxj[1][2] * V[1][2] +
                dp1dxj[2][2] * V[2][2] + dp1dxj[3][2] * V[3][2];
  dudxj[2][0] = dp1dxj[0][0] * V[0][3] + dp1dxj[1][0] * V[1][3] +
                dp1dxj[2][0] * V[2][3] + dp1dxj[3][0] * V[3][3];
  dudxj[2][1] = dp1dxj[0][1] * V[0][3] + dp1dxj[1][1] * V[1][3] +
                dp1dxj[2][1] * V[2][3] + dp1dxj[3][1] * V[3][3];
  dudxj[2][2] = dp1dxj[0][2] * V[0][3] + dp1dxj[1][2] * V[1][3] +
                dp1dxj[2][2] * V[2][3] + dp1dxj[3][2] * V[3][3];

  Scalar div  = dudxj[0][0] + dudxj[1][1] + dudxj[2][2];
  Scalar div2 = dudxj[0][0] * dudxj[0][0] + dudxj[1][1] * dudxj[1][1] + dudxj[2][2] * dudxj[2][2];
  Scalar a = dudxj[0][1] + dudxj[1][0];
  Scalar b = dudxj[0][2] + dudxj[2][0];
  Scalar c = dudxj[1][2] + dudxj[2][1];
  Scalar prod = ooreynolds_mu * mut * (2.0 * div2 - 2.0 / 3.0 * div * div + a * a + b * b + c * c)
                - 2.0 / 3.0 * rhok * div;
  Scalar S5 = -rhoeps + prod;
  Scalar S6 = (sigma_eps1 * rhoeps * prod - sigma_eps2 * rhoeps * rhoeps) / rhok;

  for(int i = 0; i < 4; ++i) {
    for(int j = 0; j < 3; ++j) {
      for(int k = 0; k < 7; ++k) {
        dRdU[i][j][5][k] = R5[j].derivatives()[7*i+k];
        dRdU[i][j][6][k] = R6[j].derivatives()[7*i+k];
      }
    }
    for(int j = 0; j < 7; ++j) {
      dSdU[i][0][j] = dSdU[i][1][j] = dSdU[i][2][j] = dSdU[i][3][j] = dSdU[i][4][j] = 0;
      dSdU[i][5][j] = S5.derivatives()[7*i+j];
      dSdU[i][6][j] = S6.derivatives()[7*i+j];
    }
  }
}

//------------------------------------------------------------------------------

#endif

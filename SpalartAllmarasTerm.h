#ifndef _SA_TERM_H_
#define _SA_TERM_H_

#include <IoData.h>
#include <ThermalCondFcn.h>

#include <Eigen/Core>
#include <unsupported/Eigen/AutoDiff>

#include <limits>

class VarFcn;
class ViscoFcn;

//------------------------------------------------------------------------------
/*
@INPROCEEDINGS{spalart-allmaras-92,
  author = "Spalart, P. R. and Allmaras, S. R.",
  title = "A One-Equation Turbulence Model for Aerodynamic Flows",
  month = jan,
  year = 1992,
  booktitle = "30th AIAA Aerospace Sciences Meeting and Exhibit, Reno, NV",
  note = "{AIAA} paper 92-0439"
}
@INPROCEEDINGS{allmaras-99,
  author = "Allmaras, S. R.",
  title = "Multigrid for the 2-D compressible Navier-Stokes equations",
  month = jan,
  year = 1999,
  booktitle = "37th AIAA Aerospace Sciences Meeting and Exhibit, Reno, NV",
  note = "{AIAA} paper 99-3336",
}
*/
class SATerm {

  static constexpr double fourth = 1.0 / 4.0;
  static constexpr double sixth = 1.0 / 6.0;

  double ooreynolds_mu;
  double dRe_mudMachNS;

  double cb1;
  double cb2;
  double cw1;
  double cw2;
  double cw3_pow6;
  double opcw3_pow;
  double cv1_pow3;
  double oocv2;
  double oosigma;
  double oovkcst2;
  double rlim;
  double c2;
  double c3;
  double ct3;
  double ct4;
  double cn1;
  double d2wall_eps;
  double ed2wall_eps;
  double mut_eps;
  bool usefv3;
  bool useneg;

  ConstantPrandtlThermalCondFcn *turbThermalCondFcn;

 public:
  SATerm(IoData&, SAModelData&, ViscoFcn *, VarFcn *);
  virtual ~SATerm();

  void rstVar(IoData&);

  ConstantPrandtlThermalCondFcn *getTurbThermalCondFcn() {
    return turbThermalCondFcn;
  }

  virtual double computeLengthScale(double[4], double[4][3]);

  virtual double computeDerivativeOfLengthScale(double[4], double[4], double[4][3], double[4][3]);

  virtual void computeDerivativeOperatorsOfLengthScale(double[4], double[4][3], double[4], double[4][3]);

  void computeMuTilde(double *, double&);

  void computeDerivativeOfMuTilde(double *, double *, double&);

  void computeMuTilde(double *[4], double&);

  void computePerturbedMuTilde(double, double, double, double&);

  void computeDerivativeOfMuTilde(double *[4], double *[4], double&);

  void computeMuTildeDerivativeOperators(double *[4], double[4][6]);

  void computeJacobianMuTilde(double *[4], double[4][6]);

  void computeTurbulentTransportCoefficients(bool, double, int, double, double&, double&, double&);

  void computeDerivativeOfTurbulentTransportCoefficients(bool, double, double, int, double, double,
                                                         double&, double&, double&);

  void computeTurbulentTransportCoefficientsDerivativeOperators(bool, double, int, double, double,
                                                                double&, double&, double&, double&,
                                                                double&, double&);

  double computeTurbulentViscosity(double, double);

  double computeDerivativeOfTurbulentViscosity(double, double, double, double);

  void computeTurbulentViscosityDerivativeOperators(double, double, double&, double&);

  double computeSecondTurbulentViscosity(double);

  double computeDerivativeOfSecondTurbulentViscosity(double);

  void computeSecondTurbulentViscosityDerivativeOperators(double, double&);

  void computeNuTildeGradient(double[4][3], double[4], double[3]);

  void computeDerivativeOfNuTildeGradient(double[4][3], double[4][3], double[4], double[4], double[3]);

  void computeDerivativeOperatorsOfNuTildeGradient(double[4][3], double[4], double[3][4][3], double[3][4]);

  void computeVolumeTermSA(double[4][3], double, double[3][3], double, double, double *[4],
                           double (*)[6], double *);

  void computeDerivativeOfVolumeTermSA(double[4][3], double[4][3], double, double, double[3][3],
                                       double[3][3], double, double, double, double, double *[4],
                                       double *[4], double, double (*)[6], double *);

  void computeDerivativeOperatorsOfVolumeTermSA(double[4][3], double, double[3][3], double, double, double *[4],
                                                double[3][4][3], double[3], double[3], double[3],
                                                double[4][3], double[3][3], double&, double&, double&,
                                                double&);

  void computeJacobianVolumeTermSA(double[4][3], double, double[3][3], double, double,
                                   double *[4], double (*)[3][1][1], double (*)[1][1]);

  void computeJacobianVolumeTermSA(double[4][3], double, double[3][3], double, double[4][6], double,
                                   double[4][6], double *[4], double (*)[3][6][6], double (*)[6][6]);

  void computeDistanceDerivativeOfVolumeTermSA(double, double[3][3], double, double, double *[4], double&);

 private:
  template<typename Scalar, typename Scalar2>
  void commonPart(Scalar, Scalar2[3][3], Scalar2, Scalar2, Scalar2, Scalar2, Scalar2&, Scalar2&, Scalar2&,
                  Scalar2&, Scalar2&, Scalar2&, Scalar&, Scalar&, Scalar2&, Scalar2&, Scalar2&, Scalar2&, Scalar&,
                  Scalar&, Scalar&, Scalar&, Scalar&, Scalar&, Scalar&, Scalar2&, Scalar2&);
};

//------------------------------------------------------------------------------

inline
SATerm::SATerm(IoData& iod, SAModelData& sa, ViscoFcn *visf, VarFcn *vfn) {
  ooreynolds_mu = 1.0 / iod.ref.reynolds_mu;
  dRe_mudMachNS = iod.ref.dRe_mudMach;
  cb1 = sa.cb1;
  cb2 = sa.cb2;
  cw2 = sa.cw2;
  double cw3 = sa.cw3;
  cw3_pow6 = cw3 * cw3 * cw3 * cw3 * cw3 * cw3;
  opcw3_pow = std::pow(1.0 + cw3_pow6, sixth);
  double cv1 = sa.cv1;
  cv1_pow3 = cv1 * cv1 * cv1;
  oocv2 = 1.0 / sa.cv2;
  oosigma = 1.0 / sa.sigma;
  oovkcst2 = 1.0 / (sa.vkcst * sa.vkcst);
  cw1 = cb1 * oovkcst2 + (1.0 + cb2) * oosigma;
  rlim = sa.rlim;
  c2 = sa.c2;
  c3 = sa.c3;
  cw1 /= iod.ref.reynolds_mu;
  oosigma /= iod.ref.reynolds_mu;
  usefv3 = (sa.form == SAModelData::FV3) ? true : false;
  ct3 = (sa.form == SAModelData::NOFT2) ? 0 : sa.ct3;
  ct4 = sa.ct4;
  cn1 = sa.cn1;
  d2wall_eps = sa.d2wall_eps;
  if(iod.problem.framework == ProblemData::EMBEDDED) {
    ed2wall_eps = iod.bc.wall.delta;
    if(ed2wall_eps < d2wall_eps) ed2wall_eps = d2wall_eps;
  }
  else {
    ed2wall_eps = d2wall_eps;
  }
  useneg = (sa.form == SAModelData::NEGATIVE) ? true : false;
  mut_eps = (sa.form == SAModelData::NEGATIVE) ? sa.mut_eps : std::numeric_limits<double>::lowest();
  turbThermalCondFcn = new ConstantPrandtlThermalCondFcn(iod, visf, vfn);
}

//------------------------------------------------------------------------------

inline
SATerm::~SATerm() {
  if(turbThermalCondFcn) {
    delete turbThermalCondFcn;
  }
}

//------------------------------------------------------------------------------

inline
void SATerm::rstVar(IoData& iod) {
  // reset anything that may depend on any sensitivity analysis variable
  oosigma /= ooreynolds_mu;
  ooreynolds_mu = 1.0 / iod.ref.reynolds_mu;
  dRe_mudMachNS = iod.ref.dRe_mudMach;
  cw1 = cb1 * oovkcst2 + (1.0 + cb2) * oosigma;
  cw1 /= iod.ref.reynolds_mu;
  oosigma /= iod.ref.reynolds_mu;
}

//------------------------------------------------------------------------------

inline
double SATerm::computeLengthScale(double d2w[4], double X[4][3]) {
  return fourth * (d2w[0] + d2w[1] + d2w[2] + d2w[3]);
}

//------------------------------------------------------------------------------

inline
double SATerm::computeDerivativeOfLengthScale(double d2w[4], double dd2w[4], double X[4][3],
                                              double dX[4][3]) {
  return fourth * (dd2w[0] + dd2w[1] + dd2w[2] + dd2w[3]);
}

//------------------------------------------------------------------------------

inline
void SATerm::computeDerivativeOperatorsOfLengthScale(double d2w[4], double X[4][3], double dd2walldd2w[4],
                                                     double dd2walldX[4][3]) {
  dd2walldd2w[0] = dd2walldd2w[1] = dd2walldd2w[2] = dd2walldd2w[3] = fourth;
}

//------------------------------------------------------------------------------

inline
void SATerm::computeMuTilde(double *V, double& mutilde) {
  mutilde = V[0] * V[5];
}

//------------------------------------------------------------------------------

inline
void SATerm::computeDerivativeOfMuTilde(double *V, double *dV, double& dmutilde) {
  dmutilde = dV[0] * V[5] + V[0] * dV[5];
}


//------------------------------------------------------------------------------

inline
void SATerm::computeMuTilde(double *V[4], double& mutilde) {
  mutilde = fourth * (V[0][0] * V[0][5] + V[1][0] * V[1][5] +
                      V[2][0] * V[2][5] + V[3][0] * V[3][5]);
}

//------------------------------------------------------------------------------

inline
void SATerm::computeDerivativeOfMuTilde(double *V[4], double *dV[4], double& dmutilde) {
  dmutilde = fourth * (dV[0][0] * V[0][5] + V[0][0] * dV[0][5] + dV[1][0] * V[1][5] + V[1][0] * dV[1][5] +
                       dV[2][0] * V[2][5] + V[2][0] * dV[2][5] + dV[3][0] * V[3][5] + V[3][0] * dV[3][5]);
}

//------------------------------------------------------------------------------

inline
void SATerm::computeMuTildeDerivativeOperators(double *V[4], double dmutildedV[4][6]) {
  for(int i = 0; i < 4; ++i) {
    dmutildedV[i][0] = fourth * V[i][5];
    dmutildedV[i][5] = fourth * V[i][0];
  }
}

//------------------------------------------------------------------------------

inline
void SATerm::computeJacobianMuTilde(double *V[4], double dmutilde[4][6]) {
  for(int k = 0; k < 4; ++k) {
    dmutilde[k][0] = 0.0;
    dmutilde[k][1] = 0.0;
    dmutilde[k][2] = 0.0;
    dmutilde[k][3] = 0.0;
    dmutilde[k][4] = 0.0;
    dmutilde[k][5] = 0.25;
  }
}

//------------------------------------------------------------------------------

inline
void SATerm::computePerturbedMuTilde(double mut_orig, double mut_rel, double mul, double& mutilde) {
  double checkVal = 1e-16 / SATerm::ooreynolds_mu;
  // std::cout << "checkVal = " << checkVal; 
  if(mut_rel <= checkVal) {
    mutilde = 1e-16;
  } else {
    double mutilde_orig = mutilde;
    if(mut_rel < 1.0) {
      mutilde = 1.0;
    } else {
      mutilde = mut_rel / mul;
    }
    int its = 0;
    int MaxIts = 50;
    double eps = 1e-16;
    double diff = 1.0;
    double mutilde_start = mutilde;
    double mutilde_prev4, mutilde_prev3, mutilde_prev2, mutilde_prev, f, fp;
    while(diff > eps && its < MaxIts) {
      // if(its == 0) {
      //   mutilde_prev = mut_rel / mul;
      // } else {
      mutilde_prev = mutilde;
      // }
      mutilde_prev2 = mutilde_prev * mutilde_prev;
      mutilde_prev3 = mutilde_prev2 * mutilde_prev;
      mutilde_prev4 = mutilde_prev3 * mutilde_prev;
      f = mutilde_prev4 - mut_rel * mutilde_prev3 - mut_rel * mul * mul * mul * cv1_pow3;
      fp = 4.0 * mutilde_prev3 - 3.0 * mutilde_prev2 * mut_rel;
      mutilde = mutilde_prev - (f / fp);
      diff = std::fabs(mutilde - mutilde_prev);
      its +=1;
    }
    // if(mutilde < 0) {
    //   mutilde = mut_rel / mul;
    // }
    // std::cout << "mut_orig = " << mut_orig << "; mut_rel = " << mut_rel << "; mul = " << mul << "; mutilde_orig = " << mutilde_orig <<";\nmutilde_start = " << mutilde_start << "; mutilde_new = " << mutilde << "; its = " << its << ";\n";
    // double percdiffNew = std::fabs((mutilde_orig - mutilde)/mutilde_orig);
    // if (percdiffNew > 1) {
    //   std::cout << "percentage diff new = " << percdiffNew << ";\n";
    // }
  }
}

//------------------------------------------------------------------------------

inline
void SATerm::computeTurbulentTransportCoefficients(bool inTrip, double mutilde, int tag, double mul,
                                                   double& mut, double& lambdat, double& kappat) {
  // Applying the laminar-turbulent trip when necessary
  if(inTrip) {
    mut = computeTurbulentViscosity(mutilde, mul);
    lambdat = computeSecondTurbulentViscosity(mut);
    kappat = turbThermalCondFcn->turbulentConductivity(mut, tag);
  }
  else {
    mut = lambdat = kappat = 0.;
  }
}

//------------------------------------------------------------------------------

inline
void SATerm::computeDerivativeOfTurbulentTransportCoefficients(bool inTrip, double mutilde, double dmutilde,
                                                               int tag, double mul, double dmul,
                                                               double& dmut, double& dlambdat, double& dkappat) {
  // Applying the laminar-turbulent trip when necessary
  if(inTrip) {
    dmut = computeDerivativeOfTurbulentViscosity(mutilde, dmutilde, mul, dmul);
    dlambdat = computeDerivativeOfSecondTurbulentViscosity(dmut);
    dkappat = turbThermalCondFcn->turbulentConductivityDerivative(dmut, tag);
  }
  else {
    dmut = dlambdat = dkappat = 0.;
  }
}

//------------------------------------------------------------------------------

inline
void SATerm::computeTurbulentTransportCoefficientsDerivativeOperators(bool inTrip, double mutilde, int tag, double mul,
                                                                      double mut, double& dmutdmutilde, double& dmutdmul,
                                                                      double& dlambdatdmutilde, double& dlambdatdmul,
                                                                      double& dkappatdmutilde, double& dkappatdmul) {
  // Applying the laminar-turbulent trip when necessary
  if(inTrip) {
    computeTurbulentViscosityDerivativeOperators(mutilde, mul, dmutdmutilde, dmutdmul);
    double dlambdatdmut;
    computeSecondTurbulentViscosityDerivativeOperators(mut, dlambdatdmut);
    dlambdatdmutilde = dlambdatdmut * dmutdmutilde;
    dlambdatdmul = dlambdatdmut * dmutdmul;
    double dkappatdmut;
    turbThermalCondFcn->turbulentConductivityDerivativeOperators(mut, tag, dkappatdmut);
    dkappatdmutilde = dkappatdmut * dmutdmutilde;
    dkappatdmul = dkappatdmut * dmutdmul;
  }
  else {
    dmutdmutilde = dmutdmul = dlambdatdmutilde = dlambdatdmul = dkappatdmutilde = dkappatdmul = 0.;
  }
}

//------------------------------------------------------------------------------

inline
double SATerm::computeTurbulentViscosity(double mutilde, double mul) {
  double maxmutilde = std::max(mutilde, mut_eps);
  double chi = maxmutilde / mul;
  double chi3 = chi * chi * chi;
  double fv1 = chi3 / (chi3 + cv1_pow3);
  return maxmutilde * fv1;
}

//------------------------------------------------------------------------------

inline
double SATerm::computeDerivativeOfTurbulentViscosity(double mutilde, double dmutilde, double mul, double dmul) {
  double maxmutilde = std::max(mutilde, mut_eps);
  double chi = maxmutilde / mul;
  double chi3 = chi * chi * chi;
  double fv1 = chi3 / (chi3 + cv1_pow3);
  double dchi = (mut_eps < mutilde) ? (dmutilde * mul - mutilde * dmul) / (mul * mul) : -mut_eps * dmul / (mul * mul);
  double dfv1 = cv1_pow3 * 3.0 * chi * chi * dchi / ((chi3 + cv1_pow3) * (chi3 + cv1_pow3));
  return (mut_eps < mutilde) ? dmutilde * fv1 + mutilde * dfv1 : mut_eps * dfv1;
}

//------------------------------------------------------------------------------

inline
void SATerm::computeTurbulentViscosityDerivativeOperators(double mutilde, double mul,
                                                          double& dmutdmutilde, double& dmutdmul) {
  double maxmutilde = std::max(mutilde, mut_eps);
  double chi = maxmutilde / mul;
  double chi3 = chi * chi * chi;
  double fv1 = chi3 / (chi3 + cv1_pow3);
  double dchidmul = -chi / mul;
  double dchi3dmul = 3 * chi * chi * dchidmul;
  double dfv1dmul = cv1_pow3 * dchi3dmul / ((chi3 + cv1_pow3) * (chi3 + cv1_pow3));
  if(mut_eps < (mutilde * fv1)) {
    double dchidmutilde = 1 / mul;
    double dchi3dmutilde = 3 * chi * chi * dchidmutilde;
    double dfv1dmutilde = cv1_pow3 * dchi3dmutilde / ((chi3 + cv1_pow3) * (chi3 + cv1_pow3));
    dmutdmutilde = fv1 + mutilde * dfv1dmutilde;
    dmutdmul = mutilde * dfv1dmul;
  }
  else {
    dmutdmutilde = 0;
    dmutdmul = mut_eps * dfv1dmul;
  }
}

//------------------------------------------------------------------------------

inline
double SATerm::computeSecondTurbulentViscosity(double mut) {
  // simple model that remains true when the Stokes' hypothesis is assumed
  return -2.0 * mut / 3.0;
}

//------------------------------------------------------------------------------

inline
double SATerm::computeDerivativeOfSecondTurbulentViscosity(double dmut) {
  return -2.0 * dmut / 3.0;
}

//------------------------------------------------------------------------------

inline
void SATerm::computeSecondTurbulentViscosityDerivativeOperators(double mut, double& dlambdatdmut) {
  dlambdatdmut = -2.0 / 3.0;
}

//------------------------------------------------------------------------------

template<class Scalar, class Scalar2>
void SATerm::commonPart(Scalar d2wall, Scalar2 dudxj[3][3], Scalar2 mul, Scalar2 mutilde, Scalar2 rho, Scalar2 ooreynolds_mu,
                        Scalar2& oorho, Scalar2& chi, Scalar2& chi3, Scalar2& fv1, Scalar2& fv2, Scalar2& fv3,
                        Scalar& ood2wall2, Scalar& zz, Scalar2& s12, Scalar2& s23, Scalar2& s31, Scalar2& s,
                        Scalar& rr, Scalar& Stilde, Scalar& Sbar, Scalar& rr2, Scalar& gg, Scalar& gg6, Scalar& fw,
                        Scalar2& ft2, Scalar2& fn) {
  using std::sqrt;
  using std::min;
  using std::pow;
  using std::exp;

  oorho = 1.0 / rho;
  chi = mutilde / mul;
  chi3 = chi * chi * chi;
  if(useneg && mutilde < 0) { // negative SA model
    fn = (cn1 + chi3) / (cn1 - chi3);
  }
  else {
    fn = 1.0;
  }
  if(d2wall <= d2wall_eps) return;
  if(d2wall < ed2wall_eps) d2wall = ed2wall_eps;
  ood2wall2 = 1.0 / (d2wall * d2wall);
  s12 = dudxj[0][1] - dudxj[1][0];
  s23 = dudxj[1][2] - dudxj[2][1];
  s31 = dudxj[2][0] - dudxj[0][2];
  s = sqrt(s12 * s12 + s23 * s23 + s31 * s31);
  if(!useneg || mutilde >= 0) {
    ft2 = ct3 * exp(-ct4 * chi * chi);
    fv1 = chi3 / (chi3 + cv1_pow3);
    if(usefv3) {
      fv2 = 1.0 + oocv2 * chi;
      fv2 = 1.0 / (fv2 * fv2 * fv2);
      if(chi == 0.0) fv3 = 0; else fv3 = (1.0 + chi * fv1) * (1.0 - fv2) / chi;
    }
    else {
      fv2 = 1. - chi / (1. + chi * fv1);
      fv3 = 1.0;
    }
    zz = ooreynolds_mu * oovkcst2 * mutilde * oorho * ood2wall2;
    Sbar = zz * fv2;
    if(Sbar >= -c2 * s) {
      Stilde = s * fv3 + Sbar;
    }
    else {
      Stilde = s * fv3 + s * (c2 * c2 * s + c3 * Sbar) / ((c3 - 2.0 * c2) * s - Sbar);
    }
    if(Stilde == 0.0) {
      rr = rlim;
    }
    else {
      rr = min(zz / Stilde, rlim);
    }
    rr2 = rr * rr;
    gg = rr + cw2 * (rr2 * rr2 * rr2 - rr);
    if(gg > 1e40) {
      // note: lim x->∞ x*(x^6 + c^6)^{-1/6} = 1
      fw = opcw3_pow;
    }
    else {
      Scalar gg2 = gg * gg;
      gg6 = gg2 * gg2 * gg2;
      fw = opcw3_pow * gg * pow(gg6 + cw3_pow6, -sixth);
    }
  }
}

//------------------------------------------------------------------------------

inline
void SATerm::computeNuTildeGradient(double dp1dxj[4][3], double nutilde[4], double dnutildedxj[3]) {
  dnutildedxj[0] = dp1dxj[0][0] * nutilde[0] + dp1dxj[1][0] * nutilde[1] +
                   dp1dxj[2][0] * nutilde[2] + dp1dxj[3][0] * nutilde[3];
  dnutildedxj[1] = dp1dxj[0][1] * nutilde[0] + dp1dxj[1][1] * nutilde[1] +
                   dp1dxj[2][1] * nutilde[2] + dp1dxj[3][1] * nutilde[3];
  dnutildedxj[2] = dp1dxj[0][2] * nutilde[0] + dp1dxj[1][2] * nutilde[1] +
                   dp1dxj[2][2] * nutilde[2] + dp1dxj[3][2] * nutilde[3];
}

//------------------------------------------------------------------------------

inline
void SATerm::computeDerivativeOfNuTildeGradient(double dp1dxj[4][3], double ddp1dxj[4][3], double nutilde[4],
                                                double dnutilde[4], double ddnutildedxj[3]) {
  ddnutildedxj[0] = ddp1dxj[0][0] * nutilde[0] + dp1dxj[0][0] * dnutilde[0] + ddp1dxj[1][0] * nutilde[1] + dp1dxj[1][0] * dnutilde[1] +
                    ddp1dxj[2][0] * nutilde[2] + dp1dxj[2][0] * dnutilde[2] + ddp1dxj[3][0] * nutilde[3] + dp1dxj[3][0] * dnutilde[3];
  ddnutildedxj[1] = ddp1dxj[0][1] * nutilde[0] + dp1dxj[0][1] * dnutilde[0] + ddp1dxj[1][1] * nutilde[1] + dp1dxj[1][1] * dnutilde[1] +
                    ddp1dxj[2][1] * nutilde[2] + dp1dxj[2][1] * dnutilde[2] + ddp1dxj[3][1] * nutilde[3] + dp1dxj[3][1] * dnutilde[3];
  ddnutildedxj[2] = ddp1dxj[0][2] * nutilde[0] + dp1dxj[0][2] * dnutilde[0] + ddp1dxj[1][2] * nutilde[1] + dp1dxj[1][2] * dnutilde[1] +
                    ddp1dxj[2][2] * nutilde[2] + dp1dxj[2][2] * dnutilde[2] + ddp1dxj[3][2] * nutilde[3] + dp1dxj[3][2] * dnutilde[3];
}

//------------------------------------------------------------------------------

inline
void SATerm::computeDerivativeOperatorsOfNuTildeGradient(double dp1dxj[4][3], double nutilde[4],
                                                         double ddnutildedxjddp1dxj[3][4][3], double ddnutildedxjdnutilde[3][4]) {
  ddnutildedxjddp1dxj[0][0][0] = nutilde[0];
  ddnutildedxjddp1dxj[0][1][0] = nutilde[1];
  ddnutildedxjddp1dxj[0][2][0] = nutilde[2];
  ddnutildedxjddp1dxj[0][3][0] = nutilde[3];
  ddnutildedxjdnutilde[0][0] = dp1dxj[0][0];
  ddnutildedxjdnutilde[0][1] = dp1dxj[1][0];
  ddnutildedxjdnutilde[0][2] = dp1dxj[2][0];
  ddnutildedxjdnutilde[0][3] = dp1dxj[3][0];
  ddnutildedxjddp1dxj[1][0][1] = nutilde[0];
  ddnutildedxjddp1dxj[1][1][1] = nutilde[1];
  ddnutildedxjddp1dxj[1][2][1] = nutilde[2];
  ddnutildedxjddp1dxj[1][3][1] = nutilde[3];
  ddnutildedxjdnutilde[1][0] = dp1dxj[0][1];
  ddnutildedxjdnutilde[1][1] = dp1dxj[1][1];
  ddnutildedxjdnutilde[1][2] = dp1dxj[2][1];
  ddnutildedxjdnutilde[1][3] = dp1dxj[3][1];
  ddnutildedxjddp1dxj[2][0][2] = nutilde[0];
  ddnutildedxjddp1dxj[2][1][2] = nutilde[1];
  ddnutildedxjddp1dxj[2][2][2] = nutilde[2];
  ddnutildedxjddp1dxj[2][3][2] = nutilde[3];
  ddnutildedxjdnutilde[2][0] = dp1dxj[0][2];
  ddnutildedxjdnutilde[2][1] = dp1dxj[1][2];
  ddnutildedxjdnutilde[2][2] = dp1dxj[2][2];
  ddnutildedxjdnutilde[2][3] = dp1dxj[3][2];
}

//------------------------------------------------------------------------------

inline
void SATerm::computeVolumeTermSA(double dp1dxj[4][3], double d2wall, double dudxj[3][3],
                                 double mul, double mutilde, double *V[4],
                                 double (*R)[6], double *S) {
  double rho = fourth * (V[0][0] + V[1][0] + V[2][0] + V[3][0]);
  double oorho, chi, chi3, fv1, fv2, fv3, ood2wall2, zz, s12, s23, s31, s, rr, Stilde, Sbar,
         rr2, gg, gg6, fw, ft2, fn;
  commonPart(d2wall, dudxj, mul, mutilde, rho, ooreynolds_mu, oorho, chi, chi3, fv1, fv2, fv3, ood2wall2,
             zz, s12, s23, s31, s, rr, Stilde, Sbar, rr2, gg, gg6, fw, ft2, fn);

  double mu5 = oosigma * (mul + fn * mutilde);
  double dnutildedxj[3] = {dp1dxj[0][0] * V[0][5] + dp1dxj[1][0] * V[1][5] +
                           dp1dxj[2][0] * V[2][5] + dp1dxj[3][0] * V[3][5],
                           dp1dxj[0][1] * V[0][5] + dp1dxj[1][1] * V[1][5] +
                           dp1dxj[2][1] * V[2][5] + dp1dxj[3][1] * V[3][5],
                           dp1dxj[0][2] * V[0][5] + dp1dxj[1][2] * V[1][5] +
                           dp1dxj[2][2] * V[2][5] + dp1dxj[3][2] * V[3][5]};
  R[0][5] = mu5 * dnutildedxj[0];
  R[1][5] = mu5 * dnutildedxj[1];
  R[2][5] = mu5 * dnutildedxj[2];
  S[0] = 0.0;
  S[1] = 0.0;
  S[2] = 0.0;
  S[3] = 0.0;
  S[4] = 0.0;
  if(d2wall <= d2wall_eps) {
    S[5] = 0.0;
    return;
  }
  if(d2wall < ed2wall_eps) d2wall = ed2wall_eps;
  double drhodxj[3] = {dp1dxj[0][0] * V[0][0] + dp1dxj[1][0] * V[1][0] +
                       dp1dxj[2][0] * V[2][0] + dp1dxj[3][0] * V[3][0],
                       dp1dxj[0][1] * V[0][0] + dp1dxj[1][1] * V[1][0] +
                       dp1dxj[2][1] * V[2][0] + dp1dxj[3][1] * V[3][0],
                       dp1dxj[0][2] * V[0][0] + dp1dxj[1][2] * V[1][0] +
                       dp1dxj[2][2] * V[2][0] + dp1dxj[3][2] * V[3][0]};

  double AA, BB, CC, DD;
  AA = oosigma * cb2 * rho * (dnutildedxj[0] * dnutildedxj[0] + dnutildedxj[1] * dnutildedxj[1] + dnutildedxj[2] * dnutildedxj[2]);
  if(useneg && mutilde < 0) {
    BB = cb1 * (1 - ct3) * s * mutilde;
    CC = cw1 * oorho * mutilde * mutilde * ood2wall2;
  }
  else {
    BB = cb1 * (1 - ft2) * Stilde * mutilde;
    CC = - (cw1 * fw - cb1 * oovkcst2 * ooreynolds_mu * ft2) * oorho * mutilde * mutilde * ood2wall2;
  }
  DD = - oorho * mu5 * (dnutildedxj[0] * drhodxj[0] + dnutildedxj[1] * drhodxj[1] + dnutildedxj[2] * drhodxj[2]);
  S[5] = AA + BB + CC + DD;
}

//------------------------------------------------------------------------------

inline
void SATerm::computeDerivativeOfVolumeTermSA(double _dp1dxj[4][3], double ddp1dxj[4][3],
                                             double _d2wall, double dd2wall, double _dudxj[3][3], double ddudxj[3][3],
                                             double _mul, double dmul, double _mutilde,
                                             double dmutilde, double *_V[4], double *dV[4], double dMach,
                                             double (*dR)[6], double *dS) {
  typedef Eigen::AutoDiffScalar<Eigen::Matrix<double, 1, 1>> Scalar;
  Scalar dp1dxj[4][3], d2wall, dudxj[3][3], mul, mutilde, *V[4];
  for(int i=0; i<4; ++i) for(int j=0; j<3; ++j) { dp1dxj[i][j].value() = _dp1dxj[i][j]; dp1dxj[i][j].derivatives()[0] = ddp1dxj[i][j]; }
  d2wall.value() = _d2wall; d2wall.derivatives()[0] = dd2wall;
  for(int i=0; i<3; ++i) for(int j=0; j<3; ++j) { dudxj[i][j].value()  = _dudxj[i][j];  dudxj[i][j].derivatives()[0]  = ddudxj[i][j]; }
  mul.value() = _mul; mul.derivatives()[0] = dmul;
  mutilde.value() = _mutilde; mutilde.derivatives()[0] = dmutilde;
  for(int i=0; i<4; ++i) { V[i] = new Scalar[6]; for(int j=0; j<6; ++j) { V[i][j].value() = _V[i][j]; V[i][j].derivatives()[0] = dV[i][j]; }}

  Scalar cw1, oosigma, ooreynolds_mu;
  double dooreynolds_mu = dMach ? -(SATerm::ooreynolds_mu * SATerm::ooreynolds_mu) * dRe_mudMachNS : 0;
  cw1.value() = SATerm::cw1; cw1.derivatives()[0] = SATerm::cw1 / SATerm::ooreynolds_mu * dooreynolds_mu;
  oosigma.value() = SATerm::oosigma; oosigma.derivatives()[0] = SATerm::oosigma / SATerm::ooreynolds_mu * dooreynolds_mu;
  ooreynolds_mu.value() = SATerm::ooreynolds_mu; ooreynolds_mu.derivatives()[0] = dooreynolds_mu;

  using std::max;
  Scalar maxmutilde = std::max(mutilde, Scalar(mut_eps));
  Scalar mu5 = oosigma * (mul + maxmutilde);
  Scalar dnutildedxj[3] = {dp1dxj[0][0] * V[0][5] + dp1dxj[1][0] * V[1][5] +
                           dp1dxj[2][0] * V[2][5] + dp1dxj[3][0] * V[3][5],
                           dp1dxj[0][1] * V[0][5] + dp1dxj[1][1] * V[1][5] +
                           dp1dxj[2][1] * V[2][5] + dp1dxj[3][1] * V[3][5],
                           dp1dxj[0][2] * V[0][5] + dp1dxj[1][2] * V[1][5] +
                           dp1dxj[2][2] * V[2][5] + dp1dxj[3][2] * V[3][5]};
  dR[0][5] = (mu5 * dnutildedxj[0]).derivatives()[0];
  dR[1][5] = (mu5 * dnutildedxj[1]).derivatives()[0];
  dR[2][5] = (mu5 * dnutildedxj[2]).derivatives()[0];
  dS[0] = 0.0;
  dS[1] = 0.0;
  dS[2] = 0.0;
  dS[3] = 0.0;
  dS[4] = 0.0;
  if(_d2wall <= d2wall_eps) {
    dS[5] = 0.0;
  }
  else {
    if(_d2wall < ed2wall_eps) _d2wall = ed2wall_eps;
    Scalar rho = 0.25 * (V[0][0] + V[1][0] + V[2][0] + V[3][0]);
    Scalar drhodxj[3] = {dp1dxj[0][0] * V[0][0] + dp1dxj[1][0] * V[1][0] +
                         dp1dxj[2][0] * V[2][0] + dp1dxj[3][0] * V[3][0],
                         dp1dxj[0][1] * V[0][0] + dp1dxj[1][1] * V[1][0] +
                         dp1dxj[2][1] * V[2][0] + dp1dxj[3][1] * V[3][0],
                         dp1dxj[0][2] * V[0][0] + dp1dxj[1][2] * V[1][0] +
                         dp1dxj[2][2] * V[2][0] + dp1dxj[3][2] * V[3][0]};

    Scalar oorho, chi, chi3, fv1, fv2, fv3, ood2wall2, zz, s12, s23, s31, s, rr, Stilde, Sbar,
           rr2, gg, gg6, fw, ft2, fn;
    commonPart(d2wall, dudxj, mul, maxmutilde, rho, ooreynolds_mu, oorho, chi, chi3, fv1, fv2, fv3, ood2wall2,
               zz, s12, s23, s31, s, rr, Stilde, Sbar, rr2, gg, gg6, fw, ft2, fn);

    Scalar AA = oosigma * cb2 * rho *
                (dnutildedxj[0] * dnutildedxj[0] + dnutildedxj[1] * dnutildedxj[1] + dnutildedxj[2] * dnutildedxj[2]);
    Scalar BB = cb1 * Stilde * maxmutilde;
    Scalar CC = - cw1 * fw * oorho * maxmutilde * maxmutilde * ood2wall2;
    Scalar DD = - oorho * mu5 * (dnutildedxj[0] * drhodxj[0] + dnutildedxj[1] * drhodxj[1] + dnutildedxj[2] * drhodxj[2]);
    dS[5] = (AA + BB + CC + DD).derivatives()[0];
  }

  for(int i=0; i<4; ++i) delete [] V[i];
}

//------------------------------------------------------------------------------

inline
void SATerm::computeDerivativeOperatorsOfVolumeTermSA(double _dp1dxj[4][3], double _d2wall, double _dudxj[3][3], double _mul,
                                                      double _mutilde, double *V[4], double dr5ddp1dxj[3][4][3], double dr5dmul[3],
                                                      double dr5dmutilde[3], double dr5dMach[3], double ds5ddp1dxj[4][3],
                                                      double ds5ddudxj[3][3], double& ds5dmul, double& ds5dmutilde,
                                                      double& ds5dMach, double& ds5dd2wall) {
  double maxmutilde = std::max(_mutilde, mut_eps);
  double mu5 = oosigma * (_mul + maxmutilde);
  double nutilde[4] = { V[0][5], V[1][5], V[2][5], V[3][5] };
  double dnutildedxj[3] = {0};
  computeNuTildeGradient(_dp1dxj, nutilde, dnutildedxj);
  double ddnutildedxjddp1dxj[3][4][3] = {0}, ddnutildedxjdnutilde[3][4] = {0};
  computeDerivativeOperatorsOfNuTildeGradient(_dp1dxj, nutilde, ddnutildedxjddp1dxj, ddnutildedxjdnutilde);
  double dooreynolds_mu = -(ooreynolds_mu * ooreynolds_mu) * dRe_mudMachNS;
  double dmu5dMach = (mu5 / ooreynolds_mu) * dooreynolds_mu;
  for(int i = 0; i < 3; ++i) {
    for(int j = 0; j < 4; ++j) {
      for(int k = 0; k < 3; ++k) {
        dr5ddp1dxj[i][j][k] = mu5 * ddnutildedxjddp1dxj[i][j][k];
      }
    }
    dr5dmul[i] = oosigma * dnutildedxj[i];
    if(mut_eps < _mutilde) {
      dr5dmutilde[i] = oosigma * dnutildedxj[i];
    }
    dr5dMach[i] = dmu5dMach * dnutildedxj[i];
  }
  if(_d2wall <= d2wall_eps) {
    return;
  }
  else {
    if(_d2wall < ed2wall_eps) _d2wall = ed2wall_eps;
    // variable         : dp1dxj[4][3], dudxj[3][3], mul, mutilde, Mach, d2wall
    // derivatives index: 0-11,         12-20,       21,  22,      23,   24
    typedef Eigen::AutoDiffScalar<Eigen::Matrix<double, 1, 25>> Scalar;

    Scalar dp1dxj[4][3], d2wall, dudxj[3][3], mul, mutilde;
    for(int i=0; i<4; ++i) for(int j=0; j<3; ++j) { dp1dxj[i][j].value() = _dp1dxj[i][j]; dp1dxj[i][j].derivatives().setZero(); dp1dxj[i][j].derivatives()[3*i+j] = 1; }
    d2wall.value() = _d2wall; d2wall.derivatives().setZero(); d2wall.derivatives()[24] = 1;
    for(int i=0; i<3; ++i) for(int j=0; j<3; ++j) { dudxj[i][j].value() = _dudxj[i][j]; dudxj[i][j].derivatives().setZero(); dudxj[i][j].derivatives()[12+3*i+j] = 1; }
    mul.value() = _mul; mul.derivatives().setZero(); mul.derivatives()[21] = 1;
    mutilde.value() = _mutilde; mutilde.derivatives().setZero(); mutilde.derivatives()[22] = 1;

    Scalar cw1, oosigma, ooreynolds_mu;
    cw1.value() = SATerm::cw1; cw1.derivatives().setZero(); cw1.derivatives()[23] = SATerm::cw1 / SATerm::ooreynolds_mu * dooreynolds_mu;
    oosigma.value() = SATerm::oosigma; oosigma.derivatives().setZero(); oosigma.derivatives()[23] = SATerm::oosigma / SATerm::ooreynolds_mu * dooreynolds_mu;
    ooreynolds_mu.value() = SATerm::ooreynolds_mu; ooreynolds_mu.derivatives().setZero(); ooreynolds_mu.derivatives()[23] = dooreynolds_mu;

    using std::max;
    Scalar maxmutilde = std::max(mutilde, Scalar(mut_eps));
    Scalar mu5 = oosigma * (mul + maxmutilde);
    Scalar dnutildedxj[3] = {dp1dxj[0][0] * V[0][5] + dp1dxj[1][0] * V[1][5] +
                             dp1dxj[2][0] * V[2][5] + dp1dxj[3][0] * V[3][5],
                             dp1dxj[0][1] * V[0][5] + dp1dxj[1][1] * V[1][5] +
                             dp1dxj[2][1] * V[2][5] + dp1dxj[3][1] * V[3][5],
                             dp1dxj[0][2] * V[0][5] + dp1dxj[1][2] * V[1][5] +
                             dp1dxj[2][2] * V[2][5] + dp1dxj[3][2] * V[3][5]};

    Scalar rho = fourth * (V[0][0] + V[1][0] + V[2][0] + V[3][0]);
    Scalar drhodxj[3] = {dp1dxj[0][0] * V[0][0] + dp1dxj[1][0] * V[1][0] +
                         dp1dxj[2][0] * V[2][0] + dp1dxj[3][0] * V[3][0],
                         dp1dxj[0][1] * V[0][0] + dp1dxj[1][1] * V[1][0] +
                         dp1dxj[2][1] * V[2][0] + dp1dxj[3][1] * V[3][0],
                         dp1dxj[0][2] * V[0][0] + dp1dxj[1][2] * V[1][0] +
                         dp1dxj[2][2] * V[2][0] + dp1dxj[3][2] * V[3][0]};

    Scalar oorho, chi, chi3, fv1, fv2, fv3, ood2wall2, zz, s12, s23, s31, s, rr, Stilde, Sbar,
           rr2, gg, gg6, fw, ft2, fn;
    commonPart(d2wall, dudxj, mul, maxmutilde, rho, ooreynolds_mu, oorho, chi, chi3, fv1, fv2, fv3, ood2wall2,
               zz, s12, s23, s31, s, rr, Stilde, Sbar, rr2, gg, gg6, fw, ft2, fn);

    Scalar AA = oosigma * cb2 * rho *
                (dnutildedxj[0] * dnutildedxj[0] + dnutildedxj[1] * dnutildedxj[1] + dnutildedxj[2] * dnutildedxj[2]);
    Scalar BB = cb1 * Stilde * maxmutilde;
    Scalar CC = - cw1 * fw * oorho * maxmutilde * maxmutilde * ood2wall2;
    Scalar DD = - oorho * mu5 * (dnutildedxj[0] * drhodxj[0] + dnutildedxj[1] * drhodxj[1] + dnutildedxj[2] * drhodxj[2]);
    Scalar S = AA + BB + CC + DD;

    for(int i=0; i<4; ++i) for(int j=0; j<3; ++j) ds5ddp1dxj[i][j] = S.derivatives()[3*i+j];
    for(int i=0; i<3; ++i) for(int j=0; j<3; ++j) ds5ddudxj[i][j] = S.derivatives()[12+3*i+j];
    ds5dmul = S.derivatives()[21];
    ds5dmutilde = S.derivatives()[22];
    ds5dMach = S.derivatives()[23];
    ds5dd2wall = S.derivatives()[24];
  }
}

//------------------------------------------------------------------------------

inline
void SATerm::computeJacobianVolumeTermSA(double dp1dxj[4][3], double d2wall, double dudxj[3][3],
                                         double mul, double mutilde, double *V[4],
                                         double (*dRdU)[3][1][1], double (*dSdU)[1][1]) {
  for(int i = 0; i < 4; ++i) {
    for(int j = 0; j < 1; ++j) {
      for(int k = 0; k < 1; ++k) {
        dSdU[i][j][k] = 0;
      }
    }
  }

  double rho = fourth * (V[0][0] + V[1][0] + V[2][0] + V[3][0]);
  double drhodxj[3] = {dp1dxj[0][0]*V[0][0] + dp1dxj[1][0]*V[1][0] +
                       dp1dxj[2][0]*V[2][0] + dp1dxj[3][0]*V[3][0],
                       dp1dxj[0][1]*V[0][0] + dp1dxj[1][1]*V[1][0] +
                       dp1dxj[2][1]*V[2][0] + dp1dxj[3][1]*V[3][0],
                       dp1dxj[0][2]*V[0][0] + dp1dxj[1][2]*V[1][0] +
                       dp1dxj[2][2]*V[2][0] + dp1dxj[3][2]*V[3][0]};

  double oorho, chi, chi3, fv1, fv2, fv3, ood2wall2, zz, s12, s23, s31, s, rr, Stilde, Sbar,
         rr2, gg, gg6, fw, ft2, fn;
  commonPart(d2wall, dudxj, mul, mutilde, rho, ooreynolds_mu, oorho, chi, chi3, fv1, fv2, fv3, ood2wall2,
             zz, s12, s23, s31, s, rr, Stilde, Sbar, rr2, gg, gg6, fw, ft2, fn);

  double dchi = 0.25 / mul;
  double dfn = (useneg && mutilde < 0) ? 6 * cn1 * chi * chi * dchi / ((cn1 - chi3)*(cn1 - chi3)) : 0;
  double mu5 = oosigma * (mul + fn * mutilde);
  double dmu5 = (useneg && mutilde < 0) ? oosigma * (fn * 0.25 + dfn * mutilde) : oosigma * 0.25;
  double dnutildedxj[3] = {dp1dxj[0][0] * V[0][5] + dp1dxj[1][0] * V[1][5] +
                           dp1dxj[2][0] * V[2][5] + dp1dxj[3][0] * V[3][5],
                           dp1dxj[0][1] * V[0][5] + dp1dxj[1][1] * V[1][5] +
                           dp1dxj[2][1] * V[2][5] + dp1dxj[3][1] * V[3][5],
                           dp1dxj[0][2] * V[0][5] + dp1dxj[1][2] * V[1][5] +
                           dp1dxj[2][2] * V[2][5] + dp1dxj[3][2] * V[3][5]};

  double drdx = dmu5 * dnutildedxj[0];
  double drdy = dmu5 * dnutildedxj[1];
  double drdz = dmu5 * dnutildedxj[2];
  for(int k = 0; k < 4; ++k) {
    double nu = mu5 / V[k][0];
    dRdU[k][0][0][0] = drdx + nu * dp1dxj[k][0];
    dRdU[k][1][0][0] = drdy + nu * dp1dxj[k][1];
    dRdU[k][2][0][0] = drdz + nu * dp1dxj[k][2];
  }
  if(d2wall <= d2wall_eps) {
    for(int k = 0; k < 4; ++k) {
      dSdU[k][0][0] = 0.0;
    }
    return;
  }
  if(d2wall < ed2wall_eps) d2wall = ed2wall_eps;
  double dft2, dStilde, dfw;
  if(!useneg || mutilde >= 0) {
    dft2 = -2 * ct3 * ct4 * chi * std::exp(-ct4 * chi * chi) * dchi;
    double chi2 = chi * chi;
    double coef1 = 1.0 / (chi3 + cv1_pow3);
    double dfv1 = 3.0 * chi2 * dchi * cv1_pow3 * coef1 * coef1;
    double coef2 = 1.0 / (1.0 + chi * oocv2);
    double coef3 = coef2 * coef2;
    double dfv2, dfv3;
    if(usefv3) {
      dfv2 = -3.0 * dchi * oocv2 * coef3 * coef3;
      dfv3 = (chi == 0.0) ? 0.0 : ((dchi * fv1 + chi * dfv1) * (1.0 - fv2) - (1.0 + chi * fv1) * dfv2 - fv3 * dchi) / chi;
    }
    else {
      dfv2 = (chi * chi * dfv1 - dchi) / ((1.0 + chi * fv1) * (1.0 + chi * fv1));
      dfv3 = 0.0;
    }
    double dSbar = ooreynolds_mu * oovkcst2 * oorho * ood2wall2 * (fv2 + mutilde * dfv2);
    if(Sbar >= -c2 * s)
      dStilde = s*dfv3 + dSbar;
    else
      dStilde = s * dfv3 + s * (c2 * c2 * s + c3 * dSbar) / ((c3 - 2.0 * c2) * s - Sbar)
              + s * (c2 * c2 * s + c3 * Sbar) / (((c3 - 2.0 * c2) * s - Sbar) * ((c3 - 2.0 * c2) * s - Sbar)) * dSbar;
    double drr;
    if(rr == rlim)
      drr = 0.0;
    else
      drr = ooreynolds_mu * oovkcst2 * oorho * ood2wall2 * (Stilde - mutilde * dStilde) / (Stilde * Stilde);

    if(gg > 1e40) {
      dfw = 0;
    } 
    else {
      double dgg = (1.0 + cw2 * (6.0 * rr2 * rr2 * rr - 1.0)) * drr;
      dfw = cw3_pow6 * opcw3_pow * dgg / std::pow(gg6 + cw3_pow6, 7.0 * sixth);
    }
  }

  double dBB, dCC;
  if(useneg && mutilde < 0) {
    dBB = cb1 * (1 - ct3) * s * 0.25;
    dCC = cw1 * oorho * 2.0 * mutilde * 0.25 * ood2wall2;
  }
  else {
    dBB = cb1 * (1 - ft2) * dStilde * mutilde + cb1 * (1 - ft2) * Stilde * 0.25 - cb1 * dft2 * Stilde * mutilde;
    dCC = - (cw1 * dfw - cb1 * oovkcst2 * ooreynolds_mu * dft2) * oorho * mutilde * mutilde * ood2wall2
          - (cw1 * fw - cb1 * oovkcst2 * ooreynolds_mu * ft2) * oorho * 2.0 * mutilde * 0.25 * ood2wall2;
  }

  double s00 = -(dBB + dCC);
  double coef4 = oosigma * cb2 * rho * 2.0;
  double coef5 = oorho * dmu5 * oosigma * (fn * 0.25 + dfn * mutilde) *
                 (dnutildedxj[0] * drhodxj[0] + dnutildedxj[1] * drhodxj[1] + dnutildedxj[2] * drhodxj[2]);
  for(int k = 0; k < 4; ++k) {
    dSdU[k][0][0] = - s00 + coef4 / V[k][0] *
                    (dnutildedxj[0] * dp1dxj[k][0] + dnutildedxj[1] * dp1dxj[k][1] + dnutildedxj[2] * dp1dxj[k][2])
                    - coef5 - oorho * mu5 / V[k][0] *
                    (drhodxj[0] * dp1dxj[k][0] + drhodxj[1] * dp1dxj[k][1] + drhodxj[2]*dp1dxj[k][2]);
  }
}

//------------------------------------------------------------------------------

inline
void SATerm::computeJacobianVolumeTermSA(double dp1dxj[4][3], double d2wall, double dudxj[3][3],
                                         double mul, double dmul[4][6], double mutilde, double dmutilde[4][6],
                                         double *V[4], double (*dRdU)[3][6][6], double (*dSdU)[6][6]) {
  for(int i = 0; i < 4; ++i) {
    for(int j = 0; j < 6; ++j) {
      for(int k = 0; k < 6; ++k) {
        dSdU[i][j][k] = 0;
      }
    }
  }

  double rho = fourth * (V[0][0] + V[1][0] + V[2][0] + V[3][0]);
  double drhodxj[3] = {dp1dxj[0][0]*V[0][0] + dp1dxj[1][0]*V[1][0] +
                       dp1dxj[2][0]*V[2][0] + dp1dxj[3][0]*V[3][0],
                       dp1dxj[0][1]*V[0][0] + dp1dxj[1][1]*V[1][0] +
                       dp1dxj[2][1]*V[2][0] + dp1dxj[3][1]*V[3][0],
                       dp1dxj[0][2]*V[0][0] + dp1dxj[1][2]*V[1][0] +
                       dp1dxj[2][2]*V[2][0] + dp1dxj[3][2]*V[3][0]};

  double oorho, chi, chi3, fv1, fv2, fv3, ood2wall2, zz, s12, s23, s31, s, rr, Stilde, Sbar,
         rr2, gg, gg6, fw, ft2, fn;
  commonPart(d2wall, dudxj, mul, mutilde, rho, ooreynolds_mu, oorho, chi, chi3, fv1, fv2, fv3, ood2wall2,
             zz, s12, s23, s31, s, rr, Stilde, Sbar, rr2, gg, gg6, fw, ft2, fn);

  double mu5 = oosigma * (mul + fn * mutilde);
  double dchi[6], dfn[6], dft2[6], dmu5[4][6];
  for(int k = 0; k < 4; ++k) {
    if(useneg && mutilde < 0) {
      // note: fn = (cn1 + chi^3) / (cn1 - chi^3), where chi = (mutilde / mul)
      //   -> dfn = 6 * cn1 * chi^2 * dchi / ((cn1 - chi^3)*(cn1 - chi^3))
      dchi[0] = - mutilde / (mul * mul) * dmul[k][0] + dmutilde[k][0] / mul;
      dchi[1] = - mutilde / (mul * mul) * dmul[k][1] + dmutilde[k][1] / mul;
      dchi[2] = - mutilde / (mul * mul) * dmul[k][2] + dmutilde[k][2] / mul;
      dchi[3] = - mutilde / (mul * mul) * dmul[k][3] + dmutilde[k][3] / mul;
      dchi[4] = - mutilde / (mul * mul) * dmul[k][4] + dmutilde[k][4] / mul;
      dchi[5] = - mutilde / (mul * mul) * dmul[k][5] + dmutilde[k][5] / mul;
      dfn[0] = 6 * cn1 * chi * chi * dchi[0] / ((cn1 - chi3)*(cn1 - chi3));
      dfn[1] = 6 * cn1 * chi * chi * dchi[1] / ((cn1 - chi3)*(cn1 - chi3));
      dfn[2] = 6 * cn1 * chi * chi * dchi[2] / ((cn1 - chi3)*(cn1 - chi3));
      dfn[3] = 6 * cn1 * chi * chi * dchi[3] / ((cn1 - chi3)*(cn1 - chi3));
      dfn[4] = 6 * cn1 * chi * chi * dchi[4] / ((cn1 - chi3)*(cn1 - chi3));
      dfn[5] = 6 * cn1 * chi * chi * dchi[5] / ((cn1 - chi3)*(cn1 - chi3));
      dmu5[k][0] = oosigma * (dmul[k][0] + fn * dmutilde[k][0] + dfn[0] * mutilde);
      dmu5[k][1] = oosigma * (dmul[k][1] + fn * dmutilde[k][1] + dfn[1] * mutilde);
      dmu5[k][2] = oosigma * (dmul[k][2] + fn * dmutilde[k][2] + dfn[2] * mutilde);
      dmu5[k][3] = oosigma * (dmul[k][3] + fn * dmutilde[k][3] + dfn[3] * mutilde);
      dmu5[k][4] = oosigma * (dmul[k][4] + fn * dmutilde[k][4] + dfn[4] * mutilde);
      dmu5[k][5] = oosigma * (dmul[k][5] + fn * dmutilde[k][5] + dfn[5] * mutilde);
    }
    else {
      dmu5[k][0] = oosigma * (dmul[k][0] + dmutilde[k][0]);
      dmu5[k][1] = oosigma * (dmul[k][1] + dmutilde[k][1]);
      dmu5[k][2] = oosigma * (dmul[k][2] + dmutilde[k][2]);
      dmu5[k][3] = oosigma * (dmul[k][3] + dmutilde[k][3]);
      dmu5[k][4] = oosigma * (dmul[k][4] + dmutilde[k][4]);
      dmu5[k][5] = oosigma * (dmul[k][5] + dmutilde[k][5]);
    }
  }
  double dnutildedx = dp1dxj[0][0] * V[0][5] + dp1dxj[1][0] * V[1][5] + dp1dxj[2][0] * V[2][5] + dp1dxj[3][0] * V[3][5];
  double dnutildedy = dp1dxj[0][1] * V[0][5] + dp1dxj[1][1] * V[1][5] + dp1dxj[2][1] * V[2][5] + dp1dxj[3][1] * V[3][5];
  double dnutildedz = dp1dxj[0][2] * V[0][5] + dp1dxj[1][2] * V[1][5] + dp1dxj[2][2] * V[2][5] + dp1dxj[3][2] * V[3][5];
  for(int k = 0; k < 4; ++k) {
    dRdU[k][0][5][0] = dmu5[k][0] * dnutildedx - mu5 * dp1dxj[k][0] * V[k][5] / V[k][0];
    dRdU[k][0][5][1] = dmu5[k][1] * dnutildedx;
    dRdU[k][0][5][2] = dmu5[k][2] * dnutildedx;
    dRdU[k][0][5][3] = dmu5[k][3] * dnutildedx;
    dRdU[k][0][5][4] = dmu5[k][4] * dnutildedx;
    dRdU[k][0][5][5] = dmu5[k][5] * dnutildedx + mu5 * dp1dxj[k][0] / V[k][0];
    dRdU[k][1][5][0] = dmu5[k][0] * dnutildedy - mu5 * dp1dxj[k][1] * V[k][5] / V[k][0];
    dRdU[k][1][5][1] = dmu5[k][1] * dnutildedy;
    dRdU[k][1][5][2] = dmu5[k][2] * dnutildedy;
    dRdU[k][1][5][3] = dmu5[k][3] * dnutildedy;
    dRdU[k][1][5][4] = dmu5[k][4] * dnutildedy;
    dRdU[k][1][5][5] = dmu5[k][5] * dnutildedy + mu5 * dp1dxj[k][1] / V[k][0];
    dRdU[k][2][5][0] = dmu5[k][0] * dnutildedz - mu5 * dp1dxj[k][2] * V[k][5] / V[k][0];
    dRdU[k][2][5][1] = dmu5[k][1] * dnutildedz;
    dRdU[k][2][5][2] = dmu5[k][2] * dnutildedz;
    dRdU[k][2][5][3] = dmu5[k][3] * dnutildedz;
    dRdU[k][2][5][4] = dmu5[k][4] * dnutildedz;
    dRdU[k][2][5][5] = dmu5[k][5] * dnutildedz + mu5 * dp1dxj[k][2] / V[k][0];
  }
  if(d2wall <= d2wall_eps) {
    for(int k = 0; k < 4; ++k) {
      dSdU[k][5][0] = 0.0;
      dSdU[k][5][1] = 0.0;
      dSdU[k][5][2] = 0.0;
      dSdU[k][5][3] = 0.0;
      dSdU[k][5][4] = 0.0;
      dSdU[k][5][5] = 0.0;
    }
    return;
  }
  if(d2wall < ed2wall_eps) d2wall = ed2wall_eps;

  // derivatives
  double doorho = -fourth / (rho * rho);
  double dfv1[6], dfv2[6], dfv3[6], dzz[6], ds12[6], ds23[6], ds31[6], ds[6], dStilde[6],
         drr[6], dgg[6], dfw[6], dAA[6], dBB[6], dCC[6], dDD[6];

  // pre-computed quantities
  double mutomul2, toto1, toto2, toto3, toto4, toto5, toto6, toto7;
  if(!useneg || mutilde >= 0) {
    mutomul2 = mutilde / (mul * mul);
    toto1 = (3.0 * chi * chi * (chi3 + cv1_pow3) - chi3 * 3.0 * chi * chi) / ((chi3 + cv1_pow3) * (chi3 + cv1_pow3));
    toto2 = ooreynolds_mu * oovkcst2 * oorho * ood2wall2;
    toto3 = (Sbar >= -c2*s) ? 0 : (c2 * c2 * s + c3 * Sbar) / ((c3 - 2.0 * c2) * s - Sbar);
    toto4 = (Sbar >= -c2*s) ? 0 : s * (c2 * c2 * s + c3 * Sbar) / (((c3 - 2.0 * c2) * s - Sbar) * ((c3 - 2.0 * c2) * s - Sbar));
    toto5 = (rr == rlim || gg > 1e40) ? 0 : (1 + cw2 * (6.0 * rr * rr2 * rr2  - 1));
    toto6 = (rr == rlim || gg > 1e40) ? 0 : opcw3_pow * std::pow(gg6 + cw3_pow6, -sixth) * (1 - gg6 / (gg6 + cw3_pow6));
  }
  toto7 = dnutildedx * drhodxj[0] + dnutildedy * drhodxj[1] + dnutildedz * drhodxj[2];

  for(int k = 0; k < 4; ++k) {
    if(useneg && mutilde < 0) {
      ds12[0] = - (dp1dxj[k][1] * V[k][1] - dp1dxj[k][0] * V[k][2]) / V[k][0];
      ds12[1] = dp1dxj[k][1] / V[k][0];
      ds12[2] = - dp1dxj[k][0] / V[k][0];
      ds12[3] = 0.0;
      ds12[4] = 0.0;
      ds12[5] = 0.0;
      ds23[0] = - (dp1dxj[k][2] * V[k][2] - dp1dxj[k][1] * V[k][3]) / V[k][0];
      ds23[1] = 0.0;
      ds23[2] = dp1dxj[k][2] / V[k][0];
      ds23[3] = - dp1dxj[k][1] / V[k][0];
      ds23[4] = 0.0;
      ds23[5] = 0.0;
      ds31[0] = - (dp1dxj[k][0] * V[k][3] - dp1dxj[k][2] * V[k][1]) / V[k][0];
      ds31[1] = - dp1dxj[k][2] / V[k][0];
      ds31[2] = 0.0;
      ds31[3] = dp1dxj[k][0] / V[k][0];
      ds31[4] = 0.0;
      ds31[5] = 0.0;
      ds[0] = 1.0 / std::max(s, std::numeric_limits<double>::epsilon()) * (s12 * ds12[0] + s23 * ds23[0] + s31 * ds31[0]);
      ds[1] = 1.0 / std::max(s, std::numeric_limits<double>::epsilon()) * (s12 * ds12[1] + s23 * ds23[1] + s31 * ds31[1]);
      ds[2] = 1.0 / std::max(s, std::numeric_limits<double>::epsilon()) * (s12 * ds12[2] + s23 * ds23[2] + s31 * ds31[2]);
      ds[3] = 1.0 / std::max(s, std::numeric_limits<double>::epsilon()) * (s12 * ds12[3] + s23 * ds23[3] + s31 * ds31[3]);
      ds[4] = 1.0 / std::max(s, std::numeric_limits<double>::epsilon()) * (s12 * ds12[4] + s23 * ds23[4] + s31 * ds31[4]);
      ds[5] = 1.0 / std::max(s, std::numeric_limits<double>::epsilon()) * (s12 * ds12[5] + s23 * ds23[5] + s31 * ds31[5]);
    }
    else {
      dchi[0] = - mutomul2 * dmul[k][0] + dmutilde[k][0] / mul;
      dchi[1] = - mutomul2 * dmul[k][1] + dmutilde[k][1] / mul;
      dchi[2] = - mutomul2 * dmul[k][2] + dmutilde[k][2] / mul;
      dchi[3] = - mutomul2 * dmul[k][3] + dmutilde[k][3] / mul;
      dchi[4] = - mutomul2 * dmul[k][4] + dmutilde[k][4] / mul;
      dchi[5] = - mutomul2 * dmul[k][5] + dmutilde[k][5] / mul;
      dfv1[0] = toto1 * dchi[0];
      dfv1[1] = toto1 * dchi[1];
      dfv1[2] = toto1 * dchi[2];
      dfv1[3] = toto1 * dchi[3];
      dfv1[4] = toto1 * dchi[4];
      dfv1[5] = toto1 * dchi[5];
      if(usefv3) {
        double fv2 = 1.0 + oocv2 * chi;
        dfv2[0] = oocv2 * dchi[0];
        dfv2[1] = oocv2 * dchi[1];
        dfv2[2] = oocv2 * dchi[2];
        dfv2[3] = oocv2 * dchi[3];
        dfv2[4] = oocv2 * dchi[4];
        dfv2[5] = oocv2 * dchi[5];
        dfv2[0] = -3.0 / (fv2 * fv2 * fv2 * fv2) * dfv2[0];
        dfv2[1] = -3.0 / (fv2 * fv2 * fv2 * fv2) * dfv2[1];
        dfv2[2] = -3.0 / (fv2 * fv2 * fv2 * fv2) * dfv2[2];
        dfv2[3] = -3.0 / (fv2 * fv2 * fv2 * fv2) * dfv2[3];
        dfv2[4] = -3.0 / (fv2 * fv2 * fv2 * fv2) * dfv2[4];
        dfv2[5] = -3.0 / (fv2 * fv2 * fv2 * fv2) * dfv2[5];
        if(chi == 0.0) {
          dfv3[0] = 0.0;
          dfv3[1] = 0.0;
          dfv3[2] = 0.0;
          dfv3[3] = 0.0;
          dfv3[4] = 0.0;
          dfv3[5] = 0.0;
        }
        else {
          dfv3[0] = ((dchi[0]*fv1 + chi*dfv1[0]) * (1.0 - fv2) * chi + (1.0 + chi*fv1) * (-dfv2[0]) * chi - (1.0 + chi*fv1) * (1.0 - fv2) * dchi[0]) / (chi * chi);
          dfv3[1] = ((dchi[1]*fv1 + chi*dfv1[1]) * (1.0 - fv2) * chi + (1.0 + chi*fv1) * (-dfv2[1]) * chi - (1.0 + chi*fv1) * (1.0 - fv2) * dchi[1]) / (chi * chi);
          dfv3[2] = ((dchi[2]*fv1 + chi*dfv1[2]) * (1.0 - fv2) * chi + (1.0 + chi*fv1) * (-dfv2[2]) * chi - (1.0 + chi*fv1) * (1.0 - fv2) * dchi[2]) / (chi * chi);
          dfv3[3] = ((dchi[3]*fv1 + chi*dfv1[3]) * (1.0 - fv2) * chi + (1.0 + chi*fv1) * (-dfv2[3]) * chi - (1.0 + chi*fv1) * (1.0 - fv2) * dchi[3]) / (chi * chi);
          dfv3[4] = ((dchi[4]*fv1 + chi*dfv1[4]) * (1.0 - fv2) * chi + (1.0 + chi*fv1) * (-dfv2[4]) * chi - (1.0 + chi*fv1) * (1.0 - fv2) * dchi[4]) / (chi * chi);
          dfv3[5] = ((dchi[5]*fv1 + chi*dfv1[5]) * (1.0 - fv2) * chi + (1.0 + chi*fv1) * (-dfv2[5]) * chi - (1.0 + chi*fv1) * (1.0 - fv2) * dchi[5]) / (chi * chi);
        }
      }
      else {
        dfv2[0] = (chi * chi * dfv1[0] - dchi[0]) / ((1.0 + chi * fv1) * (1.0 + chi * fv1));
        dfv2[1] = (chi * chi * dfv1[1] - dchi[1]) / ((1.0 + chi * fv1) * (1.0 + chi * fv1));
        dfv2[2] = (chi * chi * dfv1[2] - dchi[2]) / ((1.0 + chi * fv1) * (1.0 + chi * fv1));
        dfv2[3] = (chi * chi * dfv1[3] - dchi[3]) / ((1.0 + chi * fv1) * (1.0 + chi * fv1));
        dfv2[4] = (chi * chi * dfv1[4] - dchi[4]) / ((1.0 + chi * fv1) * (1.0 + chi * fv1));
        dfv2[5] = (chi * chi * dfv1[5] - dchi[5]) / ((1.0 + chi * fv1) * (1.0 + chi * fv1));
        dfv3[0] = 0.;
        dfv3[1] = 0.;
        dfv3[2] = 0.;
        dfv3[3] = 0.;
        dfv3[4] = 0.;
        dfv3[5] = 0.;
      }
      dzz[0] = toto2 * dmutilde[k][0] + ooreynolds_mu * oovkcst2 * mutilde * doorho * ood2wall2;
      dzz[1] = toto2 * dmutilde[k][1];
      dzz[2] = toto2 * dmutilde[k][2];
      dzz[3] = toto2 * dmutilde[k][3];
      dzz[4] = toto2 * dmutilde[k][4];
      dzz[5] = toto2 * dmutilde[k][5];
      ds12[0] = - (dp1dxj[k][1] * V[k][1] - dp1dxj[k][0] * V[k][2]) / V[k][0];
      ds12[1] = dp1dxj[k][1] / V[k][0];
      ds12[2] = - dp1dxj[k][0] / V[k][0];
      ds12[3] = 0.0;
      ds12[4] = 0.0;
      ds12[5] = 0.0;
      ds23[0] = - (dp1dxj[k][2] * V[k][2] - dp1dxj[k][1] * V[k][3]) / V[k][0];
      ds23[1] = 0.0;
      ds23[2] = dp1dxj[k][2] / V[k][0];
      ds23[3] = - dp1dxj[k][1] / V[k][0];
      ds23[4] = 0.0;
      ds23[5] = 0.0;
      ds31[0] = - (dp1dxj[k][0] * V[k][3] - dp1dxj[k][2] * V[k][1]) / V[k][0];
      ds31[1] = - dp1dxj[k][2] / V[k][0];
      ds31[2] = 0.0;
      ds31[3] = dp1dxj[k][0] / V[k][0];
      ds31[4] = 0.0;
      ds31[5] = 0.0;
      ds[0] = 1.0 / std::max(s, std::numeric_limits<double>::epsilon()) * (s12 * ds12[0] + s23 * ds23[0] + s31 * ds31[0]);
      ds[1] = 1.0 / std::max(s, std::numeric_limits<double>::epsilon()) * (s12 * ds12[1] + s23 * ds23[1] + s31 * ds31[1]);
      ds[2] = 1.0 / std::max(s, std::numeric_limits<double>::epsilon()) * (s12 * ds12[2] + s23 * ds23[2] + s31 * ds31[2]);
      ds[3] = 1.0 / std::max(s, std::numeric_limits<double>::epsilon()) * (s12 * ds12[3] + s23 * ds23[3] + s31 * ds31[3]);
      ds[4] = 1.0 / std::max(s, std::numeric_limits<double>::epsilon()) * (s12 * ds12[4] + s23 * ds23[4] + s31 * ds31[4]);
      ds[5] = 1.0 / std::max(s, std::numeric_limits<double>::epsilon()) * (s12 * ds12[5] + s23 * ds23[5] + s31 * ds31[5]);
      if(Sbar >= -c2*s) {
        dStilde[0] = ds[0] * fv3 + s * dfv3[0] + dzz[0] * fv2 + zz * dfv2[0];
        dStilde[1] = ds[1] * fv3 + s * dfv3[1] + dzz[1] * fv2 + zz * dfv2[1];
        dStilde[2] = ds[2] * fv3 + s * dfv3[2] + dzz[2] * fv2 + zz * dfv2[2];
        dStilde[3] = ds[3] * fv3 + s * dfv3[3] + dzz[3] * fv2 + zz * dfv2[3];
        dStilde[4] = ds[4] * fv3 + s * dfv3[4] + dzz[4] * fv2 + zz * dfv2[4];
        dStilde[5] = ds[5] * fv3 + s * dfv3[5] + dzz[5] * fv2 + zz * dfv2[5];
      }
      else {
        dStilde[0] = ds[0] * fv3 + s * dfv3[0] + ds[0] * toto3
                   + s * (c2 * c2 * ds[0] + c3 * (dzz[0] * fv2 + zz * dfv2[0])) / ((c3 - 2.0 * c2) * s - Sbar)
                   - toto4 * ((c3 - 2.0 * c2) * ds[0] - (dzz[0] * fv2 + zz * dfv2[0]));
        dStilde[1] = ds[1] * fv3 + s * dfv3[1] + ds[1] * toto3
                   + s * (c2 * c2 * ds[1] + c3 * (dzz[1] * fv2 + zz * dfv2[1])) / ((c3 - 2.0 * c2) * s - Sbar)
                   - toto4 * ((c3 - 2.0 * c2) * ds[1] - (dzz[1] * fv2 + zz * dfv2[1]));
        dStilde[2] = ds[2] * fv3 + s * dfv3[2] + ds[2] * toto3
                   + s * (c2 * c2 * ds[2] + c3 * (dzz[2] * fv2 + zz * dfv2[2])) / ((c3 - 2.0 * c2) * s - Sbar)
                   - toto4 * ((c3 - 2.0 * c2) * ds[2] - (dzz[2] * fv2 + zz * dfv2[2]));
        dStilde[3] = ds[3] * fv3 + s * dfv3[3] + ds[3] * toto3
                   + s * (c2 * c2 * ds[3] + c3 * (dzz[3] * fv2 + zz * dfv2[3])) / ((c3 - 2.0 * c2) * s - Sbar)
                   - toto4 * ((c3 - 2.0 * c2) * ds[3] - (dzz[3] * fv2 + zz * dfv2[3]));
        dStilde[4] = ds[4] * fv3 + s * dfv3[4] + ds[4] * toto3
                   + s * (c2 * c2 * ds[4] + c3 * (dzz[4] * fv2 + zz * dfv2[4])) / ((c3 - 2.0 * c2) * s - Sbar)
                   - toto4 * ((c3 - 2.0 * c2) * ds[4] - (dzz[4] * fv2 + zz * dfv2[4]));
        dStilde[5] = ds[5] * fv3 + s * dfv3[5] + ds[5] * toto3
                   + s * (c2 * c2 * ds[5] + c3 * (dzz[5] * fv2 + zz * dfv2[5])) / ((c3 - 2.0 * c2) * s - Sbar)
                   - toto4 * ((c3 - 2.0 * c2) * ds[5] - (dzz[5] * fv2 + zz * dfv2[5]));
      }
      if(rr == rlim || gg > 1e40) {
        dfw[0] = 0.0;
        dfw[1] = 0.0;
        dfw[2] = 0.0;
        dfw[3] = 0.0;
        dfw[4] = 0.0;
        dfw[5] = 0.0;
      }
      else {
        drr[0] = (dzz[0] * Stilde - zz * dStilde[0]) / (Stilde * Stilde);
        drr[1] = (dzz[1] * Stilde - zz * dStilde[1]) / (Stilde * Stilde);
        drr[2] = (dzz[2] * Stilde - zz * dStilde[2]) / (Stilde * Stilde);
        drr[3] = (dzz[3] * Stilde - zz * dStilde[3]) / (Stilde * Stilde);
        drr[4] = (dzz[4] * Stilde - zz * dStilde[4]) / (Stilde * Stilde);
        drr[5] = (dzz[5] * Stilde - zz * dStilde[5]) / (Stilde * Stilde);
        dgg[0] = toto5 * drr[0];
        dgg[1] = toto5 * drr[1];
        dgg[2] = toto5 * drr[2];
        dgg[3] = toto5 * drr[3];
        dgg[4] = toto5 * drr[4];
        dgg[5] = toto5 * drr[5];
        dfw[0] = toto6 * dgg[0];
        dfw[1] = toto6 * dgg[1];
        dfw[2] = toto6 * dgg[2];
        dfw[3] = toto6 * dgg[3];
        dfw[4] = toto6 * dgg[4];
        dfw[5] = toto6 * dgg[5];
      }
      // ft2 = ct3 * exp(-ct4 * chi * chi)
      dft2[0] = -2 * ct3 * ct4 * chi * std::exp(-ct4 * chi * chi) * dchi[0];
      dft2[1] = -2 * ct3 * ct4 * chi * std::exp(-ct4 * chi * chi) * dchi[1];
      dft2[2] = -2 * ct3 * ct4 * chi * std::exp(-ct4 * chi * chi) * dchi[2];
      dft2[3] = -2 * ct3 * ct4 * chi * std::exp(-ct4 * chi * chi) * dchi[3];
      dft2[4] = -2 * ct3 * ct4 * chi * std::exp(-ct4 * chi * chi) * dchi[4];
      dft2[5] = -2 * ct3 * ct4 * chi * std::exp(-ct4 * chi * chi) * dchi[5];
    }
    dAA[0] = oosigma * cb2 * fourth * (dnutildedx * dnutildedx + dnutildedy * dnutildedy + dnutildedz * dnutildedz) - oosigma * cb2 * rho * 2.0 *
             (dnutildedx * dp1dxj[k][0] * V[k][5] + dnutildedy * dp1dxj[k][1] * V[k][5] + dnutildedz * dp1dxj[k][2] * V[k][5]) / V[k][0];
    dAA[1] = 0.0;
    dAA[2] = 0.0;
    dAA[3] = 0.0;
    dAA[4] = 0.0;
    dAA[5] = oosigma * cb2 * rho * 2.0 * (dnutildedx * dp1dxj[k][0] + dnutildedy * dp1dxj[k][1] + dnutildedz * dp1dxj[k][2]) / V[k][0];
    if(useneg && mutilde < 0) {
      dBB[0] = cb1 * (1 - ct3) * ds[0] * mutilde + cb1 * (1 - ct3) * s * dmutilde[k][0];
      dBB[1] = cb1 * (1 - ct3) * ds[1] * mutilde + cb1 * (1 - ct3) * s * dmutilde[k][1];
      dBB[2] = cb1 * (1 - ct3) * ds[2] * mutilde + cb1 * (1 - ct3) * s * dmutilde[k][2];
      dBB[3] = cb1 * (1 - ct3) * ds[3] * mutilde + cb1 * (1 - ct3) * s * dmutilde[k][3];
      dBB[4] = cb1 * (1 - ct3) * ds[4] * mutilde + cb1 * (1 - ct3) * s * dmutilde[k][4];
      dBB[5] = cb1 * (1 - ct3) * ds[5] * mutilde + cb1 * (1 - ct3) * s * dmutilde[k][5];
      dCC[0] = cw1 * oorho * 2.0 * mutilde * dmutilde[k][0] * ood2wall2 + cw1 * doorho * mutilde * mutilde * ood2wall2;
      dCC[1] = cw1 * oorho * 2.0 * mutilde * dmutilde[k][1] * ood2wall2;
      dCC[2] = cw1 * oorho * 2.0 * mutilde * dmutilde[k][2] * ood2wall2;
      dCC[3] = cw1 * oorho * 2.0 * mutilde * dmutilde[k][3] * ood2wall2;
      dCC[4] = cw1 * oorho * 2.0 * mutilde * dmutilde[k][4] * ood2wall2;
      dCC[5] = cw1 * oorho * 2.0 * mutilde * dmutilde[k][5] * ood2wall2;
    }
    else {
      dBB[0] = cb1 * (1 - ft2) * dStilde[0] * mutilde + cb1 * (1 - ft2) * Stilde * dmutilde[k][0] - cb1 * dft2[0] * Stilde * mutilde;
      dBB[1] = cb1 * (1 - ft2) * dStilde[1] * mutilde + cb1 * (1 - ft2) * Stilde * dmutilde[k][1] - cb1 * dft2[1] * Stilde * mutilde;
      dBB[2] = cb1 * (1 - ft2) * dStilde[2] * mutilde + cb1 * (1 - ft2) * Stilde * dmutilde[k][2] - cb1 * dft2[2] * Stilde * mutilde;
      dBB[3] = cb1 * (1 - ft2) * dStilde[3] * mutilde + cb1 * (1 - ft2) * Stilde * dmutilde[k][3] - cb1 * dft2[3] * Stilde * mutilde;
      dBB[4] = cb1 * (1 - ft2) * dStilde[4] * mutilde + cb1 * (1 - ft2) * Stilde * dmutilde[k][4] - cb1 * dft2[4] * Stilde * mutilde;
      dBB[5] = cb1 * (1 - ft2) * dStilde[5] * mutilde + cb1 * (1 - ft2) * Stilde * dmutilde[k][5] - cb1 * dft2[5] * Stilde * mutilde;
      dCC[0] = - (cw1 * dfw[0] - cb1 * oovkcst2 * ooreynolds_mu * dft2[0]) * oorho * mutilde * mutilde * ood2wall2
               - (cw1 * fw - cb1 * oovkcst2 * ooreynolds_mu * ft2) * oorho * 2.0 * mutilde * dmutilde[k][0] * ood2wall2
               - (cw1 * fw - cb1 * oovkcst2 * ooreynolds_mu * ft2) * doorho * mutilde * mutilde * ood2wall2;
      dCC[1] = - (cw1 * dfw[1] - cb1 * oovkcst2 * ooreynolds_mu * dft2[1]) * oorho * mutilde * mutilde * ood2wall2
               - (cw1 * fw - cb1 * oovkcst2 * ooreynolds_mu * ft2) * oorho * 2.0 * mutilde * dmutilde[k][1] * ood2wall2;
      dCC[2] = - (cw1 * dfw[2] - cb1 * oovkcst2 * ooreynolds_mu * dft2[2]) * oorho * mutilde * mutilde * ood2wall2
               - (cw1 * fw - cb1 * oovkcst2 * ooreynolds_mu * ft2) * oorho * 2.0 * mutilde * dmutilde[k][2] * ood2wall2;
      dCC[3] = - (cw1 * dfw[3] - cb1 * oovkcst2 * ooreynolds_mu * dft2[3]) * oorho * mutilde * mutilde * ood2wall2
               - (cw1 * fw - cb1 * oovkcst2 * ooreynolds_mu * ft2) * oorho * 2.0 * mutilde * dmutilde[k][3] * ood2wall2;
      dCC[4] = - (cw1 * dfw[4] - cb1 * oovkcst2 * ooreynolds_mu * dft2[4]) * oorho * mutilde * mutilde * ood2wall2
               - (cw1 * fw - cb1 * oovkcst2 * ooreynolds_mu * ft2) * oorho * 2.0 * mutilde * dmutilde[k][4] * ood2wall2;
      dCC[5] = - (cw1 * dfw[5] - cb1 * oovkcst2 * ooreynolds_mu * dft2[5]) * oorho * mutilde * mutilde * ood2wall2
               - (cw1 * fw - cb1 * oovkcst2 * ooreynolds_mu * ft2) * oorho * 2.0 * mutilde * dmutilde[k][5] * ood2wall2;
    }
    dDD[0] = - (doorho * mu5 * toto7 + oorho * dmu5[k][0] * toto7
             + oorho * mu5 * (dnutildedx * dp1dxj[k][0] + dnutildedy * dp1dxj[k][1] + dnutildedz * dp1dxj[k][2]))
             + oorho * mu5 * (drhodxj[0] * dp1dxj[k][0] * V[k][5] + drhodxj[1] * dp1dxj[k][1] * V[k][5] + drhodxj[2] * dp1dxj[k][2] * V[k][5]) / V[k][0];
    dDD[1] = - (oorho * dmu5[k][1] * toto7);
    dDD[2] = - (oorho * dmu5[k][2] * toto7);
    dDD[3] = - (oorho * dmu5[k][3] * toto7);
    dDD[4] = - (oorho * dmu5[k][4] * toto7);
    dDD[5] = - (oorho * dmu5[k][5] * toto7
             + oorho * mu5 * (drhodxj[0] * dp1dxj[k][0] + drhodxj[1] * dp1dxj[k][1] + drhodxj[2] * dp1dxj[k][2]) / V[k][0]);
    dSdU[k][5][0] = dAA[0] + dBB[0] + dCC[0] + dDD[0];
    dSdU[k][5][1] = dAA[1] + dBB[1] + dCC[1] + dDD[1];
    dSdU[k][5][2] = dAA[2] + dBB[2] + dCC[2] + dDD[2];
    dSdU[k][5][3] = dAA[3] + dBB[3] + dCC[3] + dDD[3];
    dSdU[k][5][4] = dAA[4] + dBB[4] + dCC[4] + dDD[4];
    dSdU[k][5][5] = dAA[5] + dBB[5] + dCC[5] + dDD[5];
  }
}

//------------------------------------------------------------------------------

inline
void SATerm::computeDistanceDerivativeOfVolumeTermSA(double _d2wall, double dudxj[3][3],
                                                     double mul, double mutilde, double *V[4], double& dS) {
  typedef Eigen::AutoDiffScalar<Eigen::Matrix<double, 1, 1>> Scalar;
  using std::max;
  double maxmutilde = std::max(mutilde, mut_eps);
  double mu5 = oosigma * (mul + maxmutilde);
  if(_d2wall <= d2wall_eps) {
    dS = 0.0;
    return;
  }
  if(_d2wall < ed2wall_eps) _d2wall = ed2wall_eps;
  double rho = fourth * (V[0][0] + V[1][0] + V[2][0] + V[3][0]);
  double oorho, chi, chi3, fv1, fv2, fv3, s12, s23, s31, s, ft2, fn; // are input, or do not depend on d2wall
  Scalar ood2wall2, zz, rr, Stilde, Sbar, rr2, gg, gg6, fw; // depend on d2wall
  Scalar d2wall = _d2wall;
  d2wall.derivatives()[0] = 1.0;
  commonPart(d2wall, dudxj, mul, maxmutilde, rho, ooreynolds_mu, oorho, chi, chi3, fv1, fv2, fv3, ood2wall2,
             zz, s12, s23, s31, s, rr, Stilde, Sbar, rr2, gg, gg6, fw, ft2, fn);

  Scalar BB = cb1 * Stilde * maxmutilde;
  Scalar CC = - cw1 * fw * oorho * maxmutilde * maxmutilde * ood2wall2;
  dS = (BB + CC).derivatives()[0];
}

//------------------------------------------------------------------------------

#endif

#ifndef _CONTROL_INTERFACE_H_
#define _CONTROL_INTERFACE_H_

#include <iostream>

class ControlInterface {

  protected:
    double dt;         // time step size
    int cpuNum;        // process ID

  public:

    // set time step
    void setDt(double h) { dt = h; }

    // set process ID
    void setCPUNum(int n) { cpuNum = n; }

    // Control force/control surface mode value and rate routine (at this point only for linearized)
    // TODO: Add acceleration sensors
    virtual void ctrlSurfaceDeflection(double *disp, double *vel, double *delUcs, double *delYcs, double time = 0) = 0;
    virtual void ctrlForce(double *disp, double *vel, double *f, double time = 0) = 0;

    // User defined displacement routine
    // TODO: hook into nonlinear
    // TODO: use SVec?
    virtual void usd_disp(double time, int glSubID, int N, const int *glNode,
                          const double *x0, double *x, double *xDot = nullptr) = 0;
};

#endif

#ifndef _GEN_MATRIX_H_
#define _GEN_MATRIX_H_

#include <Vector.h>

//------------------------------------------------------------------------------
// base class containing the BSR storage for MvpMat and SparseMat classes
template<class Scalar, int Dim>
class GenMat {

 protected:
  typedef SVec<Scalar, (Dim==Eigen::Dynamic?Eigen::Dynamic:Dim*Dim)> MatrixType;
  MatrixType a;
  int dim;

 public:
  GenMat(int _n, int _dim) : a(_n, _dim*_dim), dim(_dim) {}
  template<class Scalar2>
  GenMat(const GenMat<Scalar2, Dim>& x) : a(x.ref()), dim(x.neq()) {}
  virtual ~GenMat() {}

  virtual void setZero() = 0;

  virtual int numNonZeroBlocks() const = 0;

  const MatrixType& ref() const {
    return a;
  }

  int neq() const {
    return dim;
  }

  int neq2() const {
    return dim*dim;
  }

};

//------------------------------------------------------------------------------

#endif

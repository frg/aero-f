#ifndef _FLUX_FCN_DESC_H_
#define _FLUX_FCN_DESC_H_

#include <FluxFcnBase.h>
#include <Eigen/Core>

class VarFcnBase;
class IoData;

//------------------------------------------------------------------------------

class FluxFcnFDJacRoeEuler3D : public FluxFcnFD<5> {

 protected:
  double gamma;

 public:
  FluxFcnFDJacRoeEuler3D(IoData& ioData, double gg, VarFcnBase *vf):
    FluxFcnFD<5>(vf) {
    sprec.setup(ioData), gamma = gg;
  }
  ~FluxFcnFDJacRoeEuler3D() {}

};

//------------------------------------------------------------------------------

class FluxFcnApprJacRoeEuler3D : public FluxFcnBase {

 protected:
  double gamma;

 public:
  FluxFcnApprJacRoeEuler3D(IoData& ioData, int rs, double gg, VarFcnBase *vf) :
    FluxFcnBase(vf) {
    sprec.setup(ioData), rshift = rs;
    gamma = gg;
  }
  ~FluxFcnApprJacRoeEuler3D() {}

};

//------------------------------------------------------------------------------

class FluxFcnExactJacRoeEuler3D : public FluxFcnBase {

 protected:
  double gamma;

 public:
  FluxFcnExactJacRoeEuler3D(IoData& ioData, double gg, VarFcnBase *vf) :
    FluxFcnBase(vf) {
    sprec.setup(ioData);
    gamma = gg;
  }
  ~FluxFcnExactJacRoeEuler3D() {}

};

//------------------------------------------------------------------------------

class FluxFcnFDJacHLLEEuler3D : public FluxFcnFD<5> {

 protected:
  double gamma;

 public:
  FluxFcnFDJacHLLEEuler3D(IoData& ioData, double gg, VarFcnBase *vf) :
    FluxFcnFD<5>(vf) {
    sprec.setup(ioData), gamma = gg;
  }
  ~FluxFcnFDJacHLLEEuler3D() {}

};

//------------------------------------------------------------------------------

class FluxFcnApprJacHLLEEuler3D : public FluxFcnBase {

 protected:
  double gamma;

 public:
  FluxFcnApprJacHLLEEuler3D(IoData& ioData, int rs, double gg, VarFcnBase *vf) :
    FluxFcnBase(vf) {
    sprec.setup(ioData), rshift = rs;
    gamma = gg;
  }
  ~FluxFcnApprJacHLLEEuler3D() {}

};

//------------------------------------------------------------------------------

class FluxFcnExactJacHLLEEuler3D : public FluxFcnBase {

 protected:
  double gamma;

 public:
  FluxFcnExactJacHLLEEuler3D(IoData& ioData, int rs, double gg, VarFcnBase *vf) :
    FluxFcnBase(vf) {
    sprec.setup(ioData), rshift = rs;
    gamma = gg;
  }
  ~FluxFcnExactJacHLLEEuler3D() {}

};

//------------------------------------------------------------------------------

class FluxFcnFDJacHLLCEuler3D : public FluxFcnFD<5> {

 protected:
  double gamma;

 public:
  FluxFcnFDJacHLLCEuler3D(IoData& ioData, double gg, VarFcnBase *vf) :
    FluxFcnFD<5>(vf) {
    sprec.setup(ioData), gamma = gg;
  }
  ~FluxFcnFDJacHLLCEuler3D() {}

};

//------------------------------------------------------------------------------

class FluxFcnApprJacHLLCEuler3D : public FluxFcnBase {

 protected:
  double gamma;

 public:
  FluxFcnApprJacHLLCEuler3D(IoData& ioData, int rs, double gg, VarFcnBase *vf) :
    FluxFcnBase(vf) {
    sprec.setup(ioData), rshift = rs;
    gamma = gg;
  }
  ~FluxFcnApprJacHLLCEuler3D() {}

};

//------------------------------------------------------------------------------

class FluxFcnExactJacHLLCEuler3D : public FluxFcnBase {

 protected:
  double gamma;

 public:
  FluxFcnExactJacHLLCEuler3D(IoData& ioData, int rs, double gg, VarFcnBase *vf) :
    FluxFcnBase(vf) {
    sprec.setup(ioData), rshift = rs;
    gamma = gg;
  }
  ~FluxFcnExactJacHLLCEuler3D() {}

};

//------------------------------------------------------------------------------

class FluxFcnVanLeerEuler3D : public FluxFcnBase {

 public:
  FluxFcnVanLeerEuler3D(VarFcnBase *vf) :
    FluxFcnBase(vf) {}
  ~FluxFcnVanLeerEuler3D() {}

 protected:
  void evalFlux(double, double, double *, double, double *, double *, int);
  void evalJac(double, double, double *, double, double *, double *, FluxFcnBase::Type, int);
  void evalDerivativeOfFlux(double, double, double, double *, double *, double, double, double *, double *,
                            double, double *, int);

};

//------------------------------------------------------------------------------

class FluxFcnWallEuler3D : public FluxFcnBase {

 protected:
  BcsWallData::Method method;

 public:
  FluxFcnWallEuler3D(IoData& ioData, VarFcnBase *vf) :
    FluxFcnBase(vf), method(ioData.bc.wall.method) {}
  ~FluxFcnWallEuler3D() {}

};

//------------------------------------------------------------------------------

class FluxFcnPorousWallEuler3D : public FluxFcnFD<5> {

 public:
  FluxFcnPorousWallEuler3D(VarFcnBase *vf) :
    FluxFcnFD<5>(vf) {}
  ~FluxFcnPorousWallEuler3D() {}

};

//------------------------------------------------------------------------------

class FluxFcnGhidagliaEuler3D : public FluxFcnFD<5> {

 public:
  FluxFcnGhidagliaEuler3D(VarFcnBase *vf) :
    FluxFcnFD<5>(vf) {}
  ~FluxFcnGhidagliaEuler3D() {}

};

//------------------------------------------------------------------------------

class FluxFcnOutflowEuler3D : public FluxFcnBase {

 public:
  FluxFcnOutflowEuler3D(VarFcnBase *vf) :
    FluxFcnBase(vf) {}
  ~FluxFcnOutflowEuler3D() {}

};

//------------------------------------------------------------------------------

class FluxFcnInternalInflowEuler3D : public FluxFcnBase {

 public:
  FluxFcnInternalInflowEuler3D(VarFcnBase *vf) :
    FluxFcnBase(vf) {}
  ~FluxFcnInternalInflowEuler3D() {}

};

//------------------------------------------------------------------------------

class FluxFcnDirectStateInflowEuler3D : public FluxFcnBase {
 protected:
  double alpha;
  double beta;
  bool forceAlphaBeta;

 public:
  FluxFcnDirectStateInflowEuler3D(IoData& ioData, VarFcnBase *vf) :
    FluxFcnBase(vf) {
      forceAlphaBeta = ioData.bc.inlet.forceAlphaBeta;
      alpha = ioData.bc.inlet.alpha;
      beta = ioData.bc.inlet.beta;
    }
  ~FluxFcnDirectStateInflowEuler3D() {}

};

//------------------------------------------------------------------------------

class FluxFcnMassFlowInflowEuler3D : public FluxFcnFD<5> {

 public:
  FluxFcnMassFlowInflowEuler3D(VarFcnBase *vf) :
    FluxFcnFD<5>(vf) {}
  ~FluxFcnMassFlowInflowEuler3D() {}

};

//------------------------------------------------------------------------------

class FluxFcnInternalOutflowEuler3D : public FluxFcnBase {

 public:
  FluxFcnInternalOutflowEuler3D(VarFcnBase *vf) :
    FluxFcnBase(vf) {}
  ~FluxFcnInternalOutflowEuler3D() {}

};

//------------------------------------------------------------------------------

class FluxFcnDirectStateOutflowEuler3D : public FluxFcnBase {

 public:
  FluxFcnDirectStateOutflowEuler3D(IoData& ioData, VarFcnBase *vf) :
    FluxFcnBase(vf) {}
  ~FluxFcnDirectStateOutflowEuler3D() {}

};

//------------------------------------------------------------------------------

class FluxFcnMassFlowOutflowEuler3D : public FluxFcnFD<5> {

 public:
  FluxFcnMassFlowOutflowEuler3D(VarFcnBase *vf) :
    FluxFcnFD<5>(vf) {}
  ~FluxFcnMassFlowOutflowEuler3D() {}

};

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
// turbulence

class FluxFcnFDJacRoeSA3D : public FluxFcnFD<6> {

 protected:
  double gamma;

 public:
  FluxFcnFDJacRoeSA3D(IoData& ioData, double gg, VarFcnBase *vf) :
    FluxFcnFD<6>(vf) {
    sprec.setup(ioData), gamma = gg;
  }
  ~FluxFcnFDJacRoeSA3D() {}

};

//------------------------------------------------------------------------------

class FluxFcnApprJacRoeSA3D : public FluxFcnBase {

 protected:
  double gamma;

 public:
  FluxFcnApprJacRoeSA3D(IoData& ioData, int rs, double gg, VarFcnBase *vf) :
    FluxFcnBase(vf) {
    sprec.setup(ioData), rshift = rs;
    gamma = gg;
  }
  ~FluxFcnApprJacRoeSA3D() {}

};

//------------------------------------------------------------------------------

class FluxFcnExactJacRoeSA3D : public FluxFcnBase {

 protected:
  double gamma;

 public:
  FluxFcnExactJacRoeSA3D(IoData& ioData, double gg, VarFcnBase *vf) :
    FluxFcnBase(vf) {
    sprec.setup(ioData);
    gamma = gg;
  }
  ~FluxFcnExactJacRoeSA3D() {}

};

//------------------------------------------------------------------------------

class FluxFcnFDJacHLLESA3D : public FluxFcnFD<6> {

 protected:
  double gamma;

 public:
  FluxFcnFDJacHLLESA3D(IoData& ioData, double gg, VarFcnBase *vf) :
    FluxFcnFD<6>(vf) {
    sprec.setup(ioData), gamma = gg;
  }
  ~FluxFcnFDJacHLLESA3D() {}

};

//------------------------------------------------------------------------------

class FluxFcnApprJacHLLESA3D : public FluxFcnBase {

 protected:
  double gamma;

 public:
  FluxFcnApprJacHLLESA3D(IoData& ioData, int rs, double gg, VarFcnBase *vf) :
    FluxFcnBase(vf) {
    sprec.setup(ioData), rshift = rs;
    gamma = gg;
  }
  ~FluxFcnApprJacHLLESA3D() {}

};

//------------------------------------------------------------------------------

class FluxFcnExactJacHLLESA3D : public FluxFcnBase {

 protected:
  double gamma;

 public:
  FluxFcnExactJacHLLESA3D(IoData& ioData, int rs, double gg, VarFcnBase *vf) :
    FluxFcnBase(vf) {
    sprec.setup(ioData), rshift = rs;
    gamma = gg;
  }
  ~FluxFcnExactJacHLLESA3D() {}

};

//------------------------------------------------------------------------------

class FluxFcnFDJacHLLCSA3D : public FluxFcnFD<6> {

 protected:
  double gamma;

 public:
  FluxFcnFDJacHLLCSA3D(IoData& ioData, double gg, VarFcnBase *vf) :
    FluxFcnFD<6>(vf) {
    sprec.setup(ioData), gamma = gg;
  }
  ~FluxFcnFDJacHLLCSA3D() {}

};

//------------------------------------------------------------------------------

class FluxFcnApprJacHLLCSA3D : public FluxFcnBase {

 protected:
  double gamma;

 public:
  FluxFcnApprJacHLLCSA3D(IoData& ioData, int rs, double gg, VarFcnBase *vf) :
    FluxFcnBase(vf) {
    sprec.setup(ioData), rshift = rs;
    gamma = gg;
  }
  ~FluxFcnApprJacHLLCSA3D() {}

};

//------------------------------------------------------------------------------

class FluxFcnExactJacHLLCSA3D : public FluxFcnBase {

 protected:
  double gamma;

 public:
  FluxFcnExactJacHLLCSA3D(IoData& ioData, int rs, double gg, VarFcnBase *vf) :
    FluxFcnBase(vf) {
    sprec.setup(ioData), rshift = rs;
    gamma = gg;
  }
  ~FluxFcnExactJacHLLCSA3D() {}

};

//------------------------------------------------------------------------------

class FluxFcnWallSA3D : public FluxFcnBase {

 public:
  FluxFcnWallSA3D(VarFcnBase *vf) :
    FluxFcnBase(vf) {}
  ~FluxFcnWallSA3D() {}

};

//------------------------------------------------------------------------------

class FluxFcnPorousWallSA3D : public FluxFcnFD<6> {

 public:
  FluxFcnPorousWallSA3D(VarFcnBase *vf) :
    FluxFcnFD<6>(vf) {}
  ~FluxFcnPorousWallSA3D() {}

};

//------------------------------------------------------------------------------

class FluxFcnGhidagliaSA3D : public FluxFcnFD<6> {

 public:
  FluxFcnGhidagliaSA3D(VarFcnBase *vf) :
    FluxFcnFD<6>(vf) {}
  ~FluxFcnGhidagliaSA3D() {}

};

//------------------------------------------------------------------------------

class FluxFcnOutflowSA3D : public FluxFcnBase {

 public:
  FluxFcnOutflowSA3D(VarFcnBase *vf) :
    FluxFcnBase(vf) {}
  ~FluxFcnOutflowSA3D() {}

};

//------------------------------------------------------------------------------

class FluxFcnInternalInflowSA3D : public FluxFcnBase {

 public:
  FluxFcnInternalInflowSA3D(VarFcnBase *vf) :
    FluxFcnBase(vf) {}
  ~FluxFcnInternalInflowSA3D() {}

};

//------------------------------------------------------------------------------

class FluxFcnDirectStateInflowSA3D : public FluxFcnBase {
 protected:
  double alpha;
  double beta;
  bool forceAlphaBeta;

 public:
  FluxFcnDirectStateInflowSA3D(IoData& ioData, VarFcnBase *vf) :
    FluxFcnBase(vf) {
      forceAlphaBeta = ioData.bc.inlet.forceAlphaBeta;
      alpha = ioData.bc.inlet.alpha;
      beta = ioData.bc.inlet.beta;
    }
  ~FluxFcnDirectStateInflowSA3D() {}

};

//------------------------------------------------------------------------------

class FluxFcnMassFlowInflowSA3D : public FluxFcnFD<6> {

 public:
  FluxFcnMassFlowInflowSA3D(VarFcnBase *vf) :
    FluxFcnFD<6>(vf) {}
  ~FluxFcnMassFlowInflowSA3D() {}

};

//------------------------------------------------------------------------------

class FluxFcnInternalOutflowSA3D : public FluxFcnBase {

 public:
  FluxFcnInternalOutflowSA3D(VarFcnBase *vf) :
    FluxFcnBase(vf) {}
  ~FluxFcnInternalOutflowSA3D() {}

};

//------------------------------------------------------------------------------

class FluxFcnDirectStateOutflowSA3D : public FluxFcnBase {

 public:
  FluxFcnDirectStateOutflowSA3D(IoData& ioData, VarFcnBase *vf) :
    FluxFcnBase(vf) {}
  ~FluxFcnDirectStateOutflowSA3D() {}

};

//------------------------------------------------------------------------------

class FluxFcnMassFlowOutflowSA3D : public FluxFcnFD<6> {

 public:
  FluxFcnMassFlowOutflowSA3D(VarFcnBase *vf) :
    FluxFcnFD<6>(vf) {}
  ~FluxFcnMassFlowOutflowSA3D() {}

};

//------------------------------------------------------------------------------

class FluxFcnRoeSAturb3D : public FluxFcnBase {

 protected:
  double gamma;

 public:
  FluxFcnRoeSAturb3D(IoData& ioData, double gg, VarFcnBase *vf) :
    FluxFcnBase(vf) {
    sprec.setup(ioData), gamma = gg;
  }
  ~FluxFcnRoeSAturb3D() {}

};

//------------------------------------------------------------------------------

class FluxFcnWallSAturb3D : public FluxFcnBase {

 public:
  FluxFcnWallSAturb3D(VarFcnBase *vf) :
    FluxFcnBase(vf) {}
  ~FluxFcnWallSAturb3D() {}

};

//------------------------------------------------------------------------------

class FluxFcnPorousWallSAturb3D : public FluxFcnBase {

 public:
  FluxFcnPorousWallSAturb3D(VarFcnBase *vf) :
    FluxFcnBase(vf) {}
  ~FluxFcnPorousWallSAturb3D() {}

};

//------------------------------------------------------------------------------

class FluxFcnGhidagliaSAturb3D : public FluxFcnBase {

 public:
  FluxFcnGhidagliaSAturb3D(VarFcnBase *vf) :
    FluxFcnBase(vf) {}
  ~FluxFcnGhidagliaSAturb3D() {}

};

//------------------------------------------------------------------------------

class FluxFcnOutflowSAturb3D : public FluxFcnBase {

 public:
  FluxFcnOutflowSAturb3D(VarFcnBase *vf) :
    FluxFcnBase(vf) {}
  ~FluxFcnOutflowSAturb3D() {}

};

//------------------------------------------------------------------------------

class FluxFcnInternalInflowSAturb3D : public FluxFcnBase {

 public:
  FluxFcnInternalInflowSAturb3D(VarFcnBase *vf) :
    FluxFcnBase(vf) {}
  ~FluxFcnInternalInflowSAturb3D() {}

};

//------------------------------------------------------------------------------

class FluxFcnDirectStateInflowSAturb3D : public FluxFcnBase {

 public:
  FluxFcnDirectStateInflowSAturb3D(IoData& ioData, VarFcnBase *vf) :
    FluxFcnBase(vf) {}
  ~FluxFcnDirectStateInflowSAturb3D() {}

};

//------------------------------------------------------------------------------

class FluxFcnMassFlowInflowSAturb3D : public FluxFcnBase {

 public:
  FluxFcnMassFlowInflowSAturb3D(VarFcnBase *vf) :
    FluxFcnBase(vf) {}
  ~FluxFcnMassFlowInflowSAturb3D() {}

};

//------------------------------------------------------------------------------

class FluxFcnInternalOutflowSAturb3D : public FluxFcnBase {

 public:
  FluxFcnInternalOutflowSAturb3D(VarFcnBase *vf) :
    FluxFcnBase(vf) {}
  ~FluxFcnInternalOutflowSAturb3D() {}

};

//------------------------------------------------------------------------------

class FluxFcnDirectStateOutflowSAturb3D : public FluxFcnBase {

 public:
  FluxFcnDirectStateOutflowSAturb3D(IoData& ioData, VarFcnBase *vf) :
    FluxFcnBase(vf) {}
  ~FluxFcnDirectStateOutflowSAturb3D() {}

};

//------------------------------------------------------------------------------

class FluxFcnMassFlowOutflowSAturb3D : public FluxFcnBase {

 public:
  FluxFcnMassFlowOutflowSAturb3D(VarFcnBase *vf) :
    FluxFcnBase(vf) {}
  ~FluxFcnMassFlowOutflowSAturb3D() {}

};

//------------------------------------------------------------------------------

class FluxFcnFDJacRoeKE3D : public FluxFcnFD<7> {

 protected:
  double gamma;

 public:
  FluxFcnFDJacRoeKE3D(IoData& ioData, double gg, VarFcnBase *vf) :
    FluxFcnFD<7>(vf) {
    sprec.setup(ioData), gamma = gg;
  }
  ~FluxFcnFDJacRoeKE3D() {}

};

//------------------------------------------------------------------------------

class FluxFcnApprJacRoeKE3D : public FluxFcnBase {

 protected:
  double gamma;

 public:
  FluxFcnApprJacRoeKE3D(IoData& ioData, int rs, double gg, VarFcnBase *vf) :
    FluxFcnBase(vf) {
    sprec.setup(ioData), rshift = rs;
    gamma = gg;
  }
  ~FluxFcnApprJacRoeKE3D() {}

};

//------------------------------------------------------------------------------

class FluxFcnExactJacRoeKE3D : public FluxFcnBase {

 protected:
  double gamma;

 public:
  FluxFcnExactJacRoeKE3D(IoData& ioData, double gg, VarFcnBase *vf) :
    FluxFcnBase(vf) {
    sprec.setup(ioData);
    gamma = gg;
  }
  ~FluxFcnExactJacRoeKE3D() {}

};

//------------------------------------------------------------------------------

class FluxFcnFDJacHLLEKE3D : public FluxFcnFD<7> {

 protected:
  double gamma;

 public:
  FluxFcnFDJacHLLEKE3D(IoData& ioData, double gg, VarFcnBase *vf) :
    FluxFcnFD<7>(vf) {
    sprec.setup(ioData), gamma = gg;
  }
  ~FluxFcnFDJacHLLEKE3D() {}

};

//------------------------------------------------------------------------------

class FluxFcnApprJacHLLEKE3D : public FluxFcnBase {

 protected:
  double gamma;

 public:
  FluxFcnApprJacHLLEKE3D(IoData& ioData, int rs, double gg, VarFcnBase *vf) :
    FluxFcnBase(vf) {
    sprec.setup(ioData), rshift = rs;
    gamma = gg;
  }
  ~FluxFcnApprJacHLLEKE3D() {}

};

//------------------------------------------------------------------------------

class FluxFcnExactJacHLLEKE3D : public FluxFcnBase {

 protected:
  double gamma;

 public:
  FluxFcnExactJacHLLEKE3D(IoData& ioData, int rs, double gg, VarFcnBase *vf) :
    FluxFcnBase(vf) {
    sprec.setup(ioData), rshift = rs;
    gamma = gg;
  }
  ~FluxFcnExactJacHLLEKE3D() {}

};

//------------------------------------------------------------------------------

class FluxFcnFDJacHLLCKE3D : public FluxFcnFD<7> {

 protected:
  double gamma;

 public:
  FluxFcnFDJacHLLCKE3D(IoData& ioData, double gg, VarFcnBase *vf) :
    FluxFcnFD<7>(vf) {
    sprec.setup(ioData), gamma = gg;
  }
  ~FluxFcnFDJacHLLCKE3D() {}

};

//------------------------------------------------------------------------------

class FluxFcnApprJacHLLCKE3D : public FluxFcnBase {

 protected:
  double gamma;

 public:
  FluxFcnApprJacHLLCKE3D(IoData& ioData, int rs, double gg, VarFcnBase *vf) :
    FluxFcnBase(vf) {
    sprec.setup(ioData), rshift = rs;
    gamma = gg;
  }
  ~FluxFcnApprJacHLLCKE3D() {}

};

//------------------------------------------------------------------------------

class FluxFcnExactJacHLLCKE3D : public FluxFcnBase {

 protected:
  double gamma;

 public:
  FluxFcnExactJacHLLCKE3D(IoData& ioData, int rs, double gg, VarFcnBase *vf) :
    FluxFcnBase(vf) {
    sprec.setup(ioData), rshift = rs;
    gamma = gg;
  }
  ~FluxFcnExactJacHLLCKE3D() {}

};

//------------------------------------------------------------------------------

class FluxFcnWallKE3D : public FluxFcnBase {

 public:
  FluxFcnWallKE3D(VarFcnBase *vf) :
    FluxFcnBase(vf) {}
  ~FluxFcnWallKE3D() {}

};

//------------------------------------------------------------------------------

class FluxFcnPorousWallKE3D : public FluxFcnFD<7> {

 public:
  FluxFcnPorousWallKE3D(VarFcnBase *vf) :
    FluxFcnFD<7>(vf) {}
  ~FluxFcnPorousWallKE3D() {}

};

//------------------------------------------------------------------------------

class FluxFcnGhidagliaKE3D : public FluxFcnFD<7> {

 public:
  FluxFcnGhidagliaKE3D(VarFcnBase *vf) :
    FluxFcnFD<7>(vf) {}
  ~FluxFcnGhidagliaKE3D() {}

};

//------------------------------------------------------------------------------

class FluxFcnOutflowKE3D : public FluxFcnBase {

 public:
  FluxFcnOutflowKE3D(VarFcnBase *vf) :
    FluxFcnBase(vf) {}
  ~FluxFcnOutflowKE3D() {}

};

//------------------------------------------------------------------------------

class FluxFcnDirectStateInflowKE3D : public FluxFcnBase {
 protected:
  double alpha;
  double beta;
  bool forceAlphaBeta;

 public:
  FluxFcnDirectStateInflowKE3D(IoData& ioData, VarFcnBase *vf) :
    FluxFcnBase(vf) {
      forceAlphaBeta = ioData.bc.inlet.forceAlphaBeta;
      alpha = ioData.bc.inlet.alpha;
      beta = ioData.bc.inlet.beta;
    }
  ~FluxFcnDirectStateInflowKE3D() {}

};

//------------------------------------------------------------------------------

class FluxFcnMassFlowInflowKE3D : public FluxFcnFD<7> {

 public:
  FluxFcnMassFlowInflowKE3D(VarFcnBase *vf) :
    FluxFcnFD<7>(vf) {}
  ~FluxFcnMassFlowInflowKE3D() {}

};

//------------------------------------------------------------------------------

class FluxFcnDirectStateOutflowKE3D : public FluxFcnBase {

 public:
  FluxFcnDirectStateOutflowKE3D(IoData& ioData, VarFcnBase *vf) :
    FluxFcnBase(vf) {}
  ~FluxFcnDirectStateOutflowKE3D() {}

};

//------------------------------------------------------------------------------

class FluxFcnMassFlowOutflowKE3D : public FluxFcnFD<7> {

 public:
  FluxFcnMassFlowOutflowKE3D(VarFcnBase *vf) :
    FluxFcnFD<7>(vf) {}
  ~FluxFcnMassFlowOutflowKE3D() {}

};

//------------------------------------------------------------------------------

class FluxFcnRoeKEturb3D : public FluxFcnBase {

 protected:
  double gamma;

 public:
  FluxFcnRoeKEturb3D(IoData& ioData, double gg, VarFcnBase *vf) :
    FluxFcnBase(vf) {
    sprec.setup(ioData), gamma = gg;
  }
  ~FluxFcnRoeKEturb3D() {}

};

//------------------------------------------------------------------------------

class FluxFcnWallKEturb3D : public FluxFcnBase {

 public:
  FluxFcnWallKEturb3D(VarFcnBase *vf) :
    FluxFcnBase(vf) {}
  ~FluxFcnWallKEturb3D() {}

};

//------------------------------------------------------------------------------

class FluxFcnPorousWallKEturb3D : public FluxFcnBase {

 public:
  FluxFcnPorousWallKEturb3D(VarFcnBase *vf) :
    FluxFcnBase(vf) {}
  ~FluxFcnPorousWallKEturb3D() {}

};

//------------------------------------------------------------------------------

class FluxFcnGhidagliaKEturb3D : public FluxFcnBase {

 public:
  FluxFcnGhidagliaKEturb3D(VarFcnBase *vf) :
    FluxFcnBase(vf) {}
  ~FluxFcnGhidagliaKEturb3D() {}

};

//------------------------------------------------------------------------------

class FluxFcnOutflowKEturb3D : public FluxFcnBase {

 public:
  FluxFcnOutflowKEturb3D(VarFcnBase *vf) :
    FluxFcnBase(vf) {}
  ~FluxFcnOutflowKEturb3D() {}

};

//------------------------------------------------------------------------------

class FluxFcnDirectStateInflowKEturb3D : public FluxFcnBase {

 public:
  FluxFcnDirectStateInflowKEturb3D(IoData& ioData, VarFcnBase *vf) :
    FluxFcnBase(vf) {}
  ~FluxFcnDirectStateInflowKEturb3D() {}

};

//------------------------------------------------------------------------------

class FluxFcnMassFlowInflowKEturb3D : public FluxFcnBase {

 public:
  FluxFcnMassFlowInflowKEturb3D(VarFcnBase *vf) :
    FluxFcnBase(vf) {}
  ~FluxFcnMassFlowInflowKEturb3D() {}

};

//------------------------------------------------------------------------------

class FluxFcnDirectStateOutflowKEturb3D : public FluxFcnBase {

 public:
  FluxFcnDirectStateOutflowKEturb3D(IoData& ioData, VarFcnBase *vf) :
    FluxFcnBase(vf) {}
  ~FluxFcnDirectStateOutflowKEturb3D() {}

};

//------------------------------------------------------------------------------

class FluxFcnMassFlowOutflowKEturb3D : public FluxFcnBase {

 public:
  FluxFcnMassFlowOutflowKEturb3D(VarFcnBase *vf) :
    FluxFcnBase(vf) {}
  ~FluxFcnMassFlowOutflowKEturb3D() {}

};

//------------------------------------------------------------------------------

template<int dim>
class FluxFcnRotatedRiemann : public FluxFcnBase {

  SchemeData::RotatedRiemannData& rrData;
  FluxFcnBase *ff1, *ff2, *ff3;
  double eps2;

  template<typename Scalar>
  using Vec3D_t = Eigen::Matrix<Scalar, 3, 1>;

  Vec3D_t<double> referenceDir;

  template<class Scalar>
  void computeDirections(double *, double *, Scalar *, Scalar *, int&, FluxFcnBase *[3], Vec3D_t<Scalar>[3], Scalar[3]);

 public:
  FluxFcnRotatedRiemann(FluidModelData&, IoData&, int, VarFcnBase *);
  ~FluxFcnRotatedRiemann();

  void compute(double, double, double *, double *, double, double *, double *, double *, bool);
  void computeJacobians(double, double, double *, double *, double, double *, double *, double *, double *, FluxFcnBase::Type, bool);

};

//------------------------------------------------------------------------------

template<int dim>
class FluxFcnExactRiemannWall : public FluxFcnBase {

  ExactRiemannWallData& erwData;
  FluxFcnBase *ff;

 public:
  FluxFcnExactRiemannWall(FluidModelData&, IoData&, int, VarFcnBase *);
  ~FluxFcnExactRiemannWall();

  void compute(double, double, double *, double *, double, double *, double *, double *, bool);
  void computeJacobian(double, double, double *, double *, double, double *, double *, double *, FluxFcnBase::Type, bool);

};

//------------------------------------------------------------------------------

#endif

#ifndef _PERIODIC_BOUNDARY_HANDLER_H_
#define _PERIODIC_BOUNDARY_HANDLER_H_

#include <vector>
#include <utility>

#include <Face.h>
#include <Types.h>

struct Vec3D;

class IoData;
class Domain;
class Communicator;
class DistGeoState;

template<class Scalar> class DistVec;
template<class Scalar, int dim> class DistSVec;

//------------------------------------------------------------------------------

template<int dim>
class PeriodicBoundaryHandler {

  typedef std::pair<long_int, Vec3D> send_tuple;
  typedef std::pair<long_int, std::vector<std::pair<int, int>>> recv_tuple;

 protected:
  int lastConfig;
  int numLocSub;
  Domain *domain;
  Communicator *pcom;
  DistGeoState *geoState;

  std::map<int,int> slaveMap;
  std::map<int, std::vector<send_tuple>> sendMap;
  std::map<int, std::vector<recv_tuple>> recvMap;
  std::vector<int> sendOffsets, recvOffsets;
  double (*sendBuffer)[dim], (*recvBuffer)[dim];

  std::map<int, Vec3D> periodicOffset;

 private:
  void clean();
  void initialize();

 public:
  PeriodicBoundaryHandler(Domain *, IoData&, DistGeoState *);
  ~PeriodicBoundaryHandler() { clean(); }

  void resize() { lastConfig = -1; }
  void update(DistSVec<double, dim>&, DistSVec<double, dim>&, DistSVec<double, dim>&,
              DistSVec<double, dim>&, DistSVec<double, Face::MaxNumNd * dim>&);
  void updateTags(DistVec<int>&);

  //TODO: level set @ periodic BC?

};

//------------------------------------------------------------------------------

#endif

#ifndef _FLUID_SHAPE_OPTIMIZATION_HANDLER_H_
#define _FLUID_SHAPE_OPTIMIZATION_HANDLER_H_

#include <ImplicitCoupledTsDesc.h>

#include <cmath>

#define Deg2Rad         0.01745329251994329576923
#define Rad2Deg        57.29577951308232087679815
#define perDeg2perRad  57.29577951308232087679815
#define perRad2perDeg   0.01745329251994329576923

class MeshMotionSolver;
class DistMovingLeastSquaresForceHandler;

template<int dim> class MatVecProd_dRdX;
#ifndef _VECSET_TMPL_
  #define _VECSET_TMPL_
  template<class VecType, class Scalar = double> class VecSet;
#endif

//------------------------------------------------------------------------------

template<int dim>
class FluidShapeOptimizationHandler {

 private:

  TsDesc<dim> *tsDesc;

  MeshMotionSolver *mms;

  MatVecProd<dim, dim> *mvp;
  MatVecProd_dRdX<dim> *dRdXop;
  KspPrec<dim, double> *pc;
  KspSolver<DistSVec<double, dim>, MatVecProd<dim, dim>, KspPrec<dim>, Communicator> *ksp;
  double steadyTol;

  int step;
  int actvar;

  double reynolds0;
  double kenergy0;

  double length;
  double surface;
  double mach;
  double alpha;
  double beta;

  bool machsensitivity, alphasensitivity, betasensitivity;

  bool pressureDistortion = false;
  DistMovingLeastSquaresForceHandler* mlsfh = 0;


  DistVec<double> *Ap;
  DistVec<double> *Am;

  DistSVec<double, 3> dXdSb; // derivative of the mesh mesh motion at the interface
  DistSVec<double, 3> lambdaSDisp;
  DistSVec<double, 3> dfaX;
  DistSVec<double, 3> Xc;
  DistSVec<double, 3> *Xp;
  DistSVec<double, 3> *Xm;
  DistSVec<double, 3> *Lp;
  DistSVec<double, 3> *Lm;
  DistSVec<double, 3> *load;
  DistSVec<double, 3> *dLoad;

  DistSVec<double, dim> Flux;
  DistSVec<double, dim> *Fp;
  DistSVec<double, dim> *Fm;
  DistSVec<double, dim> dFdS;
  DistSVec<double, dim> *Up;
  DistSVec<double, dim> *Um;
  DistSVec<double, dim> dUdS;
  DistSVec<double, dim> lambdaU;
  DistSVec<double, dim> dfaU;

  DistSVec<double, 3> lambdaX;

  DistSVec<double, 3> dGradP; // derivative of nodal pressure gradient

  FILE *outFile, *outFile2;
  void setSensitivityType(IoData&);

  // structural mode shapes for computing sensitivities of modal force
  VecSet<DistSVec<double, 3>> *mX;
  int nStrMode;

 public:
  FluidShapeOptimizationHandler(IoData&, GeoSource&, Domain *, TsDesc<dim> *);
  ~FluidShapeOptimizationHandler();

  void fsoHandler(IoData&, DistSVec<double, dim>&);
  void fsoAeroelasticHandler(IoData&, DistSVec<double, dim>&);

 protected:
  void fsoSetUpLinearSolver(IoData&, DistSVec<double, dim>&);
  void fso_on_sensitivityMach(bool, IoData&, DistSVec<double, dim>&);
  void fso_on_sensitivityAlpha(bool, IoData&, DistSVec<double, dim>&);
  void fso_on_sensitivityBeta(bool, IoData&, DistSVec<double, dim>&);
  void fso_on_sensitivityMesh(bool, IoData&, DistSVec<double, dim>&);
  void fso_on_AdjointSensitivityMesh(IoData&, DistSVec<double, dim>&);
  void fso_on_AdjointSensitivityMeshPressureDistortion(IoData&, DistSVec<double, dim>&);
  void fso_on_aeroelasticSensitivityFSI(bool, IoData&, DistSVec<double, dim>&);
  void fso_on_aeroelasticAdjointSensitivityFSI(IoData&, DistSVec<double, dim>&);

  void fsoRestartBcFluxs(IoData&);

  void fsoGetEfforts(IoData&, DistSVec<double, dim>&, Vec3D&, Vec3D&, Vec3D&);
  void fsoGetDerivativeOfEffortsFiniteDifference(IoData&, DistSVec<double, 3>&, DistSVec<double, dim>&,
                                                 DistSVec<double, dim>&, Vec3D&, Vec3D&, Vec3D&);
  void fsoGetDerivativeOfEffortsAnalytical(bool, IoData&, DistSVec<double, 3>&, DistSVec<double, dim>&, DistSVec<double, dim>&,
                                           Vec3D&, Vec3D&, Vec3D&);
  void fsoGetDerivativeOfEffortsWRTStateAndMeshPositionAnalytical(IoData&, Vec3D&, Vec3D&, Vec3D&, DistSVec<double, dim>&,
                                                                  DistSVec<double, 3>&, DistSVec<double, dim>&);
  void fsoGetDerivativeOfPressureDistortionWRTStateAnalytical(IoData&, DistSVec<double, dim>&);
  void fsoGetDerivativeOfLoadFiniteDifference(IoData&, DistSVec<double, 3>&, DistSVec<double, dim>&,
                                              DistSVec<double, dim>&, DistSVec<double, 3>&, DistSVec<double, 3>&);
  void fsoGetDerivativeOfLoadAnalytical(bool, IoData&, DistSVec<double, 3>&, DistSVec<double, dim>&, DistSVec<double, dim>&,
                                        DistSVec<double, 3>&, DistSVec<double, 3>&, bool = true);
  void fsoGetTransposeDerivativeOfLoadAnalytical(IoData&, DistSVec<double, 3>&, DistSVec<double, 3>&, DistSVec<double, dim>&);
  void fsoSemiAnalytical(IoData&, DistSVec<double, dim>&, DistSVec<double, dim>&);
  void fsoAnalytical(bool, IoData&, DistSVec<double, dim>&, DistSVec<double, dim>&);
  void fsoLinearSolver(IoData&, DistSVec<double, dim>&, DistSVec<double, dim>&, bool = false);
  void fsoAdjointLinearSolver(IoData&, DistSVec<double, dim>&, DistSVec<double, dim>&, bool = false);
  void fsoComputeDerivativesOfFluxAndSolution(IoData&, DistSVec<double, dim>&, bool = false, bool = false);
  void fsoComputeSensitivities(bool, IoData&, const char *, DistSVec<double, dim>&);
  void fsoComputeSensitivitiesPressureDistortion(IoData&, DistSVec<double, dim>&);
  void fsoComputeAdjoint(IoData&, DistSVec<double, 3>&, DistSVec<double, dim>&, bool);
  void fsoComputeAndSendForceSensitivities(bool, IoData&, DistSVec<double, dim>&);

  void Forces2Lifts(IoData& ioData, Vec3D& F, Vec3D& L) {
    double sin_a = sin(ioData.bc.inlet.alpha);
    double cos_a = cos(ioData.bc.inlet.alpha);
    double sin_b = sin(ioData.bc.inlet.beta);
    double cos_b = cos(ioData.bc.inlet.beta);
    L[0] =  F[0] * cos_a * cos_b + F[1] * cos_a * sin_b + F[2] * sin_a;
    L[1] = -F[0] * sin_b + F[1] * cos_b;
    L[2] = -F[0] * sin_a * cos_b - F[1] * sin_a * sin_b + F[2] * cos_a;
  }

  void dForces2dLifts(IoData& ioData, Vec3D& F, Vec3D& dF, Vec3D& dL) {
    Forces2Lifts(ioData, dF, dL);
    double sin_a = sin(ioData.bc.inlet.alpha);
    double cos_a = cos(ioData.bc.inlet.alpha);
    double sin_b = sin(ioData.bc.inlet.beta);
    double cos_b = cos(ioData.bc.inlet.beta);
    double dsin_a = alphasensitivity ? cos_a : 0.0, dcos_a = alphasensitivity ? -sin_a : 0.0;
    double dsin_b = betasensitivity ? cos_b : 0.0, dcos_b = betasensitivity ? -sin_b : 0.0;
    double convfac = ((ioData.sa.angleRad == ioData.sa.OFF_ANGLERAD) && (alphasensitivity || betasensitivity)) ? perRad2perDeg : 1.0;
    dL[0] += (F[0] * (dcos_a * cos_b + cos_a * dcos_b) + F[1] * (dcos_a * sin_b + cos_a * dsin_b) + F[2] * dsin_a) * convfac;
    dL[1] += (-F[0] * dsin_b + F[1] * dcos_b) * convfac;
    dL[2] += (-F[0] * (dsin_a * cos_b + sin_a * dcos_b) - F[1] * (dsin_a * sin_b + sin_a * dsin_b) + F[2] * dcos_a) * convfac;
  }

  void receiveBoundaryPositionSensitivityVector(DistSVec<double, 3>&, bool = false);
  void sendForceSensitivity(DistSVec<double, 3> *, bool = true);
  bool getdXdSb(int);

};

//------------------------------------------------------------------------------

#endif

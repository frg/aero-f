#ifndef _IMPLICIT_SEG_TS_DESC_H_
#define _IMPLICIT_SEG_TS_DESC_H_

#include <ImplicitTsDesc.h>

class IoData;
class Domain;
class GeoSource;
class BcFcn;
class FluxFcn;
class FemEquationTerm;

template<int dim> class SpaceOperator;
template<class Scalar, int dim> class DistSVec;

#ifndef _MATVECPROD_TMPL_
  #define _MATVECPROD_TMPL_
  template<int dim, int neq, class Scalar2 = double> class MatVecProd;
#endif

#ifndef _KSPPREC_TMPL_
  #define _KSPPREC_TMPL_
  template<int Dim, class Scalar2 = double> class KspPrec;
#endif

#ifndef _KSPSLVR_TMPL_
  #define _KSPSLVR_TMPL_
  template<class VecType, class MvpOp, class PrecOp, class IoOp, class ScalarT = double> class KspSolver;
#endif

//------------------------------------------------------------------------------

template<int dim, int neq1, int neq2>
class ImplicitSegTsDesc : public ImplicitTsDesc<dim> {

 protected:
  SpaceOperator<dim> *spaceOp1;
  SpaceOperator<dim> *spaceOp2;

  FluxFcn **ff1, **ff2;
  BcFcn *bf1, *bf2;
  FemEquationTerm *fet1, *fet2;

  DistSVec<double, neq1> b1, dQ1;
  DistSVec<double, neq2> b2, dQ2;

  MatVecProd<dim, neq1> *mvp1;
  MatVecProd<dim, neq2> *mvp2;

  KspPrec<neq1> *pc1;
  KspPrec<neq2> *pc2;

  KspSolver<DistSVec<double, neq1>, MatVecProd<dim, neq1>, KspPrec<neq1>, Communicator> *ksp1;
  KspSolver<DistSVec<double, neq2>, MatVecProd<dim, neq2>, KspPrec<neq2>, Communicator> *ksp2;

  MatVecProd<dim, neq1, PrecScalar> *tmvp1;
  MatVecProd<dim, neq2, PrecScalar> *tmvp2;

  SpaceOperator<dim> *createSpaceOperator1(IoData&, SpaceOperator<dim> *);
  SpaceOperator<dim> *createSpaceOperator2(IoData&, SpaceOperator<dim> *);

 public:
  ImplicitSegTsDesc(IoData&, GeoSource&, Domain *);
  ~ImplicitSegTsDesc();

  void resize1(NodeData *, DistSVec<double, dim>&);
  void resize2();

  void computeJacobian(DistSVec<double, dim>&, DistSVec<double, dim>&);
  void setOperators(DistSVec<double, dim>&);
  void solveLinearSystem(int, DistSVec<double, dim>&, DistSVec<double, dim>&);

};

//------------------------------------------------------------------------------

#endif

#ifndef _MVP_MATRIX_H_
#define _MVP_MATRIX_H_

#include <GenMatrix.h>
#include <Vector.h>
#include <Edge.h>
#include <DenseMatrix.h>

#include <utility>
#include <map>
#include <algorithm>
#include <cstring>

class Connectivity;

template<class Scalar, int Dim> class SparseMat;

//------------------------------------------------------------------------------

enum class MvpType {H1, H2};

template<class Scalar, int Dim>
class MvpMat : public GenMat<Scalar, Dim> {

 public:
  enum Type {H1, H2};

 private:
  /*
   * a[i] is a dim by dim matrix
   * n is the number of nodes
   * a[0], a[1], ... , a[n-1] are block matrices on the diagonal df_i/dU_i
   * edge l = ij has a[n + 2*l + 0] = df_ij/dU_j, a[n + 2*l + 1] = df_ji/dU_i
   */
  using GenMat<Scalar, Dim>::a;
  using GenMat<Scalar, Dim>::dim;

  int n;
  MvpType type;

  Vec<int> ia, ja;                  // BSR indexing pointers
  Vec<int> ptr_ii, ptr_ij, ptr_ji;  // maps for fast node and edge based access

  /*
   * for type = H2:
   * a2 contains the products df_ij/dV_ij * d_Vij/d(gV^N_i), df_ij/dV_ji * dV_ji/d(gV^N_j),
   *                          df_ji/dV_ij * dV_ij/d(gV^N_i), df_ji/dV_ji * dV_ji/d(gV^N_j)
   * where gV^N_i is the nodal gradient at node i
   * a2r contains the nodal gradient Jacobian d(gV^N_i)/dU_j
   * both a2 and a2r have the same sparsity pattern as a, but in blocks stacked
   * vertically (a2r) and horizontally (a2) by 3 to account for x, y, and z
   * directions of the nodal gradient
   */
  typedef SVec<Scalar, (Dim==Eigen::Dynamic?Eigen::Dynamic:Dim*Dim)> MatrixType;
  MatrixType a2, a2r;

  /*
   * for old H2 implementation (see Lesoinne et al., Comp. Meth. Appl. Mech. Eng. (2001))
   * we store only the edge contributions df_ij/dV_ij, df_ij/dV_ji, df_ji/dV_ij, df_ji/dV_ji
   * in a2 directly
   * see applyH2 in SpaceOperator class for MVP
   * optimizations for H2 + Euler equations: only include diagonal H1 terms and store only
   * half edge terms since no BC application required
   */
  const bool newH2, oldH2Euler;

  /*
   * for HPROM simulations, allow for masking of rows of the Jacobian corresponding
   * only to sample nodes in the reduced mesh
   * the quantities with suffix 'r' are special variants for the compressed H2
   * reconstruction Jacobian, which must contain more rows than just those in the mask
   */
  Vec<int> rperm, rpermr, iar, jar, ptrr_ii, ptrr_ij, ptrr_ji;
  Vec<bool> mask, maskr;

  // auxilliary storage for du_ghost/du_real
  typedef std::map<std::pair<int, int>, std::pair<Scalar *, int>> AuxilliaryRows;
  AuxilliaryRows realAuxilliaryRows, ghostAuxilliaryRows, ghostGhostAuxilliaryRows;
  // auxilliary rows used in ghost point reflection handler to communicate information to another subodmain
  typedef std::map<int,AuxilliaryRows> AuxilliaryRowsCpus;
  AuxilliaryRowsCpus ghostRealCommAuxilliaryRows, ghostGhostCommAuxilliaryRows;
  // auxilliary rows used for dependence of nodal gradients on ghost states
  AuxilliaryRows gradGhostAuxilliaryRows[3];

 public:
  MvpMat(int, int, int, MvpType, const bool = false, const bool = false);
  MvpMat(const MvpMat<Scalar, Dim>&, const bool = false);
  ~MvpMat();

  using GenMat<Scalar, Dim>::neq;
  using GenMat<Scalar, Dim>::neq2;

  int numNonZeroBlocks() const {
    int nnz = 0;
    for(auto& it : ghostRealCommAuxilliaryRows) nnz += it.second.size();
    for(auto& it : ghostGhostCommAuxilliaryRows) nnz += it.second.size();
    return nnz + a.size() + a2.size() + a2r.size() + realAuxilliaryRows.size() + ghostAuxilliaryRows.size()
           + ghostRealCommAuxilliaryRows.size() + ghostGhostCommAuxilliaryRows.size();
  }

  MvpType getType() const {
    return type;
  }

  bool isNewH2() const {
    return newH2;
  }

  bool isOldH2Euler() const {
    return oldH2Euler;
  }

  const Vec<bool>& getMask() const {
    return mask;
  }

  const Vec<bool>& getMaskR() const {
    return maskr;
  }

  void setZero() {
    std::memset(a.ref().data(), 0, a.ref().size()*sizeof(Scalar));
    if(a2.size() > 0) {
      std::memset(a2.ref().data(), 0, a2.ref().size()*sizeof(Scalar));
    }
    if(a2r.size() > 0) {
      std::memset(a2r.ref().data(), 0, a2r.ref().size()*sizeof(Scalar)); // XXX not necessary?
    }
    clearGhost();
  }

  MvpMat<Scalar, Dim> &operator*=(const Scalar s) {
    a *= s;
    if(a2.size() > 0) {
      a2 *= s;
    }
    return *this;
  }

  void clearGhost() {
    for(auto& it : realAuxilliaryRows) {
      delete[] it.second.first;
    }
    realAuxilliaryRows.clear();
    for(auto& it : ghostAuxilliaryRows) {
      delete[] it.second.first;
    }
    ghostAuxilliaryRows.clear();
    for(auto& row : ghostGhostCommAuxilliaryRows) {
      for(auto& it : row.second) {
        delete[] it.second.first;
      }
      row.second.clear();
    }
    ghostGhostCommAuxilliaryRows.clear();
    for(auto& row : ghostRealCommAuxilliaryRows) {
      for(auto& it : row.second) {
        delete[] it.second.first;
      }
      row.second.clear();
    }
    ghostRealCommAuxilliaryRows.clear();
    for(auto& it : ghostGhostAuxilliaryRows) {
      delete[] it.second.first;
    }
    ghostGhostAuxilliaryRows.clear();
    for(int dir = 0; dir < 3; dir++) {
      for(auto& it : gradGhostAuxilliaryRows[dir]) {
        delete[] it.second.first;
      }
      gradGhostAuxilliaryRows[dir].clear();
    }
  }

  void setup(Connectivity *, const EdgeSet::PtrContainer&, std::vector<bool> * = nullptr, std::vector<bool> * = nullptr);

  template<class Scalar2>
  void getData(SparseMat<Scalar2, Dim>&);

  template<class Scalar2>
  void getDataTranspose(SparseMat<Scalar2, Dim>&);

  //----------------------------------------------------------------------------
  // H1 block access methods

 public:
  // df_i/dU_i
  Scalar *getElem_ii(int i) {
    return a.row(ptr_ii[i]).data();
  }

  // df_ij/dU_j
  Scalar *getElem_ij(int l) {
    return a.row(ptr_ij[l]).data();
  }

  // df_ji/dU_i
  Scalar *getElem_ji(int l) {
    return a.row(ptr_ji[l]).data();
  }

  //----------------------------------------------------------------------------
  // Auxilliary term access for new H2 Jacobian

  // df_i/d(dgV^N)_i
  Scalar *getH2Elem_ii(int i, int d) {
    return a2.row(3 * ptr_ii[i] + d).data();
  }

  // df_ij/d(dgV^N)_j
  Scalar *getH2Elem_ij(int l, int d) {
    return a2.row(3 * ptr_ij[l] + d).data();
  }

  // df_ji/d(dgV^N)_i
  Scalar *getH2Elem_ji(int l, int d) {
    return a2.row(3 * ptr_ji[l] + d).data();
  }

  // d(dgV^N_i)/dU_i
  Scalar *getRecElem_ii(int i, int d) {
    return (rpermr.size() > 0) ? a2r.row(3 * ptrr_ii[i] + d).data() : a2r.row(3 * ptr_ii[i] + d).data();
  }

  // d(dgV^N_j)/dU_i
  Scalar *getRecElem_ij(int l, int d) {
    return (rpermr.size() > 0) ? a2r.row(3 * ptrr_ij[l] + d).data() : a2r.row(3 * ptr_ij[l] + d).data();
  }

  // d(dgV^N_i)/dU_j
  Scalar *getRecElem_ji(int l, int d) {
    return (rpermr.size() > 0) ? a2r.row(3 * ptrr_ji[l] + d).data() : a2r.row(3 * ptr_ji[l] + d).data();
  }

  //----------------------------------------------------------------------------
  // Auxilliary term access for old H2 Jacobian
  // Note: compressed storage using masking is not supported for old H2 part

  // df_ij/dV_ij
  Scalar *getH2Elem_iij(int l) {
    return (oldH2Euler) ? 0 : a2.row(4 * l).data();
  }

  // df_ij/dV_ji
  Scalar *getH2Elem_iji(int l) {
    return (oldH2Euler) ? a2.row(2 * l).data() : a2.row(4 * l + 1).data();
  }

  // df_ji/dV_ij
  Scalar *getH2Elem_jij(int l) {
    return (oldH2Euler) ? a2.row(2 * l + 1).data() : a2.row(4 * l + 2).data();
  }

  // df_ji/dV_ji
  Scalar *getH2Elem_jji(int l) {
    return (oldH2Euler) ? 0 : a2.row(4 * l + 3).data();
  }

  //----------------------------------------------------------------------------
  // Auxilliary terms for ghost points

  // du_ghost_i/du_real_j
  Scalar *getGhostNodeElem_ij(int i, int j, int offset) {
    return getAuxilliaryRow(ghostAuxilliaryRows, i, j, offset);
  }
  Scalar *queryGhostNodeElem_ij(int i, int j) {
    return queryAuxilliaryRow(ghostAuxilliaryRows, i, j);
  }
  // dr_i/du_ghost_j
  Scalar *getRealNodeElem_ij(int i, int j, int offset) {
    return getAuxilliaryRow(realAuxilliaryRows, i, j, offset);
  }
  Scalar *queryRealNodeElem_ij(int i, int j) {
    return queryAuxilliaryRow(realAuxilliaryRows, i, j);
  }

  // du_ghost_i/du_real_j where real_j is located in another subdomain with the map stored in ghostpointreflectionhandler
  Scalar *getGhostRealCommNodeElem_ij(int cpu, int i, int j) {
    return getAuxilliaryRow(ghostRealCommAuxilliaryRows[cpu], i, j, 0);
  }

  // du_ghost_i/du_ghost_j where real_j is located in another subdomain with the map stored in ghostpointreflectionhandler
  Scalar *getGhostGhostCommNodeElem_ij(int cpu, int i, int j) {
    return getAuxilliaryRow(ghostGhostCommAuxilliaryRows[cpu], i, j, 0);
  }
  // du_ghost_i/du_ghost_j for ghostpointreflectionhandler
  Scalar *getGhostGhostNodeElem_ij(int i, int j, int offset) {
    return getAuxilliaryRow(ghostGhostAuxilliaryRows, i, j, offset);
  }
  Scalar *queryGhostGhostNodeElem_ij(int i, int j) {
    return queryAuxilliaryRow(ghostGhostAuxilliaryRows, i, j);
  }
  // du_ghost_i/du_ghost_j for ghostpointreflectionhandler
  Scalar *getGradGhostNodeElem_ij(int i, int j, int dir) {
    return getAuxilliaryRow(gradGhostAuxilliaryRows[dir], i, j, 0);
  }

  struct AuxilliaryIterator {
    typename AuxilliaryRows::iterator it;
    AuxilliaryRows *map_ptr;
    int row, col;
    Scalar *pData;
    int offset;
  };

  AuxilliaryIterator *begin_realNodes() {
    if(realAuxilliaryRows.size() == 0) {
      return NULL;
    }
    AuxilliaryIterator *mvpItr = new AuxilliaryIterator;
    mvpItr->it = realAuxilliaryRows.begin();
    mvpItr->map_ptr = &realAuxilliaryRows;
    updateIterator(mvpItr);
    return mvpItr;
  }

  AuxilliaryIterator *begin_ghostNodes() {
    if(ghostAuxilliaryRows.size() == 0) {
      return NULL;
    }
    AuxilliaryIterator *mvpItr = new AuxilliaryIterator;
    mvpItr->it = ghostAuxilliaryRows.begin();
    mvpItr->map_ptr = &ghostAuxilliaryRows;
    updateIterator(mvpItr);
    return mvpItr;
  }

  AuxilliaryIterator *begin_ghostGhostCommNodes(int cpu) {
    if(ghostGhostCommAuxilliaryRows[cpu].size() == 0) {
      return NULL;
    }
    AuxilliaryIterator *mvpItr = new AuxilliaryIterator;
    mvpItr->it = ghostGhostCommAuxilliaryRows[cpu].begin();
    mvpItr->map_ptr = &ghostGhostCommAuxilliaryRows[cpu];
    updateIterator(mvpItr);
    return mvpItr;
  }

  AuxilliaryIterator *begin_ghostRealCommNodes(int cpu) {
    if(ghostRealCommAuxilliaryRows.size() == 0) {
      return NULL;
    }
    AuxilliaryIterator *mvpItr = new AuxilliaryIterator;
    mvpItr->it = ghostRealCommAuxilliaryRows[cpu].begin();
    mvpItr->map_ptr = &ghostRealCommAuxilliaryRows[cpu];
    updateIterator(mvpItr);
    return mvpItr;
  }

  AuxilliaryIterator *begin_ghostGhostNodes() {
    if(ghostGhostAuxilliaryRows.size() == 0) {
      return NULL;
    }
    AuxilliaryIterator *mvpItr = new AuxilliaryIterator;
    mvpItr->it = ghostGhostAuxilliaryRows.begin();
    mvpItr->map_ptr = &ghostGhostAuxilliaryRows;
    updateIterator(mvpItr);
    return mvpItr;
  }
  AuxilliaryIterator *begin_gradGhostNodes(int dir) {
    if(gradGhostAuxilliaryRows[dir].size() == 0) {
      return NULL;
    }
    AuxilliaryIterator *mvpItr = new AuxilliaryIterator;
    mvpItr->it = gradGhostAuxilliaryRows[dir].begin();
    mvpItr->map_ptr = &gradGhostAuxilliaryRows[dir];
    updateIterator(mvpItr);
    return mvpItr;
  }

  bool next(AuxilliaryIterator *mvpItr) {
    ++mvpItr->it;
    if(mvpItr->it == mvpItr->map_ptr->end()) {
      return false;
    }
    else {
      updateIterator(mvpItr);
      return true;
    }
  }

  void free(AuxilliaryIterator *mvpItr) {
    delete mvpItr;
  }

 protected:
  Scalar *getAuxilliaryRow(AuxilliaryRows& A, int i, int j, int offset) {
    std::pair<int, int> ij(i, j);
    typename AuxilliaryRows::iterator it = A.find(ij);
    if(it == A.end()) {
      Scalar *s = new Scalar[dim * dim];
      memset(s, 0, sizeof(Scalar) * dim * dim);
      A[ij] = std::pair<Scalar *, int>(s, offset);
      return s;
    }
    else {
      return it->second.first;
    }
  }

  Scalar *queryAuxilliaryRow(AuxilliaryRows& A, int i, int j) {
    std::pair<int, int> ij(i, j);
    typename AuxilliaryRows::iterator it = A.find(ij);
    if(it == A.end()) {
      return NULL;
    }
    else {
      return it->second.first;
    }
  }

  void updateIterator(AuxilliaryIterator *mvpItr) {
    mvpItr->row = mvpItr->it->first.first;
    mvpItr->col = mvpItr->it->first.second;
    mvpItr->pData = mvpItr->it->second.first;
    mvpItr->offset = mvpItr->it->second.second;
  }

  //----------------------------------------------------------------------------
  // Apply methods
  // Note: these correctly promote A to type Scalar2 when Scalar != Scalar2

 public:
  template<class Scalar2>
  void applyH1(SVec<Scalar2, Dim>&, SVec<Scalar2, Dim>&, Vec<bool> * = nullptr);

  template<class Scalar2>
  void applyH1Transpose(SVec<Scalar2, Dim>&, SVec<Scalar2, Dim>&);

  template<class Scalar2>
  void applyH1(DenseMat<Scalar2>&, DenseMat<Scalar2>&);

  template<class Scalar2>
  void applyGhost_Step1(SVec<Scalar2, Dim>&, SVec<Scalar2, 4*Dim>&);

  template<class Scalar2>
  void applyGhost_Step1(SVec<Scalar2, Dim>&, SVec<Scalar2, Dim>&, bool = false);

  template<class Scalar2>
  void applyGhost_Step1Transpose(SVec<Scalar2, Dim>&, SVec<Scalar2, Dim>&, bool = false);

  template<class Scalar2>
  void applyGhost_Step1(DenseMat<Scalar2>&, DenseMat<Scalar2>&);

  template<class Scalar2>
  void applyGhost_Step2(SVec<Scalar2, 4*Dim>&, SVec<Scalar2, Dim>&);

  template<class Scalar2>
  void applyGhost_Step2(SVec<Scalar2, Dim>&, SVec<Scalar2, Dim>&);

  template<class Scalar2>
  void applyGhost_Step2Transpose(SVec<Scalar2, Dim>&, SVec<Scalar2, Dim>&);

  template<class Scalar2>
  void applyGhost_Step2(DenseMat<Scalar2>&, DenseMat<Scalar2>&);

  template<class Scalar2>
  void applyToGhost_toSend(SVec<Scalar2, Dim>&, Scalar2 (*)[Dim], std::vector<int>&, bool);

  template<class Scalar2>
  void applyToGhost_toSendTranspose(SVec<Scalar2, Dim>&, Scalar2 (*)[Dim], std::vector<int>&, bool);

  // new H2 application
  template<class Scalar2>
  void applyH2_Step1(SVec<Scalar2, Dim>&, SVec<Scalar2, 3*Dim>&, Vec<bool> * = nullptr);

  template<class Scalar2>
  void applyH2_Step1Transpose(SVec<Scalar2, 3*Dim>&, SVec<Scalar2, Dim>&);

  template<class Scalar2>
  void applyH2_Step1GhostReflec(SVec<Scalar2, Dim>&, SVec<Scalar2, 3*Dim>&);

  template<class Scalar2>
  void applyH2_Step1GhostReflecTranspose(SVec<Scalar2, 3*Dim>&, SVec<Scalar2, Dim>&);

  template<class Scalar2>
  void applyH2_Step1(DenseMat<Scalar2>&, DenseMat<Scalar2>&);

  template<class Scalar2>
  void applyH2_Step2(SVec<Scalar2, 3*Dim>&, SVec<Scalar2, Dim>&, Vec<bool> * = nullptr);

  template<class Scalar2>
  void applyH2_Step2Transpose(SVec<Scalar2, Dim>&, SVec<Scalar2, 3*Dim>&);

  template<class Scalar2>
  void applyH2_Step2(DenseMat<Scalar2>&, DenseMat<Scalar2>&);

};

//------------------------------------------------------------------------------

#endif

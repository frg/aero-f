#ifndef _LES_TERM_H_
#define _LES_TERM_H_

#include <IoData.h>

#include <cmath>

//------------------------------------------------------------------------------

class LESTerm {

 protected:
  static constexpr double twothirds = 2.0 / 3.0;

  bool delta;
  double gamma;
  double prandtlT;

 public:
  LESTerm(IoData&);
  virtual ~LESTerm() {}

  double getGamma() { return gamma; }
  double getPrandtlT() { return prandtlT; }

  virtual double computeEddyViscosity(double *[4], double, double[3][3]) = 0;

  virtual double computeDerivativeOfEddyViscosity(double *[4], double *[4], double, double,
                                                  double[3][3], double[3][3]) = 0;

  virtual void computeJacobianEddyViscosity(double *[4], double, double[3][3], double[4][3],
                                            double[4][5]) = 0;

  virtual double computeTKE(double, double) = 0;

  double computeKappa(bool, double);

  double computeFilterWidth(double, double[4][3]);

  double computeDerivativeOfFilterWidth(double, double, double[4][3], double[4][3]);

  void computeTurbulentTransportCoefficients(bool, double, double[4][3], double *[4], double[3][3],
                                             double&, double&, double&);

  void computeRhoKSGS(bool, double, double[4][3], double, double, double&);

  void computeDerivativeOfTurbulentTransportCoefficients(bool, double, double, double[4][3],
                                                         double[4][3], double *[4], double *[4],
                                                         double[3][3], double[3][3], double&,
                                                         double&, double&);

  void computeJacobianTurbulentTransportCoefficients(bool, double, double[4][3], double *[4],
                                                     double[3][3], double[4][3], double[4][5],
                                                     double[4][5], double[4][5]);
};

//------------------------------------------------------------------------------

inline
LESTerm::LESTerm(IoData& iod) {
  delta    = !(iod.eqs.tc.les.delta == LESModelData::VOLUME);
  gamma    = iod.eqs.fluidModel(0).gasModel.specificHeatRatio; // XXX
  prandtlT = iod.eqs.tc.prandtlTurbulent;
}

//------------------------------------------------------------------------------

inline
double LESTerm::computeFilterWidth(double tetVol, double X[4][3]) {
  if(delta) {
    double maxl, sidel;
    maxl = -1.0;
    for(int i = 0; i < 4; ++i) {
      for(int j = i + 1; j < 4; ++j) {
        sidel = sqrt((X[i][0] - X[j][0]) * (X[i][0] - X[j][0]) +
                     (X[i][1] - X[j][1]) * (X[i][1] - X[j][1]) +
                     (X[i][2] - X[j][2]) * (X[i][2] - X[j][2]));
        maxl = std::max(maxl, sidel);
      }
    }
    return maxl;
  }
  else {
    return std::cbrt(tetVol);
  }
}

//------------------------------------------------------------------------------

inline
double LESTerm::computeDerivativeOfFilterWidth(double tetVol, double dtetVol,
                                               double X[4][3], double dX[4][3]) {
  if(delta) {
    double maxl, sidel, dmaxl;
    maxl = -1.0;
    dmaxl = 0.0;
    for(int i = 0; i < 4; ++i) {
      for(int j = i + 1; j < 4; ++j) {
        sidel = sqrt((X[i][0] - X[j][0]) * (X[i][0] - X[j][0]) +
                     (X[i][1] - X[j][1]) * (X[i][1] - X[j][1]) +
                     (X[i][2] - X[j][2]) * (X[i][2] - X[j][2]));
        if(sidel > maxl) {
          maxl = sidel;
          double dsidel2 = 2 * ((X[i][0] - X[j][0]) * (dX[i][0] - dX[j][0]) +
                                (X[i][1] - X[j][1]) * (dX[i][1] - dX[j][1]) +
                                (X[i][2] - X[j][2]) * (dX[i][2] - dX[j][2]));
          dmaxl = dsidel2 / (2 * sidel);
        }
      }
    }
    return dmaxl;
  }
  else {
     double cbrtTetVol = std::cbrt(tetVol);
     return dtetVol / (3 * cbrtTetVol * cbrtTetVol);
  }
}

//------------------------------------------------------------------------------

inline
void LESTerm::computeRhoKSGS(bool inTrip, double tetVol, double X[4][3],
                             double mut, double rhocg, double& rhok) {
  if(inTrip) {
    double nut = mut / rhocg;
    double Delta = computeFilterWidth(tetVol, X);
    double k = computeTKE(nut, Delta);
    // std::cout << "computeRhoKSGS k = " << k << ";\n";
    rhok = rhocg * k;
  }
  else {
    rhok = 0.;
  }
}

//------------------------------------------------------------------------------

inline
void LESTerm::computeTurbulentTransportCoefficients(bool inTrip, double tetVol, double X[4][3],
                                                    double *V[4], double dudxj[3][3],
                                                    double& mut, double& lambdat, double& kappat) {
  if(inTrip) {
    double Delta = computeFilterWidth(tetVol, X);
    mut = computeEddyViscosity(V, Delta, dudxj);
    lambdat = -twothirds * mut;
    kappat = gamma / prandtlT * mut;
  }
  else {
    mut = lambdat = kappat = 0.;
  }
}
//------------------------------------------------------------------------------

inline
double LESTerm::computeKappa(bool inTrip, double mut) {
  double kappat = (inTrip) ? gamma / prandtlT * mut : 0.0;
  return kappat;
}

//------------------------------------------------------------------------------

inline
void LESTerm::computeDerivativeOfTurbulentTransportCoefficients(bool inTrip, double tetVol, double dtetVol,
                                                                double X[4][3], double dX[4][3], double *V[4],
                                                                double *dV[4], double dudxj[3][3], double ddudxj[3][3],
                                                                double& dmut, double& dlambdat, double& dkappat) {
  if(inTrip) {
    double Delta = computeFilterWidth(tetVol, X);
    double dDelta = computeDerivativeOfFilterWidth(tetVol, dtetVol, X, dX);
    dmut = computeDerivativeOfEddyViscosity(V, dV, Delta, dDelta, dudxj, ddudxj);
    dlambdat = -twothirds * dmut;
    dkappat = gamma / prandtlT * dmut;
  }
  else {
    dmut = dlambdat = dkappat = 0.;
  }
}

//------------------------------------------------------------------------------

inline
void LESTerm::computeJacobianTurbulentTransportCoefficients(bool inTrip, double tetVol, double X[4][3],
                                                            double *V[4], double dudxj[3][3], double dp1dxj[4][3],
                                                            double dmut[4][5], double dlambdat[4][5],
                                                            double dkappat[4][5]) {
  if(inTrip) {
    double Delta = computeFilterWidth(tetVol, X);
    computeJacobianEddyViscosity(V, Delta, dudxj, dp1dxj, dmut);
    double gammaoprandtlT = gamma / prandtlT;
    for(int k = 0; k < 4; ++k) {
      for(int i = 0; i < 5; ++i) {
        dlambdat[k][i] = -twothirds * dmut[k][i];
        dkappat[k][i] = gammaoprandtlT * dmut[k][i];
      }
    }
  }
  else {
    for(int k = 0; k < 4; ++k) {
      for(int i = 0; i < 5; ++i) {
        dmut[k][i] = dlambdat[k][i] = dkappat[k][i] = 0.;
      }
    }
  }
}

//------------------------------------------------------------------------------

#endif

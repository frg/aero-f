#ifndef _FEM_EQUATION_TERM_H_
#define _FEM_EQUATION_TERM_H_

#include <IoData.h>
#include <PorousMediaTerm.h>
#include <BcDef.h>
#include <WallFcn.h>

#include <cstdlib>
#include <cstdio>
#include <map>

class NavierStokesTerm;

struct Vec3D;

template<class Scalar, int dim> class SVec;

//------------------------------------------------------------------------------
// CHANGES_FOR_WATER: as the Stoke's law for gas does not apply anymore, we
// have to distinguish between lambda and mu (the Lame coefficients)
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// A base class for FemEquationTerm models
class FemEquationTerm {

 protected:
  WallFcn *wallFcn;
  PorousMediaTerm *pr;
  IoData &ioData;

 public:
  FemEquationTerm(IoData&);
  virtual ~FemEquationTerm();

  virtual void rstVar(IoData& iod) {
    if(wallFcn) wallFcn->rstVar(iod);
    pr->rstVar(iod);
  }

  virtual NavierStokesTerm *getNavierStokesTerm() = 0;

  bool withWallFcn() {
    return (wallFcn != nullptr);
  }

  virtual bool doesSourceTermExist() = 0;

  virtual bool doesFaceTermExist(int code) {
    if(wallFcn) {
      return (code == BC_ADIABATIC_WALL_MOVING ||
              code == BC_ADIABATIC_WALL_FIXED ||
              code == BC_ISOTHERMAL_WALL_MOVING ||
              code == BC_ISOTHERMAL_WALL_FIXED ||
              code == BC_OUTLET_MOVING ||
              code == BC_OUTLET_FIXED ||
              code == BC_INLET_MOVING ||
              code == BC_INLET_FIXED ||
              code == BC_PERIODIC) ? true : false;
    }
    else {
      return (code == BC_ADIABATIC_WALL_MOVING ||
              code == BC_ADIABATIC_WALL_FIXED ||
              code == BC_OUTLET_MOVING ||
              code == BC_OUTLET_FIXED ||
              code == BC_INLET_MOVING ||
              code == BC_INLET_FIXED ||
              code == BC_PERIODIC) ? true : false;
    }
  }

  virtual bool doesFaceNeedGradientP1Function(int code) {
    return (!wallFcn ||
            code == BC_OUTLET_MOVING ||
            code == BC_OUTLET_FIXED ||
            code == BC_INLET_MOVING ||
            code == BC_INLET_FIXED ||
            code == BC_PERIODIC);
  }

  virtual double computeViscosity(double *, int) {
    fprintf(stderr, "*** Error: computeViscosity should not be called\n");
    exit(1);
  }

  virtual void computeJacobianViscosity(double *, double *, int) {
    fprintf(stderr, "*** Error: computeJacobianViscosity should not be called\n");
    exit(1);
  }

  virtual double computeViscousTimeStep(double *, double *, int) {
    fprintf(stderr, "*** Error: computeViscousTimeStep should not be called\n");
    exit(1);
  }

  virtual double computeDerivativeOfViscousTimeStep(double *, double *, double *, double *, double, int) {
    fprintf(stderr, "*** Error: computeDerivativeOfViscousTimeStep should not be called\n");
    exit(1);
  }

  virtual bool computeVolumeTerm(double, double[4][3], double[4][3], double[4], double *[4], int,
                                 int, int, Vec3D&, double *, int, double, double *, double *,
                                 double *, double = 1.0) {
    fprintf(stderr, "*** Error: computeVolumeTerm should not be called\n");
    exit(1);
  }

  virtual bool computeDerivativeOfVolumeTerm(double, double, double[4][3], double[4][3], double[4][3],
                                             double[4][3], double[4], double[4], double *[4], double *[4],
                                             double, int, int, int,
                                             Vec3D&, Vec3D&, double *, int, double, double,
                                             double *, double *, double *, double = 1.0, double = 0.0) {
    fprintf(stderr, "*** Error: computeDerivativeOfVolumeTerm should not be called\n");
    exit(1);
  }

  virtual void computeDerivativeOperatorsOfVolumeTerm(double[4][3], double[4][3], double[4], double *[4],
                                                      int, int, double[3][5][4][3], double[3][5],
                                                      double[5][4][3], double[5], double[5][4][3]) {
    fprintf(stderr, "*** Error: computeDerivativeOperatorsOfVolumeTerm should not be called\n");
    exit(1);
  }

  virtual void computeDerivativeOperatorsOfVolumeTerm(double[4][3], double[4][3], double[4], double *[4],
                                                      int, int, double[3][6][4][3], double[3][6],
                                                      double[6][4][3], double[6], double[6][4][3]) {
    fprintf(stderr, "*** Error: computeDerivativeOperatorsOfVolumeTerm should not be called\n");
    exit(1);
  }

  virtual void computeDerivativeOperatorsOfVolumeTerm(double[4][3], double[4][3], double[4], double *[4],
                                                      int, int, double[3][7][4][3], double[3][7],
                                                      double[7][4][3], double[7], double[7][4][3]) {
    fprintf(stderr, "*** Error: computeDerivativeOperatorsOfVolumeTerm should not be called\n");
    exit(1);
  }

  virtual bool computeJacobianVolumeTerm(double, double[4][3], double[4][3], double[4], double *[4],
                                         int, int, int, Vec3D&, double *, int, double, double *,
                                         double *, double *, double = 1.0) {
    fprintf(stderr, "*** Error: computeJacobianVolumeTerm should not be called\n");
    exit(1);
  }

  virtual void computeSurfaceTerm(int, double[3], double *[3], double *, int, Vec3D&, double *) {
    fprintf(stderr, "*** Error: computeSurfaceTerm should not be called\n");
    exit(1);
  }

  virtual void computeDerivativeOfSurfaceTerm(int, double[3], double *[3], double *[3], double *, double *,
                                              int, Vec3D&, Vec3D&, double, double *) {
    fprintf(stderr, "*** Error: computeDerivativeOfSurfaceTerm should not be called\n");
    exit(1);
  }

  virtual void computeDerivativeOperatorsOfSurfaceTerm(int, double[3], double *[3], double *, int, Vec3D&,
                                                       double[][3], double[][3], double[][3], double[][3]) {
    fprintf(stderr, "*** Error: computeDerivativeOperatorsOfSurfaceTerm should not be called\n");
    exit(1);
  }

  virtual void computeJacobianSurfaceTerm(int, double[3], double *[3], double *, int, Vec3D&, double *) {
    fprintf(stderr, "*** Error: computeJacobianSurfaceTerm should not be called\n");
    exit(1);
  }

  virtual void computeSurfaceTerm(int, double[4][3], double *[4], double *, int, Vec3D&, double *) {
    fprintf(stderr, "*** Error: computeSurfaceTerm should not be called\n");
    exit(1);
  }

  virtual void computeDerivativeOfSurfaceTerm(int, double[4][3], double[4][3], double *[4], double *[4],
                                              double *, double *, int, Vec3D&, Vec3D&, double, double *) {
    fprintf(stderr, "*** Error: computeDerivativeOfSurfaceTerm should not be called\n");
    exit(1);
  }

  virtual void computeDerivativeOperatorsOfSurfaceTerm(int, double[4][3], double *[4], double *, int, Vec3D&,
                                                       double[][4][3], double[][3], double[]) {
    fprintf(stderr, "*** Error: computeDerivativeOperatorsOfSurfaceTerm should not be called\n");
    exit(1);
  }

  virtual void computeJacobianSurfaceTerm(int, double[4][3], double *[4], double *, int, Vec3D&, double *) {
    fprintf(stderr, "*** Error: computeJacobianSurfaceTerm should not be called\n");
    exit(1);
  }

  virtual void computeDistanceDerivativeOfVolumeTerm(int, double[4][3], double[4][3], double[4], double *[4],
                                                     int, double&) {
    fprintf(stderr, "*** Error: computeDistanceDerivativeOfVolumeTerm should not be called\n");
    exit(1);
  }

  virtual double computeMutOMu(double, double[4][3], double[4][3], double *[4], int, bool = false) {
    fprintf(stderr, "*** Error: computeMutOMu should not be called\n");
    exit(1);
  }

  virtual double computeCsValue(double, double[4][3]) {
    fprintf(stderr, "*** Error: computeCsValues should not be called\n");
    exit(1);
  }

  virtual void setConstants(double[4], double[4]) {
    fprintf(stderr, "*** Error: setConstants should not be called\n");
    exit(1);
  }

  virtual void setVBar(double *[4]) {
    fprintf(stderr, "*** Error: setVBar should not be called\n");
    exit(1);
  }

  // This function accounts for the derivative of the turbulent variable wall BCs
  // when wall function is used.
  virtual void computeBCsJacobianWallValues(int, Vec3D&, double[3], double *,
                                            double *, double *[3], int) {
    fprintf(stderr, "*** Error: computeBCsJacobianWallValues should not be called\n");
    exit(1);
  }

  virtual void computeMaxwellSlipSurfaceTerm(int, double[4][3], double *[4], double *, int, Vec3D&, double *) {
    fprintf(stderr, "*** Error: computeMaxwellSlipSurfaceTerm should not be called\n");
    exit(1);
  }

};

//------------------------------------------------------------------------------

inline
FemEquationTerm::FemEquationTerm(IoData& iod) : ioData(iod) {
  wallFcn = nullptr;
  pr = new PorousMediaTerm(iod);
}

//------------------------------------------------------------------------------

inline
FemEquationTerm::~FemEquationTerm() {
  if(wallFcn) {
    delete wallFcn;
  }
  if(pr) {
    delete pr;
  }
}

//------------------------------------------------------------------------------
// A base class for turbulent (RANS and LES) FemEquationTerm models
class FemEquationTermTurb {

  double x0, y0, z0, x1, y1, z1;
  bool trip;

 protected:
  bool inTrip(double[3]);
  bool inTrip(double[4][3]);

 public:
  FemEquationTermTurb(IoData&);
  virtual ~FemEquationTermTurb() {}
  
  bool isEigenperturbation;
  bool isStochasticTurbulenceModel;
  // double delta_psi;
  // int eigenvalue;
  // int eigenvector;
  // double uq_urlx, delta_psi, mu_z, delta_z, mu_J, p0;

};

//------------------------------------------------------------------------------

inline
FemEquationTermTurb::FemEquationTermTurb(IoData& iod) {
  x0 = iod.eqs.tc.tr.bfix.x0;
  x1 = iod.eqs.tc.tr.bfix.x1;
  y0 = iod.eqs.tc.tr.bfix.y0;
  y1 = iod.eqs.tc.tr.bfix.y1;
  z0 = iod.eqs.tc.tr.bfix.z0;
  z1 = iod.eqs.tc.tr.bfix.z1;
  if(x0 > x1 || y0 > y1 || z0 > z1) {
    trip = false;
  }
  else {
    trip = true;
  }
  if(iod.ts.implicit.tmcoupling == ImplicitData::STRONG && trip == 1) {
    fprintf(stderr, "*** Warning: Laminar-turbulent trip not implemented for strongly coupled RANS simulations\n");
    trip = false;
  }
  // if(iod.eqs.tc.ep.eigenvalue == EigenperturbationData::UNPERTURBEDVALUE) {
  //   eigenvalue = 0;
  // } else if(iod.eqs.tc.ep.eigenvalue == EigenperturbationData::C1) {
  //   eigenvalue = 1;
  // } else if(iod.eqs.tc.ep.eigenvalue == EigenperturbationData::C2) {
  //   eigenvalue = 2;
  // } else if(iod.eqs.tc.ep.eigenvalue == EigenperturbationData::C3) {
  //   eigenvalue = 3;
  // }
  // if(iod.eqs.tc.ep.eigenvector == EigenperturbationData::UNPERTURBEDVECTOR) {
  //   eigenvector = 0;
  // } else if(iod.eqs.tc.ep.eigenvector == EigenperturbationData::MINIMUM) {
  //   eigenvector = 1;
  // }  else if(iod.eqs.tc.ep.eigenvector == EigenperturbationData::MAXIMUM) {
  //   eigenvector = 2;
  // }
  isEigenperturbation = (((iod.eqs.tc.ep.eigenvalue != EigenperturbationData::UNPERTURBEDVALUE) || (iod.eqs.tc.ep.eigenvector != EigenperturbationData::UNPERTURBEDVECTOR) ) ? true : false);
  // if(isEigenperturbation) {
  //   uq_urlx = iod.eqs.tc.ep.uq_urlx;
  // }
  // delta_psi = iod.eqs.tc.stm.delta_psi;
  // isStochasticTurbulenceModel = ((iod.eqs.tc.stm.delta_psi || iod.eqs.tc.stm.mu_z || iod.eqs.tc.stm.delta_z || iod.eqs.tc.stm.mu_J || iod.eqs.tc.stm.p0
  //                                 || iod.eqs.tc.stm.mu_k || iod.eqs.tc.stm.delta_k || iod.eqs.tc.stm.mu_zeta  || iod.eqs.tc.stm.delta_zeta  || iod.eqs.tc.stm.mu_eta  || iod.eqs.tc.stm.delta_eta
  //                                 || iod.eqs.tc.stm.mu_theta || iod.eqs.tc.stm.mu_theta ) ? true : false);
  isStochasticTurbulenceModel = ((iod.eqs.tc.stm.vector_perturbation || iod.eqs.tc.stm.value_perturbation || iod.eqs.tc.stm.scale_perturbation) ? true : false);
  // if(iod.eqs.tc.stm.delta_psi) {
  //   mu_z = iod.eqs.tc.stm.mu_z;
  //   delta_z = iod.eqs.tc.stm.delta_z;
  //   mu_J = iod.eqs.tc.stm.mu_J;
  //   p0 = iod.eqs.tc.stm.p0;
  //   // uq_urlx = iod.eqs.tc.stm.uq_urlx;
  // }
}

//------------------------------------------------------------------------------

inline
bool FemEquationTermTurb::inTrip(double X[3]) {
  if(trip) {
    if(X[0] >= x0 && X[0] <= x1 &&
       X[1] >= y0 && X[1] <= y1 &&
       X[2] >= z0 && X[2] <= z1) {
      return true;
    }
    else {
      return false;
    }
  }
  else {
    return true;
  }
}

//------------------------------------------------------------------------------

inline
bool FemEquationTermTurb::inTrip(double X[4][3]) {
  if(trip) {
    for(int k = 0; k < 4; ++k) {
      if(X[k][0] >= x0 && X[k][0] <= x1 &&
         X[k][1] >= y0 && X[k][1] <= y1 &&
         X[k][2] >= z0 && X[k][2] <= z1) {
        return true;
      }
    }
    return false;
  }
  else {
    return true;
  }
}

//------------------------------------------------------------------------------

#endif

#ifndef _NAVIERSTOKESCOUPLEDSOLVER_H_
#define _NAVIERSTOKESCOUPLEDSOLVER_H_

class IoData;
class GeoSource;
class Domain;

template<int dim>
class NavierStokesCoupledSolver {
 public:
  static void solve(IoData& ioData, GeoSource& geoSource, Domain& domain);
};

#endif

#ifndef _NAVIERSTOKESSEGSOLVER_H_
#define _NAVIERSTOKESSEGSOLVER_H_

class IoData;
class GeoSource;
class Domain;

template<int dim, int neq1, int neq2>
class NavierStokesSegSolver {
 public:
  static void solve(IoData& ioData, GeoSource& geoSource, Domain& domain);
};

#endif

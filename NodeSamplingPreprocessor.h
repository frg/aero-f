#ifndef _SAMPLING_PREPROCESSOR_H_
#define _SAMPLING_PREPROCESSOR_H_

#include <Types.h>
#include <VectorSet.h>
#include <DistVector.h>

#include <vector>
#include <set>

class IoData;
class Domain;
class Timer;
class Communicator;
class NonlinearRom;
class DistGeoState;

template<int dim> class ImplicitTsDesc;
template<class Scalar> class Vec;
template<class Scalar> class DenseMat;
template<class Scalar, int dim> class DistSVec;
template<class VecType> class Clustering;
template<int dim> class DistEmbeddedBcData;
template<int dim> class NearestNeighborRom;

struct SamplingWeightingData;

//------------------------------------------------------------------------------

class NodeSamplingPreprocessor {

  Domain *domain;
  Communicator *com;
  Timer *timer;
  NonlinearRom *nlrom;
  SamplingWeightingData& samplingData;
  IoData& ioData;
  DistGeoState *geoState;

 private:

  void run(bool, double, std::vector<DenseMat<double>>&, Vec<double>&, std::vector<Vec<double>>&,
           std::vector<std::pair<int, long_int>>&);

  int nncgp(DenseMat<double>&, Vec<double>&, Vec<double>&,
            const long, const long, const double, bool, bool, bool, double&);

  int pnncgp(std::vector<DenseMat<double>>&, Vec<double>&, std::vector<Vec<double>>&,
             std::vector<std::pair<int, long_int>>&,
             const long, const long, const double, bool, bool, bool, bool, double&);

  int splh(std::vector<DenseMat<double>>&, Vec<double>&, std::vector<Vec<double>>&,
           std::vector<std::pair<int, long_int>>&,
           const long, const long, const double, bool, bool, bool, bool, double&);

  bool contains_node_type(std::vector<bool>&, int);
  bool is_node_type(int, int);
  void generate_mesh_partition(std::vector<std::vector<std::vector<int>>>&,
                               std::vector<int>&, int node_type);
  void generate_mesh_row_partitions(std::vector<std::vector<std::vector<int>>>&,
                                    std::vector<int>&, DistVec<double>&, bool);

 public:
  NodeSamplingPreprocessor(IoData&, Domain *, NonlinearRom *, DistGeoState *);
  ~NodeSamplingPreprocessor() {}

  template<int dim>
  void solve(ImplicitTsDesc<dim> *, int&, int&, std::vector<std::set<int>>&, Clustering<DistSVec<double, dim>> *,
             NearestNeighborRom<dim> * = nullptr);
  void useInputSol(int&, int&, std::vector<std::set<int>>&);

};

//------------------------------------------------------------------------------

#endif

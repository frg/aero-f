#ifndef _NAVIER_STOKES_TERM_H_
#define _NAVIER_STOKES_TERM_H_

#include <IoData.h>
#include <VarFcn.h>
#include <ViscoFcn.h>
#include <ThermalCondFcn.h>
#include <SlipFcn.h>
#include <Vector3D.h>

#include <Communicator.h>
#include <unsupported/Eigen/MatrixFunctions>
#include <Eigen/Eigenvalues>
#include <random>
#include <fstream>
#include <iostream>
#include <string>

#ifdef USE_EIGEN3_DenseMat
// #include <Eigen/SVD>
#include <Eigen/QR>
// typedef Eigen::Matrix<double,Eigen::Dynamic,Eigen::Dynamic,Eigen::RowMajor> MatrixType;
// typedef Eigen::Matrix<double,Eigen::Dynamic,1,Eigen::ColMajor> VectorType;
#endif

//------------------------------------------------------------------------------

class NavierStokesTerm {

  static constexpr double fourth = 1.0 / 4.0;
  double ooreynolds_mu;
  double dRe_mudMachNS;
  double third = 1.0 / 3.0;
  double psi[3][3] = {};
  // double rotm[3][3] = {};
  double z;
  double cJ[3] = {};
  double uq_urlx;
  double kfactor;
  int eigenvalue;
  int eigenvector;
  int value_perturbation;
  int vector_perturbation;
  int scale_perturbation;
  // double delta_psi;
  // double mu_k;
  // double delta_k;
  // double mu_zeta;
  // double delta_zeta;
  // double mu_eta;
  // double delta_eta;
  // double mu_theta;
  // double delta_theta;

  std::random_device rd;
  std::mt19937 generator;
  std::normal_distribution<> distN;
  std::uniform_real_distribution<> distU;

  VarFcn *varFcn;
  ViscoFcn *viscoFcn;
  ThermalCondFcn *thermalCondFcn;
  SlipFcn *slipFcn;

 public:
  NavierStokesTerm(IoData&, VarFcn *);
  ~NavierStokesTerm();
  IoData &ioData;
  Communicator *com;

  void rstVar(IoData&);

  double getOoreynolds_mu() const {
    return ooreynolds_mu;
  }

  double getdOoreynolds_mu(bool dMach) const {
    return dMach ? -(ooreynolds_mu * ooreynolds_mu) * dRe_mudMachNS : 0.0;
  }

  double getdRe_mudMach() const {
   return dRe_mudMachNS;
  }

  VarFcn *getVarFcn() const {
    return varFcn;
  }

  ViscoFcn *getViscoFcn() const {
    return viscoFcn;
  }

  ThermalCondFcn *getThermalCondFcn() const {
    return thermalCondFcn;
  }

  void computeLaminarTransportCoefficients(double, int, double&, double&, double&, int);

  void computeDerivativeOfLaminarTransportCoefficients(double, double, int, double, bool, double&,
                                                       double&, double&, int);

  void computeLaminarTransportCoefficientsDerivativeOperators(double, int, double&, double&,
                                                              double&, double&, double&,
                                                              double&, int);

  void computeNonDimensionalLaminarTransportCoefficients(double, int, double&, double&, double&, int);

  void computeDerivativeOfNonDimensionalLaminarTransportCoefficients(double, double, int, double, double,
                                                                     double, double, double&, double&,
                                                                     double&, int);

  void computeNonDimensionalLaminarTransportCoefficientsDerivativeOperators(double, int, double&, double&,
                                                                            double&, double&, double&,
                                                                            double&, int);

  void computeJacobianNonDimensionalLaminarTransportCoefficients(double, double[4][5], int, double,
                                                                 double[4][5], double[4][5], double[4][5],
                                                                 int);

  void computeTemperature(double *, int, double&);

  void computeDerivativeOfTemperature(double *, double *, int, double&);

  void computeTemperature(double *[4], int, double[4], double&);

  void computeDerivativeOfTemperature(double *[4], double *[4], int, double[4], double&);

  void computeDerivativeOperatorsOfTemperature(double *[4], int, double[4][5], double[4][5]);

  template<int dim>
  void computeJacobianTemperature(double *[4], int, double[4], double[4][dim]);

  void computeTemperatureGradient(double[4][3], double[4], double[3]);

  void computeDerivativeOfTemperatureGradient(double[4][3], double[4][3], double[4], double[4], double[3]);

  void computeDerivativeOperatorsOfTemperatureGradient(double[4][3], double[4], double[3][4][3], double[3][4]);

  void computeDensity(double *[4], double[3], double&);

  void computeVelocity(double *[4], double[4][3], double[3]);

  void computeDerivativeOfVelocity(double *[4], double[4][3], double[3]);

  void computeDerivativeOperatorsOfVelocity(double[4][3][4][4], double[3][4][4]);

  void computeVelocityGradient(double[4][3], double[4][3], double[3][3]);

  void computeDerivativeOfVelocityGradient(double[4][3], double[4][3], double[4][3], double[4][3], double[3][3]);

  void computeDerivativeOperatorsOfVelocityGradient(double[4][3], double[4][3], double[3][3][4][3], double[3][3][4][3]);

  void computeTurbulentStressTensor(double, double, double, double[3][3], double[3][3], double);

  void computeStressTensor(double, double, double[3][3], double[3][3]);

  void computeStressTensorUQ(double, double, double[3][3], double[3][3],  double[3][3], double);

  void computeDerivativeOfStressTensor(double, double, double, double, double[3][3], double[3][3], double[3][3]);

  void computeDerivativeOperatorsOfStressTensor(double, double, double[3][3], double[3][3][3][3], double[3][3], double[3][3]);

  void computeHeatFluxVector(double, double[3], double[3]);

  void computeDerivativeOfHeatFluxVector(double, double, double[3], double[3], double[3]);

  void computeDerivativeOperatorsOfHeatFluxVector(double, double[3], double[3][3], double[3]);

  void computeStochasticRSM(double, double, double, double[3][3], double[3][3], double&, double&, double[3], double = 0.);

  void computePerturbedRSM(double, double, double, double[3][3], double[3][3], double&, double = 0.);

  void checkAndUpdatek(double, double, double, double[3][3], double&, double[3]);

  void computeAnisotropicReynoldsStressTensorAlt(double, double, double, double[3][3], double, double[3][3]);

  void computeAnisotropicReynoldsStressTensor(double, double, double, double[3][3], double[3][3], double, double[3][3]);

  void computeSpectralDecompTensor(double [3][3], double [3], double [3][3]);
  
  void computeEigenvaluesTensor(double [3][3], double [3]);

  void multiplyFiveMatrices3x3(double [3][3], double [3][3], double [3][3], double [3][3], double [3][3], double [3][3]);

  template<int dim>
  void computeVolumeTermNSUQ(double, double, double, double, double, double[3], double[3][3],
                           double[3], double (*)[dim], double[3][3]);

  template<int dim>
  void computeVolumeTermNS(double, double, double, double, double[3], double[3][3],
                           double[3], double (*)[dim]);

  template<int dim>
  void computeDerivativeOfVolumeTermNS(double, double, double, double, double, double, double, double,
                                       double[3], double[3], double[3][3], double[3][3],
                                       double[3], double[3], double (*)[dim]);

  template<int dim>
  void computeDerivativeOperatorsOfVolumeTermNS(double, double, double, double, double[3], double[3][3], double[3],
                                                double[3][dim], double[3][dim], double[3][dim], double[3][dim],
                                                double[3][dim][3][3], double[3][dim][3]);

  template<int neq>
  void computeJacobianVolumeTermNS(double[4][3], double, double, double, double *[4], double[3], double[3][3],
                                   int, double (*)[3][neq][neq]);

  template<int neq>
  void computeJacobianVolumeTermNS(double[4][3], double, double[4][neq], double, double[4][neq],
                                   double, double[4][neq], double *[4], double[3], double[3][3],
                                   int, double[3], double (*)[3][neq][neq]);

  template<int neq>
  void computeJacobianVolumeTermNS(double[4][3], double, double[4][neq], double, double[4][neq],
                                   double, double[4][neq], double, double[4][neq], double *[4],
                                   double[3], double[3][3], int, double[3], double (*)[3][neq][neq]);

  template<int neq>
  void computeJacobianVolumeTermNSUQ(double[4][3], double, double, double, double *[4], double[3], double[3][3],
                                   int, double (*)[3][neq][neq], double[3][3], double, double, double);

  template<int neq>
  void computeJacobianVolumeTermNSUQ(double[4][3], double, double[4][neq], double, double[4][neq],
                                   double, double[4][neq], double *[4], double[3], double[3][3],
                                   int, double[3], double (*)[3][neq][neq], double[3][3], double, double, double);

  template<int neq>
  void computeJacobianVolumeTermNSUQ(double[4][3], double, double[4][neq], double, double[4][neq],
                                   double, double[4][neq], double, double[4][neq], double *[4],
                                   double[3], double[3][3], int, double[3], double (*)[3][neq][neq], double[3][3], double, double, double);

  template<int neq>
  void computeJacobianVolumeTermNSUQ_SRST(double[4][3], double, double[4][neq], double, double [4][neq], 
                                     double , double[4][neq], double , double[4][neq], double , double[4][neq], 
                                     double , double[4][neq],double, double[4][neq], double *[4], double[3],
                                     double[3][3], int, double[3], double (*)[3][neq][neq], double [3][3], double, double, double[3]);

  void computeSurfaceTermNS(double, double, double *, double[3][3], Vec3D&, double *);

  void computeDerivativeOfSurfaceTermNS(double, double, double, double, double *, double *, double[3][3],
                                        double[3][3], Vec3D&, Vec3D&, double *);

  void computeDerivativeOperatorsOfSurfaceTermNS(double, double, double *, double[3][3], Vec3D&, double[],
                                                 double[], double[][5], double[][3][3], double[][3]);

  template<int neq>
  void computeJacobianSurfaceTermNS(double dp1dxj[4][3], double, double, double *[4],
                                    double *, Vec3D&, double (*)[neq][neq]);

  template<int neq>
  void computeJacobianSurfaceTermNS(double[4][3], double, double[4][5], double, double[4][5],
                                    double *[4], double *, double[3][3], Vec3D&, double (*)[neq][neq]);

  void computeMaxwellSlipSurfaceTermNS(double, double, double, double *, double, double[3], double[3][3],
                                       double[3], Vec3D&, double *);

};

//------------------------------------------------------------------------------

inline
NavierStokesTerm::NavierStokesTerm(IoData& iod, VarFcn *vf) : varFcn(vf), ioData(iod) {
  ooreynolds_mu = 1.0 / iod.ref.reynolds_mu;
  dRe_mudMachNS = iod.ref.dRe_mudMach;
  viscoFcn = 0;
  thermalCondFcn = 0;
  slipFcn = 0;

  if(iod.eqs.viscosityModel.type == ViscosityModelData::CONSTANT) {
    viscoFcn = new ConstantViscoFcn(iod);
  }
  else if(iod.eqs.viscosityModel.type == ViscosityModelData::SUTHERLAND) {
    viscoFcn = new SutherlandViscoFcn(iod);
  }
  else if(iod.eqs.viscosityModel.type == ViscosityModelData::PRANDTL) {
    viscoFcn = new PrandtlViscoFcn(iod);
  }
  if(iod.eqs.thermalCondModel.type == ThermalCondModelData::CONSTANT_PRANDTL) {
    thermalCondFcn = new ConstantPrandtlThermalCondFcn(iod, viscoFcn, varFcn);
  }
  else if(iod.eqs.thermalCondModel.type == ThermalCondModelData::CONSTANT) {
    thermalCondFcn = new ConstantThermalCondFcn(iod);
  }
  if(iod.bc.wall.slipModel == BcsWallData::MAXWELL) {
    slipFcn = new MaxwellSlipFcn(iod);
  }
  if(iod.eqs.tc.stm.vector_perturbation == StochasticTurbulenceModelData::OFF_VE) {
    vector_perturbation = 0;
  } else if(iod.eqs.tc.stm.vector_perturbation == StochasticTurbulenceModelData::PSI) {
    vector_perturbation = 2;
  } else {
    vector_perturbation = 1;
  }
  value_perturbation = ((iod.eqs.tc.stm.value_perturbation == StochasticTurbulenceModelData::OFF_VA) ? 0 : 1);
  scale_perturbation = ((iod.eqs.tc.stm.scale_perturbation == StochasticTurbulenceModelData::OFF_S) ? 0 : 1);
  uq_urlx = iod.eqs.tc.ep.uq_urlx;
  kfactor = 1.0;
  double vectWeight = 0;
  bool isStochasticTurbulenceModel = ((vector_perturbation || value_perturbation || scale_perturbation) ? true : false);
  // ioData.com->fprintf(stdout, " isStoch = %i, vector = %i; value = %i; scale = %i;\n", isStochasticTurbulenceModel, vector_perturbation, value_perturbation, scale_perturbation);
  if(isStochasticTurbulenceModel && iod.eqs.tc.stm.readOutput) {
    double psiStruct[9] = {};
    double broadcastVals[6];
    if(ioData.com->cpuNum() == 0 && psi[0][0] == 0) {
      FILE *fp = fopen(iod.eqs.tc.stm.outputFileName, "r");
      if(!fp) {
        fprintf(stderr, "*** Error: could not open \'%s\'\n", iod.eqs.tc.stm.outputFileName);
        exit(1);
      }
      fclose(fp);

      int iInc = 0;
      std::ifstream infile(iod.eqs.tc.stm.outputFileName);
      std::string line;
      while(std::getline(infile, line)) {
        int first = line.find("=");
        first += 2;
        int last = line.find(";");
        std::string strNew = line.substr(first, last-first);
        double temp = std::stod(strNew);
        if(iInc < 9){
          int j = iInc%3;
          int i = floor(iInc/3);
          psi[i][j] = temp;
        } else if(iInc == 9) {
          cJ[0] = temp;
          broadcastVals[0] = temp;
        } else if(iInc == 10) {
          cJ[1] = temp;
          broadcastVals[1] = temp;
        } else if(iInc == 11) {
          cJ[2] = temp;
          broadcastVals[2] = temp;
        } else if(iInc == 12) {
          z = temp;
          broadcastVals[3] = temp;
        } else if(iInc == 13) {
          kfactor = temp;
          broadcastVals[4] = temp;
        } else if(iInc == 14) {
          vectWeight = temp;
          broadcastVals[5] = temp;
          break;
        }
        iInc += 1;
      }
      ioData.com->fprintf(stdout, "Stochastic Draws:\npsi = [%e, %e, %e; %e, %e, %e; %e, %e, %e];\ncJ = [%f, %f, %f]; z = %e;\n", psi[0][0], psi[0][1], psi[0][2], psi[1][0], psi[1][1], psi[1][2], psi[2][0], psi[2][1], psi[2][2], cJ[0], cJ[1], cJ[2], z);
      ioData.com->fprintf(stdout, "kfactor = %e; vectWeight = %e;\n", kfactor, vectWeight);
      // ioData.com->barrier();
      for(int i = 0; i < 3; i++) {
        for(int j = 0; j < 3; j++) {
          int l = (i * 3) + j;
          psiStruct[l] = psi[i][j];
        }
      }
    }
    ioData.com->broadcast(9, psiStruct, 0);
    ioData.com->broadcast(6, broadcastVals, 0);
    cJ[0] = broadcastVals[0];
    cJ[1] = broadcastVals[1];
    cJ[2] = broadcastVals[2];
    z     = broadcastVals[3];
    kfactor= broadcastVals[4];
    vectWeight  = broadcastVals[5];
    for(int i = 0; i < 3; i++) {
      for(int j = 0; j < 3; j++) {
        int l = (i * 3) + j;
        psi[i][j] = psiStruct[l];
      }
    }
    uq_urlx = iod.eqs.tc.stm.uq_urlx;
  } else {
    // ioData.com->fprintf(stdout, "\nMain Block\n");
    if(iod.eqs.tc.ep.eigenvalue == EigenperturbationData::UNPERTURBEDVALUE) {
      eigenvalue = 0;
    } else if(iod.eqs.tc.ep.eigenvalue == EigenperturbationData::C1) {
      eigenvalue = 1;
    } else if(iod.eqs.tc.ep.eigenvalue == EigenperturbationData::C2) {
      eigenvalue = 2;
    } else if(iod.eqs.tc.ep.eigenvalue == EigenperturbationData::C3) {
      eigenvalue = 3;
    }
    if(iod.eqs.tc.ep.eigenvector == EigenperturbationData::UNPERTURBEDVECTOR) {
      eigenvector = 0;
    } else if(iod.eqs.tc.ep.eigenvector == EigenperturbationData::MINIMUM) {
      eigenvector = 1;
    }  else if(iod.eqs.tc.ep.eigenvector == EigenperturbationData::MAXIMUM) {
      eigenvector = 2;
    }
    if (eigenvector || eigenvalue) {
      ioData.com->fprintf(stdout, "Eigenvalue = %i (Component), Eigenvector  %i (1 Min, 2 Max)\n", eigenvalue, eigenvector);
      ioData.com->fprintf(stdout, "Relaxation Factor = %e;\n\n", uq_urlx);
    }

    if(isStochasticTurbulenceModel) {
      double delta_psi = iod.eqs.tc.stm.delta_psi;
      double mu_z = iod.eqs.tc.stm.mu_z;
      double delta_z = iod.eqs.tc.stm.delta_z;
      double mu_J = iod.eqs.tc.stm.mu_J;
      double p0 = iod.eqs.tc.stm.p0; //EJ decide if should put these inside the loop
      double mu_k = iod.eqs.tc.stm.mu_k;
      double delta_k = iod.eqs.tc.stm.delta_k;
      double mu_zeta = iod.eqs.tc.stm.mu_zeta;
      // double delta_zeta = iod.eqs.tc.stm.delta_zeta;
      double mu_eta = iod.eqs.tc.stm.mu_eta;
      // double delta_eta = iod.eqs.tc.stm.delta_eta;
      double mu_theta = iod.eqs.tc.stm.mu_theta;
      // double delta_theta = iod.eqs.tc.stm.delta_theta;
      double mu_iota = iod.eqs.tc.stm.mu_iota;
      // double delta_iota = iod.eqs.tc.stm.delta_iota;
      double delta_ev = iod.eqs.tc.stm.delta_ev;
      std::random_device rd;
      std::mt19937 generator(rd());
      std::normal_distribution<> distN(0, 1);
      std::uniform_real_distribution<> distU(0, 1);
      if(ioData.com->cpuNum() == 0 && scale_perturbation) {
        mu_k = iod.eqs.tc.stm.mu_k;
        delta_k = iod.eqs.tc.stm.delta_k;
        std::normal_distribution<> distN_k(mu_k, delta_k);
        kfactor = -1;
        while(kfactor < 1 || kfactor > iod.eqs.tc.stm.max_k) {
          kfactor = distN_k(generator);
        }
      }
      // ------------------------------------------------------------
      double G[3][3];
      double psiStruct[9] = {};
      double psiTemp[3][3]; // to do: delete
      double broadcastVals[6];
      double zeta, eta, theta, iota;
      if(ioData.com->cpuNum() == 0 && psi[0][0] == 0) {
        if(vector_perturbation == 2) {
          for(int i = 0; i < 3; i++) {
            for(int j = 0; j < 3; j++) {
              G[i][j] = distN(generator);
            }
          }
          for(int i = 0; i < 3; i++) {
            for(int j = 0; j < 3; j++) {
              psiTemp[i][j] = 0.5 * delta_psi * (G[i][j] - G[j][i]);
            }
          }
          Eigen::Map<Eigen::Matrix3d> psiEig(&psiTemp[0][0], 3, 3);
          const Eigen::Matrix3d& expPsiEig = psiEig.exp();
          Eigen::Map<Eigen::Matrix<double,3,3,Eigen::RowMajor>>(&psiTemp[0][0], 3, 3) = expPsiEig; //note psi_temp is transposed
          for(int i = 0; i < 3; i++) {
            for(int j = 0; j < 3; j++) {
              int l = (i * 3) + j;
              psiStruct[l] = psiTemp[j][i];
            }
          }
        } else if(vector_perturbation == 1) {
          // ioData.com->barrier();
          // ------------------------------------------------------------
          // ------- NORMAL QUATERNION APPROACH -------------------------
          std::normal_distribution<> distN_zeta(mu_zeta, delta_ev);
          std::normal_distribution<> distN_eta(mu_eta, delta_ev);
          std::normal_distribution<> distN_theta(mu_theta, delta_ev);
          double checkEps = 1e-15;
          zeta = distN_zeta(generator);//std::fabs(distN_zeta(generator));   // Goal x1
          eta = distN_eta(generator);     // Goal x2
          theta = distN_theta(generator); // Goal x3
          // zeta = (delta_zeta < checkEps) ? mu_zeta : distN_zeta(generator);//std::fabs(distN_zeta(generator));   // Goal x
          // eta = (delta_theta < checkEps) ? mu_eta : distN_eta(generator);     // Goal y
          // theta = (delta_eta < checkEps) ? mu_theta : distN_theta(generator); // Goal z
          double n_norm = sqrt(zeta * zeta + eta * eta + theta * theta);
          zeta = zeta / n_norm;
          eta = eta / n_norm;
          theta = theta / n_norm;
          // ioData.com->fprintf(stdout, "zeta = %e; eta = %e; theta = %e; n_norm = %e\n\n", zeta, eta, theta, n_norm);
          double rotm[3][3] ={};
          if((std::fabs(zeta) - 1.0 <= checkEps) && (std::fabs(eta) <= checkEps) && (std::fabs(theta) <= checkEps)) {
            double diagVal = 1.0;
            if(zeta < 0.) {
              diagVal = -1.0;
            }
            rotm[0][0] = diagVal;
            rotm[1][1] = diagVal;
            rotm[2][2] = diagVal;
          } else {
            // ioData.com->fprintf(stdout, "Hit2\n");
            // ioData.com->fprintf(stdout, "zeta = %e; eta = %e; theta = %e; n_norm = %e\n\n", zeta, eta, theta, n_norm);
            double ref0 = 1, ref1 = 0, ref2 = 0; //reference base rotation vector. Rotates to [zeta, eta, theta] via rotm
            Eigen::Vector3d a(ref0, ref1, ref2);
            Eigen::Vector3d b(zeta, eta, theta);
            Eigen::Vector3d v = a.cross(b);
            double s = v.norm();
            double c = a.dot(b);
            Eigen::Matrix3d vx;
            vx << 0, -v[2], v[1], v[2], 0, -v[0], -v[1], v[0], 0;
            Eigen::Matrix3d rotmEig = Eigen::Matrix3d::Identity(3,3);
            rotmEig = rotmEig + vx + vx*vx*((1-c)/std::pow(s, 2));
            Eigen::Map<Eigen::Matrix<double,3,3,Eigen::RowMajor>>(&rotm[0][0], 3, 3) = rotmEig;
          }
          std::normal_distribution<> distN_iota(mu_iota, delta_ev);
          iota = 100.;
          double iotaUp = 2 * M_PI;
          double iotaLow = 0.0;
          while (iota > iotaUp || iota < iotaLow) {
            iota = distN_iota(generator);
          }
          Eigen::Matrix3d rotxEig = Eigen::AngleAxisd(iota, Eigen::Vector3d::UnitX()).toRotationMatrix();
          double rotx[3][3];
          Eigen::Map<Eigen::Matrix<double,3,3,Eigen::RowMajor>>(&rotx[0][0], 3, 3) = rotxEig;
          // ioData.com->barrier();
          for(int i = 0; i < 3; i++) {
            for(int j = 0; j < 3; j++) {
              for(int i3 = 0; i3 < 3; ++i3) {
                psiTemp[i][j] += rotx[i][i3] * rotm[i3][j];
              }
              int l = (i * 3) + j;
              psiStruct[l] = psiTemp[i][j]; //need to flip to transpose correctly in the un-roll
            }
          }
        } else {
          psiStruct[0] = 1.0;
          psiStruct[4] = 1.0;
          psiStruct[8] = 1.0;
        }
        // ------------------------------------------------------------
        if(value_perturbation) {
          double sampleJc = distU(generator);
          int baryComp;
          if(sampleJc <= p0) {
            baryComp = (int)mu_J;
          } else if(sampleJc <= ((1.0 - p0) / 2.0 + p0)) {
            baryComp = (int)mu_J + 1;
          } else {
            baryComp = (int)mu_J + 2;
          }
          baryComp = baryComp > 3 ? baryComp % 3 : baryComp; 
          if(baryComp == 1) {
            cJ[0] = 1.0;
          } else if(baryComp == 2) {
            cJ[1] = 1.0;
          } else { //baryComp ==3
            cJ[2] = 1.0;
          }
          // ------------------------------------------------------------
          // STEP 4: generate z value
          // STEP 4b1: randomly sample xi
          // double xi;
          if(delta_z > 1e-10) {
            double factor1 = 1.0 / delta_z;
            double factor2 = (1.0 / delta_z) * (1.0 / mu_z - 1.0);
            double factor = std::min({factor1, factor2});
            double xi;
            xi = distN(generator); // * factor / 10; // note apply factors to encourage draw to be in support
            // STEP 4b2: check if in support, then keep and use
            while(xi < (-1.0 * factor1) || xi > factor2) {
              xi = distN(generator); // * factor / 10;
            }
            z = mu_z * (1.0 + delta_z * xi);
          } else {
            z = mu_z;
          }
        }
        broadcastVals[0] = cJ[0];
        broadcastVals[1] = cJ[1];
        broadcastVals[2] = cJ[2];
        broadcastVals[3] = z;
        broadcastVals[4] = kfactor;
        broadcastVals[5] = vectWeight;
        for(int i = 0; i < 3; i++) {
          for(int j = 0; j < 3; j++) {
            int l = (i * 3) + j;
            psi[i][j] = psiStruct[l];
          }
        }
        // ioData.com->fprintf(stdout, "\nStochastic Hyperparameters:\ndelta_psi = %e; mu_z = %e; delta_z = %e; mu_J = %e; p0 = %e;\n", delta_psi, mu_z, delta_z, mu_J, p0);
        // ioData.com->fprintf(stdout, "mu_k = %e; delta_k = %e; mu_zeta = %e; delta_zeta = %e;\nmu_eta = %e; delta_eta = %e; mu_theta = %e; delta_theta = %e;\n", mu_k, delta_k, mu_zeta, delta_zeta, mu_eta, delta_eta, mu_theta, delta_theta);
        ioData.com->fprintf(stdout, "Stochastic Draws:\npsi = [%e, %e, %e; %e, %e, %e; %e, %e, %e];\ncJ = [%f, %f, %f]; z = %e;\n", psi[0][0], psi[0][1], psi[0][2], psi[1][0], psi[1][1], psi[1][2], psi[2][0], psi[2][1], psi[2][2], cJ[0], cJ[1], cJ[2], z);
        ioData.com->fprintf(stdout, "kfactor = %e\n", kfactor);
        // ioData.com->fprintf(stdout, "Weights: zeta = %e;\n", zeta);
        // ioData.com->fprintf(stdout, "value_perturbation = %i; vector_perturbation = %i; scale_perturbation = %i; [0 = OFF, 1 = ON, 2 = PSI]\n", value_perturbation, vector_perturbation, scale_perturbation);
        // ioData.com->barrier();
      }
      ioData.com->broadcast(9, psiStruct, 0);
      ioData.com->broadcast(6, broadcastVals, 0);
      for(int i = 0; i < 3; i++) {
        for(int j = 0; j < 3; j++) {
          int l = (i * 3) + j;
          psi[i][j] = psiStruct[l];
        }
      }
      cJ[0] = broadcastVals[0];
      cJ[1] = broadcastVals[1];
      cJ[2] = broadcastVals[2];
      z     = broadcastVals[3];
      kfactor= broadcastVals[4];
      vectWeight  = broadcastVals[5];
      uq_urlx = iod.eqs.tc.stm.uq_urlx;
      ioData.com->fprintf(stdout, "Relaxation Factor = %e;\n\n", uq_urlx);
    }
    if(iod.eqs.tc.stm.writeOutput && ioData.com->cpuNum() == 0) {
      FILE *fp = fopen(iod.eqs.tc.stm.outputFileName, "w");
      if(!fp) {
        fprintf(stderr, "*** Error: could not open \'%s\'\n", iod.eqs.tc.stm.outputFileName);
        exit(1);
      }
      for(int i = 0; i < 3; i++) {
        for(int j = 0; j < 3; j++) {
          fprintf(fp, "Psi_%i_%i = %e;\n", i, j, psi[i][j]);
        }
      }
      fprintf(fp, "cJ_0 = %e;\n", cJ[0]);
      fprintf(fp, "cJ_1 = %e;\n", cJ[1]);
      fprintf(fp, "cJ_2 = %e;\n", cJ[2]);
      fprintf(fp, "z = %e;\n", z);
      fprintf(fp, "kfactor = %e;\n", kfactor);
      fprintf(fp, "vectWeight = %e;\n", vectWeight);
      fprintf(fp, "\n");
      fclose(fp);
    }
  }
  if(ioData.eqs.tc.stm.computeStochasticDrawWeights) { // maybe ioData.com->cpuNum() == 0
    // double factorV;
    ioData.eqs.tc.stm.weight_C1 = 0.;
    ioData.eqs.tc.stm.weight_C2 = 0.;
    ioData.eqs.tc.stm.weight_C3 = 0.;
    if(cJ[0] == 1) {
      // factorV = 2;
      // ioData.eqs.tc.stm.weight_J = 1./3;
      ioData.eqs.tc.stm.weight_C1 = z;
    } else if(cJ[1] == 1) {
      // factorV = 1;
      // ioData.eqs.tc.stm.weight_J = 2./3;
      ioData.eqs.tc.stm.weight_C2 = z;
    } else if(cJ[2] == 1.0) {
      // factorV = -1;
      // ioData.eqs.tc.stm.weight_J = 1.;
      ioData.eqs.tc.stm.weight_C3 = z;
    }
    // ioData.eqs.tc.stm.weight_valComp = (0.5 * z * factorV + 0.5) / 1.5;
    // ioData.eqs.tc.stm.weight_vectComp = 2.0 * std::acos(std::fabs(vectWeight)) / M_PI;
    // ioData.eqs.tc.stm.weight_z = z;
    ioData.eqs.tc.stm.weight_eul2 = 2.0 * std::fabs(atan2(-1.0*psi[2][0], sqrt(psi[0][0] * psi[0][0] + psi[1][0] * psi[1][0]))) / M_PI;
    ioData.eqs.tc.stm.weight_k = kfactor;
    // ioData.com->fprintf(stdout, "Weights: weight_J = %e; weight_z = %e; weight_eul2 = %e;\n\n", ioData.eqs.tc.stm.weight_J, ioData.eqs.tc.stm.weight_z, ioData.eqs.tc.stm.weight_eul2);

  }
}

//------------------------------------------------------------------------------

inline
NavierStokesTerm::~NavierStokesTerm() {
  if(viscoFcn) {
    delete viscoFcn;
  }
  if(thermalCondFcn) {
    delete thermalCondFcn;
  }
}

//------------------------------------------------------------------------------

inline
void NavierStokesTerm::rstVar(IoData& iod) {
  ooreynolds_mu = 1.0 / iod.ref.reynolds_mu;
  dRe_mudMachNS = iod.ref.dRe_mudMach;
  viscoFcn->rstVar(iod);
}

//------------------------------------------------------------------------------

inline
void NavierStokesTerm::computeLaminarTransportCoefficients(double T, int tag,
                                                           double& mu, double& lambda,
                                                           double& kappa, int code) {
  mu     = viscoFcn->compute_mu(T, tag);
  lambda = viscoFcn->compute_lambda(mu, code);
  kappa  = thermalCondFcn->compute(T, tag);
}

//------------------------------------------------------------------------------

inline
void NavierStokesTerm::computeDerivativeOfLaminarTransportCoefficients(double T, double dT, int tag,
                                                                       double mu, bool dMach, double& dmu,
                                                                       double& dlambda, double& dkappa,
                                                                       int code) {
  dmu     = viscoFcn->compute_muDerivative(T, dT, dMach, tag);
  dlambda = viscoFcn->compute_lambdaDerivative(mu, dmu, code);
  dkappa  = thermalCondFcn->computeDerivative(T, dT, dMach, tag);
}

//------------------------------------------------------------------------------

inline
void NavierStokesTerm::computeLaminarTransportCoefficientsDerivativeOperators(double T, int tag, double& dmudT,
                                                                              double& dmudMach, double& dlambdadT,
                                                                              double& dlambdadMach, double& dkappadT,
                                                                              double& dkappadMach, int code) {
  viscoFcn->compute_muDerivativeOperators(T, dmudT, dmudMach, tag);
  double dlambdadmu;
  viscoFcn->compute_lambdaDerivativeOperators(dlambdadmu, code);
  thermalCondFcn->computeDerivativeOperators(T, dkappadT, dkappadMach, tag);

  dlambdadT = dlambdadmu * dmudT;
  dlambdadMach = dlambdadmu * dmudMach;
}

//------------------------------------------------------------------------------

inline
void NavierStokesTerm::computeNonDimensionalLaminarTransportCoefficients(double T, int tag,
                                                                         double& mu, double& lambda,
                                                                         double& kappa, int code) {
  mu     = viscoFcn->compute_mu(T, tag);
  lambda = viscoFcn->compute_lambda(mu, code);
  kappa  = thermalCondFcn->compute(T, tag);

  mu     = ooreynolds_mu * mu;
  lambda = ooreynolds_mu * lambda;
  kappa  = ooreynolds_mu * kappa;
}

//------------------------------------------------------------------------------

inline
void NavierStokesTerm::computeDerivativeOfNonDimensionalLaminarTransportCoefficients(double T, double dT, int tag,
                                                                                     double mu, double lambda, double kappa,
                                                                                     double dMach, double& dmu,
                                                                                     double& dlambda, double& dkappa,
                                                                                     int code) {
  dmu     = viscoFcn->compute_muDerivative(T, dT, dMach, tag);
  dlambda = viscoFcn->compute_lambdaDerivative(mu / ooreynolds_mu, dmu, code);
  dkappa  = thermalCondFcn->computeDerivative(T, dT, dMach, tag);

  double dooreynolds_mu = getdOoreynolds_mu(dMach);
  dmu     = dooreynolds_mu * (mu / ooreynolds_mu) + ooreynolds_mu * dmu;
  dlambda = dooreynolds_mu * (lambda / ooreynolds_mu) + ooreynolds_mu * dlambda;
  dkappa  = dooreynolds_mu * (kappa / ooreynolds_mu) + ooreynolds_mu * dkappa;
}

//------------------------------------------------------------------------------

inline
void NavierStokesTerm::computeNonDimensionalLaminarTransportCoefficientsDerivativeOperators(double T, int tag, double& dmudT,
                                                                                            double& dmudMach, double& dlambdadT,
                                                                                            double& dlambdadMach, double& dkappadT,
                                                                                            double& dkappadMach, int code) {
  double mu     = viscoFcn->compute_mu(T, tag);
  double lambda = viscoFcn->compute_lambda(mu, code);
  double kappa  = thermalCondFcn->compute(T, tag);
  viscoFcn->compute_muDerivativeOperators(T, dmudT, dmudMach, tag);
  double dlambdadmu;
  viscoFcn->compute_lambdaDerivativeOperators(dlambdadmu, code);
  thermalCondFcn->computeDerivativeOperators(T, dkappadT, dkappadMach, tag);

  dlambdadT = ooreynolds_mu * (dlambdadmu * dmudT);
  dmudT = ooreynolds_mu * dmudT;
  dkappadT = ooreynolds_mu * dkappadT;

  double dooreynolds_mudMach = -(ooreynolds_mu * ooreynolds_mu) * dRe_mudMachNS;
  dlambdadMach = dooreynolds_mudMach * lambda + ooreynolds_mu * dlambdadmu * dmudMach;
  dmudMach     = dooreynolds_mudMach * mu + ooreynolds_mu * dmudMach;
  dkappadMach  = dooreynolds_mudMach * kappa + ooreynolds_mu * dkappadMach;
}

//------------------------------------------------------------------------------

inline
void NavierStokesTerm::computeJacobianNonDimensionalLaminarTransportCoefficients(double Tcg, double dTcg[4][5], int tag,
                                                                                 double mu, double dmu[4][5], double dlambda[4][5],
                                                                                 double dkappa[4][5], int code) {
  double dmudTcg = ooreynolds_mu * viscoFcn->compute_dmu(Tcg, tag);
  double dkappadTcg = ooreynolds_mu * thermalCondFcn->compute_dkappa(Tcg, tag);
  // mu = ooreynolds_mu * mu'  <=>  mu' = mu/ooreynolds_mu
  // lambda = ooreynolds_mu * lambda'(mu')
  // dlambda/dmu = ooreynolds_mu * dlambda'/dmu' * dmu'/dmu
  //             = ooreynolds_mu * viscoFcn->compute_dlambda(mu/ooreynolds_mu) * (1/ooreynolds_mu)
  //             = viscoFcn->compute_dlambda(mu/ooreynolds_mu, code)
  double dlambdadmu = viscoFcn->compute_dlambda(mu / ooreynolds_mu, code);
  for(int k = 0; k < 4; ++k) {
    for(int i = 0; i < 5; ++i) {
      dkappa[k][i]  = dkappadTcg * dTcg[k][i];
      dmu[k][i]     = dmudTcg * dTcg[k][i];
      dlambda[k][i] = dlambdadmu * dmu[k][i];
    }
  }
}

//------------------------------------------------------------------------------

inline
void NavierStokesTerm::computeTemperature(double *V, int tag, double& T) {
  T = varFcn->computeTemperature(V, tag);
}

//------------------------------------------------------------------------------

inline
void NavierStokesTerm::computeDerivativeOfTemperature(double *V, double *dV, int tag, double& dT) {
  dT = varFcn->computeDerivativeOfTemperature(V, dV, tag);
}

//------------------------------------------------------------------------------

inline
void NavierStokesTerm::computeTemperature(double *V[4], int tag, double T[4], double& Tcg) {
  T[0] = varFcn->computeTemperature(V[0], tag);
  T[1] = varFcn->computeTemperature(V[1], tag);
  T[2] = varFcn->computeTemperature(V[2], tag);
  T[3] = varFcn->computeTemperature(V[3], tag);
  Tcg = fourth * (T[0] + T[1] + T[2] + T[3]);
}

//------------------------------------------------------------------------------

inline
void NavierStokesTerm::computeDerivativeOfTemperature(double *V[4], double *dV[4], int tag, double dT[4], double& dTcg) {
  dT[0] = varFcn->computeDerivativeOfTemperature(V[0], dV[0], tag);
  dT[1] = varFcn->computeDerivativeOfTemperature(V[1], dV[1], tag);
  dT[2] = varFcn->computeDerivativeOfTemperature(V[2], dV[2], tag);
  dT[3] = varFcn->computeDerivativeOfTemperature(V[3], dV[3], tag);
  dTcg = fourth * (dT[0] + dT[1] + dT[2] + dT[3]);
}

//------------------------------------------------------------------------------

inline
void NavierStokesTerm::computeDerivativeOperatorsOfTemperature(double *V[4], int tag, double dTdV[4][5], double dTcgdV[4][5]) {
  varFcn->computeDerivativeOperatorsOfTemperature(V[0], dTdV[0], tag);
  varFcn->computeDerivativeOperatorsOfTemperature(V[1], dTdV[1], tag);
  varFcn->computeDerivativeOperatorsOfTemperature(V[2], dTdV[2], tag);
  varFcn->computeDerivativeOperatorsOfTemperature(V[3], dTdV[3], tag);
  for(int i = 0; i < 4; ++i)
    for(int j = 0; j < 5; ++j) {
      dTcgdV[i][j] = dTdV[i][j] * fourth;
    }
}

//------------------------------------------------------------------------------

template<int dim>
inline
void NavierStokesTerm::computeJacobianTemperature(double *V[4], int tag, double T[4], double dTcg[4][dim]) {
  for(int i = 0; i < 4; ++i) {
    varFcn->computeJacobianTemperature(V[i], dTcg[i], tag);
    for(int j = 0; j < dim; ++j) {
      dTcg[i][j] *= fourth;
    }
  }
}

//------------------------------------------------------------------------------

inline
void NavierStokesTerm::computeTemperatureGradient(double dp1dxj[4][3], double T[4],
                                                  double dTdxj[3]) {
  dTdxj[0] = dp1dxj[0][0] * T[0] + dp1dxj[1][0] * T[1] +
             dp1dxj[2][0] * T[2] + dp1dxj[3][0] * T[3];
  dTdxj[1] = dp1dxj[0][1] * T[0] + dp1dxj[1][1] * T[1] +
             dp1dxj[2][1] * T[2] + dp1dxj[3][1] * T[3];
  dTdxj[2] = dp1dxj[0][2] * T[0] + dp1dxj[1][2] * T[1] +
             dp1dxj[2][2] * T[2] + dp1dxj[3][2] * T[3];
}

//------------------------------------------------------------------------------

inline
void NavierStokesTerm::computeDerivativeOfTemperatureGradient(double dp1dxj[4][3], double ddp1dxj[4][3], double T[4],
                                                              double dT[4], double ddTdxj[3]) {
  ddTdxj[0] = ddp1dxj[0][0] * T[0] + dp1dxj[0][0] * dT[0] + ddp1dxj[1][0] * T[1] + dp1dxj[1][0] * dT[1] +
              ddp1dxj[2][0] * T[2] + dp1dxj[2][0] * dT[2] + ddp1dxj[3][0] * T[3] + dp1dxj[3][0] * dT[3];
  ddTdxj[1] = ddp1dxj[0][1] * T[0] + dp1dxj[0][1] * dT[0] + ddp1dxj[1][1] * T[1] + dp1dxj[1][1] * dT[1] +
              ddp1dxj[2][1] * T[2] + dp1dxj[2][1] * dT[2] + ddp1dxj[3][1] * T[3] + dp1dxj[3][1] * dT[3];
  ddTdxj[2] = ddp1dxj[0][2] * T[0] + dp1dxj[0][2] * dT[0] + ddp1dxj[1][2] * T[1] + dp1dxj[1][2] * dT[1] +
              ddp1dxj[2][2] * T[2] + dp1dxj[2][2] * dT[2] + ddp1dxj[3][2] * T[3] + dp1dxj[3][2] * dT[3];
}

//------------------------------------------------------------------------------

inline
void NavierStokesTerm::computeDerivativeOperatorsOfTemperatureGradient(double dp1dxj[4][3], double T[4],
                                                                       double ddTdxjddp1dxj[3][4][3], double ddTdxjdT[3][4]) {
  ddTdxjddp1dxj[0][0][0] = T[0];
  ddTdxjddp1dxj[0][1][0] = T[1];
  ddTdxjddp1dxj[0][2][0] = T[2];
  ddTdxjddp1dxj[0][3][0] = T[3];
  ddTdxjdT[0][0] = dp1dxj[0][0];
  ddTdxjdT[0][1] = dp1dxj[1][0];
  ddTdxjdT[0][2] = dp1dxj[2][0];
  ddTdxjdT[0][3] = dp1dxj[3][0];
  ddTdxjddp1dxj[1][0][1] = T[0];
  ddTdxjddp1dxj[1][1][1] = T[1];
  ddTdxjddp1dxj[1][2][1] = T[2];
  ddTdxjddp1dxj[1][3][1] = T[3];
  ddTdxjdT[1][0] = dp1dxj[0][1];
  ddTdxjdT[1][1] = dp1dxj[1][1];
  ddTdxjdT[1][2] = dp1dxj[2][1];
  ddTdxjdT[1][3] = dp1dxj[3][1];
  ddTdxjddp1dxj[2][0][2] = T[0];
  ddTdxjddp1dxj[2][1][2] = T[1];
  ddTdxjddp1dxj[2][2][2] = T[2];
  ddTdxjddp1dxj[2][3][2] = T[3];
  ddTdxjdT[2][0] = dp1dxj[0][2];
  ddTdxjdT[2][1] = dp1dxj[1][2];
  ddTdxjdT[2][2] = dp1dxj[2][2];
  ddTdxjdT[2][3] = dp1dxj[3][2];
}

//------------------------------------------------------------------------------

inline
void NavierStokesTerm::computeDensity(double *V[4], double rho[4], double& rhocg) {
  rho[0] = V[0][0];
  rho[1] = V[1][0];
  rho[2] = V[2][0];
  rho[3] = V[3][0];
  rhocg = fourth * (rho[0] + rho[1] + rho[2] + rho[3]);
}

//------------------------------------------------------------------------------

inline
void NavierStokesTerm::computeVelocity(double *V[4], double u[4][3], double ucg[3]) {
  u[0][0] = V[0][1];
  u[0][1] = V[0][2];
  u[0][2] = V[0][3];
  u[1][0] = V[1][1];
  u[1][1] = V[1][2];
  u[1][2] = V[1][3];
  u[2][0] = V[2][1];
  u[2][1] = V[2][2];
  u[2][2] = V[2][3];
  u[3][0] = V[3][1];
  u[3][1] = V[3][2];
  u[3][2] = V[3][3];
  ucg[0] = fourth * (u[0][0] + u[1][0] + u[2][0] + u[3][0]);
  ucg[1] = fourth * (u[0][1] + u[1][1] + u[2][1] + u[3][1]);
  ucg[2] = fourth * (u[0][2] + u[1][2] + u[2][2] + u[3][2]);
}

//------------------------------------------------------------------------------

inline
void NavierStokesTerm::computeDerivativeOfVelocity(double *dV[4], double du[4][3], double ducg[3]) {
  du[0][0] = dV[0][1];
  du[0][1] = dV[0][2];
  du[0][2] = dV[0][3];
  du[1][0] = dV[1][1];
  du[1][1] = dV[1][2];
  du[1][2] = dV[1][3];
  du[2][0] = dV[2][1];
  du[2][1] = dV[2][2];
  du[2][2] = dV[2][3];
  du[3][0] = dV[3][1];
  du[3][1] = dV[3][2];
  du[3][2] = dV[3][3];
  ducg[0] = fourth * (du[0][0] + du[1][0] + du[2][0] + du[3][0]);
  ducg[1] = fourth * (du[0][1] + du[1][1] + du[2][1] + du[3][1]);
  ducg[2] = fourth * (du[0][2] + du[1][2] + du[2][2] + du[3][2]);
}

//------------------------------------------------------------------------------

inline
void NavierStokesTerm::computeDerivativeOperatorsOfVelocity(double dudV[4][3][4][4], double ducgdV[3][4][4]) {
  dudV[0][2][0][3] = dudV[0][1][0][2] = dudV[0][0][0][1] = 1.0;
  dudV[1][2][1][3] = dudV[1][1][1][2] = dudV[1][0][1][1] = 1.0;
  dudV[2][2][2][3] = dudV[2][1][2][2] = dudV[2][0][2][1] = 1.0;
  dudV[3][2][3][3] = dudV[3][1][3][2] = dudV[3][0][3][1] = 1.0;
  double ducgdu[3][4][3] = {0};
  ducgdu[0][0][0] = ducgdu[0][1][0] = ducgdu[0][2][0] = ducgdu[0][3][0] = fourth;
  ducgdu[1][0][1] = ducgdu[1][1][1] = ducgdu[1][2][1] = ducgdu[1][3][1] = fourth;
  ducgdu[2][0][2] = ducgdu[2][1][2] = ducgdu[2][2][2] = ducgdu[2][3][2] = fourth;
  for(int i = 0; i < 3; ++i)
    for(int j = 0; j < 4; ++j)
      for(int k = 0; k < 3; ++k)
        for(int l = 0; l < 4; ++l)
          for(int m = 0; m < 4; ++m) {
            ducgdV[i][l][m] += ducgdu[i][j][k] * dudV[j][k][l][m];
          }
}

//------------------------------------------------------------------------------

inline
void NavierStokesTerm::computeVelocityGradient(double dp1dxj[4][3], double u[4][3],
                                               double dudxj[3][3]) {
  dudxj[0][0] = dp1dxj[0][0] * u[0][0] + dp1dxj[1][0] * u[1][0] +
                dp1dxj[2][0] * u[2][0] + dp1dxj[3][0] * u[3][0];
  dudxj[0][1] = dp1dxj[0][1] * u[0][0] + dp1dxj[1][1] * u[1][0] +
                dp1dxj[2][1] * u[2][0] + dp1dxj[3][1] * u[3][0];
  dudxj[0][2] = dp1dxj[0][2] * u[0][0] + dp1dxj[1][2] * u[1][0] +
                dp1dxj[2][2] * u[2][0] + dp1dxj[3][2] * u[3][0];
  dudxj[1][0] = dp1dxj[0][0] * u[0][1] + dp1dxj[1][0] * u[1][1] +
                dp1dxj[2][0] * u[2][1] + dp1dxj[3][0] * u[3][1];
  dudxj[1][1] = dp1dxj[0][1] * u[0][1] + dp1dxj[1][1] * u[1][1] +
                dp1dxj[2][1] * u[2][1] + dp1dxj[3][1] * u[3][1];
  dudxj[1][2] = dp1dxj[0][2] * u[0][1] + dp1dxj[1][2] * u[1][1] +
                dp1dxj[2][2] * u[2][1] + dp1dxj[3][2] * u[3][1];
  dudxj[2][0] = dp1dxj[0][0] * u[0][2] + dp1dxj[1][0] * u[1][2] +
                dp1dxj[2][0] * u[2][2] + dp1dxj[3][0] * u[3][2];
  dudxj[2][1] = dp1dxj[0][1] * u[0][2] + dp1dxj[1][1] * u[1][2] +
                dp1dxj[2][1] * u[2][2] + dp1dxj[3][1] * u[3][2];
  dudxj[2][2] = dp1dxj[0][2] * u[0][2] + dp1dxj[1][2] * u[1][2] +
                dp1dxj[2][2] * u[2][2] + dp1dxj[3][2] * u[3][2];
}

//------------------------------------------------------------------------------

inline
void NavierStokesTerm::computeDerivativeOfVelocityGradient(double dp1dxj[4][3], double ddp1dxj[4][3],
                                                           double u[4][3], double du[4][3],
                                                           double ddudxj[3][3]) {
  ddudxj[0][0] = ddp1dxj[0][0] * u[0][0] + ddp1dxj[1][0] * u[1][0] +
                 ddp1dxj[2][0] * u[2][0] + ddp1dxj[3][0] * u[3][0] +
                 dp1dxj[0][0] * du[0][0] + dp1dxj[1][0] * du[1][0] +
                 dp1dxj[2][0] * du[2][0] + dp1dxj[3][0] * du[3][0];
  ddudxj[0][1] = ddp1dxj[0][1] * u[0][0] + ddp1dxj[1][1] * u[1][0] +
                 ddp1dxj[2][1] * u[2][0] + ddp1dxj[3][1] * u[3][0] +
                 dp1dxj[0][1] * du[0][0] + dp1dxj[1][1] * du[1][0] +
                 dp1dxj[2][1] * du[2][0] + dp1dxj[3][1] * du[3][0];
  ddudxj[0][2] = ddp1dxj[0][2] * u[0][0] + ddp1dxj[1][2] * u[1][0] +
                 ddp1dxj[2][2] * u[2][0] + ddp1dxj[3][2] * u[3][0] +
                 dp1dxj[0][2] * du[0][0] + dp1dxj[1][2] * du[1][0] +
                 dp1dxj[2][2] * du[2][0] + dp1dxj[3][2] * du[3][0];
  ddudxj[1][0] = ddp1dxj[0][0] * u[0][1] + ddp1dxj[1][0] * u[1][1] +
                 ddp1dxj[2][0] * u[2][1] + ddp1dxj[3][0] * u[3][1] +
                 dp1dxj[0][0] * du[0][1] + dp1dxj[1][0] * du[1][1] +
                 dp1dxj[2][0] * du[2][1] + dp1dxj[3][0] * du[3][1];
  ddudxj[1][1] = ddp1dxj[0][1] * u[0][1] + ddp1dxj[1][1] * u[1][1] +
                 ddp1dxj[2][1] * u[2][1] + ddp1dxj[3][1] * u[3][1] +
                 dp1dxj[0][1] * du[0][1] + dp1dxj[1][1] * du[1][1] +
                 dp1dxj[2][1] * du[2][1] + dp1dxj[3][1] * du[3][1];
  ddudxj[1][2] = ddp1dxj[0][2] * u[0][1] + ddp1dxj[1][2] * u[1][1] +
                 ddp1dxj[2][2] * u[2][1] + ddp1dxj[3][2] * u[3][1] +
                 dp1dxj[0][2] * du[0][1] + dp1dxj[1][2] * du[1][1] +
                 dp1dxj[2][2] * du[2][1] + dp1dxj[3][2] * du[3][1];
  ddudxj[2][0] = ddp1dxj[0][0] * u[0][2] + ddp1dxj[1][0] * u[1][2] +
                 ddp1dxj[2][0] * u[2][2] + ddp1dxj[3][0] * u[3][2] +
                 dp1dxj[0][0] * du[0][2] + dp1dxj[1][0] * du[1][2] +
                 dp1dxj[2][0] * du[2][2] + dp1dxj[3][0] * du[3][2];
  ddudxj[2][1] = ddp1dxj[0][1] * u[0][2] + ddp1dxj[1][1] * u[1][2] +
                 ddp1dxj[2][1] * u[2][2] + ddp1dxj[3][1] * u[3][2] +
                 dp1dxj[0][1] * du[0][2] + dp1dxj[1][1] * du[1][2] +
                 dp1dxj[2][1] * du[2][2] + dp1dxj[3][1] * du[3][2];
  ddudxj[2][2] = ddp1dxj[0][2] * u[0][2] + ddp1dxj[1][2] * u[1][2] +
                 ddp1dxj[2][2] * u[2][2] + ddp1dxj[3][2] * u[3][2] +
                 dp1dxj[0][2] * du[0][2] + dp1dxj[1][2] * du[1][2] +
                 dp1dxj[2][2] * du[2][2] + dp1dxj[3][2] * du[3][2];
}

//------------------------------------------------------------------------------

inline
void NavierStokesTerm::computeDerivativeOperatorsOfVelocityGradient(double dp1dxj[4][3], double u[4][3],
                                                                    double ddudxjddp1dxj[3][3][4][3], double ddudxjdu[3][3][4][3]) {
  if(ddudxjddp1dxj) {
    ddudxjddp1dxj[0][0][0][0] = u[0][0];
    ddudxjddp1dxj[0][0][1][0] = u[1][0];
    ddudxjddp1dxj[0][0][2][0] = u[2][0];
    ddudxjddp1dxj[0][0][3][0] = u[3][0];
    ddudxjddp1dxj[0][1][0][1] = u[0][0];
    ddudxjddp1dxj[0][1][1][1] = u[1][0];
    ddudxjddp1dxj[0][1][2][1] = u[2][0];
    ddudxjddp1dxj[0][1][3][1] = u[3][0];
    ddudxjddp1dxj[0][2][0][2] = u[0][0];
    ddudxjddp1dxj[0][2][1][2] = u[1][0];
    ddudxjddp1dxj[0][2][2][2] = u[2][0];
    ddudxjddp1dxj[0][2][3][2] = u[3][0];
    ddudxjddp1dxj[1][0][0][0] = u[0][1];
    ddudxjddp1dxj[1][0][1][0] = u[1][1];
    ddudxjddp1dxj[1][0][2][0] = u[2][1];
    ddudxjddp1dxj[1][0][3][0] = u[3][1];
    ddudxjddp1dxj[1][1][0][1] = u[0][1];
    ddudxjddp1dxj[1][1][1][1] = u[1][1];
    ddudxjddp1dxj[1][1][2][1] = u[2][1];
    ddudxjddp1dxj[1][1][3][1] = u[3][1];
    ddudxjddp1dxj[1][2][0][2] = u[0][1];
    ddudxjddp1dxj[1][2][1][2] = u[1][1];
    ddudxjddp1dxj[1][2][2][2] = u[2][1];
    ddudxjddp1dxj[1][2][3][2] = u[3][1];
    ddudxjddp1dxj[2][0][0][0] = u[0][2];
    ddudxjddp1dxj[2][0][1][0] = u[1][2];
    ddudxjddp1dxj[2][0][2][0] = u[2][2];
    ddudxjddp1dxj[2][0][3][0] = u[3][2];
    ddudxjddp1dxj[2][1][0][1] = u[0][2];
    ddudxjddp1dxj[2][1][1][1] = u[1][2];
    ddudxjddp1dxj[2][1][2][1] = u[2][2];
    ddudxjddp1dxj[2][1][3][1] = u[3][2];
    ddudxjddp1dxj[2][2][0][2] = u[0][2];
    ddudxjddp1dxj[2][2][1][2] = u[1][2];
    ddudxjddp1dxj[2][2][2][2] = u[2][2];
    ddudxjddp1dxj[2][2][3][2] = u[3][2];
  }
  if(ddudxjdu) {
    ddudxjdu[0][0][0][0] = dp1dxj[0][0];
    ddudxjdu[0][0][1][0] = dp1dxj[1][0];
    ddudxjdu[0][0][2][0] = dp1dxj[2][0];
    ddudxjdu[0][0][3][0] = dp1dxj[3][0];
    ddudxjdu[0][1][0][0] = dp1dxj[0][1];
    ddudxjdu[0][1][1][0] = dp1dxj[1][1];
    ddudxjdu[0][1][2][0] = dp1dxj[2][1];
    ddudxjdu[0][1][3][0] = dp1dxj[3][1];
    ddudxjdu[0][2][0][0] = dp1dxj[0][2];
    ddudxjdu[0][2][1][0] = dp1dxj[1][2];
    ddudxjdu[0][2][2][0] = dp1dxj[2][2];
    ddudxjdu[0][2][3][0] = dp1dxj[3][2];
    ddudxjdu[1][0][0][1] = dp1dxj[0][0];
    ddudxjdu[1][0][1][1] = dp1dxj[1][0];
    ddudxjdu[1][0][2][1] = dp1dxj[2][0];
    ddudxjdu[1][0][3][1] = dp1dxj[3][0];
    ddudxjdu[1][1][0][1] = dp1dxj[0][1];
    ddudxjdu[1][1][1][1] = dp1dxj[1][1];
    ddudxjdu[1][1][2][1] = dp1dxj[2][1];
    ddudxjdu[1][1][3][1] = dp1dxj[3][1];
    ddudxjdu[1][2][0][1] = dp1dxj[0][2];
    ddudxjdu[1][2][1][1] = dp1dxj[1][2];
    ddudxjdu[1][2][2][1] = dp1dxj[2][2];
    ddudxjdu[1][2][3][1] = dp1dxj[3][2];
    ddudxjdu[2][0][0][2] = dp1dxj[0][0];
    ddudxjdu[2][0][1][2] = dp1dxj[1][0];
    ddudxjdu[2][0][2][2] = dp1dxj[2][0];
    ddudxjdu[2][0][3][2] = dp1dxj[3][0];
    ddudxjdu[2][1][0][2] = dp1dxj[0][1];
    ddudxjdu[2][1][1][2] = dp1dxj[1][1];
    ddudxjdu[2][1][2][2] = dp1dxj[2][1];
    ddudxjdu[2][1][3][2] = dp1dxj[3][1];
    ddudxjdu[2][2][0][2] = dp1dxj[0][2];
    ddudxjdu[2][2][1][2] = dp1dxj[1][2];
    ddudxjdu[2][2][2][2] = dp1dxj[2][2];
    ddudxjdu[2][2][3][2] = dp1dxj[3][2];
  }
}

//------------------------------------------------------------------------------

inline
void NavierStokesTerm::computeTurbulentStressTensor(double rho, double mu, double lambda, double dudxj[3][3], double tij[3][3], double k) {
  double div = dudxj[0][0] + dudxj[1][1] + dudxj[2][2];
  double k_term = 2.0 / 3.0 * rho * k;
  // double k_term = 0.0;
  double rhom = -1.0 / rho;
  tij[0][0] = (lambda * div + 2.0 * mu * dudxj[0][0] - k_term) * rhom;
  tij[1][1] = (lambda * div + 2.0 * mu * dudxj[1][1] - k_term) * rhom;
  tij[2][2] = (lambda * div + 2.0 * mu * dudxj[2][2] - k_term) * rhom;
  tij[0][1] = (mu * (dudxj[1][0] + dudxj[0][1])) * rhom;
  tij[0][2] = (mu * (dudxj[2][0] + dudxj[0][2])) * rhom;
  tij[1][2] = (mu * (dudxj[2][1] + dudxj[1][2])) * rhom;
  tij[1][0] = tij[0][1];
  tij[2][0] = tij[0][2];
  tij[2][1] = tij[1][2];
}
//------------------------------------------------------------------------------

inline
void NavierStokesTerm::computeStressTensor(double mu, double lambda, double dudxj[3][3], double tij[3][3]) {
  double div = dudxj[0][0] + dudxj[1][1] + dudxj[2][2];
  tij[0][0] = lambda * div + 2.0 * mu * dudxj[0][0];
  tij[1][1] = lambda * div + 2.0 * mu * dudxj[1][1];
  tij[2][2] = lambda * div + 2.0 * mu * dudxj[2][2];
  tij[0][1] = mu * (dudxj[1][0] + dudxj[0][1]);
  tij[0][2] = mu * (dudxj[2][0] + dudxj[0][2]);
  tij[1][2] = mu * (dudxj[2][1] + dudxj[1][2]);
  tij[1][0] = tij[0][1];
  tij[2][0] = tij[0][2];
  tij[2][1] = tij[1][2];
}


//------------------------------------------------------------------------------

inline
void NavierStokesTerm::computeStressTensorUQ(double mul, double lambdal, double dudxj[3][3], double tij[3][3], double RSM[3][3], double rhocg) {
  computeStressTensor(mul, lambdal, dudxj, tij);
  tij[0][0] -= RSM[0][0] * rhocg;
  tij[0][1] -= RSM[0][1] * rhocg;
  tij[0][2] -= RSM[0][2] * rhocg;
  tij[1][0] -= RSM[1][0] * rhocg;
  tij[1][1] -= RSM[1][1] * rhocg;
  tij[1][2] -= RSM[1][2] * rhocg;
  tij[2][0] -= RSM[2][0] * rhocg;
  tij[2][1] -= RSM[2][1] * rhocg;
  tij[2][2] -= RSM[2][2] * rhocg;
}
//------------------------------------------------------------------------------

inline
void NavierStokesTerm::computeDerivativeOfStressTensor(double mu, double dmu, double lambda, double dlambda,
                                                       double dudxj[3][3], double ddudxj[3][3],
                                                       double dtij[3][3]) {
  double div = dudxj[0][0] + dudxj[1][1] + dudxj[2][2];
  double ddiv = ddudxj[0][0] + ddudxj[1][1] + ddudxj[2][2];
  dtij[0][0] = dlambda * div + lambda * ddiv + 2.0 * dmu * dudxj[0][0] + 2.0 * mu * ddudxj[0][0];
  dtij[1][1] = dlambda * div + lambda * ddiv + 2.0 * dmu * dudxj[1][1] + 2.0 * mu * ddudxj[1][1];
  dtij[2][2] = dlambda * div + lambda * ddiv + 2.0 * dmu * dudxj[2][2] + 2.0 * mu * ddudxj[2][2];
  dtij[0][1] = dmu * (dudxj[1][0] + dudxj[0][1]) + mu * (ddudxj[1][0] + ddudxj[0][1]);
  dtij[0][2] = dmu * (dudxj[2][0] + dudxj[0][2]) + mu * (ddudxj[2][0] + ddudxj[0][2]);
  dtij[1][2] = dmu * (dudxj[2][1] + dudxj[1][2]) + mu * (ddudxj[2][1] + ddudxj[1][2]);
  dtij[1][0] = dtij[0][1];
  dtij[2][0] = dtij[0][2];
  dtij[2][1] = dtij[1][2];
}

//------------------------------------------------------------------------------

inline
void NavierStokesTerm::computeDerivativeOperatorsOfStressTensor(double mu, double lambda, double dudxj[3][3],
                                                                double dtijddudxj[3][3][3][3], double dtijdmu[3][3],
                                                                double dtijdlambda[3][3]) {
  double div = dudxj[0][0] + dudxj[1][1] + dudxj[2][2];
  if(dtijdlambda) {
    dtijdlambda[0][0] = dtijdlambda[1][1] = dtijdlambda[2][2] = div;
    dtijdlambda[1][0] = dtijdlambda[0][1] = 0.0;
    dtijdlambda[2][0] = dtijdlambda[0][2] = 0.0;
    dtijdlambda[2][1] = dtijdlambda[1][2] = 0.0;
  }
  if(dtijdmu) {
    dtijdmu[0][0] = 2.0*dudxj[0][0];
    dtijdmu[1][1] = 2.0*dudxj[1][1];
    dtijdmu[2][2] = 2.0*dudxj[2][2];
    dtijdmu[1][0] = dtijdmu[0][1] = (dudxj[1][0] + dudxj[0][1]);
    dtijdmu[2][0] = dtijdmu[0][2] = (dudxj[2][0] + dudxj[0][2]);
    dtijdmu[2][1] = dtijdmu[1][2] = (dudxj[2][1] + dudxj[1][2]);
  }
  if(dtijddudxj) {
    dtijddudxj[0][0][0][0] = dtijddudxj[1][1][0][0] = dtijddudxj[2][2][0][0] = lambda;
    dtijddudxj[0][0][1][1] = dtijddudxj[1][1][1][1] = dtijddudxj[2][2][1][1] = lambda;
    dtijddudxj[0][0][2][2] = dtijddudxj[1][1][2][2] = dtijddudxj[2][2][2][2] = lambda;
    dtijddudxj[0][0][0][0] += 2.0 * mu;
    dtijddudxj[1][1][1][1] += 2.0 * mu;
    dtijddudxj[2][2][2][2] += 2.0 * mu;
    dtijddudxj[1][0][0][1] = dtijddudxj[1][0][1][0] = dtijddudxj[0][1][0][1] = dtijddudxj[0][1][1][0] = mu;
    dtijddudxj[2][0][0][2] = dtijddudxj[2][0][2][0] = dtijddudxj[0][2][0][2] = dtijddudxj[0][2][2][0] = mu;
    dtijddudxj[2][1][1][2] = dtijddudxj[2][1][2][1] = dtijddudxj[1][2][1][2] = dtijddudxj[1][2][2][1] = mu;
  }
}

//------------------------------------------------------------------------------

inline
void NavierStokesTerm::computeHeatFluxVector(double kappa, double dTdxj[3], double qj[3]) {
  qj[0] = -kappa * dTdxj[0];
  qj[1] = -kappa * dTdxj[1];
  qj[2] = -kappa * dTdxj[2];
}

//------------------------------------------------------------------------------

inline
void NavierStokesTerm::computeDerivativeOfHeatFluxVector(double kappa, double dkappa, double dTdxj[3],
                                                         double ddTdxj[3], double dqj[3]) {
  dqj[0] = -dkappa * dTdxj[0] - kappa * ddTdxj[0];
  dqj[1] = -dkappa * dTdxj[1] - kappa * ddTdxj[1];
  dqj[2] = -dkappa * dTdxj[2] - kappa * ddTdxj[2];
}

//------------------------------------------------------------------------------

inline
void NavierStokesTerm::computeDerivativeOperatorsOfHeatFluxVector(double kappa, double dTdxj[3], double dqjddTdxj[3][3],
                                                                  double dqjdkappa[3]) {
  dqjddTdxj[2][2] = dqjddTdxj[1][1] = dqjddTdxj[0][0] = -kappa;
  dqjdkappa[0] = -dTdxj[0];
  dqjdkappa[1] = -dTdxj[1];
  dqjdkappa[2] = -dTdxj[2];
}


//------------------------------------------------------------------------------

inline
void NavierStokesTerm::computeStochasticRSM(double rhocg, double mut, double lambdat, double dudxj[3][3], double RSM[3][3], double& mut_rel, double& TKE, double eigvalVect[3], double rhok) {
  double vals[3], vects[3][3], a[3][3], RSM_unperturbed[3][3], k, valsS[3];
  if(rhok) {
    k = rhok / rhocg;
  } else {
    checkAndUpdatek(rhocg, mut, lambdat, dudxj, k, valsS);
  }
  k = (k < 1e-15)? 1e-15: k; //could try not clipping and isntead return 0?
  TKE = k;
  computeAnisotropicReynoldsStressTensor(rhocg, mut, lambdat, dudxj, RSM_unperturbed, k, a);
  computeSpectralDecompTensor(a, vals, vects);
  double c[3], c_stochastic[3], vals_stochastic[3];
  c[0] = vals[2] - vals[1];
  c[1] = 2.0 * (vals[1] - vals[0]);
  c[2] = 3.0 * vals[0] + 1.0;
  c_stochastic[0] = c[0] + z * (cJ[0] - c[0]);
  c_stochastic[1] = c[1] + z * (cJ[1] - c[1]);
  c_stochastic[2] = c[2] + z * (cJ[2] - c[2]);
  vals_stochastic[2] = c_stochastic[0] + 0.5 * c_stochastic[1] + third * c_stochastic[2] - third;
  vals_stochastic[1] =                   0.5 * c_stochastic[1] + third * c_stochastic[2] - third;
  vals_stochastic[0] =                                           third * c_stochastic[2] - third;
  eigvalVect[2] = vals_stochastic[2];
  eigvalVect[1] = vals_stochastic[1];
  eigvalVect[0] = vals_stochastic[0];
  // ------------------------------------------------------------
  // STEP 5: compute stochastic eigenvectors
  double vects_stochastic[3][3];
  for(int i = 0; i < 3; ++i) {
    for(int j = 0; j < 3; ++j) {
      double sum = 0;
      for(int l = 0; l < 3; ++l) {
        sum += psi[i][l] * vects[l][j];
      }
      vects_stochastic[i][j] = sum;
    }
  }
  double RSM_rlx[3][3];
  if(uq_urlx < 1.0) {
    computeTurbulentStressTensor(rhocg, mut, lambdat, dudxj, RSM_rlx, 0.0);
  }
  // ------------------------------------------------------------
  // STEP 6: compute stochastic RSM
  for(int i = 0; i < 3; i++) {
    for(int j = 0; j < 3; j++) {
      a[i][j] = 0.0;
      for(int m = 0; m < 3; m++) {
        a[i][j] += vects_stochastic[i][m] * vals_stochastic[m] * vects_stochastic[j][m];
      }
      auto delta_ij = (i == j)? 1.0 : 0.0;
      RSM[i][j] = 2.0 * k * (a[i][j] + delta_ij / 3.0);
      // -------------- EJ: if relaxation: -------------------------
      if(uq_urlx < 1.0) {
        RSM[i][j] = RSM_rlx[i][j] + uq_urlx * (RSM[i][j] - RSM_rlx[i][j]);
      }
      // -----------------------------------------------------------
    }
  }

  double Amat[3][3], Bmat[3][3];
  computeStressTensor(1.0, (-2.0/3.0), dudxj, Amat);
  // double S[3][3];
  // S[0][0] = dudxj[0][0];
  // S[1][1] = dudxj[1][1];
  // S[2][2] = dudxj[2][2];
  // S[0][1] = 0.5 * (dudxj[0][1] + dudxj[1][0]);
  // S[0][2] = 0.5 * (dudxj[0][2] + dudxj[2][0]);
  // S[1][2] = 0.5 * (dudxj[1][2] + dudxj[2][1]);
  // S[1][0] = S[0][1];
  // S[2][0] = S[0][2];
  // S[2][1] = S[1][2];
  // // Spectral decomp S
  // double valsSagain[3], vectsS[3][3];
  // computeSpectralDecompTensor(S, valsSagain, vectsS);
  // // Perturb eigenvects S
  // double vectsS_stochastic[3][3];
  // for(int i = 0; i < 3; ++i) {
  //   for(int j = 0; j < 3; ++j) {
  //     double sum = 0;
  //     for (int l = 0; l < 3; ++l){
  //       sum += vectsS[i][l] * psi[l][j];
  //     }
  //     vectsS_stochastic[i][j] = sum;
  //   }
  // }
  // double Snew[3][3];
  // for(int i = 0; i < 3; i++) {
  //   for(int j = 0; j < 3; j++) {
  //     Snew[i][j] = 0.0;
  //     for(int m = 0; m < 3; m++) {
  //       Snew[i][j] += vectsS_stochastic[i][m] * valsS[m] * vectsS_stochastic[j][m];
  //     }
  //   }
  // }
  // double div = Snew[0][0] + Snew[1][1] + Snew[2][2];
  // double lambda_a = -2.0/3.0;
  // double mu_a = 1.0;
  // Amat[0][0] = lambda_a * div + 2.0 * mu_a * Snew[0][0];
  // Amat[1][1] = lambda_a * div + 2.0 * mu_a * Snew[1][1];
  // Amat[2][2] = lambda_a * div + 2.0 * mu_a * Snew[2][2];
  // Amat[0][1] = 2.0 * mu_a * Snew[0][1];
  // Amat[0][2] = 2.0 * mu_a * Snew[0][2];
  // Amat[1][2] = 2.0 * mu_a * Snew[1][2];
  // Amat[1][0] = Amat[0][1];
  // Amat[2][0] = Amat[0][2];
  // Amat[2][1] = Amat[1][2];

  // Compute Bmat
  for (int i = 0; i < 3; i++){
    for (int j = 0; j < 3; j++){
      Bmat[i][j] = -rhocg * RSM[i][j];
    }
  }
  double coef0 = 0, coef1 = 0;
  for(int i = 0; i < 3; i++) {
    for(int j = 0; j < 3; j++) {
      coef0 += (Amat[i][j] * Amat[i][j]);
      coef1 += (-2.0 * Amat[i][j] * Bmat[i][j]);
    }
  }
  mut_rel = -1.0 * coef1 / (2.0 * coef0 + 1e-15);
  if(mut_rel < 0 && (std::fabs(mut_rel) > 1e-13)) {
  } else {
    mut_rel = std::max({mut_rel, 1e-13});
  }
}

//------------------------------------------------------------------------------

inline
void NavierStokesTerm::computePerturbedRSM(double rhocg, double mut, double lambdat, double dudxj[3][3], double RSM[3][3], double& mut_rel, double rhok) {
  double vals[3], vects[3][3], a[3][3], RSM_unperturbed[3][3], k, valsS[3];
  if(rhok) {
    k = rhok / rhocg;
  } else {
    checkAndUpdatek(rhocg, mut, lambdat, dudxj, k, valsS);
  }
  k = (k < 1e-15)? 1e-15: k;
  computeAnisotropicReynoldsStressTensor(rhocg, mut, lambdat, dudxj, RSM_unperturbed, k, a);
  computeSpectralDecompTensor(a, vals, vects);

  // ------------------------------------------------------------
  if(eigenvalue == 1) {
    vals[2] = 2.0 / 3.0;
    vals[1] = -1.0 / 3.0;
    vals[0] = -1.0 / 3.0;
  } else if(eigenvalue == 2) {
    vals[2] = 1.0 / 6.0;
    vals[1] = 1.0 / 6.0;
    vals[0] = -1.0 / 3.0;
  } else if(eigenvalue == 3) {
    vals[2] = 0.0;
    vals[1] = 0.0;
    vals[0] = 0.0;
  } else {
    // case: only perturbing eigenvectors
  }
  // ------------------------------------------------------------
  if(eigenvector == 1) { //minimum production, swap eigenvectors
    double temp;
    for(int i = 0; i < 3; i++) {
      temp = vects[i][0];
      vects[i][0] = vects[i][2];
      vects[i][2] = temp;
    }
  }

  double RSM_rlx[3][3];
  if(uq_urlx < 1.0) {
    computeTurbulentStressTensor(rhocg, mut, lambdat, dudxj, RSM_rlx, 0.0);
  }
  // ------------------------------------------------------------
  // compute perturbed RSM
  for(int i = 0; i < 3; i++) {
    for(int j = 0; j < 3; j++) {
      a[i][j] = 0.0;
      for(int m = 0; m < 3; m++) {
        a[i][j] += vects[i][m] * vals[m] * vects[j][m]; //vects[m][j]; //maybe should be vects[j][m];
      }
      auto delta_ij = (i == j)? 1.0 : 0.0;
      RSM[i][j] = 2.0 * k * (a[i][j] + delta_ij / 3.0);
      // -------------- EJ: if relaxation: -------------------------
      if(uq_urlx < 1.0) {
        RSM[i][j] = RSM_rlx[i][j] + uq_urlx * (RSM[i][j] - RSM_rlx[i][j]);
      }
      // -----------------------------------------------------------
    }
  }
  //step 1: print A and B
  double Amat[3][3], Bmat[3][3];
  computeStressTensor(1.0, (-2.0/3.0), dudxj, Amat);
  for (int i = 0; i < 3; i++){
    for (int j = 0; j < 3; j++){
      Bmat[i][j] = -rhocg * RSM[i][j];
    }
  }
  //step 2: compute quad form coef
  double coef0 = 0, coef1 = 0;
  for(int i = 0; i < 3; i++) {
    for(int j = 0; j < 3; j++) {
      coef0 += (Amat[i][j] * Amat[i][j]);
      coef1 += (-2.0 * Amat[i][j] * Bmat[i][j]);
    }
  }
  mut_rel = -1.0 * coef1 / (2.0 * coef0 + 1e-15);
  mut_rel = std::max({mut_rel, 1e-15});
}

//------------------------------------------------------------------------------

inline
void NavierStokesTerm::checkAndUpdatek(double rhocg, double mut, double lambdat, double dudxj[3][3], double& k, double valsS[3]) {
  double term1, term2, check1, check2, check3;
  term1 = 3.0 / (2.0 * rhocg);
  double divU = (dudxj[0][0] + dudxj[1][1] + dudxj[2][2]);
  term2 = lambdat * (dudxj[0][0] + dudxj[1][1] + dudxj[2][2]);
  double S[3][3]; //, W[3][3];
  S[0][0] = dudxj[0][0];
  S[1][1] = dudxj[1][1];
  S[2][2] = dudxj[2][2];
  S[0][1] = 0.5 * (dudxj[0][1] + dudxj[1][0]);
  S[0][2] = 0.5 * (dudxj[0][2] + dudxj[2][0]);
  S[1][2] = 0.5 * (dudxj[1][2] + dudxj[2][1]);
  S[1][0] = S[0][1];
  S[2][0] = S[0][2];
  S[2][1] = S[1][2];
  Eigen::Map<Eigen::Matrix3d> sEig(&S[0][0], 3, 3);
  Eigen::SelfAdjointEigenSolver<Eigen::Matrix3d> es(sEig);
  const Eigen::Vector3d& val = es.eigenvalues();
  Eigen::Map<Eigen::Vector3d>(&valsS[0], 3) = val; 
  check1 = term1 * (2.0 * mut * valsS[0] + term2);
  check2 = term1 * (2.0 * mut * valsS[1] + term2);
  check3 = term1 * (2.0 * mut * valsS[2] + term2);
  check1 = std::max({check1, check2});
  k = std::max({check1, check3}); //should alwys be check3
  k = kfactor * k;
}

//------------------------------------------------------------------------------

inline
void NavierStokesTerm::computeAnisotropicReynoldsStressTensorAlt(double rhocg, double mu, double lambda, double dudxj[3][3], double k, double a[3][3]) {

  double div = dudxj[0][0] + dudxj[1][1] + dudxj[2][2];
  double term1 = -1.0 / (2.0 * rhocg * k);
  a[0][0] = term1 * (lambda * div + 2.0 * mu * dudxj[0][0]);
  a[1][1] = term1 * (lambda * div + 2.0 * mu * dudxj[1][1]);
  a[2][2] = term1 * (lambda * div + 2.0 * mu * dudxj[2][2]);
  a[0][1] = term1 * (mu * (dudxj[1][0] + dudxj[0][1]));
  a[0][2] = term1 * (mu * (dudxj[2][0] + dudxj[0][2]));
  a[1][2] = term1 * (mu * (dudxj[2][1] + dudxj[1][2]));
  a[1][0] = a[0][1];
  a[2][0] = a[0][2];
  a[2][1] = a[1][2];
}

//------------------------------------------------------------------------------

inline
void NavierStokesTerm::computeAnisotropicReynoldsStressTensor(double rhocg, double mut, double lambdat, double dudxj[3][3], double RSM[3][3], double k, double a[3][3]) {
    computeTurbulentStressTensor(rhocg, mut, lambdat, dudxj, RSM, k);
    for (int i = 0; i < 3; i++){
      for (int j = 0; j < 3; j++){
        a[i][j] = 0.5 * RSM[i][j] / k;
      }
      a[i][i] -= 1.0 / 3.0;
    }
 }

//------------------------------------------------------------------------------

inline
void NavierStokesTerm::computeSpectralDecompTensor(double a[3][3], double vals[3], double vects[3][3]) {
    Eigen::Map<Eigen::Matrix3d> aEig(&a[0][0], 3, 3);
    Eigen::SelfAdjointEigenSolver<Eigen::Matrix3d> es(aEig);
    const Eigen::Vector3d& l = es.eigenvalues();
    Eigen::Map<Eigen::Vector3d>(&vals[0], 3) = l; 
    const Eigen::Matrix3d& V = es.eigenvectors();
    Eigen::Map<Eigen::Matrix<double,3,3,Eigen::RowMajor>>(&vects[0][0], 3, 3) = V;
}

//------------------------------------------------------------------------------

inline
void NavierStokesTerm::computeEigenvaluesTensor(double a[3][3], double vals[3]) {
    Eigen::Map<Eigen::Matrix3d> aEig(&a[0][0], 3, 3);
    Eigen::SelfAdjointEigenSolver<Eigen::Matrix3d> es(aEig);
    const Eigen::Vector3d& l = es.eigenvalues();
    Eigen::Map<Eigen::Vector3d>(&vals[0], 3) = l;
}


//------------------------------------------------------------------------------

template<int dim>
inline
void NavierStokesTerm::computeVolumeTermNSUQ(double rhocg, double nu, double mul, double lambdal, double kappa,
                                           double u[3], double dudxj[3][3], double dTdxj[3],
                                           double (*r)[dim], double RSM[3][3]) {
  double tij[3][3];
  computeStressTensor(mul, lambdal, dudxj, tij);
  double qj[3];
  computeHeatFluxVector(kappa, dTdxj, qj);
  r[0][0] = 0.0;
  r[0][1] = tij[0][0] - RSM[0][0] * rhocg;
  r[0][2] = tij[1][0] - RSM[1][0] * rhocg;
  r[0][3] = tij[2][0] - RSM[2][0] * rhocg;
  r[0][4] = nu * (u[0] * r[0][1] + u[1] * r[0][2] + u[2] * r[0][3]) - qj[0];
  r[1][0] = 0.0;
  r[1][1] = tij[0][1] - RSM[0][1] * rhocg;
  r[1][2] = tij[1][1] - RSM[1][1] * rhocg;
  r[1][3] = tij[2][1] - RSM[2][1] * rhocg;
  r[1][4] = nu * (u[0] * r[1][1] + u[1] * r[1][2] + u[2] * r[1][3]) - qj[1];
  r[2][0] = 0.0;
  r[2][1] = tij[0][2] - RSM[0][2] * rhocg;
  r[2][2] = tij[1][2] - RSM[1][2] * rhocg;
  r[2][3] = tij[2][2] - RSM[2][2] * rhocg;
  r[2][4] = nu * (u[0] * r[2][1] + u[1] * r[2][2] + u[2] * r[2][3]) - qj[2];
}

//------------------------------------------------------------------------------

template<int dim>
inline
void NavierStokesTerm::computeVolumeTermNS(double nu, double mu, double lambda, double kappa,
                                           double u[3], double dudxj[3][3], double dTdxj[3],
                                           double (*r)[dim]) {
  double tij[3][3];
  computeStressTensor(mu, lambda, dudxj, tij);
  double qj[3];
  computeHeatFluxVector(kappa, dTdxj, qj);
  r[0][0] = 0.0;
  r[0][1] = tij[0][0];
  r[0][2] = tij[1][0];
  r[0][3] = tij[2][0];
  r[0][4] = nu * (u[0] * tij[0][0] + u[1] * tij[1][0] + u[2] * tij[2][0]) - qj[0];
  r[1][0] = 0.0;
  r[1][1] = tij[0][1];
  r[1][2] = tij[1][1];
  r[1][3] = tij[2][1];
  r[1][4] = nu * (u[0] * tij[0][1] + u[1] * tij[1][1] + u[2] * tij[2][1]) - qj[1];
  r[2][0] = 0.0;
  r[2][1] = tij[0][2];
  r[2][2] = tij[1][2];
  r[2][3] = tij[2][2];
  r[2][4] = nu * (u[0] * tij[0][2] + u[1] * tij[1][2] + u[2] * tij[2][2]) - qj[2];
}

//------------------------------------------------------------------------------

template<int dim>
inline
void NavierStokesTerm::computeDerivativeOfVolumeTermNS(double nu, double dnu, double mu, double dmu, double lambda, double dlambda,
                                                       double kappa, double dkappa, double u[3], double du[3], double dudxj[3][3],
                                                       double ddudxj[3][3], double dTdxj[3], double ddTdxj[3], double (*dr)[dim]) {
  double tij[3][3];
  computeStressTensor(mu, lambda, dudxj, tij);
  double dtij[3][3];
  computeDerivativeOfStressTensor(mu, dmu, lambda, dlambda, dudxj, ddudxj, dtij);
  double dqj[3];
  computeDerivativeOfHeatFluxVector(kappa, dkappa, dTdxj, ddTdxj, dqj);
  dr[0][0] = 0.0;
  dr[0][1] = dtij[0][0];
  dr[0][2] = dtij[1][0];
  dr[0][3] = dtij[2][0];
  dr[0][4] = nu * (du[0] * tij[0][0] + u[0] * dtij[0][0] + du[1] * tij[1][0] + u[1] * dtij[1][0] + du[2] * tij[2][0] + u[2] * dtij[2][0])
           + dnu * (u[0] * tij[0][0] + u[1] * tij[1][0] + u[2] * tij[2][0]) - dqj[0];
  dr[1][0] = 0.0;
  dr[1][1] = dtij[0][1];
  dr[1][2] = dtij[1][1];
  dr[1][3] = dtij[2][1];
  dr[1][4] = nu * (du[0] * tij[0][1] + u[0] * dtij[0][1] + du[1] * tij[1][1] + u[1] * dtij[1][1] + du[2] * tij[2][1] + u[2] * dtij[2][1])
           + dnu * (u[0] * tij[0][1] + u[1] * tij[1][1] + u[2] * tij[2][1]) - dqj[1];
  dr[2][0] = 0.0;
  dr[2][1] = dtij[0][2];
  dr[2][2] = dtij[1][2];
  dr[2][3] = dtij[2][2];
  dr[2][4] = nu * (du[0] * tij[0][2] + u[0] * dtij[0][2] + du[1] * tij[1][2] + u[1] * dtij[1][2] + du[2] * tij[2][2] + u[2] * dtij[2][2])
           + dnu * (u[0] * tij[0][2] + u[1] * tij[1][2] + u[2] * tij[2][2]) - dqj[2];
}

//------------------------------------------------------------------------------

template<int dim>
inline
void NavierStokesTerm::computeDerivativeOperatorsOfVolumeTermNS(double nu, double mu, double lambda, double kappa,
                                                                double u[3], double dudxj[3][3], double dTdxj[3],
                                                                double drdnu[3][dim], double drdmu[3][dim],
                                                                double drdlambda[3][dim], double drdkappa[3][dim],
                                                                double drddudxj[3][dim][3][3], double drddTdxj[3][dim][3]) {
  double tij[3][3];
  computeStressTensor(mu, lambda, dudxj, tij);
  double dtijdlambda[3][3] = {0}, dtijddudxj[3][3][3][3] = {0}, dtijdmu[3][3] = {0};
  computeDerivativeOperatorsOfStressTensor(mu, lambda, dudxj, dtijddudxj, dtijdmu, dtijdlambda);
  double dqjdkappa[3] = {0}, dqjddTdxj[3][3] = {0};
  computeDerivativeOperatorsOfHeatFluxVector(kappa, dTdxj, dqjddTdxj, dqjdkappa);
  double drdtij[3][dim][3][3] = {0};
  drdtij[0][3][2][0] = drdtij[0][2][1][0] = drdtij[0][1][0][0] = 1.0;
  drdtij[1][3][2][1] = drdtij[1][2][1][1] = drdtij[1][1][0][1] = 1.0;
  drdtij[2][3][2][2] = drdtij[2][2][1][2] = drdtij[2][1][0][2] = 1.0;
  drdtij[0][4][0][0] = drdtij[1][4][0][1] = drdtij[2][4][0][2] = nu * u[0];
  drdtij[0][4][1][0] = drdtij[1][4][1][1] = drdtij[2][4][1][2] = nu * u[1];
  drdtij[0][4][2][0] = drdtij[1][4][2][1] = drdtij[2][4][2][2] = nu * u[2];
  double drdqj[3][dim][3] = {0};
  drdqj[2][4][2] = drdqj[1][4][1] = drdqj[0][4][0] = -1.0;
  drdnu[0][4] = u[0] * tij[0][0] + u[1] * tij[1][0] + u[2] * tij[2][0];
  drdnu[1][4] = u[0] * tij[0][1] + u[1] * tij[1][1] + u[2] * tij[2][1];
  drdnu[2][4] = u[0] * tij[0][2] + u[1] * tij[1][2] + u[2] * tij[2][2];
  for(int i = 0; i < 3; ++i)
    for(int j = 0; j < dim; ++j)
      for(int k = 0; k < 3; ++k) {
        drdkappa[i][j] += drdqj[i][j][k] * dqjdkappa[k];
        for(int l = 0; l < 3; ++l) {
          drddTdxj[i][j][l] += drdqj[i][j][k] * dqjddTdxj[k][l];
          drdmu[i][j] += drdtij[i][j][k][l] * dtijdmu[k][l];
          drdlambda[i][j] += drdtij[i][j][k][l] * dtijdlambda[k][l];
          for(int m = 0; m < 3; ++m)
            for(int n = 0; n < 3; ++n) {
              drddudxj[i][j][m][n] += drdtij[i][j][k][l] * dtijddudxj[k][l][m][n];
            }
        }
      }
}

//------------------------------------------------------------------------------

template<int neq>
inline
void NavierStokesTerm::computeJacobianVolumeTermNS(double dp1dxj[4][3], double mu, double lambda, double kappa,
                                                   double *V[4], double ucg[3], double dudxj[3][3], int tag,
                                                   double (*dRdU)[3][neq][neq]) {
  // for nu = 1, mu = constant, lambda = constant, kappa = constant
  double tij[3][3], dTduk[7]; // TODO: use dim for dTduk
  computeStressTensor(mu, lambda, dudxj, tij);
  double& txx = tij[0][0];
  double& tyy = tij[1][1];
  double& tzz = tij[2][2];
  double& txy = tij[0][1];
  double& txz = tij[0][2];
  double& tyz = tij[1][2];
  for(int k = 0; k < 4; ++k) {
    double& rho = V[k][0];
    double& u = V[k][1];
    double& v = V[k][2];
    double& w = V[k][3];
    double& dp1dx = dp1dxj[k][0];
    double& dp1dy = dp1dxj[k][1];
    double& dp1dz = dp1dxj[k][2];
    double nu = mu / rho;
    double lu = lambda / rho;
    double dtxxdu0 = -(2.0 * nu * dp1dx * u + lu * (dp1dx * u + dp1dy * v + dp1dz * w));
    double dtxxdu1 = (2.0 * nu + lu) * dp1dx;
    double dtxxdu2 = lu * dp1dy;
    double dtxxdu3 = lu * dp1dz;
    double dtyydu0 = -(2.0 * nu * dp1dy * v + lu * (dp1dx * u + dp1dy * v + dp1dz * w));
    double dtyydu1 = lu * dp1dx;
    double dtyydu2 = (2.0 * nu + lu) * dp1dy;
    double dtyydu3 = lu * dp1dz;
    double dtzzdu0 = -(2.0 * nu * dp1dz * w + lu * (dp1dx * u + dp1dy * v + dp1dz * w));
    double dtzzdu1 = lu * dp1dx;
    double dtzzdu2 = lu * dp1dy;
    double dtzzdu3 = (2.0 * nu + lu) * dp1dz;
    double dtxydu0 = -nu * (dp1dx * v + dp1dy * u);
    double dtxydu1 = nu * dp1dy;
    double dtxydu2 = nu * dp1dx;
    double dtxzdu0 = -nu * (dp1dx * w + dp1dz * u);
    double dtxzdu1 = nu * dp1dz;
    double dtxzdu3 = nu * dp1dx;
    double dtyzdu0 = -nu * (dp1dy * w + dp1dz * v);
    double dtyzdu2 = nu * dp1dz;
    double dtyzdu3 = nu * dp1dy;
    varFcn->computeJacobianTemperature(V[k], dTduk, tag);
    double c0 = -kappa * dTduk[0];
    double c1 = -kappa * dTduk[1];
    double c2 = -kappa * dTduk[2];
    double c3 = -kappa * dTduk[3];
    double c4 = -kappa * dTduk[4];
    double dqxdu0 = c0 * dp1dx;
    double dqxdu1 = c1 * dp1dx;
    double dqxdu2 = c2 * dp1dx;
    double dqxdu3 = c3 * dp1dx;
    double dqxdu4 = c4 * dp1dx;
    double dqydu0 = c0 * dp1dy;
    double dqydu1 = c1 * dp1dy;
    double dqydu2 = c2 * dp1dy;
    double dqydu3 = c3 * dp1dy;
    double dqydu4 = c4 * dp1dy;
    double dqzdu0 = c0 * dp1dz;
    double dqzdu1 = c1 * dp1dz;
    double dqzdu2 = c2 * dp1dz;
    double dqzdu3 = c3 * dp1dz;
    double dqzdu4 = c4 * dp1dz;
    double beta = fourth / rho;
    double dudu0 = -beta * u;
    double dudu1 = beta;
    double dvdu0 = -beta * v;
    double dvdu2 = beta;
    double dwdu0 = -beta * w;
    double dwdu3 = beta;
    // computation of dRdU -> dRdU[k][a][b][c]
    // k is the corner of the tetrahedra we are considering
    // a is the coordinate direction: x y z
    // b is the equation we are considering: 0 for mass conservation
    //           1 for x-momentum
    //           2 for y-momentum
    //           3 for z-momentum
    //           4 for energy
    // c is the variable wrt which we derive
    // dRxdU
    dRdU[k][0][0][0] = 0.0;
    dRdU[k][0][0][1] = 0.0;
    dRdU[k][0][0][2] = 0.0;
    dRdU[k][0][0][3] = 0.0;
    dRdU[k][0][0][4] = 0.0;
    dRdU[k][0][1][0] = dtxxdu0;
    dRdU[k][0][1][1] = dtxxdu1;
    dRdU[k][0][1][2] = dtxxdu2;
    dRdU[k][0][1][3] = dtxxdu3;
    dRdU[k][0][1][4] = 0.0;
    dRdU[k][0][2][0] = dtxydu0;
    dRdU[k][0][2][1] = dtxydu1;
    dRdU[k][0][2][2] = dtxydu2;
    dRdU[k][0][2][3] = 0.0;
    dRdU[k][0][2][4] = 0.0;
    dRdU[k][0][3][0] = dtxzdu0;
    dRdU[k][0][3][1] = dtxzdu1;
    dRdU[k][0][3][2] = 0.0;
    dRdU[k][0][3][3] = dtxzdu3;
    dRdU[k][0][3][4] = 0.0;
    dRdU[k][0][4][0] = dudu0 * txx + ucg[0] * dtxxdu0 + dvdu0 * txy + ucg[1] * dtxydu0 +
                       dwdu0 * txz + ucg[2] * dtxzdu0 - dqxdu0;
    dRdU[k][0][4][1] = dudu1 * txx + ucg[0] * dtxxdu1 +
                       ucg[1] * dtxydu1 + ucg[2] * dtxzdu1 - dqxdu1;
    dRdU[k][0][4][2] = ucg[0] * dtxxdu2 + dvdu2 * txy + ucg[1] * dtxydu2 - dqxdu2;
    dRdU[k][0][4][3] = ucg[0] * dtxxdu3 + dwdu3 * txz + ucg[2] * dtxzdu3 - dqxdu3;
    dRdU[k][0][4][4] = -dqxdu4;
    // dRydU
    dRdU[k][1][0][0] = 0.0;
    dRdU[k][1][0][1] = 0.0;
    dRdU[k][1][0][2] = 0.0;
    dRdU[k][1][0][3] = 0.0;
    dRdU[k][1][0][4] = 0.0;
    dRdU[k][1][1][0] = dtxydu0;
    dRdU[k][1][1][1] = dtxydu1;
    dRdU[k][1][1][2] = dtxydu2;
    dRdU[k][1][1][3] = 0.0;
    dRdU[k][1][1][4] = 0.0;
    dRdU[k][1][2][0] = dtyydu0;
    dRdU[k][1][2][1] = dtyydu1;
    dRdU[k][1][2][2] = dtyydu2;
    dRdU[k][1][2][3] = dtyydu3;
    dRdU[k][1][2][4] = 0.0;
    dRdU[k][1][3][0] = dtyzdu0;
    dRdU[k][1][3][1] = 0.0;
    dRdU[k][1][3][2] = dtyzdu2;
    dRdU[k][1][3][3] = dtyzdu3;
    dRdU[k][1][3][4] = 0.0;
    dRdU[k][1][4][0] = dudu0 * txy + ucg[0] * dtxydu0 + dvdu0 * tyy + ucg[1] * dtyydu0 +
                       dwdu0 * tyz + ucg[2] * dtyzdu0 - dqydu0;
    dRdU[k][1][4][1] = dudu1 * txy + ucg[0] * dtxydu1 + ucg[1] * dtyydu1 - dqydu1;
    dRdU[k][1][4][2] = ucg[0] * dtxydu2 + dvdu2 * tyy +
                       ucg[1] * dtyydu2 + ucg[2] * dtyzdu2 - dqydu2;
    dRdU[k][1][4][3] = ucg[1] * dtyydu3 + dwdu3 * tyz + ucg[2] * dtyzdu3 - dqydu3;
    dRdU[k][1][4][4] = -dqydu4;
    // dRzdU
    dRdU[k][2][0][0] = 0.0;
    dRdU[k][2][0][1] = 0.0;
    dRdU[k][2][0][2] = 0.0;
    dRdU[k][2][0][3] = 0.0;
    dRdU[k][2][0][4] = 0.0;
    dRdU[k][2][1][0] = dtxzdu0;
    dRdU[k][2][1][1] = dtxzdu1;
    dRdU[k][2][1][2] = 0.0;
    dRdU[k][2][1][3] = dtxzdu3;
    dRdU[k][2][1][4] = 0.0;
    dRdU[k][2][2][0] = dtyzdu0;
    dRdU[k][2][2][1] = 0.0;
    dRdU[k][2][2][2] = dtyzdu2;
    dRdU[k][2][2][3] = dtyzdu3;
    dRdU[k][2][2][4] = 0.0;
    dRdU[k][2][3][0] = dtzzdu0;
    dRdU[k][2][3][1] = dtzzdu1;
    dRdU[k][2][3][2] = dtzzdu2;
    dRdU[k][2][3][3] = dtzzdu3;
    dRdU[k][2][3][4] = 0.0;
    dRdU[k][2][4][0] = dudu0 * txz + ucg[0] * dtxzdu0 + dvdu0 * tyz + ucg[1] * dtyzdu0 +
                       dwdu0 * tzz + ucg[2] * dtzzdu0 - dqzdu0;
    dRdU[k][2][4][1] = dudu1 * txz + ucg[0] * dtxzdu1 + ucg[2] * dtzzdu1 - dqzdu1;
    dRdU[k][2][4][2] = dvdu2 * tyz + ucg[1] * dtyzdu2 + ucg[2] * dtzzdu2 - dqzdu2;
    dRdU[k][2][4][3] = ucg[0] * dtxzdu3 + ucg[1] * dtyzdu3 +
                       dwdu3 * tzz + ucg[2] * dtzzdu3 - dqzdu3;
    dRdU[k][2][4][4] = -dqzdu4;
  }
}

//------------------------------------------------------------------------------

template<int neq>
inline
void NavierStokesTerm::computeJacobianVolumeTermNS(double dp1dxj[4][3], double mu, double dmu[4][neq],
                                                   double lambda, double dlambda[4][neq], double kappa, double dkappa[4][neq],
                                                   double *V[4], double ucg[3], double dudxj[3][3],
                                                   int tag, double dTdxj[3], double (*dRdU)[3][neq][neq]) {
  // for nu = 1
  double tij[3][3];
  computeStressTensor(mu, lambda, dudxj, tij);
  double dtijdu0[3][3];
  double dtijdu1[3][3];
  double dtijdu2[3][3];
  double dtijdu3[3][3];
  double dtijdu4[3][3];
  double ducgdu0[3];
  double ducgdu1[3];
  double ducgdu2[3];
  double ducgdu3[3];
  double ducgdu4[3];
  double dTduk[7]; // TODO: use dim for dTduk
  double dTdu0kappa[4];
  double dTdu1kappa[4];
  double dTdu2kappa[4];
  double dTdu3kappa[4];
  double dTdu4kappa[4];
  double dqjdu0[3];
  double dqjdu1[3];
  double dqjdu2[3];
  double dqjdu3[3];
  double dqjdu4[3];

  const double toto = dudxj[0][0] + dudxj[1][1] + dudxj[2][2];

  for(int k = 0; k < 4; ++k) {
    const double invrho = 1. / V[k][0];
    const double mudivrho = mu * invrho;
    const double lambdadivrho = lambda * invrho;
    const double kappadivrho = kappa * invrho;

    ducgdu0[0] = -fourth * V[k][1] * invrho;
    ducgdu0[1] = -fourth * V[k][2] * invrho;
    ducgdu0[2] = -fourth * V[k][3] * invrho;
    ducgdu1[0] = fourth * invrho;
    ducgdu2[1] = fourth * invrho;
    ducgdu3[2] = fourth * invrho;

    dtijdu0[0][0] = 2.0 * dmu[k][0] * dudxj[0][0] - 2.0 * mudivrho * dp1dxj[k][0] * V[k][1] + dlambda[k][0] *
                    toto - lambdadivrho * (dp1dxj[k][0] * V[k][1] + dp1dxj[k][1] * V[k][2] + dp1dxj[k][2] * V[k][3]);
    dtijdu0[0][1] = -mudivrho * (dp1dxj[k][0] * V[k][2] + dp1dxj[k][1] * V[k][1]) + dmu[k][0] * (dudxj[1][0] + dudxj[0][1]);
    dtijdu0[0][2] = -mudivrho * (dp1dxj[k][0] * V[k][3] + dp1dxj[k][2] * V[k][1]) + dmu[k][0] * (dudxj[2][0] + dudxj[0][2]);
    dtijdu0[1][0] = dtijdu0[0][1];
    dtijdu0[1][1] = 2.0 * dmu[k][0] * dudxj[1][1] - 2.0 * mudivrho * dp1dxj[k][1] * V[k][2] + dlambda[k][0] *
                    toto - lambdadivrho * (dp1dxj[k][1] * V[k][2] + dp1dxj[k][0] * V[k][1] + dp1dxj[k][2] * V[k][3]);
    dtijdu0[1][2] = -mudivrho * (dp1dxj[k][1] * V[k][3] + dp1dxj[k][2] * V[k][2]) + dmu[k][0] * (dudxj[2][1] + dudxj[1][2]);
    dtijdu0[2][0] = dtijdu0[0][2];
    dtijdu0[2][1] = dtijdu0[1][2];
    dtijdu0[2][2] = 2.0 * dmu[k][0] * dudxj[2][2] - 2.0 * mudivrho * dp1dxj[k][2] * V[k][3] + dlambda[k][0] *
                    (toto) - lambdadivrho * (dp1dxj[k][2] * V[k][3] + dp1dxj[k][0] * V[k][1] + dp1dxj[k][1] * V[k][2]);

    dtijdu1[0][0] = 2.0 * dmu[k][1] * dudxj[0][0] + 2.0 * mudivrho * dp1dxj[k][0] + dlambda[k][1] * toto + lambdadivrho * dp1dxj[k][0];
    dtijdu1[0][1] = mudivrho * dp1dxj[k][1] + dmu[k][1] * (dudxj[1][0] + dudxj[0][1]);
    dtijdu1[0][2] = mudivrho * dp1dxj[k][2] + dmu[k][1] * (dudxj[2][0] + dudxj[0][2]);
    dtijdu1[1][0] = dtijdu1[0][1];
    dtijdu1[1][1] = 2.0 * dmu[k][1] * dudxj[1][1] + dlambda[k][1] * toto + lambdadivrho * dp1dxj[k][0];
    dtijdu1[1][2] = dmu[k][1] * (dudxj[2][1] + dudxj[1][2]);
    dtijdu1[2][0] = dtijdu1[0][2];
    dtijdu1[2][1] = dtijdu1[1][2];
    dtijdu1[2][2] = 2.0 * dmu[k][1] * dudxj[2][2] + dlambda[k][1] * toto + lambdadivrho * dp1dxj[k][0];

    dtijdu2[0][0] = 2.0 * dmu[k][2] * dudxj[0][0] + dlambda[k][2] * toto + lambdadivrho * dp1dxj[k][1];
    dtijdu2[0][1] = mudivrho * dp1dxj[k][0] + dmu[k][2] * (dudxj[1][0] + dudxj[0][1]);
    dtijdu2[0][2] = dmu[k][2] * (dudxj[2][0] + dudxj[0][2]);
    dtijdu2[1][0] = dtijdu2[0][1];
    dtijdu2[1][1] = 2.0 * dmu[k][2] * dudxj[1][1] + 2.0 * mudivrho * dp1dxj[k][1] + dlambda[k][2] * toto + lambdadivrho
                    * dp1dxj[k][1];
    dtijdu2[1][2] = mudivrho * dp1dxj[k][2] + dmu[k][2] * (dudxj[2][1] + dudxj[1][2]);
    dtijdu2[2][0] = dtijdu2[0][2];
    dtijdu2[2][1] = dtijdu2[1][2];
    dtijdu2[2][2] = 2.0 * dmu[k][2] * dudxj[2][2] + dlambda[k][2] * toto + lambdadivrho * dp1dxj[k][1];

    dtijdu3[0][0] = 2.0 * dmu[k][3] * dudxj[0][0] + dlambda[k][3] * toto + lambdadivrho * dp1dxj[k][2];
    dtijdu3[0][1] = dmu[k][3] * (dudxj[1][0] + dudxj[0][1]);
    dtijdu3[0][2] = mudivrho * dp1dxj[k][0] + dmu[k][3] * (dudxj[2][0] + dudxj[0][2]);
    dtijdu3[1][0] = dtijdu3[0][1];
    dtijdu3[1][1] = 2.0 * dmu[k][3] * dudxj[1][1] + dlambda[k][3] * toto + lambdadivrho * dp1dxj[k][2];
    dtijdu3[1][2] = mudivrho * dp1dxj[k][1] + dmu[k][3] * (dudxj[2][1] + dudxj[1][2]);
    dtijdu3[2][0] = dtijdu3[0][2];
    dtijdu3[2][1] = dtijdu3[1][2];
    dtijdu3[2][2] = 2.0 * dmu[k][3] * dudxj[2][2] + 2.0 * mudivrho * dp1dxj[k][2] + dlambda[k][3] * toto + lambdadivrho * dp1dxj[k][2];

    dtijdu4[0][0] = 2.0 * dmu[k][4] * dudxj[0][0] + dlambda[k][4] * toto;
    dtijdu4[0][1] = dmu[k][4] * (dudxj[1][0] + dudxj[0][1]);
    dtijdu4[0][2] = dmu[k][4] * (dudxj[2][0] + dudxj[0][2]);
    dtijdu4[1][0] = dtijdu4[0][1];
    dtijdu4[1][1] = 2.0 * dmu[k][4] * dudxj[1][1] + dlambda[k][4] * toto;
    dtijdu4[1][2] = dmu[k][4] * (dudxj[2][1] + dudxj[1][2]);
    dtijdu4[2][0] = dtijdu4[0][2];
    dtijdu4[2][1] = dtijdu4[1][2];
    dtijdu4[2][2] = 2.0 * dmu[k][4] * dudxj[2][2] + dlambda[k][4] * toto;

    varFcn->computeJacobianTemperature(V[k], dTduk, tag);
    dTdu0kappa[k] = kappa * dTduk[0];
    dTdu1kappa[k] = kappa * dTduk[1];
    dTdu2kappa[k] = kappa * dTduk[2];
    dTdu3kappa[k] = kappa * dTduk[3];
    dTdu4kappa[k] = kappa * dTduk[4];
    dqjdu0[0] = -dkappa[k][0] * dTdxj[0] - dp1dxj[k][0] * dTdu0kappa[k];
    dqjdu1[0] = -dkappa[k][1] * dTdxj[0] - dp1dxj[k][0] * dTdu1kappa[k];
    dqjdu2[0] = -dkappa[k][2] * dTdxj[0] - dp1dxj[k][0] * dTdu2kappa[k];
    dqjdu3[0] = -dkappa[k][3] * dTdxj[0] - dp1dxj[k][0] * dTdu3kappa[k];
    dqjdu4[0] = -dkappa[k][4] * dTdxj[0] - dp1dxj[k][0] * dTdu4kappa[k];
    dqjdu0[1] = -dkappa[k][0] * dTdxj[1] - dp1dxj[k][1] * dTdu0kappa[k];
    dqjdu1[1] = -dkappa[k][1] * dTdxj[1] - dp1dxj[k][1] * dTdu1kappa[k];
    dqjdu2[1] = -dkappa[k][2] * dTdxj[1] - dp1dxj[k][1] * dTdu2kappa[k];
    dqjdu3[1] = -dkappa[k][3] * dTdxj[1] - dp1dxj[k][1] * dTdu3kappa[k];
    dqjdu4[1] = -dkappa[k][4] * dTdxj[1] - dp1dxj[k][1] * dTdu4kappa[k];
    dqjdu0[2] = -dkappa[k][0] * dTdxj[2] - dp1dxj[k][2] * dTdu0kappa[k];
    dqjdu1[2] = -dkappa[k][1] * dTdxj[2] - dp1dxj[k][2] * dTdu1kappa[k];
    dqjdu2[2] = -dkappa[k][2] * dTdxj[2] - dp1dxj[k][2] * dTdu2kappa[k];
    dqjdu3[2] = -dkappa[k][3] * dTdxj[2] - dp1dxj[k][2] * dTdu3kappa[k];
    dqjdu4[2] = -dkappa[k][4] * dTdxj[2] - dp1dxj[k][2] * dTdu4kappa[k];
    // dRxdU
    dRdU[k][0][0][0] = 0.0;
    dRdU[k][0][0][1] = 0.0;
    dRdU[k][0][0][2] = 0.0;
    dRdU[k][0][0][3] = 0.0;
    dRdU[k][0][0][4] = 0.0;
    dRdU[k][0][1][0] = dtijdu0[0][0];
    dRdU[k][0][1][1] = dtijdu1[0][0];
    dRdU[k][0][1][2] = dtijdu2[0][0];
    dRdU[k][0][1][3] = dtijdu3[0][0];
    dRdU[k][0][1][4] = dtijdu4[0][0];
    dRdU[k][0][2][0] = dtijdu0[0][1];
    dRdU[k][0][2][1] = dtijdu1[0][1];
    dRdU[k][0][2][2] = dtijdu2[0][1];
    dRdU[k][0][2][3] = dtijdu3[0][1];
    dRdU[k][0][2][4] = dtijdu4[0][1];
    dRdU[k][0][3][0] = dtijdu0[0][2];
    dRdU[k][0][3][1] = dtijdu1[0][2];
    dRdU[k][0][3][2] = dtijdu2[0][2];
    dRdU[k][0][3][3] = dtijdu3[0][2];
    dRdU[k][0][3][4] = dtijdu4[0][2];
    dRdU[k][0][4][0] = ducgdu0[0] * tij[0][0] + ucg[0] * dtijdu0[0][0] + ducgdu0[1] * tij[0][1] + ucg[1] * dtijdu0[0][1] + ducgdu0[2] * tij[0][2] + ucg[2]
                       * dtijdu0[0][2] - dqjdu0[0];
    dRdU[k][0][4][1] = ducgdu1[0] * tij[0][0] + ucg[0] * dtijdu1[0][0] + ucg[1] * dtijdu1[0][1] + ucg[2] * dtijdu1[0][2] - dqjdu1[0];
    dRdU[k][0][4][2] = ucg[0] * dtijdu2[0][0] + ducgdu2[1] * tij[0][1] + ucg[1] * dtijdu2[0][1] + ucg[2] * dtijdu2[0][2] - dqjdu2[0];
    dRdU[k][0][4][3] = ucg[0] * dtijdu3[0][0] + ucg[1] * dtijdu3[0][1] + ducgdu3[2] * tij[0][2] + ucg[2] * dtijdu3[0][2] - dqjdu3[0];
    dRdU[k][0][4][4] = ucg[0] * dtijdu4[0][0] + ucg[1] * dtijdu4[0][1] + ucg[2] * dtijdu4[0][2] - dqjdu4[0];
    // dRydU
    dRdU[k][1][0][0] = 0.0;
    dRdU[k][1][0][1] = 0.0;
    dRdU[k][1][0][2] = 0.0;
    dRdU[k][1][0][3] = 0.0;
    dRdU[k][1][0][4] = 0.0;
    dRdU[k][1][1][0] = dtijdu0[0][1];
    dRdU[k][1][1][1] = dtijdu1[0][1];
    dRdU[k][1][1][2] = dtijdu2[0][1];
    dRdU[k][1][1][3] = dtijdu3[0][1];
    dRdU[k][1][1][4] = dtijdu4[0][1];
    dRdU[k][1][2][0] = dtijdu0[1][1];
    dRdU[k][1][2][1] = dtijdu1[1][1];
    dRdU[k][1][2][2] = dtijdu2[1][1];
    dRdU[k][1][2][3] = dtijdu3[1][1];
    dRdU[k][1][2][4] = dtijdu4[1][1];
    dRdU[k][1][3][0] = dtijdu0[1][2];
    dRdU[k][1][3][1] = dtijdu1[1][2];
    dRdU[k][1][3][2] = dtijdu2[1][2];
    dRdU[k][1][3][3] = dtijdu3[1][2];
    dRdU[k][1][3][4] = dtijdu4[1][2];
    dRdU[k][1][4][0] = ducgdu0[0] * tij[0][1] + ucg[0] * dtijdu0[0][1] + ducgdu0[1] * tij[1][1] + ucg[1] * dtijdu0[1][1] + ducgdu0[2] * tij[1][2] + ucg[2]
                       * dtijdu0[1][2] - dqjdu0[1];
    dRdU[k][1][4][1] = ducgdu1[0] * tij[0][1] + ucg[0] * dtijdu1[0][1] + ucg[1] * dtijdu1[1][1] + ucg[2] * dtijdu1[1][2] - dqjdu1[1];
    dRdU[k][1][4][2] = ucg[0] * dtijdu2[0][1] + ducgdu2[1] * tij[1][1] + ucg[1] * dtijdu2[1][1] + ucg[2] * dtijdu2[1][2] - dqjdu2[1];
    dRdU[k][1][4][3] = ucg[0] * dtijdu3[0][1] + ucg[1] * dtijdu3[1][1] + ducgdu3[2] * tij[1][2] + ucg[2] * dtijdu3[1][2] - dqjdu3[1];
    dRdU[k][1][4][4] = ucg[0] * dtijdu4[0][1] + ucg[1] * dtijdu4[1][1] + ucg[2] * dtijdu4[1][2] - dqjdu4[1];
    // dRzdU
    dRdU[k][2][0][0] = 0.0;
    dRdU[k][2][0][1] = 0.0;
    dRdU[k][2][0][2] = 0.0;
    dRdU[k][2][0][3] = 0.0;
    dRdU[k][2][0][4] = 0.0;
    dRdU[k][2][1][0] = dtijdu0[0][2];
    dRdU[k][2][1][1] = dtijdu1[0][2];
    dRdU[k][2][1][2] = dtijdu2[0][2];
    dRdU[k][2][1][3] = dtijdu3[0][2];
    dRdU[k][2][1][4] = dtijdu4[0][2];
    dRdU[k][2][2][0] = dtijdu0[1][2];
    dRdU[k][2][2][1] = dtijdu1[1][2];
    dRdU[k][2][2][2] = dtijdu2[1][2];
    dRdU[k][2][2][3] = dtijdu3[1][2];
    dRdU[k][2][2][4] = dtijdu4[1][2];
    dRdU[k][2][3][0] = dtijdu0[2][2];
    dRdU[k][2][3][1] = dtijdu1[2][2];
    dRdU[k][2][3][2] = dtijdu2[2][2];
    dRdU[k][2][3][3] = dtijdu3[2][2];
    dRdU[k][2][3][4] = dtijdu4[2][2];
    dRdU[k][2][4][0] = ducgdu0[0] * tij[0][2] + ucg[0] * dtijdu0[0][2] + ducgdu0[1] * tij[1][2] + ucg[1] * dtijdu0[1][2] + ducgdu0[2] * tij[2][2] + ucg[2]
                       * dtijdu0[2][2] - dqjdu0[2];
    dRdU[k][2][4][1] = ducgdu1[0] * tij[0][2] + ucg[0] * dtijdu1[0][2] + ucg[1] * dtijdu1[1][2] + ucg[2] * dtijdu1[2][2] - dqjdu1[2];
    dRdU[k][2][4][2] = ucg[0] * dtijdu2[0][2] + ducgdu2[1] * tij[1][2] + ucg[1] * dtijdu2[1][2] + ucg[2] * dtijdu2[2][2] - dqjdu2[2];
    dRdU[k][2][4][3] = ucg[0] * dtijdu3[0][2] + ucg[1] * dtijdu3[1][2] + ducgdu3[2] * tij[2][2] + ucg[2] * dtijdu3[2][2] - dqjdu3[2];
    dRdU[k][2][4][4] = ucg[0] * dtijdu4[0][2] + ucg[1] * dtijdu4[1][2] + ucg[2] * dtijdu4[2][2] - dqjdu4[2];
  }
}

//------------------------------------------------------------------------------
// override for strongly coupled SA (neq = dim = 6)
template<>
inline
void NavierStokesTerm::computeJacobianVolumeTermNS(double dp1dxj[4][3], double mu, double dmu[4][6],
                                                   double lambda, double dlambda[4][6], double kappa, double dkappa[4][6],
                                                   double *V[4], double ucg[3], double dudxj[3][3],
                                                   int tag, double dTdxj[3], double (*dRdU)[3][6][6]) {
  double tij[3][3];
  computeStressTensor(mu, lambda, dudxj, tij);
  double dtijdu0[3][3];
  double dtijdu1[3][3];
  double dtijdu2[3][3];
  double dtijdu3[3][3];
  double dtijdu4[3][3];
  double dtijdu5[3][3];
  double ducgdu0[3];
  double ducgdu1[3];
  double ducgdu2[3];
  double ducgdu3[3];
  double ducgdu4[3];
  double ducgdu5[3];
  double dTduk[6];
  double dTdu0[4];
  double dTdu1[4];
  double dTdu2[4];
  double dTdu3[4];
  double dTdu4[4];
  double dTdu5[4];
  double ddTdxjdu0[3];
  double ddTdxjdu1[3];
  double ddTdxjdu2[3];
  double ddTdxjdu3[3];
  double ddTdxjdu4[3];
  double ddTdxjdu5[3];
  double dqjdu0[3];
  double dqjdu1[3];
  double dqjdu2[3];
  double dqjdu3[3];
  double dqjdu4[3];
  double dqjdu5[3];
  for(int k = 0; k < 4; ++k) {
    ducgdu0[0] = -fourth * V[k][1] / V[k][0];
    ducgdu1[0] = fourth / V[k][0];
    ducgdu2[0] = 0.0;
    ducgdu3[0] = 0.0;
    ducgdu4[0] = 0.0;
    ducgdu5[0] = 0.0;
    ducgdu0[1] = -fourth * V[k][2] / V[k][0];
    ducgdu1[1] = 0.0;
    ducgdu2[1] = fourth / V[k][0];
    ducgdu3[1] = 0.0;
    ducgdu4[1] = 0.0;
    ducgdu5[1] = 0.0;
    ducgdu0[2] = -fourth * V[k][3] / V[k][0];
    ducgdu1[2] = 0.0;
    ducgdu2[2] = 0.0;
    ducgdu3[2] = fourth / V[k][0];
    ducgdu4[2] = 0.0;
    ducgdu5[2] = 0.0;
    dtijdu0[0][0] = 2.0 * dmu[k][0] * dudxj[0][0] - 2.0 * mu * dp1dxj[k][0] * V[k][1] / V[k][0] + dlambda[k][0] *
                    (dudxj[0][0] + dudxj[1][1] + dudxj[2][2]) - lambda * (dp1dxj[k][0] * V[k][1] + dp1dxj[k][1] * V[k][2] + dp1dxj[k][2] * V[k][3]) / V[k][0];
    dtijdu1[0][0] = 2.0 * dmu[k][1] * dudxj[0][0] + 2.0 * mu * dp1dxj[k][0] / V[k][0] + dlambda[k][1] * (dudxj[0][0] + dudxj[1][1] + dudxj[2][2]) + lambda
                    * dp1dxj[k][0] / V[k][0];
    dtijdu2[0][0] = 2.0 * dmu[k][2] * dudxj[0][0] + dlambda[k][2] * (dudxj[0][0] + dudxj[1][1] + dudxj[2][2]) + lambda * dp1dxj[k][1] / V[k][0];
    dtijdu3[0][0] = 2.0 * dmu[k][3] * dudxj[0][0] + dlambda[k][3] * (dudxj[0][0] + dudxj[1][1] + dudxj[2][2]) + lambda * dp1dxj[k][2] / V[k][0];
    dtijdu4[0][0] = 2.0 * dmu[k][4] * dudxj[0][0] + dlambda[k][4] * (dudxj[0][0] + dudxj[1][1] + dudxj[2][2]);
    dtijdu5[0][0] = 2.0 * dmu[k][5] * dudxj[0][0] + dlambda[k][5] * (dudxj[0][0] + dudxj[1][1] + dudxj[2][2]);
    dtijdu0[1][1] = 2.0 * dmu[k][0] * dudxj[1][1] - 2.0 * mu * dp1dxj[k][1] * V[k][2] / V[k][0] + dlambda[k][0] *
                    (dudxj[0][0] + dudxj[1][1] + dudxj[2][2]) - lambda * (dp1dxj[k][1] * V[k][2] + dp1dxj[k][0] * V[k][1] + dp1dxj[k][2] * V[k][3]) / V[k][0];
    dtijdu1[1][1] = 2.0 * dmu[k][1] * dudxj[1][1] + dlambda[k][1] * (dudxj[0][0] + dudxj[1][1] + dudxj[2][2]) + lambda * dp1dxj[k][0] / V[k][0];
    dtijdu2[1][1] = 2.0 * dmu[k][2] * dudxj[1][1] + 2.0 * mu * dp1dxj[k][1] / V[k][0] + dlambda[k][2] * (dudxj[0][0] + dudxj[1][1] + dudxj[2][2]) + lambda
                    * dp1dxj[k][1] / V[k][0];
    dtijdu3[1][1] = 2.0 * dmu[k][3] * dudxj[1][1] + dlambda[k][3] * (dudxj[0][0] + dudxj[1][1] + dudxj[2][2]) + lambda * dp1dxj[k][2] / V[k][0];
    dtijdu4[1][1] = 2.0 * dmu[k][4] * dudxj[1][1] + dlambda[k][4] * (dudxj[0][0] + dudxj[1][1] + dudxj[2][2]);
    dtijdu5[1][1] = 2.0 * dmu[k][5] * dudxj[1][1] + dlambda[k][5] * (dudxj[0][0] + dudxj[1][1] + dudxj[2][2]);
    dtijdu0[2][2] = 2.0 * dmu[k][0] * dudxj[2][2] - 2.0 * mu * dp1dxj[k][2] * V[k][3] / V[k][0] + dlambda[k][0] *
                    (dudxj[0][0] + dudxj[1][1] + dudxj[2][2]) - lambda * (dp1dxj[k][2] * V[k][3] + dp1dxj[k][0] * V[k][1] + dp1dxj[k][1] * V[k][2]) / V[k][0];
    dtijdu1[2][2] = 2.0 * dmu[k][1] * dudxj[2][2] + dlambda[k][1] * (dudxj[0][0] + dudxj[1][1] + dudxj[2][2]) + lambda * dp1dxj[k][0] / V[k][0];
    dtijdu2[2][2] = 2.0 * dmu[k][2] * dudxj[2][2] + dlambda[k][2] * (dudxj[0][0] + dudxj[1][1] + dudxj[2][2]) + lambda * dp1dxj[k][1] / V[k][0];
    dtijdu3[2][2] = 2.0 * dmu[k][3] * dudxj[2][2] + 2.0 * mu * dp1dxj[k][2] / V[k][0] + dlambda[k][3] * (dudxj[0][0] + dudxj[1][1] + dudxj[2][2]) + lambda
                    * dp1dxj[k][2] / V[k][0];
    dtijdu4[2][2] = 2.0 * dmu[k][4] * dudxj[2][2] + dlambda[k][4] * (dudxj[0][0] + dudxj[1][1] + dudxj[2][2]);
    dtijdu5[2][2] = 2.0 * dmu[k][5] * dudxj[2][2] + dlambda[k][5] * (dudxj[0][0] + dudxj[1][1] + dudxj[2][2]);
    dtijdu0[0][1] = -mu * (dp1dxj[k][0] * V[k][2] + dp1dxj[k][1] * V[k][1]) / V[k][0] + dmu[k][0] * (dudxj[1][0] + dudxj[0][1]);
    dtijdu1[0][1] = mu * dp1dxj[k][1] / V[k][0] + dmu[k][1] * (dudxj[1][0] + dudxj[0][1]);
    dtijdu2[0][1] = mu * dp1dxj[k][0] / V[k][0] + dmu[k][2] * (dudxj[1][0] + dudxj[0][1]);
    dtijdu3[0][1] = dmu[k][3] * (dudxj[1][0] + dudxj[0][1]);
    dtijdu4[0][1] = dmu[k][4] * (dudxj[1][0] + dudxj[0][1]);
    dtijdu5[0][1] = dmu[k][5] * (dudxj[1][0] + dudxj[0][1]);
    dtijdu0[0][2] = -mu * (dp1dxj[k][0] * V[k][3] + dp1dxj[k][2] * V[k][1]) / V[k][0] + dmu[k][0] * (dudxj[2][0] + dudxj[0][2]);
    dtijdu1[0][2] = mu * dp1dxj[k][2] / V[k][0] + dmu[k][1] * (dudxj[2][0] + dudxj[0][2]);
    dtijdu2[0][2] = dmu[k][2] * (dudxj[2][0] + dudxj[0][2]);
    dtijdu3[0][2] = mu * dp1dxj[k][0] / V[k][0] + dmu[k][3] * (dudxj[2][0] + dudxj[0][2]);
    dtijdu4[0][2] = dmu[k][4] * (dudxj[2][0] + dudxj[0][2]);
    dtijdu5[0][2] = dmu[k][5] * (dudxj[2][0] + dudxj[0][2]);
    dtijdu0[1][2] = -mu * (dp1dxj[k][1] * V[k][3] + dp1dxj[k][2] * V[k][2]) / V[k][0] + dmu[k][0] * (dudxj[2][1] + dudxj[1][2]);
    dtijdu1[1][2] = dmu[k][1] * (dudxj[2][1] + dudxj[1][2]);
    dtijdu2[1][2] = mu * dp1dxj[k][2] / V[k][0] + dmu[k][2] * (dudxj[2][1] + dudxj[1][2]);
    dtijdu3[1][2] = mu * dp1dxj[k][1] / V[k][0] + dmu[k][3] * (dudxj[2][1] + dudxj[1][2]);
    dtijdu4[1][2] = dmu[k][4] * (dudxj[2][1] + dudxj[1][2]);
    dtijdu5[1][2] = dmu[k][5] * (dudxj[2][1] + dudxj[1][2]);
    dtijdu0[1][0] = dtijdu0[0][1];
    dtijdu1[1][0] = dtijdu1[0][1];
    dtijdu2[1][0] = dtijdu2[0][1];
    dtijdu3[1][0] = dtijdu3[0][1];
    dtijdu4[1][0] = dtijdu4[0][1];
    dtijdu5[1][0] = dtijdu5[0][1];
    dtijdu0[2][0] = dtijdu0[0][2];
    dtijdu1[2][0] = dtijdu1[0][2];
    dtijdu2[2][0] = dtijdu2[0][2];
    dtijdu3[2][0] = dtijdu3[0][2];
    dtijdu4[2][0] = dtijdu4[0][2];
    dtijdu5[2][0] = dtijdu5[0][2];
    dtijdu0[2][1] = dtijdu0[1][2];
    dtijdu1[2][1] = dtijdu1[1][2];
    dtijdu2[2][1] = dtijdu2[1][2];
    dtijdu3[2][1] = dtijdu3[1][2];
    dtijdu4[2][1] = dtijdu4[1][2];
    dtijdu5[2][1] = dtijdu5[1][2];
    varFcn->computeJacobianTemperature(V[k], dTduk, tag);
    dTdu0[k] = dTduk[0];
    dTdu1[k] = dTduk[1];
    dTdu2[k] = dTduk[2];
    dTdu3[k] = dTduk[3];
    dTdu4[k] = dTduk[4];
    dTdu5[k] = dTduk[5];
    ddTdxjdu0[0] = dp1dxj[k][0] * dTdu0[k];
    ddTdxjdu1[0] = dp1dxj[k][0] * dTdu1[k];
    ddTdxjdu2[0] = dp1dxj[k][0] * dTdu2[k];
    ddTdxjdu3[0] = dp1dxj[k][0] * dTdu3[k];
    ddTdxjdu4[0] = dp1dxj[k][0] * dTdu4[k];
    ddTdxjdu5[0] = dp1dxj[k][0] * dTdu5[k];
    ddTdxjdu0[1] = dp1dxj[k][1] * dTdu0[k];
    ddTdxjdu1[1] = dp1dxj[k][1] * dTdu1[k];
    ddTdxjdu2[1] = dp1dxj[k][1] * dTdu2[k];
    ddTdxjdu3[1] = dp1dxj[k][1] * dTdu3[k];
    ddTdxjdu4[1] = dp1dxj[k][1] * dTdu4[k];
    ddTdxjdu5[1] = dp1dxj[k][1] * dTdu5[k];
    ddTdxjdu0[2] = dp1dxj[k][2] * dTdu0[k];
    ddTdxjdu1[2] = dp1dxj[k][2] * dTdu1[k];
    ddTdxjdu2[2] = dp1dxj[k][2] * dTdu2[k];
    ddTdxjdu3[2] = dp1dxj[k][2] * dTdu3[k];
    ddTdxjdu4[2] = dp1dxj[k][2] * dTdu4[k];
    ddTdxjdu5[2] = dp1dxj[k][2] * dTdu5[k];
    dqjdu0[0] = -dkappa[k][0] * dTdxj[0] - kappa * ddTdxjdu0[0];
    dqjdu1[0] = -dkappa[k][1] * dTdxj[0] - kappa * ddTdxjdu1[0];
    dqjdu2[0] = -dkappa[k][2] * dTdxj[0] - kappa * ddTdxjdu2[0];
    dqjdu3[0] = -dkappa[k][3] * dTdxj[0] - kappa * ddTdxjdu3[0];
    dqjdu4[0] = -dkappa[k][4] * dTdxj[0] - kappa * ddTdxjdu4[0];
    dqjdu5[0] = -dkappa[k][5] * dTdxj[0] - kappa * ddTdxjdu5[0];
    dqjdu0[1] = -dkappa[k][0] * dTdxj[1] - kappa * ddTdxjdu0[1];
    dqjdu1[1] = -dkappa[k][1] * dTdxj[1] - kappa * ddTdxjdu1[1];
    dqjdu2[1] = -dkappa[k][2] * dTdxj[1] - kappa * ddTdxjdu2[1];
    dqjdu3[1] = -dkappa[k][3] * dTdxj[1] - kappa * ddTdxjdu3[1];
    dqjdu4[1] = -dkappa[k][4] * dTdxj[1] - kappa * ddTdxjdu4[1];
    dqjdu5[1] = -dkappa[k][5] * dTdxj[1] - kappa * ddTdxjdu5[1];
    dqjdu0[2] = -dkappa[k][0] * dTdxj[2] - kappa * ddTdxjdu0[2];
    dqjdu1[2] = -dkappa[k][1] * dTdxj[2] - kappa * ddTdxjdu1[2];
    dqjdu2[2] = -dkappa[k][2] * dTdxj[2] - kappa * ddTdxjdu2[2];
    dqjdu3[2] = -dkappa[k][3] * dTdxj[2] - kappa * ddTdxjdu3[2];
    dqjdu4[2] = -dkappa[k][4] * dTdxj[2] - kappa * ddTdxjdu4[2];
    dqjdu5[2] = -dkappa[k][5] * dTdxj[2] - kappa * ddTdxjdu5[2];
    // dRxdU
    dRdU[k][0][0][0] = 0.0;
    dRdU[k][0][0][1] = 0.0;
    dRdU[k][0][0][2] = 0.0;
    dRdU[k][0][0][3] = 0.0;
    dRdU[k][0][0][4] = 0.0;
    dRdU[k][0][0][5] = 0.0;
    dRdU[k][0][1][0] = dtijdu0[0][0];
    dRdU[k][0][1][1] = dtijdu1[0][0];
    dRdU[k][0][1][2] = dtijdu2[0][0];
    dRdU[k][0][1][3] = dtijdu3[0][0];
    dRdU[k][0][1][4] = dtijdu4[0][0];
    dRdU[k][0][1][5] = dtijdu5[0][0];
    dRdU[k][0][2][0] = dtijdu0[0][1];
    dRdU[k][0][2][1] = dtijdu1[0][1];
    dRdU[k][0][2][2] = dtijdu2[0][1];
    dRdU[k][0][2][3] = dtijdu3[0][1];
    dRdU[k][0][2][4] = dtijdu4[0][1];
    dRdU[k][0][2][5] = dtijdu5[0][1];
    dRdU[k][0][3][0] = dtijdu0[0][2];
    dRdU[k][0][3][1] = dtijdu1[0][2];
    dRdU[k][0][3][2] = dtijdu2[0][2];
    dRdU[k][0][3][3] = dtijdu3[0][2];
    dRdU[k][0][3][4] = dtijdu4[0][2];
    dRdU[k][0][3][5] = dtijdu5[0][2];
    dRdU[k][0][4][0] = ducgdu0[0] * tij[0][0] + ucg[0] * dtijdu0[0][0] + ducgdu0[1] * tij[0][1] + ucg[1] * dtijdu0[0][1] + ducgdu0[2] * tij[0][2] + ucg[2]
                       * dtijdu0[0][2] - dqjdu0[0];
    dRdU[k][0][4][1] = ducgdu1[0] * tij[0][0] + ucg[0] * dtijdu1[0][0] + ducgdu1[1] * tij[0][1] + ucg[1] * dtijdu1[0][1] + ducgdu1[2] * tij[0][2] + ucg[2]
                       * dtijdu1[0][2] - dqjdu1[0];
    dRdU[k][0][4][2] = ducgdu2[0] * tij[0][0] + ucg[0] * dtijdu2[0][0] + ducgdu2[1] * tij[0][1] + ucg[1] * dtijdu2[0][1] + ducgdu2[2] * tij[0][2] + ucg[2]
                       * dtijdu2[0][2] - dqjdu2[0];
    dRdU[k][0][4][3] = ducgdu3[0] * tij[0][0] + ucg[0] * dtijdu3[0][0] + ducgdu3[1] * tij[0][1] + ucg[1] * dtijdu3[0][1] + ducgdu3[2] * tij[0][2] + ucg[2]
                       * dtijdu3[0][2] - dqjdu3[0];
    dRdU[k][0][4][4] = ducgdu4[0] * tij[0][0] + ucg[0] * dtijdu4[0][0] + ducgdu4[1] * tij[0][1] + ucg[1] * dtijdu4[0][1] + ducgdu4[2] * tij[0][2] + ucg[2]
                       * dtijdu4[0][2] - dqjdu4[0];
    dRdU[k][0][4][5] = ducgdu5[0] * tij[0][0] + ucg[0] * dtijdu5[0][0] + ducgdu5[1] * tij[0][1] + ucg[1] * dtijdu5[0][1] + ducgdu5[2] * tij[0][2] + ucg[2]
                       * dtijdu5[0][2] - dqjdu5[0];
    // dRydU
    dRdU[k][1][0][0] = 0.0;
    dRdU[k][1][0][1] = 0.0;
    dRdU[k][1][0][2] = 0.0;
    dRdU[k][1][0][3] = 0.0;
    dRdU[k][1][0][4] = 0.0;
    dRdU[k][1][0][5] = 0.0;
    dRdU[k][1][1][0] = dtijdu0[0][1];
    dRdU[k][1][1][1] = dtijdu1[0][1];
    dRdU[k][1][1][2] = dtijdu2[0][1];
    dRdU[k][1][1][3] = dtijdu3[0][1];
    dRdU[k][1][1][4] = dtijdu4[0][1];
    dRdU[k][1][1][5] = dtijdu5[0][1];
    dRdU[k][1][2][0] = dtijdu0[1][1];
    dRdU[k][1][2][1] = dtijdu1[1][1];
    dRdU[k][1][2][2] = dtijdu2[1][1];
    dRdU[k][1][2][3] = dtijdu3[1][1];
    dRdU[k][1][2][4] = dtijdu4[1][1];
    dRdU[k][1][2][5] = dtijdu5[1][1];
    dRdU[k][1][3][0] = dtijdu0[1][2];
    dRdU[k][1][3][1] = dtijdu1[1][2];
    dRdU[k][1][3][2] = dtijdu2[1][2];
    dRdU[k][1][3][3] = dtijdu3[1][2];
    dRdU[k][1][3][4] = dtijdu4[1][2];
    dRdU[k][1][3][5] = dtijdu5[1][2];
    dRdU[k][1][4][0] = ducgdu0[0] * tij[0][1] + ucg[0] * dtijdu0[0][1] + ducgdu0[1] * tij[1][1] + ucg[1] * dtijdu0[1][1] + ducgdu0[2] * tij[1][2] + ucg[2]
                       * dtijdu0[1][2] - dqjdu0[1];
    dRdU[k][1][4][1] = ducgdu1[0] * tij[0][1] + ucg[0] * dtijdu1[0][1] + ducgdu1[1] * tij[1][1] + ucg[1] * dtijdu1[1][1] + ducgdu1[2] * tij[1][2] + ucg[2]
                       * dtijdu1[1][2] - dqjdu1[1];
    dRdU[k][1][4][2] = ducgdu2[0] * tij[0][1] + ucg[0] * dtijdu2[0][1] + ducgdu2[1] * tij[1][1] + ucg[1] * dtijdu2[1][1] + ducgdu2[2] * tij[1][2] + ucg[2]
                       * dtijdu2[1][2] - dqjdu2[1];
    dRdU[k][1][4][3] = ducgdu3[0] * tij[0][1] + ucg[0] * dtijdu3[0][1] + ducgdu3[1] * tij[1][1] + ucg[1] * dtijdu3[1][1] + ducgdu3[2] * tij[1][2] + ucg[2]
                       * dtijdu3[1][2] - dqjdu3[1];
    dRdU[k][1][4][4] = ducgdu4[0] * tij[0][1] + ucg[0] * dtijdu4[0][1] + ducgdu4[1] * tij[1][1] + ucg[1] * dtijdu4[1][1] + ducgdu4[2] * tij[1][2] + ucg[2]
                       * dtijdu4[1][2] - dqjdu4[1];
    dRdU[k][1][4][5] = ducgdu5[0] * tij[0][1] + ucg[0] * dtijdu5[0][1] + ducgdu5[1] * tij[1][1] + ucg[1] * dtijdu5[1][1] + ducgdu5[2] * tij[1][2] + ucg[2]
                       * dtijdu5[1][2] - dqjdu5[1];
    // dRzdU
    dRdU[k][2][0][0] = 0.0;
    dRdU[k][2][0][1] = 0.0;
    dRdU[k][2][0][2] = 0.0;
    dRdU[k][2][0][3] = 0.0;
    dRdU[k][2][0][4] = 0.0;
    dRdU[k][2][0][5] = 0.0;
    dRdU[k][2][1][0] = dtijdu0[0][2];
    dRdU[k][2][1][1] = dtijdu1[0][2];
    dRdU[k][2][1][2] = dtijdu2[0][2];
    dRdU[k][2][1][3] = dtijdu3[0][2];
    dRdU[k][2][1][4] = dtijdu4[0][2];
    dRdU[k][2][1][5] = dtijdu5[0][2];
    dRdU[k][2][2][0] = dtijdu0[1][2];
    dRdU[k][2][2][1] = dtijdu1[1][2];
    dRdU[k][2][2][2] = dtijdu2[1][2];
    dRdU[k][2][2][3] = dtijdu3[1][2];
    dRdU[k][2][2][4] = dtijdu4[1][2];
    dRdU[k][2][2][5] = dtijdu5[1][2];
    dRdU[k][2][3][0] = dtijdu0[2][2];
    dRdU[k][2][3][1] = dtijdu1[2][2];
    dRdU[k][2][3][2] = dtijdu2[2][2];
    dRdU[k][2][3][3] = dtijdu3[2][2];
    dRdU[k][2][3][4] = dtijdu4[2][2];
    dRdU[k][2][3][5] = dtijdu5[2][2];
    dRdU[k][2][4][0] = ducgdu0[0] * tij[0][2] + ucg[0] * dtijdu0[0][2] + ducgdu0[1] * tij[1][2] + ucg[1] * dtijdu0[1][2] + ducgdu0[2] * tij[2][2] + ucg[2]
                       * dtijdu0[2][2] - dqjdu0[2];
    dRdU[k][2][4][1] = ducgdu1[0] * tij[0][2] + ucg[0] * dtijdu1[0][2] + ducgdu1[1] * tij[1][2] + ucg[1] * dtijdu1[1][2] + ducgdu1[2] * tij[2][2] + ucg[2]
                       * dtijdu1[2][2] - dqjdu1[2];
    dRdU[k][2][4][2] = ducgdu2[0] * tij[0][2] + ucg[0] * dtijdu2[0][2] + ducgdu2[1] * tij[1][2] + ucg[1] * dtijdu2[1][2] + ducgdu2[2] * tij[2][2] + ucg[2]
                       * dtijdu2[2][2] - dqjdu2[2];
    dRdU[k][2][4][3] = ducgdu3[0] * tij[0][2] + ucg[0] * dtijdu3[0][2] + ducgdu3[1] * tij[1][2] + ucg[1] * dtijdu3[1][2] + ducgdu3[2] * tij[2][2] + ucg[2]
                       * dtijdu3[2][2] - dqjdu3[2];
    dRdU[k][2][4][4] = ducgdu4[0] * tij[0][2] + ucg[0] * dtijdu4[0][2] + ducgdu4[1] * tij[1][2] + ucg[1] * dtijdu4[1][2] + ducgdu4[2] * tij[2][2] + ucg[2]
                       * dtijdu4[2][2] - dqjdu4[2];
    dRdU[k][2][4][5] = ducgdu5[0] * tij[0][2] + ucg[0] * dtijdu5[0][2] + ducgdu5[1] * tij[1][2] + ucg[1] * dtijdu5[1][2] + ducgdu5[2] * tij[2][2] + ucg[2]
                       * dtijdu5[2][2] - dqjdu5[2];
  }
}

//------------------------------------------------------------------------------
// override for strongly coupled KE (neq = dim = 7)
template<>
inline
void NavierStokesTerm::computeJacobianVolumeTermNS(double dp1dxj[4][3], double mu, double dmu[4][7],
                                                   double lambda, double dlambda[4][7], double kappa, double dkappa[4][7],
                                                   double *V[4], double ucg[3], double dudxj[3][3],
                                                   int tag, double dTdxj[3], double (*dRdU)[3][7][7]) {
  double tij[3][3];
  computeStressTensor(mu, lambda, dudxj, tij);
  double dtijdu0[3][3];
  double dtijdu1[3][3];
  double dtijdu2[3][3];
  double dtijdu3[3][3];
  double dtijdu4[3][3];
  double dtijdu5[3][3];
  double dtijdu6[3][3];
  double ducgdu0[3];
  double ducgdu1[3];
  double ducgdu2[3];
  double ducgdu3[3];
  double ducgdu4[3];
  double ducgdu5[3];
  double ducgdu6[3];
  double dTduk[7];
  double dTdu0[4];
  double dTdu1[4];
  double dTdu2[4];
  double dTdu3[4];
  double dTdu4[4];
  double dTdu5[4];
  double dTdu6[4];
  double ddTdxjdu0[3];
  double ddTdxjdu1[3];
  double ddTdxjdu2[3];
  double ddTdxjdu3[3];
  double ddTdxjdu4[3];
  double ddTdxjdu5[3];
  double ddTdxjdu6[3];
  double dqjdu0[3];
  double dqjdu1[3];
  double dqjdu2[3];
  double dqjdu3[3];
  double dqjdu4[3];
  double dqjdu5[3];
  double dqjdu6[3];
  for(int k = 0; k < 4; ++k) {
    ducgdu0[0] = -fourth * V[k][1] / V[k][0];
    ducgdu1[0] = fourth / V[k][0];
    ducgdu2[0] = 0.0;
    ducgdu3[0] = 0.0;
    ducgdu4[0] = 0.0;
    ducgdu5[0] = 0.0;
    ducgdu6[0] = 0.0;
    ducgdu0[1] = -fourth * V[k][2] / V[k][0];
    ducgdu1[1] = 0.0;
    ducgdu2[1] = fourth / V[k][0];
    ducgdu3[1] = 0.0;
    ducgdu4[1] = 0.0;
    ducgdu5[1] = 0.0;
    ducgdu6[1] = 0.0;
    ducgdu0[2] = -fourth * V[k][3] / V[k][0];
    ducgdu1[2] = 0.0;
    ducgdu2[2] = 0.0;
    ducgdu3[2] = fourth / V[k][0];
    ducgdu4[2] = 0.0;
    ducgdu5[2] = 0.0;
    ducgdu6[2] = 0.0;
    dtijdu0[0][0] = 2.0 * dmu[k][0] * dudxj[0][0] - 2.0 * mu * dp1dxj[k][0] * V[k][1] / V[k][0] + dlambda[k][0] *
                    (dudxj[0][0] + dudxj[1][1] + dudxj[2][2]) - lambda * (dp1dxj[k][0] * V[k][1] + dp1dxj[k][1] * V[k][2] + dp1dxj[k][2] * V[k][3]) / V[k][0];
    dtijdu1[0][0] = 2.0 * dmu[k][1] * dudxj[0][0] + 2.0 * mu * dp1dxj[k][0] / V[k][0] + dlambda[k][1] * (dudxj[0][0] + dudxj[1][1] + dudxj[2][2]) + lambda
                    * dp1dxj[k][0] / V[k][0];
    dtijdu2[0][0] = 2.0 * dmu[k][2] * dudxj[0][0] + dlambda[k][2] * (dudxj[0][0] + dudxj[1][1] + dudxj[2][2]) + lambda * dp1dxj[k][1] / V[k][0];
    dtijdu3[0][0] = 2.0 * dmu[k][3] * dudxj[0][0] + dlambda[k][3] * (dudxj[0][0] + dudxj[1][1] + dudxj[2][2]) + lambda * dp1dxj[k][2] / V[k][0];
    dtijdu4[0][0] = 2.0 * dmu[k][4] * dudxj[0][0] + dlambda[k][4] * (dudxj[0][0] + dudxj[1][1] + dudxj[2][2]);
    dtijdu5[0][0] = 2.0 * dmu[k][5] * dudxj[0][0] + dlambda[k][5] * (dudxj[0][0] + dudxj[1][1] + dudxj[2][2]);
    dtijdu6[0][0] = 2.0 * dmu[k][6] * dudxj[0][0] + dlambda[k][6] * (dudxj[0][0] + dudxj[1][1] + dudxj[2][2]);
    dtijdu0[1][1] = 2.0 * dmu[k][0] * dudxj[1][1] - 2.0 * mu * dp1dxj[k][1] * V[k][2] / V[k][0] + dlambda[k][0] *
                    (dudxj[0][0] + dudxj[1][1] + dudxj[2][2]) - lambda * (dp1dxj[k][1] * V[k][2] + dp1dxj[k][0] * V[k][1] + dp1dxj[k][2] * V[k][3]) / V[k][0];
    dtijdu1[1][1] = 2.0 * dmu[k][1] * dudxj[1][1] + dlambda[k][1] * (dudxj[0][0] + dudxj[1][1] + dudxj[2][2]) + lambda * dp1dxj[k][0] / V[k][0];
    dtijdu2[1][1] = 2.0 * dmu[k][2] * dudxj[1][1] + 2.0 * mu * dp1dxj[k][1] / V[k][0] + dlambda[k][2] * (dudxj[0][0] + dudxj[1][1] + dudxj[2][2]) + lambda
                    * dp1dxj[k][1] / V[k][0];
    dtijdu3[1][1] = 2.0 * dmu[k][3] * dudxj[1][1] + dlambda[k][3] * (dudxj[0][0] + dudxj[1][1] + dudxj[2][2]) + lambda * dp1dxj[k][2] / V[k][0];
    dtijdu4[1][1] = 2.0 * dmu[k][4] * dudxj[1][1] + dlambda[k][4] * (dudxj[0][0] + dudxj[1][1] + dudxj[2][2]);
    dtijdu5[1][1] = 2.0 * dmu[k][5] * dudxj[1][1] + dlambda[k][5] * (dudxj[0][0] + dudxj[1][1] + dudxj[2][2]);
    dtijdu6[1][1] = 2.0 * dmu[k][6] * dudxj[1][1] + dlambda[k][6] * (dudxj[0][0] + dudxj[1][1] + dudxj[2][2]);
    dtijdu0[2][2] = 2.0 * dmu[k][0] * dudxj[2][2] - 2.0 * mu * dp1dxj[k][2] * V[k][3] / V[k][0] + dlambda[k][0] *
                    (dudxj[0][0] + dudxj[1][1] + dudxj[2][2]) - lambda * (dp1dxj[k][2] * V[k][3] + dp1dxj[k][0] * V[k][1] + dp1dxj[k][1] * V[k][2]) / V[k][0];
    dtijdu1[2][2] = 2.0 * dmu[k][1] * dudxj[2][2] + dlambda[k][1] * (dudxj[0][0] + dudxj[1][1] + dudxj[2][2]) + lambda * dp1dxj[k][0] / V[k][0];
    dtijdu2[2][2] = 2.0 * dmu[k][2] * dudxj[2][2] + dlambda[k][2] * (dudxj[0][0] + dudxj[1][1] + dudxj[2][2]) + lambda * dp1dxj[k][1] / V[k][0];
    dtijdu3[2][2] = 2.0 * dmu[k][3] * dudxj[2][2] + 2.0 * mu * dp1dxj[k][2] / V[k][0] + dlambda[k][3] * (dudxj[0][0] + dudxj[1][1] + dudxj[2][2]) + lambda
                    * dp1dxj[k][2] / V[k][0];
    dtijdu4[2][2] = 2.0 * dmu[k][4] * dudxj[2][2] + dlambda[k][4] * (dudxj[0][0] + dudxj[1][1] + dudxj[2][2]);
    dtijdu5[2][2] = 2.0 * dmu[k][5] * dudxj[2][2] + dlambda[k][5] * (dudxj[0][0] + dudxj[1][1] + dudxj[2][2]);
    dtijdu6[2][2] = 2.0 * dmu[k][6] * dudxj[2][2] + dlambda[k][6] * (dudxj[0][0] + dudxj[1][1] + dudxj[2][2]);
    dtijdu0[0][1] = -mu * (dp1dxj[k][0] * V[k][2] + dp1dxj[k][1] * V[k][1]) / V[k][0] + dmu[k][0] * (dudxj[1][0] + dudxj[0][1]);
    dtijdu1[0][1] = mu * dp1dxj[k][1] / V[k][0] + dmu[k][1] * (dudxj[1][0] + dudxj[0][1]);
    dtijdu2[0][1] = mu * dp1dxj[k][0] / V[k][0] + dmu[k][2] * (dudxj[1][0] + dudxj[0][1]);
    dtijdu3[0][1] = dmu[k][3] * (dudxj[1][0] + dudxj[0][1]);
    dtijdu4[0][1] = dmu[k][4] * (dudxj[1][0] + dudxj[0][1]);
    dtijdu5[0][1] = dmu[k][5] * (dudxj[1][0] + dudxj[0][1]);
    dtijdu6[0][1] = dmu[k][6] * (dudxj[1][0] + dudxj[0][1]);
    dtijdu0[0][2] = -mu * (dp1dxj[k][0] * V[k][3] + dp1dxj[k][2] * V[k][1]) / V[k][0] + dmu[k][0] * (dudxj[2][0] + dudxj[0][2]);
    dtijdu1[0][2] = mu * dp1dxj[k][2] / V[k][0] + dmu[k][1] * (dudxj[2][0] + dudxj[0][2]);
    dtijdu2[0][2] = dmu[k][2] * (dudxj[2][0] + dudxj[0][2]);
    dtijdu3[0][2] = mu * dp1dxj[k][0] / V[k][0] + dmu[k][3] * (dudxj[2][0] + dudxj[0][2]);
    dtijdu4[0][2] = dmu[k][4] * (dudxj[2][0] + dudxj[0][2]);
    dtijdu5[0][2] = dmu[k][5] * (dudxj[2][0] + dudxj[0][2]);
    dtijdu6[0][2] = dmu[k][6] * (dudxj[2][0] + dudxj[0][2]);
    dtijdu0[1][2] = -mu * (dp1dxj[k][1] * V[k][3] + dp1dxj[k][2] * V[k][2]) / V[k][0] + dmu[k][0] * (dudxj[2][1] + dudxj[1][2]);
    dtijdu1[1][2] = dmu[k][1] * (dudxj[2][1] + dudxj[1][2]);
    dtijdu2[1][2] = mu * dp1dxj[k][2] / V[k][0] + dmu[k][2] * (dudxj[2][1] + dudxj[1][2]);
    dtijdu3[1][2] = mu * dp1dxj[k][1] / V[k][0] + dmu[k][3] * (dudxj[2][1] + dudxj[1][2]);
    dtijdu4[1][2] = dmu[k][4] * (dudxj[2][1] + dudxj[1][2]);
    dtijdu5[1][2] = dmu[k][5] * (dudxj[2][1] + dudxj[1][2]);
    dtijdu6[1][2] = dmu[k][6] * (dudxj[2][1] + dudxj[1][2]);
    dtijdu0[1][0] = dtijdu0[0][1];
    dtijdu1[1][0] = dtijdu1[0][1];
    dtijdu2[1][0] = dtijdu2[0][1];
    dtijdu3[1][0] = dtijdu3[0][1];
    dtijdu4[1][0] = dtijdu4[0][1];
    dtijdu5[1][0] = dtijdu5[0][1];
    dtijdu6[1][0] = dtijdu6[0][1];
    dtijdu0[2][0] = dtijdu0[0][2];
    dtijdu1[2][0] = dtijdu1[0][2];
    dtijdu2[2][0] = dtijdu2[0][2];
    dtijdu3[2][0] = dtijdu3[0][2];
    dtijdu4[2][0] = dtijdu4[0][2];
    dtijdu5[2][0] = dtijdu5[0][2];
    dtijdu6[2][0] = dtijdu6[0][2];
    dtijdu0[2][1] = dtijdu0[1][2];
    dtijdu1[2][1] = dtijdu1[1][2];
    dtijdu2[2][1] = dtijdu2[1][2];
    dtijdu3[2][1] = dtijdu3[1][2];
    dtijdu4[2][1] = dtijdu4[1][2];
    dtijdu5[2][1] = dtijdu5[1][2];
    dtijdu6[2][1] = dtijdu6[1][2];
    varFcn->computeJacobianTemperature(V[k], dTduk, tag);
    dTdu0[k] = dTduk[0];
    dTdu1[k] = dTduk[1];
    dTdu2[k] = dTduk[2];
    dTdu3[k] = dTduk[3];
    dTdu4[k] = dTduk[4];
    dTdu5[k] = dTduk[5];
    dTdu6[k] = dTduk[6];
    ddTdxjdu0[0] = dp1dxj[k][0] * dTdu0[k];
    ddTdxjdu1[0] = dp1dxj[k][0] * dTdu1[k];
    ddTdxjdu2[0] = dp1dxj[k][0] * dTdu2[k];
    ddTdxjdu3[0] = dp1dxj[k][0] * dTdu3[k];
    ddTdxjdu4[0] = dp1dxj[k][0] * dTdu4[k];
    ddTdxjdu5[0] = dp1dxj[k][0] * dTdu5[k];
    ddTdxjdu6[0] = dp1dxj[k][0] * dTdu6[k];
    ddTdxjdu0[1] = dp1dxj[k][1] * dTdu0[k];
    ddTdxjdu1[1] = dp1dxj[k][1] * dTdu1[k];
    ddTdxjdu2[1] = dp1dxj[k][1] * dTdu2[k];
    ddTdxjdu3[1] = dp1dxj[k][1] * dTdu3[k];
    ddTdxjdu4[1] = dp1dxj[k][1] * dTdu4[k];
    ddTdxjdu5[1] = dp1dxj[k][1] * dTdu5[k];
    ddTdxjdu6[1] = dp1dxj[k][1] * dTdu6[k];
    ddTdxjdu0[2] = dp1dxj[k][2] * dTdu0[k];
    ddTdxjdu1[2] = dp1dxj[k][2] * dTdu1[k];
    ddTdxjdu2[2] = dp1dxj[k][2] * dTdu2[k];
    ddTdxjdu3[2] = dp1dxj[k][2] * dTdu3[k];
    ddTdxjdu4[2] = dp1dxj[k][2] * dTdu4[k];
    ddTdxjdu5[2] = dp1dxj[k][2] * dTdu5[k];
    ddTdxjdu6[2] = dp1dxj[k][2] * dTdu6[k];
    dqjdu0[0] = -dkappa[k][0] * dTdxj[0] - kappa * ddTdxjdu0[0];
    dqjdu1[0] = -dkappa[k][1] * dTdxj[0] - kappa * ddTdxjdu1[0];
    dqjdu2[0] = -dkappa[k][2] * dTdxj[0] - kappa * ddTdxjdu2[0];
    dqjdu3[0] = -dkappa[k][3] * dTdxj[0] - kappa * ddTdxjdu3[0];
    dqjdu4[0] = -dkappa[k][4] * dTdxj[0] - kappa * ddTdxjdu4[0];
    dqjdu5[0] = -dkappa[k][5] * dTdxj[0] - kappa * ddTdxjdu5[0];
    dqjdu6[0] = -dkappa[k][6] * dTdxj[0] - kappa * ddTdxjdu6[0];
    dqjdu0[1] = -dkappa[k][0] * dTdxj[1] - kappa * ddTdxjdu0[1];
    dqjdu1[1] = -dkappa[k][1] * dTdxj[1] - kappa * ddTdxjdu1[1];
    dqjdu2[1] = -dkappa[k][2] * dTdxj[1] - kappa * ddTdxjdu2[1];
    dqjdu3[1] = -dkappa[k][3] * dTdxj[1] - kappa * ddTdxjdu3[1];
    dqjdu4[1] = -dkappa[k][4] * dTdxj[1] - kappa * ddTdxjdu4[1];
    dqjdu5[1] = -dkappa[k][5] * dTdxj[1] - kappa * ddTdxjdu5[1];
    dqjdu6[1] = -dkappa[k][6] * dTdxj[1] - kappa * ddTdxjdu6[1];
    dqjdu0[2] = -dkappa[k][0] * dTdxj[2] - kappa * ddTdxjdu0[2];
    dqjdu1[2] = -dkappa[k][1] * dTdxj[2] - kappa * ddTdxjdu1[2];
    dqjdu2[2] = -dkappa[k][2] * dTdxj[2] - kappa * ddTdxjdu2[2];
    dqjdu3[2] = -dkappa[k][3] * dTdxj[2] - kappa * ddTdxjdu3[2];
    dqjdu4[2] = -dkappa[k][4] * dTdxj[2] - kappa * ddTdxjdu4[2];
    dqjdu5[2] = -dkappa[k][5] * dTdxj[2] - kappa * ddTdxjdu5[2];
    dqjdu6[2] = -dkappa[k][6] * dTdxj[2] - kappa * ddTdxjdu6[2];
    // dRxdU
    dRdU[k][0][0][0] = 0.0;
    dRdU[k][0][0][1] = 0.0;
    dRdU[k][0][0][2] = 0.0;
    dRdU[k][0][0][3] = 0.0;
    dRdU[k][0][0][4] = 0.0;
    dRdU[k][0][0][5] = 0.0;
    dRdU[k][0][0][6] = 0.0;
    dRdU[k][0][1][0] = dtijdu0[0][0];
    dRdU[k][0][1][1] = dtijdu1[0][0];
    dRdU[k][0][1][2] = dtijdu2[0][0];
    dRdU[k][0][1][3] = dtijdu3[0][0];
    dRdU[k][0][1][4] = dtijdu4[0][0];
    dRdU[k][0][1][5] = dtijdu5[0][0];
    dRdU[k][0][1][6] = dtijdu6[0][0];
    dRdU[k][0][2][0] = dtijdu0[0][1];
    dRdU[k][0][2][1] = dtijdu1[0][1];
    dRdU[k][0][2][2] = dtijdu2[0][1];
    dRdU[k][0][2][3] = dtijdu3[0][1];
    dRdU[k][0][2][4] = dtijdu4[0][1];
    dRdU[k][0][2][5] = dtijdu5[0][1];
    dRdU[k][0][2][6] = dtijdu6[0][1];
    dRdU[k][0][3][0] = dtijdu0[0][2];
    dRdU[k][0][3][1] = dtijdu1[0][2];
    dRdU[k][0][3][2] = dtijdu2[0][2];
    dRdU[k][0][3][3] = dtijdu3[0][2];
    dRdU[k][0][3][4] = dtijdu4[0][2];
    dRdU[k][0][3][5] = dtijdu5[0][2];
    dRdU[k][0][3][6] = dtijdu6[0][2];
    dRdU[k][0][4][0] = ducgdu0[0] * tij[0][0] + ucg[0] * dtijdu0[0][0] + ducgdu0[1] * tij[0][1] + ucg[1] * dtijdu0[0][1] + ducgdu0[2] * tij[0][2] + ucg[2]
                       * dtijdu0[0][2] - dqjdu0[0];
    dRdU[k][0][4][1] = ducgdu1[0] * tij[0][0] + ucg[0] * dtijdu1[0][0] + ducgdu1[1] * tij[0][1] + ucg[1] * dtijdu1[0][1] + ducgdu1[2] * tij[0][2] + ucg[2]
                       * dtijdu1[0][2] - dqjdu1[0];
    dRdU[k][0][4][2] = ducgdu2[0] * tij[0][0] + ucg[0] * dtijdu2[0][0] + ducgdu2[1] * tij[0][1] + ucg[1] * dtijdu2[0][1] + ducgdu2[2] * tij[0][2] + ucg[2]
                       * dtijdu2[0][2] - dqjdu2[0];
    dRdU[k][0][4][3] = ducgdu3[0] * tij[0][0] + ucg[0] * dtijdu3[0][0] + ducgdu3[1] * tij[0][1] + ucg[1] * dtijdu3[0][1] + ducgdu3[2] * tij[0][2] + ucg[2]
                       * dtijdu3[0][2] - dqjdu3[0];
    dRdU[k][0][4][4] = ducgdu4[0] * tij[0][0] + ucg[0] * dtijdu4[0][0] + ducgdu4[1] * tij[0][1] + ucg[1] * dtijdu4[0][1] + ducgdu4[2] * tij[0][2] + ucg[2]
                       * dtijdu4[0][2] - dqjdu4[0];
    dRdU[k][0][4][5] = ducgdu5[0] * tij[0][0] + ucg[0] * dtijdu5[0][0] + ducgdu5[1] * tij[0][1] + ucg[1] * dtijdu5[0][1] + ducgdu5[2] * tij[0][2] + ucg[2]
                       * dtijdu5[0][2] - dqjdu5[0];
    dRdU[k][0][4][6] = ducgdu6[0] * tij[0][0] + ucg[0] * dtijdu6[0][0] + ducgdu6[1] * tij[0][1] + ucg[1] * dtijdu6[0][1] + ducgdu6[2] * tij[0][2] + ucg[2]
                       * dtijdu6[0][2] - dqjdu6[0];
    // dRydU
    dRdU[k][1][0][0] = 0.0;
    dRdU[k][1][0][1] = 0.0;
    dRdU[k][1][0][2] = 0.0;
    dRdU[k][1][0][3] = 0.0;
    dRdU[k][1][0][4] = 0.0;
    dRdU[k][1][0][5] = 0.0;
    dRdU[k][1][0][6] = 0.0;
    dRdU[k][1][1][0] = dtijdu0[0][1];
    dRdU[k][1][1][1] = dtijdu1[0][1];
    dRdU[k][1][1][2] = dtijdu2[0][1];
    dRdU[k][1][1][3] = dtijdu3[0][1];
    dRdU[k][1][1][4] = dtijdu4[0][1];
    dRdU[k][1][1][5] = dtijdu5[0][1];
    dRdU[k][1][1][6] = dtijdu6[0][1];
    dRdU[k][1][2][0] = dtijdu0[1][1];
    dRdU[k][1][2][1] = dtijdu1[1][1];
    dRdU[k][1][2][2] = dtijdu2[1][1];
    dRdU[k][1][2][3] = dtijdu3[1][1];
    dRdU[k][1][2][4] = dtijdu4[1][1];
    dRdU[k][1][2][5] = dtijdu5[1][1];
    dRdU[k][1][2][6] = dtijdu6[1][1];
    dRdU[k][1][3][0] = dtijdu0[1][2];
    dRdU[k][1][3][1] = dtijdu1[1][2];
    dRdU[k][1][3][2] = dtijdu2[1][2];
    dRdU[k][1][3][3] = dtijdu3[1][2];
    dRdU[k][1][3][4] = dtijdu4[1][2];
    dRdU[k][1][3][5] = dtijdu5[1][2];
    dRdU[k][1][3][6] = dtijdu6[1][2];
    dRdU[k][1][4][0] = ducgdu0[0] * tij[0][1] + ucg[0] * dtijdu0[0][1] + ducgdu0[1] * tij[1][1] + ucg[1] * dtijdu0[1][1] + ducgdu0[2] * tij[1][2] + ucg[2]
                       * dtijdu0[1][2] - dqjdu0[1];
    dRdU[k][1][4][1] = ducgdu1[0] * tij[0][1] + ucg[0] * dtijdu1[0][1] + ducgdu1[1] * tij[1][1] + ucg[1] * dtijdu1[1][1] + ducgdu1[2] * tij[1][2] + ucg[2]
                       * dtijdu1[1][2] - dqjdu1[1];
    dRdU[k][1][4][2] = ducgdu2[0] * tij[0][1] + ucg[0] * dtijdu2[0][1] + ducgdu2[1] * tij[1][1] + ucg[1] * dtijdu2[1][1] + ducgdu2[2] * tij[1][2] + ucg[2]
                       * dtijdu2[1][2] - dqjdu2[1];
    dRdU[k][1][4][3] = ducgdu3[0] * tij[0][1] + ucg[0] * dtijdu3[0][1] + ducgdu3[1] * tij[1][1] + ucg[1] * dtijdu3[1][1] + ducgdu3[2] * tij[1][2] + ucg[2]
                       * dtijdu3[1][2] - dqjdu3[1];
    dRdU[k][1][4][4] = ducgdu4[0] * tij[0][1] + ucg[0] * dtijdu4[0][1] + ducgdu4[1] * tij[1][1] + ucg[1] * dtijdu4[1][1] + ducgdu4[2] * tij[1][2] + ucg[2]
                       * dtijdu4[1][2] - dqjdu4[1];
    dRdU[k][1][4][5] = ducgdu5[0] * tij[0][1] + ucg[0] * dtijdu5[0][1] + ducgdu5[1] * tij[1][1] + ucg[1] * dtijdu5[1][1] + ducgdu5[2] * tij[1][2] + ucg[2]
                       * dtijdu5[1][2] - dqjdu5[1];
    dRdU[k][1][4][6] = ducgdu6[0] * tij[0][1] + ucg[0] * dtijdu6[0][1] + ducgdu6[1] * tij[1][1] + ucg[1] * dtijdu6[1][1] + ducgdu6[2] * tij[1][2] + ucg[2]
                       * dtijdu6[1][2] - dqjdu6[1];
    // dRzdU
    dRdU[k][2][0][0] = 0.0;
    dRdU[k][2][0][1] = 0.0;
    dRdU[k][2][0][2] = 0.0;
    dRdU[k][2][0][3] = 0.0;
    dRdU[k][2][0][4] = 0.0;
    dRdU[k][2][0][5] = 0.0;
    dRdU[k][2][0][6] = 0.0;
    dRdU[k][2][1][0] = dtijdu0[0][2];
    dRdU[k][2][1][1] = dtijdu1[0][2];
    dRdU[k][2][1][2] = dtijdu2[0][2];
    dRdU[k][2][1][3] = dtijdu3[0][2];
    dRdU[k][2][1][4] = dtijdu4[0][2];
    dRdU[k][2][1][5] = dtijdu5[0][2];
    dRdU[k][2][1][6] = dtijdu6[0][2];
    dRdU[k][2][2][0] = dtijdu0[1][2];
    dRdU[k][2][2][1] = dtijdu1[1][2];
    dRdU[k][2][2][2] = dtijdu2[1][2];
    dRdU[k][2][2][3] = dtijdu3[1][2];
    dRdU[k][2][2][4] = dtijdu4[1][2];
    dRdU[k][2][2][5] = dtijdu5[1][2];
    dRdU[k][2][2][6] = dtijdu6[1][2];
    dRdU[k][2][3][0] = dtijdu0[2][2];
    dRdU[k][2][3][1] = dtijdu1[2][2];
    dRdU[k][2][3][2] = dtijdu2[2][2];
    dRdU[k][2][3][3] = dtijdu3[2][2];
    dRdU[k][2][3][4] = dtijdu4[2][2];
    dRdU[k][2][3][5] = dtijdu5[2][2];
    dRdU[k][2][3][6] = dtijdu6[2][2];
    dRdU[k][2][4][0] = ducgdu0[0] * tij[0][2] + ucg[0] * dtijdu0[0][2] + ducgdu0[1] * tij[1][2] + ucg[1] * dtijdu0[1][2] + ducgdu0[2] * tij[2][2] + ucg[2]
                       * dtijdu0[2][2] - dqjdu0[2];
    dRdU[k][2][4][1] = ducgdu1[0] * tij[0][2] + ucg[0] * dtijdu1[0][2] + ducgdu1[1] * tij[1][2] + ucg[1] * dtijdu1[1][2] + ducgdu1[2] * tij[2][2] + ucg[2]
                       * dtijdu1[2][2] - dqjdu1[2];
    dRdU[k][2][4][2] = ducgdu2[0] * tij[0][2] + ucg[0] * dtijdu2[0][2] + ducgdu2[1] * tij[1][2] + ucg[1] * dtijdu2[1][2] + ducgdu2[2] * tij[2][2] + ucg[2]
                       * dtijdu2[2][2] - dqjdu2[2];
    dRdU[k][2][4][3] = ducgdu3[0] * tij[0][2] + ucg[0] * dtijdu3[0][2] + ducgdu3[1] * tij[1][2] + ucg[1] * dtijdu3[1][2] + ducgdu3[2] * tij[2][2] + ucg[2]
                       * dtijdu3[2][2] - dqjdu3[2];
    dRdU[k][2][4][4] = ducgdu4[0] * tij[0][2] + ucg[0] * dtijdu4[0][2] + ducgdu4[1] * tij[1][2] + ucg[1] * dtijdu4[1][2] + ducgdu4[2] * tij[2][2] + ucg[2]
                       * dtijdu4[2][2] - dqjdu4[2];
    dRdU[k][2][4][5] = ducgdu5[0] * tij[0][2] + ucg[0] * dtijdu5[0][2] + ducgdu5[1] * tij[1][2] + ucg[1] * dtijdu5[1][2] + ducgdu5[2] * tij[2][2] + ucg[2]
                       * dtijdu5[2][2] - dqjdu5[2];
    dRdU[k][2][4][6] = ducgdu6[0] * tij[0][2] + ucg[0] * dtijdu6[0][2] + ducgdu6[1] * tij[1][2] + ucg[1] * dtijdu6[1][2] + ducgdu6[2] * tij[2][2] + ucg[2]
                       * dtijdu6[2][2] - dqjdu6[2];
  }
}

//------------------------------------------------------------------------------

template<int neq>
inline
void NavierStokesTerm::computeJacobianVolumeTermNS(double dp1dxj[4][3], double nu, double dnu[4][neq], double mu, double dmu[4][neq],
                                                   double lambda, double dlambda[4][neq], double kappa, double dkappa[4][neq],
                                                   double *V[4], double ucg[3], double dudxj[3][3],
                                                   int tag, double dTdxj[3], double (*dRdU)[3][neq][neq]) {
  double tij[3][3];
  computeStressTensor(mu, lambda, dudxj, tij);
  double dtijdu0[3][3];
  double dtijdu1[3][3];
  double dtijdu2[3][3];
  double dtijdu3[3][3];
  double dtijdu4[3][3];
  double ducgdu0[3];
  double ducgdu1[3];
  double ducgdu2[3];
  double ducgdu3[3];
  double ducgdu4[3];
  double dTduk[7]; // TODO: use dim for dTduk
  double dTdu0kappa[4];
  double dTdu1kappa[4];
  double dTdu2kappa[4];
  double dTdu3kappa[4];
  double dTdu4kappa[4];
  double dqjdu0[3];
  double dqjdu1[3];
  double dqjdu2[3];
  double dqjdu3[3];
  double dqjdu4[3];

  const double toto = dudxj[0][0] + dudxj[1][1] + dudxj[2][2];

  for(int k = 0; k < 4; ++k) {
    const double invrho = 1 / V[k][0];
    const double mudivrho = mu * invrho;
    const double lambdadivrho = lambda * invrho;
    const double kappadivrho = kappa * invrho;

    ducgdu0[0] = -fourth * V[k][1] * invrho;
    ducgdu1[0] = fourth * invrho;
    ducgdu0[1] = -fourth * V[k][2] * invrho;
    ducgdu2[1] = fourth * invrho;
    ducgdu0[2] = -fourth * V[k][3] * invrho;
    ducgdu3[2] = fourth * invrho;

    dtijdu0[0][0] = 2.0 * dmu[k][0] * dudxj[0][0] - 2.0 * mudivrho * dp1dxj[k][0] * V[k][1] + dlambda[k][0] * toto -
                    lambdadivrho * (dp1dxj[k][0] * V[k][1] + dp1dxj[k][1] * V[k][2] + dp1dxj[k][2] * V[k][3]);
    dtijdu0[0][1] = -mudivrho * (dp1dxj[k][0] * V[k][2] + dp1dxj[k][1] * V[k][1]) + dmu[k][0] * (dudxj[1][0] + dudxj[0][1]);
    dtijdu0[0][2] = -mudivrho * (dp1dxj[k][0] * V[k][3] + dp1dxj[k][2] * V[k][1]) + dmu[k][0] * (dudxj[2][0] + dudxj[0][2]);
    dtijdu0[1][0] = dtijdu0[0][1];
    dtijdu0[1][1] = 2.0 * dmu[k][0] * dudxj[1][1] - 2.0 * mudivrho * dp1dxj[k][1] * V[k][2] + dlambda[k][0] * toto -
                    lambdadivrho * (dp1dxj[k][1] * V[k][2] + dp1dxj[k][0] * V[k][1] + dp1dxj[k][2] * V[k][3]);
    dtijdu0[1][2] = -mudivrho * (dp1dxj[k][1] * V[k][3] + dp1dxj[k][2] * V[k][2]) + dmu[k][0] * (dudxj[2][1] + dudxj[1][2]);
    dtijdu0[2][0] = dtijdu0[0][2];
    dtijdu0[2][1] = dtijdu0[1][2];
    dtijdu0[2][2] = 2.0 * dmu[k][0] * dudxj[2][2] - 2.0 * mudivrho * dp1dxj[k][2] * V[k][3] + dlambda[k][0] * toto -
                    lambdadivrho * (dp1dxj[k][2] * V[k][3] + dp1dxj[k][0] * V[k][1] + dp1dxj[k][1] * V[k][2]);

    dtijdu1[0][0] = 2.0 * dmu[k][1] * dudxj[0][0] + 2.0 * mudivrho * dp1dxj[k][0] + dlambda[k][1] * toto + lambdadivrho * dp1dxj[k][0];
    dtijdu1[0][1] = mudivrho * dp1dxj[k][1] + dmu[k][1] * (dudxj[1][0] + dudxj[0][1]);
    dtijdu1[0][2] = mudivrho * dp1dxj[k][2] + dmu[k][1] * (dudxj[2][0] + dudxj[0][2]);
    dtijdu1[1][0] = dtijdu1[0][1];
    dtijdu1[1][1] = 2.0 * dmu[k][1] * dudxj[1][1] + dlambda[k][1] * toto + lambdadivrho * dp1dxj[k][0];
    dtijdu1[1][2] = dmu[k][1] * (dudxj[2][1] + dudxj[1][2]);
    dtijdu1[2][0] = dtijdu1[0][2];
    dtijdu1[2][1] = dtijdu1[1][2];
    dtijdu1[2][2] = 2.0 * dmu[k][1] * dudxj[2][2] + dlambda[k][1] * toto + lambdadivrho * dp1dxj[k][0];

    dtijdu2[0][0] = 2.0 * dmu[k][2] * dudxj[0][0] + dlambda[k][2] * toto + lambdadivrho * dp1dxj[k][1];
    dtijdu2[0][1] = mudivrho * dp1dxj[k][0] + dmu[k][2] * (dudxj[1][0] + dudxj[0][1]);
    dtijdu2[0][2] = dmu[k][2] * (dudxj[2][0] + dudxj[0][2]);
    dtijdu2[1][0] = dtijdu2[0][1];
    dtijdu2[1][1] = 2.0 * dmu[k][2] * dudxj[1][1] + 2.0 * mudivrho * dp1dxj[k][1] + dlambda[k][2] * toto + lambdadivrho * dp1dxj[k][1];
    dtijdu2[1][2] = mudivrho * dp1dxj[k][2] + dmu[k][2] * (dudxj[2][1] + dudxj[1][2]);
    dtijdu2[2][0] = dtijdu2[0][2];
    dtijdu2[2][1] = dtijdu2[1][2];
    dtijdu2[2][2] = 2.0 * dmu[k][2] * dudxj[2][2] + dlambda[k][2] * toto + lambdadivrho * dp1dxj[k][1];

    dtijdu3[0][0] = 2.0 * dmu[k][3] * dudxj[0][0] + dlambda[k][3] * toto + lambdadivrho * dp1dxj[k][2];
    dtijdu3[0][1] = dmu[k][3] * (dudxj[1][0] + dudxj[0][1]);
    dtijdu3[0][2] = mudivrho * dp1dxj[k][0] + dmu[k][3] * (dudxj[2][0] + dudxj[0][2]);
    dtijdu3[1][0] = dtijdu3[0][1];
    dtijdu3[1][1] = 2.0 * dmu[k][3] * dudxj[1][1] + dlambda[k][3] * toto + lambdadivrho * dp1dxj[k][2];
    dtijdu3[1][2] = mudivrho * dp1dxj[k][1] + dmu[k][3] * (dudxj[2][1] + dudxj[1][2]);
    dtijdu3[2][0] = dtijdu3[0][2];
    dtijdu3[2][1] = dtijdu3[1][2];
    dtijdu3[2][2] = 2.0 * dmu[k][3] * dudxj[2][2] + 2.0 * mudivrho * dp1dxj[k][2] + dlambda[k][3] * toto + lambdadivrho * dp1dxj[k][2];

    dtijdu4[0][0] = 2.0 * dmu[k][4] * dudxj[0][0] + dlambda[k][4] * toto;
    dtijdu4[0][1] = dmu[k][4] * (dudxj[1][0] + dudxj[0][1]);
    dtijdu4[0][2] = dmu[k][4] * (dudxj[2][0] + dudxj[0][2]);
    dtijdu4[1][0] = dtijdu4[0][1];
    dtijdu4[1][1] = 2.0 * dmu[k][4] * dudxj[1][1] + dlambda[k][4] * toto;
    dtijdu4[1][2] = dmu[k][4] * (dudxj[2][1] + dudxj[1][2]);
    dtijdu4[2][0] = dtijdu4[0][2];
    dtijdu4[2][1] = dtijdu4[1][2];
    dtijdu4[2][2] = 2.0 * dmu[k][4] * dudxj[2][2] + dlambda[k][4] * toto;

    varFcn->computeJacobianTemperature(V[k], dTduk, tag);
    dTdu0kappa[k] = kappa * dTduk[0];
    dTdu1kappa[k] = kappa * dTduk[1];
    dTdu2kappa[k] = kappa * dTduk[2];
    dTdu3kappa[k] = kappa * dTduk[3];
    dTdu4kappa[k] = kappa * dTduk[4];
    dqjdu0[0] = -dkappa[k][0] * dTdxj[0] - dp1dxj[k][0] * dTdu0kappa[k];
    dqjdu1[0] = -dkappa[k][1] * dTdxj[0] - dp1dxj[k][0] * dTdu1kappa[k];
    dqjdu2[0] = -dkappa[k][2] * dTdxj[0] - dp1dxj[k][0] * dTdu2kappa[k];
    dqjdu3[0] = -dkappa[k][3] * dTdxj[0] - dp1dxj[k][0] * dTdu3kappa[k];
    dqjdu4[0] = -dkappa[k][4] * dTdxj[0] - dp1dxj[k][0] * dTdu4kappa[k];
    dqjdu0[1] = -dkappa[k][0] * dTdxj[1] - dp1dxj[k][1] * dTdu0kappa[k];
    dqjdu1[1] = -dkappa[k][1] * dTdxj[1] - dp1dxj[k][1] * dTdu1kappa[k];
    dqjdu2[1] = -dkappa[k][2] * dTdxj[1] - dp1dxj[k][1] * dTdu2kappa[k];
    dqjdu3[1] = -dkappa[k][3] * dTdxj[1] - dp1dxj[k][1] * dTdu3kappa[k];
    dqjdu4[1] = -dkappa[k][4] * dTdxj[1] - dp1dxj[k][1] * dTdu4kappa[k];
    dqjdu0[2] = -dkappa[k][0] * dTdxj[2] - dp1dxj[k][2] * dTdu0kappa[k];
    dqjdu1[2] = -dkappa[k][1] * dTdxj[2] - dp1dxj[k][2] * dTdu1kappa[k];
    dqjdu2[2] = -dkappa[k][2] * dTdxj[2] - dp1dxj[k][2] * dTdu2kappa[k];
    dqjdu3[2] = -dkappa[k][3] * dTdxj[2] - dp1dxj[k][2] * dTdu3kappa[k];
    dqjdu4[2] = -dkappa[k][4] * dTdxj[2] - dp1dxj[k][2] * dTdu4kappa[k];
    // computation of dRdU -> dRdU[k][a][b][c]
    // k is the corner of the tetrahedra we are considering
    // a is the coordinate direction: x y z
    // b is the equation we are considering: 0 for mass conservation
    //           1 for x-momentum
    //           2 for y-momentum
    //           3 for z-momentum
    //           4 for energy
    // c is the variable wrt which we derive
    // dRxdU
    dRdU[k][0][0][0] = 0.0;
    dRdU[k][0][0][1] = 0.0;
    dRdU[k][0][0][2] = 0.0;
    dRdU[k][0][0][3] = 0.0;
    dRdU[k][0][0][4] = 0.0;
    dRdU[k][0][1][0] = dtijdu0[0][0];
    dRdU[k][0][1][1] = dtijdu1[0][0];
    dRdU[k][0][1][2] = dtijdu2[0][0];
    dRdU[k][0][1][3] = dtijdu3[0][0];
    dRdU[k][0][1][4] = dtijdu4[0][0];
    dRdU[k][0][2][0] = dtijdu0[0][1];
    dRdU[k][0][2][1] = dtijdu1[0][1];
    dRdU[k][0][2][2] = dtijdu2[0][1];
    dRdU[k][0][2][3] = dtijdu3[0][1];
    dRdU[k][0][2][4] = dtijdu4[0][1];
    dRdU[k][0][3][0] = dtijdu0[0][2];
    dRdU[k][0][3][1] = dtijdu1[0][2];
    dRdU[k][0][3][2] = dtijdu2[0][2];
    dRdU[k][0][3][3] = dtijdu3[0][2];
    dRdU[k][0][3][4] = dtijdu4[0][2];
    dRdU[k][0][4][0] = nu * (ducgdu0[0] * tij[0][0] + ucg[0] * dtijdu0[0][0] + ducgdu0[1] * tij[0][1] + ucg[1] * dtijdu0[0][1] + ducgdu0[2] * tij[0][2] + ucg[2]
                       * dtijdu0[0][2]) + dnu[k][0] * (ucg[0] * tij[0][0] + ucg[1] * tij[1][0] + ucg[2] * tij[2][0]) - dqjdu0[0];
    dRdU[k][0][4][1] = nu * (ducgdu1[0] * tij[0][0] + ucg[0] * dtijdu1[0][0] + ucg[1] * dtijdu1[0][1] + ucg[2]
                       * dtijdu1[0][2]) + dnu[k][1] * (ucg[0] * tij[0][0] + ucg[1] * tij[1][0] + ucg[2] * tij[2][0]) - dqjdu1[0];
    dRdU[k][0][4][2] = nu * (ucg[0] * dtijdu2[0][0] + ducgdu2[1] * tij[0][1] + ucg[1] * dtijdu2[0][1] + ucg[2]
                       * dtijdu2[0][2]) + dnu[k][2] * (ucg[0] * tij[0][0] + ucg[1] * tij[1][0] + ucg[2] * tij[2][0]) - dqjdu2[0];
    dRdU[k][0][4][3] = nu * (ucg[0] * dtijdu3[0][0] + ucg[1] * dtijdu3[0][1] + ducgdu3[2] * tij[0][2] + ucg[2]
                       * dtijdu3[0][2]) + dnu[k][3] * (ucg[0] * tij[0][0] + ucg[1] * tij[1][0] + ucg[2] * tij[2][0]) - dqjdu3[0];
    dRdU[k][0][4][4] = nu * (ucg[0] * dtijdu4[0][0] + ucg[1] * dtijdu4[0][1] + ucg[2]
                       * dtijdu4[0][2]) + dnu[k][4] * (ucg[0] * tij[0][0] + ucg[1] * tij[1][0] + ucg[2] * tij[2][0]) - dqjdu4[0];
    // dRydU
    dRdU[k][1][0][0] = 0.0;
    dRdU[k][1][0][1] = 0.0;
    dRdU[k][1][0][2] = 0.0;
    dRdU[k][1][0][3] = 0.0;
    dRdU[k][1][0][4] = 0.0;
    dRdU[k][1][1][0] = dtijdu0[0][1];
    dRdU[k][1][1][1] = dtijdu1[0][1];
    dRdU[k][1][1][2] = dtijdu2[0][1];
    dRdU[k][1][1][3] = dtijdu3[0][1];
    dRdU[k][1][1][4] = dtijdu4[0][1];
    dRdU[k][1][2][0] = dtijdu0[1][1];
    dRdU[k][1][2][1] = dtijdu1[1][1];
    dRdU[k][1][2][2] = dtijdu2[1][1];
    dRdU[k][1][2][3] = dtijdu3[1][1];
    dRdU[k][1][2][4] = dtijdu4[1][1];
    dRdU[k][1][3][0] = dtijdu0[1][2];
    dRdU[k][1][3][1] = dtijdu1[1][2];
    dRdU[k][1][3][2] = dtijdu2[1][2];
    dRdU[k][1][3][3] = dtijdu3[1][2];
    dRdU[k][1][3][4] = dtijdu4[1][2];
    dRdU[k][1][4][0] = nu * (ducgdu0[0] * tij[0][1] + ucg[0] * dtijdu0[0][1] + ducgdu0[1] * tij[1][1] + ucg[1] * dtijdu0[1][1] + ducgdu0[2] * tij[1][2] + ucg[2]
                       * dtijdu0[1][2]) + dnu[k][0] * (ucg[0] * tij[0][1] + ucg[1] * tij[1][1] + ucg[2] * tij[2][1])  - dqjdu0[1];
    dRdU[k][1][4][1] = nu * (ducgdu1[0] * tij[0][1] + ucg[0] * dtijdu1[0][1] + ucg[1] * dtijdu1[1][1] + ucg[2]
                       * dtijdu1[1][2]) + dnu[k][1] * (ucg[0] * tij[0][1] + ucg[1] * tij[1][1] + ucg[2] * tij[2][1]) - dqjdu1[1];
    dRdU[k][1][4][2] = nu * (ucg[0] * dtijdu2[0][1] + ducgdu2[1] * tij[1][1] + ucg[1] * dtijdu2[1][1] + ucg[2]
                       * dtijdu2[1][2]) + dnu[k][2] * (ucg[0] * tij[0][1] + ucg[1] * tij[1][1] + ucg[2] * tij[2][1]) - dqjdu2[1];
    dRdU[k][1][4][3] = nu * (ucg[0] * dtijdu3[0][1] + ucg[1] * dtijdu3[1][1] + ducgdu3[2] * tij[1][2] + ucg[2]
                       * dtijdu3[1][2]) + dnu[k][3] * (ucg[0] * tij[0][1] + ucg[1] * tij[1][1] + ucg[2] * tij[2][1]) - dqjdu3[1];
    dRdU[k][1][4][4] = nu * (ucg[0] * dtijdu4[0][1] + ucg[1] * dtijdu4[1][1] + ucg[2]
                       * dtijdu4[1][2]) + dnu[k][4] * (ucg[0] * tij[0][1] + ucg[1] * tij[1][1] + ucg[2] * tij[2][1]) - dqjdu4[1];
    // dRzdU
    dRdU[k][2][0][0] = 0.0;
    dRdU[k][2][0][1] = 0.0;
    dRdU[k][2][0][2] = 0.0;
    dRdU[k][2][0][3] = 0.0;
    dRdU[k][2][0][4] = 0.0;
    dRdU[k][2][1][0] = dtijdu0[0][2];
    dRdU[k][2][1][1] = dtijdu1[0][2];
    dRdU[k][2][1][2] = dtijdu2[0][2];
    dRdU[k][2][1][3] = dtijdu3[0][2];
    dRdU[k][2][1][4] = dtijdu4[0][2];
    dRdU[k][2][2][0] = dtijdu0[1][2];
    dRdU[k][2][2][1] = dtijdu1[1][2];
    dRdU[k][2][2][2] = dtijdu2[1][2];
    dRdU[k][2][2][3] = dtijdu3[1][2];
    dRdU[k][2][2][4] = dtijdu4[1][2];
    dRdU[k][2][3][0] = dtijdu0[2][2];
    dRdU[k][2][3][1] = dtijdu1[2][2];
    dRdU[k][2][3][2] = dtijdu2[2][2];
    dRdU[k][2][3][3] = dtijdu3[2][2];
    dRdU[k][2][3][4] = dtijdu4[2][2];
    dRdU[k][2][4][0] = nu * (ducgdu0[0] * tij[0][2] + ucg[0] * dtijdu0[0][2] + ducgdu0[1] * tij[1][2] + ucg[1] * dtijdu0[1][2] + ducgdu0[2] * tij[2][2] + ucg[2]
                       * dtijdu0[2][2]) + dnu[k][0] * (ucg[0] * tij[0][2] + ucg[1] * tij[1][2] + ucg[2] * tij[2][2]) - dqjdu0[2];
    dRdU[k][2][4][1] = nu * (ducgdu1[0] * tij[0][2] + ucg[0] * dtijdu1[0][2] + ucg[1] * dtijdu1[1][2] + ucg[2]
                       * dtijdu1[2][2]) + dnu[k][1] * (ucg[0] * tij[0][2] + ucg[1] * tij[1][2] + ucg[2] * tij[2][2]) - dqjdu1[2];
    dRdU[k][2][4][2] = nu * (ucg[0] * dtijdu2[0][2] + ducgdu2[1] * tij[1][2] + ucg[1] * dtijdu2[1][2] + ucg[2]
                       * dtijdu2[2][2]) + dnu[k][2] * (ucg[0] * tij[0][2] + ucg[1] * tij[1][2] + ucg[2] * tij[2][2]) - dqjdu2[2];
    dRdU[k][2][4][3] = nu * (ucg[0] * dtijdu3[0][2] + ucg[1] * dtijdu3[1][2] + ducgdu3[2] * tij[2][2] + ucg[2]
                       * dtijdu3[2][2]) + dnu[k][3] * (ucg[0] * tij[0][2] + ucg[1] * tij[1][2] + ucg[2] * tij[2][2]) - dqjdu3[2];
    dRdU[k][2][4][4] = nu * (ucg[0] * dtijdu4[0][2] + ucg[1] * dtijdu4[1][2] + ucg[2]
                       * dtijdu4[2][2]) + dnu[k][4] * (ucg[0] * tij[0][2] + ucg[1] * tij[1][2] + ucg[2] * tij[2][2]) - dqjdu4[2];
  }
}

//------------------------------------------------------------------------------

template<int neq>
inline
void NavierStokesTerm::computeJacobianVolumeTermNSUQ(double dp1dxj[4][3], double mu, double lambda, double kappa,
                                                   double *V[4], double ucg[3], double dudxj[3][3], int tag,
                                                   double (*dRdU)[3][neq][neq], double RSM[3][3], double rhocg, double mul, double lambdal) {
  // for nu = 1, mu = constant, lambda = constant, kappa = constant
  double tij[3][3], dTduk[7]; // TODO: use dim for dTduk
  computeStressTensorUQ(mul, lambdal, dudxj, tij, RSM, rhocg);
  double& txx = tij[0][0];
  double& tyy = tij[1][1];
  double& tzz = tij[2][2];
  double& txy = tij[0][1];
  double& txz = tij[0][2];
  double& tyz = tij[1][2];
  for(int k = 0; k < 4; ++k) {
    double& rho = V[k][0];
    double& u = V[k][1];
    double& v = V[k][2];
    double& w = V[k][3];
    double& dp1dx = dp1dxj[k][0];
    double& dp1dy = dp1dxj[k][1];
    double& dp1dz = dp1dxj[k][2];
    double nu = mu / rho;
    double lu = lambda / rho;
    double dtxxdu0 = -(2.0 * nu * dp1dx * u + lu * (dp1dx * u + dp1dy * v + dp1dz * w));
    double dtxxdu1 = (2.0 * nu + lu) * dp1dx;
    double dtxxdu2 = lu * dp1dy;
    double dtxxdu3 = lu * dp1dz;
    double dtyydu0 = -(2.0 * nu * dp1dy * v + lu * (dp1dx * u + dp1dy * v + dp1dz * w));
    double dtyydu1 = lu * dp1dx;
    double dtyydu2 = (2.0 * nu + lu) * dp1dy;
    double dtyydu3 = lu * dp1dz;
    double dtzzdu0 = -(2.0 * nu * dp1dz * w + lu * (dp1dx * u + dp1dy * v + dp1dz * w));
    double dtzzdu1 = lu * dp1dx;
    double dtzzdu2 = lu * dp1dy;
    double dtzzdu3 = (2.0 * nu + lu) * dp1dz;
    double dtxydu0 = -nu * (dp1dx * v + dp1dy * u);
    double dtxydu1 = nu * dp1dy;
    double dtxydu2 = nu * dp1dx;
    double dtxzdu0 = -nu * (dp1dx * w + dp1dz * u);
    double dtxzdu1 = nu * dp1dz;
    double dtxzdu3 = nu * dp1dx;
    double dtyzdu0 = -nu * (dp1dy * w + dp1dz * v);
    double dtyzdu2 = nu * dp1dz;
    double dtyzdu3 = nu * dp1dy;
    varFcn->computeJacobianTemperature(V[k], dTduk, tag);
    double c0 = -kappa * dTduk[0];
    double c1 = -kappa * dTduk[1];
    double c2 = -kappa * dTduk[2];
    double c3 = -kappa * dTduk[3];
    double c4 = -kappa * dTduk[4];
    double dqxdu0 = c0 * dp1dx;
    double dqxdu1 = c1 * dp1dx;
    double dqxdu2 = c2 * dp1dx;
    double dqxdu3 = c3 * dp1dx;
    double dqxdu4 = c4 * dp1dx;
    double dqydu0 = c0 * dp1dy;
    double dqydu1 = c1 * dp1dy;
    double dqydu2 = c2 * dp1dy;
    double dqydu3 = c3 * dp1dy;
    double dqydu4 = c4 * dp1dy;
    double dqzdu0 = c0 * dp1dz;
    double dqzdu1 = c1 * dp1dz;
    double dqzdu2 = c2 * dp1dz;
    double dqzdu3 = c3 * dp1dz;
    double dqzdu4 = c4 * dp1dz;
    double beta = fourth / rho;
    double dudu0 = -beta * u;
    double dudu1 = beta;
    double dvdu0 = -beta * v;
    double dvdu2 = beta;
    double dwdu0 = -beta * w;
    double dwdu3 = beta;
    // computation of dRdU -> dRdU[k][a][b][c]
    // k is the corner of the tetrahedra we are considering
    // a is the coordinate direction: x y z
    // b is the equation we are considering: 0 for mass conservation
    //           1 for x-momentum
    //           2 for y-momentum
    //           3 for z-momentum
    //           4 for energy
    // c is the variable wrt which we derive
    // dRxdU
    dRdU[k][0][0][0] = 0.0;
    dRdU[k][0][0][1] = 0.0;
    dRdU[k][0][0][2] = 0.0;
    dRdU[k][0][0][3] = 0.0;
    dRdU[k][0][0][4] = 0.0;
    dRdU[k][0][1][0] = dtxxdu0;
    dRdU[k][0][1][1] = dtxxdu1;
    dRdU[k][0][1][2] = dtxxdu2;
    dRdU[k][0][1][3] = dtxxdu3;
    dRdU[k][0][1][4] = 0.0;
    dRdU[k][0][2][0] = dtxydu0;
    dRdU[k][0][2][1] = dtxydu1;
    dRdU[k][0][2][2] = dtxydu2;
    dRdU[k][0][2][3] = 0.0;
    dRdU[k][0][2][4] = 0.0;
    dRdU[k][0][3][0] = dtxzdu0;
    dRdU[k][0][3][1] = dtxzdu1;
    dRdU[k][0][3][2] = 0.0;
    dRdU[k][0][3][3] = dtxzdu3;
    dRdU[k][0][3][4] = 0.0;
    dRdU[k][0][4][0] = dudu0 * txx + ucg[0] * dtxxdu0 + dvdu0 * txy + ucg[1] * dtxydu0 +
                       dwdu0 * txz + ucg[2] * dtxzdu0 - dqxdu0;
    dRdU[k][0][4][1] = dudu1 * txx + ucg[0] * dtxxdu1 +
                       ucg[1] * dtxydu1 + ucg[2] * dtxzdu1 - dqxdu1;
    dRdU[k][0][4][2] = ucg[0] * dtxxdu2 + dvdu2 * txy + ucg[1] * dtxydu2 - dqxdu2;
    dRdU[k][0][4][3] = ucg[0] * dtxxdu3 + dwdu3 * txz + ucg[2] * dtxzdu3 - dqxdu3;
    dRdU[k][0][4][4] = -dqxdu4;
    // dRydU
    dRdU[k][1][0][0] = 0.0;
    dRdU[k][1][0][1] = 0.0;
    dRdU[k][1][0][2] = 0.0;
    dRdU[k][1][0][3] = 0.0;
    dRdU[k][1][0][4] = 0.0;
    dRdU[k][1][1][0] = dtxydu0;
    dRdU[k][1][1][1] = dtxydu1;
    dRdU[k][1][1][2] = dtxydu2;
    dRdU[k][1][1][3] = 0.0;
    dRdU[k][1][1][4] = 0.0;
    dRdU[k][1][2][0] = dtyydu0;
    dRdU[k][1][2][1] = dtyydu1;
    dRdU[k][1][2][2] = dtyydu2;
    dRdU[k][1][2][3] = dtyydu3;
    dRdU[k][1][2][4] = 0.0;
    dRdU[k][1][3][0] = dtyzdu0;
    dRdU[k][1][3][1] = 0.0;
    dRdU[k][1][3][2] = dtyzdu2;
    dRdU[k][1][3][3] = dtyzdu3;
    dRdU[k][1][3][4] = 0.0;
    dRdU[k][1][4][0] = dudu0 * txy + ucg[0] * dtxydu0 + dvdu0 * tyy + ucg[1] * dtyydu0 +
                       dwdu0 * tyz + ucg[2] * dtyzdu0 - dqydu0;
    dRdU[k][1][4][1] = dudu1 * txy + ucg[0] * dtxydu1 + ucg[1] * dtyydu1 - dqydu1;
    dRdU[k][1][4][2] = ucg[0] * dtxydu2 + dvdu2 * tyy +
                       ucg[1] * dtyydu2 + ucg[2] * dtyzdu2 - dqydu2;
    dRdU[k][1][4][3] = ucg[1] * dtyydu3 + dwdu3 * tyz + ucg[2] * dtyzdu3 - dqydu3;
    dRdU[k][1][4][4] = -dqydu4;
    // dRzdU
    dRdU[k][2][0][0] = 0.0;
    dRdU[k][2][0][1] = 0.0;
    dRdU[k][2][0][2] = 0.0;
    dRdU[k][2][0][3] = 0.0;
    dRdU[k][2][0][4] = 0.0;
    dRdU[k][2][1][0] = dtxzdu0;
    dRdU[k][2][1][1] = dtxzdu1;
    dRdU[k][2][1][2] = 0.0;
    dRdU[k][2][1][3] = dtxzdu3;
    dRdU[k][2][1][4] = 0.0;
    dRdU[k][2][2][0] = dtyzdu0;
    dRdU[k][2][2][1] = 0.0;
    dRdU[k][2][2][2] = dtyzdu2;
    dRdU[k][2][2][3] = dtyzdu3;
    dRdU[k][2][2][4] = 0.0;
    dRdU[k][2][3][0] = dtzzdu0;
    dRdU[k][2][3][1] = dtzzdu1;
    dRdU[k][2][3][2] = dtzzdu2;
    dRdU[k][2][3][3] = dtzzdu3;
    dRdU[k][2][3][4] = 0.0;
    dRdU[k][2][4][0] = dudu0 * txz + ucg[0] * dtxzdu0 + dvdu0 * tyz + ucg[1] * dtyzdu0 +
                       dwdu0 * tzz + ucg[2] * dtzzdu0 - dqzdu0;
    dRdU[k][2][4][1] = dudu1 * txz + ucg[0] * dtxzdu1 + ucg[2] * dtzzdu1 - dqzdu1;
    dRdU[k][2][4][2] = dvdu2 * tyz + ucg[1] * dtyzdu2 + ucg[2] * dtzzdu2 - dqzdu2;
    dRdU[k][2][4][3] = ucg[0] * dtxzdu3 + ucg[1] * dtyzdu3 +
                       dwdu3 * tzz + ucg[2] * dtzzdu3 - dqzdu3;
    dRdU[k][2][4][4] = -dqzdu4;
  }
}

//------------------------------------------------------------------------------

template<int neq>
inline
void NavierStokesTerm::computeJacobianVolumeTermNSUQ(double dp1dxj[4][3], double mu, double dmu[4][neq],
                                                   double lambda, double dlambda[4][neq], double kappa, double dkappa[4][neq],
                                                   double *V[4], double ucg[3], double dudxj[3][3],
                                                   int tag, double dTdxj[3], double (*dRdU)[3][neq][neq], double RSM[3][3], double rhocg, double mul, double lambdal) {
  // for nu = 1
  double tij[3][3];
  computeStressTensorUQ(mul, lambdal, dudxj, tij, RSM, rhocg);
  double dtijdu0[3][3];
  double dtijdu1[3][3];
  double dtijdu2[3][3];
  double dtijdu3[3][3];
  double dtijdu4[3][3];
  double ducgdu0[3];
  double ducgdu1[3];
  double ducgdu2[3];
  double ducgdu3[3];
  double ducgdu4[3];
  double dTduk[7]; // TODO: use dim for dTduk
  double dTdu0kappa[4];
  double dTdu1kappa[4];
  double dTdu2kappa[4];
  double dTdu3kappa[4];
  double dTdu4kappa[4];
  double dqjdu0[3];
  double dqjdu1[3];
  double dqjdu2[3];
  double dqjdu3[3];
  double dqjdu4[3];

  const double toto = dudxj[0][0] + dudxj[1][1] + dudxj[2][2];

  for(int k = 0; k < 4; ++k) {
    const double invrho = 1. / V[k][0];
    const double mudivrho = mu * invrho;
    const double lambdadivrho = lambda * invrho;
    const double kappadivrho = kappa * invrho;

    ducgdu0[0] = -fourth * V[k][1] * invrho;
    ducgdu0[1] = -fourth * V[k][2] * invrho;
    ducgdu0[2] = -fourth * V[k][3] * invrho;
    ducgdu1[0] = fourth * invrho;
    ducgdu2[1] = fourth * invrho;
    ducgdu3[2] = fourth * invrho;

    dtijdu0[0][0] = 2.0 * dmu[k][0] * dudxj[0][0] - 2.0 * mudivrho * dp1dxj[k][0] * V[k][1] + dlambda[k][0] *
                    toto - lambdadivrho * (dp1dxj[k][0] * V[k][1] + dp1dxj[k][1] * V[k][2] + dp1dxj[k][2] * V[k][3]);
    dtijdu0[0][1] = -mudivrho * (dp1dxj[k][0] * V[k][2] + dp1dxj[k][1] * V[k][1]) + dmu[k][0] * (dudxj[1][0] + dudxj[0][1]);
    dtijdu0[0][2] = -mudivrho * (dp1dxj[k][0] * V[k][3] + dp1dxj[k][2] * V[k][1]) + dmu[k][0] * (dudxj[2][0] + dudxj[0][2]);
    dtijdu0[1][0] = dtijdu0[0][1];
    dtijdu0[1][1] = 2.0 * dmu[k][0] * dudxj[1][1] - 2.0 * mudivrho * dp1dxj[k][1] * V[k][2] + dlambda[k][0] *
                    toto - lambdadivrho * (dp1dxj[k][1] * V[k][2] + dp1dxj[k][0] * V[k][1] + dp1dxj[k][2] * V[k][3]);
    dtijdu0[1][2] = -mudivrho * (dp1dxj[k][1] * V[k][3] + dp1dxj[k][2] * V[k][2]) + dmu[k][0] * (dudxj[2][1] + dudxj[1][2]);
    dtijdu0[2][0] = dtijdu0[0][2];
    dtijdu0[2][1] = dtijdu0[1][2];
    dtijdu0[2][2] = 2.0 * dmu[k][0] * dudxj[2][2] - 2.0 * mudivrho * dp1dxj[k][2] * V[k][3] + dlambda[k][0] *
                    (toto) - lambdadivrho * (dp1dxj[k][2] * V[k][3] + dp1dxj[k][0] * V[k][1] + dp1dxj[k][1] * V[k][2]);

    dtijdu1[0][0] = 2.0 * dmu[k][1] * dudxj[0][0] + 2.0 * mudivrho * dp1dxj[k][0] + dlambda[k][1] * toto + lambdadivrho * dp1dxj[k][0];
    dtijdu1[0][1] = mudivrho * dp1dxj[k][1] + dmu[k][1] * (dudxj[1][0] + dudxj[0][1]);
    dtijdu1[0][2] = mudivrho * dp1dxj[k][2] + dmu[k][1] * (dudxj[2][0] + dudxj[0][2]);
    dtijdu1[1][0] = dtijdu1[0][1];
    dtijdu1[1][1] = 2.0 * dmu[k][1] * dudxj[1][1] + dlambda[k][1] * toto + lambdadivrho * dp1dxj[k][0];
    dtijdu1[1][2] = dmu[k][1] * (dudxj[2][1] + dudxj[1][2]);
    dtijdu1[2][0] = dtijdu1[0][2];
    dtijdu1[2][1] = dtijdu1[1][2];
    dtijdu1[2][2] = 2.0 * dmu[k][1] * dudxj[2][2] + dlambda[k][1] * toto + lambdadivrho * dp1dxj[k][0];

    dtijdu2[0][0] = 2.0 * dmu[k][2] * dudxj[0][0] + dlambda[k][2] * toto + lambdadivrho * dp1dxj[k][1];
    dtijdu2[0][1] = mudivrho * dp1dxj[k][0] + dmu[k][2] * (dudxj[1][0] + dudxj[0][1]);
    dtijdu2[0][2] = dmu[k][2] * (dudxj[2][0] + dudxj[0][2]);
    dtijdu2[1][0] = dtijdu2[0][1];
    dtijdu2[1][1] = 2.0 * dmu[k][2] * dudxj[1][1] + 2.0 * mudivrho * dp1dxj[k][1] + dlambda[k][2] * toto + lambdadivrho
                    * dp1dxj[k][1];
    dtijdu2[1][2] = mudivrho * dp1dxj[k][2] + dmu[k][2] * (dudxj[2][1] + dudxj[1][2]);
    dtijdu2[2][0] = dtijdu2[0][2];
    dtijdu2[2][1] = dtijdu2[1][2];
    dtijdu2[2][2] = 2.0 * dmu[k][2] * dudxj[2][2] + dlambda[k][2] * toto + lambdadivrho * dp1dxj[k][1];

    dtijdu3[0][0] = 2.0 * dmu[k][3] * dudxj[0][0] + dlambda[k][3] * toto + lambdadivrho * dp1dxj[k][2];
    dtijdu3[0][1] = dmu[k][3] * (dudxj[1][0] + dudxj[0][1]);
    dtijdu3[0][2] = mudivrho * dp1dxj[k][0] + dmu[k][3] * (dudxj[2][0] + dudxj[0][2]);
    dtijdu3[1][0] = dtijdu3[0][1];
    dtijdu3[1][1] = 2.0 * dmu[k][3] * dudxj[1][1] + dlambda[k][3] * toto + lambdadivrho * dp1dxj[k][2];
    dtijdu3[1][2] = mudivrho * dp1dxj[k][1] + dmu[k][3] * (dudxj[2][1] + dudxj[1][2]);
    dtijdu3[2][0] = dtijdu3[0][2];
    dtijdu3[2][1] = dtijdu3[1][2];
    dtijdu3[2][2] = 2.0 * dmu[k][3] * dudxj[2][2] + 2.0 * mudivrho * dp1dxj[k][2] + dlambda[k][3] * toto + lambdadivrho * dp1dxj[k][2];

    dtijdu4[0][0] = 2.0 * dmu[k][4] * dudxj[0][0] + dlambda[k][4] * toto;
    dtijdu4[0][1] = dmu[k][4] * (dudxj[1][0] + dudxj[0][1]);
    dtijdu4[0][2] = dmu[k][4] * (dudxj[2][0] + dudxj[0][2]);
    dtijdu4[1][0] = dtijdu4[0][1];
    dtijdu4[1][1] = 2.0 * dmu[k][4] * dudxj[1][1] + dlambda[k][4] * toto;
    dtijdu4[1][2] = dmu[k][4] * (dudxj[2][1] + dudxj[1][2]);
    dtijdu4[2][0] = dtijdu4[0][2];
    dtijdu4[2][1] = dtijdu4[1][2];
    dtijdu4[2][2] = 2.0 * dmu[k][4] * dudxj[2][2] + dlambda[k][4] * toto;

    varFcn->computeJacobianTemperature(V[k], dTduk, tag);
    dTdu0kappa[k] = kappa * dTduk[0];
    dTdu1kappa[k] = kappa * dTduk[1];
    dTdu2kappa[k] = kappa * dTduk[2];
    dTdu3kappa[k] = kappa * dTduk[3];
    dTdu4kappa[k] = kappa * dTduk[4];
    dqjdu0[0] = -dkappa[k][0] * dTdxj[0] - dp1dxj[k][0] * dTdu0kappa[k];
    dqjdu1[0] = -dkappa[k][1] * dTdxj[0] - dp1dxj[k][0] * dTdu1kappa[k];
    dqjdu2[0] = -dkappa[k][2] * dTdxj[0] - dp1dxj[k][0] * dTdu2kappa[k];
    dqjdu3[0] = -dkappa[k][3] * dTdxj[0] - dp1dxj[k][0] * dTdu3kappa[k];
    dqjdu4[0] = -dkappa[k][4] * dTdxj[0] - dp1dxj[k][0] * dTdu4kappa[k];
    dqjdu0[1] = -dkappa[k][0] * dTdxj[1] - dp1dxj[k][1] * dTdu0kappa[k];
    dqjdu1[1] = -dkappa[k][1] * dTdxj[1] - dp1dxj[k][1] * dTdu1kappa[k];
    dqjdu2[1] = -dkappa[k][2] * dTdxj[1] - dp1dxj[k][1] * dTdu2kappa[k];
    dqjdu3[1] = -dkappa[k][3] * dTdxj[1] - dp1dxj[k][1] * dTdu3kappa[k];
    dqjdu4[1] = -dkappa[k][4] * dTdxj[1] - dp1dxj[k][1] * dTdu4kappa[k];
    dqjdu0[2] = -dkappa[k][0] * dTdxj[2] - dp1dxj[k][2] * dTdu0kappa[k];
    dqjdu1[2] = -dkappa[k][1] * dTdxj[2] - dp1dxj[k][2] * dTdu1kappa[k];
    dqjdu2[2] = -dkappa[k][2] * dTdxj[2] - dp1dxj[k][2] * dTdu2kappa[k];
    dqjdu3[2] = -dkappa[k][3] * dTdxj[2] - dp1dxj[k][2] * dTdu3kappa[k];
    dqjdu4[2] = -dkappa[k][4] * dTdxj[2] - dp1dxj[k][2] * dTdu4kappa[k];
    // dRxdU
    dRdU[k][0][0][0] = 0.0;
    dRdU[k][0][0][1] = 0.0;
    dRdU[k][0][0][2] = 0.0;
    dRdU[k][0][0][3] = 0.0;
    dRdU[k][0][0][4] = 0.0;
    dRdU[k][0][1][0] = dtijdu0[0][0];
    dRdU[k][0][1][1] = dtijdu1[0][0];
    dRdU[k][0][1][2] = dtijdu2[0][0];
    dRdU[k][0][1][3] = dtijdu3[0][0];
    dRdU[k][0][1][4] = dtijdu4[0][0];
    dRdU[k][0][2][0] = dtijdu0[0][1];
    dRdU[k][0][2][1] = dtijdu1[0][1];
    dRdU[k][0][2][2] = dtijdu2[0][1];
    dRdU[k][0][2][3] = dtijdu3[0][1];
    dRdU[k][0][2][4] = dtijdu4[0][1];
    dRdU[k][0][3][0] = dtijdu0[0][2];
    dRdU[k][0][3][1] = dtijdu1[0][2];
    dRdU[k][0][3][2] = dtijdu2[0][2];
    dRdU[k][0][3][3] = dtijdu3[0][2];
    dRdU[k][0][3][4] = dtijdu4[0][2];
    dRdU[k][0][4][0] = ducgdu0[0] * tij[0][0] + ucg[0] * dtijdu0[0][0] + ducgdu0[1] * tij[0][1] + ucg[1] * dtijdu0[0][1] + ducgdu0[2] * tij[0][2] + ucg[2]
                       * dtijdu0[0][2] - dqjdu0[0];
    dRdU[k][0][4][1] = ducgdu1[0] * tij[0][0] + ucg[0] * dtijdu1[0][0] + ucg[1] * dtijdu1[0][1] + ucg[2] * dtijdu1[0][2] - dqjdu1[0];
    dRdU[k][0][4][2] = ucg[0] * dtijdu2[0][0] + ducgdu2[1] * tij[0][1] + ucg[1] * dtijdu2[0][1] + ucg[2] * dtijdu2[0][2] - dqjdu2[0];
    dRdU[k][0][4][3] = ucg[0] * dtijdu3[0][0] + ucg[1] * dtijdu3[0][1] + ducgdu3[2] * tij[0][2] + ucg[2] * dtijdu3[0][2] - dqjdu3[0];
    dRdU[k][0][4][4] = ucg[0] * dtijdu4[0][0] + ucg[1] * dtijdu4[0][1] + ucg[2] * dtijdu4[0][2] - dqjdu4[0];
    // dRydU
    dRdU[k][1][0][0] = 0.0;
    dRdU[k][1][0][1] = 0.0;
    dRdU[k][1][0][2] = 0.0;
    dRdU[k][1][0][3] = 0.0;
    dRdU[k][1][0][4] = 0.0;
    dRdU[k][1][1][0] = dtijdu0[0][1];
    dRdU[k][1][1][1] = dtijdu1[0][1];
    dRdU[k][1][1][2] = dtijdu2[0][1];
    dRdU[k][1][1][3] = dtijdu3[0][1];
    dRdU[k][1][1][4] = dtijdu4[0][1];
    dRdU[k][1][2][0] = dtijdu0[1][1];
    dRdU[k][1][2][1] = dtijdu1[1][1];
    dRdU[k][1][2][2] = dtijdu2[1][1];
    dRdU[k][1][2][3] = dtijdu3[1][1];
    dRdU[k][1][2][4] = dtijdu4[1][1];
    dRdU[k][1][3][0] = dtijdu0[1][2];
    dRdU[k][1][3][1] = dtijdu1[1][2];
    dRdU[k][1][3][2] = dtijdu2[1][2];
    dRdU[k][1][3][3] = dtijdu3[1][2];
    dRdU[k][1][3][4] = dtijdu4[1][2];
    dRdU[k][1][4][0] = ducgdu0[0] * tij[0][1] + ucg[0] * dtijdu0[0][1] + ducgdu0[1] * tij[1][1] + ucg[1] * dtijdu0[1][1] + ducgdu0[2] * tij[1][2] + ucg[2]
                       * dtijdu0[1][2] - dqjdu0[1];
    dRdU[k][1][4][1] = ducgdu1[0] * tij[0][1] + ucg[0] * dtijdu1[0][1] + ucg[1] * dtijdu1[1][1] + ucg[2] * dtijdu1[1][2] - dqjdu1[1];
    dRdU[k][1][4][2] = ucg[0] * dtijdu2[0][1] + ducgdu2[1] * tij[1][1] + ucg[1] * dtijdu2[1][1] + ucg[2] * dtijdu2[1][2] - dqjdu2[1];
    dRdU[k][1][4][3] = ucg[0] * dtijdu3[0][1] + ucg[1] * dtijdu3[1][1] + ducgdu3[2] * tij[1][2] + ucg[2] * dtijdu3[1][2] - dqjdu3[1];
    dRdU[k][1][4][4] = ucg[0] * dtijdu4[0][1] + ucg[1] * dtijdu4[1][1] + ucg[2] * dtijdu4[1][2] - dqjdu4[1];
    // dRzdU
    dRdU[k][2][0][0] = 0.0;
    dRdU[k][2][0][1] = 0.0;
    dRdU[k][2][0][2] = 0.0;
    dRdU[k][2][0][3] = 0.0;
    dRdU[k][2][0][4] = 0.0;
    dRdU[k][2][1][0] = dtijdu0[0][2];
    dRdU[k][2][1][1] = dtijdu1[0][2];
    dRdU[k][2][1][2] = dtijdu2[0][2];
    dRdU[k][2][1][3] = dtijdu3[0][2];
    dRdU[k][2][1][4] = dtijdu4[0][2];
    dRdU[k][2][2][0] = dtijdu0[1][2];
    dRdU[k][2][2][1] = dtijdu1[1][2];
    dRdU[k][2][2][2] = dtijdu2[1][2];
    dRdU[k][2][2][3] = dtijdu3[1][2];
    dRdU[k][2][2][4] = dtijdu4[1][2];
    dRdU[k][2][3][0] = dtijdu0[2][2];
    dRdU[k][2][3][1] = dtijdu1[2][2];
    dRdU[k][2][3][2] = dtijdu2[2][2];
    dRdU[k][2][3][3] = dtijdu3[2][2];
    dRdU[k][2][3][4] = dtijdu4[2][2];
    dRdU[k][2][4][0] = ducgdu0[0] * tij[0][2] + ucg[0] * dtijdu0[0][2] + ducgdu0[1] * tij[1][2] + ucg[1] * dtijdu0[1][2] + ducgdu0[2] * tij[2][2] + ucg[2]
                       * dtijdu0[2][2] - dqjdu0[2];
    dRdU[k][2][4][1] = ducgdu1[0] * tij[0][2] + ucg[0] * dtijdu1[0][2] + ucg[1] * dtijdu1[1][2] + ucg[2] * dtijdu1[2][2] - dqjdu1[2];
    dRdU[k][2][4][2] = ucg[0] * dtijdu2[0][2] + ducgdu2[1] * tij[1][2] + ucg[1] * dtijdu2[1][2] + ucg[2] * dtijdu2[2][2] - dqjdu2[2];
    dRdU[k][2][4][3] = ucg[0] * dtijdu3[0][2] + ucg[1] * dtijdu3[1][2] + ducgdu3[2] * tij[2][2] + ucg[2] * dtijdu3[2][2] - dqjdu3[2];
    dRdU[k][2][4][4] = ucg[0] * dtijdu4[0][2] + ucg[1] * dtijdu4[1][2] + ucg[2] * dtijdu4[2][2] - dqjdu4[2];
  }
}

//------------------------------------------------------------------------------
inline
void NavierStokesTerm::multiplyFiveMatrices3x3(double A[3][3], double B[3][3], double C[3][3], double D[3][3], double E[3][3], double returnMat[3][3]) {
  double tempMat1[3][3] = {};
  double tempMat2[3][3] = {};
  double tempMat1and2[3][3] = {};
  for(int i1 = 0; i1 < 3; ++i1) {
    for(int i2 = 0; i2 < 3; ++i2) {
      for(int i3 = 0; i3 < 3; ++i3) {
        tempMat1[i1][i2] += A[i1][i3] * B[i3][i2];
        tempMat2[i1][i2] += C[i1][i3] * D[i3][i2];
      }
    }
  }
  for(int i1 = 0; i1 < 3; ++i1) {
    for(int i2 = 0; i2 < 3; ++i2) {
      for(int i3 = 0; i3 < 3; ++i3) {
        tempMat1and2[i1][i2] += tempMat1[i1][i3] * tempMat2[i3][i2];
      }
    }
  }
  for(int i1 = 0; i1 < 3; ++i1) {
    for(int i2 = 0; i2 < 3; ++i2) {
      for(int i3 = 0; i3 < 3; ++i3) {
       returnMat[i1][i2] += tempMat1and2[i1][i3] * E[i3][i2];
      }
    }
  }
}

//------------------------------------------------------------------------------
template<>
inline
void NavierStokesTerm::computeJacobianVolumeTermNSUQ_SRST(double dp1dxj[4][3], double mul, double dmul[4][6], double mut, double dmut[4][6], 
                                                   double mu, double dmu[4][6], double lambdal, double dlambdal[4][6], double lambdat, double dlambdat[4][6],
                                                   double lambda, double dlambda[4][6], double kappa, double dkappa[4][6], double *V[4], double ucg[3], 
                                                   double dudxj[3][3], int tag, double dTdxj[3], double (*dRdU)[3][6][6], double RSM[3][3], double rhocg,
                                                   double TKE, double eigvalVect[3]) {
  double taut_rlx = 0.1;
  double tij[3][3];
  computeStressTensorUQ(mul, lambdal, dudxj, tij, RSM, rhocg);
  double dtijdu0[3][3], dtijdu1[3][3], dtijdu2[3][3], dtijdu3[3][3], dtijdu4[3][3], dtijdu5[3][3];
  double dtijdu0_term1[3][3], dtijdu0_term3[3][3], dtijdu1_term3[3][3], dtijdu2_term3[3][3], dtijdu3_term3[3][3], dtijdu4_term3[3][3], dtijdu5_term3[3][3];
  double dtijdu0_term2[3][3], dtijdu1_term2[3][3], dtijdu2_term2[3][3], dtijdu3_term2[3][3], dtijdu4_term2[3][3], dtijdu5_term2[3][3];
  double ducgdu0[3], ducgdu1[3], ducgdu2[3], ducgdu3[3], ducgdu4[3], ducgdu5[3];
  double dTduk[6];
  double dTdu0[4], dTdu1[4], dTdu2[4], dTdu3[4], dTdu4[4], dTdu5[4];
  double ddTdxjdu0[3], ddTdxjdu1[3], ddTdxjdu2[3], ddTdxjdu3[3], ddTdxjdu4[3], ddTdxjdu5[3];
  double dqjdu0[3], dqjdu1[3], dqjdu2[3], dqjdu3[3], dqjdu4[3], dqjdu5[3];
  // recompute A
  double A[3][3];
  for(int i=0; i < 3; ++i) {
    for(int j=0; j < 3; ++j) {
      auto delta_ij = (i == j)? 1.0 : 0.0;
      A[i][j] = RSM[i][j] / (2.0 * TKE) - delta_ij / 3.0;
    }
  }
  // Compute S
  double S[3][3], dS[3][3][4][6] = {};
  S[0][0] = dudxj[0][0];
  S[1][1] = dudxj[1][1];
  S[2][2] = dudxj[2][2];
  S[0][1] = 0.5 * (dudxj[0][1] + dudxj[1][0]);
  S[0][2] = 0.5 * (dudxj[0][2] + dudxj[2][0]);
  S[1][2] = 0.5 * (dudxj[1][2] + dudxj[2][1]);
  S[1][0] = S[0][1];
  S[2][0] = S[0][2];
  S[2][1] = S[1][2];
  for(int k = 0; k < 4; ++k) {
    dS[0][0][k][0] = -1.0 * dp1dxj[k][0] * V[k][1] / V[k][0];
    dS[0][0][k][1] = dp1dxj[k][0] / V[k][0];
    dS[1][1][k][0] = -1.0 * dp1dxj[k][1] * V[k][2] / V[k][0];
    dS[1][1][k][2] = dp1dxj[k][1] / V[k][0];
    dS[2][2][k][0] = -1.0 * dp1dxj[k][2] * V[k][3] / V[k][0];
    dS[2][2][k][3] = dp1dxj[k][2] / V[k][0];
    dS[0][1][k][0] = -0.5 * (dp1dxj[k][1] * V[k][1] + dp1dxj[k][0] * V[k][2]) / V[k][0];
    dS[0][1][k][1] = 0.5 * dp1dxj[k][1] / V[k][0];
    dS[0][1][k][2] = 0.5 * dp1dxj[k][0] / V[k][0];
    dS[0][2][k][0] = -0.5 * (dp1dxj[k][2] * V[k][1] + dp1dxj[k][0] * V[k][3]) / V[k][0];
    dS[0][2][k][1] = 0.5 * dp1dxj[k][2] / V[k][0];
    dS[0][2][k][3] = 0.5 * dp1dxj[k][0] / V[k][0];
    dS[1][2][k][0] = -0.5 * (dp1dxj[k][2] * V[k][2] + dp1dxj[k][1] * V[k][3]) / V[k][0];
    dS[1][2][k][2] = 0.5 * dp1dxj[k][2] / V[k][0];
    dS[1][2][k][3] = 0.5 * dp1dxj[k][1] / V[k][0];
    dS[1][0][k][0] = dS[0][1][k][0];
    dS[1][0][k][1] = dS[0][1][k][1];
    dS[1][0][k][2] = dS[0][1][k][2];
    dS[2][0][k][0] = dS[0][2][k][0];
    dS[2][0][k][1] = dS[0][2][k][1];
    dS[2][0][k][3] = dS[0][2][k][3];
    dS[2][1][k][0] = dS[1][2][k][0];
    dS[2][1][k][2] = dS[1][2][k][2];
    dS[2][1][k][3] = dS[1][2][k][3];
  }
  // Spectral decomp S
  double eigvalsS[3], eigvectsS[3][3];
  computeSpectralDecompTensor(S, eigvalsS, eigvectsS);
  // EJ Compute dTKE and dA  ----------------------------------------------------------
      // dTKE[node index 1 to 4][derivative wrt, 0 is density or du0 to 5 as du5]
  double deigvalsS[3][4][6], deigvectsS[3][3][4][6], dTKE[4][6], dA[3][3][4][6];
  for(int k = 0; k < 4; ++k) {
    for(int uInc = 0; uInc < 6; ++uInc) {
      for(int index = 0; index < 3; ++index) {
        double sum = 0, tempVect[3] = {}, matA[3][3] = {}, vectB[3] = {};
        for(int i = 0; i < 3; i++) {
          for(int j = 0; j < 3; j++) {
            auto delta_ij = (i == j)? 1.0 : 0.0;
            sum += eigvectsS[i][index] * eigvectsS[j][index] * dS[i][j][k][uInc];
            matA[i][j] = eigvalsS[index] * delta_ij - S[i][j];
            vectB[i] += dS[i][j][k][uInc] * eigvectsS[j][index];
          }
        }
        Eigen::Map<Eigen::Matrix3d> aEig(&matA[0][0], 3, 3);
        Eigen::Map<Eigen::Vector3d> bEig(&vectB[0], 3);
        const Eigen::Vector3d& tempEig = aEig.fullPivHouseholderQr().solve(bEig);
        Eigen::Map<Eigen::Vector3d>(&tempVect[0], 3) = tempEig; 
        deigvalsS[index][k][uInc] = sum;
        deigvectsS[0][index][k][uInc] = tempVect[0];
        deigvectsS[1][index][k][uInc] = tempVect[1];
        deigvectsS[2][index][k][uInc] = tempVect[2];
      } //index
      // ioData.com->fprintf(stdout, "S     = [%e, %e, %e; %e, %e, %e; %e, %e, %e];\n", S[0][0], S[0][1], S[0][2], S[1][0], S[1][1], S[1][2], S[2][0], S[2][1], S[2][2]);
      // ioData.com->fprintf(stdout, "Vals  = [%e, %e, %e];\n", eigvalsS[0],eigvalsS[1],eigvalsS[2]);
      // ioData.com->fprintf(stdout, "Vects = [%e, %e, %e; %e, %e, %e; %e, %e, %e];\n", eigvectsS[0][0], eigvectsS[0][1], eigvectsS[0][2], eigvectsS[1][0], eigvectsS[1][1], eigvectsS[1][2], eigvectsS[2][0], eigvectsS[2][1], eigvectsS[2][2]);
      // ioData.com->fprintf(stdout, "dS        = [%e, %e, %e; %e, %e, %e; %e, %e, %e];\n", dS[0][0][k][uInc], dS[0][1][k][uInc], dS[0][2][k][uInc], dS[1][0][k][uInc], dS[1][1][k][uInc], dS[1][2][k][uInc], dS[2][0][k][uInc], dS[2][1][k][uInc], dS[2][2][k][uInc]);
      // ioData.com->fprintf(stdout, "deigvalsS = [%e, %e, %e];\n", deigvalsS[0][k][uInc], deigvalsS[1][k][uInc], deigvalsS[2][k][uInc]);
      // ioData.com->fprintf(stdout, "deigvectsS= [%e, %e, %e; %e, %e, %e; %e, %e, %e];\n\n", deigvectsS[0][0][k][uInc], deigvectsS[0][1][k][uInc], deigvectsS[0][2][k][uInc], deigvectsS[1][0][k][uInc], deigvectsS[1][1][k][uInc], deigvectsS[1][2][k][uInc], deigvectsS[2][0][k][uInc], deigvectsS[2][1][k][uInc], deigvectsS[2][2][k][uInc]);
      // ioData.com->barrier();
    } //uInc
  }

  // dudxj[numerator][denominator] so d(vel_x)/d(direction y) = dudxj[0][1]
  double toto = (dudxj[0][0] + dudxj[1][1] + dudxj[2][2]);
  for(int k = 0; k < 4; ++k) {
    dTKE[k][0] = (3.0 / V[k][0]) * ((-1.0 / V[k][0]) * (mut * eigvalsS[2] + 0.5 * lambdat * toto) + 
                                    (dmut[k][0] * eigvalsS[2]) + (0.5 * dlambdat[k][0] * toto) +
                                    (mut * deigvalsS[2][k][0]) + (0.5 * lambdat * (dS[0][0][k][0] + dS[1][1][k][0] + dS[2][2][k][0])));
    dTKE[k][1] = (3.0 / V[k][0]) * ((dmut[k][1] * eigvalsS[2]) + (0.5 * dlambdat[k][1] * toto) +
                                    (mut * deigvalsS[2][k][1]) + (0.5 * lambdat * dS[0][0][k][1]));
    dTKE[k][2] = (3.0 / V[k][0]) * ((dmut[k][2] * eigvalsS[2]) + (0.5 * dlambdat[k][2] * toto) +
                                    (mut * deigvalsS[2][k][2]) + (0.5 * lambdat * dS[1][1][k][2]));
    dTKE[k][3] = (3.0 / V[k][0]) * ((dmut[k][3] * eigvalsS[2]) + (0.5 * dlambdat[k][3] * toto) +
                                    (mut * deigvalsS[2][k][3]) + (0.5 * lambdat * dS[2][2][k][3]));
    dTKE[k][4] = (3.0 / V[k][0]) * ((dmut[k][4] * eigvalsS[2]) + (0.5 * dlambdat[k][4] * toto) +
                                    (mut * deigvalsS[2][k][4]));
    dTKE[k][5] = (3.0 / V[k][0]) * ((dmut[k][5] * eigvalsS[2]) + (0.5 * dlambdat[k][5] * toto) +
                                    (mut * deigvalsS[2][k][5]));
  }

  double eigvalMat[3][3] = {};
  eigvalMat[0][0] = eigvalVect[0];
  eigvalMat[1][1] = eigvalVect[1];
  eigvalMat[2][2] = eigvalVect[2];
  for(int k = 0; k < 4; ++k) {
    for(int uInc = 0; uInc < 6; ++uInc) {
      double returnMat1[3][3] = {}, returnMat2[3][3] = {}, deigvectsS_input[3][3], psi_transpose[3][3], eigvectsS_transpose[3][3], deigvectsS_input_transpose[3][3];
      for(int i = 0; i < 3; i++) {
        for(int j = 0; j < 3; j++) {
          deigvectsS_input[i][j] = deigvectsS[i][j][k][uInc];
          deigvectsS_input_transpose[j][i] = deigvectsS_input[i][j];
          psi_transpose[j][i] = psi[i][j];
          eigvectsS_transpose[j][i] = eigvectsS[i][j];
        }
      }
      multiplyFiveMatrices3x3(deigvectsS_input, psi, eigvalMat, psi_transpose, eigvectsS_transpose, returnMat1);
      multiplyFiveMatrices3x3(eigvectsS, psi, eigvalMat, psi_transpose, deigvectsS_input_transpose, returnMat2);
      // ioData.com->fprintf(stdout, "deigvectsS_in  = [%e, %e, %e; %e, %e, %e; %e, %e, %e];\n", deigvectsS_input[0][0], deigvectsS_input[0][1], deigvectsS_input[0][2], deigvectsS_input[1][0], deigvectsS_input[1][1], deigvectsS_input[1][2], deigvectsS_input[2][0], deigvectsS_input[2][1], deigvectsS_input[2][2]);
      // ioData.com->fprintf(stdout, "psi            = [%e, %e, %e; %e, %e, %e; %e, %e, %e];\n", psi[0][0], psi[0][1], psi[0][2], psi[1][0], psi[1][1], psi[1][2], psi[2][0], psi[2][1], psi[2][2]);
      // ioData.com->fprintf(stdout, "eigvalMat      = [%e, %e, %e; %e, %e, %e; %e, %e, %e];\n", eigvalMat[0][0], eigvalMat[0][1], eigvalMat[0][2], eigvalMat[1][0], eigvalMat[1][1], eigvalMat[1][2], eigvalMat[2][0], eigvalMat[2][1], eigvalMat[2][2]);
      // ioData.com->fprintf(stdout, "eigvectsS_in   = [%e, %e, %e; %e, %e, %e; %e, %e, %e];\n", eigvectsS_transpose[0][0], eigvectsS_transpose[0][1], eigvectsS_transpose[0][2], eigvectsS_transpose[1][0], eigvectsS_transpose[1][1], eigvectsS_transpose[1][2], eigvectsS_transpose[2][0], eigvectsS_transpose[2][1], eigvectsS_transpose[2][2]);
      // ioData.com->fprintf(stdout, "returnMat1     = [%e, %e, %e; %e, %e, %e; %e, %e, %e];\n", returnMat1[0][0], returnMat1[0][1], returnMat1[0][2], returnMat1[1][0], returnMat1[1][1], returnMat1[1][2], returnMat1[2][0], returnMat1[2][1], returnMat1[2][2]);
      // ioData.com->fprintf(stdout, "returnMat2     = [%e, %e, %e; %e, %e, %e; %e, %e, %e];\n\n", returnMat2[0][0], returnMat2[0][1], returnMat2[0][2], returnMat2[1][0], returnMat2[1][1], returnMat2[1][2], returnMat2[2][0], returnMat2[2][1], returnMat2[2][2]);
      // ioData.com->barrier();
      for(int i = 0; i < 3; i++) {
        for(int j = 0; j < 3; j++) {
          // need to pass in Psi and D, the eigenvalue scaling matrix
          dA[i][j][k][uInc] = returnMat1[i][j] + returnMat2[i][j];
        }
      }
    }
  }

  // EJ ABOVE HERE EDIT ----------------------------------------------------------
  for(int k = 0; k < 4; ++k) {
    ducgdu0[0] = -fourth * V[k][1] / V[k][0];
    ducgdu1[0] = fourth / V[k][0];
    ducgdu2[0] = 0.0;
    ducgdu3[0] = 0.0;
    ducgdu4[0] = 0.0;
    ducgdu5[0] = 0.0;
    ducgdu0[1] = -fourth * V[k][2] / V[k][0];
    ducgdu1[1] = 0.0;
    ducgdu2[1] = fourth / V[k][0];
    ducgdu3[1] = 0.0;
    ducgdu4[1] = 0.0;
    ducgdu5[1] = 0.0;
    ducgdu0[2] = -fourth * V[k][3] / V[k][0];
    ducgdu1[2] = 0.0;
    ducgdu2[2] = 0.0;
    ducgdu3[2] = fourth / V[k][0];
    ducgdu4[2] = 0.0;
    ducgdu5[2] = 0.0;

    // EJ ABOVE HERE EDIT ----------------------------------------------------------

    // dtijdu0[0][0] = 2.0 * dmu[k][0] * dudxj[0][0] - 2.0 * mu * dp1dxj[k][0] * V[k][1] / V[k][0] + dlambda[k][0] *
    //                 (dudxj[0][0] + dudxj[1][1] + dudxj[2][2]) - lambda * (dp1dxj[k][0] * V[k][1] + dp1dxj[k][1] * V[k][2] + dp1dxj[k][2] * V[k][3]) / V[k][0];
    // dtijdu1[0][0] = 2.0 * dmu[k][1] * dudxj[0][0] + 2.0 * mu * dp1dxj[k][0] / V[k][0] + dlambda[k][1] * (dudxj[0][0] + dudxj[1][1] + dudxj[2][2]) + lambda
    //                 * dp1dxj[k][0] / V[k][0];
    // dtijdu2[0][0] = 2.0 * dmu[k][2] * dudxj[0][0] + dlambda[k][2] * (dudxj[0][0] + dudxj[1][1] + dudxj[2][2]) + lambda * dp1dxj[k][1] / V[k][0];
    // dtijdu3[0][0] = 2.0 * dmu[k][3] * dudxj[0][0] + dlambda[k][3] * (dudxj[0][0] + dudxj[1][1] + dudxj[2][2]) + lambda * dp1dxj[k][2] / V[k][0];
    // dtijdu4[0][0] = 2.0 * dmu[k][4] * dudxj[0][0] + dlambda[k][4] * (dudxj[0][0] + dudxj[1][1] + dudxj[2][2]);
    // dtijdu5[0][0] = 2.0 * dmu[k][5] * dudxj[0][0] + dlambda[k][5] * (dudxj[0][0] + dudxj[1][1] + dudxj[2][2]);
    // dtijdu0[1][1] = 2.0 * dmu[k][0] * dudxj[1][1] - 2.0 * mu * dp1dxj[k][1] * V[k][2] / V[k][0] + dlambda[k][0] *
    //                 (dudxj[0][0] + dudxj[1][1] + dudxj[2][2]) - lambda * (dp1dxj[k][1] * V[k][2] + dp1dxj[k][0] * V[k][1] + dp1dxj[k][2] * V[k][3]) / V[k][0];
    // dtijdu1[1][1] = 2.0 * dmu[k][1] * dudxj[1][1] + dlambda[k][1] * (dudxj[0][0] + dudxj[1][1] + dudxj[2][2]) + lambda * dp1dxj[k][0] / V[k][0];
    // dtijdu2[1][1] = 2.0 * dmu[k][2] * dudxj[1][1] + 2.0 * mu * dp1dxj[k][1] / V[k][0] + dlambda[k][2] * (dudxj[0][0] + dudxj[1][1] + dudxj[2][2]) + lambda
    //                 * dp1dxj[k][1] / V[k][0];
    // dtijdu3[1][1] = 2.0 * dmu[k][3] * dudxj[1][1] + dlambda[k][3] * (dudxj[0][0] + dudxj[1][1] + dudxj[2][2]) + lambda * dp1dxj[k][2] / V[k][0];
    // dtijdu4[1][1] = 2.0 * dmu[k][4] * dudxj[1][1] + dlambda[k][4] * (dudxj[0][0] + dudxj[1][1] + dudxj[2][2]);
    // dtijdu5[1][1] = 2.0 * dmu[k][5] * dudxj[1][1] + dlambda[k][5] * (dudxj[0][0] + dudxj[1][1] + dudxj[2][2]);
    // dtijdu0[2][2] = 2.0 * dmu[k][0] * dudxj[2][2] - 2.0 * mu * dp1dxj[k][2] * V[k][3] / V[k][0] + dlambda[k][0] *
    //                 (dudxj[0][0] + dudxj[1][1] + dudxj[2][2]) - lambda * (dp1dxj[k][2] * V[k][3] + dp1dxj[k][0] * V[k][1] + dp1dxj[k][1] * V[k][2]) / V[k][0];
    // dtijdu1[2][2] = 2.0 * dmu[k][1] * dudxj[2][2] + dlambda[k][1] * (dudxj[0][0] + dudxj[1][1] + dudxj[2][2]) + lambda * dp1dxj[k][0] / V[k][0];
    // dtijdu2[2][2] = 2.0 * dmu[k][2] * dudxj[2][2] + dlambda[k][2] * (dudxj[0][0] + dudxj[1][1] + dudxj[2][2]) + lambda * dp1dxj[k][1] / V[k][0];
    // dtijdu3[2][2] = 2.0 * dmu[k][3] * dudxj[2][2] + 2.0 * mu * dp1dxj[k][2] / V[k][0] + dlambda[k][3] * (dudxj[0][0] + dudxj[1][1] + dudxj[2][2]) + lambda
    //                 * dp1dxj[k][2] / V[k][0];
    // dtijdu4[2][2] = 2.0 * dmu[k][4] * dudxj[2][2] + dlambda[k][4] * (dudxj[0][0] + dudxj[1][1] + dudxj[2][2]);
    // dtijdu5[2][2] = 2.0 * dmu[k][5] * dudxj[2][2] + dlambda[k][5] * (dudxj[0][0] + dudxj[1][1] + dudxj[2][2]);
    // dtijdu0[0][1] = -mu * (dp1dxj[k][0] * V[k][2] + dp1dxj[k][1] * V[k][1]) / V[k][0] + dmu[k][0] * (dudxj[1][0] + dudxj[0][1]);
    // dtijdu1[0][1] = mu * dp1dxj[k][1] / V[k][0] + dmu[k][1] * (dudxj[1][0] + dudxj[0][1]);
    // dtijdu2[0][1] = mu * dp1dxj[k][0] / V[k][0] + dmu[k][2] * (dudxj[1][0] + dudxj[0][1]);
    // dtijdu3[0][1] = dmu[k][3] * (dudxj[1][0] + dudxj[0][1]);
    // dtijdu4[0][1] = dmu[k][4] * (dudxj[1][0] + dudxj[0][1]);
    // dtijdu5[0][1] = dmu[k][5] * (dudxj[1][0] + dudxj[0][1]);
    // dtijdu0[0][2] = -mu * (dp1dxj[k][0] * V[k][3] + dp1dxj[k][2] * V[k][1]) / V[k][0] + dmu[k][0] * (dudxj[2][0] + dudxj[0][2]);
    // dtijdu1[0][2] = mu * dp1dxj[k][2] / V[k][0] + dmu[k][1] * (dudxj[2][0] + dudxj[0][2]);
    // dtijdu2[0][2] = dmu[k][2] * (dudxj[2][0] + dudxj[0][2]);
    // dtijdu3[0][2] = mu * dp1dxj[k][0] / V[k][0] + dmu[k][3] * (dudxj[2][0] + dudxj[0][2]);
    // dtijdu4[0][2] = dmu[k][4] * (dudxj[2][0] + dudxj[0][2]);
    // dtijdu5[0][2] = dmu[k][5] * (dudxj[2][0] + dudxj[0][2]);
    // dtijdu0[1][2] = -mu * (dp1dxj[k][1] * V[k][3] + dp1dxj[k][2] * V[k][2]) / V[k][0] + dmu[k][0] * (dudxj[2][1] + dudxj[1][2]);
    // dtijdu1[1][2] = dmu[k][1] * (dudxj[2][1] + dudxj[1][2]);
    // dtijdu2[1][2] = mu * dp1dxj[k][2] / V[k][0] + dmu[k][2] * (dudxj[2][1] + dudxj[1][2]);
    // dtijdu3[1][2] = mu * dp1dxj[k][1] / V[k][0] + dmu[k][3] * (dudxj[2][1] + dudxj[1][2]);
    // dtijdu4[1][2] = dmu[k][4] * (dudxj[2][1] + dudxj[1][2]);
    // dtijdu5[1][2] = dmu[k][5] * (dudxj[2][1] + dudxj[1][2]);
    // ioData.com->fprintf(stdout, "Original lam + turb:\n");
    // ioData.com->fprintf(stdout, "dtijdu0 = [%e, %e, %e; %e, %e, %e; %e, %e, %e];\n", dtijdu0[0][0], dtijdu0[0][1], dtijdu0[0][2], dtijdu0[0][1], dtijdu0[1][1], dtijdu0[1][2], dtijdu0[0][2], dtijdu0[1][2], dtijdu0[2][2]);
    // ioData.com->fprintf(stdout, "dtijdu1 = [%e, %e, %e; %e, %e, %e; %e, %e, %e];\n", dtijdu1[0][0], dtijdu1[0][1], dtijdu1[0][2], dtijdu1[0][1], dtijdu1[1][1], dtijdu1[1][2], dtijdu1[0][2], dtijdu1[1][2], dtijdu1[2][2]);
    // ioData.com->fprintf(stdout, "dtijdu2 = [%e, %e, %e; %e, %e, %e; %e, %e, %e];\n", dtijdu2[0][0], dtijdu2[0][1], dtijdu2[0][2], dtijdu2[0][1], dtijdu2[1][1], dtijdu2[1][2], dtijdu2[0][2], dtijdu2[1][2], dtijdu2[2][2]);
    // ioData.com->fprintf(stdout, "dtijdu3 = [%e, %e, %e; %e, %e, %e; %e, %e, %e];\n", dtijdu3[0][0], dtijdu3[0][1], dtijdu3[0][2], dtijdu3[0][1], dtijdu3[1][1], dtijdu3[1][2], dtijdu3[0][2], dtijdu3[1][2], dtijdu3[2][2]);
    // ioData.com->fprintf(stdout, "dtijdu4 = [%e, %e, %e; %e, %e, %e; %e, %e, %e];\n", dtijdu4[0][0], dtijdu4[0][1], dtijdu4[0][2], dtijdu4[0][1], dtijdu4[1][1], dtijdu4[1][2], dtijdu4[0][2], dtijdu4[1][2], dtijdu4[2][2]);
    // ioData.com->fprintf(stdout, "dtijdu5 = [%e, %e, %e; %e, %e, %e; %e, %e, %e];\n\n", dtijdu5[0][0], dtijdu5[0][1], dtijdu5[0][2], dtijdu5[0][1], dtijdu5[1][1], dtijdu5[1][2], dtijdu5[0][2], dtijdu5[1][2], dtijdu5[2][2]);
    // ioData.com->barrier();

    dtijdu0[0][0] = (1-taut_rlx) * (2.0 * dmut[k][0] * dudxj[0][0] - 2.0 * mut * dp1dxj[k][0] * V[k][1] / V[k][0] + dlambdat[k][0] *
                    (dudxj[0][0] + dudxj[1][1] + dudxj[2][2]) - lambdat * (dp1dxj[k][0] * V[k][1] + dp1dxj[k][1] * V[k][2] + dp1dxj[k][2] * V[k][3]) / V[k][0]);
    dtijdu1[0][0] = (1-taut_rlx) * (2.0 * dmut[k][1] * dudxj[0][0] + 2.0 * mut * dp1dxj[k][0] / V[k][0] + dlambdat[k][1] * (dudxj[0][0] + dudxj[1][1] + dudxj[2][2]) + lambdat
                    * dp1dxj[k][0] / V[k][0]);
    dtijdu2[0][0] = (1-taut_rlx) * (2.0 * dmut[k][2] * dudxj[0][0] + dlambdat[k][2] * (dudxj[0][0] + dudxj[1][1] + dudxj[2][2]) + lambdat * dp1dxj[k][1] / V[k][0]);
    dtijdu3[0][0] = (1-taut_rlx) * (2.0 * dmut[k][3] * dudxj[0][0] + dlambdat[k][3] * (dudxj[0][0] + dudxj[1][1] + dudxj[2][2]) + lambdat * dp1dxj[k][2] / V[k][0]);
    dtijdu4[0][0] = (1-taut_rlx) * (2.0 * dmut[k][4] * dudxj[0][0] + dlambdat[k][4] * (dudxj[0][0] + dudxj[1][1] + dudxj[2][2]));
    dtijdu5[0][0] = (1-taut_rlx) * (2.0 * dmut[k][5] * dudxj[0][0] + dlambdat[k][5] * (dudxj[0][0] + dudxj[1][1] + dudxj[2][2]));
    dtijdu0[1][1] = (1-taut_rlx) * (2.0 * dmut[k][0] * dudxj[1][1] - 2.0 * mut * dp1dxj[k][1] * V[k][2] / V[k][0] + dlambdat[k][0] *
                    (dudxj[0][0] + dudxj[1][1] + dudxj[2][2]) - lambdat * (dp1dxj[k][1] * V[k][2] + dp1dxj[k][0] * V[k][1] + dp1dxj[k][2] * V[k][3]) / V[k][0]);
    dtijdu1[1][1] = (1-taut_rlx) * (2.0 * dmut[k][1] * dudxj[1][1] + dlambdat[k][1] * (dudxj[0][0] + dudxj[1][1] + dudxj[2][2]) + lambdat * dp1dxj[k][0] / V[k][0]);
    dtijdu2[1][1] = (1-taut_rlx) * (2.0 * dmut[k][2] * dudxj[1][1] + 2.0 * mut * dp1dxj[k][1] / V[k][0] + dlambdat[k][2] * (dudxj[0][0] + dudxj[1][1] + dudxj[2][2]) + lambdat
                    * dp1dxj[k][1] / V[k][0]);
    dtijdu3[1][1] = (1-taut_rlx) * (2.0 * dmut[k][3] * dudxj[1][1] + dlambdat[k][3] * (dudxj[0][0] + dudxj[1][1] + dudxj[2][2]) + lambdat * dp1dxj[k][2] / V[k][0]);
    dtijdu4[1][1] = (1-taut_rlx) * (2.0 * dmut[k][4] * dudxj[1][1] + dlambdat[k][4] * (dudxj[0][0] + dudxj[1][1] + dudxj[2][2]));
    dtijdu5[1][1] = (1-taut_rlx) * (2.0 * dmut[k][5] * dudxj[1][1] + dlambdat[k][5] * (dudxj[0][0] + dudxj[1][1] + dudxj[2][2]));
    dtijdu0[2][2] = (1-taut_rlx) * (2.0 * dmut[k][0] * dudxj[2][2] - 2.0 * mut * dp1dxj[k][2] * V[k][3] / V[k][0] + dlambdat[k][0] *
                    (dudxj[0][0] + dudxj[1][1] + dudxj[2][2]) - lambdat * (dp1dxj[k][2] * V[k][3] + dp1dxj[k][0] * V[k][1] + dp1dxj[k][1] * V[k][2]) / V[k][0]);
    dtijdu1[2][2] = (1-taut_rlx) * (2.0 * dmut[k][1] * dudxj[2][2] + dlambdat[k][1] * (dudxj[0][0] + dudxj[1][1] + dudxj[2][2]) + lambdat * dp1dxj[k][0] / V[k][0]);
    dtijdu2[2][2] = (1-taut_rlx) * (2.0 * dmut[k][2] * dudxj[2][2] + dlambdat[k][2] * (dudxj[0][0] + dudxj[1][1] + dudxj[2][2]) + lambdat * dp1dxj[k][1] / V[k][0]);
    dtijdu3[2][2] = (1-taut_rlx) * (2.0 * dmut[k][3] * dudxj[2][2] + 2.0 * mut * dp1dxj[k][2] / V[k][0] + dlambdat[k][3] * (dudxj[0][0] + dudxj[1][1] + dudxj[2][2]) + lambdat
                    * dp1dxj[k][2] / V[k][0]);
    dtijdu4[2][2] = (1-taut_rlx) * (2.0 * dmut[k][4] * dudxj[2][2] + dlambdat[k][4] * (dudxj[0][0] + dudxj[1][1] + dudxj[2][2]));
    dtijdu5[2][2] = (1-taut_rlx) * (2.0 * dmut[k][5] * dudxj[2][2] + dlambdat[k][5] * (dudxj[0][0] + dudxj[1][1] + dudxj[2][2]));
    dtijdu0[0][1] = (1-taut_rlx) * (-mut * (dp1dxj[k][0] * V[k][2] + dp1dxj[k][1] * V[k][1]) / V[k][0] + dmut[k][0] * (dudxj[1][0] + dudxj[0][1]));
    dtijdu1[0][1] = (1-taut_rlx) * (mut * dp1dxj[k][1] / V[k][0] + dmut[k][1] * (dudxj[1][0] + dudxj[0][1]));
    dtijdu2[0][1] = (1-taut_rlx) * (mut * dp1dxj[k][0] / V[k][0] + dmut[k][2] * (dudxj[1][0] + dudxj[0][1]));
    dtijdu3[0][1] = (1-taut_rlx) * (dmut[k][3] * (dudxj[1][0] + dudxj[0][1]));
    dtijdu4[0][1] = (1-taut_rlx) * (dmut[k][4] * (dudxj[1][0] + dudxj[0][1]));
    dtijdu5[0][1] = (1-taut_rlx) * (dmut[k][5] * (dudxj[1][0] + dudxj[0][1]));
    dtijdu0[0][2] = (1-taut_rlx) * (-mut * (dp1dxj[k][0] * V[k][3] + dp1dxj[k][2] * V[k][1]) / V[k][0] + dmut[k][0] * (dudxj[2][0] + dudxj[0][2]));
    dtijdu1[0][2] = (1-taut_rlx) * (mut * dp1dxj[k][2] / V[k][0] + dmut[k][1] * (dudxj[2][0] + dudxj[0][2]));
    dtijdu2[0][2] = (1-taut_rlx) * (dmut[k][2] * (dudxj[2][0] + dudxj[0][2]));
    dtijdu3[0][2] = (1-taut_rlx) * (mut * dp1dxj[k][0] / V[k][0] + dmut[k][3] * (dudxj[2][0] + dudxj[0][2]));
    dtijdu4[0][2] = (1-taut_rlx) * (dmut[k][4] * (dudxj[2][0] + dudxj[0][2]));
    dtijdu5[0][2] = (1-taut_rlx) * (dmut[k][5] * (dudxj[2][0] + dudxj[0][2]));
    dtijdu0[1][2] = (1-taut_rlx) * (-mut * (dp1dxj[k][1] * V[k][3] + dp1dxj[k][2] * V[k][2]) / V[k][0] + dmut[k][0] * (dudxj[2][1] + dudxj[1][2]));
    dtijdu1[1][2] = (1-taut_rlx) * (dmut[k][1] * (dudxj[2][1] + dudxj[1][2]));
    dtijdu2[1][2] = (1-taut_rlx) * (mut * dp1dxj[k][2] / V[k][0] + dmut[k][2] * (dudxj[2][1] + dudxj[1][2]));
    dtijdu3[1][2] = (1-taut_rlx) * (mut * dp1dxj[k][1] / V[k][0] + dmut[k][3] * (dudxj[2][1] + dudxj[1][2]));
    dtijdu4[1][2] = (1-taut_rlx) * (dmut[k][4] * (dudxj[2][1] + dudxj[1][2]));
    dtijdu5[1][2] = (1-taut_rlx) * (dmut[k][5] * (dudxj[2][1] + dudxj[1][2]));
    // ioData.com->fprintf(stdout, "Original turb:\n");
    // ioData.com->fprintf(stdout, "dtijdu0 = [%e, %e, %e; %e, %e, %e; %e, %e, %e];\n", dtijdu0[0][0], dtijdu0[0][1], dtijdu0[0][2], dtijdu0[0][1], dtijdu0[1][1], dtijdu0[1][2], dtijdu0[0][2], dtijdu0[1][2], dtijdu0[2][2]);
    // ioData.com->fprintf(stdout, "dtijdu1 = [%e, %e, %e; %e, %e, %e; %e, %e, %e];\n", dtijdu1[0][0], dtijdu1[0][1], dtijdu1[0][2], dtijdu1[0][1], dtijdu1[1][1], dtijdu1[1][2], dtijdu1[0][2], dtijdu1[1][2], dtijdu1[2][2]);
    // ioData.com->fprintf(stdout, "dtijdu2 = [%e, %e, %e; %e, %e, %e; %e, %e, %e];\n", dtijdu2[0][0], dtijdu2[0][1], dtijdu2[0][2], dtijdu2[0][1], dtijdu2[1][1], dtijdu2[1][2], dtijdu2[0][2], dtijdu2[1][2], dtijdu2[2][2]);
    // ioData.com->fprintf(stdout, "dtijdu3 = [%e, %e, %e; %e, %e, %e; %e, %e, %e];\n", dtijdu3[0][0], dtijdu3[0][1], dtijdu3[0][2], dtijdu3[0][1], dtijdu3[1][1], dtijdu3[1][2], dtijdu3[0][2], dtijdu3[1][2], dtijdu3[2][2]);
    // ioData.com->fprintf(stdout, "dtijdu4 = [%e, %e, %e; %e, %e, %e; %e, %e, %e];\n", dtijdu4[0][0], dtijdu4[0][1], dtijdu4[0][2], dtijdu4[0][1], dtijdu4[1][1], dtijdu4[1][2], dtijdu4[0][2], dtijdu4[1][2], dtijdu4[2][2]);
    // ioData.com->fprintf(stdout, "dtijdu5 = [%e, %e, %e; %e, %e, %e; %e, %e, %e];\n\n", dtijdu5[0][0], dtijdu5[0][1], dtijdu5[0][2], dtijdu5[0][1], dtijdu5[1][1], dtijdu5[1][2], dtijdu5[0][2], dtijdu5[1][2], dtijdu5[2][2]);
    // ioData.com->barrier();


    // EJ ABOVE HERE EDIT ----------------------------------------------------------
    // dtijdu0[0][0] = -2.0 * TKE * (A[0][0] + 1.0/3.0) - 2.0 * V[k][0] * dTKE[k][0] * (A[0][0] + 1.0/3.0) - 2.0 *  V[k][0] * TKE * dA[0][0][k][0];
    // dtijdu1[0][0] = - 2.0 * V[k][0] * dTKE[k][1] * (A[0][0] + 1.0/3.0) - 2.0 * V[k][0] * TKE * dA[0][0][k][1];
    // dtijdu2[0][0] = - 2.0 * V[k][0] * dTKE[k][2] * (A[0][0] + 1.0/3.0) - 2.0 * V[k][0] * TKE * dA[0][0][k][2];
    // dtijdu3[0][0] = - 2.0 * V[k][0] * dTKE[k][3] * (A[0][0] + 1.0/3.0) - 2.0 * V[k][0] * TKE * dA[0][0][k][3];
    // dtijdu4[0][0] = - 2.0 * V[k][0] * dTKE[k][4] * (A[0][0] + 1.0/3.0) - 2.0 * V[k][0] * TKE * dA[0][0][k][4];
    // dtijdu5[0][0] = - 2.0 * V[k][0] * dTKE[k][5] * (A[0][0] + 1.0/3.0) - 2.0 * V[k][0] * TKE * dA[0][0][k][5];
    // dtijdu0[1][1] = -2.0 * TKE * (A[1][1] + 1.0/3.0) - 2.0 * V[k][0] * dTKE[k][0] * (A[1][1] + 1.0/3.0) - 2.0 * V[k][0] * TKE * dA[1][1][k][0];
    // dtijdu1[1][1] = - 2.0 * V[k][0] * dTKE[k][1] * (A[1][1] + 1.0/3.0) - 2.0 * V[k][0] * TKE * dA[1][1][k][1];
    // dtijdu2[1][1] = - 2.0 * V[k][0] * dTKE[k][2] * (A[1][1] + 1.0/3.0) - 2.0 * V[k][0] * TKE * dA[1][1][k][2];
    // dtijdu3[1][1] = - 2.0 * V[k][0] * dTKE[k][3] * (A[1][1] + 1.0/3.0) - 2.0 * V[k][0] * TKE * dA[1][1][k][3];
    // dtijdu4[1][1] = - 2.0 * V[k][0] * dTKE[k][4] * (A[1][1] + 1.0/3.0) - 2.0 * V[k][0] * TKE * dA[1][1][k][4];
    // dtijdu5[1][1] = - 2.0 * V[k][0] * dTKE[k][5] * (A[1][1] + 1.0/3.0) - 2.0 * V[k][0] * TKE * dA[1][1][k][5];
    // dtijdu0[2][2] = -2.0 * TKE * (A[2][2] + 1.0/3.0) - 2.0 * V[k][0] * dTKE[k][0] * (A[2][2] + 1.0/3.0) - 2.0 * V[k][0] * TKE * dA[2][2][k][0];
    // dtijdu1[2][2] = - 2.0 * V[k][0] * dTKE[k][1] * (A[2][2] + 1.0/3.0) - 2.0 * V[k][0] * TKE * dA[2][2][k][1];
    // dtijdu2[2][2] = - 2.0 * V[k][0] * dTKE[k][2] * (A[2][2] + 1.0/3.0) - 2.0 * V[k][0] * TKE * dA[2][2][k][2];
    // dtijdu3[2][2] = - 2.0 * V[k][0] * dTKE[k][3] * (A[2][2] + 1.0/3.0) - 2.0 * V[k][0] * TKE * dA[2][2][k][3];
    // dtijdu4[2][2] = - 2.0 * V[k][0] * dTKE[k][4] * (A[2][2] + 1.0/3.0) - 2.0 * V[k][0] * TKE * dA[2][2][k][4];
    // dtijdu5[2][2] = - 2.0 * V[k][0] * dTKE[k][5] * (A[2][2] + 1.0/3.0) - 2.0 * V[k][0] * TKE * dA[2][2][k][5];
    // dtijdu0[0][1] = -2.0 * TKE * A[0][1] - 2.0 * V[k][0] * dTKE[k][0] * A[0][1] - 2.0 * V[k][0] * TKE * dA[0][1][k][0];
    // dtijdu1[0][1] = - 2.0 * V[k][0] * dTKE[k][1] * A[0][1] - 2.0 * V[k][0] * TKE * dA[0][1][k][1];
    // dtijdu2[0][1] = - 2.0 * V[k][0] * dTKE[k][2] * A[0][1] - 2.0 * V[k][0] * TKE * dA[0][1][k][2];
    // dtijdu3[0][1] = - 2.0 * V[k][0] * dTKE[k][3] * A[0][1] - 2.0 * V[k][0] * TKE * dA[0][1][k][3];
    // dtijdu4[0][1] = - 2.0 * V[k][0] * dTKE[k][4] * A[0][1] - 2.0 * V[k][0] * TKE * dA[0][1][k][4];
    // dtijdu5[0][1] = - 2.0 * V[k][0] * dTKE[k][5] * A[0][1] - 2.0 * V[k][0] * TKE * dA[0][1][k][5];
    // dtijdu0[0][2] = -2.0 * TKE * A[0][2] - 2.0 * V[k][0] * dTKE[k][0] * A[0][2] - 2.0 * V[k][0] * TKE * dA[0][2][k][0];
    // dtijdu1[0][2] = - 2.0 * V[k][0] * dTKE[k][1] * A[0][2] - 2.0 * V[k][0] * TKE * dA[0][2][k][1];
    // dtijdu2[0][2] = - 2.0 * V[k][0] * dTKE[k][2] * A[0][2] - 2.0 * V[k][0] * TKE * dA[0][2][k][2];
    // dtijdu3[0][2] = - 2.0 * V[k][0] * dTKE[k][3] * A[0][2] - 2.0 * V[k][0] * TKE * dA[0][2][k][3];
    // dtijdu4[0][2] = - 2.0 * V[k][0] * dTKE[k][4] * A[0][2] - 2.0 * V[k][0] * TKE * dA[0][2][k][4];
    // dtijdu5[0][2] = - 2.0 * V[k][0] * dTKE[k][5] * A[0][2] - 2.0 * V[k][0] * TKE * dA[0][2][k][5];
    // dtijdu0[1][2] = -2.0 * TKE * A[1][2] - 2.0 * V[k][0] * dTKE[k][0] * A[1][2] - 2.0 * V[k][0] * TKE * dA[1][2][k][0];
    // dtijdu1[1][2] = - 2.0 * V[k][0] * dTKE[k][1] * A[1][2] - 2.0 * V[k][0] * TKE * dA[1][2][k][1];
    // dtijdu2[1][2] = - 2.0 * V[k][0] * dTKE[k][2] * A[1][2] - 2.0 * V[k][0] * TKE * dA[1][2][k][2];
    // dtijdu3[1][2] = - 2.0 * V[k][0] * dTKE[k][3] * A[1][2] - 2.0 * V[k][0] * TKE * dA[1][2][k][3];
    // dtijdu4[1][2] = - 2.0 * V[k][0] * dTKE[k][4] * A[1][2] - 2.0 * V[k][0] * TKE * dA[1][2][k][4];
    // dtijdu5[1][2] = - 2.0 * V[k][0] * dTKE[k][5] * A[1][2] - 2.0 * V[k][0] * TKE * dA[1][2][k][5];

    // dtijdu0_term1[0][0] = -2.0 * TKE * (A[0][0] + 1.0/3.0);
    // dtijdu0_term1[1][1] = -2.0 * TKE * (A[1][1] + 1.0/3.0);
    // dtijdu0_term1[2][2] = -2.0 * TKE * (A[2][2] + 1.0/3.0);
    // dtijdu0_term1[0][1] = -2.0 * TKE * A[0][1];
    // dtijdu0_term1[0][2] = -2.0 * TKE * A[0][2];
    // dtijdu0_term1[1][2] = -2.0 * TKE * A[1][2];
    
    dtijdu0[0][0] += taut_rlx * (-2.0 * V[k][0] * dTKE[k][0] * (A[0][0] + 1.0/3.0));
    dtijdu1[0][0] += taut_rlx * (-2.0 * V[k][0] * dTKE[k][1] * (A[0][0] + 1.0/3.0));
    dtijdu2[0][0] += taut_rlx * (-2.0 * V[k][0] * dTKE[k][2] * (A[0][0] + 1.0/3.0));
    dtijdu3[0][0] += taut_rlx * (-2.0 * V[k][0] * dTKE[k][3] * (A[0][0] + 1.0/3.0));
    dtijdu4[0][0] += taut_rlx * (-2.0 * V[k][0] * dTKE[k][4] * (A[0][0] + 1.0/3.0));
    dtijdu5[0][0] += taut_rlx * (-2.0 * V[k][0] * dTKE[k][5] * (A[0][0] + 1.0/3.0));
    dtijdu0[1][1] += taut_rlx * (-2.0 * V[k][0] * dTKE[k][0] * (A[1][1] + 1.0/3.0));
    dtijdu1[1][1] += taut_rlx * (-2.0 * V[k][0] * dTKE[k][1] * (A[1][1] + 1.0/3.0));
    dtijdu2[1][1] += taut_rlx * (-2.0 * V[k][0] * dTKE[k][2] * (A[1][1] + 1.0/3.0));
    dtijdu3[1][1] += taut_rlx * (-2.0 * V[k][0] * dTKE[k][3] * (A[1][1] + 1.0/3.0));
    dtijdu4[1][1] += taut_rlx * (-2.0 * V[k][0] * dTKE[k][4] * (A[1][1] + 1.0/3.0));
    dtijdu5[1][1] += taut_rlx * (-2.0 * V[k][0] * dTKE[k][5] * (A[1][1] + 1.0/3.0));
    dtijdu0[2][2] += taut_rlx * (-2.0 * V[k][0] * dTKE[k][0] * (A[2][2] + 1.0/3.0));
    dtijdu1[2][2] += taut_rlx * (-2.0 * V[k][0] * dTKE[k][1] * (A[2][2] + 1.0/3.0));
    dtijdu2[2][2] += taut_rlx * (-2.0 * V[k][0] * dTKE[k][2] * (A[2][2] + 1.0/3.0));
    dtijdu3[2][2] += taut_rlx * (-2.0 * V[k][0] * dTKE[k][3] * (A[2][2] + 1.0/3.0));
    dtijdu4[2][2] += taut_rlx * (-2.0 * V[k][0] * dTKE[k][4] * (A[2][2] + 1.0/3.0));
    dtijdu5[2][2] += taut_rlx * (-2.0 * V[k][0] * dTKE[k][5] * (A[2][2] + 1.0/3.0));
    dtijdu0[0][1] += taut_rlx * (-2.0 * V[k][0] * dTKE[k][0] * A[0][1]);
    dtijdu1[0][1] += taut_rlx * (-2.0 * V[k][0] * dTKE[k][1] * A[0][1]);
    dtijdu2[0][1] += taut_rlx * (-2.0 * V[k][0] * dTKE[k][2] * A[0][1]);
    dtijdu3[0][1] += taut_rlx * (-2.0 * V[k][0] * dTKE[k][3] * A[0][1]);
    dtijdu4[0][1] += taut_rlx * (-2.0 * V[k][0] * dTKE[k][4] * A[0][1]);
    dtijdu5[0][1] += taut_rlx * (-2.0 * V[k][0] * dTKE[k][5] * A[0][1]);
    dtijdu0[0][2] += taut_rlx * (-2.0 * V[k][0] * dTKE[k][0] * A[0][2]);
    dtijdu1[0][2] += taut_rlx * (-2.0 * V[k][0] * dTKE[k][1] * A[0][2]);
    dtijdu2[0][2] += taut_rlx * (-2.0 * V[k][0] * dTKE[k][2] * A[0][2]);
    dtijdu3[0][2] += taut_rlx * (-2.0 * V[k][0] * dTKE[k][3] * A[0][2]);
    dtijdu4[0][2] += taut_rlx * (-2.0 * V[k][0] * dTKE[k][4] * A[0][2]);
    dtijdu5[0][2] += taut_rlx * (-2.0 * V[k][0] * dTKE[k][5] * A[0][2]);
    dtijdu0[1][2] += taut_rlx * (-2.0 * V[k][0] * dTKE[k][0] * A[1][2]);
    dtijdu1[1][2] += taut_rlx * (-2.0 * V[k][0] * dTKE[k][1] * A[1][2]);
    dtijdu2[1][2] += taut_rlx * (-2.0 * V[k][0] * dTKE[k][2] * A[1][2]);
    dtijdu3[1][2] += taut_rlx * (-2.0 * V[k][0] * dTKE[k][3] * A[1][2]);
    dtijdu4[1][2] += taut_rlx * (-2.0 * V[k][0] * dTKE[k][4] * A[1][2]);
    dtijdu5[1][2] += taut_rlx * (-2.0 * V[k][0] * dTKE[k][5] * A[1][2]);

    // dtijdu0_term2[0][0] = - 2.0 * V[k][0] * dTKE[k][0] * (A[0][0] + 1.0/3.0);
    // dtijdu1_term2[0][0] = - 2.0 * V[k][0] * dTKE[k][1] * (A[0][0] + 1.0/3.0);
    // dtijdu2_term2[0][0] = - 2.0 * V[k][0] * dTKE[k][2] * (A[0][0] + 1.0/3.0);
    // dtijdu3_term2[0][0] = - 2.0 * V[k][0] * dTKE[k][3] * (A[0][0] + 1.0/3.0);
    // dtijdu4_term2[0][0] = - 2.0 * V[k][0] * dTKE[k][4] * (A[0][0] + 1.0/3.0);
    // dtijdu5_term2[0][0] = - 2.0 * V[k][0] * dTKE[k][5] * (A[0][0] + 1.0/3.0);
    // dtijdu0_term2[1][1] = - 2.0 * V[k][0] * dTKE[k][0] * (A[1][1] + 1.0/3.0);
    // dtijdu1_term2[1][1] = - 2.0 * V[k][0] * dTKE[k][1] * (A[1][1] + 1.0/3.0);
    // dtijdu2_term2[1][1] = - 2.0 * V[k][0] * dTKE[k][2] * (A[1][1] + 1.0/3.0);
    // dtijdu3_term2[1][1] = - 2.0 * V[k][0] * dTKE[k][3] * (A[1][1] + 1.0/3.0);
    // dtijdu4_term2[1][1] = - 2.0 * V[k][0] * dTKE[k][4] * (A[1][1] + 1.0/3.0);
    // dtijdu5_term2[1][1] = - 2.0 * V[k][0] * dTKE[k][5] * (A[1][1] + 1.0/3.0);
    // dtijdu0_term2[2][2] = - 2.0 * V[k][0] * dTKE[k][0] * (A[2][2] + 1.0/3.0);
    // dtijdu1_term2[2][2] = - 2.0 * V[k][0] * dTKE[k][1] * (A[2][2] + 1.0/3.0);
    // dtijdu2_term2[2][2] = - 2.0 * V[k][0] * dTKE[k][2] * (A[2][2] + 1.0/3.0);
    // dtijdu3_term2[2][2] = - 2.0 * V[k][0] * dTKE[k][3] * (A[2][2] + 1.0/3.0);
    // dtijdu4_term2[2][2] = - 2.0 * V[k][0] * dTKE[k][4] * (A[2][2] + 1.0/3.0);
    // dtijdu5_term2[2][2] = - 2.0 * V[k][0] * dTKE[k][5] * (A[2][2] + 1.0/3.0);
    // dtijdu0_term2[0][1] = - 2.0 * V[k][0] * dTKE[k][0] * A[0][1];
    // dtijdu1_term2[0][1] = - 2.0 * V[k][0] * dTKE[k][1] * A[0][1];
    // dtijdu2_term2[0][1] = - 2.0 * V[k][0] * dTKE[k][2] * A[0][1];
    // dtijdu3_term2[0][1] = - 2.0 * V[k][0] * dTKE[k][3] * A[0][1];
    // dtijdu4_term2[0][1] = - 2.0 * V[k][0] * dTKE[k][4] * A[0][1];
    // dtijdu5_term2[0][1] = - 2.0 * V[k][0] * dTKE[k][5] * A[0][1];
    // dtijdu0_term2[0][2] = - 2.0 * V[k][0] * dTKE[k][0] * A[0][2];
    // dtijdu1_term2[0][2] = - 2.0 * V[k][0] * dTKE[k][1] * A[0][2];
    // dtijdu2_term2[0][2] = - 2.0 * V[k][0] * dTKE[k][2] * A[0][2];
    // dtijdu3_term2[0][2] = - 2.0 * V[k][0] * dTKE[k][3] * A[0][2];
    // dtijdu4_term2[0][2] = - 2.0 * V[k][0] * dTKE[k][4] * A[0][2];
    // dtijdu5_term2[0][2] = - 2.0 * V[k][0] * dTKE[k][5] * A[0][2];
    // dtijdu0_term2[1][2] = - 2.0 * V[k][0] * dTKE[k][0] * A[1][2];
    // dtijdu1_term2[1][2] = - 2.0 * V[k][0] * dTKE[k][1] * A[1][2];
    // dtijdu2_term2[1][2] = - 2.0 * V[k][0] * dTKE[k][2] * A[1][2];
    // dtijdu3_term2[1][2] = - 2.0 * V[k][0] * dTKE[k][3] * A[1][2];
    // dtijdu4_term2[1][2] = - 2.0 * V[k][0] * dTKE[k][4] * A[1][2];
    // dtijdu5_term2[1][2] = - 2.0 * V[k][0] * dTKE[k][5] * A[1][2];
    
    // dtijdu0_term3[0][0] = - 2.0 * V[k][0] * TKE * dA[0][0][k][0];
    // dtijdu1_term3[0][0] = - 2.0 * V[k][0] * TKE * dA[0][0][k][1];
    // dtijdu2_term3[0][0] = - 2.0 * V[k][0] * TKE * dA[0][0][k][2];
    // dtijdu3_term3[0][0] = - 2.0 * V[k][0] * TKE * dA[0][0][k][3];
    // dtijdu4_term3[0][0] = - 2.0 * V[k][0] * TKE * dA[0][0][k][4];
    // dtijdu5_term3[0][0] = - 2.0 * V[k][0] * TKE * dA[0][0][k][5];
    // dtijdu0_term3[1][1] = - 2.0 * V[k][0] * TKE * dA[1][1][k][0];
    // dtijdu1_term3[1][1] = - 2.0 * V[k][0] * TKE * dA[1][1][k][1];
    // dtijdu2_term3[1][1] = - 2.0 * V[k][0] * TKE * dA[1][1][k][2];
    // dtijdu3_term3[1][1] = - 2.0 * V[k][0] * TKE * dA[1][1][k][3];
    // dtijdu4_term3[1][1] = - 2.0 * V[k][0] * TKE * dA[1][1][k][4];
    // dtijdu5_term3[1][1] = - 2.0 * V[k][0] * TKE * dA[1][1][k][5];
    // dtijdu0_term3[2][2] = - 2.0 * V[k][0] * TKE * dA[2][2][k][0];
    // dtijdu1_term3[2][2] = - 2.0 * V[k][0] * TKE * dA[2][2][k][1];
    // dtijdu2_term3[2][2] = - 2.0 * V[k][0] * TKE * dA[2][2][k][2];
    // dtijdu3_term3[2][2] = - 2.0 * V[k][0] * TKE * dA[2][2][k][3];
    // dtijdu4_term3[2][2] = - 2.0 * V[k][0] * TKE * dA[2][2][k][4];
    // dtijdu5_term3[2][2] = - 2.0 * V[k][0] * TKE * dA[2][2][k][5];
    // dtijdu0_term3[0][1] = - 2.0 * V[k][0] * TKE * dA[0][1][k][0];
    // dtijdu1_term3[0][1] = - 2.0 * V[k][0] * TKE * dA[0][1][k][1];
    // dtijdu2_term3[0][1] = - 2.0 * V[k][0] * TKE * dA[0][1][k][2];
    // dtijdu3_term3[0][1] = - 2.0 * V[k][0] * TKE * dA[0][1][k][3];
    // dtijdu4_term3[0][1] = - 2.0 * V[k][0] * TKE * dA[0][1][k][4];
    // dtijdu5_term3[0][1] = - 2.0 * V[k][0] * TKE * dA[0][1][k][5];
    // dtijdu0_term3[0][2] = - 2.0 * V[k][0] * TKE * dA[0][2][k][0];
    // dtijdu1_term3[0][2] = - 2.0 * V[k][0] * TKE * dA[0][2][k][1];
    // dtijdu2_term3[0][2] = - 2.0 * V[k][0] * TKE * dA[0][2][k][2];
    // dtijdu3_term3[0][2] = - 2.0 * V[k][0] * TKE * dA[0][2][k][3];
    // dtijdu4_term3[0][2] = - 2.0 * V[k][0] * TKE * dA[0][2][k][4];
    // dtijdu5_term3[0][2] = - 2.0 * V[k][0] * TKE * dA[0][2][k][5];
    // dtijdu0_term3[1][2] = - 2.0 * V[k][0] * TKE * dA[1][2][k][0];
    // dtijdu1_term3[1][2] = - 2.0 * V[k][0] * TKE * dA[1][2][k][1];
    // dtijdu2_term3[1][2] = - 2.0 * V[k][0] * TKE * dA[1][2][k][2];
    // dtijdu3_term3[1][2] = - 2.0 * V[k][0] * TKE * dA[1][2][k][3];
    // dtijdu4_term3[1][2] = - 2.0 * V[k][0] * TKE * dA[1][2][k][4];
    // dtijdu5_term3[1][2] = - 2.0 * V[k][0] * TKE * dA[1][2][k][5];
    // ioData.com->fprintf(stdout, "New turb:\n");
    // ioData.com->fprintf(stdout, "dtijdu0 = [%e, %e, %e; %e, %e, %e; %e, %e, %e];\n", dtijdu0[0][0], dtijdu0[0][1], dtijdu0[0][2], dtijdu0[0][1], dtijdu0[1][1], dtijdu0[1][2], dtijdu0[0][2], dtijdu0[1][2], dtijdu0[2][2]);
    // ioData.com->fprintf(stdout, "dtijdu1 = [%e, %e, %e; %e, %e, %e; %e, %e, %e];\n", dtijdu1[0][0], dtijdu1[0][1], dtijdu1[0][2], dtijdu1[0][1], dtijdu1[1][1], dtijdu1[1][2], dtijdu1[0][2], dtijdu1[1][2], dtijdu1[2][2]);
    // ioData.com->fprintf(stdout, "dtijdu2 = [%e, %e, %e; %e, %e, %e; %e, %e, %e];\n", dtijdu2[0][0], dtijdu2[0][1], dtijdu2[0][2], dtijdu2[0][1], dtijdu2[1][1], dtijdu2[1][2], dtijdu2[0][2], dtijdu2[1][2], dtijdu2[2][2]);
    // ioData.com->fprintf(stdout, "dtijdu3 = [%e, %e, %e; %e, %e, %e; %e, %e, %e];\n", dtijdu3[0][0], dtijdu3[0][1], dtijdu3[0][2], dtijdu3[0][1], dtijdu3[1][1], dtijdu3[1][2], dtijdu3[0][2], dtijdu3[1][2], dtijdu3[2][2]);
    // ioData.com->fprintf(stdout, "dtijdu4 = [%e, %e, %e; %e, %e, %e; %e, %e, %e];\n", dtijdu4[0][0], dtijdu4[0][1], dtijdu4[0][2], dtijdu4[0][1], dtijdu4[1][1], dtijdu4[1][2], dtijdu4[0][2], dtijdu4[1][2], dtijdu4[2][2]);
    // ioData.com->fprintf(stdout, "dtijdu5 = [%e, %e, %e; %e, %e, %e; %e, %e, %e];\n\n", dtijdu5[0][0], dtijdu5[0][1], dtijdu5[0][2], dtijdu5[0][1], dtijdu5[1][1], dtijdu5[1][2], dtijdu5[0][2], dtijdu5[1][2], dtijdu5[2][2]);
    // ioData.com->barrier();


    // ioData.com->fprintf(stdout, "New turb -term1:\n");
    // ioData.com->fprintf(stdout, "A = [%e, %e, %e; %e, %e, %e; %e, %e, %e];\n", A[0][0], A[0][1], A[0][2], A[0][1], A[1][1], A[1][2], A[0][2], A[1][2], A[2][2]);
    // ioData.com->fprintf(stdout, "S = [%e, %e, %e; %e, %e, %e; %e, %e, %e];\n", S[0][0], S[0][1], S[0][2], S[0][1], S[1][1], S[1][2], S[0][2], S[1][2], S[2][2]);
    // ioData.com->fprintf(stdout, "V[k][0] = %e; TKE = %e;\n", V[k][0]);
    // ioData.com->fprintf(stdout, "dtijdu0 = [%e, %e, %e; %e, %e, %e; %e, %e, %e];\n", dtijdu0_term1[0][0], dtijdu0_term1[0][1], dtijdu0_term1[0][2], dtijdu0_term1[0][1], dtijdu0_term1[1][1], dtijdu0_term1[1][2], dtijdu0_term1[0][2], dtijdu0_term1[1][2], dtijdu0_term1[2][2]);
    // ioData.com->fprintf(stdout, "New turb -term2:\n");
    // ioData.com->fprintf(stdout, "dtijdu0 = [%e, %e, %e; %e, %e, %e; %e, %e, %e];\n", dtijdu0_term2[0][0], dtijdu0_term2[0][1], dtijdu0_term2[0][2], dtijdu0_term2[0][1], dtijdu0_term2[1][1], dtijdu0_term2[1][2], dtijdu0_term2[0][2], dtijdu0_term2[1][2], dtijdu0_term2[2][2]);
    // ioData.com->fprintf(stdout, "dtijdu1 = [%e, %e, %e; %e, %e, %e; %e, %e, %e];\n", dtijdu1_term2[0][0], dtijdu1_term2[0][1], dtijdu1_term2[0][2], dtijdu1_term2[0][1], dtijdu1_term2[1][1], dtijdu1_term2[1][2], dtijdu1_term2[0][2], dtijdu1_term2[1][2], dtijdu1_term2[2][2]);
    // ioData.com->fprintf(stdout, "dtijdu2 = [%e, %e, %e; %e, %e, %e; %e, %e, %e];\n", dtijdu2_term2[0][0], dtijdu2_term2[0][1], dtijdu2_term2[0][2], dtijdu2_term2[0][1], dtijdu2_term2[1][1], dtijdu2_term2[1][2], dtijdu2_term2[0][2], dtijdu2_term2[1][2], dtijdu2_term2[2][2]);
    // ioData.com->fprintf(stdout, "dtijdu3 = [%e, %e, %e; %e, %e, %e; %e, %e, %e];\n", dtijdu3_term2[0][0], dtijdu3_term2[0][1], dtijdu3_term2[0][2], dtijdu3_term2[0][1], dtijdu3_term2[1][1], dtijdu3_term2[1][2], dtijdu3_term2[0][2], dtijdu3_term2[1][2], dtijdu3_term2[2][2]);
    // ioData.com->fprintf(stdout, "dtijdu4 = [%e, %e, %e; %e, %e, %e; %e, %e, %e];\n", dtijdu4_term2[0][0], dtijdu4_term2[0][1], dtijdu4_term2[0][2], dtijdu4_term2[0][1], dtijdu4_term2[1][1], dtijdu4_term2[1][2], dtijdu4_term2[0][2], dtijdu4_term2[1][2], dtijdu4_term2[2][2]);
    // ioData.com->fprintf(stdout, "dtijdu5 = [%e, %e, %e; %e, %e, %e; %e, %e, %e];\n", dtijdu5_term2[0][0], dtijdu5_term2[0][1], dtijdu5_term2[0][2], dtijdu5_term2[0][1], dtijdu5_term2[1][1], dtijdu5_term2[1][2], dtijdu5_term2[0][2], dtijdu5_term2[1][2], dtijdu5_term2[2][2]);
    // ioData.com->fprintf(stdout, "New turb -term3:\n");
    // ioData.com->fprintf(stdout, "dtijdu0 = [%e, %e, %e; %e, %e, %e; %e, %e, %e];\n", dtijdu0_term3[0][0], dtijdu0_term3[0][1], dtijdu0_term3[0][2], dtijdu0_term3[0][1], dtijdu0_term3[1][1], dtijdu0_term3[1][2], dtijdu0_term3[0][2], dtijdu0_term3[1][2], dtijdu0_term3[2][2]);
    // ioData.com->fprintf(stdout, "dtijdu1 = [%e, %e, %e; %e, %e, %e; %e, %e, %e];\n", dtijdu1_term3[0][0], dtijdu1_term3[0][1], dtijdu1_term3[0][2], dtijdu1_term3[0][1], dtijdu1_term3[1][1], dtijdu1_term3[1][2], dtijdu1_term3[0][2], dtijdu1_term3[1][2], dtijdu1_term3[2][2]);
    // ioData.com->fprintf(stdout, "dtijdu2 = [%e, %e, %e; %e, %e, %e; %e, %e, %e];\n", dtijdu2_term3[0][0], dtijdu2_term3[0][1], dtijdu2_term3[0][2], dtijdu2_term3[0][1], dtijdu2_term3[1][1], dtijdu2_term3[1][2], dtijdu2_term3[0][2], dtijdu2_term3[1][2], dtijdu2_term3[2][2]);
    // ioData.com->fprintf(stdout, "dtijdu3 = [%e, %e, %e; %e, %e, %e; %e, %e, %e];\n", dtijdu3_term3[0][0], dtijdu3_term3[0][1], dtijdu3_term3[0][2], dtijdu3_term3[0][1], dtijdu3_term3[1][1], dtijdu3_term3[1][2], dtijdu3_term3[0][2], dtijdu3_term3[1][2], dtijdu3_term3[2][2]);
    // ioData.com->fprintf(stdout, "dtijdu4 = [%e, %e, %e; %e, %e, %e; %e, %e, %e];\n", dtijdu4_term3[0][0], dtijdu4_term3[0][1], dtijdu4_term3[0][2], dtijdu4_term3[0][1], dtijdu4_term3[1][1], dtijdu4_term3[1][2], dtijdu4_term3[0][2], dtijdu4_term3[1][2], dtijdu4_term3[2][2]);
    // ioData.com->fprintf(stdout, "dtijdu5 = [%e, %e, %e; %e, %e, %e; %e, %e, %e];\n\n", dtijdu5_term3[0][0], dtijdu5_term3[0][1], dtijdu5_term3[0][2], dtijdu5_term3[0][1], dtijdu5_term3[1][1], dtijdu5_term3[1][2], dtijdu5_term3[0][2], dtijdu5_term3[1][2], dtijdu5_term3[2][2]);

    // ioData.com->barrier();

    // EJ ABOVE HERE EDIT ----------------------------------------------------------
    dtijdu0[0][0] += 2.0 * dmul[k][0] * dudxj[0][0] - 2.0 * mul * dp1dxj[k][0] * V[k][1] / V[k][0] + dlambdal[k][0] *
                    (dudxj[0][0] + dudxj[1][1] + dudxj[2][2]) - lambdal * (dp1dxj[k][0] * V[k][1] + dp1dxj[k][1] * V[k][2] + dp1dxj[k][2] * V[k][3]) / V[k][0];
    dtijdu1[0][0] += 2.0 * dmul[k][1] * dudxj[0][0] + 2.0 * mul * dp1dxj[k][0] / V[k][0] + dlambdal[k][1] * (dudxj[0][0] + dudxj[1][1] + dudxj[2][2]) + lambdal
                    * dp1dxj[k][0] / V[k][0];
    dtijdu2[0][0] += 2.0 * dmul[k][2] * dudxj[0][0] + dlambdal[k][2] * (dudxj[0][0] + dudxj[1][1] + dudxj[2][2]) + lambdal * dp1dxj[k][1] / V[k][0];
    dtijdu3[0][0] += 2.0 * dmul[k][3] * dudxj[0][0] + dlambdal[k][3] * (dudxj[0][0] + dudxj[1][1] + dudxj[2][2]) + lambdal * dp1dxj[k][2] / V[k][0];
    dtijdu4[0][0] += 2.0 * dmul[k][4] * dudxj[0][0] + dlambdal[k][4] * (dudxj[0][0] + dudxj[1][1] + dudxj[2][2]);
    dtijdu5[0][0] += 2.0 * dmul[k][5] * dudxj[0][0] + dlambdal[k][5] * (dudxj[0][0] + dudxj[1][1] + dudxj[2][2]);
    dtijdu0[1][1] += 2.0 * dmul[k][0] * dudxj[1][1] - 2.0 * mul * dp1dxj[k][1] * V[k][2] / V[k][0] + dlambdal[k][0] *
                    (dudxj[0][0] + dudxj[1][1] + dudxj[2][2]) - lambdal * (dp1dxj[k][1] * V[k][2] + dp1dxj[k][0] * V[k][1] + dp1dxj[k][2] * V[k][3]) / V[k][0];
    dtijdu1[1][1] += 2.0 * dmul[k][1] * dudxj[1][1] + dlambdal[k][1] * (dudxj[0][0] + dudxj[1][1] + dudxj[2][2]) + lambdal * dp1dxj[k][0] / V[k][0];
    dtijdu2[1][1] += 2.0 * dmul[k][2] * dudxj[1][1] + 2.0 * mul * dp1dxj[k][1] / V[k][0] + dlambdal[k][2] * (dudxj[0][0] + dudxj[1][1] + dudxj[2][2]) + lambdal
                    * dp1dxj[k][1] / V[k][0];
    dtijdu3[1][1] += 2.0 * dmul[k][3] * dudxj[1][1] + dlambdal[k][3] * (dudxj[0][0] + dudxj[1][1] + dudxj[2][2]) + lambdal * dp1dxj[k][2] / V[k][0];
    dtijdu4[1][1] += 2.0 * dmul[k][4] * dudxj[1][1] + dlambdal[k][4] * (dudxj[0][0] + dudxj[1][1] + dudxj[2][2]);
    dtijdu5[1][1] += 2.0 * dmul[k][5] * dudxj[1][1] + dlambdal[k][5] * (dudxj[0][0] + dudxj[1][1] + dudxj[2][2]);
    dtijdu0[2][2] += 2.0 * dmul[k][0] * dudxj[2][2] - 2.0 * mul * dp1dxj[k][2] * V[k][3] / V[k][0] + dlambdal[k][0] *
                    (dudxj[0][0] + dudxj[1][1] + dudxj[2][2]) - lambdal * (dp1dxj[k][2] * V[k][3] + dp1dxj[k][0] * V[k][1] + dp1dxj[k][1] * V[k][2]) / V[k][0];
    dtijdu1[2][2] += 2.0 * dmul[k][1] * dudxj[2][2] + dlambdal[k][1] * (dudxj[0][0] + dudxj[1][1] + dudxj[2][2]) + lambdal * dp1dxj[k][0] / V[k][0];
    dtijdu2[2][2] += 2.0 * dmul[k][2] * dudxj[2][2] + dlambdal[k][2] * (dudxj[0][0] + dudxj[1][1] + dudxj[2][2]) + lambdal * dp1dxj[k][1] / V[k][0];
    dtijdu3[2][2] += 2.0 * dmul[k][3] * dudxj[2][2] + 2.0 * mul * dp1dxj[k][2] / V[k][0] + dlambdal[k][3] * (dudxj[0][0] + dudxj[1][1] + dudxj[2][2]) + lambdal
                    * dp1dxj[k][2] / V[k][0];
    dtijdu4[2][2] += 2.0 * dmul[k][4] * dudxj[2][2] + dlambdal[k][4] * (dudxj[0][0] + dudxj[1][1] + dudxj[2][2]);
    dtijdu5[2][2] += 2.0 * dmul[k][5] * dudxj[2][2] + dlambdal[k][5] * (dudxj[0][0] + dudxj[1][1] + dudxj[2][2]);
    dtijdu0[0][1] += -mul * (dp1dxj[k][0] * V[k][2] + dp1dxj[k][1] * V[k][1]) / V[k][0] + dmul[k][0] * (dudxj[1][0] + dudxj[0][1]);
    dtijdu1[0][1] += mul * dp1dxj[k][1] / V[k][0] + dmul[k][1] * (dudxj[1][0] + dudxj[0][1]);
    dtijdu2[0][1] += mul * dp1dxj[k][0] / V[k][0] + dmul[k][2] * (dudxj[1][0] + dudxj[0][1]);
    dtijdu3[0][1] += dmul[k][3] * (dudxj[1][0] + dudxj[0][1]);
    dtijdu4[0][1] += dmul[k][4] * (dudxj[1][0] + dudxj[0][1]);
    dtijdu5[0][1] += dmul[k][5] * (dudxj[1][0] + dudxj[0][1]);
    dtijdu0[0][2] += -mul * (dp1dxj[k][0] * V[k][3] + dp1dxj[k][2] * V[k][1]) / V[k][0] + dmul[k][0] * (dudxj[2][0] + dudxj[0][2]);
    dtijdu1[0][2] += mul * dp1dxj[k][2] / V[k][0] + dmul[k][1] * (dudxj[2][0] + dudxj[0][2]);
    dtijdu2[0][2] += dmul[k][2] * (dudxj[2][0] + dudxj[0][2]);
    dtijdu3[0][2] += mul * dp1dxj[k][0] / V[k][0] + dmul[k][3] * (dudxj[2][0] + dudxj[0][2]);
    dtijdu4[0][2] += dmul[k][4] * (dudxj[2][0] + dudxj[0][2]);
    dtijdu5[0][2] += dmul[k][5] * (dudxj[2][0] + dudxj[0][2]);
    dtijdu0[1][2] += -mul * (dp1dxj[k][1] * V[k][3] + dp1dxj[k][2] * V[k][2]) / V[k][0] + dmul[k][0] * (dudxj[2][1] + dudxj[1][2]);
    dtijdu1[1][2] += dmul[k][1] * (dudxj[2][1] + dudxj[1][2]);
    dtijdu2[1][2] += mul * dp1dxj[k][2] / V[k][0] + dmul[k][2] * (dudxj[2][1] + dudxj[1][2]);
    dtijdu3[1][2] += mul * dp1dxj[k][1] / V[k][0] + dmul[k][3] * (dudxj[2][1] + dudxj[1][2]);
    dtijdu4[1][2] += dmul[k][4] * (dudxj[2][1] + dudxj[1][2]);
    dtijdu5[1][2] += dmul[k][5] * (dudxj[2][1] + dudxj[1][2]);
    dtijdu0[1][0] = dtijdu0[0][1];
    dtijdu1[1][0] = dtijdu1[0][1];
    dtijdu2[1][0] = dtijdu2[0][1];
    dtijdu3[1][0] = dtijdu3[0][1];
    dtijdu4[1][0] = dtijdu4[0][1];
    dtijdu5[1][0] = dtijdu5[0][1];
    dtijdu0[2][0] = dtijdu0[0][2];
    dtijdu1[2][0] = dtijdu1[0][2];
    dtijdu2[2][0] = dtijdu2[0][2];
    dtijdu3[2][0] = dtijdu3[0][2];
    dtijdu4[2][0] = dtijdu4[0][2];
    dtijdu5[2][0] = dtijdu5[0][2];
    dtijdu0[2][1] = dtijdu0[1][2];
    dtijdu1[2][1] = dtijdu1[1][2];
    dtijdu2[2][1] = dtijdu2[1][2];
    dtijdu3[2][1] = dtijdu3[1][2];
    dtijdu4[2][1] = dtijdu4[1][2];
    dtijdu5[2][1] = dtijdu5[1][2];
    varFcn->computeJacobianTemperature(V[k], dTduk, tag);
    dTdu0[k] = dTduk[0];
    dTdu1[k] = dTduk[1];
    dTdu2[k] = dTduk[2];
    dTdu3[k] = dTduk[3];
    dTdu4[k] = dTduk[4];
    dTdu5[k] = dTduk[5];
    ddTdxjdu0[0] = dp1dxj[k][0] * dTdu0[k];
    ddTdxjdu1[0] = dp1dxj[k][0] * dTdu1[k];
    ddTdxjdu2[0] = dp1dxj[k][0] * dTdu2[k];
    ddTdxjdu3[0] = dp1dxj[k][0] * dTdu3[k];
    ddTdxjdu4[0] = dp1dxj[k][0] * dTdu4[k];
    ddTdxjdu5[0] = dp1dxj[k][0] * dTdu5[k];
    ddTdxjdu0[1] = dp1dxj[k][1] * dTdu0[k];
    ddTdxjdu1[1] = dp1dxj[k][1] * dTdu1[k];
    ddTdxjdu2[1] = dp1dxj[k][1] * dTdu2[k];
    ddTdxjdu3[1] = dp1dxj[k][1] * dTdu3[k];
    ddTdxjdu4[1] = dp1dxj[k][1] * dTdu4[k];
    ddTdxjdu5[1] = dp1dxj[k][1] * dTdu5[k];
    ddTdxjdu0[2] = dp1dxj[k][2] * dTdu0[k];
    ddTdxjdu1[2] = dp1dxj[k][2] * dTdu1[k];
    ddTdxjdu2[2] = dp1dxj[k][2] * dTdu2[k];
    ddTdxjdu3[2] = dp1dxj[k][2] * dTdu3[k];
    ddTdxjdu4[2] = dp1dxj[k][2] * dTdu4[k];
    ddTdxjdu5[2] = dp1dxj[k][2] * dTdu5[k];
    dqjdu0[0] = -dkappa[k][0] * dTdxj[0] - kappa * ddTdxjdu0[0];
    dqjdu1[0] = -dkappa[k][1] * dTdxj[0] - kappa * ddTdxjdu1[0];
    dqjdu2[0] = -dkappa[k][2] * dTdxj[0] - kappa * ddTdxjdu2[0];
    dqjdu3[0] = -dkappa[k][3] * dTdxj[0] - kappa * ddTdxjdu3[0];
    dqjdu4[0] = -dkappa[k][4] * dTdxj[0] - kappa * ddTdxjdu4[0];
    dqjdu5[0] = -dkappa[k][5] * dTdxj[0] - kappa * ddTdxjdu5[0];
    dqjdu0[1] = -dkappa[k][0] * dTdxj[1] - kappa * ddTdxjdu0[1];
    dqjdu1[1] = -dkappa[k][1] * dTdxj[1] - kappa * ddTdxjdu1[1];
    dqjdu2[1] = -dkappa[k][2] * dTdxj[1] - kappa * ddTdxjdu2[1];
    dqjdu3[1] = -dkappa[k][3] * dTdxj[1] - kappa * ddTdxjdu3[1];
    dqjdu4[1] = -dkappa[k][4] * dTdxj[1] - kappa * ddTdxjdu4[1];
    dqjdu5[1] = -dkappa[k][5] * dTdxj[1] - kappa * ddTdxjdu5[1];
    dqjdu0[2] = -dkappa[k][0] * dTdxj[2] - kappa * ddTdxjdu0[2];
    dqjdu1[2] = -dkappa[k][1] * dTdxj[2] - kappa * ddTdxjdu1[2];
    dqjdu2[2] = -dkappa[k][2] * dTdxj[2] - kappa * ddTdxjdu2[2];
    dqjdu3[2] = -dkappa[k][3] * dTdxj[2] - kappa * ddTdxjdu3[2];
    dqjdu4[2] = -dkappa[k][4] * dTdxj[2] - kappa * ddTdxjdu4[2];
    dqjdu5[2] = -dkappa[k][5] * dTdxj[2] - kappa * ddTdxjdu5[2];
    // dRxdU
    dRdU[k][0][0][0] = 0.0;
    dRdU[k][0][0][1] = 0.0;
    dRdU[k][0][0][2] = 0.0;
    dRdU[k][0][0][3] = 0.0;
    dRdU[k][0][0][4] = 0.0;
    dRdU[k][0][0][5] = 0.0;
    dRdU[k][0][1][0] = dtijdu0[0][0];
    dRdU[k][0][1][1] = dtijdu1[0][0];
    dRdU[k][0][1][2] = dtijdu2[0][0];
    dRdU[k][0][1][3] = dtijdu3[0][0];
    dRdU[k][0][1][4] = dtijdu4[0][0];
    dRdU[k][0][1][5] = dtijdu5[0][0];
    dRdU[k][0][2][0] = dtijdu0[0][1];
    dRdU[k][0][2][1] = dtijdu1[0][1];
    dRdU[k][0][2][2] = dtijdu2[0][1];
    dRdU[k][0][2][3] = dtijdu3[0][1];
    dRdU[k][0][2][4] = dtijdu4[0][1];
    dRdU[k][0][2][5] = dtijdu5[0][1];
    dRdU[k][0][3][0] = dtijdu0[0][2];
    dRdU[k][0][3][1] = dtijdu1[0][2];
    dRdU[k][0][3][2] = dtijdu2[0][2];
    dRdU[k][0][3][3] = dtijdu3[0][2];
    dRdU[k][0][3][4] = dtijdu4[0][2];
    dRdU[k][0][3][5] = dtijdu5[0][2];
    dRdU[k][0][4][0] = ducgdu0[0] * tij[0][0] + ucg[0] * dtijdu0[0][0] + ducgdu0[1] * tij[0][1] + ucg[1] * dtijdu0[0][1] + ducgdu0[2] * tij[0][2] + ucg[2]
                       * dtijdu0[0][2] - dqjdu0[0];
    dRdU[k][0][4][1] = ducgdu1[0] * tij[0][0] + ucg[0] * dtijdu1[0][0] + ducgdu1[1] * tij[0][1] + ucg[1] * dtijdu1[0][1] + ducgdu1[2] * tij[0][2] + ucg[2]
                       * dtijdu1[0][2] - dqjdu1[0];
    dRdU[k][0][4][2] = ducgdu2[0] * tij[0][0] + ucg[0] * dtijdu2[0][0] + ducgdu2[1] * tij[0][1] + ucg[1] * dtijdu2[0][1] + ducgdu2[2] * tij[0][2] + ucg[2]
                       * dtijdu2[0][2] - dqjdu2[0];
    dRdU[k][0][4][3] = ducgdu3[0] * tij[0][0] + ucg[0] * dtijdu3[0][0] + ducgdu3[1] * tij[0][1] + ucg[1] * dtijdu3[0][1] + ducgdu3[2] * tij[0][2] + ucg[2]
                       * dtijdu3[0][2] - dqjdu3[0];
    dRdU[k][0][4][4] = ducgdu4[0] * tij[0][0] + ucg[0] * dtijdu4[0][0] + ducgdu4[1] * tij[0][1] + ucg[1] * dtijdu4[0][1] + ducgdu4[2] * tij[0][2] + ucg[2]
                       * dtijdu4[0][2] - dqjdu4[0];
    dRdU[k][0][4][5] = ducgdu5[0] * tij[0][0] + ucg[0] * dtijdu5[0][0] + ducgdu5[1] * tij[0][1] + ucg[1] * dtijdu5[0][1] + ducgdu5[2] * tij[0][2] + ucg[2]
                       * dtijdu5[0][2] - dqjdu5[0];
    // dRydU
    dRdU[k][1][0][0] = 0.0;
    dRdU[k][1][0][1] = 0.0;
    dRdU[k][1][0][2] = 0.0;
    dRdU[k][1][0][3] = 0.0;
    dRdU[k][1][0][4] = 0.0;
    dRdU[k][1][0][5] = 0.0;
    dRdU[k][1][1][0] = dtijdu0[0][1];
    dRdU[k][1][1][1] = dtijdu1[0][1];
    dRdU[k][1][1][2] = dtijdu2[0][1];
    dRdU[k][1][1][3] = dtijdu3[0][1];
    dRdU[k][1][1][4] = dtijdu4[0][1];
    dRdU[k][1][1][5] = dtijdu5[0][1];
    dRdU[k][1][2][0] = dtijdu0[1][1];
    dRdU[k][1][2][1] = dtijdu1[1][1];
    dRdU[k][1][2][2] = dtijdu2[1][1];
    dRdU[k][1][2][3] = dtijdu3[1][1];
    dRdU[k][1][2][4] = dtijdu4[1][1];
    dRdU[k][1][2][5] = dtijdu5[1][1];
    dRdU[k][1][3][0] = dtijdu0[1][2];
    dRdU[k][1][3][1] = dtijdu1[1][2];
    dRdU[k][1][3][2] = dtijdu2[1][2];
    dRdU[k][1][3][3] = dtijdu3[1][2];
    dRdU[k][1][3][4] = dtijdu4[1][2];
    dRdU[k][1][3][5] = dtijdu5[1][2];
    dRdU[k][1][4][0] = ducgdu0[0] * tij[0][1] + ucg[0] * dtijdu0[0][1] + ducgdu0[1] * tij[1][1] + ucg[1] * dtijdu0[1][1] + ducgdu0[2] * tij[1][2] + ucg[2]
                       * dtijdu0[1][2] - dqjdu0[1];
    dRdU[k][1][4][1] = ducgdu1[0] * tij[0][1] + ucg[0] * dtijdu1[0][1] + ducgdu1[1] * tij[1][1] + ucg[1] * dtijdu1[1][1] + ducgdu1[2] * tij[1][2] + ucg[2]
                       * dtijdu1[1][2] - dqjdu1[1];
    dRdU[k][1][4][2] = ducgdu2[0] * tij[0][1] + ucg[0] * dtijdu2[0][1] + ducgdu2[1] * tij[1][1] + ucg[1] * dtijdu2[1][1] + ducgdu2[2] * tij[1][2] + ucg[2]
                       * dtijdu2[1][2] - dqjdu2[1];
    dRdU[k][1][4][3] = ducgdu3[0] * tij[0][1] + ucg[0] * dtijdu3[0][1] + ducgdu3[1] * tij[1][1] + ucg[1] * dtijdu3[1][1] + ducgdu3[2] * tij[1][2] + ucg[2]
                       * dtijdu3[1][2] - dqjdu3[1];
    dRdU[k][1][4][4] = ducgdu4[0] * tij[0][1] + ucg[0] * dtijdu4[0][1] + ducgdu4[1] * tij[1][1] + ucg[1] * dtijdu4[1][1] + ducgdu4[2] * tij[1][2] + ucg[2]
                       * dtijdu4[1][2] - dqjdu4[1];
    dRdU[k][1][4][5] = ducgdu5[0] * tij[0][1] + ucg[0] * dtijdu5[0][1] + ducgdu5[1] * tij[1][1] + ucg[1] * dtijdu5[1][1] + ducgdu5[2] * tij[1][2] + ucg[2]
                       * dtijdu5[1][2] - dqjdu5[1];
    // dRzdU
    dRdU[k][2][0][0] = 0.0;
    dRdU[k][2][0][1] = 0.0;
    dRdU[k][2][0][2] = 0.0;
    dRdU[k][2][0][3] = 0.0;
    dRdU[k][2][0][4] = 0.0;
    dRdU[k][2][0][5] = 0.0;
    dRdU[k][2][1][0] = dtijdu0[0][2];
    dRdU[k][2][1][1] = dtijdu1[0][2];
    dRdU[k][2][1][2] = dtijdu2[0][2];
    dRdU[k][2][1][3] = dtijdu3[0][2];
    dRdU[k][2][1][4] = dtijdu4[0][2];
    dRdU[k][2][1][5] = dtijdu5[0][2];
    dRdU[k][2][2][0] = dtijdu0[1][2];
    dRdU[k][2][2][1] = dtijdu1[1][2];
    dRdU[k][2][2][2] = dtijdu2[1][2];
    dRdU[k][2][2][3] = dtijdu3[1][2];
    dRdU[k][2][2][4] = dtijdu4[1][2];
    dRdU[k][2][2][5] = dtijdu5[1][2];
    dRdU[k][2][3][0] = dtijdu0[2][2];
    dRdU[k][2][3][1] = dtijdu1[2][2];
    dRdU[k][2][3][2] = dtijdu2[2][2];
    dRdU[k][2][3][3] = dtijdu3[2][2];
    dRdU[k][2][3][4] = dtijdu4[2][2];
    dRdU[k][2][3][5] = dtijdu5[2][2];
    dRdU[k][2][4][0] = ducgdu0[0] * tij[0][2] + ucg[0] * dtijdu0[0][2] + ducgdu0[1] * tij[1][2] + ucg[1] * dtijdu0[1][2] + ducgdu0[2] * tij[2][2] + ucg[2]
                       * dtijdu0[2][2] - dqjdu0[2];
    dRdU[k][2][4][1] = ducgdu1[0] * tij[0][2] + ucg[0] * dtijdu1[0][2] + ducgdu1[1] * tij[1][2] + ucg[1] * dtijdu1[1][2] + ducgdu1[2] * tij[2][2] + ucg[2]
                       * dtijdu1[2][2] - dqjdu1[2];
    dRdU[k][2][4][2] = ducgdu2[0] * tij[0][2] + ucg[0] * dtijdu2[0][2] + ducgdu2[1] * tij[1][2] + ucg[1] * dtijdu2[1][2] + ducgdu2[2] * tij[2][2] + ucg[2]
                       * dtijdu2[2][2] - dqjdu2[2];
    dRdU[k][2][4][3] = ducgdu3[0] * tij[0][2] + ucg[0] * dtijdu3[0][2] + ducgdu3[1] * tij[1][2] + ucg[1] * dtijdu3[1][2] + ducgdu3[2] * tij[2][2] + ucg[2]
                       * dtijdu3[2][2] - dqjdu3[2];
    dRdU[k][2][4][4] = ducgdu4[0] * tij[0][2] + ucg[0] * dtijdu4[0][2] + ducgdu4[1] * tij[1][2] + ucg[1] * dtijdu4[1][2] + ducgdu4[2] * tij[2][2] + ucg[2]
                       * dtijdu4[2][2] - dqjdu4[2];
    dRdU[k][2][4][5] = ducgdu5[0] * tij[0][2] + ucg[0] * dtijdu5[0][2] + ducgdu5[1] * tij[1][2] + ucg[1] * dtijdu5[1][2] + ducgdu5[2] * tij[2][2] + ucg[2]
                       * dtijdu5[2][2] - dqjdu5[2];



                       
  }
}


//------------------------------------------------------------------------------
// override for strongly coupled SA (neq = dim = 6)
template<>
inline
void NavierStokesTerm::computeJacobianVolumeTermNSUQ(double dp1dxj[4][3], double mu, double dmu[4][6],
                                                   double lambda, double dlambda[4][6], double kappa, double dkappa[4][6],
                                                   double *V[4], double ucg[3], double dudxj[3][3],
                                                   int tag, double dTdxj[3], double (*dRdU)[3][6][6], double RSM[3][3], double rhocg, double mul, double lambdal) {
  double tij[3][3], tij_alt[3][3];

  // std::cout <<"Hit1\n";
  // computeStressTensor(mu, lambda, dudxj, tij);
  // std::cout << "tij[0][0] = " << tij[0][0] <<"\n";
  computeStressTensorUQ(mul, lambdal, dudxj, tij, RSM, rhocg);
  // std::cout << "tij[0][0] = " << tij[0][0] <<"\n";
  // computeStressTensor(mu, lambda, dudxj, tij);
  // double diff;
  // for(int i = 0; i < 3; ++i) {
  //   for(int j = 0; j < 3; ++j) {
  //     diff = std::fabs(tij[i][j] - tij_alt[i][j]);
  //     // if(diff < 1e-1) {
  //     tij[i][j] = tij_alt[i][j];
  //     // } else {
  //     //   ioData.com->fprintf(stdout, "\nCutoff a value of tij = %e; tij_alt = %e; diff = %e;\n", tij[i][j], tij_alt[i][j], diff); //HIT
  //     // }
  //   }
  // }
  double dtijdu0[3][3];
  double dtijdu1[3][3];
  double dtijdu2[3][3];
  double dtijdu3[3][3];
  double dtijdu4[3][3];
  double dtijdu5[3][3];
  double ducgdu0[3];
  double ducgdu1[3];
  double ducgdu2[3];
  double ducgdu3[3];
  double ducgdu4[3];
  double ducgdu5[3];
  double dTduk[6];
  double dTdu0[4];
  double dTdu1[4];
  double dTdu2[4];
  double dTdu3[4];
  double dTdu4[4];
  double dTdu5[4];
  double ddTdxjdu0[3];
  double ddTdxjdu1[3];
  double ddTdxjdu2[3];
  double ddTdxjdu3[3];
  double ddTdxjdu4[3];
  double ddTdxjdu5[3];
  double dqjdu0[3];
  double dqjdu1[3];
  double dqjdu2[3];
  double dqjdu3[3];
  double dqjdu4[3];
  double dqjdu5[3];
  for(int k = 0; k < 4; ++k) {
    ducgdu0[0] = -fourth * V[k][1] / V[k][0];
    ducgdu1[0] = fourth / V[k][0];
    ducgdu2[0] = 0.0;
    ducgdu3[0] = 0.0;
    ducgdu4[0] = 0.0;
    ducgdu5[0] = 0.0;
    ducgdu0[1] = -fourth * V[k][2] / V[k][0];
    ducgdu1[1] = 0.0;
    ducgdu2[1] = fourth / V[k][0];
    ducgdu3[1] = 0.0;
    ducgdu4[1] = 0.0;
    ducgdu5[1] = 0.0;
    ducgdu0[2] = -fourth * V[k][3] / V[k][0];
    ducgdu1[2] = 0.0;
    ducgdu2[2] = 0.0;
    ducgdu3[2] = fourth / V[k][0];
    ducgdu4[2] = 0.0;
    ducgdu5[2] = 0.0;
    // Compute S
    double Snew[3][3];
    Snew[0][0] = dudxj[0][0];
    Snew[1][1] = dudxj[1][1];
    Snew[2][2] = dudxj[2][2];
    Snew[0][1] = 0.5 * (dudxj[0][1] + dudxj[1][0]);
    Snew[0][2] = 0.5 * (dudxj[0][2] + dudxj[2][0]);
    Snew[1][2] = 0.5 * (dudxj[1][2] + dudxj[2][1]);
    Snew[1][0] = Snew[0][1];
    Snew[2][0] = Snew[0][2];
    Snew[2][1] = Snew[1][2];
    dtijdu0[0][0] = 2.0 * dmu[k][0] * Snew[0][0] - 2.0 * mu * dp1dxj[k][0] * V[k][1] / V[k][0] + dlambda[k][0] *
                    (Snew[0][0] + Snew[1][1] + Snew[2][2]) - lambda * (dp1dxj[k][0] * V[k][1] + dp1dxj[k][1] * V[k][2] + dp1dxj[k][2] * V[k][3]) / (V[k][0] * V[k][0]);
    dtijdu1[0][0] = 2.0 * dmu[k][1] * Snew[0][0] + 2.0 * mu * dp1dxj[k][0] / V[k][0] + dlambda[k][1] * (Snew[0][0] + Snew[1][1] + Snew[2][2]) + lambda
                    * dp1dxj[k][0] / V[k][0];
    dtijdu2[0][0] = 2.0 * dmu[k][2] * Snew[0][0] + dlambda[k][2] * (Snew[0][0] + Snew[1][1] + Snew[2][2]) + lambda * dp1dxj[k][1] / V[k][0];
    dtijdu3[0][0] = 2.0 * dmu[k][3] * Snew[0][0] + dlambda[k][3] * (Snew[0][0] + Snew[1][1] + Snew[2][2]) + lambda * dp1dxj[k][2] / V[k][0];
    dtijdu4[0][0] = 2.0 * dmu[k][4] * Snew[0][0] + dlambda[k][4] * (Snew[0][0] + Snew[1][1] + Snew[2][2]);
    dtijdu5[0][0] = 2.0 * dmu[k][5] * Snew[0][0] + dlambda[k][5] * (Snew[0][0] + Snew[1][1] + Snew[2][2]);
    dtijdu0[1][1] = 2.0 * dmu[k][0] * Snew[1][1] - 2.0 * mu * dp1dxj[k][1] * V[k][2] / V[k][0] + dlambda[k][0] *
                    (Snew[0][0] + Snew[1][1] + Snew[2][2]) - lambda * (dp1dxj[k][1] * V[k][2] + dp1dxj[k][0] * V[k][1] + dp1dxj[k][2] * V[k][3]) / (V[k][0] * V[k][0]);
    dtijdu1[1][1] = 2.0 * dmu[k][1] * Snew[1][1] + dlambda[k][1] * (Snew[0][0] + Snew[1][1] + Snew[2][2]) + lambda * dp1dxj[k][0] / V[k][0];
    dtijdu2[1][1] = 2.0 * dmu[k][2] * Snew[1][1] + 2.0 * mu * dp1dxj[k][1] / V[k][0] + dlambda[k][2] * (Snew[0][0] + Snew[1][1] + Snew[2][2]) + lambda
                    * dp1dxj[k][1] / V[k][0];
    dtijdu3[1][1] = 2.0 * dmu[k][3] * Snew[1][1] + dlambda[k][3] * (Snew[0][0] + Snew[1][1] + Snew[2][2]) + lambda * dp1dxj[k][2] / V[k][0];
    dtijdu4[1][1] = 2.0 * dmu[k][4] * Snew[1][1] + dlambda[k][4] * (Snew[0][0] + Snew[1][1] + Snew[2][2]);
    dtijdu5[1][1] = 2.0 * dmu[k][5] * Snew[1][1] + dlambda[k][5] * (Snew[0][0] + Snew[1][1] + Snew[2][2]);
    dtijdu0[2][2] = 2.0 * dmu[k][0] * Snew[2][2] - 2.0 * mu * dp1dxj[k][2] * V[k][3] / V[k][0] + dlambda[k][0] *
                    (Snew[0][0] + Snew[1][1] + Snew[2][2]) - lambda * (dp1dxj[k][2] * V[k][3] + dp1dxj[k][0] * V[k][1] + dp1dxj[k][1] * V[k][2]) / (V[k][0] * V[k][0]);
    dtijdu1[2][2] = 2.0 * dmu[k][1] * Snew[2][2] + dlambda[k][1] * (Snew[0][0] + Snew[1][1] + Snew[2][2]) + lambda * dp1dxj[k][0] / V[k][0];
    dtijdu2[2][2] = 2.0 * dmu[k][2] * Snew[2][2] + dlambda[k][2] * (Snew[0][0] + Snew[1][1] + Snew[2][2]) + lambda * dp1dxj[k][1] / V[k][0];
    dtijdu3[2][2] = 2.0 * dmu[k][3] * Snew[2][2] + 2.0 * mu * dp1dxj[k][2] / V[k][0] + dlambda[k][3] * (Snew[0][0] + Snew[1][1] + Snew[2][2]) + lambda
                    * dp1dxj[k][2] / V[k][0];
    dtijdu4[2][2] = 2.0 * dmu[k][4] * Snew[2][2] + dlambda[k][4] * (Snew[0][0] + Snew[1][1] + Snew[2][2]);
    dtijdu5[2][2] = 2.0 * dmu[k][5] * Snew[2][2] + dlambda[k][5] * (Snew[0][0] + Snew[1][1] + Snew[2][2]);
    dtijdu0[0][1] = -mu * (dp1dxj[k][0] * V[k][2] + dp1dxj[k][1] * V[k][1]) / V[k][0] + dmu[k][0] * (2.0 * Snew[0][1]);
    dtijdu1[0][1] = mu * dp1dxj[k][1] / V[k][0] + dmu[k][1] * (2.0 * Snew[0][1]);
    dtijdu2[0][1] = mu * dp1dxj[k][0] / V[k][0] + dmu[k][2] * (2.0 * Snew[0][1]);
    dtijdu3[0][1] = dmu[k][3] * (2.0 * Snew[0][1]);
    dtijdu4[0][1] = dmu[k][4] * (2.0 * Snew[0][1]);
    dtijdu5[0][1] = dmu[k][5] * (2.0 * Snew[0][1]);
    dtijdu0[0][2] = -mu * (dp1dxj[k][0] * V[k][3] + dp1dxj[k][2] * V[k][1]) / V[k][0] + dmu[k][0] * (2.0 * Snew[0][2]);
    dtijdu1[0][2] = mu * dp1dxj[k][2] / V[k][0] + dmu[k][1] * (2.0 * Snew[0][2]);
    dtijdu2[0][2] = dmu[k][2] * (2.0 * Snew[0][2]);
    dtijdu3[0][2] = mu * dp1dxj[k][0] / V[k][0] + dmu[k][3] * (2.0 * Snew[0][2]);
    dtijdu4[0][2] = dmu[k][4] * (2.0 * Snew[0][2]);
    dtijdu5[0][2] = dmu[k][5] * (2.0 * Snew[0][2]);
    dtijdu0[1][2] = -mu * (dp1dxj[k][1] * V[k][3] + dp1dxj[k][2] * V[k][2]) / V[k][0] + dmu[k][0] * (2.0 * Snew[1][2]);
    dtijdu1[1][2] = dmu[k][1] * (2.0 * Snew[1][2]);
    dtijdu2[1][2] = mu * dp1dxj[k][2] / V[k][0] + dmu[k][2] * (2.0 * Snew[1][2]);
    dtijdu3[1][2] = mu * dp1dxj[k][1] / V[k][0] + dmu[k][3] * (2.0 * Snew[1][2]);
    dtijdu4[1][2] = dmu[k][4] * (2.0 * Snew[1][2]);
    dtijdu5[1][2] = dmu[k][5] * (2.0 * Snew[1][2]);
    dtijdu0[1][0] = dtijdu0[0][1];
    dtijdu1[1][0] = dtijdu1[0][1];
    dtijdu2[1][0] = dtijdu2[0][1];
    dtijdu3[1][0] = dtijdu3[0][1];
    dtijdu4[1][0] = dtijdu4[0][1];
    dtijdu5[1][0] = dtijdu5[0][1];
    dtijdu0[2][0] = dtijdu0[0][2];
    dtijdu1[2][0] = dtijdu1[0][2];
    dtijdu2[2][0] = dtijdu2[0][2];
    dtijdu3[2][0] = dtijdu3[0][2];
    dtijdu4[2][0] = dtijdu4[0][2];
    dtijdu5[2][0] = dtijdu5[0][2];
    dtijdu0[2][1] = dtijdu0[1][2];
    dtijdu1[2][1] = dtijdu1[1][2];
    dtijdu2[2][1] = dtijdu2[1][2];
    dtijdu3[2][1] = dtijdu3[1][2];
    dtijdu4[2][1] = dtijdu4[1][2];
    dtijdu5[2][1] = dtijdu5[1][2];
    varFcn->computeJacobianTemperature(V[k], dTduk, tag);
    dTdu0[k] = dTduk[0];
    dTdu1[k] = dTduk[1];
    dTdu2[k] = dTduk[2];
    dTdu3[k] = dTduk[3];
    dTdu4[k] = dTduk[4];
    dTdu5[k] = dTduk[5];
    ddTdxjdu0[0] = dp1dxj[k][0] * dTdu0[k];
    ddTdxjdu1[0] = dp1dxj[k][0] * dTdu1[k];
    ddTdxjdu2[0] = dp1dxj[k][0] * dTdu2[k];
    ddTdxjdu3[0] = dp1dxj[k][0] * dTdu3[k];
    ddTdxjdu4[0] = dp1dxj[k][0] * dTdu4[k];
    ddTdxjdu5[0] = dp1dxj[k][0] * dTdu5[k];
    ddTdxjdu0[1] = dp1dxj[k][1] * dTdu0[k];
    ddTdxjdu1[1] = dp1dxj[k][1] * dTdu1[k];
    ddTdxjdu2[1] = dp1dxj[k][1] * dTdu2[k];
    ddTdxjdu3[1] = dp1dxj[k][1] * dTdu3[k];
    ddTdxjdu4[1] = dp1dxj[k][1] * dTdu4[k];
    ddTdxjdu5[1] = dp1dxj[k][1] * dTdu5[k];
    ddTdxjdu0[2] = dp1dxj[k][2] * dTdu0[k];
    ddTdxjdu1[2] = dp1dxj[k][2] * dTdu1[k];
    ddTdxjdu2[2] = dp1dxj[k][2] * dTdu2[k];
    ddTdxjdu3[2] = dp1dxj[k][2] * dTdu3[k];
    ddTdxjdu4[2] = dp1dxj[k][2] * dTdu4[k];
    ddTdxjdu5[2] = dp1dxj[k][2] * dTdu5[k];
    dqjdu0[0] = -dkappa[k][0] * dTdxj[0] - kappa * ddTdxjdu0[0];
    dqjdu1[0] = -dkappa[k][1] * dTdxj[0] - kappa * ddTdxjdu1[0];
    dqjdu2[0] = -dkappa[k][2] * dTdxj[0] - kappa * ddTdxjdu2[0];
    dqjdu3[0] = -dkappa[k][3] * dTdxj[0] - kappa * ddTdxjdu3[0];
    dqjdu4[0] = -dkappa[k][4] * dTdxj[0] - kappa * ddTdxjdu4[0];
    dqjdu5[0] = -dkappa[k][5] * dTdxj[0] - kappa * ddTdxjdu5[0];
    dqjdu0[1] = -dkappa[k][0] * dTdxj[1] - kappa * ddTdxjdu0[1];
    dqjdu1[1] = -dkappa[k][1] * dTdxj[1] - kappa * ddTdxjdu1[1];
    dqjdu2[1] = -dkappa[k][2] * dTdxj[1] - kappa * ddTdxjdu2[1];
    dqjdu3[1] = -dkappa[k][3] * dTdxj[1] - kappa * ddTdxjdu3[1];
    dqjdu4[1] = -dkappa[k][4] * dTdxj[1] - kappa * ddTdxjdu4[1];
    dqjdu5[1] = -dkappa[k][5] * dTdxj[1] - kappa * ddTdxjdu5[1];
    dqjdu0[2] = -dkappa[k][0] * dTdxj[2] - kappa * ddTdxjdu0[2];
    dqjdu1[2] = -dkappa[k][1] * dTdxj[2] - kappa * ddTdxjdu1[2];
    dqjdu2[2] = -dkappa[k][2] * dTdxj[2] - kappa * ddTdxjdu2[2];
    dqjdu3[2] = -dkappa[k][3] * dTdxj[2] - kappa * ddTdxjdu3[2];
    dqjdu4[2] = -dkappa[k][4] * dTdxj[2] - kappa * ddTdxjdu4[2];
    dqjdu5[2] = -dkappa[k][5] * dTdxj[2] - kappa * ddTdxjdu5[2];
    // dRxdU
    dRdU[k][0][0][0] = 0.0;
    dRdU[k][0][0][1] = 0.0;
    dRdU[k][0][0][2] = 0.0;
    dRdU[k][0][0][3] = 0.0;
    dRdU[k][0][0][4] = 0.0;
    dRdU[k][0][0][5] = 0.0;
    dRdU[k][0][1][0] = dtijdu0[0][0];
    dRdU[k][0][1][1] = dtijdu1[0][0];
    dRdU[k][0][1][2] = dtijdu2[0][0];
    dRdU[k][0][1][3] = dtijdu3[0][0];
    dRdU[k][0][1][4] = dtijdu4[0][0];
    dRdU[k][0][1][5] = dtijdu5[0][0];
    dRdU[k][0][2][0] = dtijdu0[0][1];
    dRdU[k][0][2][1] = dtijdu1[0][1];
    dRdU[k][0][2][2] = dtijdu2[0][1];
    dRdU[k][0][2][3] = dtijdu3[0][1];
    dRdU[k][0][2][4] = dtijdu4[0][1];
    dRdU[k][0][2][5] = dtijdu5[0][1];
    dRdU[k][0][3][0] = dtijdu0[0][2];
    dRdU[k][0][3][1] = dtijdu1[0][2];
    dRdU[k][0][3][2] = dtijdu2[0][2];
    dRdU[k][0][3][3] = dtijdu3[0][2];
    dRdU[k][0][3][4] = dtijdu4[0][2];
    dRdU[k][0][3][5] = dtijdu5[0][2];
    dRdU[k][0][4][0] = ducgdu0[0] * tij[0][0] + ucg[0] * dtijdu0[0][0] + ducgdu0[1] * tij[0][1] + ucg[1] * dtijdu0[0][1] + ducgdu0[2] * tij[0][2] + ucg[2]
                       * dtijdu0[0][2] - dqjdu0[0];
    dRdU[k][0][4][1] = ducgdu1[0] * tij[0][0] + ucg[0] * dtijdu1[0][0] + ducgdu1[1] * tij[0][1] + ucg[1] * dtijdu1[0][1] + ducgdu1[2] * tij[0][2] + ucg[2]
                       * dtijdu1[0][2] - dqjdu1[0];
    dRdU[k][0][4][2] = ducgdu2[0] * tij[0][0] + ucg[0] * dtijdu2[0][0] + ducgdu2[1] * tij[0][1] + ucg[1] * dtijdu2[0][1] + ducgdu2[2] * tij[0][2] + ucg[2]
                       * dtijdu2[0][2] - dqjdu2[0];
    dRdU[k][0][4][3] = ducgdu3[0] * tij[0][0] + ucg[0] * dtijdu3[0][0] + ducgdu3[1] * tij[0][1] + ucg[1] * dtijdu3[0][1] + ducgdu3[2] * tij[0][2] + ucg[2]
                       * dtijdu3[0][2] - dqjdu3[0];
    dRdU[k][0][4][4] = ducgdu4[0] * tij[0][0] + ucg[0] * dtijdu4[0][0] + ducgdu4[1] * tij[0][1] + ucg[1] * dtijdu4[0][1] + ducgdu4[2] * tij[0][2] + ucg[2]
                       * dtijdu4[0][2] - dqjdu4[0];
    dRdU[k][0][4][5] = ducgdu5[0] * tij[0][0] + ucg[0] * dtijdu5[0][0] + ducgdu5[1] * tij[0][1] + ucg[1] * dtijdu5[0][1] + ducgdu5[2] * tij[0][2] + ucg[2]
                       * dtijdu5[0][2] - dqjdu5[0];
    // dRydU
    dRdU[k][1][0][0] = 0.0;
    dRdU[k][1][0][1] = 0.0;
    dRdU[k][1][0][2] = 0.0;
    dRdU[k][1][0][3] = 0.0;
    dRdU[k][1][0][4] = 0.0;
    dRdU[k][1][0][5] = 0.0;
    dRdU[k][1][1][0] = dtijdu0[0][1];
    dRdU[k][1][1][1] = dtijdu1[0][1];
    dRdU[k][1][1][2] = dtijdu2[0][1];
    dRdU[k][1][1][3] = dtijdu3[0][1];
    dRdU[k][1][1][4] = dtijdu4[0][1];
    dRdU[k][1][1][5] = dtijdu5[0][1];
    dRdU[k][1][2][0] = dtijdu0[1][1];
    dRdU[k][1][2][1] = dtijdu1[1][1];
    dRdU[k][1][2][2] = dtijdu2[1][1];
    dRdU[k][1][2][3] = dtijdu3[1][1];
    dRdU[k][1][2][4] = dtijdu4[1][1];
    dRdU[k][1][2][5] = dtijdu5[1][1];
    dRdU[k][1][3][0] = dtijdu0[1][2];
    dRdU[k][1][3][1] = dtijdu1[1][2];
    dRdU[k][1][3][2] = dtijdu2[1][2];
    dRdU[k][1][3][3] = dtijdu3[1][2];
    dRdU[k][1][3][4] = dtijdu4[1][2];
    dRdU[k][1][3][5] = dtijdu5[1][2];
    dRdU[k][1][4][0] = ducgdu0[0] * tij[0][1] + ucg[0] * dtijdu0[0][1] + ducgdu0[1] * tij[1][1] + ucg[1] * dtijdu0[1][1] + ducgdu0[2] * tij[1][2] + ucg[2]
                       * dtijdu0[1][2] - dqjdu0[1];
    dRdU[k][1][4][1] = ducgdu1[0] * tij[0][1] + ucg[0] * dtijdu1[0][1] + ducgdu1[1] * tij[1][1] + ucg[1] * dtijdu1[1][1] + ducgdu1[2] * tij[1][2] + ucg[2]
                       * dtijdu1[1][2] - dqjdu1[1];
    dRdU[k][1][4][2] = ducgdu2[0] * tij[0][1] + ucg[0] * dtijdu2[0][1] + ducgdu2[1] * tij[1][1] + ucg[1] * dtijdu2[1][1] + ducgdu2[2] * tij[1][2] + ucg[2]
                       * dtijdu2[1][2] - dqjdu2[1];
    dRdU[k][1][4][3] = ducgdu3[0] * tij[0][1] + ucg[0] * dtijdu3[0][1] + ducgdu3[1] * tij[1][1] + ucg[1] * dtijdu3[1][1] + ducgdu3[2] * tij[1][2] + ucg[2]
                       * dtijdu3[1][2] - dqjdu3[1];
    dRdU[k][1][4][4] = ducgdu4[0] * tij[0][1] + ucg[0] * dtijdu4[0][1] + ducgdu4[1] * tij[1][1] + ucg[1] * dtijdu4[1][1] + ducgdu4[2] * tij[1][2] + ucg[2]
                       * dtijdu4[1][2] - dqjdu4[1];
    dRdU[k][1][4][5] = ducgdu5[0] * tij[0][1] + ucg[0] * dtijdu5[0][1] + ducgdu5[1] * tij[1][1] + ucg[1] * dtijdu5[1][1] + ducgdu5[2] * tij[1][2] + ucg[2]
                       * dtijdu5[1][2] - dqjdu5[1];
    // dRzdU
    dRdU[k][2][0][0] = 0.0;
    dRdU[k][2][0][1] = 0.0;
    dRdU[k][2][0][2] = 0.0;
    dRdU[k][2][0][3] = 0.0;
    dRdU[k][2][0][4] = 0.0;
    dRdU[k][2][0][5] = 0.0;
    dRdU[k][2][1][0] = dtijdu0[0][2];
    dRdU[k][2][1][1] = dtijdu1[0][2];
    dRdU[k][2][1][2] = dtijdu2[0][2];
    dRdU[k][2][1][3] = dtijdu3[0][2];
    dRdU[k][2][1][4] = dtijdu4[0][2];
    dRdU[k][2][1][5] = dtijdu5[0][2];
    dRdU[k][2][2][0] = dtijdu0[1][2];
    dRdU[k][2][2][1] = dtijdu1[1][2];
    dRdU[k][2][2][2] = dtijdu2[1][2];
    dRdU[k][2][2][3] = dtijdu3[1][2];
    dRdU[k][2][2][4] = dtijdu4[1][2];
    dRdU[k][2][2][5] = dtijdu5[1][2];
    dRdU[k][2][3][0] = dtijdu0[2][2];
    dRdU[k][2][3][1] = dtijdu1[2][2];
    dRdU[k][2][3][2] = dtijdu2[2][2];
    dRdU[k][2][3][3] = dtijdu3[2][2];
    dRdU[k][2][3][4] = dtijdu4[2][2];
    dRdU[k][2][3][5] = dtijdu5[2][2];
    dRdU[k][2][4][0] = ducgdu0[0] * tij[0][2] + ucg[0] * dtijdu0[0][2] + ducgdu0[1] * tij[1][2] + ucg[1] * dtijdu0[1][2] + ducgdu0[2] * tij[2][2] + ucg[2]
                       * dtijdu0[2][2] - dqjdu0[2];
    dRdU[k][2][4][1] = ducgdu1[0] * tij[0][2] + ucg[0] * dtijdu1[0][2] + ducgdu1[1] * tij[1][2] + ucg[1] * dtijdu1[1][2] + ducgdu1[2] * tij[2][2] + ucg[2]
                       * dtijdu1[2][2] - dqjdu1[2];
    dRdU[k][2][4][2] = ducgdu2[0] * tij[0][2] + ucg[0] * dtijdu2[0][2] + ducgdu2[1] * tij[1][2] + ucg[1] * dtijdu2[1][2] + ducgdu2[2] * tij[2][2] + ucg[2]
                       * dtijdu2[2][2] - dqjdu2[2];
    dRdU[k][2][4][3] = ducgdu3[0] * tij[0][2] + ucg[0] * dtijdu3[0][2] + ducgdu3[1] * tij[1][2] + ucg[1] * dtijdu3[1][2] + ducgdu3[2] * tij[2][2] + ucg[2]
                       * dtijdu3[2][2] - dqjdu3[2];
    dRdU[k][2][4][4] = ducgdu4[0] * tij[0][2] + ucg[0] * dtijdu4[0][2] + ducgdu4[1] * tij[1][2] + ucg[1] * dtijdu4[1][2] + ducgdu4[2] * tij[2][2] + ucg[2]
                       * dtijdu4[2][2] - dqjdu4[2];
    dRdU[k][2][4][5] = ducgdu5[0] * tij[0][2] + ucg[0] * dtijdu5[0][2] + ducgdu5[1] * tij[1][2] + ucg[1] * dtijdu5[1][2] + ducgdu5[2] * tij[2][2] + ucg[2]
                       * dtijdu5[2][2] - dqjdu5[2];



                       
  }
}

//------------------------------------------------------------------------------
// override for strongly coupled KE (neq = dim = 7)
template<>
inline
void NavierStokesTerm::computeJacobianVolumeTermNSUQ(double dp1dxj[4][3], double mu, double dmu[4][7],
                                                   double lambda, double dlambda[4][7], double kappa, double dkappa[4][7],
                                                   double *V[4], double ucg[3], double dudxj[3][3],
                                                   int tag, double dTdxj[3], double (*dRdU)[3][7][7], double RSM[3][3], double rhocg, double mul, double lambdal) {
  double tij[3][3];
  computeStressTensor(mu, lambda, dudxj, tij);
  computeStressTensorUQ(mul, lambdal, dudxj, tij, RSM, rhocg);
  double dtijdu0[3][3];
  double dtijdu1[3][3];
  double dtijdu2[3][3];
  double dtijdu3[3][3];
  double dtijdu4[3][3];
  double dtijdu5[3][3];
  double dtijdu6[3][3];
  double ducgdu0[3];
  double ducgdu1[3];
  double ducgdu2[3];
  double ducgdu3[3];
  double ducgdu4[3];
  double ducgdu5[3];
  double ducgdu6[3];
  double dTduk[7];
  double dTdu0[4];
  double dTdu1[4];
  double dTdu2[4];
  double dTdu3[4];
  double dTdu4[4];
  double dTdu5[4];
  double dTdu6[4];
  double ddTdxjdu0[3];
  double ddTdxjdu1[3];
  double ddTdxjdu2[3];
  double ddTdxjdu3[3];
  double ddTdxjdu4[3];
  double ddTdxjdu5[3];
  double ddTdxjdu6[3];
  double dqjdu0[3];
  double dqjdu1[3];
  double dqjdu2[3];
  double dqjdu3[3];
  double dqjdu4[3];
  double dqjdu5[3];
  double dqjdu6[3];
  for(int k = 0; k < 4; ++k) {
    ducgdu0[0] = -fourth * V[k][1] / V[k][0];
    ducgdu1[0] = fourth / V[k][0];
    ducgdu2[0] = 0.0;
    ducgdu3[0] = 0.0;
    ducgdu4[0] = 0.0;
    ducgdu5[0] = 0.0;
    ducgdu6[0] = 0.0;
    ducgdu0[1] = -fourth * V[k][2] / V[k][0];
    ducgdu1[1] = 0.0;
    ducgdu2[1] = fourth / V[k][0];
    ducgdu3[1] = 0.0;
    ducgdu4[1] = 0.0;
    ducgdu5[1] = 0.0;
    ducgdu6[1] = 0.0;
    ducgdu0[2] = -fourth * V[k][3] / V[k][0];
    ducgdu1[2] = 0.0;
    ducgdu2[2] = 0.0;
    ducgdu3[2] = fourth / V[k][0];
    ducgdu4[2] = 0.0;
    ducgdu5[2] = 0.0;
    ducgdu6[2] = 0.0;
    dtijdu0[0][0] = 2.0 * dmu[k][0] * dudxj[0][0] - 2.0 * mu * dp1dxj[k][0] * V[k][1] / V[k][0] + dlambda[k][0] *
                    (dudxj[0][0] + dudxj[1][1] + dudxj[2][2]) - lambda * (dp1dxj[k][0] * V[k][1] + dp1dxj[k][1] * V[k][2] + dp1dxj[k][2] * V[k][3]) / V[k][0];
    dtijdu1[0][0] = 2.0 * dmu[k][1] * dudxj[0][0] + 2.0 * mu * dp1dxj[k][0] / V[k][0] + dlambda[k][1] * (dudxj[0][0] + dudxj[1][1] + dudxj[2][2]) + lambda
                    * dp1dxj[k][0] / V[k][0];
    dtijdu2[0][0] = 2.0 * dmu[k][2] * dudxj[0][0] + dlambda[k][2] * (dudxj[0][0] + dudxj[1][1] + dudxj[2][2]) + lambda * dp1dxj[k][1] / V[k][0];
    dtijdu3[0][0] = 2.0 * dmu[k][3] * dudxj[0][0] + dlambda[k][3] * (dudxj[0][0] + dudxj[1][1] + dudxj[2][2]) + lambda * dp1dxj[k][2] / V[k][0];
    dtijdu4[0][0] = 2.0 * dmu[k][4] * dudxj[0][0] + dlambda[k][4] * (dudxj[0][0] + dudxj[1][1] + dudxj[2][2]);
    dtijdu5[0][0] = 2.0 * dmu[k][5] * dudxj[0][0] + dlambda[k][5] * (dudxj[0][0] + dudxj[1][1] + dudxj[2][2]);
    dtijdu6[0][0] = 2.0 * dmu[k][6] * dudxj[0][0] + dlambda[k][6] * (dudxj[0][0] + dudxj[1][1] + dudxj[2][2]);
    dtijdu0[1][1] = 2.0 * dmu[k][0] * dudxj[1][1] - 2.0 * mu * dp1dxj[k][1] * V[k][2] / V[k][0] + dlambda[k][0] *
                    (dudxj[0][0] + dudxj[1][1] + dudxj[2][2]) - lambda * (dp1dxj[k][1] * V[k][2] + dp1dxj[k][0] * V[k][1] + dp1dxj[k][2] * V[k][3]) / V[k][0];
    dtijdu1[1][1] = 2.0 * dmu[k][1] * dudxj[1][1] + dlambda[k][1] * (dudxj[0][0] + dudxj[1][1] + dudxj[2][2]) + lambda * dp1dxj[k][0] / V[k][0];
    dtijdu2[1][1] = 2.0 * dmu[k][2] * dudxj[1][1] + 2.0 * mu * dp1dxj[k][1] / V[k][0] + dlambda[k][2] * (dudxj[0][0] + dudxj[1][1] + dudxj[2][2]) + lambda
                    * dp1dxj[k][1] / V[k][0];
    dtijdu3[1][1] = 2.0 * dmu[k][3] * dudxj[1][1] + dlambda[k][3] * (dudxj[0][0] + dudxj[1][1] + dudxj[2][2]) + lambda * dp1dxj[k][2] / V[k][0];
    dtijdu4[1][1] = 2.0 * dmu[k][4] * dudxj[1][1] + dlambda[k][4] * (dudxj[0][0] + dudxj[1][1] + dudxj[2][2]);
    dtijdu5[1][1] = 2.0 * dmu[k][5] * dudxj[1][1] + dlambda[k][5] * (dudxj[0][0] + dudxj[1][1] + dudxj[2][2]);
    dtijdu6[1][1] = 2.0 * dmu[k][6] * dudxj[1][1] + dlambda[k][6] * (dudxj[0][0] + dudxj[1][1] + dudxj[2][2]);
    dtijdu0[2][2] = 2.0 * dmu[k][0] * dudxj[2][2] - 2.0 * mu * dp1dxj[k][2] * V[k][3] / V[k][0] + dlambda[k][0] *
                    (dudxj[0][0] + dudxj[1][1] + dudxj[2][2]) - lambda * (dp1dxj[k][2] * V[k][3] + dp1dxj[k][0] * V[k][1] + dp1dxj[k][1] * V[k][2]) / V[k][0];
    dtijdu1[2][2] = 2.0 * dmu[k][1] * dudxj[2][2] + dlambda[k][1] * (dudxj[0][0] + dudxj[1][1] + dudxj[2][2]) + lambda * dp1dxj[k][0] / V[k][0];
    dtijdu2[2][2] = 2.0 * dmu[k][2] * dudxj[2][2] + dlambda[k][2] * (dudxj[0][0] + dudxj[1][1] + dudxj[2][2]) + lambda * dp1dxj[k][1] / V[k][0];
    dtijdu3[2][2] = 2.0 * dmu[k][3] * dudxj[2][2] + 2.0 * mu * dp1dxj[k][2] / V[k][0] + dlambda[k][3] * (dudxj[0][0] + dudxj[1][1] + dudxj[2][2]) + lambda
                    * dp1dxj[k][2] / V[k][0];
    dtijdu4[2][2] = 2.0 * dmu[k][4] * dudxj[2][2] + dlambda[k][4] * (dudxj[0][0] + dudxj[1][1] + dudxj[2][2]);
    dtijdu5[2][2] = 2.0 * dmu[k][5] * dudxj[2][2] + dlambda[k][5] * (dudxj[0][0] + dudxj[1][1] + dudxj[2][2]);
    dtijdu6[2][2] = 2.0 * dmu[k][6] * dudxj[2][2] + dlambda[k][6] * (dudxj[0][0] + dudxj[1][1] + dudxj[2][2]);
    dtijdu0[0][1] = -mu * (dp1dxj[k][0] * V[k][2] + dp1dxj[k][1] * V[k][1]) / V[k][0] + dmu[k][0] * (dudxj[1][0] + dudxj[0][1]);
    dtijdu1[0][1] = mu * dp1dxj[k][1] / V[k][0] + dmu[k][1] * (dudxj[1][0] + dudxj[0][1]);
    dtijdu2[0][1] = mu * dp1dxj[k][0] / V[k][0] + dmu[k][2] * (dudxj[1][0] + dudxj[0][1]);
    dtijdu3[0][1] = dmu[k][3] * (dudxj[1][0] + dudxj[0][1]);
    dtijdu4[0][1] = dmu[k][4] * (dudxj[1][0] + dudxj[0][1]);
    dtijdu5[0][1] = dmu[k][5] * (dudxj[1][0] + dudxj[0][1]);
    dtijdu6[0][1] = dmu[k][6] * (dudxj[1][0] + dudxj[0][1]);
    dtijdu0[0][2] = -mu * (dp1dxj[k][0] * V[k][3] + dp1dxj[k][2] * V[k][1]) / V[k][0] + dmu[k][0] * (dudxj[2][0] + dudxj[0][2]);
    dtijdu1[0][2] = mu * dp1dxj[k][2] / V[k][0] + dmu[k][1] * (dudxj[2][0] + dudxj[0][2]);
    dtijdu2[0][2] = dmu[k][2] * (dudxj[2][0] + dudxj[0][2]);
    dtijdu3[0][2] = mu * dp1dxj[k][0] / V[k][0] + dmu[k][3] * (dudxj[2][0] + dudxj[0][2]);
    dtijdu4[0][2] = dmu[k][4] * (dudxj[2][0] + dudxj[0][2]);
    dtijdu5[0][2] = dmu[k][5] * (dudxj[2][0] + dudxj[0][2]);
    dtijdu6[0][2] = dmu[k][6] * (dudxj[2][0] + dudxj[0][2]);
    dtijdu0[1][2] = -mu * (dp1dxj[k][1] * V[k][3] + dp1dxj[k][2] * V[k][2]) / V[k][0] + dmu[k][0] * (dudxj[2][1] + dudxj[1][2]);
    dtijdu1[1][2] = dmu[k][1] * (dudxj[2][1] + dudxj[1][2]);
    dtijdu2[1][2] = mu * dp1dxj[k][2] / V[k][0] + dmu[k][2] * (dudxj[2][1] + dudxj[1][2]);
    dtijdu3[1][2] = mu * dp1dxj[k][1] / V[k][0] + dmu[k][3] * (dudxj[2][1] + dudxj[1][2]);
    dtijdu4[1][2] = dmu[k][4] * (dudxj[2][1] + dudxj[1][2]);
    dtijdu5[1][2] = dmu[k][5] * (dudxj[2][1] + dudxj[1][2]);
    dtijdu6[1][2] = dmu[k][6] * (dudxj[2][1] + dudxj[1][2]);
    dtijdu0[1][0] = dtijdu0[0][1];
    dtijdu1[1][0] = dtijdu1[0][1];
    dtijdu2[1][0] = dtijdu2[0][1];
    dtijdu3[1][0] = dtijdu3[0][1];
    dtijdu4[1][0] = dtijdu4[0][1];
    dtijdu5[1][0] = dtijdu5[0][1];
    dtijdu6[1][0] = dtijdu6[0][1];
    dtijdu0[2][0] = dtijdu0[0][2];
    dtijdu1[2][0] = dtijdu1[0][2];
    dtijdu2[2][0] = dtijdu2[0][2];
    dtijdu3[2][0] = dtijdu3[0][2];
    dtijdu4[2][0] = dtijdu4[0][2];
    dtijdu5[2][0] = dtijdu5[0][2];
    dtijdu6[2][0] = dtijdu6[0][2];
    dtijdu0[2][1] = dtijdu0[1][2];
    dtijdu1[2][1] = dtijdu1[1][2];
    dtijdu2[2][1] = dtijdu2[1][2];
    dtijdu3[2][1] = dtijdu3[1][2];
    dtijdu4[2][1] = dtijdu4[1][2];
    dtijdu5[2][1] = dtijdu5[1][2];
    dtijdu6[2][1] = dtijdu6[1][2];
    varFcn->computeJacobianTemperature(V[k], dTduk, tag);
    dTdu0[k] = dTduk[0];
    dTdu1[k] = dTduk[1];
    dTdu2[k] = dTduk[2];
    dTdu3[k] = dTduk[3];
    dTdu4[k] = dTduk[4];
    dTdu5[k] = dTduk[5];
    dTdu6[k] = dTduk[6];
    ddTdxjdu0[0] = dp1dxj[k][0] * dTdu0[k];
    ddTdxjdu1[0] = dp1dxj[k][0] * dTdu1[k];
    ddTdxjdu2[0] = dp1dxj[k][0] * dTdu2[k];
    ddTdxjdu3[0] = dp1dxj[k][0] * dTdu3[k];
    ddTdxjdu4[0] = dp1dxj[k][0] * dTdu4[k];
    ddTdxjdu5[0] = dp1dxj[k][0] * dTdu5[k];
    ddTdxjdu6[0] = dp1dxj[k][0] * dTdu6[k];
    ddTdxjdu0[1] = dp1dxj[k][1] * dTdu0[k];
    ddTdxjdu1[1] = dp1dxj[k][1] * dTdu1[k];
    ddTdxjdu2[1] = dp1dxj[k][1] * dTdu2[k];
    ddTdxjdu3[1] = dp1dxj[k][1] * dTdu3[k];
    ddTdxjdu4[1] = dp1dxj[k][1] * dTdu4[k];
    ddTdxjdu5[1] = dp1dxj[k][1] * dTdu5[k];
    ddTdxjdu6[1] = dp1dxj[k][1] * dTdu6[k];
    ddTdxjdu0[2] = dp1dxj[k][2] * dTdu0[k];
    ddTdxjdu1[2] = dp1dxj[k][2] * dTdu1[k];
    ddTdxjdu2[2] = dp1dxj[k][2] * dTdu2[k];
    ddTdxjdu3[2] = dp1dxj[k][2] * dTdu3[k];
    ddTdxjdu4[2] = dp1dxj[k][2] * dTdu4[k];
    ddTdxjdu5[2] = dp1dxj[k][2] * dTdu5[k];
    ddTdxjdu6[2] = dp1dxj[k][2] * dTdu6[k];
    dqjdu0[0] = -dkappa[k][0] * dTdxj[0] - kappa * ddTdxjdu0[0];
    dqjdu1[0] = -dkappa[k][1] * dTdxj[0] - kappa * ddTdxjdu1[0];
    dqjdu2[0] = -dkappa[k][2] * dTdxj[0] - kappa * ddTdxjdu2[0];
    dqjdu3[0] = -dkappa[k][3] * dTdxj[0] - kappa * ddTdxjdu3[0];
    dqjdu4[0] = -dkappa[k][4] * dTdxj[0] - kappa * ddTdxjdu4[0];
    dqjdu5[0] = -dkappa[k][5] * dTdxj[0] - kappa * ddTdxjdu5[0];
    dqjdu6[0] = -dkappa[k][6] * dTdxj[0] - kappa * ddTdxjdu6[0];
    dqjdu0[1] = -dkappa[k][0] * dTdxj[1] - kappa * ddTdxjdu0[1];
    dqjdu1[1] = -dkappa[k][1] * dTdxj[1] - kappa * ddTdxjdu1[1];
    dqjdu2[1] = -dkappa[k][2] * dTdxj[1] - kappa * ddTdxjdu2[1];
    dqjdu3[1] = -dkappa[k][3] * dTdxj[1] - kappa * ddTdxjdu3[1];
    dqjdu4[1] = -dkappa[k][4] * dTdxj[1] - kappa * ddTdxjdu4[1];
    dqjdu5[1] = -dkappa[k][5] * dTdxj[1] - kappa * ddTdxjdu5[1];
    dqjdu6[1] = -dkappa[k][6] * dTdxj[1] - kappa * ddTdxjdu6[1];
    dqjdu0[2] = -dkappa[k][0] * dTdxj[2] - kappa * ddTdxjdu0[2];
    dqjdu1[2] = -dkappa[k][1] * dTdxj[2] - kappa * ddTdxjdu1[2];
    dqjdu2[2] = -dkappa[k][2] * dTdxj[2] - kappa * ddTdxjdu2[2];
    dqjdu3[2] = -dkappa[k][3] * dTdxj[2] - kappa * ddTdxjdu3[2];
    dqjdu4[2] = -dkappa[k][4] * dTdxj[2] - kappa * ddTdxjdu4[2];
    dqjdu5[2] = -dkappa[k][5] * dTdxj[2] - kappa * ddTdxjdu5[2];
    dqjdu6[2] = -dkappa[k][6] * dTdxj[2] - kappa * ddTdxjdu6[2];
    // dRxdU
    dRdU[k][0][0][0] = 0.0;
    dRdU[k][0][0][1] = 0.0;
    dRdU[k][0][0][2] = 0.0;
    dRdU[k][0][0][3] = 0.0;
    dRdU[k][0][0][4] = 0.0;
    dRdU[k][0][0][5] = 0.0;
    dRdU[k][0][0][6] = 0.0;
    dRdU[k][0][1][0] = dtijdu0[0][0];
    dRdU[k][0][1][1] = dtijdu1[0][0];
    dRdU[k][0][1][2] = dtijdu2[0][0];
    dRdU[k][0][1][3] = dtijdu3[0][0];
    dRdU[k][0][1][4] = dtijdu4[0][0];
    dRdU[k][0][1][5] = dtijdu5[0][0];
    dRdU[k][0][1][6] = dtijdu6[0][0];
    dRdU[k][0][2][0] = dtijdu0[0][1];
    dRdU[k][0][2][1] = dtijdu1[0][1];
    dRdU[k][0][2][2] = dtijdu2[0][1];
    dRdU[k][0][2][3] = dtijdu3[0][1];
    dRdU[k][0][2][4] = dtijdu4[0][1];
    dRdU[k][0][2][5] = dtijdu5[0][1];
    dRdU[k][0][2][6] = dtijdu6[0][1];
    dRdU[k][0][3][0] = dtijdu0[0][2];
    dRdU[k][0][3][1] = dtijdu1[0][2];
    dRdU[k][0][3][2] = dtijdu2[0][2];
    dRdU[k][0][3][3] = dtijdu3[0][2];
    dRdU[k][0][3][4] = dtijdu4[0][2];
    dRdU[k][0][3][5] = dtijdu5[0][2];
    dRdU[k][0][3][6] = dtijdu6[0][2];
    dRdU[k][0][4][0] = ducgdu0[0] * tij[0][0] + ucg[0] * dtijdu0[0][0] + ducgdu0[1] * tij[0][1] + ucg[1] * dtijdu0[0][1] + ducgdu0[2] * tij[0][2] + ucg[2]
                       * dtijdu0[0][2] - dqjdu0[0];
    dRdU[k][0][4][1] = ducgdu1[0] * tij[0][0] + ucg[0] * dtijdu1[0][0] + ducgdu1[1] * tij[0][1] + ucg[1] * dtijdu1[0][1] + ducgdu1[2] * tij[0][2] + ucg[2]
                       * dtijdu1[0][2] - dqjdu1[0];
    dRdU[k][0][4][2] = ducgdu2[0] * tij[0][0] + ucg[0] * dtijdu2[0][0] + ducgdu2[1] * tij[0][1] + ucg[1] * dtijdu2[0][1] + ducgdu2[2] * tij[0][2] + ucg[2]
                       * dtijdu2[0][2] - dqjdu2[0];
    dRdU[k][0][4][3] = ducgdu3[0] * tij[0][0] + ucg[0] * dtijdu3[0][0] + ducgdu3[1] * tij[0][1] + ucg[1] * dtijdu3[0][1] + ducgdu3[2] * tij[0][2] + ucg[2]
                       * dtijdu3[0][2] - dqjdu3[0];
    dRdU[k][0][4][4] = ducgdu4[0] * tij[0][0] + ucg[0] * dtijdu4[0][0] + ducgdu4[1] * tij[0][1] + ucg[1] * dtijdu4[0][1] + ducgdu4[2] * tij[0][2] + ucg[2]
                       * dtijdu4[0][2] - dqjdu4[0];
    dRdU[k][0][4][5] = ducgdu5[0] * tij[0][0] + ucg[0] * dtijdu5[0][0] + ducgdu5[1] * tij[0][1] + ucg[1] * dtijdu5[0][1] + ducgdu5[2] * tij[0][2] + ucg[2]
                       * dtijdu5[0][2] - dqjdu5[0];
    dRdU[k][0][4][6] = ducgdu6[0] * tij[0][0] + ucg[0] * dtijdu6[0][0] + ducgdu6[1] * tij[0][1] + ucg[1] * dtijdu6[0][1] + ducgdu6[2] * tij[0][2] + ucg[2]
                       * dtijdu6[0][2] - dqjdu6[0];
    // dRydU
    dRdU[k][1][0][0] = 0.0;
    dRdU[k][1][0][1] = 0.0;
    dRdU[k][1][0][2] = 0.0;
    dRdU[k][1][0][3] = 0.0;
    dRdU[k][1][0][4] = 0.0;
    dRdU[k][1][0][5] = 0.0;
    dRdU[k][1][0][6] = 0.0;
    dRdU[k][1][1][0] = dtijdu0[0][1];
    dRdU[k][1][1][1] = dtijdu1[0][1];
    dRdU[k][1][1][2] = dtijdu2[0][1];
    dRdU[k][1][1][3] = dtijdu3[0][1];
    dRdU[k][1][1][4] = dtijdu4[0][1];
    dRdU[k][1][1][5] = dtijdu5[0][1];
    dRdU[k][1][1][6] = dtijdu6[0][1];
    dRdU[k][1][2][0] = dtijdu0[1][1];
    dRdU[k][1][2][1] = dtijdu1[1][1];
    dRdU[k][1][2][2] = dtijdu2[1][1];
    dRdU[k][1][2][3] = dtijdu3[1][1];
    dRdU[k][1][2][4] = dtijdu4[1][1];
    dRdU[k][1][2][5] = dtijdu5[1][1];
    dRdU[k][1][2][6] = dtijdu6[1][1];
    dRdU[k][1][3][0] = dtijdu0[1][2];
    dRdU[k][1][3][1] = dtijdu1[1][2];
    dRdU[k][1][3][2] = dtijdu2[1][2];
    dRdU[k][1][3][3] = dtijdu3[1][2];
    dRdU[k][1][3][4] = dtijdu4[1][2];
    dRdU[k][1][3][5] = dtijdu5[1][2];
    dRdU[k][1][3][6] = dtijdu6[1][2];
    dRdU[k][1][4][0] = ducgdu0[0] * tij[0][1] + ucg[0] * dtijdu0[0][1] + ducgdu0[1] * tij[1][1] + ucg[1] * dtijdu0[1][1] + ducgdu0[2] * tij[1][2] + ucg[2]
                       * dtijdu0[1][2] - dqjdu0[1];
    dRdU[k][1][4][1] = ducgdu1[0] * tij[0][1] + ucg[0] * dtijdu1[0][1] + ducgdu1[1] * tij[1][1] + ucg[1] * dtijdu1[1][1] + ducgdu1[2] * tij[1][2] + ucg[2]
                       * dtijdu1[1][2] - dqjdu1[1];
    dRdU[k][1][4][2] = ducgdu2[0] * tij[0][1] + ucg[0] * dtijdu2[0][1] + ducgdu2[1] * tij[1][1] + ucg[1] * dtijdu2[1][1] + ducgdu2[2] * tij[1][2] + ucg[2]
                       * dtijdu2[1][2] - dqjdu2[1];
    dRdU[k][1][4][3] = ducgdu3[0] * tij[0][1] + ucg[0] * dtijdu3[0][1] + ducgdu3[1] * tij[1][1] + ucg[1] * dtijdu3[1][1] + ducgdu3[2] * tij[1][2] + ucg[2]
                       * dtijdu3[1][2] - dqjdu3[1];
    dRdU[k][1][4][4] = ducgdu4[0] * tij[0][1] + ucg[0] * dtijdu4[0][1] + ducgdu4[1] * tij[1][1] + ucg[1] * dtijdu4[1][1] + ducgdu4[2] * tij[1][2] + ucg[2]
                       * dtijdu4[1][2] - dqjdu4[1];
    dRdU[k][1][4][5] = ducgdu5[0] * tij[0][1] + ucg[0] * dtijdu5[0][1] + ducgdu5[1] * tij[1][1] + ucg[1] * dtijdu5[1][1] + ducgdu5[2] * tij[1][2] + ucg[2]
                       * dtijdu5[1][2] - dqjdu5[1];
    dRdU[k][1][4][6] = ducgdu6[0] * tij[0][1] + ucg[0] * dtijdu6[0][1] + ducgdu6[1] * tij[1][1] + ucg[1] * dtijdu6[1][1] + ducgdu6[2] * tij[1][2] + ucg[2]
                       * dtijdu6[1][2] - dqjdu6[1];
    // dRzdU
    dRdU[k][2][0][0] = 0.0;
    dRdU[k][2][0][1] = 0.0;
    dRdU[k][2][0][2] = 0.0;
    dRdU[k][2][0][3] = 0.0;
    dRdU[k][2][0][4] = 0.0;
    dRdU[k][2][0][5] = 0.0;
    dRdU[k][2][0][6] = 0.0;
    dRdU[k][2][1][0] = dtijdu0[0][2];
    dRdU[k][2][1][1] = dtijdu1[0][2];
    dRdU[k][2][1][2] = dtijdu2[0][2];
    dRdU[k][2][1][3] = dtijdu3[0][2];
    dRdU[k][2][1][4] = dtijdu4[0][2];
    dRdU[k][2][1][5] = dtijdu5[0][2];
    dRdU[k][2][1][6] = dtijdu6[0][2];
    dRdU[k][2][2][0] = dtijdu0[1][2];
    dRdU[k][2][2][1] = dtijdu1[1][2];
    dRdU[k][2][2][2] = dtijdu2[1][2];
    dRdU[k][2][2][3] = dtijdu3[1][2];
    dRdU[k][2][2][4] = dtijdu4[1][2];
    dRdU[k][2][2][5] = dtijdu5[1][2];
    dRdU[k][2][2][6] = dtijdu6[1][2];
    dRdU[k][2][3][0] = dtijdu0[2][2];
    dRdU[k][2][3][1] = dtijdu1[2][2];
    dRdU[k][2][3][2] = dtijdu2[2][2];
    dRdU[k][2][3][3] = dtijdu3[2][2];
    dRdU[k][2][3][4] = dtijdu4[2][2];
    dRdU[k][2][3][5] = dtijdu5[2][2];
    dRdU[k][2][3][6] = dtijdu6[2][2];
    dRdU[k][2][4][0] = ducgdu0[0] * tij[0][2] + ucg[0] * dtijdu0[0][2] + ducgdu0[1] * tij[1][2] + ucg[1] * dtijdu0[1][2] + ducgdu0[2] * tij[2][2] + ucg[2]
                       * dtijdu0[2][2] - dqjdu0[2];
    dRdU[k][2][4][1] = ducgdu1[0] * tij[0][2] + ucg[0] * dtijdu1[0][2] + ducgdu1[1] * tij[1][2] + ucg[1] * dtijdu1[1][2] + ducgdu1[2] * tij[2][2] + ucg[2]
                       * dtijdu1[2][2] - dqjdu1[2];
    dRdU[k][2][4][2] = ducgdu2[0] * tij[0][2] + ucg[0] * dtijdu2[0][2] + ducgdu2[1] * tij[1][2] + ucg[1] * dtijdu2[1][2] + ducgdu2[2] * tij[2][2] + ucg[2]
                       * dtijdu2[2][2] - dqjdu2[2];
    dRdU[k][2][4][3] = ducgdu3[0] * tij[0][2] + ucg[0] * dtijdu3[0][2] + ducgdu3[1] * tij[1][2] + ucg[1] * dtijdu3[1][2] + ducgdu3[2] * tij[2][2] + ucg[2]
                       * dtijdu3[2][2] - dqjdu3[2];
    dRdU[k][2][4][4] = ducgdu4[0] * tij[0][2] + ucg[0] * dtijdu4[0][2] + ducgdu4[1] * tij[1][2] + ucg[1] * dtijdu4[1][2] + ducgdu4[2] * tij[2][2] + ucg[2]
                       * dtijdu4[2][2] - dqjdu4[2];
    dRdU[k][2][4][5] = ducgdu5[0] * tij[0][2] + ucg[0] * dtijdu5[0][2] + ducgdu5[1] * tij[1][2] + ucg[1] * dtijdu5[1][2] + ducgdu5[2] * tij[2][2] + ucg[2]
                       * dtijdu5[2][2] - dqjdu5[2];
    dRdU[k][2][4][6] = ducgdu6[0] * tij[0][2] + ucg[0] * dtijdu6[0][2] + ducgdu6[1] * tij[1][2] + ucg[1] * dtijdu6[1][2] + ducgdu6[2] * tij[2][2] + ucg[2]
                       * dtijdu6[2][2] - dqjdu6[2];
  }
}

//------------------------------------------------------------------------------

template<int neq>
inline
void NavierStokesTerm::computeJacobianVolumeTermNSUQ(double dp1dxj[4][3], double nu, double dnu[4][neq], double mu, double dmu[4][neq],
                                                   double lambda, double dlambda[4][neq], double kappa, double dkappa[4][neq],
                                                   double *V[4], double ucg[3], double dudxj[3][3],
                                                   int tag, double dTdxj[3], double (*dRdU)[3][neq][neq], double RSM[3][3], double rhocg, double mul, double lambdal) {
  double tij[3][3], tij_alt[3][3];
  std::cout <<"Hit2\n";
  computeStressTensorUQ(mul, lambdal, dudxj, tij, RSM, rhocg);
  // computeStressTensor(mu, lambda, dudxj, tij);
  // double diff;
  // for(int i = 0; i < 3; ++i) {
  //   for(int j = 0; j < 3; ++j) {
  //     diff = std::fabs(tij[i][j] - tij_alt[i][j]);
  //     // if(diff < 1e-1) {
  //     tij[i][j] = tij_alt[i][j];
  //     // } else {
  //     //   ioData.com->fprintf(stdout, "\nCutoff a value of tij = %e; tij_alt = %e; diff = %e;\n", tij[i][j], tij_alt[i][j], diff); //Not hit
  //     // }
  //   }
  // }
  double dtijdu0[3][3];
  double dtijdu1[3][3];
  double dtijdu2[3][3];
  double dtijdu3[3][3];
  double dtijdu4[3][3];
  double ducgdu0[3];
  double ducgdu1[3];
  double ducgdu2[3];
  double ducgdu3[3];
  double ducgdu4[3];
  double dTduk[7]; // TODO: use dim for dTduk
  double dTdu0kappa[4];
  double dTdu1kappa[4];
  double dTdu2kappa[4];
  double dTdu3kappa[4];
  double dTdu4kappa[4];
  double dqjdu0[3];
  double dqjdu1[3];
  double dqjdu2[3];
  double dqjdu3[3];
  double dqjdu4[3];

  const double toto = dudxj[0][0] + dudxj[1][1] + dudxj[2][2];

  for(int k = 0; k < 4; ++k) {
    const double invrho = 1 / V[k][0];
    const double mudivrho = mu * invrho;
    const double lambdadivrho = lambda * invrho;
    const double kappadivrho = kappa * invrho;

    ducgdu0[0] = -fourth * V[k][1] * invrho;
    ducgdu1[0] = fourth * invrho;
    ducgdu0[1] = -fourth * V[k][2] * invrho;
    ducgdu2[1] = fourth * invrho;
    ducgdu0[2] = -fourth * V[k][3] * invrho;
    ducgdu3[2] = fourth * invrho;

    dtijdu0[0][0] = 2.0 * dmu[k][0] * dudxj[0][0] - 2.0 * mudivrho * dp1dxj[k][0] * V[k][1] + dlambda[k][0] * toto -
                    lambdadivrho * (dp1dxj[k][0] * V[k][1] + dp1dxj[k][1] * V[k][2] + dp1dxj[k][2] * V[k][3]);
    dtijdu0[0][1] = -mudivrho * (dp1dxj[k][0] * V[k][2] + dp1dxj[k][1] * V[k][1]) + dmu[k][0] * (dudxj[1][0] + dudxj[0][1]);
    dtijdu0[0][2] = -mudivrho * (dp1dxj[k][0] * V[k][3] + dp1dxj[k][2] * V[k][1]) + dmu[k][0] * (dudxj[2][0] + dudxj[0][2]);
    dtijdu0[1][0] = dtijdu0[0][1];
    dtijdu0[1][1] = 2.0 * dmu[k][0] * dudxj[1][1] - 2.0 * mudivrho * dp1dxj[k][1] * V[k][2] + dlambda[k][0] * toto -
                    lambdadivrho * (dp1dxj[k][1] * V[k][2] + dp1dxj[k][0] * V[k][1] + dp1dxj[k][2] * V[k][3]);
    dtijdu0[1][2] = -mudivrho * (dp1dxj[k][1] * V[k][3] + dp1dxj[k][2] * V[k][2]) + dmu[k][0] * (dudxj[2][1] + dudxj[1][2]);
    dtijdu0[2][0] = dtijdu0[0][2];
    dtijdu0[2][1] = dtijdu0[1][2];
    dtijdu0[2][2] = 2.0 * dmu[k][0] * dudxj[2][2] - 2.0 * mudivrho * dp1dxj[k][2] * V[k][3] + dlambda[k][0] * toto -
                    lambdadivrho * (dp1dxj[k][2] * V[k][3] + dp1dxj[k][0] * V[k][1] + dp1dxj[k][1] * V[k][2]);

    dtijdu1[0][0] = 2.0 * dmu[k][1] * dudxj[0][0] + 2.0 * mudivrho * dp1dxj[k][0] + dlambda[k][1] * toto + lambdadivrho * dp1dxj[k][0];
    dtijdu1[0][1] = mudivrho * dp1dxj[k][1] + dmu[k][1] * (dudxj[1][0] + dudxj[0][1]);
    dtijdu1[0][2] = mudivrho * dp1dxj[k][2] + dmu[k][1] * (dudxj[2][0] + dudxj[0][2]);
    dtijdu1[1][0] = dtijdu1[0][1];
    dtijdu1[1][1] = 2.0 * dmu[k][1] * dudxj[1][1] + dlambda[k][1] * toto + lambdadivrho * dp1dxj[k][0];
    dtijdu1[1][2] = dmu[k][1] * (dudxj[2][1] + dudxj[1][2]);
    dtijdu1[2][0] = dtijdu1[0][2];
    dtijdu1[2][1] = dtijdu1[1][2];
    dtijdu1[2][2] = 2.0 * dmu[k][1] * dudxj[2][2] + dlambda[k][1] * toto + lambdadivrho * dp1dxj[k][0];

    dtijdu2[0][0] = 2.0 * dmu[k][2] * dudxj[0][0] + dlambda[k][2] * toto + lambdadivrho * dp1dxj[k][1];
    dtijdu2[0][1] = mudivrho * dp1dxj[k][0] + dmu[k][2] * (dudxj[1][0] + dudxj[0][1]);
    dtijdu2[0][2] = dmu[k][2] * (dudxj[2][0] + dudxj[0][2]);
    dtijdu2[1][0] = dtijdu2[0][1];
    dtijdu2[1][1] = 2.0 * dmu[k][2] * dudxj[1][1] + 2.0 * mudivrho * dp1dxj[k][1] + dlambda[k][2] * toto + lambdadivrho * dp1dxj[k][1];
    dtijdu2[1][2] = mudivrho * dp1dxj[k][2] + dmu[k][2] * (dudxj[2][1] + dudxj[1][2]);
    dtijdu2[2][0] = dtijdu2[0][2];
    dtijdu2[2][1] = dtijdu2[1][2];
    dtijdu2[2][2] = 2.0 * dmu[k][2] * dudxj[2][2] + dlambda[k][2] * toto + lambdadivrho * dp1dxj[k][1];

    dtijdu3[0][0] = 2.0 * dmu[k][3] * dudxj[0][0] + dlambda[k][3] * toto + lambdadivrho * dp1dxj[k][2];
    dtijdu3[0][1] = dmu[k][3] * (dudxj[1][0] + dudxj[0][1]);
    dtijdu3[0][2] = mudivrho * dp1dxj[k][0] + dmu[k][3] * (dudxj[2][0] + dudxj[0][2]);
    dtijdu3[1][0] = dtijdu3[0][1];
    dtijdu3[1][1] = 2.0 * dmu[k][3] * dudxj[1][1] + dlambda[k][3] * toto + lambdadivrho * dp1dxj[k][2];
    dtijdu3[1][2] = mudivrho * dp1dxj[k][1] + dmu[k][3] * (dudxj[2][1] + dudxj[1][2]);
    dtijdu3[2][0] = dtijdu3[0][2];
    dtijdu3[2][1] = dtijdu3[1][2];
    dtijdu3[2][2] = 2.0 * dmu[k][3] * dudxj[2][2] + 2.0 * mudivrho * dp1dxj[k][2] + dlambda[k][3] * toto + lambdadivrho * dp1dxj[k][2];

    dtijdu4[0][0] = 2.0 * dmu[k][4] * dudxj[0][0] + dlambda[k][4] * toto;
    dtijdu4[0][1] = dmu[k][4] * (dudxj[1][0] + dudxj[0][1]);
    dtijdu4[0][2] = dmu[k][4] * (dudxj[2][0] + dudxj[0][2]);
    dtijdu4[1][0] = dtijdu4[0][1];
    dtijdu4[1][1] = 2.0 * dmu[k][4] * dudxj[1][1] + dlambda[k][4] * toto;
    dtijdu4[1][2] = dmu[k][4] * (dudxj[2][1] + dudxj[1][2]);
    dtijdu4[2][0] = dtijdu4[0][2];
    dtijdu4[2][1] = dtijdu4[1][2];
    dtijdu4[2][2] = 2.0 * dmu[k][4] * dudxj[2][2] + dlambda[k][4] * toto;

    varFcn->computeJacobianTemperature(V[k], dTduk, tag);
    dTdu0kappa[k] = kappa * dTduk[0];
    dTdu1kappa[k] = kappa * dTduk[1];
    dTdu2kappa[k] = kappa * dTduk[2];
    dTdu3kappa[k] = kappa * dTduk[3];
    dTdu4kappa[k] = kappa * dTduk[4];
    dqjdu0[0] = -dkappa[k][0] * dTdxj[0] - dp1dxj[k][0] * dTdu0kappa[k];
    dqjdu1[0] = -dkappa[k][1] * dTdxj[0] - dp1dxj[k][0] * dTdu1kappa[k];
    dqjdu2[0] = -dkappa[k][2] * dTdxj[0] - dp1dxj[k][0] * dTdu2kappa[k];
    dqjdu3[0] = -dkappa[k][3] * dTdxj[0] - dp1dxj[k][0] * dTdu3kappa[k];
    dqjdu4[0] = -dkappa[k][4] * dTdxj[0] - dp1dxj[k][0] * dTdu4kappa[k];
    dqjdu0[1] = -dkappa[k][0] * dTdxj[1] - dp1dxj[k][1] * dTdu0kappa[k];
    dqjdu1[1] = -dkappa[k][1] * dTdxj[1] - dp1dxj[k][1] * dTdu1kappa[k];
    dqjdu2[1] = -dkappa[k][2] * dTdxj[1] - dp1dxj[k][1] * dTdu2kappa[k];
    dqjdu3[1] = -dkappa[k][3] * dTdxj[1] - dp1dxj[k][1] * dTdu3kappa[k];
    dqjdu4[1] = -dkappa[k][4] * dTdxj[1] - dp1dxj[k][1] * dTdu4kappa[k];
    dqjdu0[2] = -dkappa[k][0] * dTdxj[2] - dp1dxj[k][2] * dTdu0kappa[k];
    dqjdu1[2] = -dkappa[k][1] * dTdxj[2] - dp1dxj[k][2] * dTdu1kappa[k];
    dqjdu2[2] = -dkappa[k][2] * dTdxj[2] - dp1dxj[k][2] * dTdu2kappa[k];
    dqjdu3[2] = -dkappa[k][3] * dTdxj[2] - dp1dxj[k][2] * dTdu3kappa[k];
    dqjdu4[2] = -dkappa[k][4] * dTdxj[2] - dp1dxj[k][2] * dTdu4kappa[k];
    // computation of dRdU -> dRdU[k][a][b][c]
    // k is the corner of the tetrahedra we are considering
    // a is the coordinate direction: x y z
    // b is the equation we are considering: 0 for mass conservation
    //           1 for x-momentum
    //           2 for y-momentum
    //           3 for z-momentum
    //           4 for energy
    // c is the variable wrt which we derive
    // dRxdU
    dRdU[k][0][0][0] = 0.0;
    dRdU[k][0][0][1] = 0.0;
    dRdU[k][0][0][2] = 0.0;
    dRdU[k][0][0][3] = 0.0;
    dRdU[k][0][0][4] = 0.0;
    dRdU[k][0][1][0] = dtijdu0[0][0];
    dRdU[k][0][1][1] = dtijdu1[0][0];
    dRdU[k][0][1][2] = dtijdu2[0][0];
    dRdU[k][0][1][3] = dtijdu3[0][0];
    dRdU[k][0][1][4] = dtijdu4[0][0];
    dRdU[k][0][2][0] = dtijdu0[0][1];
    dRdU[k][0][2][1] = dtijdu1[0][1];
    dRdU[k][0][2][2] = dtijdu2[0][1];
    dRdU[k][0][2][3] = dtijdu3[0][1];
    dRdU[k][0][2][4] = dtijdu4[0][1];
    dRdU[k][0][3][0] = dtijdu0[0][2];
    dRdU[k][0][3][1] = dtijdu1[0][2];
    dRdU[k][0][3][2] = dtijdu2[0][2];
    dRdU[k][0][3][3] = dtijdu3[0][2];
    dRdU[k][0][3][4] = dtijdu4[0][2];
    dRdU[k][0][4][0] = nu * (ducgdu0[0] * tij[0][0] + ucg[0] * dtijdu0[0][0] + ducgdu0[1] * tij[0][1] + ucg[1] * dtijdu0[0][1] + ducgdu0[2] * tij[0][2] + ucg[2]
                       * dtijdu0[0][2]) + dnu[k][0] * (ucg[0] * tij[0][0] + ucg[1] * tij[1][0] + ucg[2] * tij[2][0]) - dqjdu0[0];
    dRdU[k][0][4][1] = nu * (ducgdu1[0] * tij[0][0] + ucg[0] * dtijdu1[0][0] + ucg[1] * dtijdu1[0][1] + ucg[2]
                       * dtijdu1[0][2]) + dnu[k][1] * (ucg[0] * tij[0][0] + ucg[1] * tij[1][0] + ucg[2] * tij[2][0]) - dqjdu1[0];
    dRdU[k][0][4][2] = nu * (ucg[0] * dtijdu2[0][0] + ducgdu2[1] * tij[0][1] + ucg[1] * dtijdu2[0][1] + ucg[2]
                       * dtijdu2[0][2]) + dnu[k][2] * (ucg[0] * tij[0][0] + ucg[1] * tij[1][0] + ucg[2] * tij[2][0]) - dqjdu2[0];
    dRdU[k][0][4][3] = nu * (ucg[0] * dtijdu3[0][0] + ucg[1] * dtijdu3[0][1] + ducgdu3[2] * tij[0][2] + ucg[2]
                       * dtijdu3[0][2]) + dnu[k][3] * (ucg[0] * tij[0][0] + ucg[1] * tij[1][0] + ucg[2] * tij[2][0]) - dqjdu3[0];
    dRdU[k][0][4][4] = nu * (ucg[0] * dtijdu4[0][0] + ucg[1] * dtijdu4[0][1] + ucg[2]
                       * dtijdu4[0][2]) + dnu[k][4] * (ucg[0] * tij[0][0] + ucg[1] * tij[1][0] + ucg[2] * tij[2][0]) - dqjdu4[0];
    // dRydU
    dRdU[k][1][0][0] = 0.0;
    dRdU[k][1][0][1] = 0.0;
    dRdU[k][1][0][2] = 0.0;
    dRdU[k][1][0][3] = 0.0;
    dRdU[k][1][0][4] = 0.0;
    dRdU[k][1][1][0] = dtijdu0[0][1];
    dRdU[k][1][1][1] = dtijdu1[0][1];
    dRdU[k][1][1][2] = dtijdu2[0][1];
    dRdU[k][1][1][3] = dtijdu3[0][1];
    dRdU[k][1][1][4] = dtijdu4[0][1];
    dRdU[k][1][2][0] = dtijdu0[1][1];
    dRdU[k][1][2][1] = dtijdu1[1][1];
    dRdU[k][1][2][2] = dtijdu2[1][1];
    dRdU[k][1][2][3] = dtijdu3[1][1];
    dRdU[k][1][2][4] = dtijdu4[1][1];
    dRdU[k][1][3][0] = dtijdu0[1][2];
    dRdU[k][1][3][1] = dtijdu1[1][2];
    dRdU[k][1][3][2] = dtijdu2[1][2];
    dRdU[k][1][3][3] = dtijdu3[1][2];
    dRdU[k][1][3][4] = dtijdu4[1][2];
    dRdU[k][1][4][0] = nu * (ducgdu0[0] * tij[0][1] + ucg[0] * dtijdu0[0][1] + ducgdu0[1] * tij[1][1] + ucg[1] * dtijdu0[1][1] + ducgdu0[2] * tij[1][2] + ucg[2]
                       * dtijdu0[1][2]) + dnu[k][0] * (ucg[0] * tij[0][1] + ucg[1] * tij[1][1] + ucg[2] * tij[2][1])  - dqjdu0[1];
    dRdU[k][1][4][1] = nu * (ducgdu1[0] * tij[0][1] + ucg[0] * dtijdu1[0][1] + ucg[1] * dtijdu1[1][1] + ucg[2]
                       * dtijdu1[1][2]) + dnu[k][1] * (ucg[0] * tij[0][1] + ucg[1] * tij[1][1] + ucg[2] * tij[2][1]) - dqjdu1[1];
    dRdU[k][1][4][2] = nu * (ucg[0] * dtijdu2[0][1] + ducgdu2[1] * tij[1][1] + ucg[1] * dtijdu2[1][1] + ucg[2]
                       * dtijdu2[1][2]) + dnu[k][2] * (ucg[0] * tij[0][1] + ucg[1] * tij[1][1] + ucg[2] * tij[2][1]) - dqjdu2[1];
    dRdU[k][1][4][3] = nu * (ucg[0] * dtijdu3[0][1] + ucg[1] * dtijdu3[1][1] + ducgdu3[2] * tij[1][2] + ucg[2]
                       * dtijdu3[1][2]) + dnu[k][3] * (ucg[0] * tij[0][1] + ucg[1] * tij[1][1] + ucg[2] * tij[2][1]) - dqjdu3[1];
    dRdU[k][1][4][4] = nu * (ucg[0] * dtijdu4[0][1] + ucg[1] * dtijdu4[1][1] + ucg[2]
                       * dtijdu4[1][2]) + dnu[k][4] * (ucg[0] * tij[0][1] + ucg[1] * tij[1][1] + ucg[2] * tij[2][1]) - dqjdu4[1];
    // dRzdU
    dRdU[k][2][0][0] = 0.0;
    dRdU[k][2][0][1] = 0.0;
    dRdU[k][2][0][2] = 0.0;
    dRdU[k][2][0][3] = 0.0;
    dRdU[k][2][0][4] = 0.0;
    dRdU[k][2][1][0] = dtijdu0[0][2];
    dRdU[k][2][1][1] = dtijdu1[0][2];
    dRdU[k][2][1][2] = dtijdu2[0][2];
    dRdU[k][2][1][3] = dtijdu3[0][2];
    dRdU[k][2][1][4] = dtijdu4[0][2];
    dRdU[k][2][2][0] = dtijdu0[1][2];
    dRdU[k][2][2][1] = dtijdu1[1][2];
    dRdU[k][2][2][2] = dtijdu2[1][2];
    dRdU[k][2][2][3] = dtijdu3[1][2];
    dRdU[k][2][2][4] = dtijdu4[1][2];
    dRdU[k][2][3][0] = dtijdu0[2][2];
    dRdU[k][2][3][1] = dtijdu1[2][2];
    dRdU[k][2][3][2] = dtijdu2[2][2];
    dRdU[k][2][3][3] = dtijdu3[2][2];
    dRdU[k][2][3][4] = dtijdu4[2][2];
    dRdU[k][2][4][0] = nu * (ducgdu0[0] * tij[0][2] + ucg[0] * dtijdu0[0][2] + ducgdu0[1] * tij[1][2] + ucg[1] * dtijdu0[1][2] + ducgdu0[2] * tij[2][2] + ucg[2]
                       * dtijdu0[2][2]) + dnu[k][0] * (ucg[0] * tij[0][2] + ucg[1] * tij[1][2] + ucg[2] * tij[2][2]) - dqjdu0[2];
    dRdU[k][2][4][1] = nu * (ducgdu1[0] * tij[0][2] + ucg[0] * dtijdu1[0][2] + ucg[1] * dtijdu1[1][2] + ucg[2]
                       * dtijdu1[2][2]) + dnu[k][1] * (ucg[0] * tij[0][2] + ucg[1] * tij[1][2] + ucg[2] * tij[2][2]) - dqjdu1[2];
    dRdU[k][2][4][2] = nu * (ucg[0] * dtijdu2[0][2] + ducgdu2[1] * tij[1][2] + ucg[1] * dtijdu2[1][2] + ucg[2]
                       * dtijdu2[2][2]) + dnu[k][2] * (ucg[0] * tij[0][2] + ucg[1] * tij[1][2] + ucg[2] * tij[2][2]) - dqjdu2[2];
    dRdU[k][2][4][3] = nu * (ucg[0] * dtijdu3[0][2] + ucg[1] * dtijdu3[1][2] + ducgdu3[2] * tij[2][2] + ucg[2]
                       * dtijdu3[2][2]) + dnu[k][3] * (ucg[0] * tij[0][2] + ucg[1] * tij[1][2] + ucg[2] * tij[2][2]) - dqjdu3[2];
    dRdU[k][2][4][4] = nu * (ucg[0] * dtijdu4[0][2] + ucg[1] * dtijdu4[1][2] + ucg[2]
                       * dtijdu4[2][2]) + dnu[k][4] * (ucg[0] * tij[0][2] + ucg[1] * tij[1][2] + ucg[2] * tij[2][2]) - dqjdu4[2];
  }
}

//------------------------------------------------------------------------------

inline
void NavierStokesTerm::computeSurfaceTermNS(double mu, double lambda, double *Vwall,
                                            double dudxj[3][3], Vec3D& n, double *R) {
  double tij[3][3];
  computeStressTensor(mu, lambda, dudxj, tij);
  R[0] = 0.0;
  R[1] = 0.0;
  R[2] = 0.0;
  R[3] = 0.0;
  R[4] = (Vwall[1] * tij[0][0] + Vwall[2] * tij[1][0] + Vwall[3] * tij[2][0]) * n[0] +
         (Vwall[1] * tij[0][1] + Vwall[2] * tij[1][1] + Vwall[3] * tij[2][1]) * n[1] +
         (Vwall[1] * tij[0][2] + Vwall[2] * tij[1][2] + Vwall[3] * tij[2][2]) * n[2];
}

//------------------------------------------------------------------------------

inline
void NavierStokesTerm::computeDerivativeOfSurfaceTermNS(double mu, double dmu, double lambda, double dlambda,
                                                        double *Vwall, double *dVwall, double dudxj[3][3], double ddudxj[3][3],
                                                        Vec3D& n, Vec3D& dn, double *dR) {
  double tij[3][3];
  computeStressTensor(mu, lambda, dudxj, tij);
  double dtij[3][3];
  computeDerivativeOfStressTensor(mu, dmu, lambda, dlambda, dudxj, ddudxj, dtij);
  dR[0] = 0.0;
  dR[1] = 0.0;
  dR[2] = 0.0;
  dR[3] = 0.0;
  dR[4] = (Vwall[1] * dtij[0][0] + dVwall[1] * tij[0][0] + Vwall[2] * dtij[1][0] + dVwall[2] * tij[1][0] + Vwall[3] * dtij[2][0] + dVwall[3] * tij[2][0]) * n[0] +
          (Vwall[1] * tij[0][0] + Vwall[2] * tij[1][0] + Vwall[3] * tij[2][0]) * dn[0] +
          (Vwall[1] * dtij[0][1] + dVwall[1] * tij[0][1] + Vwall[2] * dtij[1][1] + dVwall[2] * tij[1][1] + Vwall[3] * dtij[2][1] + dVwall[3] * tij[2][1]) * n[1] +
          (Vwall[1] * tij[0][1] + Vwall[2] * tij[1][1] + Vwall[3] * tij[2][1]) * dn[1] +
          (Vwall[1] * dtij[0][2] + dVwall[1] * tij[0][2] + Vwall[2] * dtij[1][2] + dVwall[2] * tij[1][2] + Vwall[3] * dtij[2][2] + dVwall[3] * tij[2][2]) * n[2] +
          (Vwall[1] * tij[0][2] + Vwall[2] * tij[1][2] + Vwall[3] * tij[2][2]) * dn[2];
}

//------------------------------------------------------------------------------

inline
void NavierStokesTerm::computeDerivativeOperatorsOfSurfaceTermNS(double mu, double lambda, double *Vwall,
                                                                 double dudxj[3][3], Vec3D& n, double dRdmu[],
                                                                 double dRdlambda[], double dRdVwall[][5],
                                                                 double dRddudxj[][3][3], double dRdn[][3]) {
  double tij[3][3];
  computeStressTensor(mu, lambda, dudxj, tij);
  double dtijdlambda[3][3] = {0}, dtijddudxj[3][3][3][3] = {0}, dtijdmu[3][3] = {0};
  computeDerivativeOperatorsOfStressTensor(mu, lambda, dudxj, dtijddudxj, dtijdmu, dtijdlambda);
  dRdmu[4] = (Vwall[1] * dtijdmu[0][0] + Vwall[2] * dtijdmu[1][0] + Vwall[3] * dtijdmu[2][0]) * n[0] +
             (Vwall[1] * dtijdmu[0][1] + Vwall[2] * dtijdmu[1][1] + Vwall[3] * dtijdmu[2][1]) * n[1] +
             (Vwall[1] * dtijdmu[0][2] + Vwall[2] * dtijdmu[1][2] + Vwall[3] * dtijdmu[2][2]) * n[2];
  dRdlambda[4] = (Vwall[1] * dtijdlambda[0][0] + Vwall[2] * dtijdlambda[1][0] + Vwall[3] * dtijdlambda[2][0]) * n[0] +
                 (Vwall[1] * dtijdlambda[0][1] + Vwall[2] * dtijdlambda[1][1] + Vwall[3] * dtijdlambda[2][1]) * n[1] +
                 (Vwall[1] * dtijdlambda[0][2] + Vwall[2] * dtijdlambda[1][2] + Vwall[3] * dtijdlambda[2][2]) * n[2];
  dRdVwall[4][1] = tij[0][0] * n[0] + tij[0][1] * n[1] + tij[0][2] * n[2];
  dRdVwall[4][2] = tij[1][0] * n[0] + tij[1][1] * n[1] + tij[1][2] * n[2];
  dRdVwall[4][3] = tij[2][0] * n[0] + tij[2][1] * n[1] + tij[2][2] * n[2];
  for(int i = 0; i < 3; ++i) {
    for(int j = 0; j < 3; ++j) {
      dRddudxj[4][i][j] = (Vwall[1] * dtijddudxj[0][0][i][j] + Vwall[2] * dtijddudxj[1][0][i][j] + Vwall[3] * dtijddudxj[2][0][i][j]) * n[0] +
                          (Vwall[1] * dtijddudxj[0][1][i][j] + Vwall[2] * dtijddudxj[1][1][i][j] + Vwall[3] * dtijddudxj[2][1][i][j]) * n[1] +
                          (Vwall[1] * dtijddudxj[0][2][i][j] + Vwall[2] * dtijddudxj[1][2][i][j] + Vwall[3] * dtijddudxj[2][2][i][j]) * n[2];
    }
  }
  dRdn[4][0] = Vwall[1] * tij[0][0] + Vwall[2] * tij[1][0] + Vwall[3] * tij[2][0];
  dRdn[4][1] = Vwall[1] * tij[0][1] + Vwall[2] * tij[1][1] + Vwall[3] * tij[2][1];
  dRdn[4][2] = Vwall[1] * tij[0][2] + Vwall[2] * tij[1][2] + Vwall[3] * tij[2][2];
}

//------------------------------------------------------------------------------

template<int neq>
inline
void NavierStokesTerm::computeJacobianSurfaceTermNS(double dp1dxj[4][3], double mu, double lambda,
                                                    double *V[4], double *Vwall, Vec3D& n,
                                                    double (*dRdU)[neq][neq]) {
  double dtijdu0[3][3];
  double dtijdu1[3][3];
  double dtijdu2[3][3];
  double dtijdu3[3][3];
  double dtijdu4[3][3];
  for(int k = 0; k < 4; ++k) {
    dtijdu0[0][0] = -2.0 * mu * dp1dxj[k][0] * V[k][1] / V[k][0] -
                    lambda * (dp1dxj[k][0] * V[k][1] + dp1dxj[k][1] * V[k][2] + dp1dxj[k][2] * V[k][3]) / V[k][0];
    dtijdu1[0][0] = 2.0 * mu * dp1dxj[k][0] / V[k][0] + lambda * dp1dxj[k][0] / V[k][0];
    dtijdu2[0][0] = lambda * dp1dxj[k][1] / V[k][0];
    dtijdu3[0][0] = lambda * dp1dxj[k][2] / V[k][0];
    dtijdu4[0][0] = 0.0;
    dtijdu0[1][1] = -2.0 * mu * dp1dxj[k][1] * V[k][2] / V[k][0] -
                    lambda * (dp1dxj[k][1] * V[k][2] + dp1dxj[k][0] * V[k][1] + dp1dxj[k][2] * V[k][3]) / V[k][0];
    dtijdu1[1][1] = lambda * dp1dxj[k][0] / V[k][0];
    dtijdu2[1][1] = 2.0 * mu * dp1dxj[k][1] / V[k][0] + lambda * dp1dxj[k][1] / V[k][0];
    dtijdu3[1][1] = lambda * dp1dxj[k][2] / V[k][0];
    dtijdu4[1][1] = 0.0;
    dtijdu0[2][2] = -2.0 * mu * dp1dxj[k][2] * V[k][3] / V[k][0] -
                    lambda * (dp1dxj[k][2] * V[k][3] + dp1dxj[k][0] * V[k][1] + dp1dxj[k][1] * V[k][2]) / V[k][0];
    dtijdu1[2][2] = lambda * dp1dxj[k][0] / V[k][0];
    dtijdu2[2][2] = lambda * dp1dxj[k][1] / V[k][0];
    dtijdu3[2][2] = 2.0 * mu * dp1dxj[k][2] / V[k][0] + lambda * dp1dxj[k][2] / V[k][0];
    dtijdu4[2][2] = 0.0;
    dtijdu0[0][1] = -mu * (dp1dxj[k][0] * V[k][2] + dp1dxj[k][1] * V[k][1]) / V[k][0];
    dtijdu1[0][1] = mu * dp1dxj[k][1] / V[k][0];
    dtijdu2[0][1] = mu * dp1dxj[k][0] / V[k][0];
    dtijdu3[0][1] = 0.0;
    dtijdu4[0][1] = 0.0;
    dtijdu0[0][2] = -mu * (dp1dxj[k][0] * V[k][3] + dp1dxj[k][2] * V[k][1]) / V[k][0];
    dtijdu1[0][2] = mu * dp1dxj[k][2] / V[k][0];
    dtijdu2[0][2] = 0.0;
    dtijdu3[0][2] = mu * dp1dxj[k][0] / V[k][0];
    dtijdu4[0][2] = 0.0;
    dtijdu0[1][2] = -mu * (dp1dxj[k][1] * V[k][3] + dp1dxj[k][2] * V[k][2]) / V[k][0];
    dtijdu1[1][2] = 0.0;
    dtijdu2[1][2] = mu * dp1dxj[k][2] / V[k][0];
    dtijdu3[1][2] = mu * dp1dxj[k][1] / V[k][0];
    dtijdu4[1][2] = 0.0;
    dtijdu0[1][0] = dtijdu0[0][1];
    dtijdu1[1][0] = dtijdu1[0][1];
    dtijdu2[1][0] = dtijdu2[0][1];
    dtijdu3[1][0] = dtijdu3[0][1];
    dtijdu4[1][0] = dtijdu4[0][1];
    dtijdu0[2][0] = dtijdu0[0][2];
    dtijdu1[2][0] = dtijdu1[0][2];
    dtijdu2[2][0] = dtijdu2[0][2];
    dtijdu3[2][0] = dtijdu3[0][2];
    dtijdu4[2][0] = dtijdu4[0][2];
    dtijdu0[2][1] = dtijdu0[1][2];
    dtijdu1[2][1] = dtijdu1[1][2];
    dtijdu2[2][1] = dtijdu2[1][2];
    dtijdu3[2][1] = dtijdu3[1][2];
    dtijdu4[2][1] = dtijdu4[1][2];
    dRdU[k][0][0] = 0.0;
    dRdU[k][0][1] = 0.0;
    dRdU[k][0][2] = 0.0;
    dRdU[k][0][3] = 0.0;
    dRdU[k][0][4] = 0.0;
    dRdU[k][1][0] = 0.0;
    dRdU[k][1][1] = 0.0;
    dRdU[k][1][2] = 0.0;
    dRdU[k][1][3] = 0.0;
    dRdU[k][1][4] = 0.0;
    dRdU[k][2][0] = 0.0;
    dRdU[k][2][1] = 0.0;
    dRdU[k][2][2] = 0.0;
    dRdU[k][2][3] = 0.0;
    dRdU[k][2][4] = 0.0;
    dRdU[k][3][0] = 0.0;
    dRdU[k][3][1] = 0.0;
    dRdU[k][3][2] = 0.0;
    dRdU[k][3][3] = 0.0;
    dRdU[k][3][4] = 0.0;
    dRdU[k][4][0] = (Vwall[1] * dtijdu0[0][0] + Vwall[2] * dtijdu0[1][0] + Vwall[3] * dtijdu0[2][0]) * n[0] +
                    (Vwall[1] * dtijdu0[0][1] + Vwall[2] * dtijdu0[1][1] + Vwall[3] * dtijdu0[2][1]) * n[1] +
                    (Vwall[1] * dtijdu0[0][2] + Vwall[2] * dtijdu0[1][2] + Vwall[3] * dtijdu0[2][2]) * n[2];
    dRdU[k][4][1] = (Vwall[1] * dtijdu1[0][0] + Vwall[2] * dtijdu1[1][0] + Vwall[3] * dtijdu1[2][0]) * n[0] +
                    (Vwall[1] * dtijdu1[0][1] + Vwall[2] * dtijdu1[1][1] + Vwall[3] * dtijdu1[2][1]) * n[1] +
                    (Vwall[1] * dtijdu1[0][2] + Vwall[2] * dtijdu1[1][2] + Vwall[3] * dtijdu1[2][2]) * n[2];
    dRdU[k][4][2] = (Vwall[1] * dtijdu2[0][0] + Vwall[2] * dtijdu2[1][0] + Vwall[3] * dtijdu2[2][0]) * n[0] +
                    (Vwall[1] * dtijdu2[0][1] + Vwall[2] * dtijdu2[1][1] + Vwall[3] * dtijdu2[2][1]) * n[1] +
                    (Vwall[1] * dtijdu2[0][2] + Vwall[2] * dtijdu2[1][2] + Vwall[3] * dtijdu2[2][2]) * n[2];
    dRdU[k][4][3] = (Vwall[1] * dtijdu3[0][0] + Vwall[2] * dtijdu3[1][0] + Vwall[3] * dtijdu3[2][0]) * n[0] +
                    (Vwall[1] * dtijdu3[0][1] + Vwall[2] * dtijdu3[1][1] + Vwall[3] * dtijdu3[2][1]) * n[1] +
                    (Vwall[1] * dtijdu3[0][2] + Vwall[2] * dtijdu3[1][2] + Vwall[3] * dtijdu3[2][2]) * n[2];
    dRdU[k][4][4] = (Vwall[1] * dtijdu4[0][0] + Vwall[2] * dtijdu4[1][0] + Vwall[3] * dtijdu4[2][0]) * n[0] +
                    (Vwall[1] * dtijdu4[0][1] + Vwall[2] * dtijdu4[1][1] + Vwall[3] * dtijdu4[2][1]) * n[1] +
                    (Vwall[1] * dtijdu4[0][2] + Vwall[2] * dtijdu4[1][2] + Vwall[3] * dtijdu4[2][2]) * n[2];
  }
}

//------------------------------------------------------------------------------

template<int neq>
inline
void NavierStokesTerm::computeJacobianSurfaceTermNS(double dp1dxj[4][3], double mu, double dmu[4][5],
                                                    double lambda, double dlambda[4][5],
                                                    double *V[4], double *Vwall, double dudxj[3][3],
                                                    Vec3D& n, double (*dRdU)[neq][neq]) {
  double dtijdu0[3][3];
  double dtijdu1[3][3];
  double dtijdu2[3][3];
  double dtijdu3[3][3];
  double dtijdu4[3][3];
  for(int k = 0; k < 4; ++k) {
    dtijdu0[0][0] = 2.0 * dmu[k][0] * dudxj[0][0] - 2.0 * mu * dp1dxj[k][0] * V[k][1] / V[k][0] + dlambda[k][0] *
                    (dudxj[0][0] + dudxj[1][1] + dudxj[2][2]) - lambda * (dp1dxj[k][0] * V[k][1] + dp1dxj[k][1] * V[k][2] + dp1dxj[k][2] * V[k][3]) / V[k][0];
    dtijdu1[0][0] = 2.0 * dmu[k][1] * dudxj[0][0] + 2.0 * mu * dp1dxj[k][0] / V[k][0] + dlambda[k][1] * (dudxj[0][0] + dudxj[1][1] + dudxj[2][2]) + lambda
                    * dp1dxj[k][0] / V[k][0];
    dtijdu2[0][0] = 2.0 * dmu[k][2] * dudxj[0][0] + dlambda[k][2] * (dudxj[0][0] + dudxj[1][1] + dudxj[2][2]) + lambda * dp1dxj[k][1] / V[k][0];
    dtijdu3[0][0] = 2.0 * dmu[k][3] * dudxj[0][0] + dlambda[k][3] * (dudxj[0][0] + dudxj[1][1] + dudxj[2][2]) + lambda * dp1dxj[k][2] / V[k][0];
    dtijdu4[0][0] = 2.0 * dmu[k][4] * dudxj[0][0] + dlambda[k][4] * (dudxj[0][0] + dudxj[1][1] + dudxj[2][2]);
    dtijdu0[1][1] = 2.0 * dmu[k][0] * dudxj[1][1] - 2.0 * mu * dp1dxj[k][1] * V[k][2] / V[k][0] + dlambda[k][0] *
                    (dudxj[0][0] + dudxj[1][1] + dudxj[2][2]) - lambda * (dp1dxj[k][1] * V[k][2] + dp1dxj[k][0] * V[k][1] + dp1dxj[k][2] * V[k][3]) / V[k][0];
    dtijdu1[1][1] = 2.0 * dmu[k][1] * dudxj[1][1] + dlambda[k][1] * (dudxj[0][0] + dudxj[1][1] + dudxj[2][2]) + lambda * dp1dxj[k][0] / V[k][0];
    dtijdu2[1][1] = 2.0 * dmu[k][2] * dudxj[1][1] + 2.0 * mu * dp1dxj[k][1] / V[k][0] + dlambda[k][2] * (dudxj[0][0] + dudxj[1][1] + dudxj[2][2]) + lambda
                    * dp1dxj[k][1] / V[k][0];
    dtijdu3[1][1] = 2.0 * dmu[k][3] * dudxj[1][1] + dlambda[k][3] * (dudxj[0][0] + dudxj[1][1] + dudxj[2][2]) + lambda * dp1dxj[k][2] / V[k][0];
    dtijdu4[1][1] = 2.0 * dmu[k][4] * dudxj[1][1] + dlambda[k][4] * (dudxj[0][0] + dudxj[1][1] + dudxj[2][2]);
    dtijdu0[2][2] = 2.0 * dmu[k][0] * dudxj[2][2] - 2.0 * mu * dp1dxj[k][2] * V[k][3] / V[k][0] + dlambda[k][0] *
                    (dudxj[0][0] + dudxj[1][1] + dudxj[2][2]) - lambda * (dp1dxj[k][2] * V[k][3] + dp1dxj[k][0] * V[k][1] + dp1dxj[k][1] * V[k][2]) / V[k][0];
    dtijdu1[2][2] = 2.0 * dmu[k][1] * dudxj[2][2] + dlambda[k][1] * (dudxj[0][0] + dudxj[1][1] + dudxj[2][2]) + lambda * dp1dxj[k][0] / V[k][0];
    dtijdu2[2][2] = 2.0 * dmu[k][2] * dudxj[2][2] + dlambda[k][2] * (dudxj[0][0] + dudxj[1][1] + dudxj[2][2]) + lambda * dp1dxj[k][1] / V[k][0];
    dtijdu3[2][2] = 2.0 * dmu[k][3] * dudxj[2][2] + 2.0 * mu * dp1dxj[k][2] / V[k][0] + dlambda[k][3] * (dudxj[0][0] + dudxj[1][1] + dudxj[2][2]) + lambda
                    * dp1dxj[k][2] / V[k][0];
    dtijdu4[2][2] = 2.0 * dmu[k][4] * dudxj[2][2] + dlambda[k][4] * (dudxj[0][0] + dudxj[1][1] + dudxj[2][2]);
    dtijdu0[0][1] = -mu * (dp1dxj[k][0] * V[k][2] + dp1dxj[k][1] * V[k][1]) / V[k][0] + dmu[k][0] * (dudxj[1][0] + dudxj[0][1]);
    dtijdu1[0][1] = mu * dp1dxj[k][1] / V[k][0] + dmu[k][1] * (dudxj[1][0] + dudxj[0][1]);
    dtijdu2[0][1] = mu * dp1dxj[k][0] / V[k][0] + dmu[k][2] * (dudxj[1][0] + dudxj[0][1]);
    dtijdu3[0][1] = dmu[k][3] * (dudxj[1][0] + dudxj[0][1]);
    dtijdu4[0][1] = dmu[k][4] * (dudxj[1][0] + dudxj[0][1]);
    dtijdu0[0][2] = -mu * (dp1dxj[k][0] * V[k][3] + dp1dxj[k][2] * V[k][1]) / V[k][0] + dmu[k][0] * (dudxj[2][0] + dudxj[0][2]);
    dtijdu1[0][2] = mu * dp1dxj[k][2] / V[k][0] + dmu[k][1] * (dudxj[2][0] + dudxj[0][2]);
    dtijdu2[0][2] = dmu[k][2] * (dudxj[2][0] + dudxj[0][2]);
    dtijdu3[0][2] = mu * dp1dxj[k][0] / V[k][0] + dmu[k][3] * (dudxj[2][0] + dudxj[0][2]);
    dtijdu4[0][2] = dmu[k][4] * (dudxj[2][0] + dudxj[0][2]);
    dtijdu0[1][2] = -mu * (dp1dxj[k][1] * V[k][3] + dp1dxj[k][2] * V[k][2]) / V[k][0] + dmu[k][0] * (dudxj[2][1] + dudxj[1][2]);
    dtijdu1[1][2] = dmu[k][1] * (dudxj[2][1] + dudxj[1][2]);
    dtijdu2[1][2] = mu * dp1dxj[k][2] / V[k][0] + dmu[k][2] * (dudxj[2][1] + dudxj[1][2]);
    dtijdu3[1][2] = mu * dp1dxj[k][1] / V[k][0] + dmu[k][3] * (dudxj[2][1] + dudxj[1][2]);
    dtijdu4[1][2] = dmu[k][4] * (dudxj[2][1] + dudxj[1][2]);
    dtijdu0[1][0] = dtijdu0[0][1];
    dtijdu1[1][0] = dtijdu1[0][1];
    dtijdu2[1][0] = dtijdu2[0][1];
    dtijdu3[1][0] = dtijdu3[0][1];
    dtijdu4[1][0] = dtijdu4[0][1];
    dtijdu0[2][0] = dtijdu0[0][2];
    dtijdu1[2][0] = dtijdu1[0][2];
    dtijdu2[2][0] = dtijdu2[0][2];
    dtijdu3[2][0] = dtijdu3[0][2];
    dtijdu4[2][0] = dtijdu4[0][2];
    dtijdu0[2][1] = dtijdu0[1][2];
    dtijdu1[2][1] = dtijdu1[1][2];
    dtijdu2[2][1] = dtijdu2[1][2];
    dtijdu3[2][1] = dtijdu3[1][2];
    dtijdu4[2][1] = dtijdu4[1][2];
    dRdU[k][0][0] = 0.0;
    dRdU[k][0][1] = 0.0;
    dRdU[k][0][2] = 0.0;
    dRdU[k][0][3] = 0.0;
    dRdU[k][0][4] = 0.0;
    dRdU[k][1][0] = 0.0;
    dRdU[k][1][1] = 0.0;
    dRdU[k][1][2] = 0.0;
    dRdU[k][1][3] = 0.0;
    dRdU[k][1][4] = 0.0;
    dRdU[k][2][0] = 0.0;
    dRdU[k][2][1] = 0.0;
    dRdU[k][2][2] = 0.0;
    dRdU[k][2][3] = 0.0;
    dRdU[k][2][4] = 0.0;
    dRdU[k][3][0] = 0.0;
    dRdU[k][3][1] = 0.0;
    dRdU[k][3][2] = 0.0;
    dRdU[k][3][3] = 0.0;
    dRdU[k][3][4] = 0.0;
    dRdU[k][4][0] = (Vwall[1] * dtijdu0[0][0] + Vwall[2] * dtijdu0[1][0] + Vwall[3] * dtijdu0[2][0]) * n[0] +
                    (Vwall[1] * dtijdu0[0][1] + Vwall[2] * dtijdu0[1][1] + Vwall[3] * dtijdu0[2][1]) * n[1] +
                    (Vwall[1] * dtijdu0[0][2] + Vwall[2] * dtijdu0[1][2] + Vwall[3] * dtijdu0[2][2]) * n[2];
    dRdU[k][4][1] = (Vwall[1] * dtijdu1[0][0] + Vwall[2] * dtijdu1[1][0] + Vwall[3] * dtijdu1[2][0]) * n[0] +
                    (Vwall[1] * dtijdu1[0][1] + Vwall[2] * dtijdu1[1][1] + Vwall[3] * dtijdu1[2][1]) * n[1] +
                    (Vwall[1] * dtijdu1[0][2] + Vwall[2] * dtijdu1[1][2] + Vwall[3] * dtijdu1[2][2]) * n[2];
    dRdU[k][4][2] = (Vwall[1] * dtijdu2[0][0] + Vwall[2] * dtijdu2[1][0] + Vwall[3] * dtijdu2[2][0]) * n[0] +
                    (Vwall[1] * dtijdu2[0][1] + Vwall[2] * dtijdu2[1][1] + Vwall[3] * dtijdu2[2][1]) * n[1] +
                    (Vwall[1] * dtijdu2[0][2] + Vwall[2] * dtijdu2[1][2] + Vwall[3] * dtijdu2[2][2]) * n[2];
    dRdU[k][4][3] = (Vwall[1] * dtijdu3[0][0] + Vwall[2] * dtijdu3[1][0] + Vwall[3] * dtijdu3[2][0]) * n[0] +
                    (Vwall[1] * dtijdu3[0][1] + Vwall[2] * dtijdu3[1][1] + Vwall[3] * dtijdu3[2][1]) * n[1] +
                    (Vwall[1] * dtijdu3[0][2] + Vwall[2] * dtijdu3[1][2] + Vwall[3] * dtijdu3[2][2]) * n[2];
    dRdU[k][4][4] = (Vwall[1] * dtijdu4[0][0] + Vwall[2] * dtijdu4[1][0] + Vwall[3] * dtijdu4[2][0]) * n[0] +
                    (Vwall[1] * dtijdu4[0][1] + Vwall[2] * dtijdu4[1][1] + Vwall[3] * dtijdu4[2][1]) * n[1] +
                    (Vwall[1] * dtijdu4[0][2] + Vwall[2] * dtijdu4[1][2] + Vwall[3] * dtijdu4[2][2]) * n[2];
  }
}

//------------------------------------------------------------------------------

inline
void NavierStokesTerm::computeMaxwellSlipSurfaceTermNS(double mu, double lambda, double kappa, double *Vwall,
                                                       double rhocg, double ucg[3], double dudxj[3][3], double dTdxj[3],
                                                       Vec3D& n, double *R) {
  double tij[3][3];
  computeStressTensor(mu, lambda, dudxj, tij);

  double qj[3];
  computeHeatFluxVector(kappa, dTdxj, qj);

  Vec3D unit_n = n/n.norm();

  double qn = 0;
  for(int i = 0; i < 3; i++) {
    qn += qj[i] * unit_n[i];
  }

  double qs[3], tdotn[3], tddotn = 0;
  for(int i = 0; i < 3; i++) {
    qs[i] = qj[i] - qn * unit_n[i];
    tdotn[i] = 0;
    for(int j = 0; j < 3; j++) {
      tdotn[i] += tij[i][j]*unit_n[j];
    }
    tddotn += tdotn[i] * unit_n[i];
  }

  double ts[3];
  for(int i = 0; i < 3; i++) {
    ts[i] = tdotn[i] - tddotn * unit_n[i];
  }

  double gk[3];
  slipFcn->compute(Vwall, qs, ts, gk);

  R[1] = rhocg * (Vwall[1] - ucg[0]) + gk[0];
  R[2] = rhocg * (Vwall[2] - ucg[1]) + gk[1];
  R[3] = rhocg * (Vwall[3] - ucg[2]) + gk[2];
}

//------------------------------------------------------------------------------

#endif

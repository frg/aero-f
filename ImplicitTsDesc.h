#ifndef _IMPLICIT_TS_DESC_H_
#define _IMPLICIT_TS_DESC_H_

#include <TsDesc.h>

struct DistInfo;

class GeoSource;
class Domain;
class Communicator;

template<class Scalar, int dim> class DistSVec;
template<class ProblemDescriptor, class KspDataType> class NewtonSolver;
namespace std {
  template<class Scalar> class complex;
}

#ifndef _MATVECPROD_TMPL_
  #define _MATVECPROD_TMPL_
  template<int dim, int neq, class Scalar2 = double> class MatVecProd;
#endif

#ifndef _KSPPREC_TMPL_
  #define _KSPPREC_TMPL_
  template<int Dim, class Scalar2 = double> class KspPrec;
#endif

#ifndef _KSPSLVR_TMPL_
  #define _KSPSLVR_TMPL_
  template<class VecType, class MvpOp, class PrecOp, class IoOp, class ScalarT = double> class KspSolver;
#endif

//------------------------------------------------------------------------------

template<int dim>
class ImplicitTsDesc : public TsDesc<dim> {

 protected:
  int failSafe;
  DistSVec<bool, 2> *tag;

  // for IRK time integrators
  DistSVec<double, dim> U0;
  DistSVec<double, dim> k1;

  NewtonSolver<ImplicitTsDesc<dim>, KspFluidData> *ns;

  int computeBackwardDifference(DistSVec<double, dim>&, DistSVec<double, dim> * = nullptr);
  int computeRKSecondOrder(DistSVec<double, dim>&, DistSVec<double, dim> * = nullptr);
  int computeRKThirdOrder(DistSVec<double, dim>&, DistSVec<double, dim> * = nullptr);

 public:
  template<class Scalar, int neq>
  static KspPrec<neq> *createPreconditioner(KspData&, Domain *, int);

  template<int Dim, int neq, class Scalar2>
  static KspSolver<DistSVec<double, neq>, MatVecProd<Dim, neq, Scalar2>, KspPrec<neq, Scalar2>, Communicator> *
    createKrylovSolver(const DistInfo&, int, KspData&, MatVecProd<Dim, neq, Scalar2> *, KspPrec<neq, Scalar2> *, Communicator *, Timer *, Domain *,
                       void (MatVecProd<Dim, neq, Scalar2>::*)(DistSVec<double, neq>&, DistSVec<double, neq>&) = &MatVecProd<Dim, neq, Scalar2>::apply);

  template<class Scalar, class PrecScalar, int neq, class Scalar2>
  static void setOperator(MatVecProd<dim, neq, Scalar2> *, KspPrec<neq, Scalar2> *, DistSVec<double, dim>&, SpaceOperator<dim> *,
                          KspData::KspPrecJacobian, Scalar = 1, double = 1, MatVecProd<dim, neq, PrecScalar> * = nullptr, bool = false);

  ImplicitTsDesc(IoData&, GeoSource&, Domain *);
  ~ImplicitTsDesc();

  void resize1(NodeData *, DistSVec<double, dim>&);
  void resize2();

  virtual int solveNonLinearSystem(DistSVec<double, dim>&, int, double, DistSVec<double, dim> * = nullptr);
  virtual void computeFunction(DistSVec<double, dim>&, DistSVec<double, dim>&, bool);
  virtual double meritFunction(DistSVec<double, dim>& F) { return F.norm(); }
  virtual void computeJacobian(DistSVec<double, dim>&, DistSVec<double, dim>&) {}
  virtual void setOperators(DistSVec<double, dim>&) {}
  virtual void solveLinearSystem(int, DistSVec<double, dim>&, DistSVec<double, dim>&) {}

  bool lineSearchCondition(double restrial, double res, double alpha, double c1) {
    return (restrial < sqrt(1 - 2.0 * alpha * c1) * res);
  }
  virtual void adjustStep(DistSVec<double, dim>& dQ, double c) {
    dQ *= c;
  }
  virtual void incrementSolution(DistSVec<double, dim>& Q, DistSVec<double, dim>& dQ, DistSVec<double, dim>& Qn) {
    Qn = Q; Q += dQ;
  }
  virtual void resetSolution(DistSVec<double, dim>& Q, DistSVec<double, dim>& Qn) {
    Q = Qn;
  }

  int checkFailSafe(DistSVec<double, dim>&);
  void resetFixesTag();

  using TsDesc<dim>::getVecInfo;

 private:
  // helper class to facilitate re-use of NewtonSolver for level-set equation
  class ImplicitLevelSetTsDesc {
     ImplicitTsDesc *implicitTsDesc;
    public:
     typedef DistSVec<double, dimLS> SolVecType;
     ImplicitLevelSetTsDesc(ImplicitTsDesc *_implicitTsDesc) : implicitTsDesc(_implicitTsDesc) {}
     DistInfo& getVecInfo() const { return implicitTsDesc->getVecInfo(); }
     Communicator *getCommunicator() { return implicitTsDesc->getCommunicator(); }
     TsParameters *getTsParams() { return implicitTsDesc->getTsParams(); }
     void computeFunction(DistSVec<double, dimLS>&, DistSVec<double, dimLS>&, bool);
     double meritFunction(DistSVec<double, dimLS>& F) { return F.norm(); }
     void computeJacobian(DistSVec<double, dimLS>&, DistSVec<double, dimLS>&);
     void setOperators(DistSVec<double, dimLS>&);
     void solveLinearSystem(int, DistSVec<double, dimLS>&, DistSVec<double, dimLS>&);
     bool lineSearchCondition(double restrial, double res, double alpha, double c1) {
       return (restrial < sqrt(1 - 2.0 * alpha * c1) * res);
     }
     void adjustStep(DistSVec<double, dimLS>& dQ, double c) {
       dQ *= c;
     }
     void incrementSolution(DistSVec<double, dimLS>& Q, DistSVec<double, dimLS>& dQ, DistSVec<double, dimLS>& Qn) {
       Qn = Q; Q += dQ;
     }
     void resetSolution(DistSVec<double, dimLS>& Q, DistSVec<double, dimLS>& Qn) {
       Q = Qn;
     }
     int checkFailSafe(DistSVec<double, dimLS>&) {
       return 0;
     }
     int checkSolution(DistSVec<double, dimLS>&) {
       return 0;
     }
  };

  ImplicitLevelSetTsDesc *implicitLevelSetTsDesc;
  NewtonSolver<ImplicitLevelSetTsDesc, KspFluidData> *nsLS;
  MatVecProd<dimLS, dimLS> *mvpLS;
  KspPrec<dimLS> *pcLS;
  KspSolver<DistSVec<double, dimLS>, MatVecProd<dimLS, dimLS>, KspPrec<dimLS>, Communicator> *kspLS;

};

//------------------------------------------------------------------------------

#endif

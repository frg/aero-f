#ifndef _DIST_MATRIX_H_
#define _DIST_MATRIX_H_

#include <vector>
#include <utility>

//------------------------------------------------------------------------------

template<class MatrixType>
class DistMat {

  std::vector<MatrixType> a;

 public:
  DistMat(int n) {
    a.reserve(n);
  }
  DistMat(const DistMat<MatrixType>& x) {
    a.reserve(x.a.size());
    for(int i = 0; i < x.a.size(); ++i) {
      a.push_back(x.a[i]);  // copy constructor called
    }
  }
  ~DistMat() {}

  MatrixType& operator()(int i) {
    return a[i];
  }

  void setZero() {
    #pragma omp parallel for
    for(int i = 0; i < a.size(); ++i) {
      a[i].setZero();
    }
  }

  int size() {
    return a.size();
  }

  void clear() {
    a.clear();
  }

  template<class... Args>
  void emplace_back(Args&&... args) {
    a.emplace_back(std::forward<Args>(args)...);
  }

};

//------------------------------------------------------------------------------

#endif

#ifndef _EXPLICIT_TS_DESC_H_
#define _EXPLICIT_TS_DESC_H_

#include <TsDesc.h>
#include <Types.h>

class IoData;
class GeoSource;
class Domain;

template<class Scalar, int dim> class DistSVec;

//------------------------------------------------------------------------------

template<int dim>
class ExplicitTsDesc : public TsDesc<dim> {

  int failSafe;

  // Fluid
  DistSVec<double, dim> U0;
  DistSVec<double, dim> k1;
  DistSVec<double, dim> k2;
  DistSVec<double, dim> k3;
  DistSVec<double, dim> k4;
  // Level-set
  DistSVec<double, dimLS> Phi0;
  DistSVec<double, dimLS> p1;
  DistSVec<double, dimLS> p2;
  DistSVec<double, dimLS> p3;
  DistSVec<double, dimLS> p4;

 public:
  ExplicitTsDesc(IoData&, GeoSource&, Domain *);
  ~ExplicitTsDesc() {}

  void resize1(NodeData *, DistSVec<double, dim>&);
  void resize2();

  int solveNonLinearSystem(DistSVec<double, dim>&, int, double, DistSVec<double, dim> * = nullptr);
  void resetFixesTag();

  using TsDesc<dim>::getVecInfo;

 private:
  // Fluid
  int computeRKFourthOrder(DistSVec<double, dim>&);
  int computeRKSecondOrder(DistSVec<double, dim>&);
  int computeForwardEuler(DistSVec<double, dim>&);
  void computeRKUpdate(DistSVec<double, dim>&, DistSVec<double, dim>&);
  // Level-set
  void computeRKFourthOrderLS(DistSVec<double, dim>&, DistSVec<double, dimLS>&);
  void computeRKSecondOrderLS(DistSVec<double, dim>&, DistSVec<double, dimLS>&);
  void computeForwardEulerLS(DistSVec<double, dim>&, DistSVec<double, dimLS>&);
  void computeRKUpdateLS(DistSVec<double, dim>&, DistSVec<double, dimLS>&, DistSVec<double, dimLS>&);

};

//------------------------------------------------------------------------------

#endif

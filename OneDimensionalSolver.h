#ifndef _ONE_DIMENSIONAL_SOLVER_H_
#define _ONE_DIMENSIONAL_SOLVER_H_

//------------------------------------------------------------------------------
// Created March 2010 by A. Rallu @Stanford University
//------------------------------------------------------------------------------
// This class is used to solve the one-dimensional Euler equations.
// They can be written in cartesian, cylindrical or spherical coordinates.
// All these cases are considered in this class.
// This class is geared toward solving two-phase one-dimensional Euler equations
// with a possible burn process of explosives.
//
// In order to allow some flexibility and ease of use, this class
// uses only some of the local-level classes of the rest of the AERO-F code
// (like SVec, FluxFcn, LocalRiemann, VarFcn). Hence, it does not use
// classes like DistTimeState or SpaceOperator. One reason is that these
// classes then always refer to Edges and Faces and Elems which are not
// available for the simulations intended with this class. (It would be possible
// to go through Edges, Faces, Elems and therefore use SpaceOperator and
// DistTimeState and others, but that requires more work/time than I can
// afford now).
//
//
// Note that state vectors have 5 coordinates. Only three of them are used.
// The coordinates [2] and [3] are unused but must be present in order to
// be able to use the functions of VarFcn, FluxFcn, LocalRiemann (as are).
//
// Note: the spherical one-D Euler equations can be written as
// d(r^2 U)/dt + d(r^2 F(U))/dr = S2(U,r) = {0, 2*r*pressure, 0}
// or
// dU/dt + dF(U)/dx = S(U,r) = {-a*density*velocity/r, -a*density*velocity^2/r, -a*(density*energy+pressure)*velocity/r}
// where a = 2 for spherical coordinates (if a = 1, these are the cylindrical
// one-dimensional Euler equations)
// The first should require that the conserved quantities are r^2*U.
// The second requires that the source be integrated in a specific manner at r=0
//
//------------------------------------------------------------------------------

#include <Vector.h>
#include <FluidSelector.h>
#include <RefVal.h>
#include <PostFcn.h>
#include <Node.h>
#include <VarFcn.h>

#include <fstream>
#include <algorithm>

class Domain;
class FluxFcn;
class IoData;
class LocalRiemann;
class OneDimensionalSourceTerm;
class ProgrammedBurn;
class RecFcn;
class SparseGridCluster;

template<int dim> class ExactRiemannSolver;
template<class T> class RKIntegrator;

//------------------------------------------------------------------------------

class OneDimensional {
  // note this class currently works for the Euler equation (dim = 5) and a single level-set equation (dimLS = 1)
  // however, I think it can be used to initialize a 3D simulation dim != 5 and dimLS > 1
  OneDimensionalInfo::CoordinateType coordType;
  OneDimensionalInfo::VolumeType volumeType;

  enum ProblemMode { MultiFluid, FSI };
  ProblemMode problemMode;

  int numPoints;
  SVec<double, 1> ctrlVol;
  // for cartesian, cylindrical and spherical one-D simulation with control surfaces
  SVec<double, 1> ctrlSurf;
  SVec<double, 1> X; // center of control volumes
  SVec<double, 1> Y; // control surface locations

  double finalTime;
  double cfl;

  double BC[2][5];
  double BCphi[2];

  SVec<double, 5> U;
  SVec<double, 5> V;
  SVec<double, 5> R;

  SVec<double, 1> Phi;
  SVec<double, 1> Phin;
  SVec<double, 1> Rphi;
  Vec<int> fluidId;
  Vec<int> fluidIdn;

  FluxFcn **fluxFcn;
  VarFcn *varFcn;
  ExactRiemannSolver<5> *riemann;
  FluidSelector fluidSelector;
  // for cartesian, cylindrical and spherical one-D simulation with source term
  OneDimensionalSourceTerm *source;

  int frequency; // postprocessing output frequency
  char *outfile;
  RefVal refVal;

  // Riemann solutions at the interface
  // Needed for appropriate handling of phase change updates.
  SVec<double, 5> Wr;

  SVec<double, 5> lastPhaseChangeValue;

  SVec<double, 5> Vslope;
  SVec<double, 1> Phislope;

  Vec<int> riemannStatus;
  Vec<int> cutCellStatus;

  RecFcn *recFcn, *recFcnLS;

  char *bubbleRadiusFile;

  RecFcn *createRecFcn(IoData&);
  RecFcn *createRecFcnLS(IoData&);

  RKIntegrator<SVec<double, 5>> *Vintegrator;
  RKIntegrator<SVec<double, 1>> *Phiintegrator;

  double time;

  SVec<double, 5> rupdate;
  Vec<double> weight;
  Vec<int> fidToSet;

  void loadSparseGrid(IoData&);

  SparseGridCluster *tabulationC;

  ProgrammedBurn *programmedBurn;

  double programmedBurnStopPercentDistance;
  bool programmedBurnIsUsed;

  double sscale[PostFcn::SSIZE];
  double vscale[PostFcn::SSIZE];

  char *solutions;
  char *scalars[PostFcn::SSIZE];
  char *vectors[PostFcn::VSIZE];

  struct {
    int numNodes;
    int step;
    std::vector<Vec3D> locations;
    std::vector<int> ids;
    std::vector<double> alpha;
  } nodal_output;

  char *nodal_scalars[PostFcn::SSIZE];
  char *nodal_vectors[PostFcn::VSIZE];

  void setupOutputFiles(IoData&);
  void setupFixes(IoData&);

  int *loctag;

  int typePhaseChange;

  bool isSixthOrder;

  bool isSinglePhase;

  double beta;
  double interfaceLocation;

  int interfaceTreatment;
  int interfaceExtrapolation;

  // 0 - constant reconstruction
  // 1 - linear reconstruction
  // 2 - limited
  int limiterLeft, limiterRight;

  int levelSetMethod;

  Timer *myTimer;

  void setupProbes(IoData&);
  void outputProbes(double, int);

  class Veval {

   public:
    int findNode(const Vec3D& loc, double& localRadius) {
      if(spherical) {
        localRadius = sqrt((loc[0] - bubble_x0) * (loc[0] - bubble_x0) + (loc[1] - bubble_y0) * (loc[1] - bubble_y0) + (loc[2] - bubble_z0) * (loc[2] - bubble_z0));
      }
      else {
        localRadius = loc[0];
      }
      // If the node is inside the sphere, set its values accordingly
      if(localRadius < max_distance) {
        // Do a binary search to find the closest point whose r
        // is less than or equal to localRadius
        int a = numPoints / 2;
        int rmin = 0, rmax = numPoints - 2;
        while(!(x_1D[a] <= localRadius && x_1D[a + 1] > localRadius)) {
          if(x_1D[a] < localRadius) {
            rmin = a;
          }
          else {
            rmax = a;
          }
          if(a == rmin) {
            a = (rmax + rmin) / 2 + 1;
          }
          else {
            a = (rmax + rmin) / 2;
          }
        }
        return a;
      }
      else {
        return -1;
      }
    }

    Veval(VarFcn *_vf, OneDimensionalInputData *_oned, FluidSelector *_fluidSelector, double *_x, double *_v, int *_fids, NodeSet *_X, int _np,
          double x0, double y0, double z0, bool sph = true)  : x_1D(_x), v_1D(_v), fids(_fids), X(_X), numPoints(_np),
      bubble_x0(x0), bubble_y0(y0), bubble_z0(z0), oned(_oned), varFcn(_vf), spherical(sph) {
      int i, fid_new = 0;
      memset(outletState, 0, sizeof(double) * 5);
      for(i = 0; i < numPoints; ++i) {
        if(fids[i] == 0) {
          memset(boundaryStateL, 0, sizeof(double) * 5);
          memset(boundaryStateR, 0, sizeof(double) * 5);
          boundaryStateL[0] = v_1D[(i - 1) * 5];
          boundaryStateR[0] = v_1D[i * 5];
          boundaryStateL[1] = v_1D[(i - 1) * 5 + 1];
          boundaryStateR[1] = v_1D[i * 5 + 1];
          fid_new = fids[i - 1];
          if(oned && oned->fluidRemap.dataMap.find(fids[i - 1]) != oned->fluidRemap.dataMap.end()) {
            fid_new = oned->fluidRemap.dataMap.find(fids[i - 1])->second->newID;
          }
          if(varFcn->getType(fid_new) == VarFcnBase::TAIT) {
            boundaryStateL[4] = v_1D[(i - 1) * 5 + 4];
          }
          else {
            boundaryStateL[4] = v_1D[(i - 1) * 5 + 2];
          }
          fid_new = fids[i];
          if(oned && oned->fluidRemap.dataMap.find(fids[i]) != oned->fluidRemap.dataMap.end()) {
            fid_new = oned->fluidRemap.dataMap.find(fids[i])->second->newID;
          }
          if(varFcn->getType(fid_new) == VarFcnBase::TAIT) {
            boundaryStateR[4] = v_1D[i * 5 + 4];
          }
          else {
            boundaryStateR[4] = v_1D[i * 5 + 2];
          }
          rad = (x_1D[i - 1] * v_1D[i * 5 + 3] - x_1D[i] * v_1D[(i - 1) * 5 + 3]) / (v_1D[i * 5 + 3] - v_1D[(i - 1) * 5 + 3]);
          break;
        }
      }
      max_distance = x_1D[numPoints - 1];
      outletState[0] = v_1D[(numPoints - 1) * 5];
      fid_new = fids[numPoints - 1];
      if(oned && oned->fluidRemap.dataMap.find(fids[numPoints - 1]) != oned->fluidRemap.dataMap.end()) {
        fid_new = oned->fluidRemap.dataMap.find(fids[numPoints - 1])->second->newID;
      }
      if(varFcn->getType(fid_new) == VarFcnBase::TAIT) {
        outletState[4] = v_1D[(numPoints - 1) * 5 + 4];
      }
      else {
        outletState[4] = v_1D[(numPoints - 1) * 5 + 2];
      }
    }

    void Eval(int node, const Vec3D& loc, double *f) {
      double localRadius;
      double ff[5];
      int lsdim;
      int i = node;
      int fid_new = 0;
      double bub_x0[3] = { bubble_x0, bubble_y0, bubble_z0 };
      double xrad;
      if(spherical) {
        xrad = sqrt(((*X)[i][0] - bubble_x0) * ((*X)[i][0] - bubble_x0) + ((*X)[i][1] - bubble_y0) * ((*X)[i][1] - bubble_y0) + ((*X)[i][2] - bubble_z0) * ((
                      *X)[i][2] - bubble_z0));
      }
      else {
        xrad = (*X)[i][0];
      }
      int a = findNode(loc, localRadius);
      if(a >= 0) {
        double alpha = (localRadius - x_1D[a]) / (x_1D[a + 1] - x_1D[a]);
        if(fids[a] != fids[a + 1]) {
          if(rad - localRadius > 0.0) {
            alpha = 0.0;
            fid_new = fids[a];
            if(oned && oned->fluidRemap.dataMap.find(fids[a]) != oned->fluidRemap.dataMap.end()) {
              fid_new = oned->fluidRemap.dataMap.find(fids[a])->second->newID;
            }
          }
          else {
            alpha = 1.0;
            fid_new = fids[a + 1];
            if(oned && oned->fluidRemap.dataMap.find(fids[a + 1]) != oned->fluidRemap.dataMap.end()) {
              fid_new = oned->fluidRemap.dataMap.find(fids[a + 1])->second->newID;
            }
          }
        }
        else {
          fid_new = fids[a];
          if(oned && oned->fluidRemap.dataMap.find(fids[a]) != oned->fluidRemap.dataMap.end()) {
            fid_new = oned->fluidRemap.dataMap.find(fids[a])->second->newID;
          }
        }
        if(!spherical) {
          fid_new = 1 - fid_new;
        }
        fid_new = std::min<int>(fid_new, varFcn->size() - 1);
        if((localRadius > rad && xrad > rad) || (localRadius <= rad && xrad <= rad)) {
          ff[0] = v_1D[a * 5] * (1.0 - alpha) + v_1D[(a + 1) * 5] * (alpha);
          if(spherical) {
            ff[1] = (v_1D[a * 5 + 1] * (1.0 - alpha) + v_1D[(a + 1) * 5 + 1] * (alpha)) * (loc[0] - bubble_x0) / std::max(localRadius, 1.0e-8);
            ff[2] = (v_1D[a * 5 + 1] * (1.0 - alpha) + v_1D[(a + 1) * 5 + 1] * (alpha)) * (loc[1] - bubble_y0) / std::max(localRadius, 1.0e-8);
            ff[3] = (v_1D[a * 5 + 1] * (1.0 - alpha) + v_1D[(a + 1) * 5 + 1] * (alpha)) * (loc[2] - bubble_z0) / std::max(localRadius, 1.0e-8);
          }
          else {
            ff[1] = (v_1D[a * 5 + 1] * (1.0 - alpha) + v_1D[(a + 1) * 5 + 1] * (alpha));
            ff[2] = 0.0;
            ff[3] = 0.0;
          }
          if(varFcn->getType(fid_new) == VarFcnBase::TAIT) {
            ff[4] = v_1D[a * 5 + 4] * (1.0 - alpha) + v_1D[(a + 1) * 5 + 4] * (alpha);
          }
          else {
            ff[4] = v_1D[a * 5 + 2] * (1.0 - alpha) + v_1D[(a + 1) * 5 + 2] * (alpha);
          }
          varFcn->primitiveToConservative(ff, f, fid_new);
        }
        else if(xrad <= rad) {
          fid_new = fids[0];
          if(oned && oned->fluidRemap.dataMap.find(fids[0]) != oned->fluidRemap.dataMap.end()) {
            fid_new = oned->fluidRemap.dataMap.find(fids[0])->second->newID;
          }
          varFcn->primitiveToConservative(boundaryStateL, f, fid_new);
          if(spherical) {
            for(int j = 1; j <= 3; ++j) {
              f[j] = boundaryStateL[0] * boundaryStateL[1] * (loc[j - 1] - bub_x0[j - 1]) / std::max(localRadius, 1.0e-8);
            }
          }
          else {
            f[1] = boundaryStateL[0] * boundaryStateL[1];
            f[2] = f[3] = 0.0;
          }
        }
        else {
          fid_new = fids[numPoints - 1];
          if(oned && oned->fluidRemap.dataMap.find(fids[numPoints - 1]) != oned->fluidRemap.dataMap.end()) {
            fid_new = oned->fluidRemap.dataMap.find(fids[numPoints - 1])->second->newID;
          }
          varFcn->primitiveToConservative(boundaryStateR, f, fid_new);
          if(spherical) {
            for(int j = 1; j <= 3; ++j) {
              f[j] = boundaryStateR[0] * boundaryStateR[1] * (loc[j - 1] - bub_x0[j - 1]) / std::max(localRadius, 1.0e-8);
            }
          }
          else {
            f[1] = boundaryStateR[0] * boundaryStateR[1];
            f[2] = f[3] = 0.0;
          }
        }
      }
      else {
        fid_new = fids[numPoints - 1];
        if(oned && oned->fluidRemap.dataMap.find(fids[numPoints - 1]) != oned->fluidRemap.dataMap.end()) {
          fid_new = oned->fluidRemap.dataMap.find(fids[numPoints - 1])->second->newID;
        }
        varFcn->primitiveToConservative(outletState, f, fid_new);
      }
      assert(f[0] > 0.0);
      assert(f[4] > 0.0);
      double v[5];
      varFcn->conservativeToPrimitive(f, v, fid_new);
      for(int i = 0; i < 5; ++i) {
        f[i] = v[i];
      }
    }

   private:
    bool spherical;
    double *x_1D, *v_1D;
    double boundaryStateL[5], boundaryStateR[5], outletState[5];
    NodeSet *X;
    int *fids;
    double bubble_x0, bubble_y0, bubble_z0;
    int numPoints;
    double rad;
    OneDimensionalInputData *oned;
    VarFcn *varFcn;
    double max_distance;
  };

 public:

  void levelSetDerivative(double, Vec<double>&, Vec<double>&);

  void EulerF(double, SVec<double, 5>&, SVec<double, 5>&);
  void PhiF(double, SVec<double, 1>&, SVec<double, 1>&);

  enum ReadMode { ModeU, ModePhi };

  template<int dim, int dimLS>
  static void read1DSolution(IoData&, DistSVec<double, dim>&, DistSVec<double, dimLS> *,
                             FluidSelector *, VarFcn *, Domain&, ReadMode, bool = true);

  template<int dim, int dimLS>
  static void read1DSolution(IoData&, const char *, DistSVec<double, dim>&,
                             DistSVec<double, dimLS> *, FluidSelector *, VarFcn *, Domain&,
                             ReadMode, bool = true);

  void resultsOutput(double, int);
  void restartOutput(double, int);

  void output2DVTK();

 public:
  OneDimensional(int, double *, IoData&, Domain *);
  ~OneDimensional();

  void spatialSetup();
  void temporalSetup();
  void stateInitialization(OneDimensionalInfo&);
  void totalTimeIntegration();
  double computeMaxTimeStep();
  void singleTimeIntegration(double);
  void computeEulerFluxes(SVec<double, 5>&);
  void computeLevelSetFluxes(SVec<double, 1>&);

  static void load1DMesh(IoData&, int&, double *&);

  template<int dim>
  void computeSlopes(SVec<double, dim>&, SVec<double, dim>&, Vec<int>&, bool);

};

#endif

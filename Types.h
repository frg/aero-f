#ifndef __AEROF_TYPES_H__
#define __AEROF_TYPES_H__

#include <boost/container/flat_set.hpp>
#include <Eigen/Core>

// note #1: by default TYPE prec is defined and has value of float
// note #2: to undefine TYPE_PREC configure with cmake -DWITH_TYPE_PREC_FLOAT=OFF ...
#ifdef TYPE_PREC
  typedef TYPE_PREC PrecScalar;
#else
  typedef double PrecScalar;
#endif

// note #1: by default DIM_LS is defined and has value of 1
// note #2: to change the value, configure with (for example) cmake -DDIM_LS=2 ...
// note #3: when DIM_LS is undefined the level-set dimension can be determined at run-time
//          and associated memories are dynamically allocated; this option is unsupported
#ifdef DIM_LS
  const int dimLS = DIM_LS;
#else
  const int dimLS = Eigen::Dynamic;
#endif

// small set, used for elem_to_node and face_to_node
// also node_to_gl_face, node_to_gl_elem, gl_elem_to_gl_node
template<class Key>
using set_t = boost::container::flat_set<Key>;

// note #1: USE_64BIT_INT can be defined by configuring with cmake -DUSE_64BIT_INT=ON ...
// note #2: if USE_64BIT_INT is defined then Aero-F must be configured with 64-bit metis/parmetis libraries
// note #3: currently (as of 01/16/2020) Sower does not support 64-bit integers
#ifdef USE_64BIT_INT
  typedef long int int_t;
#else
  typedef int int_t;
#endif

// pair structs used with corresponding MPI types MPI_DOUBLE_INT and MPI_LONG_INT
struct double_int {
  double val;
  int rank;
};
struct long_int {
  long index;
  int sub;
};

#endif

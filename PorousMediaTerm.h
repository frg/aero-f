#ifndef _POROUS_MEDIA_TERM_H_
#define _POROUS_MEDIA_TERM_H_

#include <IoData.h>

#include <iostream>
#include <map>
#include <cmath>

//------------------------------------------------------------------------------

class PorousMediaTerm {

  double velocity, density, length;
  std::map<int, PorousMedia *> volInfo;

  void computePermittivityTensor(double[3], double[3], double[3], double[9], double *);

  void computeGradPermittivityTensor(double[3], double[3], double[9], double *);

  void multiplyBydVdU(double[4], double[9], double *, double);

 public:
  PorousMediaTerm(IoData&);
  ~PorousMediaTerm() {}

  void rstVar(IoData&);

  std::map<int, PorousMedia *>::iterator find(int material_id) {
    return volInfo.find(material_id);
  }

  std::map<int, PorousMedia *>::iterator end() {
    return volInfo.end();
  }

  double computePorousTurbulentViscosity(std::map<int, PorousMedia *>::iterator, double[3]);

  double computeDerivativeOfPorousTurbulentViscosity(std::map<int, PorousMedia *>::iterator,
                                                     double[3], double[3]);

  double computeSecondPorousTurbulentViscosity(double);

  double computeDerivativeOfSecondPorousTurbulentViscosity(double);

  bool computeVolumeTermPorousCore(double, std::map<int, PorousMedia *>::iterator,
                                   double[3], double *[4], double *);

  bool computeDerivativeOfVolumeTermPorousCore(double, double, std::map<int, PorousMedia *>::iterator,
                                               double[3], double[3], double *[4], double *[4], double *);

  template<int dim>
  bool computeJacobianVolumeTermPorousCore(double, std::map<int, PorousMedia *>::iterator,
                                           double[3], double *[4], double (*)[4][dim][dim]);

};

//------------------------------------------------------------------------------

inline
PorousMediaTerm::PorousMediaTerm(IoData& iod) {
  velocity = iod.ref.rv.velocity;
  density = iod.ref.rv.density;
  length = iod.ref.rv.length;

  // construction of volInfo (map from id to porousMedia)
  // ...loop on all the VolumeData and check which one is a PorousMedia...
  auto& volData = iod.volumes.volumeMap.dataMap;
  if(!volData.empty()) {
    for(auto it = volData.begin(); it != volData.end(); it++) {
      //...if it is a PorousMedia, add it to volInfo
      if(it->second->type == VolumeData::POROUS) {
        //...check if it already exists...
        std::map<int, PorousMedia *>::iterator pmit = volInfo.find(it->first);
        //...otherwise add it...
        if(pmit == volInfo.end()) {
          volInfo[it->first] = &(it->second->porousMedia);
        }
      }
    }
  }
}

//------------------------------------------------------------------------------

inline
void PorousMediaTerm::rstVar(IoData& iod) {
  velocity = iod.ref.rv.velocity;
  density = iod.ref.rv.density;
  length = iod.ref.rv.length;
}

//------------------------------------------------------------------------------

inline
double PorousMediaTerm::computePorousTurbulentViscosity(std::map<int, PorousMedia *>::iterator it,
                                                        double up[3]) {
  double cmu = 0.09;
  double coeff = 1.2247 * pow(cmu, 0.25);
  double Idr = it->second->idr;            // average turbulence intensity
  double Ldr = it->second->ldr / length;   // 0.1*characteristic passage dimension
  double vel = sqrt(up[0] * up[0] + up[1] * up[1] + up[2] * up[2]);
  return coeff * Idr * Ldr * vel;
}

//------------------------------------------------------------------------------

inline
double PorousMediaTerm::computeDerivativeOfPorousTurbulentViscosity(std::map<int, PorousMedia *>::iterator it,
                                                                    double up[3], double dup[3]) {
  std::cerr << "*** Error: PorousMediaTerm::computeDerivativeOfPorousTurbulentViscosity is not implemented\n";
  exit(-1);
}

//------------------------------------------------------------------------------
// simple model that remains true when the Stokes' hypothesis is assumed
inline
double PorousMediaTerm::computeSecondPorousTurbulentViscosity(double mut) {
  return -2.0 * mut / 3.0;
}

//------------------------------------------------------------------------------

inline
double PorousMediaTerm::computeDerivativeOfSecondPorousTurbulentViscosity(double dmut) {
  return -2.0 * dmut / 3.0;
}

//------------------------------------------------------------------------------

inline
bool PorousMediaTerm::computeVolumeTermPorousCore(double tetVol, std::map<int, PorousMedia *>::iterator it,
                                                  double up[3], double *V[4], double *PR) {
  double RR[9], K[9];
  double alpha[3], beta[3];
  double volten = tetVol * (1.0 / 10.0);
  // non-dimensionalization
  alpha[0] = it->second->alphax * length / density;
  alpha[1] = it->second->alphay * length / density;
  alpha[2] = it->second->alphaz * length / density;
  beta[0]  = it->second->betax * length / (density * velocity);
  beta[1]  = it->second->betay * length / (density * velocity);
  beta[2]  = it->second->betaz * length / (density * velocity);
  // transformation matrix
  RR[0] = it->second->iprimex;
  RR[1] = it->second->iprimey;
  RR[2] = it->second->iprimez;
  RR[3] = it->second->jprimex;
  RR[4] = it->second->jprimey;
  RR[5] = it->second->jprimez;
  RR[6] = it->second->kprimex;
  RR[7] = it->second->kprimey;
  RR[8] = it->second->kprimez;
  // permittivity matrix
  computePermittivityTensor(alpha, beta, up, RR, K);
  double SS[4][3];
  SS[0][0] = volten * (V[0][1] + 0.5 * (V[1][1] + V[2][1] + V[3][1]));
  SS[0][1] = volten * (V[0][2] + 0.5 * (V[1][2] + V[2][2] + V[3][2]));
  SS[0][2] = volten * (V[0][3] + 0.5 * (V[1][3] + V[2][3] + V[3][3]));
  SS[1][0] = volten * (V[1][1] + 0.5 * (V[0][1] + V[2][1] + V[3][1]));
  SS[1][1] = volten * (V[1][2] + 0.5 * (V[0][2] + V[2][2] + V[3][2]));
  SS[1][2] = volten * (V[1][3] + 0.5 * (V[0][3] + V[2][3] + V[3][3]));
  SS[2][0] = volten * (V[2][1] + 0.5 * (V[1][1] + V[0][1] + V[3][1]));
  SS[2][1] = volten * (V[2][2] + 0.5 * (V[1][2] + V[0][2] + V[3][2]));
  SS[2][2] = volten * (V[2][3] + 0.5 * (V[1][3] + V[0][3] + V[3][3]));
  SS[3][0] = volten * (V[3][1] + 0.5 * (V[1][1] + V[2][1] + V[0][1]));
  SS[3][1] = volten * (V[3][2] + 0.5 * (V[1][2] + V[2][2] + V[0][2]));
  SS[3][2] = volten * (V[3][3] + 0.5 * (V[1][3] + V[2][3] + V[0][3]));
  // Initialize PR (porous media term)
  for(int j = 0; j < 12; ++j) {
    PR[j] = 0.0;
  }
  // FE flux for the porous sink term
  for(int j = 0; j < 4; ++j) {
    for(int k = 0; k < 3; ++k) {
      PR[3 * j + k] += (K[3 * k + 0] * SS[j][0] + K[3 * k + 1] * SS[j][1] + K[3 * k + 2] * SS[j][2]);
    }
  }
  return true;
}

//------------------------------------------------------------------------------

inline
bool PorousMediaTerm::computeDerivativeOfVolumeTermPorousCore(double tetVol, double dTetVol, std::map<int, PorousMedia *>::iterator it,
                                                              double up[3], double dup[3], double *V[4], double *dV[4],
                                                              double *dPR) {
  std::cerr << "*** Error: PorousMediaTerm::computeDerivativeOfVolumeTermPorousCore is not implemented\n";
  exit(-1);
}

//------------------------------------------------------------------------------

template<int dim>
inline
bool PorousMediaTerm::computeJacobianVolumeTermPorousCore(double tetVol, std::map<int, PorousMedia *>::iterator it,
                                                          double up[3], double *V[4], double (*dPdU)[4][dim][dim]) {
  double RR[9], K[9], B[9];
  double alpha[3], beta[3];
  double volten = tetVol * (1.0 / 10.0);
  // non-dimensionalization correction
  alpha[0] = it->second->alphax * length / density;
  alpha[1] = it->second->alphay * length / density;
  alpha[2] = it->second->alphaz * length / density;
  beta[0]  = it->second->betax * length / (density * velocity);
  beta[1]  = it->second->betay * length / (density * velocity);
  beta[2]  = it->second->betaz * length / (density * velocity);
  // transformation matrix
  RR[0] = it->second->iprimex;
  RR[1] = it->second->iprimey;
  RR[2] = it->second->iprimez;
  RR[3] = it->second->jprimex;
  RR[4] = it->second->jprimey;
  RR[5] = it->second->jprimez;
  RR[6] = it->second->kprimex;
  RR[7] = it->second->kprimey;
  RR[8] = it->second->kprimez;
  // permittivity matrix
  computePermittivityTensor(alpha, beta, up, RR, K);
  // gradient of permittivity matrix
  computeGradPermittivityTensor(alpha, up, RR, B);
  double SS[4][3];
  SS[0][0] = volten * (V[0][1] + 0.5 * (V[1][1] + V[2][1] + V[3][1]));
  SS[0][1] = volten * (V[0][2] + 0.5 * (V[1][2] + V[2][2] + V[3][2]));
  SS[0][2] = volten * (V[0][3] + 0.5 * (V[1][3] + V[2][3] + V[3][3]));
  SS[1][0] = volten * (V[1][1] + 0.5 * (V[0][1] + V[2][1] + V[3][1]));
  SS[1][1] = volten * (V[1][2] + 0.5 * (V[0][2] + V[2][2] + V[3][2]));
  SS[1][2] = volten * (V[1][3] + 0.5 * (V[0][3] + V[2][3] + V[3][3]));
  SS[2][0] = volten * (V[2][1] + 0.5 * (V[1][1] + V[0][1] + V[3][1]));
  SS[2][1] = volten * (V[2][2] + 0.5 * (V[1][2] + V[0][2] + V[3][2]));
  SS[2][2] = volten * (V[2][3] + 0.5 * (V[1][3] + V[0][3] + V[3][3]));
  SS[3][0] = volten * (V[3][1] + 0.5 * (V[1][1] + V[2][1] + V[0][1]));
  SS[3][1] = volten * (V[3][2] + 0.5 * (V[1][2] + V[2][2] + V[0][2]));
  SS[3][2] = volten * (V[3][3] + 0.5 * (V[1][3] + V[2][3] + V[0][3]));
  for(int k = 0; k < 4; ++k) {
    double BB[3];
    BB[0]  = B[0] * SS[k][0] +  B[1] * SS[k][1] +  B[2] * SS[k][2];
    BB[1]  = B[3] * SS[k][0] +  B[4] * SS[k][1] +  B[5] * SS[k][2];
    BB[2]  = B[6] * SS[k][0] +  B[7] * SS[k][1] +  B[8] * SS[k][2];
    double BV[9];
    BV[0] = up[0] * BB[0];
    BV[1] = up[1] * BB[0];
    BV[2] = up[2] * BB[0];
    BV[3] = up[0] * BB[1];
    BV[4] = up[1] * BB[1];
    BV[5] = up[2] * BB[1];
    BV[6] = up[0] * BB[2];
    BV[7] = up[1] * BB[2];
    BV[8] = up[2] * BB[2];
    for(int j = 0; j < 4; ++j) {
      double v[4] = {V[j][0], V[j][1], V[j][2], V[j][3]};
      double KU[25];
      multiplyBydVdU(v, K, KU, volten);
      double BU[25];
      multiplyBydVdU(v, BV, BU, 1.0);
      double cKU = (k == j) ? 1.0 : 0.5;
      for(int l = 0; l < 5; l++) {
        dPdU[k][j][0][l] = 0.0;
        dPdU[k][j][1][l] = cKU * KU[ 5 + l] + BU[ 5 + l];
        dPdU[k][j][2][l] = cKU * KU[10 + l] + BU[10 + l];
        dPdU[k][j][3][l] = cKU * KU[15 + l] + BU[15 + l];
        dPdU[k][j][4][l] = 0.0;
      }
    }
  }
  return true;
}

//------------------------------------------------------------------------------

inline
void PorousMediaTerm::computePermittivityTensor(double alpha[3], double beta[3], double ucg[3],
                                                double R[9], double *K) {
  double norm_u = sqrt(ucg[0] * ucg[0] + ucg[1] * ucg[1] + ucg[2] * ucg[2]);
  double diag[3] = {alpha[0] *norm_u + beta[0],
                    alpha[1] *norm_u + beta[1],
                    alpha[2] *norm_u + beta[2]
                   };
  double Rt_diag[9] = { R[0] *diag[0], R[3] *diag[1], R[6] *diag[2],
                        R[1] *diag[0], R[4] *diag[1], R[7] *diag[2],
                        R[2] *diag[0], R[5] *diag[1], R[8] *diag[2]
                      };
  for(int i = 0; i < 9; ++i) {
    K[i] = 0.0;
  }
  for(int i = 0; i < 3; ++i) {
    for(int j = 0; j < 3; ++j) {
      for(int k = 0; k < 3; ++k) {
        K[3 * i + j] += Rt_diag[3 * i + k] * R[3 * k + j];
      }
    }
  }
}

//------------------------------------------------------------------------------

inline
void PorousMediaTerm::computeGradPermittivityTensor(double alpha[3], double ucg[3],
                                                    double R[9], double *B) {
  double onefourth = 1.0 / 4.0;
  double norm_u = sqrt(ucg[0] * ucg[0] + ucg[1] * ucg[1] + ucg[2] * ucg[2]);
  double inv_norm = 1.0 / norm_u;
  double diag[3] = {onefourth * inv_norm * alpha[0],
                    onefourth * inv_norm * alpha[1],
                    onefourth * inv_norm * alpha[2]
                   };
  double Rt_diag[9] = { R[0] *diag[0], R[3] *diag[1], R[6] *diag[2],
                        R[1] *diag[0], R[4] *diag[1], R[7] *diag[2],
                        R[2] *diag[0], R[5] *diag[1], R[8] *diag[2]
                      };
  for(int i = 0; i < 9; ++i) {
    B[i] = 0.0;
  }
  for(int i = 0; i < 3; ++i) {
    for(int j = 0; j < 3; ++j) {
      for(int k = 0; k < 3; ++k) {
        B[3 * i + j] += Rt_diag[3 * i + k] * R[3 * k + j];
      }
    }
  }
}

//------------------------------------------------------------------------------

inline
void PorousMediaTerm::multiplyBydVdU(double V[4], double mat[9], double *res, double scaling) {
  double rho = V[0];
  double invRho = 1.0 / rho;
  double u = V[1];
  double v = V[2];
  double w = V[3];
  res[0] = 0.0;
  res[1] = 0.0;
  res[2] = 0.0;
  res[3] = 0.0;
  res[4] = 0.0;
  res[5] = -invRho * (mat[0] * u + mat[1] * v + mat[2] * w) * scaling;
  res[6] = mat[0] * invRho * scaling;
  res[7] = mat[1] * invRho * scaling;
  res[8] = mat[2] * invRho * scaling;
  res[9] = 0.0;
  res[10] = -invRho * (mat[3] * u + mat[4] * v + mat[5] * w) * scaling;
  res[11] = mat[3] * invRho * scaling;
  res[12] = mat[4] * invRho * scaling;
  res[13] = mat[5] * invRho * scaling;
  res[14] = 0.0;
  res[15] = -invRho * (mat[6] * u + mat[7] * v + mat[8] * w) * scaling;
  res[16] = mat[6] * invRho * scaling;
  res[17] = mat[7] * invRho * scaling;
  res[18] = mat[8] * invRho * scaling;
  res[19] = 0.0;
  res[20] = 0.0;
  res[21] = 0.0;
  res[22] = 0.0;
  res[23] = 0.0;
  res[24] = 0.0;
}

//------------------------------------------------------------------------------

#endif

#ifndef _NODE_H_
#define _NODE_H_

#include <BinFileHandler.h>
#include <Types.h>
#include <Vector3D.h>

#include <vector>

//------------------------------------------------------------------------------

class NodeSet {

 public:
  typedef std::vector<Vec3D> Container;

 private:
  Container nodes;

 public:
  NodeSet(int n) {
    nodes.resize(n);
  }
  NodeSet(const NodeSet& other) {
    nodes = other.nodes;
  }
  ~NodeSet() {}

  Container& getNodes() {
    return nodes;
  }

  Vec3D& operator[](int i) {
    return nodes[i];
  }

  void addNode(int i, Vec3D node) {
    if(i == nodes.size()) nodes.push_back(node);
    else nodes[i] = node;
  }

  void removeNode(int i) {
    nodes.erase(nodes.begin()+i);
  }

  auto begin() -> decltype(nodes.begin()) { return nodes.begin(); }

  auto end() -> decltype(nodes.end()) { return nodes.end(); }

  int size() const {
    return int(nodes.size());
  }

  int read(BinFileHandler&, int, int (*)[2], int_t *, int *, int);

  void write(BinFileHandler&, int, BinFileHandler::OffType,
             int, int (*)[3], const int_t *, double, int);

  NodeSet& operator*=(double d) {
    for(auto& node : nodes) node *= d;
    return *this;
  }

};

//------------------------------------------------------------------------------

#endif

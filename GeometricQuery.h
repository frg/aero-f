#ifndef _GEOMETRIC_QUERY_H_
#define _GEOMETRIC_QUERY_H_

#include <IoData.h>

#include <Eigen/Core>
#ifdef USE_GTE
#include <Mathematics/Hyperellipsoid.h>
#include <Mathematics/Line.h>
#endif
#ifdef USE_CGAL
#include <CGAL/Simple_cartesian.h>
#include <CGAL/Filtered_kernel.h>
#include <CGAL/AABB_tree.h>
#include <CGAL/AABB_traits.h>
#include <CGAL/AABB_triangle_primitive.h>
#endif

#include <array>
#include <map>
#include <vector>

class Vec3D;
template<typename Scalar> class Vec;

//------------------------------------------------------------------------------

class Source {
protected:
  SourceData::CorotatingFrame corotating_frame;
  SourceData::Predictor predictor;
  int predictor_level;
  Eigen::Vector3d cg_0; // origin of attached corotating frame

public:
  Source();
  Source(const SourceData&);
  virtual ~Source() {}

  bool use_corotating_frame();
  bool use_predictor();
  int get_predictor_level();
  void initializeCorotatingFrame(double[3]);
  virtual void updateCorotatingFrame(double[3], double[3][3]) {}
  virtual void updateAxisAlignedBoundingBoxTree(int, int (*)[3], Vec<Vec3D>&, int *) {}

  virtual Eigen::Vector3d direction(const Eigen::Vector3d&);
  virtual Eigen::Vector3d distance_and_direction(const Eigen::Vector3d&, double&);
  virtual double distance(const Eigen::Vector3d&);
  virtual double distance(const Eigen::Vector3d&, const Eigen::Vector3d&);
  virtual bool intersects(const Eigen::Vector3d&, const Eigen::Vector3d&);
};

//------------------------------------------------------------------------------

class Point : public Source {
  const PointData& pointData;
  Eigen::Vector3d o;

public:
  Point(const SourceData&);

  void updateCorotatingFrame(double[3], double[3][3]);

  Eigen::Vector3d direction(const Eigen::Vector3d&);
  Eigen::Vector3d distance_and_direction(const Eigen::Vector3d&, double&);
  double distance(const Eigen::Vector3d&);
  double distance(const Eigen::Vector3d&, const Eigen::Vector3d&);
  bool intersects(const Eigen::Vector3d&, const Eigen::Vector3d&);
};

//------------------------------------------------------------------------------

class Line : public Source {
  const LineData& lineData;
#ifdef USE_GTE
  gte::Line3<double> line;
#endif

public:
  Line(const SourceData&);

  void updateCorotatingFrame(double[3], double[3][3]);

  Eigen::Vector3d direction(const Eigen::Vector3d&);
  Eigen::Vector3d distance_and_direction(const Eigen::Vector3d&, double&);
  double distance(const Eigen::Vector3d&);
  double distance(const Eigen::Vector3d&, const Eigen::Vector3d&);
  bool intersects(const Eigen::Vector3d&, const Eigen::Vector3d&);
};

//------------------------------------------------------------------------------

class Plane : public Source {
  const PlaneData& planeData;
  Eigen::Vector3d n, o;

  double signed_distance(const Eigen::Vector3d&);

public:
  Plane(const SourceData&);

  void updateCorotatingFrame(double[3], double[3][3]);

  Eigen::Vector3d direction(const Eigen::Vector3d&);
  Eigen::Vector3d distance_and_direction(const Eigen::Vector3d&, double&);
  double distance(const Eigen::Vector3d&);
  double distance(const Eigen::Vector3d&, const Eigen::Vector3d&);
  bool intersects(const Eigen::Vector3d&, const Eigen::Vector3d&);
};

//------------------------------------------------------------------------------

class Ellipsoid : public Source {
  const EllipsoidData& ellipsoidData;
#ifdef USE_GTE
  gte::Ellipsoid3<double> ellipsoid;
  // The ellipsoid is defined implicitly by
  //   q(x,y,z) = c[0] + c[1]*x + c[2]*y + c[3]*z + c[4]*x^2 + c[5]*x*y + c[6]*x*z + c[7]*y^2 + c[8]*y*z + c[9]*z^2 = 0
  // A normal vector at (x,y,z) is
  //   grad[Q](x,y,z) = (c[1] + 2*c[4]*x + c[5]*y + c[6]*z, c[2] + c[5]*x + 2*c[7]*y + c[8]*z, c[3] + c[6]*x + c[8]*y + 2*c[9]*z)
  std::array<double, 10> c;
#endif

  double signed_distance(const Eigen::Vector3d&);

public:
  Ellipsoid(const SourceData&);

  void updateCorotatingFrame(double[3], double[3][3]);

  Eigen::Vector3d direction(const Eigen::Vector3d&);
  Eigen::Vector3d distance_and_direction(const Eigen::Vector3d&, double&);
  double distance(const Eigen::Vector3d&);
  double distance(const Eigen::Vector3d&, const Eigen::Vector3d&);
  bool intersects(const Eigen::Vector3d&, const Eigen::Vector3d&);
};

//------------------------------------------------------------------------------

class TriangleSet : public Source {
  int surfaceID;
  double eps;
#ifdef USE_CGAL
#ifdef USE_FILTERED_KERNEL
  typedef CGAL::Simple_cartesian<double> CK;
  typedef CGAL::Filtered_kernel<CK> K;
#else
  typedef CGAL::Simple_cartesian<double> K;
#endif
  typedef K::Point_3 Point;
  typedef K::Segment_3 Segment;
  typedef K::Triangle_3 Triangle;
  typedef K::Ray_3 Ray;
  typedef std::vector<Triangle>::iterator Iterator;
  typedef CGAL::AABB_triangle_primitive<K, Iterator> Primitive;
  typedef std::pair<Point, Primitive::Id> Point_and_primitive_id;
  typedef CGAL::AABB_traits<K, Primitive> AABB_triangle_traits;
  typedef CGAL::AABB_tree<AABB_triangle_traits> Tree;

  std::vector<Triangle> triangles;
  Tree tree;
#endif

public:
  TriangleSet(int = -1, double = 1e-12);
  TriangleSet(const SourceData&);

  void updateAxisAlignedBoundingBoxTree(int, int (*)[3], Vec<Vec3D>&, int *);

  Eigen::Vector3d direction(const Eigen::Vector3d&);
  Eigen::Vector3d direction_and_id(const Eigen::Vector3d&, int&);
  Eigen::Vector3d distance_and_direction(const Eigen::Vector3d&, double&);
  double distance(const Eigen::Vector3d&);
  bool isinside(const Eigen::Vector3d&);
  double distance(const Eigen::Vector3d&, const Eigen::Vector3d&);
  bool intersects(const Eigen::Vector3d&, const Eigen::Vector3d&);
  void intersection_alpha(const Eigen::Vector3d&, const Eigen::Vector3d&, double&, double&);
};

//------------------------------------------------------------------------------

class GeometricQuery {
  std::vector<Source*> sources;
  std::vector<bool> myMemory;

public:
  GeometricQuery(const std::map<int, SourceData *>&, TriangleSet *);
  ~GeometricQuery();

  int size() { return sources.size(); }

  void initializeCorotatingFrame(double[3]);
  void updateCorotatingFrame(double[3], double[3][3]);

  void updateAxisAlignedBoundingBoxTree(int, int (*)[3], Vec<Vec3D>&, int *, Vec<Vec3D> *, Vec3D *, double);

  Eigen::Vector3d direction(const Vec3D&);
  Eigen::Vector3d distance_and_direction(const Vec3D&, double&, int&);
  double distance(const Vec3D&);
  double distance(const Vec3D&, const Vec3D&);
  bool intersects(const Vec3D&, const Vec3D&);
  bool intersects_other(const Vec3D&, const Vec3D&, int, int);

  void distance_and_directions(const Vec3D&, double&, std::vector<Eigen::Vector3d>&, double = 0.);
};

//------------------------------------------------------------------------------

#endif

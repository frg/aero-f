#ifndef _NODE_PATCH_H_
#define _NODE_PATCH_H_

#include <Types.h>

#include <map>
#include <utility>
#include <set>

struct FaceData
{
  int_t index;
  int code, surface_id;
};

struct NodePatch
{
  int_t node_index_1, node_index_2;
  int edge_index_1, edge_index_2;
  std::set<std::pair<int,int>> elem_index_pairs;
  std::set<std::pair<int,int>> face_index_pairs;
  std::map<int_t,int> gl_elem_to_sub;
  std::map<int_t,FaceData> gl_elem_to_face_data;
  NodePatch() : node_index_1(-1), node_index_2(-1), edge_index_1(-1), edge_index_2(-1) {}
};

#endif

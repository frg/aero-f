#ifndef _GEO_STATE_H_
#define _GEO_STATE_H_

#include <Face.h>

struct Vec3D;

template<class Scalar> class Vec;

//------------------------------------------------------------------------------

class GeoState {

  SVec<double, 3>& X;
  Vec<double>& ctrlVol;
  Vec<double>& ctrlVol_n;
  Vec<double>& ctrlVol_nm1;
  Vec<double>& ctrlVol_nm2;

  Vec<double>& d2wall;
  Vec<Vec3D>& edgeNorm;
  Vec<Vec3D>& faceNorm;
  Vec<double>& edgeNormVel;
  Vec<double>& faceNormVel;
  SVec<double, 6> *edgeUpwindDirections;
  SVec<double, Face::MaxNumNd * 3> *faceUpwindDirections;

  SVec<double, 3> *dX;
  SVec<double, 3> *dXdot;
  Vec<double> *dCtrlVol;
  Vec<double> *dd2wall;
  Vec<Vec3D> *dEdgeNorm;
  Vec<Vec3D> *dFaceNorm;
  Vec<double> *dEdgeNormVel;
  Vec<double> *dFaceNormVel;

 public:
  GeoState(SVec<double, 3>& x, Vec<double>& ctrlvol, Vec<double>& ctrlvol_n, Vec<double>& ctrlvol_nm1,
           Vec<double>& ctrlvol_nm2, Vec<double>& d2w, Vec<Vec3D>& edgenorm,
           Vec<Vec3D>& facenorm, Vec<double>& edgenormvel, Vec<double>& facenormvel,
           SVec<double, 6> *edgeupwdir, SVec<double, Face::MaxNumNd * 3> *faceupwdir)
    : X(x), ctrlVol(ctrlvol), ctrlVol_n(ctrlvol_n), ctrlVol_nm1(ctrlvol_nm1), ctrlVol_nm2(ctrlvol_nm2),
      d2wall(d2w), edgeNorm(edgenorm), faceNorm(facenorm),
      edgeNormVel(edgenormvel), faceNormVel(facenormvel), edgeUpwindDirections(edgeupwdir),
      faceUpwindDirections(faceupwdir), dX(nullptr), dXdot(nullptr), dCtrlVol(nullptr),
      dd2wall(nullptr), dEdgeNorm(nullptr), dFaceNorm(nullptr), dEdgeNormVel(nullptr),
      dFaceNormVel(nullptr) {}
  ~GeoState() {}

  void fsoInitialize(SVec<double, 3> *dx, SVec<double, 3> *dxdot, Vec<double> *dctrlvol,
                     Vec<double> *dd2w, Vec<Vec3D> *dedgenorm, Vec<Vec3D> *dfacenorm,
                     Vec<double> *dedgenormvel, Vec<double> *dfacenormvel) {
    dX = dx;
    dXdot = dxdot;
    dCtrlVol = dctrlvol;
    dd2wall = dd2w;
    dEdgeNorm = dedgenorm;
    dFaceNorm = dfacenorm;
    dEdgeNormVel = dedgenormvel;
    dFaceNormVel = dfacenormvel;
  }

  SVec<double, 3>& getPositionVector() const {
    return X;
  }
  SVec<double, 3>& getdPositionVector() const {
    return *dX;
  }
  Vec<double>& getCtrlVol() const {
    return ctrlVol;
  }
  Vec<double>& getCtrlVol_n() const {
    return ctrlVol_n;
  }
  Vec<double>& getCtrlVol_nm1() const {
    return ctrlVol_nm1;
  }
  Vec<double>& getCtrlVol_nm2() const {
    return ctrlVol_nm2;
  }
  Vec<double>& getdCtrlVol() const {
    return *dCtrlVol;
  }
  Vec<double>& getDistanceToWall() const {
    return d2wall;
  }
  Vec<double>& getdDistanceToWall() const {
    return *dd2wall;
  }
  Vec<Vec3D>& getEdgeNormal() const {
    return edgeNorm;
  }
  Vec<Vec3D>& getdEdgeNormal() const {
    return *dEdgeNorm;
  }
  Vec<Vec3D>& getFaceNormal() const {
    return faceNorm;
  }
  Vec<Vec3D>& getdFaceNormal() const {
    return *dFaceNorm;
  }
  Vec<double>& getEdgeNormalVel() const {
    return edgeNormVel;
  }
  Vec<double>& getdEdgeNormalVel() const {
    return *dEdgeNormVel;
  }
  Vec<double>& getFaceNormalVel() const {
    return faceNormVel;
  }
  Vec<double>& getdFaceNormalVel() const {
    return *dFaceNormVel;
  }
  SVec<double, 6> *getEdgeUpwindDirections() const {
    return edgeUpwindDirections;
  }
  SVec<double, Face::MaxNumNd * 3> *getFaceUpwindDirections() const {
    return faceUpwindDirections;
  }

};

//------------------------------------------------------------------------------

#endif

#ifndef _DIST_RESTART_STATE_
#define _DIST_RESTART_STATE_

#include <IoData.h>
#include <NodeData.h>
#include <Types.h>

struct DistInfo;
class DistGeoState;
class DistMeshAdapter;
class DistMetricSpace;
class MeshMotionHandler;
class ReinitializeDistanceToWall;

template<int dim> class DistEmbeddedBcData;
template<int dim> class DistTimeState;
template<int dim> class RestartState;
template<int dim> class SpaceOperator;
template<int dim> class TsOutput;
template<int dimLS> class LevelSet;
template<class Scalar> class DistVec;
template<class Scalar, int dim> class DistSVec;
template<int dim, class Scalar> class DistNodalGrad;

template<int dim>
class DistRestartState {
 public:
  DistSVec<double, dim> *Un;
  DistSVec<double, dim> *Unm1;
  DistSVec<double, dim> *Unm2;
  DistSVec<double, dim> *U0;
  DistSVec<double, dim> *Unmp;
  DistSVec<double, dim> *Unmp1;
  DistSVec<double, dim> *Unmp2;
  DistSVec<double, dim> *dU;
  DistSVec<double, dim> *dUPrev;
  DistSVec<double, 3> *X;
  DistSVec<double, 3> *Xn;
  DistSVec<double, 3> *Xnm1;
  DistSVec<double, 3> *Xnm2;
  DistSVec<double, 3> *Xsave;
  DistSVec<double, 3> *Xs;
  DistVec<double> *d2wall;
  DistVec<double> *d2walln;
  DistVec<double> *d2wallnm1;
  DistVec<double> *d2wallnm2;
  DistVec<double> *d2wallnmp;
  DistVec<double> *d2bfwall;
  DistVec<int> *status;
  DistVec<bool> *is_occluded;
  DistVec<bool> *is_active;
  DistVec<int> *status_nmp;
  DistVec<bool> *is_occluded_nmp;
  DistVec<bool> *ntag;
  DistSVec<double, dim> *limiterphi;
  DistSVec<double, dimLS> *Phi;
  DistSVec<double, dimLS> *Phin;
  DistSVec<double, dimLS> *Phinm1;
  DistSVec<double, dimLS> *Phinm2;
  DistVec<int> *fluidId;
  DistVec<int> *fluidIdn;
  DistVec<int> *fluidIdnm1;
  DistVec<int> *fluidIdnm2;
  DistSVec<double, 3> *modes;
  DistVec<double> *AvQs0, *AvQs1, *AvQs2, *AvQs3, *AvQs4, *AvQs5, *AvQs6, *AvQs7, *AvQs8, *AvQs9,
                  *AvQs10, *AvQs11, *AvQs12, *AvQs13, *AvQs14, *AvQs15, *AvQs16, *AvQs17, *AvQs18;
  DistSVec<double, 3> *AvQv0, *AvQv1, *AvQv2, *AvQv3, *AvQv4, *AvQv5;
  DistSVec<double, 6> *AvQt0, *AvQt1, *AvQt2;
  DistVec<double> *dists;
  DistSVec<double, 3> *dirs;
  DistSVec<double, 9> *mets;
  DistSVec<double, 1> *refScalar0, *refScalar1, *refScalar2, *refScalar3, *refScalar4, *refScalar5, *refScalar6, *refScalar7;
  DistSVec<double, 1> *ddx0, *ddx1, *ddx2, *ddx3, *ddx4, *ddx5, *ddx6, *ddx7;
  DistSVec<double, 1> *ddy0, *ddy1, *ddy2, *ddy3, *ddy4, *ddy5, *ddy6, *ddy7;
  DistSVec<double, 1> *ddz0, *ddz1, *ddz2, *ddz3, *ddz4, *ddz5, *ddz6, *ddz7;
  bool myMemory;

  DistRestartState(DistTimeState<dim> *, DistGeoState *, DistEmbeddedBcData<dim> *, IoData&, SpaceOperator<dim> *,
                   ReinitializeDistanceToWall *, DistSVec<double, dim> *, DistSVec<double, dim> *, DistSVec<double, dim> *,
                   DistSVec<double, dim> *, DistSVec<double, dim> *, DistSVec<double, dimLS> *, LevelSet<dimLS> *,
                   MeshMotionHandler *, TsOutput<dim> *, DistSVec<double, 3> *, DistMetricSpace * = nullptr, DistMeshAdapter * = nullptr);

  DistRestartState(DistSVec<double, dim> *);

  DistRestartState(const DistRestartState<dim>&);

  ~DistRestartState();

  RestartState<dim> operator()(int) const;

  int getIntPacketSize() const;

  int getDblPacketSize() const;

  void resize(const DistInfo&, NodeData *, DistNodalGrad<dim, double> * = nullptr);

  void resize(const DistInfo&, const DistRestartState<dim>&, GenNodeData<4> *);

};

#endif

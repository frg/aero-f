#ifndef _VECTOR_SET_H_
#define _VECTOR_SET_H_

#include <DenseMatrix.h>

#include <iostream>
#include <vector>
#include <limits>
#include <assert.h>

#ifndef _VECSET_TMPL_
  #define _VECSET_TMPL_
  template<class VecType, class Scalar = double> class VecSet;
#endif

//------------------------------------------------------------------------------

template<class VecType, class Scalar>
class VecSet {

  int numVec;
  typename VecType::InfoType len; // for sets of vectors of different length, see the VecSetA class
  std::vector<VecType> vecSet;

 public:
  VecSet(int, const typename VecType::InfoType&);
  VecSet(int, const typename VecType::InfoType&, int);
  VecSet(VecSet<VecType>&);
  ~VecSet() {}

  VecType& operator[](int i) {
    return vecSet[i];
  }

  const VecType& operator[](int i) const {
    return vecSet[i];
  }

  int numVectors() const {
    return numVec;
  }

  const typename VecType::InfoType& vectorSize() const {
    return len;
  }

  void print(const char * = "");
  void resize(int);
  void resizeVectors(const typename VecType::InfoType&);
  void operator=(const Scalar&);
  void operator*=(const Scalar&);

  void applyTransToVector(const VecType&, Scalar *, int = 0);          // A^T * b
  void applyTransToMatrix(const VecSet<VecType>&, Scalar *, int = 0);  // A^T * B
  void applyTransToSelf(Scalar *, int = 0);                            // A^T * A
  void applyTransToSelfAlt(Scalar *, int = 0);                         // A^T * A

  void QR(Scalar *, int = 0);
  void RSVD(Scalar *, Scalar * = nullptr);

  void computeSparsityMasks();

  typename VecType::RealScalarType norm() const;

};

//------------------------------------------------------------------------------

template<class VecType, class Scalar>
VecSet<VecType, Scalar>::VecSet(int _numVec, const typename VecType::InfoType& _len) : len(_len) {
  numVec = _numVec;
  vecSet.resize(numVec, VecType(len));
}

//------------------------------------------------------------------------------

template<class VecType, class Scalar>
VecSet<VecType, Scalar>::VecSet(int _numVec, const typename VecType::InfoType& _len, int dim) : len(_len) {
  numVec = _numVec;
  vecSet.resize(numVec, VecType(len, dim));
}

//------------------------------------------------------------------------------

template<class VecType, class Scalar>
VecSet<VecType, Scalar>::VecSet(VecSet<VecType>& vectorSet) : len(vectorSet.vectorSize()) {
  numVec = vectorSet.numVectors();
  vecSet.reserve(numVec);
  for(int i = 0; i < numVec; ++i) {
    vecSet.emplace_back(vectorSet[i]); // calls VecType copy constructor
  }
}

//------------------------------------------------------------------------------

template<class VecType, class Scalar>
void VecSet<VecType, Scalar>::resize(int n) {
  numVec = n;
  vecSet.resize(numVec, VecType(len));
}

//------------------------------------------------------------------------------

template<class VecType, class Scalar>
void VecSet<VecType, Scalar>::resizeVectors(const typename VecType::InfoType& _len) {
  len = _len;
  for(int i = 0; i < numVec; ++i) {
    vecSet[i].resize(len);
  }
}

//------------------------------------------------------------------------------

template<class VecType, class Scalar>
void VecSet<VecType, Scalar>::print(const char *msg) {
  if(msg) {
    std::cout << msg << std::endl;
  }
  for(int i = 0; i < numVec; ++i) {
    std::cout << "vector " << i << ":";
    vecSet[i].print();
  }
}

//------------------------------------------------------------------------------

template<class VecType, class Scalar>
void VecSet<VecType, Scalar>::operator=(const Scalar& x) {
  for(int i = 0; i < numVec; ++i) {
    vecSet[i] = x;
  }
}

//------------------------------------------------------------------------------
// assumes the VecType class has defined the *=(Scalar) operator (multiply by constant)
template<class VecType, class Scalar>
void VecSet<VecType, Scalar>::operator*=(const Scalar& x) {
  for(int i = 0; i < numVec; ++i) {
    vecSet[i] *= x;
  }
}

//------------------------------------------------------------------------------
// these methods work assuming the VecType class has defined a (local multiplication)
// ^ operator and has an info() method with a .com object (for example DistVec
// or DistSVec)
template<class VecType, class Scalar>
void VecSet<VecType, Scalar>::applyTransToVector(const VecType& b, Scalar *res, int numV) {
  if(numV == 0) numV = numVec;
  for(int i = 0; i < numV; ++i) {
    res[i] = vecSet[i] ^ b;
  }
  len.com->globalSum(numV, res);
}

//------------------------------------------------------------------------------

template<class VecType, class Scalar>
void VecSet<VecType, Scalar>::applyTransToMatrix(const VecSet<VecType>& B, Scalar *res, int numV) {
  if(numV == 0) numV = numVec;
  for(int i = 0; i < numV; ++i) {
    for(int j = 0; j < B.numVec; ++j) {
      res[i * B.numVec + j] = vecSet[i] ^ B[j];  // row major
    }
  }
  len.com->globalSum(numV * B.numVec, res);
}

//------------------------------------------------------------------------------

template<class VecType, class Scalar>
void VecSet<VecType, Scalar>::applyTransToSelf(Scalar *res, int numV) {
  if(numV == 0) numV = numVec;
  for(int i = 0; i < numV; ++i) {
    for(int j = 0; j < numV; ++j) {
      if(j >= i) {
        res[i * numV + j] = vecSet[i] ^ vecSet[j];  // row major, symmetric
      }
      else {
        res[i * numV + j] = res[j * numV + i];
      }
    }
  }
  len.com->globalSum(numV * numV, res);
}

//------------------------------------------------------------------------------

template<class VecType, class Scalar>
void VecSet<VecType, Scalar>::applyTransToSelfAlt(Scalar *res, int numV) {
  if(numV == 0) numV = numVec;
  for(int i = 0; i < numV; ++i) {
    for(int j = 0; j < numV; ++j) {
      if(j >= i) {
        res[i * numV + j] = vecSet[i] * vecSet[j];  // row major, symmetric
      }
      else {
        res[i * numV + j] = res[j * numV + i];
      }
    }
  }
}

//------------------------------------------------------------------------------
// Distributed QR via (modified) Gram-Schmidt (in place)
template<class VecType, class Scalar>
void VecSet<VecType, Scalar>::QR(Scalar *R, int numV) {
  if(numV == 0) numV = numVec;
  const Scalar normTol = std::numeric_limits<Scalar>::epsilon();
  for(int i = 0; i < numV; ++i) {
    for(int j = 0; j < numV; ++j) {
      R[i * numV + j] = 0.0;
    }
    Scalar norm = vecSet[i].norm();
    R[i * numV + i] = norm;
    if(norm >= normTol) {
      vecSet[i] *= (1.0 / norm);
    }
    else {
      vecSet[i].info().com->fprintf(stderr, "*** Warning: QR encountered rank deficient matrix (col = %d, norm = %e)\n",
                                    i, norm);
    }
    for(int j = i + 1; j < numV; ++j) {
      Scalar r = vecSet[i] * vecSet[j];
      vecSet[j] -= r * vecSet[i];
      R[i * numV + j] = r;
    }
  }
}

//------------------------------------------------------------------------------
// Standard R-SVD algorithm (using modified Gram-Schmidt for QR):
//   [Q R] = qr(X)
//   [Utmp S V] = svd(R) (in serial using Eigen)
//   U = Q * Utmp
// NOTE: performs operation in place, overwriting input matrix with U
template<class VecType, class Scalar>
void VecSet<VecType, Scalar>::RSVD(Scalar *Sigma, Scalar *V) {
  // Gram-Schmidt
  vecSet[0].info().com->fprintf(stdout, " ... orthogonalizing\n");
  VecSet<VecType, Scalar> Q(*this);
  DenseMat<double> R(numVec, numVec);
  Q.QR(R.data());
  // SVD (in serial on all processes, and in place)
  vecSet[0].info().com->fprintf(stdout, " ... computing SVD using Eigen\n");
  R.template svd<true>(Sigma, V);
  // Form U = Q * Y
  vecSet[0].info().com->fprintf(stdout, " ... forming U\n");
  for(int i = 0; i < numVec; ++i) {
    vecSet[i] = 0.0;
    for(int j = 0; j < numVec; ++j) {
      vecSet[i] += R[j][i] * Q[j];  // R stored row major
    }
  }
}

//------------------------------------------------------------------------------

template<class VecType, class Scalar>
typename VecType::RealScalarType VecSet<VecType, Scalar>::norm() const {
  Scalar s;
  for(int i = 0; i < numVec; ++i) {
    s += vecSet[i]*vecSet[i];
  }
  return sqrt(s);
}

//------------------------------------------------------------------------------

template<class VecType, class Scalar>
void VecSet<VecType, Scalar>::computeSparsityMasks() {
  for(int iVec = 0; iVec < numVec; iVec++) {
    vecSet[iVec].computeSparsityMask();
  }
}

//------------------------------------------------------------------------------

#endif

#ifndef _DIST_MESH_STATE_
#define _DIST_MESH_STATE_

#include <Domain.h>
#include <Elem.h>
#include <Node.h>
#include <RTree.h>
#include <SubDomain.h>
#include <GeometricQuery.h>

#include <algorithm>
#include <iostream>
#include <limits>
#include <vector>

#define USE_TRIANGLE_SET

//------------------------------------------------------------------------------

class DistMeshState {
 public:
  std::vector<NodeSet*> nodes;
  std::vector<ElemSet*> elems;
  std::vector<RTree<Elem>*> rtree;
  std::vector<int> sn;

  DistMeshState(Domain *domain) {
    int numLocSub = domain->getNumLocSub();
    SubDomain **subDomain = domain->getSubDomain();
    #pragma omp parallel for
    for(int iSub = 0; iSub < numLocSub; ++iSub) {
      nodes.emplace_back(new NodeSet(subDomain[iSub]->getNodes()));
      elems.emplace_back(new ElemSet);
      rtree.emplace_back(subDomain[iSub]->getMyTree()->clone(elems.back()->getElems(), elems.back()->memElems));
      sn.emplace_back(subDomain[iSub]->getNodes().size());
    }
  }

  ~DistMeshState() {
    for(auto p : nodes) delete p;
    for(auto p : elems) delete p;
    for(auto p : rtree) delete p;
  }

  void findNodes(Domain *domain, GenNodeData<4> *meshAdapter, double eps0, TriangleSet *triangleSet = nullptr) {
    #pragma omp parallel for
    for(int iSub = 0; iSub < elems.size(); ++iSub) {
      SubDomain **subDomain = domain->getSubDomain();
      GenNodeData<4>& nodeData = meshAdapter[iSub];
      for(int node_index = sn[iSub]; node_index < subDomain[iSub]->numNodes(); ++node_index) {
        // find the element in the pre-coarsened mesh that contains this node from the refined mesh
        Vec3D V(subDomain[iSub]->getNodes()[node_index]);
        double bary[3];
        Elem *E = nullptr;
        double maxit = 100;
        for(int i = 0; i < maxit; ++i) {
          double eps = (i-1) * eps0;
          if((E = rtree[iSub]->search<NodeSet, &Elem::isPointInside>(*nodes[iSub], V, bary, eps))) break;
        }
        if(!E) {
          // note: if this error is raised, try increasing maxit and/or eps0 (see also MeshAdapter::coarsenNodes_Step2i)
          std::cerr << "*** Error: in DistMeshState::findNodes: V = [" << V[0] << " " << V[1] << " " << V[2] << "]\n";
        }
        else {
          std::array<int, 4> nn = {{E->nodeNum(0), E->nodeNum(1), E->nodeNum(2), E->nodeNum(3)}};
          std::array<double, 4> cn = {{bary[0], bary[1], bary[2], 1-bary[0]-bary[1]-bary[2]}};
          int tag = 1;
#ifdef USE_TRIANGLE_SET
          if(triangleSet) {
            Eigen::Vector3d pi, pj;
            pi << V[0], V[1], V[2];
            // find candidate nodes that can be used to interpolate between or extrapolate from
            std::vector<int> candidates; candidates.reserve(4);
            for(int j = 0; j < 4; ++j) {
              pj << (*nodes[iSub])[nn[j]][0], (*nodes[iSub])[nn[j]][1], (*nodes[iSub])[nn[j]][2];
              if(!triangleSet->intersects(pi, pj)) { // ij is not intersected
                candidates.push_back(j);
              }
            }
            if(candidates.size() == 0) {
              // updateSweptNodes function will be used to populate states at nn
              tag = -1;
            }
            else if(candidates.size() < 4) {
              // find the closest candidate node
              double dmin = std::numeric_limits<double>::max(); int jmin;
              for(int j : candidates) {
                pj << (*nodes[iSub])[nn[j]][0], (*nodes[iSub])[nn[j]][1], (*nodes[iSub])[nn[j]][2];
                double d = (pi - pj).norm();
                if(d < dmin) {
                  dmin = d;
                  jmin = j;
                }
              }
              // activate constant extrapolation from closest candidate node by:
              //   (a) changing the ordering of nodeData so that the closest node is first, and
              //   (b) setting the local tag to zero (see resize functions in Vector.h)
              if(jmin != 0) {
                std::swap(nn[0], nn[jmin]);
                std::swap(cn[0], cn[jmin]);
              }
              tag = 0;
            }
          }
#endif
          nodeData.emplace_back(GenNodeData<4>::value_type(node_index, nn, cn, tag, 0.));
        }
      }
    }
  }

  void reset() {
    for(int iSub = 0; iSub < sn.size(); ++iSub) {
      sn[iSub] = 0; 
    }
  }

  void update(Domain *domain) {
    int numLocSub = domain->getNumLocSub();
    SubDomain **subDomain = domain->getSubDomain();
    for(int iSub = 0; iSub < numLocSub; ++iSub) {
      sn[iSub] = subDomain[iSub]->getNodes().size();
    }
  }

};

#endif

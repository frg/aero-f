#ifndef _PARALLEL_ROM_H_
#define _PARALLEL_ROM_H_

#include <vector>

class Domain;
class Communicator;
class DistInfo;

template<class MatrixType> class DistMat;
template<class Scalar> class DenseMat;

//------------------------------------------------------------------------------
// Wrapper class for ScaLAPACK operations, generalized for matrices stored as
// sets of DistSVecs or DistVecs, or DistMat<DenseMat> as well
template<int dim>
class ParallelRom {

  const int INT_ZERO = 0;
  const int INT_ONE  = 1;
  const char charV = 'V';
  const char charN = 'N';
  const char charU = 'U';
  const char charC = 'C';

  Domain *domain;
  Communicator *com;
  DistInfo *distInfo;

  bool isinit;
  int ictxt, npcol, nprow, myrow, mycol;

  std::vector<int> masterNodes;  // CPU node count from original mesh decomposition
  std::vector<int> cpuNodes;     // CPU node count for desired block cyclic decomposition
  std::vector<int> receivedNodes;
  int desc_a[9], desc_b[9];

  std::vector<double> submat_a, submat_b, work;

 private:
  void setBlocking(int, int, int&, int&);

  template<class VecContainer1, class VecContainer2>
  void distributeData(VecContainer1 *, double *, VecContainer2 *, double *);

  template<class VecContainer1, class VecContainer2>
  void distributeData_Step1(VecContainer1 *, VecContainer2 *, double *,
                            int, int, int, int&, int&, int&);

  template<class VecContainer1, class VecContainer2>
  void distributeData_Step2(VecContainer1 *, VecContainer2 *, double *, double *,
                            int, int, int, int);

  template<class VecContainer2>
  void distributeData_Step1(DistMat<DenseMat<double>> *, VecContainer2 *, double *,
                            int, int, int, int&, int&, int&);

  template<class VecContainer2>
  void distributeData_Step2(DistMat<DenseMat<double>> *, VecContainer2 *, double *, double *,
                            int, int, int, int);

  template<class VecContainer>
  void gatherDataSVD(double *, VecContainer&);

  void gatherDataLS(double *, double *);

 public:
  ParallelRom(Domain *);
  ~ParallelRom();

  void resize(DistInfo&, int, int);
  void resize(int, int, int);

  template<class VecContainer>
  void parallelSVD(VecContainer&, double *, double * = nullptr);

  template<class VecContainer1, class VecContainer2>
  void parallelLS(VecContainer1&, VecContainer2&, double *);

  template<class VecContainer>
  void parallelQPF(VecContainer&, double *, int *);

};

//------------------------------------------------------------------------------

#endif

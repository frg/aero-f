#ifndef _WALL_FCN_H_
#define _WALL_FCN_H_

class IoData;
class VarFcn;
class ViscoFcn;
class Communicator;

struct Vec3D;

//------------------------------------------------------------------------------
/*
@ARTICLE{reichardt-42,
  author = "Reichardt, H.",
  title = "Gesetzm{\"a}ssigkeiten der freien {T}urbulenz",
  journal = "VDI-Forschungsheft 414, 1st edition, Berlin",
  year = 1942,
}
*/
class WallFcn {

  const static double third;
  const static double eleventh;

  double prandtl;

  VarFcn *varFcn;
  ViscoFcn *viscoFcn;

 protected:

  int maxits;
  double eps;
  double vkcst;
  double vsl_threshold;
  bool use_cutoff;
  double dRedMach;
  double reynolds;
  double prandtlT;
  int integrationType;
  int codeWall;
  double isoTemp;
  int shearStressType;
  double k_uni;
  double a_uni;
  double m_uni;
  double b_uni;
  double n_uni;
  double ntrapz;
  double uniFactor;

  virtual void computeWallValues(double, double, double, double, double, double *) {}

  virtual void computeDerivativeOfWallValues(double, double, double, double, double, double, double,
                                             double, double, double, double *, double *) {}

  virtual void computeDerivativeOperatorsOfWallValues(double, double, double, Vec3D&, Vec3D&, double,
                                                      double[3], double[3][3], double, double[][3],
                                                      double[][3]) {}

  virtual void bc_RANS_SL(double[3], double[4], double, double, double, double, double, double *) {}

  virtual void bc_RANS_SL_deriv(double[3], double[4], double, double, double, double, double, double *, double *, double[2]) {}

  virtual void bc_RANS_SL_jacderivatives(double *, double *, double[3], double[4], double, double, double *, bool, int) {}

 private:

  Vec3D computeTangentVector(Vec3D&, Vec3D&);

  void computeFaceValues(double[3], double *, double *[3], double&, Vec3D&, double&, Vec3D&, double&,
                         double&, double&, int);

  double computeFrictionVelocity(Vec3D&, double, double, Vec3D&, double, int * = nullptr);

  void computeFrictionVelocityUniversalTerms(double, double, double, double, double&, double&);

  void computeDerivativeOfFrictionVelocityUniversalTerms(double, double, double, double&, double&,
                                                         double&);

  double computeFrictionTemperature(double, double, double, double, double);

  Vec3D computeDerivativeOfTangentVector(Vec3D&, Vec3D&, Vec3D&, Vec3D&);

  Vec3D computeDerivativeOfTangentVector(Vec3D&, Vec3D&, Vec3D&);

  void computeDerivativeOperatorOfUnitVector(Vec3D&, double[3][3]);

  void computeDerivativeOperatorsOfTangentVector(Vec3D&, Vec3D&, double[3][3], double[3][3]);

  void computeDerivativeOfFaceValues(double[3], double *, double *, double *[3], double *[3], double&,
                                     Vec3D&, double&, Vec3D&, double&, double&, double&, int);
  void computeDerivativeOperatorsOfFaceValues(double *[3], int, double[3][3][5], double[3][5], double[3][5],
                                              double&);

  double computeDerivativeOfFrictionVelocity(Vec3D&, Vec3D&, double, double, double, double, Vec3D&, Vec3D&,
                                             double, double, double);
  void computeDerivativeOperatorsOfFrictionVelocity(Vec3D&, double, double, Vec3D&, double, double, double,
                                                    double[3], double&, double&, double[3], double&,
                                                    double&, int);

  double computeDerivativeOfFrictionTemperature(double, double, double, double, double, double, double, double,
                                                double, double, double);
  void computeDerivativeOperatorsOfFrictionTemperature(double, double, double, double, double, double&,
                                                       double&, double&, double&, double&);

 public:

  WallFcn(IoData&, VarFcn *, ViscoFcn *);
  virtual ~WallFcn() {}

  void rstVar(IoData&);

  virtual void computeSurfaceTerm(int, Vec3D&, double[3], double *, double *[3], double *, int);

  void computeDerivativeOfSurfaceTerm(int, Vec3D&, Vec3D&, double[3], double *, double *, double *[3],
                                      double *[3], double, double *, int);

  void computeDerivativeOperatorsOfSurfaceTerm(int, Vec3D&, double[3], double *, double *[3], double[][3],
                                               double[][3], double[][3], double[][3], int);

  virtual Vec3D computeForce(Vec3D&, double[3], double *, double *[3], int);

  virtual Vec3D computeDerivativeOfForce(Vec3D&, Vec3D&, double[3], double *, double *, double *[3], double *[3],
                                         double, int);

  virtual void computeDerivativeOperatorsOfForce(Vec3D&, double[3], double *, double *[3], int, double[3][3],
                                                 double *[3][4], double[3][3]);

  virtual double computeInterfaceWork(Vec3D&, double[3], double *, double *[3], int);

  virtual double computeHeatPower(Vec3D&, double[3], double *, double *[3], int);

  double computeDelta(double[3]);

  virtual double computeDeltaPlus(Vec3D&, double[3], double *, double *[3], int);

  virtual double computeDeltaPlus(Vec3D&, double, Vec3D&, double *, int);

  double computeFrictionVelocity(Vec3D&, double, Vec3D&, double *, int);

  double computeNormalGradient(Vec3D&, double, Vec3D&, double *, int);

  template<int dim>
  void computeVolumeTerm(int, Vec3D&, double, double *, int, double *[4], int, double[3][3], double[3],
                         double, double, double, double[3], double (*)[dim]);

  template<int dim>
  void computeJacobianVolumeTerm(int, Vec3D&, double, double *, int, double *[4], int, double[4][3],
                                 double, double[4][dim], double, double[4][dim], double, double[4][dim],
                                 double (*)[3][dim][dim]);

  template<int dim>
  void computeDerivativeOfVolumeTerm(int, Vec3D&, Vec3D&, double, double, double *, int, double *[4], double *[4],
                                int, double[3][3], double[3][3], double[3], double[3], double, double, double, double, double,
                                double, double, double[3], double[3], double (*)[dim], double (*)[dim] = nullptr);

  virtual void computeNormalGradientLES(double, double, Vec3D&, Vec3D&, double, double&, double, double&, bool, int);

  virtual void computeDerivativeNormalGradientLES(double, double, Vec3D&, Vec3D&, double, double&, double, double&, double *, double *, bool, int);

  virtual void adiabatic_wall_model(const double, const double, const double, const double, const double, double&, double *, double *, int);

  virtual void isothermal_wall_model(const double, const double, const double, const double, const double, double&, double *, double *, int);

  virtual void computeJacobianSurfaceTermLES(int, Vec3D&, double[3], double *, double *[3], double (*)[5][5], int);

  template<int dim>
  void computeJacobianSurfaceTerm(int, Vec3D&, double[3], double *, double *[3], double (*)[dim][dim], int);

  template<int dim>
  void computeBCsJacobianWallValues(int, Vec3D&, double[3], double *, double *, double *[3], int);

  friend class WallFcnODE;

};

//------------------------------------------------------------------------------

class WallFcnSA : public WallFcn {

  const static double coef0;

  double cv1_pow3;

 public:

  WallFcnSA(IoData&, VarFcn *, ViscoFcn *);
  ~WallFcnSA() {}

  void computeWallValues(double, double, double, double, double, double *);

  void computeDerivativeOfWallValues(double, double, double, double, double, double, double, double,
                                     double, double, double *, double *);

  void computeDerivativeOperatorsOfWallValues(double, double, double, Vec3D&, Vec3D&, double, double[3],
                                              double[3][3], double, double[][3], double[][3]);

};

//------------------------------------------------------------------------------

class WallFcnKE : public WallFcn {

  double oocmu;

 public:

  WallFcnKE(IoData&, VarFcn *, ViscoFcn *);
  ~WallFcnKE() {}

  void computeWallValues(double, double, double, double, double, double *);

  void computeDerivativeOfWallValues(double, double, double, double, double, double, double, double,
                                     double, double, double *, double *);

  void computeDerivativeOperatorsOfWallValues(double, double, double, Vec3D&, Vec3D&, double, double[3],
                                              double[3][3], double, double[][3], double[][3]);

};

//------------------------------------------------------------------------------

#endif

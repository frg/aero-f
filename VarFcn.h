#ifndef _VAR_FCN_H_
#define _VAR_FCN_H_

#include <IoData.h>
#include <DistVector.h>
#include <DistMatrix.h>
#include <RectangularSparseMatrix.h>
#include <Types.h>
#include <Vector3D.h>
#include <VarFcnJwl.h>
#include <VarFcnSGEuler.h>
#include <VarFcnSGSA.h>
#include <VarFcnSGKE.h>
#include <VarFcnTait.h>
#include <VarFcnTaitSA.h>
#include <VarFcnTaitKE.h>

#include <cassert>
#include <cmath>
#include <iostream>
#include <complex>

//--------------------------------------------------------------------------
//
// This class is mostly a collection of VarFcns used during the simulation
// and is created dynamically according to the needs of the simulation.
// The methods of a specific VarFcn can be accessed if a tag integer
// is provided for multiphase flows. A default tag value of zero
// always calls the member functions of the first VarFcn.
//
// In addition, gravity related and mesh velocity terms can be found
// in this class.
//
//--------------------------------------------------------------------------

class VarFcn {

  friend class FluxFcn;

 private:
  int numPhases;
  int ghostId;
  VarFcnBase **varFcn;

  double gravity[3];
  double gravity_norm;
  double depth;

  int structId() {
    return ghostId;
  }
  void check(int tag) const {
    assert(0 <= tag && tag < numPhases);
  }

  VarFcnBase *createVarFcnBase(IoData&, FluidModelData&);

 public:
  VarFcn(IoData&);
  ~VarFcn() {
    for(int i = 0; i < numPhases; i++) {
      delete varFcn[i];
    }
    delete [] varFcn;
  }

  void rstVar(IoData& iod);

  int size() {
    return numPhases;
  }

  //----- Transformation Operators -----//
  /* Distributed Operators */
  void computeDerivativeOfPressureConstant(double, DistVec<double>&, DistVec<int> *);
  template<int dim>
  void conservativeToPrimitive(DistSVec<double, dim>&, DistSVec<double, dim>&, DistVec<int> *);
  template<int dim>
  void primitiveToConservative(DistSVec<double, dim>&, DistSVec<double, dim>&, DistVec<int> *);
  template<int dim>
  void conservativeToPrimitiveDerivative(DistSVec<double, dim>&, DistSVec<double, dim>&, DistSVec<double, dim>&,
                                         DistSVec<double, dim>&, double, DistVec<int> *);
  template<int dim>
  void conservativeToPrimitiveDerivative(DistMat<RectangularSparseMat<double, dim, dim>>&, DistMat<RectangularSparseMat<double, 1, dim>>&,
                                         DistSVec<double, dim>&, DistSVec<double, dim>&, DistVec<double>&, DistVec<int> *);
  template<int dim>
  void conservativeToPrimitiveTransposeDerivative(DistMat<RectangularSparseMat<double, dim, dim>>&, DistSVec<double, dim>&, DistSVec<double, dim>&,
                                                  DistVec<int> *);
  template<int dim>
  void computeConservativeToPrimitiveDerivativeOperators(DistSVec<double, dim>&, DistSVec<double, dim>&, DistMat<RectangularSparseMat<double, dim, dim>>&,
                                                         DistMat<RectangularSparseMat<double, 1, dim>>&, DistVec<int> *);
  template<int dim>
  void primitiveToConservativeDerivative(DistMat<RectangularSparseMat<double, dim, dim>>&, DistMat<RectangularSparseMat<double, 1, dim>>&,
                                         DistSVec<double, dim>&, DistSVec<double, dim>&, DistVec<double>&, DistVec<int> *);
  template<int dim>
  void primitiveToConservativeTransposeDerivative(DistMat<RectangularSparseMat<double, dim, dim>>&, DistSVec<double, dim>&, DistSVec<double, dim>&,
                                                  DistVec<int> *);
  template<int dim>
  void computePrimitiveToConservativeDerivativeOperators(DistSVec<double, dim>&, DistSVec<double, dim>&, DistMat<RectangularSparseMat<double, dim, dim>>&,
                                                         DistMat<RectangularSparseMat<double, 1, dim>>&, DistVec<int> *);

  /* Non-Distributed Operators */
  template<int dim>
  void conservativeToPrimitive(SVec<double, dim>&, SVec<double, dim>&, Vec<int> *);
  template<int dim>
  void primitiveToConservative(SVec<double, dim>&, SVec<double, dim>&, Vec<int> *);
  template<int dim>
  void conservativeToPrimitiveDerivative(SVec<double, dim>&, SVec<double, dim>&, SVec<double, dim>&,
                                         SVec<double, dim>&, double, Vec<int> *);
  template<int dim>
  void computeConservativeToPrimitiveDerivativeOperators(SVec<double, dim>&, SVec<double, dim>&, RectangularSparseMat<double, dim, dim>&,
                                                         RectangularSparseMat<double, 1, dim>&, Vec<int> *);
  template<int dim>
  void computePrimitiveToConservativeDerivativeOperators(SVec<double, dim>&, SVec<double, dim>&, RectangularSparseMat<double, dim, dim>&,
                                                         RectangularSparseMat<double, 1, dim>&, Vec<int> *);

  /* Node-Level Operators */
  void conservativeToPrimitive(double *U, double *V, int tag) {
    check(tag);
    varFcn[tag]->conservativeToPrimitive(U, V);
  }
  void primitiveToConservative(double *V, double *U, int tag) {
    check(tag);
    varFcn[tag]->primitiveToConservative(V, U);
  }
  void conservativeToPrimitiveDerivative(double *U, double *dU, double *V, double *dV, double dMach, int tag) {
    check(tag);
    varFcn[tag]->conservativeToPrimitiveDerivative(U, dU, V, dV, dMach);
  }
  void primitiveToConservativeDerivative(double *V, double *dV, double *U, double *dU, double dMach, int tag) {
    check(tag);
    varFcn[tag]->primitiveToConservativeDerivative(V, dV, U, dU, dMach);
  }
  int conservativeToPrimitiveVerification(int_t glob, double *U, double *V, int tag) {
    check(tag);
    return varFcn[tag]->conservativeToPrimitiveVerification(glob, U, V);
  }
  void multiplyBydVdU(double *V, double *vec, double *res, int tag) {
    check(tag);
    varFcn[tag]->multiplyBydVdU(V, vec, res);
  }
  void multiplyBydVdU(double *V, std::complex<double> *vec, std::complex<double> *res, int tag) {
    check(tag);
    varFcn[tag]->multiplyBydVdU(V, vec, res);
  }
  void postMultiplyBydVdU(double *V, double *mat, double *res, int tag) {
    check(tag);
    varFcn[tag]->postMultiplyBydVdU(V, mat, res);
  }
  void postMultiplyBydVdU(double *V, double *mat, std::complex<double> *res, int tag) {
    check(tag);
    varFcn[tag]->postMultiplyBydVdU(V, mat, res);
  }
  void postMultiplyBydVdU(double *V, double *mat, float *res, int tag) {
    check(tag);
    varFcn[tag]->postMultiplyBydVdU(V, mat, res);
  }
  void postMultiplyBydUdV(double *V, double *mat, double *res, int tag) {
    check(tag);
    varFcn[tag]->postMultiplyBydUdV(V, mat, res);
  }
  void preMultiplyBydUdV(double *V, double *mat, double *res, int tag) {
    check(tag);
    varFcn[tag]->preMultiplyBydUdV(V, mat, res);
  }
  void postMultiplyBydVdUmean(double *V, double *mat, double *res, int tag) {
    check(tag);
    varFcn[tag]->postMultiplyBydVdUmean(V, mat, res);
  }
  void postMultiplyBydVdUturb(double *V, double *mat, double *res, int tag) {
    check(tag);
    varFcn[tag]->postMultiplyBydVdUturb(V, mat, res);
  }
  void postMultiplyBydUdVmean(double *V, double *mat, double *res, int tag) {
    check(tag);
    varFcn[tag]->postMultiplyBydUdVmean(V, mat, res);
  }
  void postMultiplyBydUdVturb(double *V, double *mat, double *res, int tag) {
    check(tag);
    varFcn[tag]->postMultiplyBydUdVturb(V, mat, res);
  }
  void extrapolatePrimitive(double un, double c, double *Vb, double *Vinter, double *V, int tag) {
    check(tag);
    varFcn[tag]->extrapolatePrimitive(un, c, Vb, Vinter, V);
  }
  void extrapolateCharacteristic(double n[3], double un, double c, double *Vb, double *dV, int tag) {
    check(tag);
    varFcn[tag]->extrapolateCharacteristic(n, un, c, Vb, dV);
  }
  void primitiveToCharacteristicVariations(double n[3], double *V, double *dV, double *dW, int tag) {
    check(tag);
    varFcn[tag]->primitiveToCharacteristicVariations(n, V, dV, dW);
  }
  void characteristicToPrimitiveVariations(double n[3], double *V, double *dW, double *dV, int tag) {
    check(tag);
    varFcn[tag]->characteristicToPrimitiveVariations(n, V, dW, dV);
  }

  //----- General Functions -----//
  int getType(int tag) const {
    check(tag);
    return varFcn[tag]->getType();
  }
  const char *pname(int dim, int tag) const {
    return varFcn[tag]->pname[dim];
  }
  bool doVerification() const {
    for(int iPhase = 0; iPhase < numPhases; iPhase++)
      if(varFcn[iPhase]->doVerification()) {
        return true;
      }
    return false;
  }
  double getDensity(double *V, int tag) const {
    check(tag);
    return varFcn[tag]->getDensity(V);
  }
  Vec3D getVelocity(double *V, int tag) const {
    check(tag);
    return varFcn[tag]->getVelocity(V);
  }
  Vec3D getWtVelocity(double *V, double meshVel[3], int tag) const {
    check(tag);
    return varFcn[tag]->getWtVelocity(V, meshVel);
  }
  double getVelocityX(double *V, int tag) const {
    check(tag);
    return varFcn[tag]->getVelocityX(V);
  }
  double getVelocityY(double *V, int tag) const {
    check(tag);
    return varFcn[tag]->getVelocityY(V);
  }
  double getVelocityZ(double *V, int tag) const {
    check(tag);
    return varFcn[tag]->getVelocityZ(V);
  }
  double getPressure(double *V, int tag) const {
    check(tag);
    return varFcn[tag]->getPressure(V);
  }
  void computedPdV(double *dPdV, int tag) const {
    check(tag);
    varFcn[tag]->computedPdV(dPdV);
  }
  void setDensity(double density, double *V, int tag) {
    check(tag);
    return varFcn[tag]->setDensity(density, V);
  }
  void setPressure(double p, double *V, int tag) {
    check(tag);
    return varFcn[tag]->setPressure(p, V);
  }
  void setDensity(double *V, double *Vorig, int tag) {
    check(tag);
    return varFcn[tag]->setDensity(V, Vorig);
  }
  void setPressure(double *V, double *Vorig, int tag) {
    check(tag);
    return varFcn[tag]->setPressure(V, Vorig);
  }
  // checks that the Euler equations are still hyperbolic
  double checkPressure(double *V, int tag) const {
    check(tag);
    return varFcn[tag]->checkPressure(V);
  }
  bool checkReconstructedValues(double *V, int nodeNum, int otherNodeNum,
                                int failsafe, int_t *locToGlobNodeMap, int tag) const {
    check(tag);
    return varFcn[tag]->checkReconstructedValues(V, nodeNum, otherNodeNum, failsafe, locToGlobNodeMap);
    // failsafe indicates what kind of message to output: error if there is no failsafe, warning if there is one.
  }
  double computeTemperature(double *V, int tag) const {
    check(tag);
    return varFcn[tag]->computeTemperature(V);
  }
  double computedTdp(double rho, double p, int tag) const {
    check(tag);
    return varFcn[tag]->computedTdp(rho, p);
  }
  double computedTdrho(double rho, double p, int tag) const {
    check(tag);
    return varFcn[tag]->computedTdrho(rho, p);
  }
  double computedrhodT(double rho, double p, int tag) const {
    check(tag);
    return varFcn[tag]->computedrhodT(rho, p);
  }
  double computedpdT(double rho, double p, int tag) const {
    check(tag);
    return varFcn[tag]->computedpdT(rho, p);
  }
  double computedrhodp(double rho, double p, int tag) const {
    check(tag);
    return varFcn[tag]->computedrhodp(rho, p);
  }
  void computeTemperatureGradient(double *V, double *Tg, int tag) const {
    check(tag);
    varFcn[tag]->computeTemperatureGradient(V, Tg);
  }
  void computeTemperatureHessian(double *V, double& Trr, double& Trp,
                                 double& Tpp, int tag) const {
    check(tag);
    varFcn[tag]->computeTemperatureHessian(V, Trr, Trp, Tpp);
  }
  void getV4FromTemperature(double *V, double T, int tag) const {
    check(tag);
    varFcn[tag]->getV4FromTemperature(V, T);
  }
  void getdV4fromdVg(double *Vg, double *dVg, double& dV4, int tag) const {
    check(tag);
    varFcn[tag]->getdV4fromdVg(Vg, dVg, dV4);
  }
  double computeRhoEnergy(double *V, int tag) const {
    check(tag);
    return varFcn[tag]->computeRhoEnergy(V);
  }
  double computeRhoEnergy(double rho, double p, double T, double vel, int tag) const {
    check(tag);
    return varFcn[tag]->computeRhoEnergy(rho, p, T, vel);
  }
  // this function computes the internal energy (=rho*e-0.5*rho*u^2)
  double computeRhoEpsilon(double *V, int tag) const {
    check(tag);
    return varFcn[tag]->computeRhoEpsilon(V);
  }
  double getVelocitySquare(double *V, int tag) const {
    check(tag);
    return varFcn[tag]->getVelocitySquare(V);
  }
  double getWtVelocitySquare(double *V, double meshVel[3], int tag) const {
    check(tag);
    return varFcn[tag]->getWtVelocitySquare(V, meshVel);
  }
  double getVelocityNorm(double *V, int tag) const {
    check(tag);
    return varFcn[tag]->getVelocityNorm(V);
  }
  double getWtVelocityNorm(double *V, double meshVel[3], int tag) const {
    check(tag);
    return varFcn[tag]->getWtVelocityNorm(V, meshVel);
  }
  double computeDensity(double pressure, double temperature, int tag) const {
    check(tag);
    return varFcn[tag]->computeDensity(pressure, temperature);
  }
  double computeMachNumber(double *V, int tag) const {
    check(tag);
    return varFcn[tag]->computeMachNumber(V);
  }
  double computeWtMachNumber(double *V, double meshVel[3], int tag) const {
    check(tag);
    return varFcn[tag]->computeWtMachNumber(V, meshVel);
  }
  double computeSoundSpeed(double *V, int tag) const {
    check(tag);
    return varFcn[tag]->computeSoundSpeed(V);
  }
  double computeSoundSpeed(double density, double entropy, int tag) const {
    check(tag);
    return varFcn[tag]->computeSoundSpeed(density, entropy);
  }
  double computeSoundSpeed2(double density, double pressure, int tag) const {
    check(tag);
    return varFcn[tag]->computeSoundSpeed2(density, pressure);
  }
  double computeEntropy(double density, double pressure, int tag) const {
    check(tag);
    return varFcn[tag]->computeEntropy(density, pressure);
  }
  double computeIsentropicPressure(double entropy, double density, int tag) const {
    check(tag);
    return varFcn[tag]->computeIsentropicPressure(entropy, density);
  }
  double computeTotalPressure(double machr, double *V, int tag) const {
    check(tag);
    return varFcn[tag]->computeTotalPressure(machr, V);
  }
  double getTurbulentNuTilde(double *V, int tag) const {
    check(tag);
    return varFcn[tag]->getTurbulentNuTilde(V);
  }
  double getTurbulentKineticEnergy(double *V, int tag) const {
    check(tag);
    return varFcn[tag]->getTurbulentKineticEnergy(V);
  }
  double getTurbulentDissipationRate(double *V, int tag) const {
    check(tag);
    return varFcn[tag]->getTurbulentDissipationRate(V);
  }
  Vec3D getDerivativeOfVelocity(double *dV, int tag) {
    check(tag);
    return varFcn[tag]->getDerivativeOfVelocity(dV);
  }
  double getDerivativeOfVelocityNorm(double *V, double *dV, int tag) {
    check(tag);
    return varFcn[tag]->getDerivativeOfVelocityNorm(V, dV);
  }
  double computeDerivativeOfTemperature(double *V, double *dV, int tag) {
    check(tag);
    return varFcn[tag]->computeDerivativeOfTemperature(V, dV);
  }
  void computeDerivativeOperatorsOfTemperature(double *V, double *dTdV, int tag) {
    check(tag);
    varFcn[tag]->computeDerivativeOperatorsOfTemperature(V, dTdV);
  }
  void computeJacobianTemperature(double *V, double *dTdU, int tag) {
    check(tag);
    varFcn[tag]->computeJacobianTemperature(V, dTdU);
  }
  double computeDerivativeOfMachNumber(double *V, double *dV, double dMach, int tag) {
    check(tag);
    return varFcn[tag]->computeDerivativeOfMachNumber(V, dV, dMach);
  }
  double computeDerivativeOfSoundSpeed(double *V, double *dV, double dMach, int tag) {
    check(tag);
    return varFcn[tag]->computeDerivativeOfSoundSpeed(V, dV, dMach);
  }
  double computeDerivativeOfTotalPressure(double machr, double dmachr, double *V, double *dV, double dmach, int tag) {
    check(tag);
    return varFcn[tag]->computeDerivativeOfTotalPressure(machr, dmachr, V, dV, dmach);
  }
  double getDerivativeOfPressureConstant(int tag) {
    check(tag);
    return varFcn[tag]->getDerivativeOfPressureConstant();
  }
  double specificHeatCstPressure(int tag) const {
    check(tag);
    return varFcn[tag]->specificHeatCstPressure();
  }
  double computePressureCoefficient(double *V, double pinfty, double mach, bool dimFlag, int tag) const {
    check(tag);
    return varFcn[tag]->computePressureCoefficient(V, pinfty, mach, dimFlag);
  }

  //----- Gravity Related Functions -----//
  double gravity_value() const {
    return gravity_norm;
  }
  double hydrostaticPressure(double density, double X[3]) const {
    return density * (gravity_norm * depth + gravity[0] * X[0] + gravity[1] * X[1] + gravity[2] * X[2]);
  }
  double hydrodynamicPressure(double *V, double X[3], int tag) const {
    check(tag);
    double localP = varFcn[tag]->getPressure(V);
    return localP - hydrostaticPressure(V[0], X);
  }
  double DerivativeHydrostaticPressure(double density, double ddensity,
                                       double *X, double *dX) const {
    return ddensity * (gravity_norm * depth + gravity[0] * X[0] + gravity[1] * X[1] + gravity[2] * X[2])
           + density * (gravity[0] * dX[0] + gravity[1] * dX[1] + gravity[2] * dX[2]);
  }

  //----- Equation of State Parameters -----//
  /* PERFECT/STIFFENED GAS EOS */
  double getGamma(int tag) const {
    check(tag);
    return varFcn[tag]->getGamma();
  }
  double getGamma1(int tag) const {
    check(tag);
    return varFcn[tag]->getGamma1();
  }
  double getPressureConstant(int tag) const {
    check(tag);
    return varFcn[tag]->getPressureConstant();
  }
  /* TAIT EOS */
  double getCv(int tag) const {
    check(tag);
    return varFcn[tag]->getCv();
  }
  double getAlphaWater(int tag) const {
    check(tag);
    return varFcn[tag]->getAlphaWater();
  }
  double getBetaWater(int tag) const {
    check(tag);
    return varFcn[tag]->getBetaWater();
  }
  double getPrefWater(int tag) const {
    check(tag);
    return varFcn[tag]->getPrefWater();
  }
  /* JWL EOS */
  double getOmega(int tag) const {
    check(tag);
    return varFcn[tag]->getOmega();
  }
  double getOmegap1(int tag) const {
    check(tag);
    return varFcn[tag]->getOmegap1();
  }
  double getInvOmega(int tag) const {
    check(tag);
    return varFcn[tag]->getInvOmega();
  }
  double getInvOmegap1(int tag) const {
    check(tag);
    return varFcn[tag]->getInvOmegap1();
  }
  double getA1(int tag) const {
    check(tag);
    return varFcn[tag]->getA1();
  }
  double getA2(int tag) const {
    check(tag);
    return varFcn[tag]->getA2();
  }
  double getR1(int tag) const {
    check(tag);
    return varFcn[tag]->getR1();
  }
  double getR2(int tag) const {
    check(tag);
    return varFcn[tag]->getR2();
  }
  double getRhoref(int tag) const {
    check(tag);
    return varFcn[tag]->getRhoref();
  }
  double getR1r(int tag) const {
    check(tag);
    return varFcn[tag]->getR1r();
  }
  double getR2r(int tag) const {
    check(tag);
    return varFcn[tag]->getR2r();
  }
  double getPmin(int tag) const {
    check(tag);
    return varFcn[tag]->pmin;
  }
  double getNuTildeMin(int tag) const {
    check(tag);
    return varFcn[tag]->nutmin;
  }
  bool isBurnable(int tag) const {
    check(tag);
    return varFcn[tag]->isBurnable();
  }

  //----- EOS related functions -----//
  double computeExponentials(double density, int tag) const {
    check(tag);
    return varFcn[tag]->computeExponentials(density);
  }
  double computeDerivativeOfExponentials(double density, int tag) const {
    check(tag);
    return varFcn[tag]->computeDerivativeOfExponentials(density);
  }
  double computeExponentials2(double density, int tag) const {
    check(tag);
    return varFcn[tag]->computeExponentials2(density);
  }
  double computeDerivativeOfExponentials2(double density, int tag) const {
    check(tag);
    return varFcn[tag]->computeDerivativeOfExponentials2(density);
  }
  double computeFrho(double *V, int tag) const {
    check(tag);
    return varFcn[tag]->computeFrho(V);
  }
  double computeFrho(double density, int tag) const {
    check(tag);
    return varFcn[tag]->computeFrho(density);
  }
  double computeFrhop(double *V, int tag) const {
    check(tag);
    return varFcn[tag]->computeFrhop(V);
  }
  double computeFrhop(double density, int tag) const {
    check(tag);
    return varFcn[tag]->computeFrhop(density);
  }
  VarFcnBase *getVarFcnBase(int tag) const {
    check(tag);
    return varFcn[tag];
  }

};

//------------------------------------------------------------------------------

inline
VarFcn::VarFcn(IoData& iod) {
  numPhases = iod.eqs.numPhase();
  int numRealFluid = numPhases;

  // In the case of Embedded Framework, add a VarFcn for Ghost
  if(iod.problem.type[ProblemData::EMBEDDED_FRAMEWORK]) numPhases++;

  varFcn = new VarFcnBase *[numPhases];
  for(int iPhase = 0; iPhase < numRealFluid; iPhase++) {
    std::map<int, FluidModelData *>::iterator it = iod.eqs.fluidModelMap.dataMap.find(iPhase);
    if(it == iod.eqs.fluidModelMap.dataMap.end()) {
      fprintf(stderr, "*** Error: no FluidModel[%d] was specified\n", iPhase);
      exit(1);
    }
    varFcn[iPhase] = createVarFcnBase(iod, *it->second);
  }

  if(iod.problem.type[ProblemData::EMBEDDED_FRAMEWORK]) {
    ghostId = numPhases - 1;
    varFcn[ghostId] = createVarFcnBase(iod, iod.eqs.fluidModel(0));
  }

  gravity[0] = iod.eqs.gravity_x;
  gravity[1] = iod.eqs.gravity_y;
  gravity[2] = iod.eqs.gravity_z;
  gravity_norm = sqrt(gravity[0] * gravity[0] + gravity[1] * gravity[1] + gravity[2] * gravity[2]);
  depth = iod.bc.hydro.depth;
}

//------------------------------------------------------------------------------

inline
void VarFcn::rstVar(IoData& iod) {
  for(int iPhase = 0; iPhase < iod.eqs.numPhase(); iPhase++) {
    std::map<int, FluidModelData *>::iterator it = iod.eqs.fluidModelMap.dataMap.find(iPhase);
    varFcn[iPhase]->rstVar(iod, *it->second);
  }
  if(iod.problem.type[ProblemData::EMBEDDED_FRAMEWORK]) {
    varFcn[numPhases-1]->rstVar(iod, iod.eqs.fluidModel(0));
  }
  gravity[0] = iod.eqs.gravity_x;
  gravity[1] = iod.eqs.gravity_y;
  gravity[2] = iod.eqs.gravity_z;
  gravity_norm = sqrt(gravity[0] * gravity[0] + gravity[1] * gravity[1] + gravity[2] * gravity[2]);
}

//------------------------------------------------------------------------------

inline
VarFcnBase *VarFcn::createVarFcnBase(IoData& iod, FluidModelData& fluidModel) {
  VarFcnBase *vf_;
  if(fluidModel.fluid == FluidModelData::PERFECT_GAS ||
     fluidModel.fluid == FluidModelData::STIFFENED_GAS) {
    if(iod.eqs.type == EquationsData::NAVIER_STOKES &&
       iod.eqs.tc.type == TurbulenceClosureData::EDDY_VISCOSITY) {
      if(iod.eqs.tc.tm.type == TurbulenceModelData::ONE_EQUATION_SPALART_ALLMARAS ||
         iod.eqs.tc.tm.type == TurbulenceModelData::ONE_EQUATION_DES) {
        vf_ = new VarFcnSGSA(fluidModel);
      }
      else if(iod.eqs.tc.tm.type == TurbulenceModelData::TWO_EQUATION_KE) {
        vf_ = new VarFcnSGKE(fluidModel);
      }
    }
    else {
      vf_ = new VarFcnSGEuler(fluidModel);
    }
  }
  else if(fluidModel.fluid == FluidModelData::LIQUID) {
    if(iod.eqs.type == EquationsData::NAVIER_STOKES &&
       iod.eqs.tc.type == TurbulenceClosureData::EDDY_VISCOSITY) {
      if(iod.eqs.tc.tm.type == TurbulenceModelData::ONE_EQUATION_SPALART_ALLMARAS ||
         iod.eqs.tc.tm.type == TurbulenceModelData::ONE_EQUATION_DES) {
        vf_ = new VarFcnTaitSA(fluidModel);
      }
      else if(iod.eqs.tc.tm.type == TurbulenceModelData::TWO_EQUATION_KE) {
        vf_ = new VarFcnTaitKE(fluidModel);
      }
    }
    else {
      vf_ = new VarFcnTait(fluidModel);
    }
  }
  else if(fluidModel.fluid == FluidModelData::JWL) {
    vf_ = new VarFcnJwl(fluidModel);
  }
  else {
    fprintf(stdout, "No VarFcn created\n");
    fflush(stdout);
    exit(1);
  }
  return vf_;
}

//------------------------------------------------------------------------------

template<int dim>
void VarFcn::conservativeToPrimitive(SVec<double, dim>& U, SVec<double, dim>& V, Vec<int> *tag) {
  if(tag) {
    for(int i = 0; i < U.size(); ++i) {
      check((*tag)[i]);
      varFcn[(*tag)[i]]->conservativeToPrimitive(U[i], V[i]);
    }
  }
  else {
    for(int i = 0; i < U.size(); ++i) {
      varFcn[0]->conservativeToPrimitive(U[i], V[i]);
    }
  }
}

//------------------------------------------------------------------------------

template<int dim>
void VarFcn::conservativeToPrimitive(DistSVec<double, dim>& U, DistSVec<double, dim>& V, DistVec<int> *tag) {
  int numLocSub = U.numLocSub();
  #pragma omp parallel for
  for(int iSub = 0; iSub < numLocSub; ++iSub) {
    double (*u)[dim] = U.subData(iSub);
    double (*v)[dim] = V.subData(iSub);
    if(tag) {
      int *loctag = tag->subData(iSub);
      for(int i = 0; i < U.subSize(iSub); ++i) {
        check(loctag[i]);
        varFcn[loctag[i]]->conservativeToPrimitive(u[i], v[i]);
      }
    }
    else {
      for(int i = 0; i < U.subSize(iSub); ++i) {
        varFcn[0]->conservativeToPrimitive(u[i], v[i]);
      }
    }
  }
}

//------------------------------------------------------------------------------

template<int dim>
void VarFcn::conservativeToPrimitiveDerivative(SVec<double, dim>& U, SVec<double, dim>& dU,
                                               SVec<double, dim>& V, SVec<double, dim>& dV, double dMach, Vec<int> *tag) {
  if(tag) {
    for(int i = 0; i < U.size(); ++i) {
      check((*tag)[i]);
      varFcn[(*tag)[i]]->conservativeToPrimitiveDerivative(U[i], dU[i], V[i], dV[i], dMach);
    }
  }
  else {
    for(int i = 0; i < U.size(); ++i) {
      varFcn[0]->conservativeToPrimitiveDerivative(U[i], dU[i], V[i], dV[i], dMach);
    }
  }
}

//------------------------------------------------------------------------------

template<int dim>
void VarFcn::conservativeToPrimitiveDerivative(DistSVec<double, dim>& U, DistSVec<double, dim>& dU,
                                               DistSVec<double, dim>& V, DistSVec<double, dim>& dV, double dMach, DistVec<int> *tag) {
  int numLocSub = U.numLocSub();
  #pragma omp parallel for
  for(int iSub = 0; iSub < numLocSub; ++iSub) {
    double (*u)[dim] = U.subData(iSub);
    double (*du)[dim] = dU.subData(iSub);
    double (*v)[dim] = V.subData(iSub);
    double (*dv)[dim] = dV.subData(iSub);
    if(tag) {
      int *loctag = tag->subData(iSub);
      for(int i = 0; i < U.subSize(iSub); ++i) {
        varFcn[loctag[i]]->conservativeToPrimitiveDerivative(u[i], du[i], v[i], dv[i], dMach);
      }
    }
    else {
      for(int i = 0; i < U.subSize(iSub); ++i) {
        varFcn[0]->conservativeToPrimitiveDerivative(u[i], du[i], v[i], dv[i], dMach);
      }
    }
  }
}

//------------------------------------------------------------------------------

template<int dim>
void VarFcn::computeConservativeToPrimitiveDerivativeOperators(DistSVec<double, dim>& U, DistSVec<double, dim>& V,
                                                               DistMat<RectangularSparseMat<double, dim, dim>>& dVdU,
                                                               DistMat<RectangularSparseMat<double, 1, dim>>& dVdPstiff,
                                                               DistVec<int> *tag) {
  int numLocSub = U.numLocSub();
  #pragma omp parallel for
  for(int iSub = 0; iSub < numLocSub; ++iSub) {
    computeConservativeToPrimitiveDerivativeOperators(U(iSub), V(iSub), dVdU(iSub), dVdPstiff(iSub),
                                                      (tag ? &((*tag)[iSub]) : nullptr));
  }
}

//------------------------------------------------------------------------------

template<int dim>
void VarFcn::computePrimitiveToConservativeDerivativeOperators(DistSVec<double, dim>& U, DistSVec<double, dim>& V,
                                                               DistMat<RectangularSparseMat<double, dim, dim>>& dUdV,
                                                               DistMat<RectangularSparseMat<double, 1, dim>>& dUdPstiff,
                                                               DistVec<int> *tag) {
  int numLocSub = U.numLocSub();
  #pragma omp parallel for
  for(int iSub = 0; iSub < numLocSub; ++iSub) {
    computePrimitiveToConservativeDerivativeOperators(U(iSub), V(iSub), dUdV(iSub), dUdPstiff(iSub),
                                                      (tag ? &((*tag)[iSub]) : nullptr));
  }
}

//------------------------------------------------------------------------------

template<int dim>
void VarFcn::computeConservativeToPrimitiveDerivativeOperators(SVec<double, dim>& U, SVec<double, dim>& V,
                                                               RectangularSparseMat<double, dim, dim>& dVdU,
                                                               RectangularSparseMat<double, 1, dim>& dVdPstiff,
                                                               Vec<int> *tag) {
  if(tag) {
    for(int i = 0; i < U.size(); ++i) {
      double dVdUarray[dim*dim] = {0}, dVdPstiffarray[dim] = {0};
      varFcn[(*tag)[i]]->computeConservativeToPrimitiveDerivativeOperators(U[i], V[i], dVdUarray, dVdPstiffarray);
      dVdU.addContrib(i, i, dVdUarray);
      dVdPstiff.addContrib(i, 0, dVdPstiffarray);
    }
  }
  else {
    for(int i = 0; i < U.size(); ++i) {
      double dVdUarray[dim*dim] = {0}, dVdPstiffarray[dim] = {0};
      varFcn[0]->computeConservativeToPrimitiveDerivativeOperators(U[i], V[i], dVdUarray, dVdPstiffarray);
      dVdU.addContrib(i, i, dVdUarray);
      dVdPstiff.addContrib(i, 0, dVdPstiffarray);
    }
  }
}

//------------------------------------------------------------------------------

template<int dim>
void VarFcn::computePrimitiveToConservativeDerivativeOperators(SVec<double, dim>& U, SVec<double, dim>& V,
                                                               RectangularSparseMat<double, dim, dim>& dUdV,
                                                               RectangularSparseMat<double, 1, dim>& dUdPstiff,
                                                               Vec<int> *tag) {
  if(tag) {
    for(int i = 0; i < U.size(); ++i) {
      double dUdVarray[dim*dim] = {0}, dUdPstiffarray[dim] = {0};
      varFcn[(*tag)[i]]->computePrimitiveToConservativeDerivativeOperators(U[i], V[i], dUdVarray, dUdPstiffarray);
      dUdV.addContrib(i, i, dUdVarray);
      dUdPstiff.addContrib(i, 0, dUdPstiffarray);
    }
  }
  else {
    for(int i = 0; i < U.size(); ++i) {
      double dUdVarray[dim*dim] = {0}, dUdPstiffarray[dim] = {0};
      varFcn[0]->computePrimitiveToConservativeDerivativeOperators(U[i], V[i], dUdVarray, dUdPstiffarray);
      dUdV.addContrib(i, i, dUdVarray);
      dUdPstiff.addContrib(i, 0, dUdPstiffarray);
    }
  }
}

//------------------------------------------------------------------------------

inline
void VarFcn::computeDerivativeOfPressureConstant(double dMach, DistVec<double>& dPstiff, DistVec<int> *tag) {
  if(dMach == 0) {
    dPstiff = 0.0;
  }
  else {
    int numLocSub = dPstiff.numLocSub();
    #pragma omp parallel for
    for(int iSub = 0; iSub < numLocSub; ++iSub) {
      Vec<double>& dpstiff = dPstiff(iSub);
      for(int i = 0; i < dpstiff.size(); ++i) {
        dpstiff[i] = varFcn[(tag?(*tag)[iSub][i]:0)]->getDerivativeOfPressureConstant() * dMach;
      }
    }
  }
}

//------------------------------------------------------------------------------

template<int dim>
void VarFcn::conservativeToPrimitiveDerivative(DistMat<RectangularSparseMat<double, dim, dim>>& dVdU,
                                               DistMat<RectangularSparseMat<double, 1, dim>>& dVdPstiff,
                                               DistSVec<double, dim>& dU, DistSVec<double, dim>& dV,
                                               DistVec<double>& dPstiff, DistVec<int> *tag) {
  int numLocSub = dU.numLocSub();
  #pragma omp parallel for
  for(int iSub = 0; iSub < numLocSub; ++iSub) {
    dVdU(iSub).apply(dU(iSub), dV(iSub));
    dVdPstiff(iSub).applyAndAdd(dPstiff(iSub), dV(iSub));
  }
}

//------------------------------------------------------------------------------

template<int dim>
void VarFcn::primitiveToConservativeDerivative(DistMat<RectangularSparseMat<double, dim, dim>>& dUdV,
                                               DistMat<RectangularSparseMat<double, 1, dim>>& dUdPstiff,
                                               DistSVec<double, dim>& dU, DistSVec<double, dim>& dV,
                                               DistVec<double>& dPstiff, DistVec<int> *tag) {
  int numLocSub = dV.numLocSub();
  #pragma omp parallel for
  for(int iSub = 0; iSub < numLocSub; ++iSub) {
    dUdV(iSub).apply(dV(iSub), dU(iSub));
    dUdPstiff(iSub).applyAndAdd(dPstiff(iSub), dU(iSub));
  }
}

//------------------------------------------------------------------------------

template<int dim>
void VarFcn::conservativeToPrimitiveTransposeDerivative(DistMat<RectangularSparseMat<double, dim, dim>>& dVdU,
                                                        DistSVec<double, dim>& dV, DistSVec<double, dim>& dU,
                                                        DistVec<int> *tag) {
  int numLocSub = dU.numLocSub();
  #pragma omp parallel for
  for(int iSub = 0; iSub < numLocSub; ++iSub) {
    dVdU(iSub).applyTranspose(dV(iSub), dU(iSub));
  }
}

//------------------------------------------------------------------------------

template<int dim>
void VarFcn::primitiveToConservativeTransposeDerivative(DistMat<RectangularSparseMat<double, dim, dim>>& dUdV,
                                                        DistSVec<double, dim>& dV, DistSVec<double, dim>& dU,
                                                        DistVec<int> *tag) {
  int numLocSub = dV.numLocSub();
  #pragma omp parallel for
  for(int iSub = 0; iSub < numLocSub; ++iSub) {
    dUdV(iSub).applyTranspose(dU(iSub), dV(iSub));
  }
}

//------------------------------------------------------------------------------

template<int dim>
void VarFcn::primitiveToConservative(SVec<double, dim>& V, SVec<double, dim>& U, Vec<int> *tag) {
  if(tag) {
    for(int i = 0; i < U.size(); ++i) {
      check((*tag)[i]);
      varFcn[(*tag)[i]]->primitiveToConservative(V[i], U[i]);
    }
  }
  else {
    for(int i = 0; i < U.size(); ++i) {
      varFcn[0]->primitiveToConservative(V[i], U[i]);
    }
  }
}

//------------------------------------------------------------------------------

template<int dim>
void VarFcn::primitiveToConservative(DistSVec<double, dim>& V, DistSVec<double, dim>& U, DistVec<int> *tag) {
  int numLocSub = U.numLocSub();
  #pragma omp parallel for
  for(int iSub = 0; iSub < numLocSub; ++iSub) {
    double (*u)[dim] = U.subData(iSub);
    double (*v)[dim] = V.subData(iSub);
    if(tag) {
      int *loctag = tag->subData(iSub);
      for(int i = 0; i < U.subSize(iSub); ++i) {
        check(loctag[i]);
        varFcn[loctag[i]]->primitiveToConservative(v[i], u[i]);
      }
    }
    else {
      for(int i = 0; i < U.subSize(iSub); ++i) {
        varFcn[0]->primitiveToConservative(v[i], u[i]);
      }
    }
  }
}

//------------------------------------------------------------------------------

#endif

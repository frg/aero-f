#include <RestrictionMap.h>

#include <DistVector.h>
#include <Vector.h>
#include <DistMatrix.h>
#include <DenseMatrix.h>
#include <Timer.h>

#include <algorithm>

//------------------------------------------------------------------------------

template<int dim>
void RestrictionMap::restriction(DistSVec<double, dim>& Vin, DistSVec<double, dim>& Vout) {
  double t0 = timer->getTime();
  for(int iSub = 0; iSub < numLocSub; ++iSub) {
    SVec<double, dim>& subVin = Vin(iSub);
    SVec<double, dim>& subVout = Vout(iSub);
    bool *flag = restrictDistInfo.getMasterFlag(iSub);
    if(weighted) {
      for(int i = 0; i < restrictedToOrig[iSub].size(); ++i) {
        if(flag[i]) {
          const int& orig = restrictedToOrig[iSub][i];
          const double& weight = restrictedWeights[iSub][i];
          subVout.row(i) = weight * subVin.row(orig);
        }
        else {
          std::fill(subVout[i], subVout[i] + dim, 0.0);
        }
      }
    }
    else {
      for(int i = 0; i < restrictedToOrig[iSub].size(); ++i) {
        if(flag[i]) {
          const int& orig = restrictedToOrig[iSub][i];
          std::copy(subVin[orig], subVin[orig] + dim, subVout[i]);
        }
        else {
          std::fill(subVout[i], subVout[i] + dim, 0.0);
        }
      }
    }
  }
  timer->addRestrictionTime(t0);
}

//------------------------------------------------------------------------------

template<int dim>
void RestrictionMap::restriction(DistMat<DenseMat<double>>& Vin, DistMat<DenseMat<double>>& Vout) {
  double t0 = timer->getTime();
  for(int iSub = 0; iSub < numLocSub; ++iSub) {
    DenseMat<double>& subVin = Vin(iSub);
    DenseMat<double>& subVout = Vout(iSub);
    const int blk = dim * subVout.numCol();
    bool *flag = restrictDistInfo.getMasterFlag(iSub);
    if(weighted) {
      for(int i = 0; i < restrictedToOrig[iSub].size(); ++i) {
        if(flag[i]) {
          const int& orig = restrictedToOrig[iSub][i];
          const double& weight = restrictedWeights[iSub][i];
          std::transform(subVin[orig * dim], subVin[orig * dim] + blk, subVout[i * dim], [&weight](double& v) {return v*weight; });
        }
        else {
          std::fill(subVout[i * dim], subVout[i * dim] + blk, 0.0);
        }
      }
    }
    else {
      for(int i = 0; i < restrictedToOrig[iSub].size(); ++i) {
        if(flag[i]) {
          const int& orig = restrictedToOrig[iSub][i];
          std::copy(subVin[orig * dim], subVin[orig * dim] + blk, subVout[i * dim]);
        }
        else {
          std::fill(subVout[i * dim], subVout[i * dim] + blk, 0.0);
        }
      }
    }
  }
  timer->addRestrictionTime(t0);
}

//------------------------------------------------------------------------------

template<int dim>
void RestrictionMap::restriction(DistMat<DenseMat<double>>& Vin, DistMat<DenseMat<double>>& Vout, 
                                  std::vector<double>& componentScaling) {
  double t0 = timer->getTime();
  for(int iSub = 0; iSub < numLocSub; ++iSub) {
    DenseMat<double>& subVin = Vin(iSub);
    DenseMat<double>& subVout = Vout(iSub);
    const int blk = dim * subVout.numCol();
    bool *flag = restrictDistInfo.getMasterFlag(iSub);
    if(weighted) {
      for(int i = 0; i < restrictedToOrig[iSub].size(); ++i) {
        if(flag[i]) {
          const int& orig = restrictedToOrig[iSub][i];
          const double& weight = restrictedWeights[iSub][i];
          // std::transform(subVin[orig * dim], subVin[orig * dim] + blk, subVout[i * dim], [&weight](double& v) {return v*weight; });
          for(int j = 0; j < subVout.numCol(); j++) 
            for(int iDim = 0; iDim < dim; ++iDim)
              subVout[i * dim][j + iDim * subVout.numCol()] = componentScaling[iDim] * weight *
                                                               subVin[orig * dim][j + iDim * subVout.numCol()];
        }
        else {
          std::fill(subVout[i * dim], subVout[i * dim] + blk, 0.0);
        }
      }
    }
    else {
      for(int i = 0; i < restrictedToOrig[iSub].size(); ++i) {
        if(flag[i]) {
          const int& orig = restrictedToOrig[iSub][i];
          //std::copy(subVin[orig * dim], subVin[orig * dim] + blk, subVout[i * dim]);
          for(int j = 0; j < subVout.numCol(); j++) 
            for(int iDim = 0; iDim < dim; ++iDim)
              subVout[i * dim][j + iDim * subVout.numCol()] = componentScaling[iDim] * 
                                                               subVin[orig * dim][j + iDim * subVout.numCol()];
        }
        else {
          std::fill(subVout[i * dim], subVout[i * dim] + blk, 0.0);
        }
      }
    }
  }
  timer->addRestrictionTime(t0);
}

//------------------------------------------------------------------------------

#ifndef _FLUID_SELECTOR_H_
#define _FLUID_SELECTOR_H_

#include <DistVector.h>
#include <NodeData.h>

#include <map>

class Domain;
class DistLevelSetStructure;
class IoData;
class ProgrammedBurn;

//--------------------------------------------------------------------------
// This algorithm class allows to determine in which fluid a node is,
// by determining the fluid identification from the different
// available level sets.
//
// The convention is as follows. For n different fluids, only
// n-1 (=dimLS) level-sets are necessary.
// Phi[i] is positive where fluid i+1 (=varFcn[i+1]) is found.
// fluid 0 (=varFcn[0]) is found where all levelsets are negative.
//--------------------------------------------------------------------------

class FluidSelector {

  IoData *iodp;
  int numPhases;
  Domain *domain;
  std::map<int, std::pair<int, int>> ls2phases; // maps each levelset to the pair of fluid Ids for phi<0 and >0.
                                                // used only for multi-phase fluid-structure interactions right now.

  ProgrammedBurn *programmedBurn;

 public:
  DistVec<int> *fluidId;
  DistVec<int> *fluidIdn;
  DistVec<int> *fluidIdnm1;
  DistVec<int> *fluidIdnm2;

 protected:
  void setupFluidIdVolumesInitialConditions(IoData&);
  void setupFluidIdOneDimensionalSolution(IoData&, DistSVec<double, 3>&);
  void setupFluidIdMultiFluidInitialConditions(IoData&, DistSVec<double, 3>&);

 public:

  FluidSelector(int, IoData&, Domain * = 0);

  ~FluidSelector();

  void resize(NodeData *);

  void attachProgrammedBurn(ProgrammedBurn *p) {
    programmedBurn = p;
  }

  ProgrammedBurn *getProgrammedBurn() const {
    return programmedBurn;
  }

  //------------------------------------------------------------------------------

  int getNumOfPhases() {
    return numPhases;
  }
  void getFluidId(DistVec<double>&);
  void getFluidId(int& tag, double phi) {
    tag = (phi < 0.0) ? 0 : 1;
  }
  void getFluidId(int&, double *);
  void getFluidId(Vec<int>&, Vec<double>&);
  void getFluidId(DistVec<int>&, DistVec<double>&);
  int getLevelSetDim(int, int, int = 0, int = 0);
  void update() {
    if(fluidIdnm2) {
      *fluidIdnm2 = *fluidIdnm1;
    }
    if(fluidIdnm1) {
      *fluidIdnm1 = *fluidIdn;
    }
    *fluidIdn = *fluidId;
  }

  //------------------------------------------------------------------------------

  template<int dimLS>
  void initializeFluidIds(DistSVec<double, dimLS>&, DistSVec<double, dimLS>&, DistSVec<double, dimLS>&);

  template<int dimLS>
  void getFluidId(DistVec<int>&, DistSVec<double, dimLS>&, DistVec<int> * = 0);

  template<int dimLS>
  void getFluidId(Vec<int>&, SVec<double, dimLS>&);

  template<int dimLS>
  void getFluidId(DistSVec<double, dimLS>&);

  template<int dimLS>
  void checkLSConsistency(DistSVec<double, dimLS>&);

  void writeToDisk(const char *);
};

//------------------------------------------------------------------------------

#endif

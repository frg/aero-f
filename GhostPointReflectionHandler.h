#ifndef _GHOST_POINT_REFLECTION_HANDLER_H_
#define _GHOST_POINT_REFLECTION_HANDLER_H_

#include <Types.h>
#include <Vector3D.h>

#include <vector>
#include <utility>
#include <map>

class Domain;
class Communicator;
class DistGeoState;
class VarFcn;
class Elem;

template<class Scalar, int dim> class DistSVec;
template<int dim> class DistEmbeddedBcData;
template<class T> class CommPattern;
template<class MatrixType> class DistMat;
template<class Scalar, int Dim> class MvpMat;
template<class Scalar> class Vec;

//------------------------------------------------------------------------------

template<int dim>
class GhostPointReflectionHandler {

 protected:
  bool initialized;
  int numLocSub;
  Domain *domain;
  Communicator *pcom;
  DistEmbeddedBcData<dim> *ebcData;
  DistGeoState *geoState;
  double residual = 0.0;
  int numGhost = 0;
  double eps = 1e-6;
  bool sensitivities_dvel = false;
  double bbMin[3], bbMax[3];
  bool unwettedEmbeddedSurface = false;

  struct send_tuple {
    int iSub;
    Elem *elem;
    Vec3D reflected_point;
    Vec3D dreflected_point;
    Vec3D vs_surface;
    Vec3D dvs_surface;
    int opposite;
    send_tuple(int _iSub, Elem* _elem, Vec3D _reflected_point, Vec3D _vs_surface, int _opposite)
    : iSub(_iSub), elem(_elem), reflected_point(_reflected_point), vs_surface(_vs_surface), opposite(_opposite) {}
  };
  typedef long_int recv_tuple; // in this case the recv_tuple is just info about a node that receives a state

  std::map<int, std::vector<send_tuple>> sendMap;
  std::map<int, std::vector<recv_tuple>> recvMap;
  std::vector<int> sendOffsets, recvOffsets;
  double (*sendBuffer)[dim], (*recvBuffer)[dim];
  // communication for shared ghost nodes
  CommPattern<int> *ghostPat1;
  CommPattern<double> *ghostPat2;
  std::vector<std::vector<std::vector<int>>> sharedNodeSendId;
  std::vector<std::vector<std::vector<int>>> sharedNodeRecvId;

  int count; // total for sending
  int localNodes; // total for receiving

  // needed to be made class members for re-use in sensitivities
  std::vector<int> localPointSides; // the side of ghost node
  std::vector<int> counts, displacements;
  int globNodes;
  std::vector<double_int> points;
  std::vector<std::vector<int>> localIdxMap;

 private:
  void clean();
  void update(DistSVec<double, dim>&, VarFcn *);
  void updateDerivative(DistSVec<double, dim>&, VarFcn *);
  void restrictToGlobalBoundingBox(Vec3D&);

 public:
  GhostPointReflectionHandler(Domain *, DistEmbeddedBcData<dim> *, DistGeoState *);
  ~GhostPointReflectionHandler() { clean(); }

  void initialize(DistSVec<double, dim>&, VarFcn *);
  void populate(DistSVec<double, dim>&, VarFcn *, bool);
  void populateDerivative(DistSVec<double, dim>&, VarFcn *);
  void setSendMapSensitivities();

  template<class Scalar, int neq>
  void populate_dGhost(DistSVec<Scalar, neq>&, DistSVec<Scalar, neq>&, DistSVec<Scalar, neq>&);

  template<class Scalar, int neq>
  void populateGhostJacobian(DistSVec<double, dim>&, VarFcn *, DistMat<MvpMat<Scalar, neq>>&);

  template<class Scalar, int neq>
  void sendToGhostContributions(DistSVec<Scalar, neq>&, DistSVec<Scalar, neq>&, DistMat<MvpMat<Scalar, neq>>&, bool);

  template<class Scalar, int neq>
  void sendToGhostContributionsTranspose(DistSVec<Scalar, neq>&, DistSVec<Scalar, neq>&, DistMat<MvpMat<Scalar, neq>>&, bool);

  void setInitialized(bool _initialized) { initialized = _initialized; }

  void detectEmbeddedStructureOutsideFluidDomain(Vec<Vec3D>&);

  void getBoundingBoxPointers(double *& _bbMin, double *& _bbMax) {_bbMin = bbMin; _bbMax = bbMax;}

};

//------------------------------------------------------------------------------

#endif

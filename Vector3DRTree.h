#ifndef _VECTOR3D_RTREE_H_
#define _VECTOR3D_RTREE_H_

#include <Vector3D.h>

struct Vec3DRTree {

  Vec3D vec;
  int idxTag;

  Vec3DRTree(const Vec3D& v2) {
    vec = v2;
    idxTag = -1;
  }
  Vec3DRTree(const Vec3D& v2, const int idx) {
    vec = v2;
    idxTag = idx;
  }
  ~Vec3DRTree() {}

  template<class PosVecType>
  bool isPointInside(PosVecType& X, const Vec3D& v2, double bary[3], double eps) {
    double bb[6];
    computeBoundingBox(X, bb, eps);
    if((v2.v[0] < bb[0]) || (v2.v[0] > bb[1]) ||
       (v2.v[1] < bb[2]) || (v2.v[1] > bb[3]) ||
       (v2.v[2] < bb[4]) || (v2.v[2] > bb[5])) {
      return false;
    }
    else {
      return true;
    }
  }

  template<class PosVecType>
  void computeBoundingBox(PosVecType& X, double bb[6], double eps) {
    bb[0] = vec.v[0] - eps;
    bb[1] = vec.v[0] + eps;
    bb[2] = vec.v[1] - eps;
    bb[3] = vec.v[1] + eps;
    bb[4] = vec.v[2] - eps;
    bb[5] = vec.v[2] + eps;
  }

};

#endif
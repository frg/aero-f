#ifndef __REINITIALIZE_GAP_H__
#define __REINITIALIZE_GAP_H__

class DistLevelSetStructure;
class DistGeoState;
class Domain;
class IoData;
class ReinitializeDistanceToWall;

#include <DistVector.h>
#include <vector>

//------------------------------------------------------------------------------

class ReinitializeGap {
  IoData& iod;
  Domain& dom;
  std::vector<ReinitializeDistanceToWall*> wall_computers;
  DistVec<double> gap;

 public:
  ReinitializeGap(IoData&, Domain&, const std::vector<int>&);
  ~ReinitializeGap();

  void resize(NodeData *);

  void computeGapFunction(DistLevelSetStructure&, DistGeoState&, const double, bool);

  DistVec<double> *getGap() {
    return &gap;
  }

};

//------------------------------------------------------------------------------

#endif

#ifndef _POST_OPERATOR_H_
#define _POST_OPERATOR_H_

#include <MovingLeastSquaresForceHandler.h>
#include <PostFcn.h>
#include <Types.h>

#include <map>
#include <vector>

class IoData;
class VarFcn;
class SubDomain;
class Domain;
class DistGeoState;
class Communicator;
class FemEquationTermLES;

template<int dim> class DistBcData;
template<class Scalar> class DistVec;
template<class Scalar, int dim> class DistSVec;
template<int dim> class SpaceOperator;
template<int dim> class DistTimeState;
template<int dim> class DistLESData;
template<int dim> class DistEmbeddedBcData;
template<int dim> class MatVecProd_dRdX;
template<class Scalar> class Vec;

struct Vec3D;

#ifndef _VECSET_TMPL_
  #define _VECSET_TMPL_
  template<class VecType, class Scalar = double> class VecSet;
#endif

//------------------------------------------------------------------------------

template<int dim>
class ForceGenerator {
 public:
  virtual void getForcesAndMoments(std::map<int, int>&, DistSVec<double, dim>&,
                                   Vec3D *, Vec3D *, Vec3D&, int) = 0;
  virtual void getDerivativeOfForcesAndMoments(std::map<int, int>&, DistSVec<double, dim>&, DistSVec<double, dim> *,
                                               Vec3D *, Vec3D *, Vec3D&, double, int) = 0;
};

//------------------------------------------------------------------------------

template<int dim>
class PostOperator {

  IoData& ioData;
  VarFcn *varFcn;
  DistBcData<dim> *bcData;
  DistEmbeddedBcData<dim> *ebcData;
  DistGeoState *geoState;
  NavierStokesTerm *ns;

  int numLocSub;
  SubDomain **subDomain;
  Domain *domain;
  Communicator *com;
  int numSurf;
  int numSurfHF;
  int numSurfBQ;
  std::map<int, int> surfOutMap;
  std::map<int, int> surfOutMapHF;
  std::map<int, int> surfOutMapBQ;
  ForceGenerator<dim> *forceGen;

  double nodalForceWeights[2];  // Coefficients to compute nodal force transfer
  double refLengthSq;
  double pressInfty;

  DistLESData<dim> *les, *les2;
  FemEquationTermLES *fet;
  SATerm *sa;

  SpaceOperator<dim> *spaceOp;
  PostFcn *postFcn;

  DistSVec<double, dim> *V, *dV;
  DistSVec<double, dim> *tmpV;
  DistVec<double> *tmp;
  DistVec<double> *tmp1;
  DistSVec<double, 2> *tmp2;
  DistSVec<double, 3> *tmp3;

  DistVec<int> *fluidId;

 public:
  PostOperator(IoData&, VarFcn *, DistBcData<dim> *, DistEmbeddedBcData<dim> *, DistGeoState *, Domain *,
               SpaceOperator<dim> *, DistSVec<double, dim> *);
  ~PostOperator();

  void rstVar(IoData&);

  void fsoInitialize();

  void resize();

  void computeNodalForce(DistSVec<double, dim>&, double, DistSVec<double, 3>&, DistSVec<double, 3>&);

  void computeDerivativeOfNodalForce(DistSVec<double, dim>&, DistSVec<double, dim>&, double, bool,
                                     DistSVec<double, 3>&, DistSVec<double, 3>&, DistSVec<double, 3>&);

  void computeDerivativeOfNodalForce(MatVecProd_dRdX<dim>&, DistSVec<double, 3>&, DistSVec<double, dim>&,
                                     bool, DistSVec<double, 3>&);

  void computeTransposeDerivativeOfNodalForce(MatVecProd_dRdX<dim>&, DistSVec<double, 3>&, DistSVec<double, 3>&,
                                              DistSVec<double, 3>&, DistSVec<double, dim>&);

  void computeDerivativeOperatorsOfNodalForce(DistSVec<double, dim>&, double, DistSVec<double, 3>&,
                                              MatVecProd_dRdX<dim>&);

  void computeNodalHeatPower(DistSVec<double, dim>&, DistVec<double>&);

  void computeBulkQuantities(DistSVec<double, dim>&, double *, double *);

  void computeForceAndMoment(Vec3D&, DistSVec<double, dim>&, Vec3D *, Vec3D *, Vec3D *, Vec3D *, DistSVec<double, 3>&,
                             int = 0);

  void computeDerivativeOfForceAndMoment(Vec3D&, DistSVec<double, dim>&, DistSVec<double, dim> *, bool,
                                         Vec3D *, Vec3D *, Vec3D *, Vec3D *, DistSVec<double, 3>&, DistSVec<double, 3>&, double, int = 0);

  void computeDerivativeOfForceAndMoment(MatVecProd_dRdX<dim>&, DistSVec<double, dim>&, bool, DistSVec<double, 3>&,
                                         Vec3D *, Vec3D *, Vec3D *, Vec3D *, int = 0);

  void computeTransposeDerivativeOfForceAndMoment(MatVecProd_dRdX<dim>&, SVec<double, 3>&, SVec<double, 3>&,
                                                  SVec<double, 3>&, SVec<double, 3>&, DistSVec<double, 3>&,
                                                  DistSVec<double, dim>&, SVec<double, 3>&, DistSVec<double, 3>&,
                                                  int = 0);

  void computeDerivativeOperatorsOfForceAndMoment(MatVecProd_dRdX<dim>&, Vec3D&, DistSVec<double, dim>&,
                                                  DistSVec<double, 3>&, int = 0);

  void computeHeatFluxes(DistSVec<double, dim>&, double *);

  void computeSolution(PostFcn::SolutionType, DistSVec<double, dim>&, DistSVec<double, dim>&);

  void computeScalarQuantity(PostFcn::ScalarType, DistSVec<double, 3>&, DistSVec<double, dim>&, DistVec<double>&,
                             DistTimeState<dim> *, DistVec<double> *, DistSVec<double, dimLS> *, double[3]);

  void computeScalarQuantity(PostFcn::ScalarType, DistSVec<double, dim>&, DistTimeState<dim> *, int *,
                             int *, int *, int, double *, std::vector<Vec3D>&, double[3]);

  void computeScalarQuantityWithAverages(PostFcn::ScalarType, DistVec<double>&, DistVec<double>&, DistVec<double>&, double, double);

  void computeScalarQuantityWithFavreAverages(PostFcn::ScalarType, DistVec<double>&, DistVec<double>&, DistVec<double>&,
                                              DistVec<double>&, DistVec<double>&, double, double);

  void computeKTurbWithFavreAverages(DistSVec<double, 6>&, DistSVec<double, 3>&, DistSVec<double, 6>&, DistSVec<double, 3>&, 
                                     DistVec<double>&, DistVec<double>&, double, double);

  void computeVectorQuantity(PostFcn::VectorType, DistSVec<double, 3>&, DistSVec<double, dim>&, DistSVec<double, 3>&);

  void computeVectorQuantity(PostFcn::VectorType, DistSVec<double, dim>&, DistTimeState<dim> *, int *,
                             int *, int *, int, double *, std::vector<Vec3D>&);

  void computeTensorQuantity(PostFcn::TensorType, DistSVec<double, 3>&, DistSVec<double, dim>&, DistSVec<double, 6>&);

  void computeTensorQuantityWithAverages(DistSVec<double, 6>&, DistSVec<double, 3>&, DistSVec<double, 3>&,
                                         DistSVec<double, 3>&, double, double);

  void computeTensorQuantityWithFavreAverages(DistSVec<double, 6>&, DistSVec<double, 3>&, DistVec<double>&, DistSVec<double, 3>&,
                                         DistSVec<double, 3>&, double, double);

  void splitTensorQuantity(DistSVec<double, 6>&, DistSVec<double, 3>&, DistSVec<double, 3>&);

  void computeForceDerivs(DistSVec<double, 3>&, DistSVec<double, dim>&, DistSVec<double, dim>&, Vec<double>&,
                          VecSet<DistSVec<double, 3>>&);

  template<class TRIPLET>
  void computeForceDerivMat(DistSVec<double, 3>&, DistSVec<double, dim>&,
                            VecSet<DistSVec<double, 3>>&, std::vector<std::vector<TRIPLET>>&);

  void computeTotalPressureDistortion(DistSVec<double, dim>& U, double *Q);

  void computeEmbScalarQuantity(DistSVec<double, dim>&, double **, DistTimeState<dim> *, WallFcn *, std::vector<PostFcn::ScalarType>&);

  void computeInletDiskScalarQuantity(DistSVec<double, dim>&, double **, DistTimeState<dim> *, std::vector<PostFcn::ScalarType>&, int);

  void computeDerivativeOfScalarQuantity(PostFcn::ScalarDerivativeType, bool, DistSVec<double, 3>&, DistSVec<double, 3>&, DistSVec<double, dim>&,
                                         DistSVec<double, dim>&, DistVec<double>&, DistTimeState<dim> *);

  void computeDerivativeOfVectorQuantity(PostFcn::VectorDerivativeType, bool, DistSVec<double, 3>&, DistSVec<double, 3>&, DistSVec<double, dim>&,
                                         DistSVec<double, dim>&, DistSVec<double, 3>&);

  void computeHessianOfScalarQuantity(PostFcn::ScalarHessianType, DistSVec<double, 3>&, DistSVec<double, dim>&, DistSVec<double, 9>&);

  int getNumSurf() {
    return numSurf;
  }

  std::map<int, int>& getSurfMap() {
    return surfOutMap;
  }

  int getNumSurfHF() {
    return numSurfHF;
  }

  int getNumSurfBQ() {
    return numSurfBQ;
  }

  std::map<int, int>& getSurfMapBQ() {
    return surfOutMapBQ;
  }

  std::map<int, int>& getSurfMapHF() {
    return surfOutMapHF;
  }

  PostFcn *getPostFcn() {
    return postFcn;
  }

  void setForceGenerator(ForceGenerator<dim> *fg) {
    forceGen = fg;
  }

  void computeNodalForceLinearized(MatVecProd_dRdX<dim>&, DistSVec<double, dim>&, DistSVec<double, 3>&, DistSVec<double, 6>&,
                                   DistSVec<double, 3>&, DistSVec<double, 3>&);
  void computeModalForce(MatVecProd_dRdX<dim>&, DistSVec<double, dim>&, DistSVec<double, 3>&, VecSet<DistSVec<double, 3>>&, Vec<double>&);

};

//------------------------------------------------------------------------------

#endif

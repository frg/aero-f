#ifndef _NONLINEAR_ROM_A_H_
#define _NONLINEAR_ROM_A_H_

#include <NonlinearRom.h>

class IoData;
class Domain;
class Communicator;

template<int dim> class Supermesh;

#include <VarFcn.h>
#include <DistVector.h>
#include <VectorSetAdaptive.h>
#include <Vector.h>
#include <Eigen/Core>
#include <Eigen/Geometry>
#include <vector>

//--------------------------- HPMOR-AMR CLASS ------------------------------

template<int dim>
class NonlinearRomAdaptive : public NonlinearRom {

  IoData& ioData;
  Domain *domain;
  Communicator *com;

  VarFcn *varFcn;

  int numSnaps;
  std::vector<VecSet<DistSVec<double,dim>> *> adaptivesnapshots;
  std::vector<VecSet<Vec<Vec3D>> *> surfacesnapshots;
  std::vector<VecSet<Vec<Vec3D>> *> surfacesnapshotsvel;
  VecSet<Vec<double>> *snapshotsCoord;
  Eigen::MatrixXd innerProducts;

  std::vector<std::string> snapFiles;
  std::vector<std::tuple<int, int, int, double>> snapData;
  std::vector<int> snapToMesh;
  std::vector<int> snapToFile;
  std::vector<int> snapToIndex;
  std::vector<int> snapToCount;
  std::vector<double> snapToTag;
  std::vector<std::string> surfaceTopFiles;

  char *innerProductsName;

  Supermesh<dim> *supermesh;

  std::vector<Domain *> domains; // pointers to adaptive domains

  Domain *domainSubtractState;
  DistSVec<double,dim> *subtractState;

public:

  bool computeEmbedded;
  bool computeALS;

  // constructors
  NonlinearRomAdaptive(IoData&, Domain *, VarFcn *);

  ~NonlinearRomAdaptive();

  // methods

  int countSnapshots() {
    int result = 0;
    for(auto p : adaptivesnapshots) {
      result += p->numVectors();
    }
    return result;
  }

  std::vector<Domain*>& getDomains() {
    return domains;
  }

  std::vector<VecSet<DistSVec<double,dim>> *>& getAdaptiveSnapshots() {
    return adaptivesnapshots;
  }  

  std::vector<VecSet<Vec<Vec3D>> *>& getSurfaceSnapshots() {
    return surfacesnapshots;
  }

  std::vector<VecSet<Vec<Vec3D>> *>& getSurfaceSnapshotsVel() {
    return surfacesnapshotsvel;
  }

  VecSet<Vec<double>> *getSnapshotsCoord() {
    return snapshotsCoord;
  }

  Eigen::MatrixXd *getInnerProducts() {
    return &innerProducts;
  }

  std::vector<int>& getSnapToMesh() {
    return snapToMesh;
  }

  std::vector<std::string>& getSurfaceTopFiles() {
    return surfaceTopFiles;
  }

  void changeGeometryPrefix(const char *);

  void computeUnbalanced();

  void computeSubtractState();

  void initialInnerProduct();

  void submeshInnerProduct();

  void interpolateSupermesh(Clustering<Vec<double>> *);

  void readSnapshots(const char *, std::vector<VecSet<DistSVec<double,dim>> *>&,
                     std::vector<std::pair<std::string, int>> * = nullptr);

  void createSnapshotsCoord(VecSet<Vec<double>>&);

  void writeMatrix(char *, Eigen::MatrixXd&);
  void readMatrix(char *, Eigen::MatrixXd&);
  Eigen::MatrixXd *convertMatrix(const VecSet<Vec<double>>&);
  VecSet<Vec<double>> *convertMatrix(const Eigen::MatrixXd&);

  void computeInnerProducts();
  void shiftInnerProducts(int = 0);
  void convertRecomputedInnerProducts(int, Clustering<Vec<double>> *);
  void writeInnerProducts() {
    writeMatrix(innerProductsName, innerProducts);
  }
  void readInnerProducts() {
    readMatrix(innerProductsName, innerProducts);
    numSnaps = innerProducts.cols();
  }

  void writeClusteredSnapshots(int, VecSet<Vec<double>>&, int **, int *,
                               std::vector<std::pair<std::string, int>>&);
  void readClusteredSnapshots(int, int *, VecSet<Vec<double>>&);
  void writeClusterCenters(int, int *, VecSet<Vec<double>> *);
  void readClusterCenters(int, int *&, VecSet<Vec<double>> *&);

  void referenceSnapshots(int, int, VecSet<Vec<double>> *, VecSet<Vec<double>>&, Vec<double>&);

  void writeBasis(int, const char *, Eigen::MatrixXd&, std::vector<double>&, int);
  void readBasis(int, const char *, Eigen::MatrixXd&, bool = false);
  void readBasis(int, const char *, VecSet<Vec<double>>&, bool = false);

  void readCachedOfflineQuantities(Clustering<Vec<double>> *);

  void readReferenceState(int, Vec<double>&);
};

//------------------------------------------------------------------------------

#endif

#ifndef _CONTROL_FOCUSED_TRAINING_H_
#define _CONTROL_FOCUSED_TRAINING_H_

#include <DistVector.h>
#include <VectorSet.h>
#include <Eigen/Core>
#include <complex>

class IoData;
class Domain;
class Communicator;

template<int dim> class PostOperator;
template<int dim> class MatVecProd_dRdX;
template<int dim> class ModalSolver;
template<class Scalar> class DenseMat;

#ifndef _MATVECPROD_TMPL_
#define _MATVECPROD_TMPL_
template<int dim, int neq, class Scalar2 = double> class MatVecProd;
#endif

#ifndef _MATVECPROD2_TMPL_
#define _MATVECPROD2_TMPL_
template<int dim, int neq, class Scalar2 = double> class MatVecProdFD;
template<int dim, class Scalar, int neq, class Scalar2 = double> class MatVecProdH1;
template<int dim, class Scalar, int neq, class Scalar2 = double> class MatVecProdH2;
template<int dim, class Scalar, class Scalar2 = double> class MatVecProdLS;
#endif

//------------------------------------------------------------------------------

template<int dim>
class CtrlFocusedTrainingMatVecProd {
 private:
  IoData *ioData;
  Communicator *com;
  PostOperator<dim> *postOp;
  Domain& domain;

  MatVecProdH2<dim, std::complex<double>, dim, std::complex<double>> *HOpC;
  VecSet<DistSVec<double, dim>>& DX;
  VecSet<DistSVec<double, dim>>& DE;
  DenseMat<double>& mass;
  DenseMat<double>& stiffness;
  DenseMat<std::complex<double>> structOp;
  MatVecProd_dRdX<dim> *dRdXop;
  DistSVec<double, 3> *dX;
  VecSet<DistSVec<double, 3>>& mX;
  std::complex<double> kImag;
  int ns;
  double ootref;
  double threshold;

  void computeModalForce(DistSVec<double, dim>&, Vec<double>&);
  void setStructOp();

 public:
  CtrlFocusedTrainingMatVecProd(ModalSolver<dim> *);
  void apply(DistSVec<std::complex<double>, dim>&, DistSVec<std::complex<double>, dim>&);
  void applyTranspose(DistSVec<std::complex<double>, dim>&, DistSVec<std::complex<double>, dim>&);
  void evaluate(DistVec<double>&, DistSVec<double, dim>&, DistSVec<double, dim>&, std::complex<double>, double);
};

//------------------------------------------------------------------------------

#endif

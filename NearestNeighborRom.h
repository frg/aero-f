#ifndef _NEARESTNEIGHBOR_ROM_H_
#define _NEARESTNEIGHBOR_ROM_H_

#include <VectorSet.h>
#include <DistMatrix.h>
#include <Eigen/SVD>

class Communicator;
class IoData;
class Domain;
class NonlinearRom;
class Timer;

template<class Scalar, int dim> class DistSVec;
template<class Scalar> class Vec;
template<class Scalar> class DenseMat;

typedef Eigen::Matrix<double,Eigen::Dynamic,Eigen::Dynamic,Eigen::RowMajor> MatrixType;
typedef Eigen::Matrix<double,Eigen::Dynamic,1,Eigen::ColMajor> VectorType;

//------------------------------------------------------------------------------

template<int dim>
class NearestNeighborRom {

 private:
  Domain *domain;
  IoData& ioData;
  NonlinearRom *nlrom;
  Communicator *com;
  Timer *timer;
  VecSet<DistSVec<double, dim>> *ROBv0;
  DistMat<DenseMat<double>> *ROB;
  std::vector<int> *snapFileNums;   // which snapshot file each snapshot came from

  bool orthog;
  bool fastDistanceComparisons;
  bool preprocess;
  int numNeighbors;
  int numSnapFiles;

  std::vector<int> neighborInds;
  std::vector<int> neighborIndsPrev; // for projecting from old basis to new basis

  VecSet<DistSVec<double, dim>> *snapshots;
  DistSVec<double, dim> Uref;
  DenseMat<double> STS; // snapshot inner products, for fast distances/projections
  DenseMat<double> VTV; // basis inner products, for non-orthogonal projections
  DenseMat<double> VTVprev;
  Eigen::JacobiSVD<MatrixType> VTV_SVD;

  void distsToSnapshots(const Vec<double>&, const DistSVec<double, dim>&, std::vector<double>&);
  void nearestInds(const Vec<double>&, const DistSVec<double, dim>&, std::vector<int>&);
  void includeNeighborsFromAllFiles(std::vector<int>&);
  void fastNearestNeighborDistances(const Vec<double>&, std::vector<double>&);
  void fullNearestNeighborDistances(const DistSVec<double, dim>&, std::vector<double>&);
  void fastNearestNeighborVTV();
  bool useFastFormulas();

 public:
  NearestNeighborRom(Domain*, IoData&, NonlinearRom*, int);
  ~NearestNeighborRom();

  // getters, setters, IO
  DistSVec<double, dim>& getUref() { return Uref; }
  void setROB(VecSet<DistSVec<double, dim>> *_ROB) { ROBv0 = _ROB; }
  void setROB(DistMat<DenseMat<double>> *_ROB) { ROB = _ROB; }
  VecSet<DistSVec<double, dim>> *getSnapshots() { return snapshots; }
  std::vector<int>& getNeighborInds() { return neighborInds; }
  void setIC(Vec<double>&);
  void writeSTS();
  void writeICInfo(const Vec<double>&);
  int getNumNeighbors() { return numNeighbors; }

  void loadBasis(const Vec<int>&);
  void loadBasis(const std::vector<int>&);
  void updateNearestNeighborROB(const Vec<double>&, const DistSVec<double, dim>&);
  void SVDProject(const DistSVec<double, dim>&, double *);
  void SVDProjectFast(Vec<double>&);
  void projectOntoBasis(Vec<double>&, const DistSVec<double, dim>&);
  void resize(int);
};

//------------------------------------------------------------------------------

#endif

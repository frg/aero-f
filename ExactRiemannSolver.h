#ifndef _EXACT_RIEMANN_SOLVER_H
#define _EXACT_RIEMANN_SOLVER_H

class HigherOrderMultiFluid;
class IoData;
class LocalRiemann;
class SparseGridCluster;
class VarFcn;

template<class Scalar> class Vec;
template<class Scalar, int dim> class SVec;

//------------------------------------------------------------------------------

template<int dim>
class ExactRiemannSolver {

  VarFcn *vf;

  int numLriemann;
  LocalRiemann **lriemann;
  LocalRiemann *fsiRiemann;
  LocalRiemann *symmetryPlaneRiemann;

  int iteration;
  SVec<double, dim>& rupdate;
  Vec<double>& weight;
  Vec<int>& fluidIdToSet;

  int getRiemannSolverId(int i, int j) const;

  int levelSetMap[10][10];
  double levelSetSign[10][10];

  bool isHigherOrder;

 public:
  ExactRiemannSolver(IoData&, SVec<double, dim>&, Vec<double>&,
                     VarFcn *, SparseGridCluster *, Vec<int>&);
  ~ExactRiemannSolver();

  SVec<double, dim>& getRiemannUpdate() const {
    return rupdate;
  }
  Vec<double>& getRiemannWeight() const {
    return weight;
  }

  // for multiphase Riemann problem
  int fluid1(int, int);
  int fluid2(int, int);
  int computeRiemannSolution(double *, double *, int, int, double *, VarFcn *,
                             double *, double *, int, int, double[3], int, bool);
  void computeRiemannJacobian(double *, double *, int, int, double *, VarFcn *,
                              double *, double *, int, int, double[3], double *,
                              double *, double *, double *);

  // for structure-fluid "half-Riemann" problem
  int computeFSIRiemannSolution(double *, double *, double *, double *, int = 0, bool = false);
  void computeFSIRiemannJacobian(double *, double *, double *, double *, double *, int = 0);
  void computeFSIRiemannDerivative(double *, double *, double *, double *, double *, double *, int = 0);

  // for symmetry plane Riemann problem
  int computeSymmetryPlaneRiemannSolution(double *, double *, double *, double *, int = 0);
  void computeSymmetryRiemannJacobian(double *, double *, double *, double *, double *, int = 0);
};

//------------------------------------------------------------------------------

#endif

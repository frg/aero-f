#ifndef _IMPLICIT_HYPER_ROM_TS_DESC_H_
#define _IMPLICIT_HYPER_ROM_TS_DESC_H_

#include <ImplicitRomTsDesc.h>
#include <VectorSet.h>
#include <DistMatrix.h>

class RestrictionMap;

template<class Scalar, int dim> class DistSVec;
template<class Scalar> class Vec;
template<class Scalar> class DenseMat;

//------------------------------------------------------------------------------

template<int dim>
class ImplicitHyperRomTsDesc : public ImplicitRomTsDesc<dim> {

protected:
  RestrictionMap *map;
  DistMat<DenseMat<double>> AJtmp, ROBtmp;
  VecSet<DistSVec<double, dim>> *ROBv0tmp;

protected:
  void computeFunction(DistSVec<double, dim>&, DistSVec<double, dim>&, bool);
  virtual double meritFunction(DistSVec<double, dim>&);
  virtual void solveLinearSystem(int, DistSVec<double, dim>&, DistSVec<double, dim>&);
  virtual double computeResidualNorm(DistSVec<double, dim>&);
  virtual void loadCluster(int, bool);

public:
  ImplicitHyperRomTsDesc(IoData&, GeoSource&, Domain *, bool, bool, bool);
  virtual ~ImplicitHyperRomTsDesc();

};

//------------------------------------------------------------------------------

template<int dim>
class ImplicitCollocRomTsDesc : public ImplicitHyperRomTsDesc<dim> {

public:
  ImplicitCollocRomTsDesc(IoData&, GeoSource&, Domain *);
  ~ImplicitCollocRomTsDesc() {}

};

//------------------------------------------------------------------------------

template<int dim>
class ImplicitSWRomTsDesc : public ImplicitHyperRomTsDesc<dim> {

public:
  ImplicitSWRomTsDesc(IoData&, GeoSource&, Domain *);
  ~ImplicitSWRomTsDesc() {}

};

//------------------------------------------------------------------------------

template<int dim>
class ImplicitGnatRomTsDesc : public ImplicitHyperRomTsDesc<dim> {

  int nPodJac;

  DistMat<DenseMat<double>> gnatMat;         // offline GNAT pseudoinverse
  VecSet<DistSVec<double, dim>> *gnatMatv0;

  DenseMat<double> onlineJacMat;             // stores gnatMat^T * AJ
  Vec<double> onlineResMat;                  // stores gnatMat^T * F

  // global linear solver
  ParallelRom<1> *globParallelRom;

protected:
  double meritFunction(DistSVec<double, dim>&);
  void solveLinearSystem(int, DistSVec<double, dim>&, DistSVec<double, dim>&);
  double computeResidualNorm(DistSVec<double, dim>&);
  void loadCluster(int, bool);

public:
  ImplicitGnatRomTsDesc(IoData&, GeoSource&, Domain *);
  ~ImplicitGnatRomTsDesc();

};

//------------------------------------------------------------------------------

#endif

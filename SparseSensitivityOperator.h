#ifndef _SPARSE_SENSITIVITY_OPERATOR_H_
#define _SPARSE_SENSITIVITY_OPERATOR_H_

#include <DistMatrix.h>
#include <Face.h>

class Domain;
class SubDomain;

struct Vec3D;

template<class Scalar, int dim> class DistSVec;
template<int dim> class PostOperator;
template<int dim> class SpaceOperator;
template<class Scalar, int dim, int dim2> class RectangularSparseMat;

//------------------------------------------------------------------------------

template<int dim>
struct dRdXoperators {
  RectangularSparseMat<double, 3, 6> *dRdX;
  RectangularSparseMat<double, 6, 6> *dRdR;
  RectangularSparseMat<double, 3, dim> *dddxdX;
  RectangularSparseMat<double, 3, dim> *dddydX;
  RectangularSparseMat<double, 3, dim> *dddzdX;
  RectangularSparseMat<double, dim, dim> *dddxdV;
  RectangularSparseMat<double, dim, dim> *dddydV;
  RectangularSparseMat<double, dim, dim> *dddzdV;
  RectangularSparseMat<double, 6, dim> *dddxdR;
  RectangularSparseMat<double, 6, dim> *dddydR;
  RectangularSparseMat<double, 6, dim> *dddzdR;
  RectangularSparseMat<double, dim, 3> *dGradPdddx;
  RectangularSparseMat<double, dim, 3> *dGradPdddy;
  RectangularSparseMat<double, dim, 3> *dGradPdddz;
  RectangularSparseMat<double, 3, 3> *dForcedGradP;
  RectangularSparseMat<double, 3, 3> *dForcedX;
  RectangularSparseMat<double, dim, 3> *dForcedV;
  RectangularSparseMat<double, 3, 3> *dForcedS;
  RectangularSparseMat<double, dim, dim> *dVdU;
  RectangularSparseMat<double, 1, dim> *dVdPstiff;
  RectangularSparseMat<double, 3, 3> *dFaceNormdX;
  RectangularSparseMat<double, 3, 3> *dEdgeNormdX;
  RectangularSparseMat<double, 3, 1> *dCtrlVoldX;
  RectangularSparseMat<double, 3, 3> *dMidX;
  RectangularSparseMat<double, 3, 3> *dMvdX;
  RectangularSparseMat<double, dim, 3> *dMidV;
  RectangularSparseMat<double, dim, 3> *dMvdV;
  RectangularSparseMat<double, 3, 3> *dMidS;
  RectangularSparseMat<double, 3, 3> *dMidGradP;
  RectangularSparseMat<double, 3, 3> *dFidX;
  RectangularSparseMat<double, 3, 3> *dFvdX;
  RectangularSparseMat<double, dim, 3> *dFidV;
  RectangularSparseMat<double, dim, 3> *dFvdV;
  RectangularSparseMat<double, 3, 3> *dFidS;
  RectangularSparseMat<double, 3, 3> *dFidGradP;
  RectangularSparseMat<double, dim, dim> *dFluxdddx;
  RectangularSparseMat<double, dim, dim> *dFluxdddy;
  RectangularSparseMat<double, dim, dim> *dFluxdddz;
  RectangularSparseMat<double, 3, dim> *dFluxdEdgeNorm;
  RectangularSparseMat<double, 1, dim> *dFluxdPstiff;
  RectangularSparseMat<double, 3, dim> *dFluxdFaceNormal;
  RectangularSparseMat<double, 1, dim> *dFluxdFaceNormalVel;
  RectangularSparseMat<double, Face::MaxNumNd * dim, dim> *dFluxdUb;
  RectangularSparseMat<double, 3, dim> *dFluxdX;
  RectangularSparseMat<double, dim, dim> *dFluxdV;
  RectangularSparseMat<double, 3, dim> *dViscousFluxdX;
  RectangularSparseMat<double, 3, 3> *dFvdS;
  RectangularSparseMat<double, 3, 3> *dMvdS;
  RectangularSparseMat<double, 3, dim> *dViscousFluxdS;
  RectangularSparseMat<double, 3, dim> *dViscousFluxdFaceNormal;
  RectangularSparseMat<double, 3, Face::MaxNumNd * dim> *dVwalldS;
  RectangularSparseMat<double, 3, Face::MaxNumNd * dim> *dVwalldFaceNormal;
  RectangularSparseMat<double, 3, dim-5+1> *dUnodedFaceNormal;
  RectangularSparseMat<double, Face::MaxNumNd * dim, dim-5+1> *dUnodedUface;
  RectangularSparseMat<double, dim-5+1, dim> *dUnodedTmp;
  RectangularSparseMat<double, dim, dim> *dUdV;
  RectangularSparseMat<double, 1, dim> *dUdPstiff;
  RectangularSparseMat<double, dim, 1> *dPressureDistortiondV;
};

//------------------------------------------------------------------------------

template<int dim>
class MatVecProd_dRdX {

  int numLocSub;
  SubDomain **subDomain;
  SpaceOperator<dim> *spaceOp;

  dRdXoperators<dim> **dRdXop;

 public:
  DistMat<RectangularSparseMat<double, 3, 6>> dRdX;
  DistMat<RectangularSparseMat<double, 6, 6>> dRdR;
  DistMat<RectangularSparseMat<double, 3, dim>> dddxdX;
  DistMat<RectangularSparseMat<double, 3, dim>> dddydX;
  DistMat<RectangularSparseMat<double, 3, dim>> dddzdX;
  DistMat<RectangularSparseMat<double, dim, dim>> dddxdV;
  DistMat<RectangularSparseMat<double, dim, dim>> dddydV;
  DistMat<RectangularSparseMat<double, dim, dim>> dddzdV;
  DistMat<RectangularSparseMat<double, 6, dim>> dddxdR;
  DistMat<RectangularSparseMat<double, 6, dim>> dddydR;
  DistMat<RectangularSparseMat<double, 6, dim>> dddzdR;
  DistMat<RectangularSparseMat<double, dim, 3>> dGradPdddx;
  DistMat<RectangularSparseMat<double, dim, 3>> dGradPdddy;
  DistMat<RectangularSparseMat<double, dim, 3>> dGradPdddz;
  DistMat<RectangularSparseMat<double, 3, 3>> dForcedGradP;
  DistMat<RectangularSparseMat<double, 3, 3>> dForcedX;
  DistMat<RectangularSparseMat<double, dim, 3>> dForcedV;
  DistMat<RectangularSparseMat<double, 3, 3>> dForcedS;
  DistMat<RectangularSparseMat<double, dim, dim>> dVdU;
  DistMat<RectangularSparseMat<double, 1, dim>> dVdPstiff;
  DistMat<RectangularSparseMat<double, 3, 3>> dFaceNormdX;
  DistMat<RectangularSparseMat<double, 3, 3>> dEdgeNormdX;
  DistMat<RectangularSparseMat<double, 3, 1>> dCtrlVoldX;
  DistMat<RectangularSparseMat<double, 3, 3>> dMidX;
  DistMat<RectangularSparseMat<double, 3, 3>> dMvdX;
  DistMat<RectangularSparseMat<double, dim, 3>> dMidV;
  DistMat<RectangularSparseMat<double, dim, 3>> dMvdV;
  DistMat<RectangularSparseMat<double, 3, 3>> dMidS; // dS implies a derivative directly in terms of the design variable
                                                     // (mach, alpha, beta) sensitivity (may not be in that order)
  DistMat<RectangularSparseMat<double, 3, 3>> dMidGradP;
  DistMat<RectangularSparseMat<double, 3, 3>> dFidX;
  DistMat<RectangularSparseMat<double, 3, 3>> dFvdX;
  DistMat<RectangularSparseMat<double, dim, 3>> dFidV;
  DistMat<RectangularSparseMat<double, dim, 3>> dFvdV;
  DistMat<RectangularSparseMat<double, 3, 3>> dFidS;
  DistMat<RectangularSparseMat<double, 3, 3>> dFidGradP;
  DistMat<RectangularSparseMat<double, dim, dim>> dFluxdddx;
  DistMat<RectangularSparseMat<double, dim, dim>> dFluxdddy;
  DistMat<RectangularSparseMat<double, dim, dim>> dFluxdddz;
  DistMat<RectangularSparseMat<double, 3, dim>> dFluxdEdgeNorm;
  DistMat<RectangularSparseMat<double, 1, dim>> dFluxdPstiff;
  DistMat<RectangularSparseMat<double, 3, dim>> dFluxdFaceNormal;
  DistMat<RectangularSparseMat<double, 1, dim>> dFluxdFaceNormalVel;
  DistMat<RectangularSparseMat<double, Face::MaxNumNd * dim, dim>> dFluxdUb;
  DistMat<RectangularSparseMat<double, 3, dim>> dFluxdX;
  DistMat<RectangularSparseMat<double, dim, dim>> dFluxdV;
  DistMat<RectangularSparseMat<double, 3, dim>> dViscousFluxdX;
  DistMat<RectangularSparseMat<double, 3, 3>> dFvdS;
  DistMat<RectangularSparseMat<double, 3, 3>> dMvdS;
  DistMat<RectangularSparseMat<double, 3, dim>> dViscousFluxdS;
  DistMat<RectangularSparseMat<double, 3, dim>> dViscousFluxdFaceNormal;
  DistMat<RectangularSparseMat<double, 3, Face::MaxNumNd * dim>> dVwalldS;
  DistMat<RectangularSparseMat<double, 3, Face::MaxNumNd * dim>> dVwalldFaceNormal;
  DistMat<RectangularSparseMat<double, 3, dim-5+1>> dUnodedFaceNormal;
  DistMat<RectangularSparseMat<double, Face::MaxNumNd * dim, dim-5+1>> dUnodedUface;
  DistMat<RectangularSparseMat<double, dim-5+1, dim>> dUnodedTmp;
  DistMat<RectangularSparseMat<double, dim, dim>> dUdV;
  DistMat<RectangularSparseMat<double, 1, dim>> dUdPstiff;
  DistMat<RectangularSparseMat<double, dim, 1>> dPressureDistortiondV;


  MatVecProd_dRdX(SpaceOperator<dim> *, Domain *, bool, bool);

  ~MatVecProd_dRdX();

  dRdXoperators<dim>& operator()(int iSub) {
    return *(dRdXop[iSub]);
  }

  void initializeOperators();

  void constructOperators(Vec3D&, DistSVec<double, dim>&, DistSVec<double, 3>&,
                          PostOperator<dim> *, int = 0);

};

//------------------------------------------------------------------------------

#endif


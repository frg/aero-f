#ifndef _NODAL_GRAD_H_
#define _NODAL_GRAD_H_

#include <Types.h>

template<class Scalar, int Dim> class SVec;
template<class Scalar> class Vec;
class SchemeData;

//------------------------------------------------------------------------------

template<int Dim, class Scalar>
class NodalGrad {

  SchemeData& schemeData;

  SVec<Scalar, Dim>& ddx;
  SVec<Scalar, Dim>& ddy;
  SVec<Scalar, Dim>& ddz;

  SVec<double, 3> *wij;
  SVec<double, 3> *wji;
  SVec<double, 3> *wii;
  SVec<double, 2> *weights;

  Vec<bool> *tag;
  Vec<int> *count;

  SVec<Scalar, Dim> *dddx;
  SVec<Scalar, Dim> *dddy;
  SVec<Scalar, Dim> *dddz;

  SVec<double, Dim> *Vmin;
  SVec<double, Dim> *Vmax;
  SVec<double, Dim> *phi;
  SVec<double, Dim> *dphi_ii;
  SVec<double, Dim> *dphi_ij;
  SVec<double, Dim> *dphi_ji;
  SVec<Scalar, Dim> *ddx_copy;
  SVec<Scalar, Dim> *ddy_copy;
  SVec<Scalar, Dim> *ddz_copy;
  SVec<int_t, Dim> *phi_loc;
  SVec<double, 5*Dim> *Vrec;

 public:
  NodalGrad(SchemeData& _schemeData, SVec<Scalar, Dim>& _ddx, SVec<Scalar, Dim>& _ddy, SVec<Scalar, Dim>& _ddz,
            SVec<double, 3> *_wij, SVec<double, 3> *_wji, SVec<double, 3> *_wii, SVec<double, 2> *_weights,
            Vec<bool> *_tag, Vec<int> *_count, SVec<Scalar, Dim> *_dddx, SVec<Scalar, Dim> *_dddy, SVec<Scalar, Dim> *_dddz,
            SVec<double, Dim> *_Vmin, SVec<double, Dim> *_Vmax, SVec<double, Dim> *_phi, SVec<double, Dim> *_dphi_ii,
            SVec<double, Dim> *_dphi_ij, SVec<double, Dim> *_dphi_ji, SVec<Scalar, Dim> *_ddx_copy,
            SVec<Scalar, Dim> *_ddy_copy, SVec<Scalar, Dim> *_ddz_copy, SVec<int_t, Dim> *_phi_loc,
            SVec<double, 5*Dim> *_Vrec)
    : schemeData(_schemeData), ddx(_ddx), ddy(_ddy), ddz(_ddz), wij(_wij), wji(_wji), wii(_wii),
      weights(_weights), tag(_tag), count(_count), dddx(_dddx), dddy(_dddy), dddz(_dddz),
      Vmin(_Vmin), Vmax(_Vmax), phi(_phi), dphi_ii(_dphi_ii), dphi_ij(_dphi_ij), dphi_ji(_dphi_ji),
      ddx_copy(_ddx_copy), ddy_copy(_ddy_copy), ddz_copy(_ddz_copy), phi_loc(_phi_loc), Vrec(_Vrec) {}

  void fsoInitialize(SVec<Scalar, Dim> *_dddx, SVec<Scalar, Dim> *_dddy, SVec<Scalar, Dim> *_dddz) {
    dddx = _dddx;
    dddy = _dddy;
    dddz = _dddz;
  }

  void setTag(Vec<bool> *_tag) {
    tag = _tag;
  }
  void setCount(Vec<int> *_count) {
    count = _count;
  }
  SchemeData& getSchemeData() {
    return schemeData;
  }
  SVec<Scalar, Dim>& getX() const {
    return ddx;
  }
  SVec<Scalar, Dim>& getY() const {
    return ddy;
  }
  SVec<Scalar, Dim>& getZ() const {
    return ddz;
  }
  SVec<double, 3> *getWij() const {
    return wij;
  }
  SVec<double, 3> *getWji() const {
    return wji;
  }
  SVec<double, 3> *getWii() const {
    return wii;
  }
  SVec<double, 2> *getWeights() const {
    return weights;
  }
  Vec<bool> *getTag() const {
    return tag;
  }
  Vec<int> *getCount() const {
    return count;
  }
  SVec<Scalar, Dim>& getXderivative() const {
    return *dddx;
  }
  SVec<Scalar, Dim>& getYderivative() const {
    return *dddy;
  }
  SVec<Scalar, Dim>& getZderivative() const {
    return *dddz;
  }

  SVec<double, Dim> *getVmin() const {
    return Vmin;
  }
  SVec<double, Dim> *getVmax() const {
    return Vmax;
  }
  SVec<double, Dim> *getPhi() const {
    return phi;
  }
  SVec<double, Dim> *getdPhi_ii() const {
    return dphi_ii;
  }
  SVec<double, Dim> *getdPhi_ij() const {
    return dphi_ij;
  }
  SVec<double, Dim> *getdPhi_ji() const {
    return dphi_ji;
  }
  SVec<Scalar, Dim> *getXcopy() const {
    return ddx_copy;
  }
  SVec<Scalar, Dim> *getYcopy() const {
    return ddy_copy;
  }
  SVec<Scalar, Dim> *getZcopy() const {
    return ddz_copy;
  }
  SVec<int_t, Dim> *getPhiLoc() const {
    return phi_loc;
  }
  SVec<double, 5*Dim> *getVrec() const {
    return Vrec;
  }
};

//------------------------------------------------------------------------------

#endif

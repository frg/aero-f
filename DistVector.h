#ifndef _DIST_VECTOR_H_
#define _DIST_VECTOR_H_

#include <Vector.h>
#include <DistInfo.h>
#include <Communicator.h>
#include <DistNodalGrad.h>

#include <algorithm>
#include <numeric>
#include <cmath>
#include <complex>
#include <functional>
#include <Eigen/Core>

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

template<class Scalar>
class DistVec : public Vec<Vec<Scalar>> {
 public:
  typedef DistInfo InfoType;
  typedef typename Eigen::NumTraits<Scalar>::Real RealScalarType;

 private:
  mutable DistInfo distInfo;
  using Vec<Vec<Scalar>>::v;

 public:
  // constructors --------------------------------------------------------------
  DistVec(const DistInfo&);
  DistVec(const DistVec<Scalar>&);

  // destructor ----------------------------------------------------------------
  ~DistVec();

  using Vec<Vec<Scalar>>::operator=;
  using Vec<Vec<Scalar>>::operator+=;
  using Vec<Vec<Scalar>>::operator+;
  using Vec<Vec<Scalar>>::operator-=;
  using Vec<Vec<Scalar>>::operator-;
  using Vec<Vec<Scalar>>::operator*=;
  using Vec<Vec<Scalar>>::operator*;
  using Vec<Vec<Scalar>>::operator/=;
  using Vec<Vec<Scalar>>::operator/;
  using Vec<Vec<Scalar>>::operator[];

  // assignment operators ------------------------------------------------------
  DistVec<Scalar>& operator=(const Scalar&);
  DistVec<Scalar>& setZero();

  // arithmetic operators ------------------------------------------------------
  DistVec<Scalar>& operator*=(const Scalar&);
  DistVec<Scalar>& operator*=(const DistVec<Scalar>&);
  DistVec<Scalar>& operator/=(const Scalar&);
  DistVec<Scalar>& operator/=(const DistVec<Scalar>&);
  Scalar operator*(const DistVec<Scalar>&) const;  // global inner product
  Scalar operator^(const DistVec<Scalar>&) const;  // local inner product

  // other operators -----------------------------------------------------------
  Vec<Scalar>& operator()(int i) {
    return v[i];
  }
  const Vec<Scalar>& operator()(int i) const {
    return v[i];
  }

  // resize methods ------------------------------------------------------------
  void resize(const DistInfo&);
  void resize(const DistInfo&, const Scalar&);
  void resize(const DistInfo&, NodeData*, int = 1, Scalar = 0);
  void resize(const DistInfo&, const DistVec<Scalar>&, GenNodeData<4> *, int = 1, Scalar = 0);

  // other methods -------------------------------------------------------------
  double sizeMB() const {
    double ret = 0;
    for(int i = 0; i < v.size(); ++i)
      ret += static_cast<double>(v[i].size() * sizeof(Scalar)) / (1024.0 * 1024.0);
    return ret;
  }

  DistVec<Scalar>& pow(const DistVec<Scalar>&, double);

  Scalar min() const;

  Scalar absMin() const;

  Scalar max() const;

  Scalar absMax() const;

  Scalar locSum() const;

  Scalar sum() const;

  Scalar locAbsSum() const;

  Scalar absSum() const;

  RealScalarType norm() const {
    return std::real(sqrt(*this * *this));
  }

  InfoType info() const {
    for(int i = 0; i < v.size(); ++i) distInfo.subLen[i] = v[i].size();
    return distInfo;
  }

  int numLocSub() const {
    return int(v.size());
  }

  int subSize(int i) const {
    return v[i].size();
  }

  Scalar *subData(int i) const {
    return v[i].data();
  }

  bool *getMasterFlag(int i) const {
    return (distInfo.masterFlag) ? distInfo.masterFlag->subData(i) : nullptr;
  }

  void print() const {
    for(int i = 0; i < v.size(); ++i) std::cout << v[i] << std::endl;
  }

};

//------------------------------------------------------------------------------

template<class Scalar>
inline
DistVec<Scalar>::DistVec(const DistInfo& dI) :
  Vec<Vec<Scalar>>(dI.numLocSub), distInfo(dI) {
  #pragma omp parallel for
  for(int iSub = 0; iSub < numLocSub(); ++iSub) {
    v[iSub].resize(distInfo.subLen[iSub]);
  }
}

//------------------------------------------------------------------------------

template<class Scalar>
inline
DistVec<Scalar>::DistVec(const DistVec<Scalar>& y) :
  Vec<Vec<Scalar>>(y.numLocSub()), distInfo(y.distInfo) {
  #pragma omp parallel for
  for(int iSub = 0; iSub < y.numLocSub(); ++iSub) {
    v[iSub].resize(y.subSize(iSub));
    v[iSub] = y.v[iSub];
  }
}

//------------------------------------------------------------------------------

template<class Scalar>
inline
DistVec<Scalar>::~DistVec() {
}

//------------------------------------------------------------------------------

template<class Scalar>
inline
DistVec<Scalar>&
DistVec<Scalar>::setZero() {
  #pragma omp parallel for
  for(int iSub = 0; iSub < numLocSub(); ++iSub) {
    v[iSub].setZero();
  }
  return *this;
}

//------------------------------------------------------------------------------

template<class Scalar>
inline
DistVec<Scalar>&
DistVec<Scalar>::operator=(const Scalar& s) {
  #pragma omp parallel for
  for(int iSub = 0; iSub < numLocSub(); ++iSub) {
    v[iSub] = s;
  }
  return *this;
}

//------------------------------------------------------------------------------

template<class Scalar>
inline
DistVec<Scalar>&
DistVec<Scalar>::operator*=(const Scalar& s) {
  #pragma omp parallel for
  for(int iSub = 0; iSub < numLocSub(); ++iSub) {
    v[iSub] *= s;
  }
  return *this;
}

//------------------------------------------------------------------------------

template<class Scalar>
inline
DistVec<Scalar>&
DistVec<Scalar>::operator*=(const DistVec<Scalar>& x) {
  #pragma omp parallel for
  for(int iSub = 0; iSub < numLocSub(); ++iSub) {
    v[iSub] *= x[iSub];
  }
  return *this;
}

//------------------------------------------------------------------------------

template<class Scalar>
inline
DistVec<Scalar>&
DistVec<Scalar>::operator/=(const Scalar& s) {
  #pragma omp parallel for
  for(int iSub = 0; iSub < numLocSub(); ++iSub) {
    v[iSub] /= s;
  }
  return *this;
}

//------------------------------------------------------------------------------

template<class Scalar>
inline
DistVec<Scalar>&
DistVec<Scalar>::operator/=(const DistVec<Scalar>& x) {
  #pragma omp parallel for
  for(int iSub = 0; iSub < numLocSub(); ++iSub) {
    v[iSub] /= x[iSub];
  }
  return *this;
}

//------------------------------------------------------------------------------

template<class Scalar>
inline
Scalar
DistVec<Scalar>::operator^(const DistVec<Scalar>& y) const {
  Scalar res = 0;
  if(distInfo.masterFlag) {
    #pragma omp parallel for reduction(+: res)
    for(int iSub = 0; iSub < numLocSub(); ++iSub) {
      const Vec<Scalar>& vSub = v[iSub];
      const Vec<Scalar>& ySub = y[iSub];
      const Vec<bool>& masterFlag = (*distInfo.masterFlag)[iSub];
      Scalar locres = 0;
      for(int i = 0; i < vSub.size(); ++i) {
        if(masterFlag[i]) {
          locres += vSub[i] * ySub[i];
        }
      }
      res += locres;
    }
  }
  else {
    #pragma omp parallel for reduction(+: res)
    for(int iSub = 0; iSub < numLocSub(); ++iSub) {
      res += v[iSub] * y.v[iSub];
    }
  }
  return res;
}

//------------------------------------------------------------------------------

template<class Scalar>
inline
Scalar
DistVec<Scalar>::operator*(const DistVec<Scalar>& y) const {
  Scalar res = *this ^ y;
  distInfo.com->globalSum(1, &res);
  return res;
}

//------------------------------------------------------------------------------

template<class Scalar>
inline
void
DistVec<Scalar>::resize(const DistInfo& dI) {
  distInfo = dI;
  #pragma omp parallel for
  for(int iSub = 0; iSub < numLocSub(); ++iSub) {
    v[iSub].resize(distInfo.subLen[iSub]);
  }
}

//------------------------------------------------------------------------------

template<class Scalar>
inline
void
DistVec<Scalar>::resize(const DistInfo& dI, const Scalar& initialValue) {
  distInfo = dI;
  #pragma omp parallel for
  for(int iSub = 0; iSub < numLocSub(); ++iSub) {
    v[iSub].resize(distInfo.subLen[iSub], initialValue);
  }
}

//------------------------------------------------------------------------------

template<class Scalar>
inline
void
DistVec<Scalar>::resize(const DistInfo& dI, NodeData *data, int iflag, Scalar spval) {
  distInfo = dI;
  #pragma omp parallel for
  for(int iSub = 0; iSub < numLocSub(); ++iSub) {
    if(data) v[iSub].resize(distInfo.subLen[iSub], data[iSub], iflag, spval);
    else v[iSub].resize(distInfo.subLen[iSub]);
  }
}

//------------------------------------------------------------------------------

template<class Scalar>
void
DistVec<Scalar>::resize(const DistInfo& dI, const DistVec<Scalar>& V0, GenNodeData<4> *data, int iflag, Scalar spval) {
  distInfo = dI;
  #pragma omp parallel for
  for(int iSub = 0; iSub < numLocSub(); ++iSub) {
    if(data) v[iSub].resize(distInfo.subLen[iSub], V0[iSub], data[iSub], iflag, spval);
    else v[iSub].resize(distInfo.subLen[iSub]);
  }
}

//------------------------------------------------------------------------------

template<class Scalar>
inline
DistVec<Scalar>& DistVec<Scalar>::pow(const DistVec<Scalar>& y, double exp) {
  using std::pow;
  #pragma omp parallel for
  for(int iSub = 0; iSub < numLocSub(); ++iSub) {
    Vec<Scalar>& vSub = v[iSub];
    const Vec<Scalar>& ySub = y[iSub];
    for(int i = 0; i < vSub.size(); ++i) {
      vSub[i] = pow(ySub[i], exp);
    }
  }
  return *this;
}

//------------------------------------------------------------------------------

template<class Scalar>
inline
Scalar
DistVec<Scalar>::min() const {
  std::vector<Scalar> allmin(numLocSub());
  #pragma omp parallel for
  for(int iSub = 0; iSub < numLocSub(); ++iSub) {
    if(v[iSub].size() > 0) {
      allmin[iSub] = v[iSub].min();
    }
  }
  Scalar vmin = std::numeric_limits<Scalar>::max();
  for(int iSub = 0; iSub < numLocSub(); ++iSub) {
    if(v[iSub].size() > 0) {
      vmin = vmin < allmin[iSub] ? vmin : allmin[iSub];
    }
  }
  distInfo.com->globalMin(1, &vmin);
  return vmin;
}

//------------------------------------------------------------------------------

template<class Scalar>
inline
Scalar
DistVec<Scalar>::absMin() const {
  std::vector<Scalar> allmin(numLocSub());
  #pragma omp parallel for
  for(int iSub = 0; iSub < numLocSub(); ++iSub) {
    if(v[iSub].size() > 0) {
      allmin[iSub] = v[iSub].absMin();
    }
  }
  Scalar vmin = std::numeric_limits<Scalar>::max();
  for(int iSub = 0; iSub < numLocSub(); ++iSub) {
    if(v[iSub].size() > 0) {
      vmin = vmin < allmin[iSub] ? vmin : allmin[iSub];
    }
  }
  distInfo.com->globalMin(1, &vmin);
  return vmin;
}

//------------------------------------------------------------------------------

template<class Scalar>
inline
Scalar
DistVec<Scalar>::max() const {
  std::vector<Scalar> allmax(numLocSub());
  #pragma omp parallel for
  for(int iSub = 0; iSub < numLocSub(); ++iSub) {
    if(v[iSub].size() > 0) {
      allmax[iSub] = v[iSub].max();
    }
  }
  Scalar vmax = std::numeric_limits<Scalar>::lowest();
  for(int iSub = 0; iSub < numLocSub(); ++iSub) {
    if(v[iSub].size() > 0) {
      vmax = vmax > allmax[iSub] ? vmax : allmax[iSub];
    }
  }
  distInfo.com->globalMax(1, &vmax);
  return vmax;
}

//------------------------------------------------------------------------------

template<class Scalar>
inline
Scalar
DistVec<Scalar>::absMax() const {
  std::vector<Scalar> allmax(numLocSub());
  #pragma omp parallel for
  for(int iSub = 0; iSub < numLocSub(); ++iSub) {
    if(v[iSub].size() > 0) {
      allmax[iSub] = v[iSub].absMax();
    }
  }
  Scalar vmax = std::numeric_limits<Scalar>::lowest();
  for(int iSub = 0; iSub < numLocSub(); ++iSub) {
    if(v[iSub].size() > 0) {
      vmax = vmax > allmax[iSub] ? vmax : allmax[iSub];
    }
  }
  distInfo.com->globalMax(1, &vmax);
  return vmax;
}

//------------------------------------------------------------------------------

template<class Scalar>
inline
Scalar
DistVec<Scalar>::locSum() const {
  Scalar res = 0;
  if(distInfo.masterFlag) {
    #pragma omp parallel for reduction(+: res)
    for(int iSub = 0; iSub < numLocSub(); ++iSub) {
      const Vec<Scalar>& vSub = v[iSub];
      const Vec<bool>& masterFlag = (*distInfo.masterFlag)[iSub];
      Scalar locres = 0;
      for(int i = 0; i < vSub.size(); ++i)
        if(masterFlag[i]) {
          locres += vSub[i];
        }
      res += locres;
    }
  }
  else {
    #pragma omp parallel for reduction(+: res)
    for(int iSub = 0; iSub < numLocSub(); ++iSub) {
      res += v[iSub].sum();
    }
  }
  return res;
}

//------------------------------------------------------------------------------

template<class Scalar>
inline
Scalar
DistVec<Scalar>::sum() const {
  double res = this->locSum();
  distInfo.com->globalSum(1, &res);
  return res;
}

//------------------------------------------------------------------------------

template<class Scalar>
inline
Scalar
DistVec<Scalar>::locAbsSum() const {
  Scalar res = 0;
  if(distInfo.masterFlag) {
    #pragma omp parallel for reduction(+: res)
    for(int iSub = 0; iSub < numLocSub(); ++iSub) {
      const Vec<Scalar>& vSub = v[iSub];
      const Vec<bool>& masterFlag = (*distInfo.masterFlag)[iSub];
      Scalar locres = 0;
      for(int i = 0; i < vSub.size(); ++i)
        if(masterFlag[i]) {
          locres += std::abs(vSub[i]);
        }
      res += locres;
    }
  }
  else {
    #pragma omp parallel for reduction(+: res)
    for(int iSub = 0; iSub < numLocSub(); ++iSub) {
      res += v[iSub].absSum();
    }
  }
  return res;
}

//------------------------------------------------------------------------------

template<class Scalar>
inline
Scalar
DistVec<Scalar>::absSum() const {
  double res = this->locAbsSum();
  distInfo.com->globalSum(1, &res);
  return res;
}

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

template<class Scalar, int Dim>
class DistSVec : public Vec<SVec<Scalar, Dim>> {
 public:
  typedef DistInfo InfoType;
  typedef typename Eigen::NumTraits<Scalar>::Real RealScalarType;

 private:
  mutable DistInfo distInfo;
  using Vec<SVec<Scalar, Dim>>::v;
  DistVec<bool> *sparsityMask;

 public:
  // constructors --------------------------------------------------------------
  DistSVec(const DistInfo&);
  DistSVec(const DistInfo&, int);
  DistSVec(const DistSVec<Scalar, Dim>&);

  // destructor ----------------------------------------------------------------
  ~DistSVec();

  using Vec<SVec<Scalar, Dim>>::operator=;
  using Vec<SVec<Scalar, Dim>>::operator+=;
  using Vec<SVec<Scalar, Dim>>::operator+;
  using Vec<SVec<Scalar, Dim>>::operator-=;
  using Vec<SVec<Scalar, Dim>>::operator-;
  using Vec<SVec<Scalar, Dim>>::operator*=;
  using Vec<SVec<Scalar, Dim>>::operator*;
  using Vec<SVec<Scalar, Dim>>::operator/=;
  using Vec<SVec<Scalar, Dim>>::operator/;
  using Vec<SVec<Scalar, Dim>>::operator[];
  using Vec<SVec<Scalar, Dim>>::real;
  using Vec<SVec<Scalar, Dim>>::imag;

  // assignment operators ------------------------------------------------------
  DistSVec<Scalar, Dim>& operator=(const Scalar&);
  DistSVec<Scalar, Dim>& setZero();

  // arithmetic operators ------------------------------------------------------
  DistSVec<Scalar, Dim>& operator*=(const Scalar&);
  DistSVec<Scalar, Dim>& operator*=(const DistVec<Scalar>&);
  DistSVec<Scalar, Dim>& operator/=(const Scalar&);
  DistSVec<Scalar, Dim>& operator/=(const DistVec<Scalar>&);
  Scalar operator*(const DistSVec<Scalar, Dim>&) const;  // global inner product
  Scalar operator^(const DistSVec<Scalar, Dim>&) const;  // local inner product
  Scalar localDotProductSparse(const DistSVec<Scalar, Dim>&) const;  // local inner product (sparse)

  void linAdd(const Scalar&, const DistSVec<Scalar, Dim>&);

  // other operators -----------------------------------------------------------
  SVec<Scalar, Dim>& operator()(int i) {
    return v[i];
  }
  const SVec<Scalar, Dim>& operator()(int i) const {
    return v[i];
  }

  // resize methods ------------------------------------------------------------
  void resize(const DistInfo&);
  void resize(const DistInfo&, const Scalar&);
  void resize(const DistInfo&, NodeData*, int = 1, DistNodalGrad<Dim, Scalar> * = nullptr);
  void resize(const DistInfo&, const DistSVec<Scalar, Dim>&, GenNodeData<4> *, int = 1);

  // other methods -------------------------------------------------------------
  double sizeMB() const {
    double ret = 0;
    for(int i = 0; i < v.size(); ++i)
      ret += static_cast<double>(v[i].size() * v[i].dim() * sizeof(Scalar)) / (1024.0 * 1024.0);
    return ret;
  }

  void gather(Eigen::Ref<Eigen::Matrix<Scalar, Eigen::Dynamic, 1>>) const;

  void scatter(const Eigen::Ref<const Eigen::Matrix<Scalar, Eigen::Dynamic, 1>>, std::function<void(DistSVec<Scalar,Dim>&)>);

  void restrict();

  void average();

  template<int Dim1, int Dim2>
  void split(DistSVec<Scalar, Dim1>&, DistSVec<Scalar, Dim2>&) const;

  template<int Dim1, int Dim2>
  void merge(const DistSVec<Scalar, Dim1>&, const DistSVec<Scalar, Dim2>&);

  template<int Dim1>
  void strip(DistSVec<Scalar, Dim1>&) const;

  template<int Dim1>
  void pad(DistSVec<Scalar, Dim1>&) const;

  void getReal(const DistSVec<std::complex<Scalar>, Dim>&);

  void getImag(const DistSVec<std::complex<Scalar>, Dim>&);

  void min(std::vector<Scalar>&) const;

  void absMin(std::vector<Scalar>&) const;

  void max(std::vector<Scalar>&) const;

  void absMax(std::vector<Scalar>&) const;

  void locSum(std::vector<Scalar>&) const;

  void sum(std::vector<Scalar>&) const;

  void locSumSq(std::vector<Scalar>&) const;

  void sumSq(std::vector<Scalar>&) const;

  void locAbsSum(std::vector<Scalar>&) const;

  void absSum(std::vector<Scalar>&) const;

  Scalar min() const;

  Scalar absMin() const;

  Scalar max() const;

  Scalar absMax() const;

  Scalar locSum() const;

  Scalar sum() const;

  Scalar locAbsSum() const;

  Scalar absSum() const;

  RealScalarType squaredNorm() const;
  RealScalarType norm() const;

  InfoType info() const {
    for(int i = 0; i < v.size(); ++i) distInfo.subLen[i] = v[i].size();
    return distInfo;
  }

  int dim() const {
    return (Dim == Eigen::Dynamic) ? v[0].dim() : Dim;
  }

  int numLocSub() const {
    return int(v.size());
  }

  int subSize(int i) const {
    return v[i].size();
  }

  Scalar(*subData(int i) const)[(Dim==Eigen::Dynamic)?0:Dim] {
    static_assert(Dim != Eigen::Dynamic, "*** Error: in DistSVec::data(), Dim == Eigen::Dynamic");
    return v[i].data();
  }

  bool *getMasterFlag(int i) const {
    return (distInfo.masterFlag) ? distInfo.masterFlag->subData(i) : nullptr;
  }

  void print() const {
    for(int i = 0; i < v.size(); ++i) std::cout << v[i] << std::endl;
  }

  void computeSparsityMask();

  void setSparsityMask(DistVec<bool> *);

  DistVec<bool> *getSparsityMask() const { return sparsityMask; }

  void applyMaskColumn(const DistVec<int>&);
  void applyMaskColumn(const DistVec<bool>&);
  void applyMaskColumn(const DistVec<bool>&, DistSVec<double, Dim>&);
  void applyMaskColumn(const DistVec<int>&, DistSVec<double, Dim>&);
  void applyMaskColumn(const DistVec<bool>&, double *);

  bool isSparse() const { return (sparsityMask ? true : false); }

  void undoSparsity();

};

//------------------------------------------------------------------------------

template<class Scalar, int Dim>
inline
DistSVec<Scalar, Dim>::DistSVec(const DistInfo& dI) :
  Vec<SVec<Scalar, Dim>>(dI.numLocSub), distInfo(dI) {
  sparsityMask = nullptr;
  #pragma omp parallel for
  for(int iSub = 0; iSub < numLocSub(); ++iSub) {
    v[iSub].resize(distInfo.subLen[iSub]);
  }
}

//------------------------------------------------------------------------------

template<class Scalar, int Dim>
inline
DistSVec<Scalar, Dim>::DistSVec(const DistInfo& dI, int _dim) :
  Vec<SVec<Scalar, Dim>>(dI.numLocSub), distInfo(dI) {
  sparsityMask = nullptr;
  #pragma omp parallel for
  for(int iSub = 0; iSub < numLocSub(); ++iSub) {
    v[iSub].resize(distInfo.subLen[iSub], _dim);
  }
}

//------------------------------------------------------------------------------

template<class Scalar, int Dim>
inline
DistSVec<Scalar, Dim>::DistSVec(const DistSVec<Scalar, Dim>& y) :
  Vec<SVec<Scalar, Dim>>(y.numLocSub()), distInfo(y.distInfo) {
  sparsityMask = nullptr;
  #pragma omp parallel for
  for(int iSub = 0; iSub < numLocSub(); ++iSub) {
    v[iSub].resize(y.subSize(iSub));
    v[iSub] = y.v[iSub];
  }
}

//------------------------------------------------------------------------------

template<class Scalar, int Dim>
inline
DistSVec<Scalar, Dim>::~DistSVec() {
  if(sparsityMask) {
    delete sparsityMask;
  }
}

//------------------------------------------------------------------------------

template<class Scalar, int Dim>
inline
DistSVec<Scalar, Dim>&
DistSVec<Scalar, Dim>::setZero() {
  #pragma omp parallel for
  for(int iSub = 0; iSub < numLocSub(); ++iSub) {
    v[iSub].setZero();
  }
  return *this;
}

//------------------------------------------------------------------------------

template<class Scalar, int Dim>
inline
void
DistSVec<Scalar, Dim>::linAdd(const Scalar& a, const DistSVec<Scalar, Dim>& b) {
  #pragma omp parallel for
  for(int iSub = 0; iSub < numLocSub(); ++iSub) {
    v[iSub] += a*b[iSub];
  }
}

//------------------------------------------------------------------------------

template<class Scalar, int Dim>
inline
DistSVec<Scalar, Dim>&
DistSVec<Scalar, Dim>::operator=(const Scalar& s) {
  #pragma omp parallel for
  for(int iSub = 0; iSub < numLocSub(); ++iSub) {
    v[iSub] = s;
  }
  return *this;
}

//------------------------------------------------------------------------------

template<class Scalar, int Dim>
inline
DistSVec<Scalar, Dim>&
DistSVec<Scalar, Dim>::operator*=(const Scalar& s) {
  #pragma omp parallel for
  for(int iSub = 0; iSub < numLocSub(); ++iSub) {
    v[iSub] *= s;
  }
  return *this;
}

//------------------------------------------------------------------------------

template<class Scalar, int Dim>
inline
DistSVec<Scalar, Dim>&
DistSVec<Scalar, Dim>::operator*=(const DistVec<Scalar>& x) {
  #pragma omp parallel for
  for(int iSub = 0; iSub < numLocSub(); ++iSub) {
    v[iSub] *= x[iSub];
  }
  return *this;
}

//------------------------------------------------------------------------------

template<class Scalar, int Dim>
inline
DistSVec<Scalar, Dim>&
DistSVec<Scalar, Dim>::operator/=(const Scalar& s) {
  #pragma omp parallel for
  for(int iSub = 0; iSub < numLocSub(); ++iSub) {
    v[iSub] /= s;
  }
  return *this;
}

//------------------------------------------------------------------------------

template<class Scalar, int Dim>
inline
DistSVec<Scalar, Dim>&
DistSVec<Scalar, Dim>::operator/=(const DistVec<Scalar>& x) {
  #pragma omp parallel for
  for(int iSub = 0; iSub < numLocSub(); ++iSub) {
    v[iSub] /= x[iSub];
  }
  return *this;
}

//------------------------------------------------------------------------------

template<class Scalar, int Dim>
inline
Scalar
DistSVec<Scalar, Dim>::operator^(const DistSVec<Scalar, Dim>& x) const {
  if(this->isSparse() && x.isSparse()) {
    return localDotProductSparse(x);
  }
  Scalar res = 0;
  if(distInfo.masterFlag) {
    #pragma omp parallel for reduction(+: res)
    for(int iSub = 0; iSub < numLocSub(); ++iSub) {
      const SVec<Scalar, Dim>& vSub = v[iSub];
      const SVec<Scalar, Dim>& xSub = x[iSub];
      const Vec<bool>& masterFlag = (*distInfo.masterFlag)[iSub];
      Scalar locres = 0;
      for(int i = 0; i < vSub.size(); ++i) {
        if(masterFlag[i]) {
          locres += xSub.row(i).dot(vSub.row(i));
        }
      }
      res += locres;
    }
  }
  else {
    #pragma omp parallel for reduction(+: res)
    for(int iSub = 0; iSub < numLocSub(); ++iSub) {
      const SVec<Scalar, Dim>& vSub = v[iSub];
      const SVec<Scalar, Dim>& xSub = x[iSub];
      Scalar locres = 0;
      for(int i = 0; i < vSub.size(); ++i) {
        locres += xSub.row(i).dot(vSub.row(i));
      }
      res += locres;
    }
  }
  return res;
}

//------------------------------------------------------------------------------

template<class Scalar, int Dim>
inline
Scalar
DistSVec<Scalar, Dim>::localDotProductSparse(const DistSVec<Scalar, Dim>& x) const {
  Scalar res = 0;
  if(distInfo.masterFlag) {
    #pragma omp parallel for reduction(+: res)
    for(int iSub = 0; iSub < numLocSub(); ++iSub) {
      const SVec<Scalar, Dim>& vSub = v[iSub];
      const SVec<Scalar, Dim>& xSub = x[iSub];
      const Vec<bool>& thisMaskSub = (*sparsityMask)[iSub];
      const Vec<bool>& xMaskSub = (*(x.getSparsityMask()))[iSub];
      const Vec<bool>& masterFlag = (*distInfo.masterFlag)[iSub];
      Scalar locres = 0;
      for(int i = 0; i < vSub.size(); ++i) {
        if(masterFlag[i] && thisMaskSub[i] && xMaskSub[i]) {
          locres += xSub.row(i).dot(vSub.row(i));
        }
      }
      res += locres;
    }
  }
  else {
    #pragma omp parallel for reduction(+: res)
    for(int iSub = 0; iSub < numLocSub(); ++iSub) {
      const SVec<Scalar, Dim>& vSub = v[iSub];
      const SVec<Scalar, Dim>& xSub = x[iSub];
      const Vec<bool>& thisMaskSub = (*sparsityMask)[iSub];
      const Vec<bool>& xMaskSub = (*(x.getSparsityMask()))[iSub];
      Scalar locres = 0;
      for(int i = 0; i < vSub.size(); ++i) {
        if(thisMaskSub[i] && xMaskSub[i]) {
          locres += xSub.row(i).dot(vSub.row(i));
        }
      }
      res += locres;
    }
  }
  return res;
}

//------------------------------------------------------------------------------

template<class Scalar, int Dim>
inline
Scalar
DistSVec<Scalar, Dim>::operator*(const DistSVec<Scalar, Dim>& x) const {
  Scalar res = *this ^ x;
  distInfo.com->globalSum(1, &res);
  return res;
}

//------------------------------------------------------------------------------

template<class Scalar, int Dim>
inline
void
DistSVec<Scalar, Dim>::resize(const DistInfo& dI) {
  distInfo = dI;
  #pragma omp parallel for
  for(int iSub = 0; iSub < numLocSub(); ++iSub) {
    v[iSub].resize(distInfo.subLen[iSub]);
  }
}

//------------------------------------------------------------------------------

template<class Scalar, int Dim>
inline
void
DistSVec<Scalar, Dim>::resize(const DistInfo& dI, const Scalar& initialValue) {
  distInfo = dI;
  #pragma omp parallel for
  for(int iSub = 0; iSub < numLocSub(); ++iSub) {
    v[iSub].resize(distInfo.subLen[iSub], initialValue);
  }
}

//------------------------------------------------------------------------------

template<class Scalar, int Dim>
inline
void
DistSVec<Scalar, Dim>::resize(const DistInfo& dI, NodeData *data, int iflag, DistNodalGrad<Dim, Scalar> *ngrad) {
  distInfo = dI;
  #pragma omp parallel for
  for(int iSub = 0; iSub < numLocSub(); ++iSub) {
    if(data) {
      if(ngrad) {
        v[iSub].resize(distInfo.subLen[iSub], data[iSub], (*ngrad)(iSub), iflag);
      }
      else {
        v[iSub].resize(distInfo.subLen[iSub], data[iSub], iflag);
      }
    }
    else v[iSub].resize(distInfo.subLen[iSub]);
  }
}

//------------------------------------------------------------------------------

template<class Scalar, int Dim>
inline
void
DistSVec<Scalar, Dim>::resize(const DistInfo& dI, const DistSVec<Scalar, Dim>& V0, GenNodeData<4> *data, int iflag) {
  distInfo = dI;
  #pragma omp parallel for
  for(int iSub = 0; iSub < numLocSub(); ++iSub) {
    if(data) v[iSub].resize(distInfo.subLen[iSub], V0[iSub], data[iSub], iflag);
    else v[iSub].resize(distInfo.subLen[iSub]);
  }
}

//------------------------------------------------------------------------------

template<class Scalar, int Dim>
inline
void
DistSVec<Scalar, Dim>::gather(Eigen::Ref<Eigen::Matrix<Scalar, Eigen::Dynamic, 1>> y) const {
  int k = 0;
  for(int iSub = 0; iSub < numLocSub(); ++iSub) {
    const SVec<Scalar, Dim>& vSub = v[iSub];
    const Vec<bool>& masterFlag = (*distInfo.masterFlag)[iSub];
    for(int i = 0; i < vSub.size(); ++i) {
      if(masterFlag[i]) {
        for(int j = 0; j < vSub.dim(); ++j) {
          y[k++] = vSub(i,j);
        }
      }
    }
  }
}

//------------------------------------------------------------------------------

template<class Scalar, int Dim>
inline
void
DistSVec<Scalar, Dim>::scatter(const Eigen::Ref<const Eigen::Matrix<Scalar, Eigen::Dynamic, 1>> y,
                               std::function<void(DistSVec<Scalar,Dim>&)> assemble) {
  int k = 0;
  for(int iSub = 0; iSub < numLocSub(); ++iSub) {
    SVec<Scalar, Dim>& vSub = v[iSub];
    const Vec<bool>& masterFlag = (*distInfo.masterFlag)[iSub];
    for(int i = 0; i < vSub.size(); ++i) {
      if(masterFlag[i]) {
        for(int j = 0; j < vSub.dim(); ++j) {
          vSub(i,j) = y[k++];
        }
      }
      else {
        for(int j = 0; j < vSub.dim(); ++j) {
          vSub(i,j) = Scalar(0);
        }
      }
    }
  }
  assemble(*this);
}

//------------------------------------------------------------------------------

template<class Scalar, int Dim>
inline
void
DistSVec<Scalar, Dim>::restrict() {
  if(!distInfo.masterFlag) {
    return;
  }
  #pragma omp parallel for
  for(int iSub = 0; iSub < numLocSub(); ++iSub) {
    SVec<Scalar, Dim>& vSub = v[iSub];
    const Vec<bool>& masterFlag = (*distInfo.masterFlag)[iSub];
    for(int i = 0; i < vSub.size(); ++i) {
      if(!masterFlag[i]) {
        for(int j = 0; j < vSub.dim(); ++j) {
          vSub(i,j) = 0;
        }
      }
    }
  }
}

//------------------------------------------------------------------------------

template<class Scalar, int Dim>
inline
void
DistSVec<Scalar, Dim>::average() {
  if(!distInfo.invNdWeight) {
    return;
  }
  #pragma omp parallel for
  for(int iSub = 0; iSub < numLocSub(); ++iSub) {
    SVec<Scalar, Dim>& vSub = v[iSub];
    for(int i = 0; i < vSub.size(); ++i) {
      for(int j = 0; j < vSub.dim(); ++j) {
        vSub(i,j) *= distInfo.getInvWeight(iSub)[i];
      }
    }
  }
}

//------------------------------------------------------------------------------

template<class Scalar, int Dim>
template<int Dim1, int Dim2>
inline
void
DistSVec<Scalar, Dim>::split(DistSVec<Scalar, Dim1>& y, DistSVec<Scalar, Dim2>& z) const {
  #pragma omp parallel for
  for(int iSub = 0; iSub < numLocSub(); ++iSub) {
    const SVec<Scalar, Dim>& vSub = v[iSub];
    SVec<Scalar, Dim1>& ySub = y[iSub];
    SVec<Scalar, Dim2>& zSub = z[iSub];
    for(int i = 0; i < vSub.size(); ++i) {
      for(int j = 0; j < ySub.dim(); ++j) {
        ySub(i,j) = vSub(i,j);
      }
      for(int j = 0; j < zSub.dim(); ++j) {
        zSub(i,j) = vSub(i,ySub.dim()+j);
      }
    }
  }
}

//------------------------------------------------------------------------------

template<class Scalar, int Dim>
template<int Dim1, int Dim2>
inline
void
DistSVec<Scalar, Dim>::merge(const DistSVec<Scalar, Dim1>& y, const DistSVec<Scalar, Dim2>& z) {
  #pragma omp parallel for
  for(int iSub = 0; iSub < numLocSub(); ++iSub) {
    SVec<Scalar, Dim>& vSub = v[iSub];
    const SVec<Scalar, Dim1>& ySub = y[iSub];
    const SVec<Scalar, Dim2>& zSub = z[iSub];
    for(int i = 0; i < vSub.size(); ++i) {
      for(int j = 0; j < ySub.dim(); ++j) {
        vSub(i,j) = ySub(i,j);
      }
      for(int j = 0; j < zSub.dim(); ++j) {
        vSub(i,ySub.dim()+j) = zSub(i,j);
      }
    }
  }
}

//------------------------------------------------------------------------------

template<class Scalar, int Dim>
template<int Dim1>
inline
void
DistSVec<Scalar, Dim>::strip(DistSVec<Scalar, Dim1>& y) const {
  int offset = 0;
  if(dim() > y.dim()) {
    if(y.dim() < 5) {
      offset = 5;
    }
  }
  #pragma omp parallel for
  for(int iSub = 0; iSub < numLocSub(); ++iSub) {
    const SVec<Scalar, Dim>& vSub = v[iSub];
    SVec<Scalar, Dim1>& ySub = y[iSub];
    for(int i = 0; i < vSub.size(); ++i) {
      for(int j = 0; j < ySub.dim(); ++j) {
        ySub(i,j) = vSub(i,offset+j);
      }
    }
  }
}

//------------------------------------------------------------------------------

template<class Scalar, int Dim>
template<int Dim1>
inline
void
DistSVec<Scalar, Dim>::pad(DistSVec<Scalar, Dim1>& y) const {
  int offset = 0;
  if(dim() < y.dim())
    if(dim() < 5) {
      offset = 5;
    }
  #pragma omp parallel for
  for(int iSub = 0; iSub < numLocSub(); ++iSub) {
    const SVec<Scalar, Dim>& vSub = v[iSub];
    SVec<Scalar, Dim1>& ySub = y[iSub];
    for(int i = 0; i < vSub.size(); ++i) {
      for(int j = 0; j < vSub.dim(); ++j) {
        ySub(i,j+offset) = vSub(i,j);
      }
    }
  }
}

//------------------------------------------------------------------------------

template<class Scalar, int Dim>
inline
void
DistSVec<Scalar, Dim>::getReal(const DistSVec<std::complex<Scalar>, Dim>& y) {
  #pragma omp parallel for
  for(int iSub = 0; iSub < numLocSub(); ++iSub) {
    v[iSub] = y[iSub].real();
  }
}

//------------------------------------------------------------------------------

template<class Scalar, int Dim>
inline
void
DistSVec<Scalar, Dim>::getImag(const DistSVec<std::complex<Scalar>, Dim>& y) {
  #pragma omp parallel for
  for(int iSub = 0; iSub < numLocSub(); ++iSub) {
    v[iSub] = y[iSub].imag();
  }
}

//------------------------------------------------------------------------------

template<class Scalar, int Dim>
inline
void
DistSVec<Scalar, Dim>::min(std::vector<Scalar>& vmin) const {
  std::vector<Scalar> allmin[numLocSub()];
  #pragma omp parallel for
  for(int iSub = 0; iSub < numLocSub(); ++iSub) {
    if(v[iSub].size() > 0) {
      v[iSub].min(allmin[iSub]);
    }
  }
  std::fill(vmin.begin(), vmin.end(), std::numeric_limits<Scalar>::max());
  for(int iSub = 0; iSub < numLocSub(); ++iSub) {
    if(v[iSub].size() > 0) {
      for(int iDim = 0; iDim < dim(); iDim++) {
        vmin[iDim] = std::min(vmin[iDim], allmin[iSub][iDim]);
      }
    }
  }
  distInfo.com->globalMin(dim(), vmin.data());
}

//------------------------------------------------------------------------------

template<class Scalar, int Dim>
inline
void
DistSVec<Scalar, Dim>::absMin(std::vector<Scalar>& vmin) const {
  std::vector<Scalar> allmin[numLocSub()];
  #pragma omp parallel for
  for(int iSub = 0; iSub < numLocSub(); ++iSub) {
    if(v[iSub].size() > 0) {
      v[iSub].absMin(allmin[iSub]);
    }
  }
  std::fill(vmin.begin(), vmin.end(), std::numeric_limits<Scalar>::max());
  for(int iSub = 0; iSub < numLocSub(); ++iSub) {
    if(v[iSub].size() > 0) {
      for(int iDim = 0; iDim < dim(); iDim++) {
        vmin[iDim] = std::min(vmin[iDim], allmin[iSub][iDim]);
      }
    }
  }
  distInfo.com->globalMin(dim(), vmin.data());
}

//------------------------------------------------------------------------------

template<class Scalar, int Dim>
inline
void
DistSVec<Scalar, Dim>::max(std::vector<Scalar>& vmax) const {
  std::vector<Scalar> allmax[numLocSub()];
  #pragma omp parallel for
  for(int iSub = 0; iSub < numLocSub(); ++iSub) {
    if(v[iSub].size() > 0) {
      v[iSub].max(allmax[iSub]);
    }
  }
  std::fill(vmax.begin(), vmax.end(), std::numeric_limits<Scalar>::lowest());
  for(int iSub = 0; iSub < numLocSub(); ++iSub) {
    if(v[iSub].size() > 0) {
      for(int iDim = 0; iDim < dim(); iDim++) {
        vmax[iDim] = std::max(vmax[iDim], allmax[iSub][iDim]);
      }
    }
  }
  distInfo.com->globalMax(dim(), vmax.data());
}

//------------------------------------------------------------------------------

template<class Scalar, int Dim>
inline
void
DistSVec<Scalar, Dim>::absMax(std::vector<Scalar>& vmax) const {
  std::vector<Scalar> allmax[numLocSub()];
  #pragma omp parallel for
  for(int iSub = 0; iSub < numLocSub(); ++iSub) {
    if(v[iSub].size() > 0) {
      v[iSub].absMax(allmax[iSub]);
    }
  }
  std::fill(vmax.begin(), vmax.end(), std::numeric_limits<Scalar>::lowest());
  for(int iSub = 0; iSub < numLocSub(); ++iSub) {
    if(v[iSub].size() > 0) {
      for(int iDim = 0; iDim < dim(); iDim++) {
        vmax[iDim] = std::max(vmax[iDim], allmax[iSub][iDim]);
      }
    }
  }
  distInfo.com->globalMax(dim(), vmax.data());
}

//------------------------------------------------------------------------------

template<class Scalar, int Dim>
inline
void
DistSVec<Scalar, Dim>::locSum(std::vector<Scalar>& sumres) const {
  Scalar sum;
  sumres.resize(dim());
  for(int iDim = 0; iDim < dim(); iDim++) {
    sum = 0;
    if(distInfo.masterFlag) {
      #pragma omp parallel for reduction(+: sum)
      for(int iSub = 0; iSub < numLocSub(); ++iSub) {
        const SVec<Scalar, Dim>& vSub = v[iSub];
        const Vec<bool>& masterFlag = (*distInfo.masterFlag)[iSub];
        Scalar locsum = 0;
        for(int i = 0; i < vSub.size(); ++i) {
          if(masterFlag[i]) {
            locsum += vSub(i,iDim);
          }
        }
        sum += locsum;
      }
    }
    else {
      #pragma omp parallel for reduction(+: sum)
      for(int iSub = 0; iSub < numLocSub(); ++iSub) {
        const SVec<Scalar, Dim>& vSub = v[iSub];
        Scalar locsum = 0;
        for(int i = 0; i < vSub.size(); ++i) {
          locsum += vSub(i,iDim);
        }
        sum += locsum;
      }
    }
    sumres[iDim] = sum;
  }
}

//------------------------------------------------------------------------------

template<class Scalar, int Dim>
inline
void
DistSVec<Scalar, Dim>::sum(std::vector<Scalar>& sumres) const {
  this->locSum(sumres);
  distInfo.com->globalSum(dim(), sumres.data());
}

//------------------------------------------------------------------------------

template<class Scalar, int Dim>
inline
void
DistSVec<Scalar, Dim>::locSumSq(std::vector<Scalar>& sumres) const {
  Scalar sum;
  sumres.resize(dim());
  for(int iDim = 0; iDim < dim(); iDim++) {
    sum = 0;
    if(distInfo.masterFlag) {
      #pragma omp parallel for reduction(+: sum)
      for(int iSub = 0; iSub < numLocSub(); ++iSub) {
        const SVec<Scalar, Dim>& vSub = v[iSub];
        const Vec<bool>& masterFlag = (*distInfo.masterFlag)[iSub];
        Scalar locsum = 0;
        for(int i = 0; i < vSub.size(); ++i) {
          if(masterFlag[i]) {
            locsum += vSub(i,iDim) * vSub(i,iDim);
          }
        }
        sum += locsum;
      }
    }
    else {
      #pragma omp parallel for reduction(+: sum)
      for(int iSub = 0; iSub < numLocSub(); ++iSub) {
        const SVec<Scalar, Dim>& vSub = v[iSub];
        Scalar locsum = 0;
        for(int i = 0; i < vSub.size(); ++i) {
          locsum += vSub(i,iDim) * vSub(i,iDim);
        }
        sum += locsum;
      }
    }
    sumres[iDim] = sum;
  }
}

//------------------------------------------------------------------------------

template<class Scalar, int Dim>
inline
void
DistSVec<Scalar, Dim>::sumSq(std::vector<Scalar>& sumres) const {
  this->locSumSq(sumres);
  distInfo.com->globalSum(dim(), sumres.data());
}

//------------------------------------------------------------------------------

template<class Scalar, int Dim>
inline
void
DistSVec<Scalar, Dim>::locAbsSum(std::vector<Scalar>& sumres) const {
  Scalar sum = 0;
  sumres.resize(dim());
  for(int iDim = 0; iDim < dim(); iDim++) {
    sum = 0;
    if(distInfo.masterFlag) {
      #pragma omp parallel for reduction(+: sum)
      for(int iSub = 0; iSub < numLocSub(); ++iSub) {
        const SVec<Scalar, Dim>& vSub = v[iSub];
        const Vec<bool>& masterFlag = (*distInfo.masterFlag)[iSub];
        Scalar locsum = 0;
        for(int i = 0; i < vSub.size(); ++i) {
          if(masterFlag[i]) {
            locsum += std::abs(vSub(i,iDim));
          }
        }
        sum += locsum;
      }
    }
    else {
      #pragma omp parallel for reduction(+: sum)
      for(int iSub = 0; iSub < numLocSub(); ++iSub) {
        const SVec<Scalar, Dim>& vSub = v[iSub];
        Scalar locsum = 0;
        for(int i = 0; i < vSub.size(); ++i) {
          locsum += std::abs(vSub(i,iDim));
        }
        sum += locsum;
      }
    }
    sumres[iDim] = sum;
  }
}

//------------------------------------------------------------------------------

template<class Scalar, int Dim>
inline
void
DistSVec<Scalar, Dim>::absSum(std::vector<Scalar>& sumres) const {
  this->locAbsSum(sumres);
  distInfo.com->globalSum(dim(), sumres.data());
}

//------------------------------------------------------------------------------

template<class Scalar, int Dim>
inline
Scalar
DistSVec<Scalar, Dim>::min() const {
  std::vector<Scalar> vmin(dim());
  this->min(vmin);
  return *std::min_element(vmin.begin(), vmin.end());
}

//------------------------------------------------------------------------------

template<class Scalar, int Dim>
inline
Scalar
DistSVec<Scalar, Dim>::absMin() const {
  std::vector<Scalar> vmin(dim());
  this->absMin(vmin);
  return *std::min_element(vmin.begin(), vmin.end());
}

//------------------------------------------------------------------------------

template<class Scalar, int Dim>
inline
Scalar
DistSVec<Scalar, Dim>::max() const {
  std::vector<Scalar> vmax(dim());
  this->max(vmax);
  return *std::max_element(vmax.begin(), vmax.end());
}

//------------------------------------------------------------------------------

template<class Scalar, int Dim>
inline
Scalar
DistSVec<Scalar, Dim>::absMax() const {
  std::vector<Scalar> vmax(dim());
  this->absMax(vmax);
  return *std::max_element(vmax.begin(), vmax.end());
}

//------------------------------------------------------------------------------

template<class Scalar, int Dim>
inline
Scalar
DistSVec<Scalar, Dim>::locSum() const {
  std::vector<Scalar> sumres(dim());
  this->locSum(sumres);
  return std::accumulate(sumres.begin(), sumres.end(), static_cast<Scalar>(0.0));
}

//------------------------------------------------------------------------------

template<class Scalar, int Dim>
inline
Scalar
DistSVec<Scalar, Dim>::sum() const {
  Scalar res = this->locSum();
  distInfo.com->globalSum(1, &res);
  return res;
}

//------------------------------------------------------------------------------

template<class Scalar, int Dim>
inline
Scalar
DistSVec<Scalar, Dim>::locAbsSum() const {
  std::vector<Scalar> sumres(dim());
  this->locAbsSum(sumres);
  return std::accumulate(sumres.begin(), sumres.end(), static_cast<Scalar>(0.0));
}

//------------------------------------------------------------------------------

template<class Scalar, int Dim>
inline
Scalar
DistSVec<Scalar, Dim>::absSum() const {
  Scalar res = this->locAbsSum();
  distInfo.com->globalSum(1, &res);
  return res;
}

//------------------------------------------------------------------------------

template<class Scalar, int Dim>
inline
typename DistSVec<Scalar, Dim>::RealScalarType
DistSVec<Scalar, Dim>::squaredNorm() const {
  RealScalarType res = 0;
  if(distInfo.masterFlag) {
    #pragma omp parallel for reduction(+: res)
    for(int iSub = 0; iSub < numLocSub(); ++iSub) {
      const SVec<Scalar, Dim>& vSub = v[iSub];
      const Vec<bool>& masterFlag = (*distInfo.masterFlag)[iSub];
      RealScalarType locres = 0;
      for(int i = 0; i < vSub.size(); ++i)
        if(masterFlag[i])
          locres += vSub.row(i).squaredNorm();
      res += locres;
    }
  }
  else {
    #pragma omp parallel for reduction(+: res)
    for(int iSub = 0; iSub < numLocSub(); ++iSub) {
      res += v[iSub].sqNorm();
    }
  }
  distInfo.com->globalSum(1, &res);
  return res;
}

//------------------------------------------------------------------------------

template<class Scalar, int Dim>
inline
typename DistSVec<Scalar, Dim>::RealScalarType
DistSVec<Scalar, Dim>::norm() const {
  RealScalarType res = this->squaredNorm();
  return sqrt(res);
}

//------------------------------------------------------------------------------

template<class Scalar, int Dim>
inline
void
DistSVec<Scalar, Dim>::computeSparsityMask() {
  if(!sparsityMask) { sparsityMask = new DistVec<bool>(distInfo); }
  #pragma omp parallel for
  for(int iSub = 0; iSub < numLocSub(); iSub++) {
    Vec<bool>& locm = (*sparsityMask)(iSub);
    locm = false;
    const SVec<Scalar, Dim>& locU = v[iSub];
    for(int i = 0; i < locU.size(); i++) {
      for(int idim = 0; idim < Dim; idim++) {
        if(locU[i][idim] != 0.0) {
          locm[i] = true;
        }
      }
    }
  }
}

//------------------------------------------------------------------------------

template<class Scalar, int Dim>
inline
void
DistSVec<Scalar, Dim>::setSparsityMask(DistVec<bool> *newMask) {
  if(!sparsityMask) { sparsityMask = new DistVec<bool>(distInfo); }
  *sparsityMask = *newMask;
}

//------------------------------------------------------------------------------

template<class Scalar, int Dim>
inline
void
DistSVec<Scalar, Dim>::applyMaskColumn(const DistVec<int>& m) {
  #pragma omp parallel for
  for(int iSub = 0; iSub < numLocSub(); iSub++) {
    const Vec<int>& locm = m(iSub);
    SVec<Scalar, Dim>& locU = v[iSub];
    for(int i = 0; i < locm.size(); i++) {
      if(!locm[i]) {
        for(int d = 0; d < Dim; d++) {
          locU(i, d) = 0.0;
        }
      }
    }
  }
}

//------------------------------------------------------------------------------

template<class Scalar, int Dim>
inline
void
DistSVec<Scalar, Dim>::applyMaskColumn(const DistVec<bool>& m) {
  #pragma omp parallel for
  for(int iSub = 0; iSub < numLocSub(); iSub++) {
    const Vec<bool>& locm = m(iSub);
    SVec<Scalar, Dim>& locU = v[iSub];
    for(int i = 0; i < locm.size(); i++) {
      if(!locm[i]) {
        for(int d = 0; d < Dim; d++) {
          locU(i, d) = 0.0;
        }
      }
    }
  }
}

//------------------------------------------------------------------------------

template<class Scalar, int Dim>
inline
void
DistSVec<Scalar, Dim>::applyMaskColumn(const DistVec<bool>& m, DistSVec<double, Dim>& ref) {
  #pragma omp parallel for
  for(int iSub = 0; iSub < numLocSub(); iSub++) {
    const Vec<bool>& locm = m(iSub);
    SVec<Scalar, Dim>& locU = v[iSub];
    const SVec<Scalar, Dim>& locRef = ref(iSub);
    for(int i = 0; i < locm.size(); i++) {
      if(!locm[i]) {
        for(int d = 0; d < Dim; d++) {
          locU(i, d) = locRef(i, d);
        }
      }
    }
  }
}

//------------------------------------------------------------------------------

template<class Scalar, int Dim>
inline
void
DistSVec<Scalar, Dim>::applyMaskColumn(const DistVec<int>& m, DistSVec<double, Dim>& ref) {
  #pragma omp parallel for
  for(int iSub = 0; iSub < numLocSub(); iSub++) {
    const Vec<int>& locm = m(iSub);
    SVec<Scalar, Dim>& locU = v[iSub];
    const SVec<Scalar, Dim>& locRef = ref(iSub);
    for(int i = 0; i < locm.size(); i++) {
      if(!locm[i]) {
        for(int d = 0; d < Dim; d++) {
          locU(i, d) = locRef(i, d);
        }
      }
    }
  }
}

//------------------------------------------------------------------------------

template<class Scalar, int Dim>
inline
void
DistSVec<Scalar, Dim>::applyMaskColumn(const DistVec<bool>& m, double *ref) {
  #pragma omp parallel for
  for(int iSub = 0; iSub < numLocSub(); iSub++) {
    const Vec<bool>& locm = m(iSub);
    SVec<Scalar, Dim>& locU = v[iSub];
    for(int i = 0; i < locm.size(); i++) {
      if(!locm[i]) {
        for(int d = 0; d < Dim; d++) {
          locU(i, d) = ref[d];
        }
      }
    }
  }
}

//------------------------------------------------------------------------------

template<class Scalar, int Dim>
inline
void
DistSVec<Scalar, Dim>::undoSparsity() {
  if(sparsityMask) {
    delete sparsityMask;
    sparsityMask = nullptr;
  }
}

//------------------------------------------------------------------------------

#endif

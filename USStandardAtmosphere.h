//
// Created by amcclell on 8/5/19.
//

#ifndef _US_STANDARD_ATMOSPHERE_H
#define _US_STANDARD_ATMOSPHERE_H

#include <cmath>
#include <Communicator.h>

#include <RefVal.h>

struct USStandardAtmosphere {
/******************************************************************************************************************
 * 5 August 2018: Andrew McClellan                                                                                *
 * Parameters used for US Standard Atmosphere (US Standard Atmosphere (1976) National Oceanic and Atmospheric     *
 * Administration (NOAA), National Aeronautics and Space Administration (NASA), United States Air Force,          *
 * Washington DC; see https://ntrs.nasa.gov/archive/nasa/casi.ntrs.nasa.gov/19770009539.pdf )                     *
 *                                                                                                                *
 * Note that this document gives all equations/constants in SI units; implementation will require user to specify *
 * input altitude and output units to have a consistent unit system, and any necessary conversions will be done   *
 * internally                                                                                                     *
 ******************************************************************************************************************
 */

  constexpr static double r0 = 6356766.0; // effective Earth radius at 45 deg North latitude (m)
  constexpr static double g0 = 9.80665;   // sea-level acceleration due to gravity at 45 deg North (m/s^2)
  constexpr static double M0 = 28.9644;   // molecular weight of air (kg/kmol)
  constexpr static double T0 = 288.15;    // sea-level temperature (K)
  constexpr static double Ru = 8.31432e3; // Universal gas constant (J/kmol*K)
  constexpr static double R = Ru/M0;      // Gas constant of air (J/kg*K)
  constexpr static double P0 = 101325.0;  // sea-level pressure (N/m^2)

  const static int numSegments = 7;       // number of segments in piecewise linear temperature function
  constexpr static double Z_max = 86.0e3; // maximum altitude accounted for (actual standard goes to 1000 km) (m)
  constexpr static double Hb_max = r0*Z_max/(r0+Z_max); // maximum geopotential altitude (m)
  const static double L[];  // slope of piecewise linear temperature function segments (K/m)
  const static double Hb[]; // geopotential heights at segment start (m)
  double Pb[numSegments];   // Pressures at segment start (N/m^2)
  double Tmb[numSegments];  // Temperatures at segment start (K)

  constexpr static double g0dR = g0/R;
  double g0dRL[numSegments]; // stores g0/(R*L[i])

  const static int numMdM0Segments = 12; // number of data points in the molecular weight ratio table
  const static double MdM0[]; // ratio of molecular weight to sea-level value altitudes between 80 km and 86 km
  const static double Z[]; // Corresponding altitudes for the above ratios (m)
  double slopeMdM0[numMdM0Segments]; // slopes used for linear interpolation of MdM0 (m^-1)

  // These quantities may not be needed for the boundary conditions, but could be used to overwrite other parameters
  // of the simulation to ensure consistent models
  constexpr static double gam_air = 1.4; // Specific heat ratio of air; if the US Stan. Atm. is used, will assume air everywhere
                                         // (need to set parser to force the gas model to be this way)
  constexpr static double beta = 1.458e-6; // Sutherland's constant for air (AERO-F manual calls this mu_0; units kg/m*s*K^(1/2))
  constexpr static double S = 110.4; // Sutherland reference temperature for air (K)

  // Specific heat at constant pressure
  constexpr static double Cp = gam_air*R/(gam_air-1);

  // Conversion factors for converting given altitude units to meters, and from meters to problem conversion units
  enum Unit {CM = 0, M = 1, KM = 2, IN = 3, FT = 4, MI = 5};
  Unit altitude_unit, problem_unit;
  double altitude_conversion;
  double problem_rho_conversion;
  double problem_P_conversion;
  double problem_T_conversion;
  double problem_Length_conversion;
  double problem_mu0_conversion;
  double problem_R_conversion;

  USStandardAtmosphere();
  ~USStandardAtmosphere() {};

  // Initialize all arrays above
  void initializeUSStandardAtmosphere();

  // Reset conversion factors to desired units
  void resetConversions(Unit, Unit, Communicator *);

  // Given altitude, calculate density, pressure, temperature and speed of sound (all dimensional quantities);
  // altitude should be in meters
  void calculateUSStandardAtmosphere(Communicator *, RefVal&, double, double&, double&, double&, double&, bool = true);
};

#endif //_US_STANDARD_ATMOSPHERE_H

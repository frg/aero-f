#ifndef DYNAMIC_NODALTRANSFER_H_
#define DYNAMIC_NODALTRANSFER_H_

#include <IoData.h>
#include <Vector.h>
#include <DistVector.h>

#include <fstream>
#include <map>
#include <string>
#include <vector>

class Communicator;
class CrackingSurface;
class DistInfo;
class MatchNodeSet;
class StructExc;
class Timer;
struct RotationData;
struct Vec3D;

//------------------------------------------------------------------------------

class EmbeddedStructure {

  friend class DynamicNodalTransfer;
  friend class DistIntersectorPhysBAM;

  Communicator& com;
  Timer *timer;
  StructExc *structExc;

  bool getSurfFromFEM;

  char *meshFile;
  char *restartmeshFile;
  char *matcherFile;

  bool coupled;
  int algNum;
  int sendInitialForce;

  double tScale;
  double XScale;
  double UScale;

  // for forced motion
  bool smoothing_off;
  int mode;
  double dt, tMax;
  double omega;
  double phase_shift;
  double smoothing_factor;
  double dx, dy, dz;
  double alpha_in, alpha_max, x1[3], x2[3], u, v, w, ix, iy, iz;
  double beta_in, beta_max, y1[3], y2[3];

  char *deformMeshFile;
  double (*Xd)[3];    // deformed node coordinates
  double (*dXmax)[3]; // maximum deformation

  double cableLen, xbeg;

  double t0; // starting time.
  int it;

  int nNodes, totalNodes;
  int nElems, totalElems;
  int elemType;
  CrackingSurface *cracking; // activated only if cracking is considered in the structure code.

  // NOTE: the following variables should be dimensional!
  Vec<Vec3D> X0;         // original node coordinates
  Vec<Vec3D> X;          // updated node coordinates
  int (*Tria)[3];        // mesh topology (activated only if the mesh is provided by FEM)
  double dt_tmax[2];

  std::string nodeSetName;
  std::map<int, BoundaryData::Type> boundaryConditionsMap;
  std::vector<int> faceIDs;
  int *surfaceID;
  int *faceID;
  int maxFaceID;

  int *rotOwn;
  std::map<int, RotationData *> *rotationMap;

  DistInfo *di;
  MatchNodeSet **mns;

  void splitQuads(int *, int); // utility function which split quads into triangles
  void getInitialCrack();
  int getNewCracking();
  void getNewCracking(int, int, int);

  IoData& iod;

  double timeStepOffset;

 public:
  EmbeddedStructure(IoData&, Communicator&, Communicator&, Timer *);
  ~EmbeddedStructure();

  static void readMeshFile(IoData&, Communicator *, const char *, double, std::string&,
                           std::map<int, BoundaryData::Type>&, std::vector<int>&,
                           int&, int&, Vec<Vec3D>&, int *&, int (*&)[3],
                           int *&, int&);
  static void readPositionFile(Communicator *, char *, Vec<Vec3D>&);
  static void readVelocityFile(Communicator *, char *, Vec3D *);

  void makeRotationOwnership(IoData&);
  void updateRotation(double time, SVec<double, 3>&, SVec<double, 3>&);

  void sendTimeStep(double&);
  void sendMaxTime(double&);
  void sendInfo(double&, double&);
  void sendInitialPosition(DistSVec<double, 3>&, DistSVec<double, 3>&);
  void sendDisplacement(DistSVec<double, 3>&, DistSVec<double, 3>&, bool = true);
  int sendSubcyclingInfo();
  void processReceivedForce(DistSVec<double, 3>&);
  void sendFluidSuggestedTimestep(double);

  // if embedded mesh provided by FEM
  bool embeddedMeshByFEM() {
    return getSurfFromFEM;
  }

  void writeCrackingData(std::ofstream&) const;
  void readCrackingData(std::ifstream&);

  void setPosition(Vec<Vec3D>&);

};

//------------------------------------------------------------------------------
/*
 * Class to handle communication of nodal forces and displacement with the structure
 */
class DynamicNodalTransfer {
  const double tScale; // scaling factor for time
  int algNum;
  int sendInitialForce;

  Communicator& com;
  Timer *timer;
  EmbeddedStructure structure;
  int structureSubcycling;

  DistSVec<double, 3> X, Udot; // non-dimensionalized (replaces XandUdot)
  DistSVec<double, 3> F; // TODO: needs to be reset by resetOutputToStructure
  double dts, tMax;

  IoData& iod;

 public:
  DynamicNodalTransfer(IoData&, Communicator&, Communicator&, Timer *);
  ~DynamicNodalTransfer();

  int getAlgorithmNumber() {
    return algNum;
  }
  int getSendInitialForce() {
    return sendInitialForce;
  }

  // routine to send the force to the structure. On output, F has been dimensionalized.
  void sendForce();
  // routine to receive the new time step and final time from structure.
  void updateInfo();
  // routine to receive the new cracking of the structure.
  int getNewCracking();
  // routine to receive the displacement of the structure.
  void getDisplacement();
  // routine to receive a flag indicating whether or not the structure solver does subcycling.
  int getStructSubcyclingInfo();
  // send fluid suggested time-step when the structure solver is under subcycling.
  void sendFluidSuggestedTimestep(double);

  void updateOutputToStructure(double, double, SVec<double, 3>&);

  double getStructureTimeStep() {
    return dts;
  }
  double getStructureMaxTime() {
    return tMax;
  }
  int getStructureIteration() {
    return structure.it;
  }
  void setStructureIteration(int it) {
    structure.it = it;
  }
  void setStructurePosition(Vec<Vec3D>& X) {
    structure.setPosition(X);
  }
  bool embeddedMeshByFEM() {
    return structure.embeddedMeshByFEM();
  }
  int numStNodes() {
    return structure.nNodes;
  }
  int numStElems() {
    return structure.nElems;
  }
  int totStNodes() {
    return structure.totalNodes;
  }
  int totStElems() {
    return structure.totalElems;
  }
  bool cracking() {
    return (structure.cracking) ? true : false;
  }
  int structSubcycling() {
    return structureSubcycling;
  }
  double *getStNodes() {
    return reinterpret_cast<double *>(X(0).data());
  }
  double *getStVelocity() {
    return reinterpret_cast<double *>(Udot(0).data());
  }
  int (*getStElems())[3] {
    return structure.Tria;
  }
  int *getSurfaceID() {
    return structure.surfaceID;
  }
  EmbeddedStructure& getEmbeddedStructure() {
    return structure;
  }
  CrackingSurface *getCrackingSurface() {
    return structure.cracking;
  }
  StructExc *getStructExc() {
    return structure.structExc;
  }
  bool isCoupled() const {
    return structure.coupled;
  }

  void writeCrackingData(std::ofstream&) const;
  void readCrackingData(std::ifstream&);

};

//------------------------------------------------------------------------------

#endif

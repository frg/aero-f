#include <FSI/DynamicNodalTransfer.h>

#include <FSI/CrackingSurface.h>
#include <Vector3D.h>
#include <MatchNode.h>
#include <StructExc.h>
#include <Timer.h>
#include <TsRestart.h>

#include <iostream>
#include <cassert>
#include <cmath>
#include <deque>
#include <utility>
#include <list>

#define MAXLINE 500

//------------------------------------------------------------------------------

DynamicNodalTransfer::DynamicNodalTransfer(IoData& iod, Communicator& c, Communicator& sc, Timer *tim)
  : com(c), timer(tim), structure(iod, c, sc, tim), F(*structure.di), X(*structure.di), Udot(*structure.di), iod(iod),
    tScale(iod.ref.rv.time) {
  if(cracking()) {
    /* YYY
        if(strcmp(iod.input.cracking, "") != 0 ||
           iod.input.restart_file_package[0] != 0) {
          char *fn;
          if(iod.input.restart_file_package[0] == 0) {
            int lns = strlen(iod.input.prefix) + strlen(iod.input.cracking) + 1;
            fn = new char[lns];
            sprintf(fn, "%s%s", iod.input.prefix, iod.input.cracking);
          }
          else {
            fn = new char[256];
            char dummy[256];
            int lns = strlen(iod.input.prefix) + strlen(iod.input.restart_file_package) + 1;
            char *fn2 = new char[lns];
            sprintf(fn2, "%s%s", iod.input.prefix, iod.input.restart_file_package);
            TsRestart::readRestartFileNames(fn2, dummy, dummy, dummy,
                                            fn, dummy, dummy, dummy, &c);
            delete [] fn2;
          }
          std::ifstream infile(fn, std::ios::binary);
          readCrackingData(infile);
          delete [] fn;
        }
    */
  }
  algNum = structure.algNum;
  sendInitialForce = structure.sendInitialForce;
  structure.sendTimeStep(dts);
  structure.sendMaxTime(tMax);
  dts /= tScale;
  tMax /= tScale;

  // get structure position
  structure.sendInitialPosition(X, Udot);
  structureSubcycling = (algNum == 22) ? getStructSubcyclingInfo() : 0;
}

//------------------------------------------------------------------------------

DynamicNodalTransfer::~DynamicNodalTransfer() {
}

//------------------------------------------------------------------------------

void DynamicNodalTransfer::sendForce() {
  if(isCoupled()) {
    com.reduce(3 * F(0).size(), reinterpret_cast<double *>(F(0).data()), MPI_SUM, 0);
    structure.processReceivedForce(F);
  }
}

//------------------------------------------------------------------------------

void DynamicNodalTransfer::sendFluidSuggestedTimestep(double dtf0) {
  dtf0 *= tScale;
  structure.sendFluidSuggestedTimestep(dtf0);
}

//------------------------------------------------------------------------------

void DynamicNodalTransfer::updateInfo() {
  structure.sendInfo(dts, tMax);
  dts  /= tScale;
  tMax /= tScale;
}

//------------------------------------------------------------------------------

int DynamicNodalTransfer::getNewCracking() {
  if(!cracking()) {
    com.fprintf(stderr, "*** Warning: Cracking is not considered in the structure simulation!\n");
    return 0;
  }
  return structure.getNewCracking();
}

//------------------------------------------------------------------------------

void DynamicNodalTransfer::getDisplacement() {
  structure.sendDisplacement(X, Udot);
}

//------------------------------------------------------------------------------

int DynamicNodalTransfer::getStructSubcyclingInfo() {
  int subcyc = structure.sendSubcyclingInfo();
  return subcyc;
}

//------------------------------------------------------------------------------

void DynamicNodalTransfer::updateOutputToStructure(double, double, SVec<double, 3>& fs) {
  if(F(0).size() != fs.size()) {
    F(0).resize(fs.size());
  }
  F(0) = fs;
}

//------------------------------------------------------------------------------

void DynamicNodalTransfer::writeCrackingData(std::ofstream& restart_file) const {
  structure.writeCrackingData(restart_file);
}

//------------------------------------------------------------------------------

void DynamicNodalTransfer::readCrackingData(std::ifstream& restart_file) {
  structure.readCrackingData(restart_file);
}

//------------------------------------------------------------------------------

EmbeddedStructure::EmbeddedStructure(IoData& iod, Communicator& comm, Communicator& strCom, Timer *tim)
  : com(comm), iod(iod), timer(tim), tScale(iod.ref.rv.time), XScale(iod.ref.rv.tlength), UScale(iod.ref.rv.tvelocity),
    nNodes(0), nElems(0), elemType(3), cracking(0), X(0), X0(0), Tria(0), surfaceID(0), faceID(0), it(0), structExc(0),
    mns(0), algNum(6), di(0), sendInitialForce(0) {
  // ----------------------------------
  //       User-Provided Info
  // ----------------------------------
  if(iod.problem.type[ProblemData::AERO]) {
    coupled = true;
  }
  else if(iod.problem.type[ProblemData::FORCED]) {
    coupled = false;
  }
  else {
    com.fprintf(stderr, "*** Error: Simulation type is not supported by the embedded framework.\n");
    exit(-1);
  }
  // ---- input files ----
  size_t sp = strlen(iod.input.prefix) + 1;
  meshFile = new char[sp + strlen(iod.input.embeddedSurface)];
  sprintf(meshFile, "%s%s", iod.input.prefix, iod.input.embeddedSurface);
  restartmeshFile = new char[sp + strlen(iod.input.embeddedpositions)];
  if(iod.input.embeddedpositions[0] != 0) {
    sprintf(restartmeshFile, "%s%s", iod.input.prefix, iod.input.embeddedpositions);
  }
  else { // no restart position file provided
    restartmeshFile[0] = '\0';
  }
  matcherFile = new char[sp + strlen(iod.input.match)];
  sprintf(matcherFile, "%s%s", iod.input.prefix, iod.input.match);
  getSurfFromFEM = coupled && (!strlen(iod.input.embeddedSurface));
  if(getSurfFromFEM) {
    com.fprintf(stderr, "- Using the embedded surface provided by structure code.\n");
  }
  // ---- for forced-motion only ------
  if(!coupled) {
    if(iod.forced.type == ForcedData::HEAVING) {
      mode = 1;
    }
    else if(iod.forced.type == ForcedData::PITCHING) {
      mode = 2;
    }
    else if(iod.forced.type == ForcedData::VELOCITY) {
      mode = 3;
    }
    else if(iod.forced.type == ForcedData::DEFORMING) {
      mode = 4;
    }
    else if(iod.forced.type == ForcedData::SPIRALING) {
      mode = 5;
    }
    else if(iod.forced.type == ForcedData::ACOUSTICVISCOUSBEAM) {
      mode = 97;
    }
    else if(iod.forced.type == ForcedData::ACOUSTICBEAM) {
      mode = 98;
    }
    else if(iod.forced.type == ForcedData::PRESCRIBEDDEFORMING) {
      mode = 99;
    }
    else {
      com.fprintf(stderr, "*** Error: Forced motion type is not supported by the embedded framework.\n");
      exit(-1);
    }
  }
  else {
    mode = -1;
  }
  // NOTE: All variables stored in EmbeddedStructure must be dimensional! (unless the simulation itself is non-dim)
  tMax = tScale * iod.ts.maxTime; // iod.ts.maxTime is already non-dimensionalized in IoData
  dt   = tScale * iod.forced.timestep;
  t0   = tScale * iod.restart.etime;
  omega = 2.0 * acos(-1.0) * (1.0 / tScale) * iod.forced.frequency;
  phase_shift = (acos(-1.0) * iod.forced.phase_shift) / 180.0;
  smoothing_off = (iod.forced.smoothing == ForcedData::SMOOTHING_OFF);
  smoothing_factor = iod.forced.smoothing_factor;
  // for heaving
  if(mode != 99) {
    dx = iod.ref.rv.length * iod.forced.hv.ax;
    dy = iod.ref.rv.length * iod.forced.hv.ay;
    dz = iod.ref.rv.length * iod.forced.hv.az;
  }
  else {
    dx = dy = dz = iod.ref.rv.length * iod.forced.pd.amplification;
  }
  // for pitching
  alpha_in  = (acos(-1.0) * iod.forced.pt.alpha_in) / 180.0; // initial angle of rotation
  alpha_max = (acos(-1.0) * iod.forced.pt.alpha_max) / 180.0; // maximum angle of rotation
  x1[0] = iod.forced.pt.x11;
  x1[1] = iod.forced.pt.y11;
  x1[2] = iod.forced.pt.z11;
  x2[0] = iod.forced.pt.x21;
  x2[1] = iod.forced.pt.y21;
  x2[2] = iod.forced.pt.z21;
  beta_in  = (acos(-1.0) * iod.forced.pt.beta_in) / 180.0; // initial angle of rotation
  beta_max = (acos(-1.0) * iod.forced.pt.beta_max) / 180.0; // maximum angle of rotation
  y1[0] = iod.forced.pt.x12;
  y1[1] = iod.forced.pt.y12;
  y1[2] = iod.forced.pt.z12;
  y2[0] = iod.forced.pt.x22;
  y2[1] = iod.forced.pt.y22;
  y2[2] = iod.forced.pt.z22;
  for(int i = 0; i < 3; ++i) { // get back to user-specified coordinates.
    x1[i] *= iod.ref.rv.length;
    x2[i] *= iod.ref.rv.length;
    y1[i] *= iod.ref.rv.length;
    y2[i] *= iod.ref.rv.length;
  }
  u = x2[0] - x1[0];
  v = x2[1] - x1[1];
  w = x2[2] - x1[2];
  // unit normals of axis of rotation
  ix = u / sqrt(u * u + v * v + w * w);
  iy = v / sqrt(u * u + v * v + w * w);
  iz = w / sqrt(u * u + v * v + w * w);
  // for deforming data
  deformMeshFile = new char[strlen(iod.forced.df.positions) + 1];
  if(iod.forced.df.positions[0] != 0) {
    sprintf(deformMeshFile, "%s", iod.forced.df.positions);
  }
  else { // no deforming data position file provided
    deformMeshFile[0] = '\0';
  }
  Xd = 0;
  dXmax = 0;
  // for spiraling
  cableLen = iod.ref.rv.length * iod.forced.sp.xL;
  xbeg = iod.ref.rv.length * iod.forced.sp.x0;
  // ----------------------------------
  //               End
  // ----------------------------------
  // ---------------------------------
  //       F-S Communication
  // ---------------------------------
  if(coupled) {
    if(iod.embed.verbose) { com.barrier(); com.fprintf(stderr, "Initializing embedded structure\n"); }
    mns = new MatchNodeSet *[1];
    if(com.cpuNum() == 0 && !getSurfFromFEM) {
      mns[0] = new MatchNodeSet(matcherFile, MatchNodeSet::EMBEDDED);
    }
    else {
      mns[0] = new MatchNodeSet(MatchNodeSet::EMBEDDED);
    }
    structExc = new StructExc(iod, mns, 6, &strCom, &com, 1);
    if(getSurfFromFEM) { // receive embedded surface from the structure code.
      bool crack;
      int  nStNodes, nStElems, totalStNodes, totalStElems;
      structExc->getEmbeddedWetSurfaceInfo(elemType, crack, nStNodes, nStElems);
      // initialize cracking information
      if(crack) {
        structExc->getInitialCrackingSetup(totalStNodes, totalStElems);
        cracking = new CrackingSurface(elemType, nStElems, totalStElems, nStNodes, totalStNodes);
      }
      else {
        totalStNodes = nStNodes;
        totalStElems = nStElems;
      }
      // allocate memory for the node list
      totalNodes = totalStNodes;
      nNodes     = nStNodes;
      X.resize(totalNodes);
      X0.resize(totalNodes);
      for(int i = 0; i < totalNodes; ++i) {
        X[i][0] = X[i][1] = X[i][2] = X0[i][0] = X0[i][1] = X0[i][2] = 0.0;
      }
      // allocate memory for the element topology list
      int tmpTopo[nStElems][4];
      switch(elemType) {
        case 3: // all triangles
          totalElems = totalStElems;
          nElems     = nStElems;
          Tria = new int[totalElems][3];
          structExc->getEmbeddedWetSurface(nNodes, X0.data(), nElems, (int *)Tria, elemType);
          break;
        case 4: // quadrangles include triangles represented as degenerated quadrangles.
          structExc->getEmbeddedWetSurface(nNodes, X0.data(), nStElems, (int *)tmpTopo, elemType);
          if(cracking) {
            totalElems = totalStElems * 2;
            Tria = new int[totalElems][3];
            nElems = cracking->splitQuads((int *)tmpTopo, nStElems, Tria);
          }
          else {
            splitQuads((int *)tmpTopo, nStElems); // memory for Tria will be allocated
          }
          break;
        default:
          com.fprintf(stderr, "*** Error: Element type (%d) of the wet surface not recognized! Must be 3 or 4.\n", elemType);
          exit(-1);
      }
      for(int i = 0; i < nNodes; i++) {
        for(int j = 0; j < 3; ++j) {
          X[i][j] = X0[i][j];
        }
      }
      if(com.cpuNum() == 0) {
        mns[0]->autoInit(totalNodes); // in case of cracking, match all the nodes including inactive ones.
        structExc->updateMNS(mns);
      }
    }
    structExc->negotiate();
    structExc->getInfo();
    dt = tScale * structExc->getTimeStep();
    tMax = tScale * structExc->getMaxTime();
    algNum = structExc->getAlgorithmNumber();
    sendInitialForce = structExc->getSendInitialForce();
    if(cracking) {
      getInitialCrack();
      cracking->setNewCrackingFlag(false);
    }
    maxFaceID = 0;
    faceID = new int[nElems];
    for(int i = 0; i < nElems; ++i) {
      faceID[i] = 0;
    }
  }
  // ----------------------------------
  //               End
  // ----------------------------------
  // ----------------------------------
  //    Load Structure Mesh (at t=0)
  // ----------------------------------
  if(!getSurfFromFEM) { // otherwise mesh is already loaded
    readMeshFile(iod, &com, meshFile, 1.0, nodeSetName, boundaryConditionsMap, faceIDs,
                 nNodes, nElems, X, surfaceID, Tria, faceID, maxFaceID);
    X0.resize(nNodes);
    X0 = X;
    if(restartmeshFile[0] != 0) {
      readPositionFile(&com, restartmeshFile, X);
    }
    totalNodes = nNodes;
    totalElems = nElems;
    if(mode == 4) { // deforming data
      // load deforming data
      if(deformMeshFile[0] != 0) {
        int nInputs, num1;
        double x1, x2, x3;
        char c1[200];
        FILE *defoTopFile = fopen(deformMeshFile, "r");
        if(defoTopFile == NULL) {
          com.fprintf(stderr, "*** Error: Deforming data file doesn't exist.\n");
          exit(1);
        }
        int ndMax = 0, ndMax2 = 0;
        std::list<std::pair<int, Vec3D>> nodeList2;
        std::list<std::pair<int, Vec3D>>::iterator it2;
        while(1) {
          nInputs = fscanf(defoTopFile, "%s", c1);
          if(nInputs != 1) {
            break;
          }
          char *endptr;
          num1 = (int)strtol(c1, &endptr, 10);
          if(endptr == c1) {
            break;
          }
          int toto = fscanf(defoTopFile, "%lf %lf %lf\n", &x1, &x2, &x3);
          nodeList2.push_back(std::pair<int, Vec3D>(num1, Vec3D(x1, x2, x3)));
          ndMax = std::max(num1, ndMax);
        }
        if(ndMax != totalNodes) {
          com.fprintf(stderr, "*** Error: number of nodes in Deforming data file is wrong. %d vs %d \n", ndMax, totalNodes);
          exit(1);
        }
        Xd = new double[totalNodes][3];
        dXmax = new double[totalNodes][3];
        for(int i = 0; i < totalNodes; i++) {
          Xd[i][0] = Xd[i][1] = Xd[i][2] = 0.0;
        }
        for(it2 = nodeList2.begin(); it2 != nodeList2.end(); it2++) {
          Xd[it2->first - 1][0] = (it2->second)[0];
          Xd[it2->first - 1][1] = (it2->second)[1];
          Xd[it2->first - 1][2] = (it2->second)[2];
          dXmax[it2->first - 1][0] = iod.forced.df.amplification * (Xd[it2->first - 1][0] - X0[it2->first - 1][0]);
          dXmax[it2->first - 1][1] = iod.forced.df.amplification * (Xd[it2->first - 1][1] - X0[it2->first - 1][1]);
          dXmax[it2->first - 1][2] = iod.forced.df.amplification * (Xd[it2->first - 1][2] - X0[it2->first - 1][2]);
        }
        fclose(defoTopFile);
      }
    }
  }
  makeRotationOwnership(iod);
  // ----------------------------------
  //               End
  // ----------------------------------
  // prepare distinfo for struct nodes
  di = new DistInfo(1, &com);
  di->setLen(0, totalNodes);
  di->finalize(false);

  timeStepOffset = iod.forced.tsoffset;
}

//------------------------------------------------------------------------------

void
EmbeddedStructure::readMeshFile(IoData& iod, Communicator *com, const char *fileName, double XScale, std::string& nodeSetName,
                                std::map<int, BoundaryData::Type>& boundaryConditionsMap, std::vector<int>& faceIDs,
                                int& numStNodes, int& numStElems, Vec<Vec3D>& Xs, int *&surfaceID, int (*&stElem)[3],
                                int *&faceID, int& maxFaceID) {
  // read data from the surface mesh input file
  FILE *topFile = fopen(fileName, "r");
  if(topFile == NULL) {
    com->fprintf(stderr, "*** Error: surface mesh file %s doesn't exist\n", fileName);
    exit(1);
  }
  char line[MAXLINE], key1[MAXLINE], key2[MAXLINE], copyForType[MAXLINE];
  // load the nodes and initialize all node-based variables
  // load solid nodes at t=0
  int num0 = 0;
  int num1 = 0;
  double x1, x2, x3;
  int node1, node2, node3;
  int type_read = 0;
  int surfaceid = 0;
  std::deque<std::pair<int, Vec3D>> nodeList;
  std::deque<std::array<int, 4>> elemList; // surface ID + three node IDs
  int maxIndex = 0, maxElem = -1;
  if(iod.embed.verbose) { com->barrier(); com->fprintf(stderr, "Reading surface mesh from \'%s\'\n", fileName); }
  while(fgets(line, MAXLINE, topFile) != 0) {
    sscanf(line, "%s", key1);
    bool skip = false;
    if(strcmp(key1, "Nodes") == 0) {
      sscanf(line, "%*s %s", key2);
      nodeSetName = std::string(key2);
      skip = true;
      type_read = 1;
    }
    else if(strcmp(key1, "Elements") == 0) {
      sscanf(line, "%*s %s", key2);
      skip = true;
      type_read = 2;
      int underscore_pos = -1;
      int k = 0;
      while((key2[k] != '\0') && (k < MAXLINE)) {
        if(key2[k] == '_') {
          underscore_pos = k;
        }
        k++;
      }
      if(underscore_pos > -1) {
        sscanf(key2 + (underscore_pos + 1), "%d", &surfaceid);
        // now we look for keywords for the type of structure
        strcpy(copyForType, key2);
        int l = 0;
        while((copyForType[l] != '\0') && (l < MAXLINE)) {
          copyForType[l] = (char)std::tolower(static_cast<unsigned char>(copyForType[l]));
          l++;
        }
        // read the name of the file and detects keyword for type
        if(strstr(copyForType, "symmetry") != NULL) {
          if(boundaryConditionsMap.count(surfaceid) != 0) {
            com->fprintf(stderr, "*** Error: two surfaces in %s have the same id (%d)\n", fileName, surfaceid);
            exit(1);
          }
          boundaryConditionsMap[surfaceid] = BoundaryData::SYMMETRYPLANE;
        }
        else if(strstr(copyForType, "porouswall") != NULL) {
          if(boundaryConditionsMap.count(surfaceid) != 0) {
            com->fprintf(stderr, "*** Error: two surfaces in %s have the same id (%d)\n", fileName, surfaceid);
            exit(1);
          }
          boundaryConditionsMap[surfaceid] = BoundaryData::POROUSWALL;
        }
        else if(strstr(copyForType, "actuatordisk") != NULL) {
          if(boundaryConditionsMap.count(surfaceid) != 0) {
            com->fprintf(stderr, "*** Error: two surfaces in %s have the same id (%d)\n", fileName, surfaceid);
            exit(1);
          }
          boundaryConditionsMap[surfaceid] = BoundaryData::ACTUATORDISK;
        }
        else if(strstr(copyForType, "outlet") != NULL) {
          if(boundaryConditionsMap.count(surfaceid) != 0) {
            com->fprintf(stderr, "*** Error: two surfaces in %s have the same id (%d)\n", fileName, surfaceid);
            exit(1);
          }
          boundaryConditionsMap[surfaceid] = BoundaryData::OUTLET;
        }
        else {
          faceIDs.push_back(surfaceid);
        }
      }
    }
    if(!skip) { // we are reading a node or an element (not a header)
      if(type_read == 1) {
        sscanf(line, "%d %lf %lf %lf", &num1, &x1, &x2, &x3);
        if(num1 < 1) {
          com->fprintf(stderr, "*** Error: detected a node with index %d in the surface mesh file %s\n", num1, fileName);
          exit(-1);
        }
        x1 /= XScale;
        x2 /= XScale;
        x3 /= XScale;
        if(num1 > maxIndex) {
          maxIndex = num1;
        }
        nodeList.push_back({num1, {x1, x2, x3}});
      }
      if(type_read == 2) { // we are reading an element
        sscanf(line, "%d %d %d %d %d", &num0, &num1, &node1, &node2, &node3);
        elemList.push_back({surfaceid, node1 - 1, node2 - 1, node3 - 1});
      }
    }
  }
  numStNodes = int(nodeList.size());
  if(numStNodes != maxIndex) {
    com->fprintf(stderr, "*** Error: the node set of the surface mesh in %s has gaps: max index = %d, number of nodes = %d",
                 fileName, maxIndex, numStNodes);
    exit(-1);
  }
  numStElems = int(elemList.size());
  if(surfaceID) delete [] surfaceID;
  if(stElem) delete [] stElem;
  if(faceID) delete [] faceID;
  // feed data to Xs
  Xs.resize(numStNodes);
  surfaceID = new int[numStNodes];
  for(auto it1 = nodeList.begin(); it1 != nodeList.end(); it1++) {
    Xs[it1->first - 1][0] = it1->second[0];
    Xs[it1->first - 1][1] = it1->second[1];
    Xs[it1->first - 1][2] = it1->second[2];
    surfaceID[it1->first - 1] = 0;
  }
  nodeList.clear();
  stElem = new int[numStElems][3];
  faceID = new int[numStElems];
  maxFaceID = 0;
  auto it2 = elemList.begin();
  for(int i = 0; i < numStElems; i++) {
    stElem[i][0] = (*it2)[1];
    stElem[i][1] = (*it2)[2];
    stElem[i][2] = (*it2)[3];
    faceID[i] = (*it2)[0]; // give the face the ID of the element
    maxFaceID = std::max(maxFaceID, faceID[i]);
    surfaceID[(*it2)[1]] = (*it2)[0]; // gives every node the ID of the element
    surfaceID[(*it2)[2]] = (*it2)[0];
    surfaceID[(*it2)[3]] = (*it2)[0];
    it2++;
  }
  elemList.clear();
  fclose(topFile);
}

//------------------------------------------------------------------------------

void
EmbeddedStructure::readPositionFile(Communicator *com, char *restartSolidSurface, Vec<Vec3D>& Xs) {
  FILE *resTopFile = fopen(restartSolidSurface, "r");
  if(resTopFile == NULL) {
    com->fprintf(stderr, "*** Error: restart embedded position file doesn't exist: \"%s\".\n", restartSolidSurface);
    exit(1);
  }
  char c1[200];
  int num1;
  double x1, x2, x3;
  while(1) {
    int nInputs = fscanf(resTopFile, "%s", c1);
    if(nInputs != 1) {
      break;
    }
    char *endptr;
    num1 = (int)strtol(c1, &endptr, 10);
    if(endptr == c1) {
      break;
    }
    if(num1 > Xs.size()) {
      com->fprintf(stderr, "*** Error: number of nodes in restart embedded position file is wrong; ndMax = %d; numStNodes = %d\n", num1, Xs.size());
      exit(1);
    }
    int toto = fscanf(resTopFile, "%lf %lf %lf\n", &x1, &x2, &x3);
    Xs[num1 - 1][0] = x1;
    Xs[num1 - 1][1] = x2;
    Xs[num1 - 1][2] = x3;
  }
  fclose(resTopFile);
  com->printf(0, "Read embedded surface position from \'%s\'\n", restartSolidSurface);
}

//------------------------------------------------------------------------------

void
EmbeddedStructure::readVelocityFile(Communicator *com, char *restartSolidSurface, Vec3D *Xdot) {
  FILE *resTopFile = fopen(restartSolidSurface, "r");
  if(resTopFile == NULL) {
    com->fprintf(stderr, "*** Error: restart embedded velocity file doesn't exist: \"%s\".\n", restartSolidSurface);
    exit(1);
  }
  char c1[200];
  int num1;
  double v1, v2, v3;
  while(1) {
    int nInputs = fscanf(resTopFile, "%s", c1);
    if(nInputs != 1) {
      break;
    }
    char *endptr;
    num1 = (int)strtol(c1, &endptr, 10);
    if(endptr == c1) {
      break;
    }
    int toto = fscanf(resTopFile, "%lf %lf %lf\n", &v1, &v2, &v3);
    Xdot[num1 - 1][0] = v1;
    Xdot[num1 - 1][1] = v2;
    Xdot[num1 - 1][2] = v3;
  }
  fclose(resTopFile);
  com->printf(0, "Read embedded surface velocity from \'%s\'\n", restartSolidSurface);
}

//------------------------------------------------------------------------------

EmbeddedStructure::~EmbeddedStructure() {
  if(Xd) {
    delete[] Xd;
  }
  if(dXmax) {
    delete[] dXmax;
  }
  if(Tria) {
    delete[] Tria;
  }
  if(faceID) {
    delete[] faceID;
  }
  if(surfaceID) {
    delete[] surfaceID;
  }
  if(rotOwn) {
    delete[] rotOwn;
  }
  if(structExc) {
    delete structExc;
  }
  if(mns) {
    delete mns[0];
    delete[] mns;
  }
  delete[] meshFile;
  if(restartmeshFile) {
    delete[] restartmeshFile;
  }
  if(deformMeshFile) {
    delete[] deformMeshFile;
  }
  delete[] matcherFile;
  if(di) {
    delete di;
  }
}

//------------------------------------------------------------------------------

void EmbeddedStructure::makeRotationOwnership(IoData& iod) {
  std::map<int, SurfaceData *>& surfaceMap = iod.surfaces.surfaceMap.dataMap;
  std::map<int, SurfaceData *>::iterator it = surfaceMap.begin();
  rotationMap = &(iod.forced.vel.rotationMap.dataMap);
  rotOwn = 0;
  int numRotSurfs = 0;
  int numTransWalls = 0;
  while(it != surfaceMap.end()) {
    std::map<int, RotationData *>::iterator it1 = rotationMap->find(it->second->forceID);
    // ForceID is "ID number of a prescribed velocity data-set including the
    // rotation or translation axis, the center of rotation, and the angular
    // or translational velocity"
    if(it1 != rotationMap->end()) { // a surface with this rotation ID exist
      if(it1->second->infRadius) { // translation
        numTransWalls++;
        com.fprintf(stderr, " ... surface %2d is ``translating''\n", it->first, it1->first);
        com.fprintf(stderr, "     -> uniform velocity V = %3.2e in direction %3.2e %3.2e %3.2e\n",
                    it1->second->omega, it1->second->nx, it1->second->ny, it1->second->nz);
      }
      else { // rotation
        numRotSurfs++;
        com.fprintf(stderr, " ... surface %2d is ``rotating'' in DynamicNodalTransfer using rotation data %2d\n", it->first, it1->first);
        com.fprintf(stderr, "     -> omega = %3.2e, rotation axis = %3.2e %3.2e %3.2e\n",
                    it1->second->omega, it1->second->nx, it1->second->ny, it1->second->nz);
      }
    }
    it++;
  }
  if(surfaceID && (numRotSurfs || numTransWalls)) {
    rotOwn = new int[nNodes];
    for(int k = 0; k < nNodes; k++) {
      rotOwn[k] = -1;
      std::map<int, SurfaceData *>::iterator it = surfaceMap.find(surfaceID[k]);
      if(it != surfaceMap.end()) {
        int rotID = it->second->forceID; // = -1 if not defined in input file
        if((rotOwn[k] != -1 && rotOwn[k] != rotID) ||
           (rotOwn[k] != -1 && rotOwn[k] != rotID) ||
           (rotOwn[k] != -1 && rotOwn[k] != rotID)) {
          com.fprintf(stderr, "*** Warning: Embedded Node %d associated to more than 1 Rotation ID\n", k);
        }
        rotOwn[k] = rotID;
        rotOwn[k] = rotID;
        rotOwn[k] = rotID;
      }
    }
  }
}

//------------------------------------------------------------------------------

void EmbeddedStructure::updateRotation(double time, SVec<double, 3>& U, SVec<double, 3>& Udot) {
  if(rotOwn) {
    for(int k = 0; k < nNodes; k++) {
      if(rotOwn[k] >= 0) { // node belongs to a (potential) "rotating" surface
        std::map<int, RotationData *>::iterator it = rotationMap->find(rotOwn[k]);
        if(it != rotationMap->end()) { // the rotation data have been defined
          if(it->second->infRadius == RotationData::TRUE) {
            double vel = it->second->omega;
            U[k][0] = vel * it->second->nx * time;
            U[k][1] = vel * it->second->ny * time;
            U[k][2] = vel * it->second->nz * time;
            Udot[k][0] = vel * it->second->nx;
            Udot[k][1] = vel * it->second->ny;
            Udot[k][2] = vel * it->second->nz;
          }
          else {
            double xd = X0[k][0] - it->second->x0;
            double yd = X0[k][1] - it->second->y0;
            double zd = X0[k][2] - it->second->z0;
            double arg = -smoothing_factor * it->second->omega * it->second->omega * time * time;
            double s = (smoothing_off || arg < -30) ? 1.0 : (1.0 - exp(-smoothing_factor * it->second->omega * it->second->omega * time * time));
            double theta = s * it->second->omega * time;
            double costheta = cos(theta);
            double sintheta = sin(theta);
            double ix = it->second->nx;
            double iy = it->second->ny;
            double iz = it->second->nz;
            double dx[3] = {0., 0., 0.};
            dx[0] += (costheta + (1 - costheta) * ix * ix) * xd;
            dx[0] += ((1 - costheta) * ix * iy - iz * sintheta) * yd;
            dx[0] += ((1 - costheta) * ix * iz + iy * sintheta) * zd;
            dx[1] += ((1 - costheta) * ix * iy + iz * sintheta) * xd;
            dx[1] += (costheta + (1 - costheta) * iy * iy) * yd;
            dx[1] += ((1 - costheta) * iy * iz - ix * sintheta) * zd;
            dx[2] += ((1 - costheta) * ix * iz - iy * sintheta) * xd;
            dx[2] += ((1 - costheta) * iy * iz + ix * sintheta) * yd;
            dx[2] += (costheta + (1 - costheta) * iz * iz) * zd;
            dx[0] += it->second->x0;
            dx[1] += it->second->y0;
            dx[2] += it->second->z0;
            U[k][0] = dx[0] - X0[k][0];
            U[k][1] = dx[1] - X0[k][1];
            U[k][2] = dx[2] - X0[k][2];
            double ox = it->second->omega * it->second->nx;
            double oy = it->second->omega * it->second->ny;
            double oz = it->second->omega * it->second->nz;
            xd = dx[0] - it->second->x0;
            yd = dx[1] - it->second->y0;
            zd = dx[2] - it->second->z0;
            Udot[k][0] = oy * zd - oz * yd;
            Udot[k][1] = oz * xd - ox * zd;
            Udot[k][2] = ox * yd - oy * xd;
          }
        }
        else { // no rotation data
          U[k][0] = 0.;
          U[k][1] = 0.;
          U[k][2] = 0.;
          Udot[k][0] = 0.;
          Udot[k][1] = 0.;
          Udot[k][2] = 0.;
        }
      }
      else { // no rotation data
        U[k][0] = 0.;
        U[k][1] = 0.;
        U[k][2] = 0.;
        Udot[k][0] = 0.;
        Udot[k][1] = 0.;
        Udot[k][2] = 0.;
      }
    }
  }
}

//------------------------------------------------------------------------------

void EmbeddedStructure::sendTimeStep(double& _dt) {
  com.broadcast(1, &dt, 0);
  _dt = dt;
}

//------------------------------------------------------------------------------

void EmbeddedStructure::sendMaxTime(double& _tMax) {
  com.broadcast(1, &tMax, 0);
  _tMax = tMax;
}

//------------------------------------------------------------------------------

void EmbeddedStructure::sendInfo(double& _dt, double& _tMax) {
  if(coupled) {
    structExc->getInfo();
  }
  if(com.cpuNum() == 0) {
    if(coupled) {
      dt = tScale * structExc->getTimeStep();
      tMax = tScale * structExc->getMaxTime();
    }
    dt_tmax[0] = dt;
    dt_tmax[1] = tMax;
  }
  com.broadcast(2, dt_tmax, 0);
  _dt   = dt_tmax[0];
  _tMax = dt_tmax[1];
}

//------------------------------------------------------------------------------

void EmbeddedStructure::sendInitialPosition(DistSVec<double, 3>& x, DistSVec<double, 3>& udot) {
  if(com.cpuNum() == 0) {
    for(int i = 0; i < nNodes; ++i) {
      for(int j = 0; j < 3; ++j) {
        x(0)[i][j] = X[i][j] / XScale;
        udot(0)[i][j] = 0;
      }
    }
  }
  com.broadcast(3 * nNodes, reinterpret_cast<double *>(x(0).data()), 0);
  com.broadcast(3 * nNodes, reinterpret_cast<double *>(udot(0).data()), 0);
}

//------------------------------------------------------------------------------

void EmbeddedStructure::sendDisplacement(DistSVec<double, 3>& x, DistSVec<double, 3>& udot, bool calculatePrevious) {
  if(coupled) {
    structExc->getTotDisplacement(udot, x); // note: x temporarily stores displacement
    if(com.cpuNum() == 0) {
      for(int i = 0; i < nNodes; i++) {
        for(int j = 0; j < 3; ++j) {
          X[i][j] = X0[i][j] + XScale * x(0)[i][j];
          x(0)[i][j] = X[i][j] / XScale; // x now stores updated position
        }
      }
    }
  }
  if(com.cpuNum() == 0) {
    it++;
    if(!coupled) {
      SVec<double, 3>& U = x(0);
      SVec<double, 3>& Udot = udot(0);
      SVec<double, 3> Uold = U;
      double time;
      time = t0 + dt * ((double)it + timeStepOffset);
      if(mode == 1) { // heaving
        double& t = time;
        double arg = -smoothing_factor * omega * omega * t * t;
        double sintheta = (smoothing_off || arg < -30) ? sin(omega * t + phase_shift) : (1.0 - exp(arg)) * sin(omega * t + phase_shift);
        double dsintheta_dt = (smoothing_off || arg < -30) ? omega * cos(omega * t + phase_shift) : omega*(1.0 - exp(arg)) * cos(omega * t + phase_shift)
                              + (2 * smoothing_factor * omega * omega * t * exp(arg)) * sin(omega * t + phase_shift);
        for(int i = 0; i < nNodes; ++i) {
          U[i][0] = dx * sintheta;
          U[i][1] = dy * sintheta;
          U[i][2] = dz * sintheta;
          Udot[i][0] = dx * dsintheta_dt;
          Udot[i][1] = dy * dsintheta_dt;
          Udot[i][2] = dz * dsintheta_dt;
        }
      }
      else if(mode == 2) { // pitching
        auto disp_step = [&]() {
          double arg = -smoothing_factor * omega * omega * time * time;
          double s = (smoothing_off || arg < -30) ? 1.0 : (1.0 - exp(-smoothing_factor * omega * omega * time * time));
          double theta = alpha_in + alpha_max * s * sin(omega * time + phase_shift);
          double costheta = cos(theta);
          double sintheta = sin(theta);
          double phi = beta_in + beta_max * s * sin(omega * time + phase_shift);
          double cosphi = cos(phi);
          double sinphi = sin(phi);
          // rotate the axis of 2nd rotation about the first rotation axis
          double p[3], yy1[3], yy2[3];
          p[0] = y1[0] - x1[0];
          p[1] = y1[1] - x1[1];
          p[2] = y1[2] - x1[2];
          yy1[0] = 0.0;
          yy1[1] = 0.0;
          yy1[2] = 0.0;
          yy1[0] += (costheta + (1 - costheta) * ix * ix) * p[0];
          yy1[0] += ((1 - costheta) * ix * iy - iz * sintheta) * p[1];
          yy1[0] += ((1 - costheta) * ix * iz + iy * sintheta) * p[2];
          yy1[1] += ((1 - costheta) * ix * iy + iz * sintheta) * p[0];
          yy1[1] += (costheta + (1 - costheta) * iy * iy) * p[1];
          yy1[1] += ((1 - costheta) * iy * iz - ix * sintheta) * p[2];
          yy1[2] += ((1 - costheta) * ix * iz - iy * sintheta) * p[0];
          yy1[2] += ((1 - costheta) * iy * iz + ix * sintheta) * p[1];
          yy1[2] += (costheta + (1 - costheta) * iz * iz) * p[2];
          yy1[0] += x1[0];
          yy1[1] += x1[1];
          yy1[2] += x1[2];
          p[0] = y2[0] - x1[0];
          p[1] = y2[1] - x1[1];
          p[2] = y2[2] - x1[2];
          yy2[0] = 0.0;
          yy2[1] = 0.0;
          yy2[2] = 0.0;
          yy2[0] += (costheta + (1 - costheta) * ix * ix) * p[0];
          yy2[0] += ((1 - costheta) * ix * iy - iz * sintheta) * p[1];
          yy2[0] += ((1 - costheta) * ix * iz + iy * sintheta) * p[2];
          yy2[1] += ((1 - costheta) * ix * iy + iz * sintheta) * p[0];
          yy2[1] += (costheta + (1 - costheta) * iy * iy) * p[1];
          yy2[1] += ((1 - costheta) * iy * iz - ix * sintheta) * p[2];
          yy2[2] += ((1 - costheta) * ix * iz - iy * sintheta) * p[0];
          yy2[2] += ((1 - costheta) * iy * iz + ix * sintheta) * p[1];
          yy2[2] += (costheta + (1 - costheta) * iz * iz) * p[2];
          yy2[0] += x1[0];
          yy2[1] += x1[1];
          yy2[2] += x1[2];
          // unit normals of axis of 2nd rotation
          u = yy2[0] - yy1[0];
          v = yy2[1] - yy1[1];
          w = yy2[2] - yy1[2];
          double jx = u / sqrt(u * u + v * v + w * w);
          double jy = v / sqrt(u * u + v * v + w * w);
          double jz = w / sqrt(u * u + v * v + w * w);
          for(int i = 0; i < nNodes; ++i) {
            p[0] = X0[i][0] - x1[0];
            p[1] = X0[i][1] - x1[1];
            p[2] = X0[i][2] - x1[2];
            U[i][0] = 0.0;
            U[i][1] = 0.0;
            U[i][2] = 0.0;
            U[i][0] += (costheta + (1 - costheta) * ix * ix) * p[0];
            U[i][0] += ((1 - costheta) * ix * iy - iz * sintheta) * p[1];
            U[i][0] += ((1 - costheta) * ix * iz + iy * sintheta) * p[2];
            U[i][1] += ((1 - costheta) * ix * iy + iz * sintheta) * p[0];
            U[i][1] += (costheta + (1 - costheta) * iy * iy) * p[1];
            U[i][1] += ((1 - costheta) * iy * iz - ix * sintheta) * p[2];
            U[i][2] += ((1 - costheta) * ix * iz - iy * sintheta) * p[0];
            U[i][2] += ((1 - costheta) * iy * iz + ix * sintheta) * p[1];
            U[i][2] += (costheta + (1 - costheta) * iz * iz) * p[2];
            U[i][0] += x1[0];
            U[i][1] += x1[1];
            U[i][2] += x1[2];
            p[0] = U[i][0] -  yy1[0];
            p[1] = U[i][1] -  yy1[1];
            p[2] = U[i][2] -  yy1[2];
            U[i][0] = 0.0;
            U[i][1] = 0.0;
            U[i][2] = 0.0;
            U[i][0] += (cosphi + (1 - cosphi) * jx * jx) * p[0];
            U[i][0] += ((1 - cosphi) * jx * jy - jz * sinphi) * p[1];
            U[i][0] += ((1 - cosphi) * jx * jz + jy * sinphi) * p[2];
            U[i][1] += ((1 - cosphi) * jx * jy + jz * sinphi) * p[0];
            U[i][1] += (cosphi + (1 - cosphi) * jy * jy) * p[1];
            U[i][1] += ((1 - cosphi) * jy * jz - jx * sinphi) * p[2];
            U[i][2] += ((1 - cosphi) * jx * jz - jy * sinphi) * p[0];
            U[i][2] += ((1 - cosphi) * jy * jz + jx * sinphi) * p[1];
            U[i][2] += (cosphi + (1 - cosphi) * jz * jz) * p[2];
            U[i][0] += yy1[0];
            U[i][1] += yy1[1];
            U[i][2] += yy1[2];
          }
        };
        if(calculatePrevious) {
          time -= dt;
          disp_step();
          Uold = U;
          time += dt;
        }
        disp_step();
        for(int i = 0; i < nNodes; ++i) {
          for(int j = 0; j < 3; ++j) {
            if(calculatePrevious) {
              Udot[i][j] = (U[i][j] - Uold[i][j]) / dt;
            }
            else {
              Udot[i][j] = (U[i][j] - X[i][j]) / dt;
            }
          }
          for(int j = 0; j < 3; ++j) {
            U[i][j] -= X0[i][j];
          }
        }
      }
      else if(mode == 3) { // heaving with a constant velocity (in this case dx dy dz are velocity
        updateRotation(time, U, Udot);
      }
      else if(mode == 4) { // deforming data
        double& t = time;
        double arg = -smoothing_factor * omega * omega * t * t;
        double sintheta = (smoothing_off || arg < -30) ? sin(omega * t + phase_shift) : (1.0 - exp(arg)) * sin(omega * t + phase_shift);
        double dsintheta_dt = (smoothing_off || arg < -30) ? omega * cos(omega * t + phase_shift) : omega*(1.0 - exp(arg)) * cos(omega * t + phase_shift)
                              + (2 * smoothing_factor * omega * omega * t * exp(arg)) * sin(omega * t + phase_shift);
        for(int i = 0; i < nNodes; ++i) {
          for(int j = 0; j < 3; ++j) {
            U[i][j] = sintheta * dXmax[i][j];
            Udot[i][j] = dsintheta_dt * dXmax[i][j];
          }
        }
      }
      else if(mode == 5) { // spiraling data
        double Rcurv = cableLen / (omega * time);
        for(int i = 0; i < nNodes; ++i) {
          if(X0[i][0] >= xbeg) {
            double xloc = X0[i][0] - xbeg;
            double theta = xloc / Rcurv;
            U[i][0] = (Rcurv + X0[i][1]) * sin(theta) + xbeg;
            U[i][1] = (Rcurv + X0[i][1]) * cos(theta) - Rcurv;
            U[i][2] = X0[i][2];
          }
          else {
            U[i][0] = X0[i][0];
            U[i][1] = X0[i][1];
            U[i][2] = X0[i][2];
          }
          for(int j = 0; j < 3; ++j) {
            Udot[i][j] = (U[i][j] - X[i][j]) / dt;
          }
          for(int j = 0; j < 3; ++j) {
            U[i][j] -= X0[i][j];
          }
        }
      }
      else if(mode == 99) { // prescribed deforming
        double& t = time;
        auto sign = [](double x) { return (double(0) < x) - (x < double(0));};
        if(iod.forced.pd.extrudedirection == PrescribedDeformingData::Y) {
          /* Circle centered at (0.,0.,0.),
           * extruded in y direction. */
          auto g = [](double x, double z) { return (fabs(x) + fabs(z) - sqrt(x*x + z*z))/2.0; };

          for(int i = 0; i < nNodes; ++i) {
            double g_value = g(X0[i][0], X0[i][2]);
            double  u_amp  = g_value * (1.0 - cos(omega * t));
            double du_amp  = g_value * (omega * sin(omega * t));
            double n_x = -sign(X0[i][0]), n_z = -sign(X0[i][2]);
            U[i][0] = u_amp*n_x;
            U[i][1] = 0.0;
            U[i][2] = u_amp*n_z;
            Udot[i][0] = du_amp*n_x;
            Udot[i][1] = 0.0;
            Udot[i][2] = du_amp*n_z;
          }
        }
        else if(iod.forced.pd.extrudedirection == PrescribedDeformingData::Z) {
          /* Circle centered at (0.,0.,0.),
           * extruded in z direction. */
          auto g = [](double x, double y) { return (fabs(x) + fabs(y) - sqrt(x*x + y*y))/2.0; };

          for(int i = 0; i < nNodes; ++i) {
            double g_value = g(X0[i][0], X0[i][1]);
            double  u_amp  = g_value * (1.0 - cos(omega * t));
            double du_amp  = g_value * (omega * sin(omega * t));
            double n_x = -sign(X0[i][0]), n_y = -sign(X0[i][1]);
            U[i][0] = u_amp*n_x;
            U[i][1] = u_amp*n_y;
            U[i][2] = 0.0;
            Udot[i][0] = du_amp*n_x;
            Udot[i][1] = du_amp*n_y;
            Udot[i][2] = 0.0;
          }
        }
        else {
          com.fprintf(stderr, "*** Error: Prescribed Deforming extrude direction not supported.\n");
        }
      }
      for(int i = 0; i < nNodes; i++) {
        for(int j = 0; j < 3; ++j) {
          X[i][j] = X0[i][j] + U[i][j];
          x(0)[i][j] = X[i][j] / XScale;
          udot(0)[i][j] = Udot[i][j] / UScale;
        }
      }
    }
  }
  com.broadcast(3 * nNodes, reinterpret_cast<double *>(x(0).data()), 0);
  com.broadcast(3 * nNodes, reinterpret_cast<double *>(udot(0).data()), 0);
}

//------------------------------------------------------------------------------

void EmbeddedStructure::setPosition(Vec<Vec3D>& _X) {
  for(int i = 0; i < nNodes; ++i) {
    for(int j = 0; j < 3; ++j) {
      X[i][j] = _X[i][j];
    }
  }
}

//------------------------------------------------------------------------------

int EmbeddedStructure::sendSubcyclingInfo() {
  return structExc->getSubcyclingInfo();
}

//------------------------------------------------------------------------------

void EmbeddedStructure::processReceivedForce(DistSVec<double, 3>& f) {
  if(coupled) {
    structExc->sendForce(f);
  }
}

//------------------------------------------------------------------------------

void EmbeddedStructure::sendFluidSuggestedTimestep(double dtf0) {
  structExc->sendFluidSuggestedTimestep(dtf0);
}

//------------------------------------------------------------------------------

void EmbeddedStructure::splitQuads(int *quads, int nStElems) {
  int nTrias = 0;
  for(int i = 0; i < nStElems; ++i) {
    if(quads[i * 4 + 2] == quads[i * 4 + 3]) {
      nTrias += 1;
    }
    else {
      nTrias += 2;
    }
  }
  Tria = new int[nTrias][3];
  int count = 0;
  for(int i = 0; i < nStElems; ++i) {
    Tria[count][0] = quads[i * 4];
    Tria[count][1] = quads[i * 4 + 1];
    Tria[count][2] = quads[i * 4 + 2];
    count++;
    if(quads[i * 4 + 2] == quads[i * 4 + 3]) {
      continue;
    }
    Tria[count][0] = quads[i * 4];
    Tria[count][1] = quads[i * 4 + 2];
    Tria[count][2] = quads[i * 4 + 3];
    count++;
  }
  if(count != nTrias) {
    com.fprintf(stderr, "Software bug in FSI/EmbeddedStructure/splitQuad!\n");
    exit(-1);
  }
  nElems = totalElems = nTrias;
}

//------------------------------------------------------------------------------

void EmbeddedStructure::getInitialCrack() {
  int newNodes, numConnUpdate, numLSUpdate;
  bool need2update = structExc->getNewCrackingStats(numConnUpdate, numLSUpdate, newNodes); // inputs will be modified
  if(!need2update) {
    return;
  }
  // get initial phantom nodes.
  structExc->getInitialPhantomNodes(newNodes, X, nNodes);
  // NOTE: nNodes will be updated in "getNewCracking"
  // get initial phantom elements (topo change)
  getNewCracking(numConnUpdate, numLSUpdate, newNodes);
}

//------------------------------------------------------------------------------

void EmbeddedStructure::getNewCracking(int numConnUpdate, int numLSUpdate, int newNodes) {
  if(numConnUpdate < 1) {
    return;
  }
  int phantElems[5 * numConnUpdate]; // elem id and node id
  double phi[4 * numLSUpdate];
  int phiIndex[numLSUpdate];
  int new2old[std::max(1, newNodes * 2)]; // in the case of element deletion, newNodes might be 0
  structExc->getNewCracking(numConnUpdate, numLSUpdate, phantElems, phi, phiIndex, new2old, newNodes);
  if(elemType != 4) {
    com.fprintf(stderr, "*** Error: only support quadrangles for cracking!\n");
    exit(1);
  }
  nNodes += newNodes;
  nElems += cracking->updateCracking(numConnUpdate, numLSUpdate, phantElems, phi, phiIndex, Tria, nNodes, new2old, newNodes);
  if(nElems != cracking->usedTrias()) {
    com.fprintf(stderr, "*** Error: inconsistency in the number of used triangles. (Software bug.)\n");
    exit(-1);
  }
  if(com.cpuNum() == 0 && newNodes) {
    structExc->updateNumStrNodes(nNodes); // set numStrNodes to nNodes in structExc and mns
  }
}

//------------------------------------------------------------------------------

int EmbeddedStructure::getNewCracking() {
  int newNodes, numConnUpdate, numLSUpdate;
  bool need2update = structExc->getNewCrackingStats(numConnUpdate, numLSUpdate, newNodes); // inputs will be modified
  if(!need2update) {
    assert(numConnUpdate == 0);
    return 0;
  }
  getNewCracking(numConnUpdate, numLSUpdate, newNodes);
  return numConnUpdate;
}

//------------------------------------------------------------------------------

void EmbeddedStructure::writeCrackingData(std::ofstream& restart_file) const {
  if(!cracking) {
    return;
  }
  restart_file.write(reinterpret_cast<const char *>(Tria), sizeof(int)*totalElems * 3);
  cracking->writeCrackingData(restart_file);
}

//------------------------------------------------------------------------------

void EmbeddedStructure::readCrackingData(std::ifstream& restart_file) {
  if(!cracking) {
    return;
  }
  restart_file.read(reinterpret_cast<char *>(Tria), sizeof(int)*totalElems * 3);
  cracking->readCrackingData(restart_file);
}

//------------------------------------------------------------------------------

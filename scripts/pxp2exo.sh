#!/bin/bash
XP2EXO=/home/pavery/bin/xp2exo
NS=16 # number of subdomains

let FIRST=1+$OMPI_COMM_WORLD_RANK
COUNT=$(ls -1 postpro/OUTPUT.top.???? | wc -l)

# convert mesh and results to exodus format for paraview
if [ $OMPI_COMM_WORLD_RANK -eq 0 ]
then
  $XP2EXO ../sources/domain.top postpro/OUTPUT.e ../sources/domain.top.dec.$NS postpro/Pressure.xpost postpro/Mach.xpost
fi

for i in $(seq -f %04.0f $FIRST $OMPI_COMM_WORLD_SIZE $COUNT);
do 
  $XP2EXO postpro/OUTPUT.top.$i postpro/OUTPUT.e-s.$i postpro/OUTPUT.top.$i.dec postpro/Pressure.$i.xpost postpro/Mach.$i.xpost
done

# convert embedded surface mesh to exodus format for paraview
if [ $OMPI_COMM_WORLD_RANK -eq 0 ]
then
  $XP2EXO ../sources/circle.top postpro/embeddedsurface.exo results/embeddedsurfacedisplacement 
fi

XP2EXO=/home/pavery/bin/xp2exo
NS=64 # number of subdomains

COUNT=$(ls -1 postpro/fluidmodel.top.???? | wc -l)

# convert mesh and results to exodus format for paraview
$XP2EXO ../../sources/domain1.top postpro/fluidmodel.e ../../sources/domain1.top.dec.${NS} postpro/Pressure.xpost postpro/Mach.xpost
for i in $(seq -f %04.0f $COUNT);
do 
  $XP2EXO postpro/fluidmodel.top.$i postpro/fluidmodel.e-s.$i postpro/fluidmodel.top.$i.dec postpro/Pressure.$i.xpost postpro/Mach.$i.xpost
done

# convert embedded surface mesh to exodus format for paraview
$XP2EXO ../../sources/airfoil.top postpro/embeddedsurface.exo

#include <iostream>
#include <fstream>
#include <map>
#include <array>
#include <vector>
#include <set>
#include <iomanip>
#include <cmath>
#include <algorithm>
#include <sstream>

//#define EXTRUDE

int main()
{
  double a = 10, b = 5; // semi-major and semi-minor axes of ellipse defining location of farfield boundary
  double xc = 0.5, yc = 0; // center of ellipse

  std::ifstream file("../simulations/PreRefine/postpro/fluidmodel.top.0001");
  char line[512];
  std::string word1, word2;
  std::map<int, std::array<double, 3>> nodes;
  std::map<int, std::array<int, 4>> elems;
  std::map<int, std::array<int, 3>> faces;
  double zmin = std::numeric_limits<double>::max();
  double zmax = std::numeric_limits<double>::lowest();
  int nmax = -1, emax = -1;
  while(!file.eof()) {
    file.getline(line, 512);
    std::stringstream ss(line);
    ss >> word1 >> word2;
    if(word1 == "Nodes") {
      while(!file.eof()) {
        int n; std::array<double, 3> xyz;
        file >> n >> xyz[0] >> xyz[1] >> xyz[2];
        if(file.fail()) {
          file.clear();
          break;
        }
        nodes[n] = xyz;
        zmin = std::min(zmin, xyz[2]);
        zmax = std::max(zmax, xyz[2]);
        nmax = std::max(nmax, n);
      }
    }
    if(word1 == "Elements" && word2 == "Volume_0") {
      while(!file.eof()) {
        int e; int t; std::array<int, 4> v;
        file >> e >> t >> v[0] >> v[1] >> v[2] >> v[3];
        if(file.fail()) {
          file.clear();
          break;
        }
        elems[e] = v;
        emax = std::max(emax, e);
      }
    }
    if(word1 == "Elements" && word2 == "SymmetrySurface_1") {
      while(!file.eof()) {
        int f; int t; std::array<int, 3> v;
        file >> f >> t >> v[0] >> v[1] >> v[2];
        if(file.fail()) {
          file.clear();
          break;
        }
        faces[f] = v;
      }
    }
  }
  file.close();

  std::cerr << "nodes.size() = " << nodes.size() << std::endl;
  std::cerr << "elems.size() = " << elems.size() << std::endl;
  std::cerr << "faces.size() = " << faces.size() << std::endl;
  std::cerr << "zmin = " << zmin << ", zmax = " << zmax << std::endl;

  std::vector<int> new_elems;
  std::set<int> new_nodes;
#ifdef EXTRUDE
  for(auto& p : faces) {
    const int& f = p.first;
    const std::array<int, 3>& v = p.second;
    bool outside = true;
    for(int j = 0; j < 3; ++j) {
      const std::array<double, 3>& xyz = nodes[v[j]];
      const double& x = xyz[0];
      const double& y = xyz[1];
      if((x-xc)*(x-xc)/(a*a) + (y-yc)*(y-yc)/(b*b) <= 1) { outside = false; break; }
    }
    if(!outside) {
      for(int j = 0; j < 3; ++j) {
        new_nodes.insert(v[j]);
        const std::array<double, 3>& xyz = nodes[v[j]];
        nodes[nmax+v[j]] = { xyz[0], xyz[1], zmax };
        new_nodes.insert(nmax+v[j]);
      }
      // split triangular prism formed by nodes v[0], v[1], v[2], v[0], v[1], v[2]
      // assume that v[0], v[1], v[2] face normal is inward pointing
      // ref: HOW TO SUBDIVIDE PYRAMIDS, PRISMS AND HEXAHEDRA INTO TETRAHEDRA
      //      Julien Dompierre, Paul Labbé, Marie-Gabrielle Vallet, Ricardo Camarero
      std::array<int,6> VI;
      if(v[0] < v[1] && v[0] < v[2]) VI = { v[0], v[1], v[2], v[0]+nmax, v[1]+nmax, v[2]+nmax};
      if(v[1] < v[0] && v[1] < v[2]) VI = { v[1], v[2], v[0], v[1]+nmax, v[2]+nmax, v[0]+nmax};
      if(v[2] < v[0] && v[2] < v[1]) VI = { v[2], v[0], v[1], v[2]+nmax, v[0]+nmax, v[1]+nmax};
      std::array<int,4> v1, v2, v3;
      if(std::min(VI[1], VI[5]) < std::min(VI[2], VI[4])) {
        v1 = { VI[0], VI[1], VI[2], VI[5] };
        v2 = { VI[0], VI[1], VI[5], VI[4] };
        v3 = { VI[0], VI[4], VI[5], VI[3] };
      }
      else {
        v1 = { VI[0], VI[1], VI[2], VI[4] };
        v2 = { VI[0], VI[4], VI[2], VI[5] };
        v3 = { VI[0], VI[4], VI[5], VI[3] };
      }
      int e1 = new_elems.size()+1, e2 = new_elems.size()+2, e3 = new_elems.size()+3;
      elems[e1] = v1; elems[e2] = v2; elems[e3] = v3;
      new_elems.push_back(e1); new_elems.push_back(e2); new_elems.push_back(e3);
    }
  }
#else
  for(auto& p : elems) {
    const int& e = p.first;
    const std::array<int, 4>& v = p.second;
    bool outside = true;
    for(int j = 0; j < 4; ++j) {
      const std::array<double, 3>& xyz = nodes[v[j]];
      const double& x = xyz[0];
      const double& y = xyz[1];
      if((x-xc)*(x-xc)/(a*a) + (y-yc)*(y-yc)/(b*b) <= 1) { outside = false; break; }
    }
    if(!outside) {
      new_elems.push_back(e);
      for(int j = 0; j < 4; ++j) new_nodes.insert(v[j]);
    }
  }
#endif
  std::cerr << "new_nodes.size() = " << new_nodes.size() << std::endl;
  std::cerr << "new_elems.size() = " << new_elems.size() << std::endl;

  std::ofstream fout("../sources/domain.top");
  fout << "Nodes FluidNodes\n";
  std::map<int, int> node_map, inv_node_map;
  for(auto& n : new_nodes) {
    int nn = node_map.size()+1;
    node_map[n] = nn;
    inv_node_map[nn] = n;
    const std::array<double, 3>& xyz = nodes[n];
    fout << nn << "  " << std::setprecision(12) << xyz[0] << " " << xyz[1] << " " << xyz[2] << std::endl;
  }

  fout << "Elements Volume_0 using FluidNodes\n";
  std::map<int, int> elem_map;
  std::multiset<std::array<int, 3>> new_faces;
  for(auto& e : new_elems) {
    int ne = elem_map.size()+1;
    elem_map[e] = ne;
    const std::array<int, 4>& v = elems[e];
    std::array<int, 4> nv{node_map[v[0]], node_map[v[1]], node_map[v[2]], node_map[v[3]]};
    fout << ne << "  " << 5 << "  " << nv[0] << "  " << nv[1] << "  " << nv[2] << "  " << nv[3] << std::endl;
    std::sort(nv.begin(), nv.end());
    new_faces.insert({nv[0], nv[1], nv[2]});
    new_faces.insert({nv[0], nv[1], nv[3]});
    new_faces.insert({nv[1], nv[2], nv[3]});
    new_faces.insert({nv[0], nv[2], nv[3]});
  }
  std::cerr << "new_faces.size() = " << new_faces.size() << std::endl;

  std::vector<std::array<int, 3>> sym1, sym2, inlet;
  double tol = 1e-8;
  for(auto& f : new_faces) {
    if(new_faces.count(f) == 1) {
      std::array<double, 3>& a = nodes[inv_node_map[f[0]]];
      std::array<double, 3>& b = nodes[inv_node_map[f[1]]];
      std::array<double, 3>& c = nodes[inv_node_map[f[2]]];
      if(std::fabs(a[2] - zmin) <= tol && std::fabs(b[2]-zmin) <= tol && std::fabs(c[2]-zmin) <= tol) sym1.push_back(f);
      else if(std::fabs(a[2] - zmax) <= tol && std::fabs(b[2]-zmax) <= tol && std::fabs(c[2]-zmax) <= tol) sym2.push_back(f);
      else inlet.push_back(f);
    }
  }
  std::cerr << "sym1.size() = " << sym1.size() << ", sym2.size() = " << sym2.size() << ", inlet.size() = " << inlet.size() << std::endl;

  fout << "Elements SymmetrySurface_1 using FluidNodes\n";
  int count = 0;
  for(auto& f : sym1) {
    fout << ++count << "  " << 4 << "  " << f[0] << "  " << f[1] << "  " << f[2] << std::endl;
  }

  fout << "Elements SymmetrySurface_2 using FluidNodes\n";
  count = 0;
  for(auto& f : sym2) {
    fout << ++count << "  " << 4 << "  " << f[2] << "  " << f[1] << "  " << f[0] << std::endl;
  }

  fout << "Elements InletFixedSurface_3 using FluidNodes\n";
  count = 0;
  for(auto& f : inlet) {
    fout << ++count << "  " << 4 << "  " << f[0] << "  " << f[1] << "  " << f[2] << std::endl;
  }
  fout.close();
}

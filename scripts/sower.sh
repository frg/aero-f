SOWER=/home/pavery/bin/sower

# merge results for original mesh
$SOWER -fluid -merge -con ../../data/fluidmodel.con -mesh ../../data/fluidmodel.msh \
       -result results/Pressure.bin -output postpro/Pressure -name Pressure
$SOWER -fluid -merge -con ../../data/fluidmodel.con -mesh ../../data/fluidmodel.msh \
       -result results/Mach.bin -output postpro/Mach -name Mach

##########################################################################################

COUNT=$(ls -1 results/fluidmodel.msh.*.1 | wc -l)

# convert the binary mesh files to xpost format
for i in $(seq -f %04.0f 1 $COUNT);
do
  $SOWER -fluid -merge -con results/fluidmodel.con.$i \
         -mesh results/fluidmodel.msh.$i. -dec results/fluidmodel.dec.$i. \
         -topo postpro/fluidmodel.top.$i -width 23 -precision 16
done

# merge results for refined meshes
for i in $(seq -f %04.0f 1 $COUNT);
do
  $SOWER -fluid -merge -con results/fluidmodel.con.$i \
         -mesh results/fluidmodel.msh.$i. -result results/Pressure.bin.$i. \
         -output postpro/Pressure.$i -name Pressure
  $SOWER -fluid -merge -con results/fluidmodel.con.$i \
         -mesh results/fluidmodel.msh.$i. -result results/Mach.bin.$i. \
         -output postpro/Mach.$i -name Mach
done

##########################################################################################

#!/bin/bash
SOWER=/home/pavery/bin/sower

let FIRST=1+$OMPI_COMM_WORLD_RANK
COUNT=$(ls -1 results/OUTPUT.msh.*.01 | wc -l)

if [ $OMPI_COMM_WORLD_RANK -eq 0 ]
then
  # merge results for original mesh
  $SOWER -fluid -merge -con ../data/OUTPUT.con -mesh ../data/OUTPUT.msh \
         -result results/Pressure.bin -output postpro/Pressure -name Pressure
  $SOWER -fluid -merge -con ../data/OUTPUT.con -mesh ../data/OUTPUT.msh \
         -result results/Mach.bin -output postpro/Mach -name Mach
fi

##########################################################################################

# convert the binary mesh files to xpost format
for i in $(seq -f %04.0f $FIRST $OMPI_COMM_WORLD_SIZE $COUNT);
do
  $SOWER -fluid -merge -con results/OUTPUT.con.$i \
         -mesh results/OUTPUT.msh.$i. -dec results/OUTPUT.dec.$i. \
         -topo postpro/OUTPUT.top.$i -width 23 -precision 16
done

# merge results for refined meshes
for i in $(seq -f %04.0f $FIRST $OMPI_COMM_WORLD_SIZE $COUNT);
do
  $SOWER -fluid -merge -con results/OUTPUT.con.$i \
         -mesh results/OUTPUT.msh.$i. -result results/Pressure.bin.$i. \
         -output postpro/Pressure.$i -name Pressure
  $SOWER -fluid -merge -con results/OUTPUT.con.$i \
         -mesh results/OUTPUT.msh.$i. -result results/Mach.bin.$i. \
         -output postpro/Mach.$i -name Mach
done

##########################################################################################

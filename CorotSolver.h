#ifndef _COROT_SOLVER_H_
#define _COROT_SOLVER_H_

#include <IoData.h>
#include <DistVector.h>
#include <NodeData.h>

class MatchNodeSet;
class Domain;
class Communicator;
class BCApplier;

template<class Scalar, int dim> class DistSVec;

//------------------------------------------------------------------------------

class CorotSolver {

  int numLocSub;

  int *nInterfNd;  // int nInerfNd[3] ==> number of nodes for sub 3
  int **interfNd;  // int *interfNd[3] ==> array of nodes for sub 3
  int *nInfNd;     // number of nodes at infinity per sub
  int **infNd;
  int *nInternalNd;
  int **internalNd;

  bool locAllocGap;

  double (**gapVec)[3]; // gap vectors on structure interface

  double cg0[3];
  double cgN[3];

  double R[3][3];

  DistSVec<double, 3> X0;
  DistSVec<double, 3> Xtilde;

  MatchNodeSet **matchNodes;
  Domain *domain;
  Communicator *com;

  enum SymmetryAxis {NONE, AXIS_X, AXIS_Y, AXIS_Z} SymAxis;

  double cm0[3];
  double cmN[3];

  DefoMeshMotionData::Type type;

  std::string corotmatrix;

 private:

  void computeCG(DistSVec<double, 3>&, double[3]);
  void computeRotGradAndJac(DistSVec<double, 3>&, double[3][3],
                            double[3], double[3], double[3][3]);
  void computeRotMat(double *, double[3][3]);
  double computeSubGradAndJac(SVec<double, 3>&, double[3][3], double[3],
                              double[3], double[3][3], int);
  void rotLocVec(double[3][3], double[3]);
  void solveDeltaRot(DistSVec<double, 3>&, double[3][3], double[3]);
  void solveRotMat(double[3][3], double[3]);
  void computeInfNodeRot(double[3][3], DistSVec<double, 3>&, double[3], double[3]);
  void computeNodeRot(double[3][3], DistSVec<double, 3>&,
                      DistSVec<double, 3>&, double[3]);

  void printRotMat(double[3][3]);

  void computeRigidNodeRot(double[3][3], DistSVec<double, 3>&, DistSVec<double, 3>&,
                           double[3], double[3]);
  void updateCM(double[3]);
  void readRotationMatrix();

 public:

  CorotSolver(DefoMeshMotionData&, MatchNodeSet **, Domain *, std::string = "");
  ~CorotSolver();

  void resize(NodeData *);

  void solve(DistSVec<double, 3>&, DistSVec<double, 3>&, BCApplier * = 0);
  // setup computes the rotation and CG that will fit best for restarting
  void setup(DistSVec<double, 3>&);

  void getDeltaXCM(double[3]);

  DefoMeshMotionData::Type getType() { return type; }

  void getRotationMatrix(double[3][3]);

  void updateMomentArm(Vec3D&, Vec3D&);

  void solveCGAndRotMat(DistSVec<double, 3>&, DistSVec<double, 3>&);

  void reset() { setup(X0); }

};

//------------------------------------------------------------------------------

#endif

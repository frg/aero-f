#ifndef _WALL_FCN_ODE_H_
#define _WALL_FCN_ODE_H_

#include <WallFcn.h>
#include <VarFcn.h>
#include <Vector3D.h>
#include <ViscoFcn.h>
#include <BcDef.h>
#include <cmath>

class WallFcnODE : public WallFcn {

  double ooreynolds_mu;
  double ooprandtl;

 public:

  WallFcnODE(IoData&, VarFcn *, ViscoFcn *);
  ~WallFcnODE() {}

  void computeSurfaceTerm(int, Vec3D&, double[3], double *, double *[3], double *, int);

  void computeJacobianSurfaceTermLES(int, Vec3D&, double[3], double *, double *[3], double (*)[5][5], int);

  void computeNormalGradientLES(double, double, Vec3D&, Vec3D&, double, double&, double, double&, bool, int);

  void computeDerivativeNormalGradientLES(double, double, Vec3D&, Vec3D&, double, double&, double, double&, double *, double *, bool, int);

  void adiabatic_wall_model(const double, const double, const double, const double, const double, double&, double *, double *, int);

  void isothermal_wall_model(const double, const double, const double, const double, const double, double&, double *, double *, int);

  void bc_RANS_SL(double[3], double[4], double, double, double, double, double, double *);

  void bc_RANS_SL_deriv(double[3], double[4], double, double, double, double, double, double *, double *, double[2]);

  void bc_RANS_SL_jacderivatives(double *, double *, double[3], double[4], double, double, double *, bool, int);

  Vec3D computeForce(Vec3D&, double[3], double *, double *[3], int);

  double computeInterfaceWork(Vec3D&, double[3], double *, double *[3], int);

  double computeHeatPower(Vec3D&, double[3], double *, double *[3], int);

  double computeDeltaPlus(Vec3D&, double[3], double *, double *[3], int);

  double computeDeltaPlus(Vec3D&, double, Vec3D&, double *, int);

};

//------------------------------------------------------------------------------

inline
WallFcnODE::WallFcnODE(IoData& iod, VarFcn *varf, ViscoFcn *visf) :
  WallFcn(iod, varf, visf) {
  ooreynolds_mu = 1.0 / iod.ref.reynolds_mu;
  ooprandtl     = 1.0 / iod.eqs.thermalCondModel.prandtl;

  vkcst         = 0.41;
  maxits        = 100; //iod.bc.wall.wallFcnData.maxits;
  eps           = 1e-5; //iod.bc.wall.wallFcnData.eps;
  vsl_threshold = iod.bc.wall.wallFcnData.vsl_threshold;
  reynolds      = iod.ref.reynolds_mu;
  prandtl       = iod.eqs.thermalCondModel.prandtl;
  dRedMach      = iod.ref.dRe_mudMach;
  prandtlT      = iod.eqs.tc.prandtlTurbulent;
  codeWall      = iod.bc.wall.type;
  isoTemp       = iod.bc.wall.temperature;
}

//------------------------------------------------------------------------------

inline
void WallFcnODE::computeSurfaceTerm(int code, Vec3D& normal, double d2wall[3],
                                    double *Vwall, double *V[3], double *term, int tag) {
  double delta, dT, rhow, muw, gamw;
  Vec3D du, uw;
  computeFaceValues(d2wall, Vwall, V, delta, du, dT, uw, rhow, muw, gamw, tag);

  muw          = muw * ooreynolds_mu;
  double kappa = muw * ooprandtl * gamw;
  double norm  = sqrt(normal * normal);
  Vec3D n      = (1.0 / norm) * normal;
  Vec3D t      = computeTangentVector(n, du);
  double T     = third * (varFcn->computeTemperature(V[0], tag) +
                          varFcn->computeTemperature(V[1], tag) +
                          varFcn->computeTemperature(V[2], tag));
  double ut    = du * t;
  dT           = (-1.*0.5*prandtlT*ut*ut/gamw); //assumes adiabatic
  double Tw    = T - dT;
  bool isIsothermal = false;
  if(code == BC_ISOTHERMAL_WALL_MOVING || code == BC_ISOTHERMAL_WALL_FIXED) {
    isIsothermal = true;
    Tw           = isoTemp;
    dT           = T - Tw;
  }

  double *Vwall_new = Vwall;
  varFcn->getV4FromTemperature(Vwall_new, Tw, tag);
  double pwall = Vwall_new[4];
  double p     = third * (varFcn->getPressure(V[0], tag) +
                          varFcn->getPressure(V[1], tag) +
                          varFcn->getPressure(V[2], tag));
  double dudns, dTdns = 0.0;
  computeNormalGradientLES(T, p, du, t, Tw, dTdns, delta, dudns, isIsothermal, tag);
  double mu_turb_term = rhow * vkcst * vkcst * delta * delta * dudns;
  double a = - mu_turb_term * dudns * norm;

  term[0] = 0.0;
  term[1] = a * t[0];
  term[2] = a * t[1];
  term[3] = a * t[2];
  term[4] = a * (uw * t) + kappa * dTdns * norm;
}

//------------------------------------------------------------------------------
// This function was modified to account for the derivative of the muw with respect to the conservative variables
inline
void WallFcnODE::computeJacobianSurfaceTermLES(int code, Vec3D& normal, double d2wall[3],
                                               double *Vwall, double *V[3], double (*dRdU)[5][5], int tag) {
  double delta, dT, rho, mu, gam;
  Vec3D du, u;
  computeFaceValues(d2wall, Vwall, V, delta, du, dT, u, rho, mu, gam, tag);
  mu           = mu * ooreynolds_mu;
  double kappa = mu * ooprandtl * gam;
  double gam1  = gam - 1; // gam1 = R = R/Cv
  double norm  = sqrt(normal * normal);
  Vec3D n      = (1.0 / norm) * normal;
  Vec3D t      = computeTangentVector(n, du);
  double T     = third * (varFcn->computeTemperature(V[0], tag) +
                          varFcn->computeTemperature(V[1], tag) +
                          varFcn->computeTemperature(V[2], tag));
  double ut    = du * t;
  bool isIsothermal = false;
  dT           = (-1.*0.5*prandtlT*ut*ut/gam); //assumes adiabatic
  double Tw    = T - dT;
    if(code == BC_ISOTHERMAL_WALL_MOVING || code == BC_ISOTHERMAL_WALL_FIXED) {
    isIsothermal = true;
    Tw           = isoTemp;
    dT           = T - Tw;
  }

  double *Vwall_new = Vwall;
  varFcn->getV4FromTemperature(Vwall_new, Tw, tag);
  double pwall = Vwall_new[4];
  double p     = third * (varFcn->getPressure(V[0], tag) +
                          varFcn->getPressure(V[1], tag) +
                          varFcn->getPressure(V[2], tag));

  double dudns, dTdns = 0.0;
  double ddudns[5], ddTdns[5];
  computeDerivativeNormalGradientLES(T, p, du, t, Tw, dTdns, delta, dudns, ddudns, ddTdns, isIsothermal, tag);
  double dMach = 0.0;
  double dooreynolds_mu = 0.0;
  double dmu[3][5];
  double dkappa[3][5];
  double dTdV[5];
  double ddTdV[5];
  double dpdV[5];
  double ddudV[3][5];
  double dtdu[3][5];
  Vec3D dtdu0;
  Vec3D dtdu1;
  Vec3D dtdu2;
  Vec3D dtdu3;
  Vec3D dtdu4;
  Vec3D ddudu0;
  Vec3D ddudu1;
  Vec3D ddudu2;
  Vec3D ddudu3;
  Vec3D ddudu4;
  Vec3D ddudnsdu0;
  Vec3D ddudnsdu1;
  Vec3D ddudnsdu2;
  Vec3D ddudnsdu3;
  Vec3D ddudnsdu4;
  Vec3D ddTdnsdu0;
  Vec3D ddTdnsdu1;
  Vec3D ddTdnsdu2;
  Vec3D ddTdnsdu3;
  Vec3D ddTdnsdu4;
  double dTdu0, dTdu1, dTdu2, dTdu3, dTdu4;
  for(int i = 0; i < 3; ++i) {
    for(int j = 0; j < 5; ++j) {
      dkappa[i][j] = 0.0;
      dmu[i][j] = 0.0;
    }
  }

  for(int k = 0; k < 3; ++k) {
    double rho = varFcn->getDensity(V[k], tag);
    double Tk = varFcn->computeTemperature(V[k], tag);
    dTdu0 = - third / V[k][0] * (Tk - 0.5 * (V[k][1] * V[k][1] + V[k][2] * V[k][2] + V[k][3] * V[k][3]));
    dTdu1 = - third / V[k][0] * V[k][1];
    dTdu2 = - third / V[k][0] * V[k][2];
    dTdu3 = - third / V[k][0] * V[k][3];
    dTdu4 =   third / V[k][0];
    dTdV[0] = dTdu0;
    dTdV[1] = dTdu1;
    dTdV[2] = dTdu2;
    dTdV[3] = dTdu3;
    dTdV[4] = dTdu4;
    dmu[k][0] = dooreynolds_mu * (mu / ooreynolds_mu) + ooreynolds_mu * viscoFcn->compute_muDerivative(T, dTdu0, dMach, tag);
    dmu[k][1] = dooreynolds_mu * (mu / ooreynolds_mu) + ooreynolds_mu * viscoFcn->compute_muDerivative(T, dTdu1, dMach, tag);
    dmu[k][2] = dooreynolds_mu * (mu / ooreynolds_mu) + ooreynolds_mu * viscoFcn->compute_muDerivative(T, dTdu2, dMach, tag);
    dmu[k][3] = dooreynolds_mu * (mu / ooreynolds_mu) + ooreynolds_mu * viscoFcn->compute_muDerivative(T, dTdu3, dMach, tag);
    dmu[k][4] = dooreynolds_mu * (mu / ooreynolds_mu) + ooreynolds_mu * viscoFcn->compute_muDerivative(T, dTdu4, dMach, tag);
    dkappa[k][0] = dooreynolds_mu * (kappa / ooreynolds_mu) + ooreynolds_mu * ooprandtl * gam * viscoFcn->compute_muDerivative(T, dTdu0, dMach, tag);
    dkappa[k][1] = dooreynolds_mu * (kappa / ooreynolds_mu) + ooreynolds_mu * ooprandtl * gam * viscoFcn->compute_muDerivative(T, dTdu1, dMach, tag);
    dkappa[k][2] = dooreynolds_mu * (kappa / ooreynolds_mu) + ooreynolds_mu * ooprandtl * gam * viscoFcn->compute_muDerivative(T, dTdu2, dMach, tag);
    dkappa[k][3] = dooreynolds_mu * (kappa / ooreynolds_mu) + ooreynolds_mu * ooprandtl * gam * viscoFcn->compute_muDerivative(T, dTdu3, dMach, tag);
    dkappa[k][4] = dooreynolds_mu * (kappa / ooreynolds_mu) + ooreynolds_mu * ooprandtl * gam * viscoFcn->compute_muDerivative(T, dTdu4, dMach, tag);
    dpdV[0] = dTdV[0] * gam1 * V[k][0];
    dpdV[1] = dTdV[1] * gam1 * V[k][0];
    dpdV[2] = dTdV[2] * gam1 * V[k][0];
    dpdV[3] = dTdV[3] * gam1 * V[k][0];
    dpdV[4] = dTdV[4] * gam1 * V[k][0];
    ddudV[0][0] = - third * varFcn->getVelocityX(V[k], tag) / rho;
    ddudV[1][0] = - third * varFcn->getVelocityY(V[k], tag) / rho;
    ddudV[2][0] = - third * varFcn->getVelocityZ(V[k], tag) / rho;
    ddudV[0][1] = third / rho;
    ddudV[1][1] = 0.0;
    ddudV[2][1] = 0.0;
    ddudV[0][2] = 0.0;
    ddudV[1][2] = third / rho;
    ddudV[2][2] = 0.0;
    ddudV[0][3] = 0.0;
    ddudV[1][3] = 0.0;
    ddudV[2][3] = third / rho;
    ddudV[0][4] = 0.0;
    ddudV[1][4] = 0.0;
    ddudV[2][4] = 0.0;

    ddudu0 = {ddudV[0][0], ddudV[1][0], ddudV[2][0]};
    ddudu1 = {ddudV[0][1], ddudV[1][1], ddudV[2][1]};
    ddudu2 = {ddudV[0][2], ddudV[1][2], ddudV[2][2]};
    ddudu3 = {ddudV[0][3], ddudV[1][3], ddudV[2][3]};
    ddudu4 = {ddudV[0][4], ddudV[1][4], ddudV[2][4]};
    dtdu0 = computeDerivativeOfTangentVector(n, du, ddudu0);
    dtdu1 = computeDerivativeOfTangentVector(n, du, ddudu1);
    dtdu2 = computeDerivativeOfTangentVector(n, du, ddudu2);
    dtdu3 = computeDerivativeOfTangentVector(n, du, ddudu3);
    dtdu4 = computeDerivativeOfTangentVector(n, du, ddudu4);

    double ddudnsdu[3][5];
    double ddTdnsdu[3][5]; //initialize to zero, since adiabatic has 0 value
    double dydV = 0.0;
    double ddTdV = 0.0;

    for(int m = 0; m < 5; ++m) {   // m over partial derivatives
      for(int l = 0; l < 3; ++l) { // l over x,y,z
        ddudnsdu[l][m] = ddudns[0] * dpdV[m] + ddudns[1] * ddudV[l][m] + ddudns[2] * dTdV[m] + ddudns[3] * dydV + ddudns[4] * ddTdV;
        if(code == BC_ISOTHERMAL_WALL_MOVING || code == BC_ISOTHERMAL_WALL_FIXED) {
          ddTdnsdu[l][m] = ddTdns[0] * dpdV[m] + ddTdns[1] * ddudV[l][m] + ddTdns[2] * dTdV[m] + ddTdns[3] * dydV + ddTdns[4] * ddTdV;
        }
        else {
          ddTdnsdu[l][m] = 0.0;
        }
      }
    }
    ddudnsdu0 = {ddudnsdu[0][0], ddudnsdu[1][0], ddudnsdu[2][0]};
    ddudnsdu1 = {ddudnsdu[0][1], ddudnsdu[1][1], ddudnsdu[2][1]};
    ddudnsdu2 = {ddudnsdu[0][2], ddudnsdu[1][2], ddudnsdu[2][2]};
    ddudnsdu3 = {ddudnsdu[0][3], ddudnsdu[1][3], ddudnsdu[2][3]};
    ddudnsdu4 = {ddudnsdu[0][4], ddudnsdu[1][4], ddudnsdu[2][4]};
    ddTdnsdu0 = {ddTdnsdu[0][0], ddTdnsdu[1][0], ddTdnsdu[2][0]};
    ddTdnsdu1 = {ddTdnsdu[0][1], ddTdnsdu[1][1], ddTdnsdu[2][1]};
    ddTdnsdu2 = {ddTdnsdu[0][2], ddTdnsdu[1][2], ddTdnsdu[2][2]};
    ddTdnsdu3 = {ddTdnsdu[0][3], ddTdnsdu[1][3], ddTdnsdu[2][3]};
    ddTdnsdu4 = {ddTdnsdu[0][4], ddTdnsdu[1][4], ddTdnsdu[2][4]};

    dRdU[k][0][0] = 0.0;
    dRdU[k][0][1] = 0.0;
    dRdU[k][0][2] = 0.0;
    dRdU[k][0][3] = 0.0;
    dRdU[k][0][4] = 0.0;
    dRdU[k][1][0] = ((dmu[k][0] * dudns + mu * ddudnsdu[0][0]) * t[0] + (mu * dudns) * dtdu0[0]) * norm;
    dRdU[k][1][1] = ((dmu[k][1] * dudns + mu * ddudnsdu[0][1]) * t[0] + (mu * dudns) * dtdu1[0]) * norm;
    dRdU[k][1][2] = ((dmu[k][2] * dudns + mu * ddudnsdu[0][2]) * t[0] + (mu * dudns) * dtdu2[0]) * norm;
    dRdU[k][1][3] = ((dmu[k][3] * dudns + mu * ddudnsdu[0][3]) * t[0] + (mu * dudns) * dtdu3[0]) * norm;
    dRdU[k][1][4] = ((dmu[k][4] * dudns + mu * ddudnsdu[0][4]) * t[0] + (mu * dudns) * dtdu4[0]) * norm;
    dRdU[k][2][0] = ((dmu[k][0] * dudns + mu * ddudnsdu[1][0]) * t[1] + (mu * dudns) * dtdu0[1]) * norm;
    dRdU[k][2][1] = ((dmu[k][1] * dudns + mu * ddudnsdu[1][1]) * t[1] + (mu * dudns) * dtdu1[1]) * norm;
    dRdU[k][2][2] = ((dmu[k][2] * dudns + mu * ddudnsdu[1][2]) * t[1] + (mu * dudns) * dtdu2[1]) * norm;
    dRdU[k][2][3] = ((dmu[k][3] * dudns + mu * ddudnsdu[1][3]) * t[1] + (mu * dudns) * dtdu3[1]) * norm;
    dRdU[k][2][4] = ((dmu[k][4] * dudns + mu * ddudnsdu[1][4]) * t[1] + (mu * dudns) * dtdu4[1]) * norm;
    dRdU[k][3][0] = ((dmu[k][0] * dudns + mu * ddudnsdu[2][0]) * t[2] + (mu * dudns) * dtdu0[2]) * norm;
    dRdU[k][3][1] = ((dmu[k][1] * dudns + mu * ddudnsdu[2][1]) * t[2] + (mu * dudns) * dtdu1[2]) * norm;
    dRdU[k][3][2] = ((dmu[k][2] * dudns + mu * ddudnsdu[2][2]) * t[2] + (mu * dudns) * dtdu2[2]) * norm;
    dRdU[k][3][3] = ((dmu[k][3] * dudns + mu * ddudnsdu[2][3]) * t[2] + (mu * dudns) * dtdu3[2]) * norm;
    dRdU[k][3][4] = ((dmu[k][4] * dudns + mu * ddudnsdu[2][4]) * t[2] + (mu * dudns) * dtdu4[2]) * norm;

    dRdU[k][4][0] = ((dmu[k][0] * dudns * (u * t) + mu * (ddudnsdu0 * t)) + (mu * dudns) * (u * dtdu0)) * norm;
    dRdU[k][4][1] = ((dmu[k][1] * dudns * (u * t) + mu * (ddudnsdu1 * t)) + (mu * dudns) * (u * dtdu1)) * norm;
    dRdU[k][4][2] = ((dmu[k][2] * dudns * (u * t) + mu * (ddudnsdu2 * t)) + (mu * dudns) * (u * dtdu2)) * norm;
    dRdU[k][4][3] = ((dmu[k][3] * dudns * (u * t) + mu * (ddudnsdu3 * t)) + (mu * dudns) * (u * dtdu3)) * norm;
    dRdU[k][4][4] = ((dmu[k][4] * dudns * (u * t) + mu * (ddudnsdu4 * t)) + (mu * dudns) * (u * dtdu4)) * norm;
    if(code == BC_ISOTHERMAL_WALL_MOVING || code == BC_ISOTHERMAL_WALL_FIXED) {
      dRdU[k][4][0] += (dkappa[k][0] * dTdns + kappa * ddTdnsdu0[0]) * norm; //TODO CHANGE ddT term to be in correct directions
      dRdU[k][4][1] += (dkappa[k][1] * dTdns + kappa * ddTdnsdu1[0]) * norm;
      dRdU[k][4][2] += (dkappa[k][2] * dTdns + kappa * ddTdnsdu2[0]) * norm;
      dRdU[k][4][3] += (dkappa[k][3] * dTdns + kappa * ddTdnsdu3[0]) * norm;
      dRdU[k][4][4] += (dkappa[k][4] * dTdns + kappa * ddTdnsdu4[0]) * norm;
    }
  }
}

//------------------------------------------------------------------------------
/* This is the wall model for LES, based on
 * "Wall-modeling in large eddy simulation: Length scales, grid resolution, and
    accuracy" by Soshi Kawai, and Johan Larsson
 * param[in] double rho:    density of the outer layer
 * param[in] double T:      temperature of the outer layer
 * param[in] double p:      pressure of the outer layer
 * param[in] Vec3D& du:     velocity of the outer layer, relative to the wall
 * param[in] Vec3D& et:     velocity tangential direction, parallel to the wall
 * param[in] double Ts:     temperature of the isothermal wall, -1., for adiabatic wall
 * param[in] double dTdns:  heat flux dT/dn at the wall, used only for adiabatic wall
 * param[in] double d2w:    distance to the wall
 *
 * param[out] double dudns:       du/dn at the wall (aerof wall)
 * param[out] double Ts_out:      temperature at the wall
 * param[out] double dTdns_out:   temperature gradient at the wall.
 * param[out] double drhodns_out: density gradient at the wall.
 * param[out] double dmudns_out:  mu_t gradient at the wall.
 * TODO only for ideal gas
 */
inline
void WallFcnODE::computeNormalGradientLES(double T, double p, Vec3D& du, Vec3D& et, double Tw, double& dTdns,
                                          double d2w, double& dudns, bool isIsothermal, int tag) {
  double ut = du * et;
  if(ut < 1e-10) {
    // std::cerr << "*** Error: in WallFcnODE::computeNormalGradientLES, ut is " << ut << std::endl;
    ut = 0.0;
    dudns = 0.0;
    return;
  }
  double x[2];
  double f_out[5];
  if(!isIsothermal) { // adiabatic wall
    adiabatic_wall_model(p, ut, T, d2w, Tw, dTdns, x, f_out, tag);
    dudns = x[0];
  }
  else { // isothermal wall
    std::cerr << "*** Error: called isothermal. Currently not validated.\n";
    // isothermal_wall_model(p, ut, T, d2w, Tw, dTdns, x, F, tag);
    // dudns = x[0];
    // dTdns = x[1];
  }
}

//------------------------------------------------------------------------------
/* This is the wall model for LES, based on
 * "Wall-modeling in large eddy simulation: Length scales, grid resolution, and
    accuracy" by Soshi Kawai, and Johan Larsson
 */
inline
void WallFcnODE::computeDerivativeNormalGradientLES(double T, double p, Vec3D& du, Vec3D& et, double Tw, double& dTdns,
                                                    double d2w, double& dudns, double *ddudns, double *ddTdns, bool isIsothermal, int tag) {
  double ut = du * et;
  if(ut < 1e-10) {
    // std::cerr << "*** Error: in WallFcnODE::computeDerivativeNormalGradientLES, ut is " << ut << std::endl;
    ut = 0.0;
    dudns = 0.0;
    *ddudns = 0.0;
    *ddTdns = 0.0;
    return;
  }
  double x[2];
  double f_out[5];
  double F[2];
  if(!isIsothermal) { // adiabatic wall
    adiabatic_wall_model(p, ut, T, d2w, Tw, dTdns, x, f_out, tag);
  }
  else { // isothermal wall
    std::cerr << "*** Error: called isothermal. Currently not validated.\n";
    // isothermal_wall_model(p, ut, T, d2w, Tw, dTdns, x, f_out, tag);
    // isIsothermal = true;
    // dudns = x[0];
    // dTdns = x[1];
    // double ddudns[5], ddTdns[5];
    // double muw = viscoFcn->compute_mu(Tw, tag) * ooreynolds_mu;
    // double u_new = f_out[0];
    // double T_new = f_out[1];
    // double mu_new = viscoFcn->compute_mu(T_new, tag) * ooreynolds_mu;
    // double U[3] = {u_new, T_new, mu_new};
    // double P[4] = {dudns, T, dTdns, muw}; //unsure if T_new and if chain rule derivatives are correct
    // bc_RANS_SL_jacderivatives(ddudns, ddTdns, U, P, d2w, p, F, isIsothermal, tag);
  }
  dudns = x[0];
  double muw   = viscoFcn->compute_mu(Tw, tag) * ooreynolds_mu;
  double u_new = f_out[0];
  double T_new = f_out[1];
  double mu_new = viscoFcn->compute_mu(T_new, tag) * ooreynolds_mu;
  double U[3]  = {u_new, T_new, mu_new};
  double du0   = f_out[2];
  double T0_p  = f_out[3];
  double dT0_p = f_out[4];
  double mu0_p = viscoFcn->compute_mu(T0_p, tag) * ooreynolds_mu;
  double P[4]  = {du0, T0_p, dT0_p, mu0_p};
  bc_RANS_SL_jacderivatives(ddudns, ddTdns, U, P, d2w, p, F, isIsothermal, tag);
}

//------------------------------------------------------------------------------

inline
void WallFcnODE::bc_RANS_SL(double U[3], double P[4], double y, double p, double gam, double R,
                            double Pr, double *F) {
  double du0, T0, dT0, mu0, u, T, muT, tau0, rhoT, rho0, mu_turb_coef, yp_w_term, yp_sl_term, yp_l_term, yp_term,
         D_term, D, mu_turb, cp;
  /*
  U[3] = {u, T, muT};
  P[4] = {du0, T0, dT0, mu0};
  y    = distance to wall
  p    = constant pressure
  F    = {du_dy0, dT_dy}, TO BE SOLVED
  NOTE: Tau_w = mu_w * du_w == tau0
  NOTE: vkcst = 0.41 (globally defined in WallFunctionCore.C)
  NOTE: prandtlT (globally defined in WallFunctionCore.C)
  */
  du0 = P[0];
  T0  = P[1];
  dT0 = P[2];
  mu0 = P[3];
  u   = U[0];
  T   = U[1];
  muT = U[2];

  tau0 = mu0 * du0;
  rhoT = p / (T * R);
  rho0 = p / (T0 * R);

  mu_turb_coef = sqrt(tau0 * rhoT);
  yp_w_term    = sqrt(rho0 * du0 / mu0 );
  yp_sl_term   = sqrt(tau0 * rhoT) / muT;
  yp_l_term    = sqrt(tau0 * p / R * T0) / (T * muT);
  yp_term      = std::min((yp_w_term + yp_sl_term), (yp_l_term + yp_sl_term)) / 0.2e1;

  D_term       = exp(-0.58823529411764705882e-1 * y * yp_term);
  D            = pow(0.1e1 - D_term, 0.2e1);
  mu_turb      = y * mu_turb_coef * D * vkcst;
  cp           = gam;

  F[0] = tau0 / (muT + mu_turb);
  F[1] = (mu0 / Pr * dT0 - tau0 * u / cp) / (muT / Pr + mu_turb / prandtlT);
}

//------------------------------------------------------------------------------

inline
void WallFcnODE::bc_RANS_SL_deriv(double U[3], double P[4], double y, double p, double gam, double R, double Pr, double *dFdU, double *dFdP, double dmuVect[2]) {
  double du0, T0, dT0, mu0, u, T, muT, dmu0, dmuT, tau0, rhoT, rho0, mu_turb_coef, yp_w_term, yp_sl_term, yp_l_term, yp_term,
       D_term, D, mu_turb, cp;
  double yp_term_opt1, yp_term_opt2, dyp_sl_term_dU1, dyp_sl_term_dU2, dyp_sl_term_dP0, dyp_sl_term_dP1, dyp_sl_term_dP3,
       dDterm_dU1, dDterm_dU2, dDterm_dP0, dDterm_dP1, dDterm_dP3, dyp_l_term_dU1, dyp_l_term_dU2, dyp_l_term_dP0,
       dyp_l_term_dP1, dyp_l_term_dP3, dyp_w_term_dU1, dyp_w_term_dU2, dyp_w_term_dP0, dyp_w_term_dP1, dyp_w_term_dP3;

  du0 = P[0];
  T0  = P[1];
  dT0 = P[2];
  mu0 = P[3];
  u   = U[0];
  T   = U[1];
  muT = U[2];
  dmu0 = dmuVect[0];
  dmuT = dmuVect[1];

  tau0 = mu0 * du0;
  rhoT = p / (T * R);
  rho0 = p / (T0 * R);

  mu_turb_coef = sqrt(tau0 * rhoT);
  yp_w_term    = sqrt(du0 / mu0 * rho0);
  yp_sl_term   = sqrt(tau0 * rhoT) / muT;
  yp_l_term    = sqrt(tau0 * p / R * T0) / (T * muT);
  yp_term_opt1 = (yp_w_term + yp_sl_term) / 0.2e1;
  yp_term_opt2 = (yp_l_term + yp_sl_term) / 0.2e1;
  yp_term      = std::min(yp_term_opt1, yp_term_opt2);

  D_term       = exp(-0.58823529411764705882e-1 * y * yp_term);
  D            = pow(0.1e1 - D_term, 0.2e1);
  mu_turb      = y * mu_turb_coef * D * vkcst;
  cp           = gam;

  dyp_sl_term_dU1   = -0.5e0 * sqrt(tau0 * rhoT) / (muT * T);
  dyp_sl_term_dU2   = -0.1e1 * sqrt(tau0 * rhoT) / (muT * muT);
  dyp_sl_term_dP0   = 0.5e0 * sqrt(rho0 * mu0 / du0) / muT;
  dyp_sl_term_dP1   = 0.0e0;
  dyp_sl_term_dP3   = 0.5e0 * sqrt(rhoT * du0 / mu0) / muT;

  if(yp_term == yp_term_opt1) {
    dyp_w_term_dU1  = 0.0e0;
    dyp_w_term_dU2  = 0.0e0;
    dyp_w_term_dP0  = 0.5e0 * sqrt(rho0 / (mu0 * du0));
    dyp_w_term_dP1  = -0.5e0 * sqrt(p * du0 / (R * mu0 * T0)) / (mu0 * T0) * (dmu0 * T0 + mu0);
    dyp_w_term_dP3  = -0.5e0 * sqrt(p * du0 / (R * mu0 * T0)) / (mu0 * T0) * (mu0/dmu0 + T0);

    dDterm_dU1      = y * (dyp_w_term_dU1 + dyp_sl_term_dU1)  / 0.2e1;
    dDterm_dU2      = y * (dyp_w_term_dU2 + dyp_sl_term_dU2)  / 0.2e1;
    dDterm_dP0      = y * (dyp_w_term_dP0 + dyp_sl_term_dP0)  / 0.2e1;
    dDterm_dP1      = y * (dyp_w_term_dP1 + dyp_sl_term_dP1)  / 0.2e1;
    dDterm_dP3      = y * (dyp_w_term_dP3 + dyp_sl_term_dP3)  / 0.2e1;
  }
  else {
    dyp_l_term_dU1  = -0.1e1 * sqrt(tau0 * p / R * T0) / (T * T * muT * muT) * (muT + T * dmuT);
    dyp_l_term_dU2  = -0.1e1 * sqrt(tau0 * p / R * T0) / (T * T * muT * muT) * (muT/dmuT + T);
    dyp_l_term_dP0  = 0.5e0 * sqrt(mu0 * p / R * T0 / du0) / (T * muT);
    dyp_l_term_dP1  = 0.5e0 * sqrt(du0 * p / (R * mu0 * T0)) / (muT * T) * (dmu0 * T0 + mu0);
    dyp_l_term_dP3  = 0.5e0 * sqrt(du0 * p / (R * mu0 * T0)) / (muT * T) * (mu0/dmu0 + T0);

    dDterm_dU1      = y * (dyp_l_term_dU1 + dyp_sl_term_dU1)  / 0.2e1;
    dDterm_dU2      = y * (dyp_l_term_dU2 + dyp_sl_term_dU2)  / 0.2e1;
    dDterm_dP0      = y * (dyp_l_term_dP0 + dyp_sl_term_dP0)  / 0.2e1;
    dDterm_dP1      = y * (dyp_l_term_dP1 + dyp_sl_term_dP1)  / 0.2e1;
    dDterm_dP3      = y * (dyp_l_term_dP3 + dyp_sl_term_dP3)  / 0.2e1;
  }

  /*
  dFdU[0] = d(du_dy0) / d(u)
  dFdU[1] = d(dT_dy)  / d(u)
  dFdU[2] = d(du_dy0) / d(T)
  dFdU[3] = d(dT_dy)  / d(T)
  dFdU[4] = d(du_dy0) / d(muT)
  dFdU[5] = d(dT_dy)  / d(muT)
  */
  double dF0_coef, dDterm_coef, dmu_turb_dU1, dF0dU1_term1, dF1_coef1, dmu_turb_dU2, dF0dU2_term1;
  dF0_coef      = (-0.1e1 * tau0) / ((muT + mu_turb) * (muT + mu_turb));
  dDterm_coef   = 0.2e1 * (0.1e1 - D_term) * D_term * (0.58823529411764705882e-1);
  dmu_turb_dU1  = -0.5e0 * mu_turb_coef / T;
  dF0dU1_term1  = y * vkcst * (D * dmu_turb_dU1 + mu_turb_coef * dDterm_coef * dDterm_dU1);
  dF1_coef1     = (-0.1e1) * (mu0 / Pr * dT0 - tau0 * u / cp) / ((muT / Pr + mu_turb / prandtlT) * (muT / Pr + mu_turb / prandtlT));
  dmu_turb_dU2  = dmu_turb_dU1 / dmuT;
  dF0dU2_term1  = y * vkcst * (D * dmu_turb_dU2 + mu_turb_coef * dDterm_coef * dDterm_dU2);

  dFdU[0] = 0.0e0;
  dFdU[1] = (-0.1e1 * tau0 / cp) / (muT / Pr + mu_turb / prandtlT);
  dFdU[2] = dF0_coef * (dmuT + dF0dU1_term1);
  dFdU[3] = dF1_coef1 * (dmuT / Pr + dF0dU1_term1 / prandtlT);
  dFdU[4] = dF0_coef * (0.1e1 + dF0dU2_term1);
  dFdU[5] = dF1_coef1 * (0.1e1 / Pr + dF0dU2_term1 / prandtlT);

  /*
  dFdP[0] = d(du_dy0) / d(du0)
  dFdP[1] = d(dT_dy)  / d(du0)
  dFdP[2] = d(du_dy0) / d(T0)
  dFdP[3] = d(dT_dy)  / d(T0)
  dFdP[4] = d(du_dy0) / d(dT0)
  dFdP[5] = d(dT_dy)  / d(dT0)
  dFdP[6] = d(du_dy0) / d(mu0)
  dFdP[7] = d(dT_dy)  / d(mu0)
  */
  double dF0dP0_term1, dF0dP0_term2, dF0_coefy, dmu_turb_dP0, dF1_denom, dF1dP0_term1, dF1dP0_coef1, dmu_turb_dP1, dF0dP1_term1, dF1dP1_term1,
  dF0dP3_term1, dF0dP3_term2, dmu_turb_dP3, dF1dP3_term1;

  dF0dP0_term1 = mu0 / (muT + mu_turb);
  dF0_coefy    = dF0_coef * vkcst * y;
  dmu_turb_dP0 = 0.5e0 * mu_turb_coef / du0;
  dF0dP0_term2 = D * dmu_turb_dP0 + mu_turb_coef * (dDterm_coef * dDterm_dP0);
  dF1_denom    = (muT / Pr + mu_turb / prandtlT);
  dF1dP0_term1 = (-0.1e1 * mu0 * u / cp) / dF1_denom;
  dF1dP0_coef1 = (mu0 * dT0 / Pr - tau0 * u / cp) / (dF1_denom * dF1_denom);
  dmu_turb_dP1 = 0.5e0 * mu_turb_coef * dmu0 / mu0;
  dF0dP1_term1 = dDterm_coef * dDterm_dP1 * mu_turb_coef + D * dmu_turb_dP1;
  dF1dP1_term1 = ((dmu0 * dT0 / Pr) - (du0 * dmu0 * u / cp)) / dF1_denom;
  dF0dP3_term1 = du0 / (muT + mu_turb);
  dmu_turb_dP3 = 0.5e0 * mu_turb_coef / mu0;
  dF0dP3_term2 = D * dmu_turb_dP3 + mu_turb_coef * (dDterm_coef * dDterm_dP3);
  dF1dP3_term1  = (dT0 / Pr - du0 * u / cp) / dF1_denom;

  dFdP[0] = dF0dP0_term1 - dF0_coefy * dF0dP0_term2;
  dFdP[1] = dF1dP0_term1 - dF1dP0_coef1 / prandtlT * y * vkcst * dF0dP0_term2;
  dFdP[2] = (dmu0 * du0) / (muT + mu_turb) + dF0_coefy * dF0dP1_term1;
  dFdP[3] = dF1dP1_term1 - dF1dP0_coef1 / prandtlT * y * vkcst * dF0dP1_term1;
  dFdP[4] = 0.0e0;
  dFdP[5] = (mu0 / Pr) / dF1_denom;
  dFdP[6] = dF0dP3_term1 - dF0_coefy * dF0dP3_term2;
  dFdP[7] = dF1dP3_term1 - dF1dP0_coef1 / prandtlT * y * vkcst * dF0dP3_term2;
}

//------------------------------------------------------------------------------

inline
void WallFcnODE::bc_RANS_SL_jacderivatives(double *ddudns, double *ddTdns, double U[3], double P[4], double y, double p, double *F, bool isIsothermal, int tag) {
  double du0, T0, dT0, mu0, u, T, muT, tau0, rhoT, rho0, mu_turb_coef, yp_w_term, yp_sl_term, yp_l_term, yp_term,
         D_term, D, mu_turb, cp;
  double dmu_turb_dp, dmu_turb_coef_dp, dyp_term_dp, dD_dp, dD_dy;
  double dmuT_dT, dyp_term_dT, dD_dT, dmu_turb_coef_dT, dmu_turb_dT;
  double ddudns_dp, ddudns_dut, ddudns_dT, ddudns_dy, ddudns_ddT0, ddTdns_dp, ddTdns_dut, ddTdns_dT, ddTdns_dy, ddTdns_ddT0;
  double gam = varFcn->getGamma(tag), R = gam - 1.; // R is nondimensional by R/Cv;
  double Pr = prandtl;
  /*
  U[3] = {u, T, muT};
  P[4] = {du0, T0, dT0, mu0};
  y    = distance to wall
  p    = constant pressure
  F    = {du_dy, dT_dy}, TO BE SOLVED
  NOTE: ddudns derivatives wrt G where
        G[4] = {p, ut, T, y = delta, dT0}
  NOTE: dT0 = dT_dy0 = dTdns
  NOTE: Tau_w = mu_w * du_w == tau0
  NOTE: vkcst = 0.41 (globally defined in WallFunctionCore.C)
  NOTE: prandtlT (globally defined in WallFunctionCore.C)
  */
  du0 = P[0];
  T0  = P[1];
  dT0 = P[2];
  mu0 = P[3];
  u   = U[0];
  T   = U[1];
  muT = U[2];

  tau0 = mu0 * du0;
  rhoT = p / (T * R);
  rho0 = p / (T0 * R);

  mu_turb_coef = sqrt(tau0 * rhoT);
  yp_w_term    = sqrt(du0 / mu0 * rho0);
  yp_sl_term   = sqrt(tau0 * rhoT) / muT;
  yp_l_term    = sqrt(tau0 * p * T0 / R) / (T * muT);
  yp_term      = std::min((yp_w_term + yp_sl_term), (yp_l_term + yp_sl_term)) / 0.2e1;
  D_term       = exp(-0.58823529411764705882e-1 * y * yp_term);
  D            = pow(0.1e1 - D_term, 0.2e1);
  mu_turb      = y * mu_turb_coef * D * vkcst;
  cp           = gam;

  F[0] = tau0 / (muT + mu_turb);
  F[1] = (mu0 / Pr * dT0 - tau0 * u / cp) / (muT / Pr + mu_turb / prandtlT);

  dmu_turb_coef_dp = 0.5e0 * mu_turb_coef / p;
  dyp_term_dp  = 0.5e0 * yp_term / p;
  dD_dp        = 0.2e1 * (0.1e1 - D_term) * D_term * (y * 0.58823529411764705882e-1) * dyp_term_dp;
  dmu_turb_dp  = y * vkcst * (dmu_turb_coef_dp * D + mu_turb_coef * dD_dp);

  dmuT_dT      = viscoFcn->compute_dmu(T, tag) * ooreynolds_mu;
  dyp_term_dT  = (-0.5e0) * yp_sl_term / T - sqrt(tau0 * rhoT) / (muT * muT) * dmuT_dT;
  dD_dT        = 0.2e1 * (0.1e1 - D_term) * D_term * (y * 0.58823529411764705882e-1) * dyp_term_dT;
  dmu_turb_coef_dT = (-0.5e0) * mu_turb_coef / T;
  dmu_turb_dT  = y * vkcst * D * dmu_turb_coef_dT + y * vkcst * mu_turb_coef * dD_dT;

  dD_dy        = 0.2e1 * (0.1e1 - D_term) * D_term * 0.58823529411764705882e-1 * yp_term;

  ddudns_dp   = (-0.1e1) * F[0] / (muT + mu_turb) * dmu_turb_dp;
  ddudns_dut  = 0.0;
  ddudns_dT   = (-0.1e1) * F[0] / (muT + mu_turb) * (dmuT_dT + dmu_turb_dT);
  ddudns_dy   = (-0.1e1) * F[0] / (muT + mu_turb) * mu_turb_coef * vkcst * (D + y * dD_dy);
  ddudns_ddT0 = 0.0;

  ddTdns_dp   = (-0.1e1) * F[1] / (muT / Pr + mu_turb / prandtlT) * dmu_turb_dp / prandtlT; //0.0;
  ddTdns_dut  = (-0.1e1) * (tau0 / cp) / (muT / Pr + mu_turb / prandtlT); //0.0;
  ddTdns_dy   = (-0.1e1) * F[1] / (muT / Pr + mu_turb / prandtlT) * mu_turb_coef * vkcst * (D + y * dD_dy) / prandtlT; // 0.0;
  ddTdns_dT   = (-0.1e1) * F[1] / (muT / Pr + mu_turb / prandtlT) * (dmuT_dT / Pr + dmu_turb_dT / prandtlT);
  ddTdns_ddT0 = (mu0 / Pr) / (muT / Pr + mu_turb / prandtlT); // 0.0;

  if(isIsothermal) { //To check and validate still
    ddTdns_dp   = 0.0;
    ddTdns_dut  = tau0 / cp * Pr / mu0;
    ddTdns_dy   = 0.0;
    ddTdns_ddT0 = 1.0;
  }

  ddudns[0] = ddudns_dp;
  ddudns[1] = ddudns_dut;
  ddudns[2] = ddudns_dT;
  ddudns[3] = ddudns_dy;
  ddudns[4] = ddudns_ddT0;

  ddTdns[0] = ddTdns_dp;
  ddTdns[1] = ddTdns_dut;
  ddTdns[2] = ddTdns_dT;
  ddTdns[3] = ddTdns_dy;
  ddTdns[4] = ddTdns_ddT0;
}

//------------------------------------------------------------------------------

inline
void WallFcnODE::adiabatic_wall_model(const double p, const double u, const double T, const double y,
                                      const double Tw, double& dT_dy0, double *x, double *f_out, int tag) {
  /*
  :param p : constant pressure
  :param u : velocity at the outer layer
  :param T : temperature at the outer layer
  :param y : distance to wall
  :param dT_dy0 : dT/dy(y=0), heat flux at the wall

  Assume we have u0, tau_w, T0, q_w
  param R, Pr,
  :return: x =  {du_dy0, T0};
  */
  // Flow parameters
  double gam = varFcn->getGamma(tag), R = gam - 1.; // R is nondimensional by R/Cv;
  double Pr = prandtl;

  auto func = [&](const double *x, const double y, const double *para, double *f) {
    // x = initial condition x0 = [u, T, du0/dp0, dT0/dp0, du0/dp1, dT0/dp1], here p0 = du_dy, p1 = T
    // para = {du_dy0, T0, dT_dy0, p, gam, R, Pr};
    double u   = x[0],    T  = x[1],    d1  = x[2],    d2 = x[3],    d3  = x[4],    d4 = x[5];
    double du0 = para[0], T0 = para[1], dT0 = para[2], p  = para[3], gam = para[4], R  = para[5], Pr = para[6];
    double mu0 = viscoFcn->compute_mu(T0, tag) * ooreynolds_mu,  muT  = viscoFcn->compute_mu(T, tag) * ooreynolds_mu,
          dmu0 = viscoFcn->compute_dmu(T0, tag) * ooreynolds_mu, dmuT = viscoFcn->compute_dmu(T, tag) * ooreynolds_mu;
    double F[2] = {0., 0.};
    double dFdU[6], dFdP[8];
    double U[3] = {u, T, muT};
    double P[4] = {du0, T0, dT0, mu0};
    bc_RANS_SL(U, P, y, p, gam, R, Pr, F);

    //dF = dF/d(du_dy, T) = \partial_F/\partial(u,T,muw)*d(u,T,muw)/d(du_dy, T) + \partial_F/\partial(du_dy, T)
    double dmuVect[2] = {dmu0, dmuT};
    bc_RANS_SL_deriv(U, P, y, p, gam, R, Pr, dFdU, dFdP, dmuVect);
    double dF[4] = {dFdU[0] * d1 + dFdU[2] * d2 + dFdU[4] * d2 * dmuT + dFdP[0],
                    dFdU[1] * d1 + dFdU[3] * d2 + dFdU[5] * d2 * dmuT + dFdP[1],
                    dFdU[0] * d3 + dFdU[2] * d4 + dFdU[4] * d4 * dmuT + dFdP[2] + dFdP[6] * dmu0, // For isothermal, no final dmu0 term
                    dFdU[1] * d3 + dFdU[3] * d4 + dFdU[5] * d4 * dmuT + dFdP[3] + dFdP[7] * dmu0};// For isothermal, no final dmu0 term

    f[0] = F[0];
    f[1] = F[1];
    f[2] = dF[0];
    f[3] = dF[1];
    f[4] = dF[2];
    f[5] = dF[3];

  };

  auto RK_solver = [&](double *x, const double *para, const double *t, const int N = 20) {
    /*
    optimal second order TVD Runge-Kutta method
    dx/dt = func(x,t, para)
    x(t[0]) = x0
    solve for x(t[1])
    :param n: divide time domain to n segments
    :return:
    */
    int nx = 6;
    double f[nx], xtemp[nx];
    double t0 = t[0], tn = t[1], ta, tb;
    double dt = (tn - t0) / N;
    for(int i = 0; i < N; i++) {
      ta      = t0 + i * dt, tb = t0 + (i + 1) * dt;
      func(x, ta, para, f);
      for(int j  = 0; j < nx; j++)
        xtemp[j] = x[j] + (tb - ta) * f[j];
      func(xtemp, tb, para, f);
      for(int j  = 0; j < nx; j++)
        x[j]     = 0.5 * (x[j] + xtemp[j] + (tb - ta) * f[j]);
    }
  };

  auto adiabatic_bc_func = [&](const double *x, const double *para, double *f, double *df, double *f_out) {
    /*
    Assuming equilibrium, the unresolved inner layer is modeled by solving RANS boundary layer equation, in an overlapping
    layer between y = 0 and y = h
    check the equation in the paper "Wall-modeling in large eddy simulation: Length scales, grid resolution, and
    accuracy" by Soshi Kawai, and Johan Larsson
    the equation can be rewrite to ODE format
    du/dy = f1(u,T, muw, du0, T0, dT0, mu0, y, p, gam, R, Pr)
    dT/dy = f2(u,T, muw, du0, T0, dT0, mu0, y, p, gam, R, Pr)

    :param x: for adiabatic wall the parameters we need to solve are [du_dy0, T0],
    :param para: parameters, we know, dT_dy0; y; p(y); u(y); T(y); gam; R; Pr; N: N+1 points for RK integrator

    :return: solve the ODE, we can get u(y) and T(y)
         f  = u(y,...) - u, T(y, ...) - T
         df = df/dx
    */
    double du_dy0 = x[0], T0 = x[1];
    double dT_dy0 = para[0], y = para[1], p = para[2], u = para[3], T = para[4], gam = para[5], R = para[6], Pr = para[7];
    // initial condition x0 = [u, T, du/dp0, dT/dp0, du/dp1, dT/dp1], here p0 = du_dy0, p1 = T
    double x0[6] = {0.0, T0, 0.0, 0.0, 0.0, 1.0};
    double t[2]  = {0, y};
    double para1[7] = {du_dy0, T0, dT_dy0, p, gam, R, Pr};

    RK_solver(x0, para1, t);

    f[0]  = x0[0] - u;
    f[1]  = x0[1] - T;
    df[0] = x0[2];
    df[1] = x0[3];
    df[2] = x0[4];
    df[3] = x0[5];

    f_out[0] = x0[0];
    f_out[1] = x0[1];
    f_out[2] = para1[0];
    f_out[3] = para1[1];
    f_out[4] = para1[2];
  };

  auto nonlinear_solver = [&](const double *para, double *x, double *f_out, const double eps=1e-5, const int maxite=100) {
    /*
    Nonlinear newton solver,
    :param x: initial guess, a 1D array of 2 elements!!!
    :param func: solve func(x, para) = 0
    :param eps: stop criterion, |dx| < eps or |dx|/|x| < eps
    :param maxite: stop criterion max iteration
    :return x = {du_dy, T}
    */
    double dx[2], dx_norm, x0[2] = {x[0], x[1]}, x0_norm = sqrt(x0[0]*x0[0] + x0[1]*x0[1]);
    double f[2], df[4], detf;
    for(int i = 0; i < maxite; i++) {
      adiabatic_bc_func(x, para, f, df, f_out);
      detf  = df[0] * df[3] - df[1] * df[2];
      dx[0] = ( df[3]*f[0] - df[2]*f[1])/detf;
      dx[1] = (-df[1]*f[0] + df[0]*f[1])/detf;
      x[0] -= dx[0];
      x[1] -= dx[1];
      dx_norm = sqrt(dx[0]*dx[0] + dx[1]*dx[1]);
      if(dx_norm < eps || dx_norm/sqrt(x0[0]*x0[0] + x0[1]*x0[1]) < eps)
        return;
    }
    std::cerr << "*** Error: Nonlinear solver diverges, |dx| is " << dx_norm <<  " |dx|/|x0| is " <<  dx_norm/x0_norm
              << " expect eps is " <<  eps << std::endl;
  };

  // ODE parameters
  double du_dy0 = u / y, T0 = T; // initial guess
  x[0] = du_dy0, x[1] = T0;
  double para[8] = {dT_dy0, y, p, u, T, gam, R, Pr};

  nonlinear_solver(para, x, f_out);
}

//------------------------------------------------------------------------------

inline
void WallFcnODE::isothermal_wall_model(const double p, const double u, const double T, const double y,
                                       const double T0, double& dT_dy0, double *x, double *f_out, int tag) {
  /*
  :param p : constant pressure
  :param u : velocity at the outer layer
  :param T : temperature at the outer layer
  :param y : distance to wall
  :param T0 : wall temperature

  Assume we have u0, tau_w, T0, q_w
  param R, Pr,
  :return: x = {du_dy0; x[1] = dT_dy0;}
  */
  // Flow parameters
  double gam = varFcn->getGamma(tag), R = gam - 1.; // R is nondimensional by R/Cv;
  double Pr = prandtl;

  auto func = [&](const double *x, const double y, const double *para, double *f) {
    // x = initial condition x0 = [u,T, du/dp0, dT/dp0, du/dp1, dT/dp1], here p0 = du_dy, p1 = dT_dy
    // para = {du_dy0, T0, dT_dy0, p, gam, R, Pr};
    double u = x[0],      T  = x[1],    d1  = x[2],   d2 = x[3],     d3 = x[4],   d4 = x[5];
    double du0 = para[0], T0 = para[1], dT0 = para[2], p = para[3], gam = para[4], R = para[5], Pr = para[6];
    double mu0  = viscoFcn->compute_mu(T0, tag) * ooreynolds_mu,   muT = viscoFcn->compute_mu(T, tag) * ooreynolds_mu,
           dmu0 = viscoFcn->compute_dmu(T0, tag) * ooreynolds_mu, dmuT = viscoFcn->compute_dmu(T, tag)* ooreynolds_mu;

    double F[2] = {0., 0.};
    double dFdU[6], dFdP[8];
    double U[3] = {u, T, muT};
    double P[4] = {du0, T0, dT0, mu0};

    bc_RANS_SL(U, P, y, p, gam, R, Pr, F);

    // dF = dF/d(du_dy, dT_dy) = \partial_F/\partial(u,T)*d(u,T)/d(du_dy, dT_dy) + \partial_F/\partial(du_dy, dT_dy)

    double dmuVect[2] = {dmu0, dmuT};
    bc_RANS_SL_deriv(U, P, y, p, gam, R, Pr, dFdU, dFdP, dmuVect);

    double dF[4] = {dFdU[0] * d1 + dFdU[2] * d2 + dFdU[4] * d2 * dmuT + dFdP[0],
                    dFdU[1] * d1 + dFdU[3] * d2 + dFdU[5] * d2 * dmuT + dFdP[1],
                    dFdU[0] * d3 + dFdU[2] * d4 + dFdU[4] * d4 * dmuT + dFdP[4],
                    dFdU[1] * d3 + dFdU[3] * d4 + dFdU[5] * d4 * dmuT + dFdP[5]};

    f[0] = F[0];
    f[1] = F[1];
    f[2] = dF[0]; // = dF[0]/d(du_dy0)
    f[3] = dF[1]; // = dF[1]/d(du_dy0)
    f[4] = dF[2]; // = dF[0]/d(dT_dy0)
    f[5] = dF[3]; // = dF[1]/d(dT_dy0)
  };

  auto RK_solver = [&](double *x, const double *para, const double *t, const int N = 20) {
    /*
    optimal second order TVD Runge-Kutta method
    dx/dt = func(x,t, para)
    x(t[0]) = x0
    solve for x(t[1])
    :param n: divide time domain to n segments
    :return:
    */
    int nx = 6;
    double f[nx], xtemp[nx];
    double t0 = t[0], tn = t[1], ta, tb;
    double dt = (tn - t0) / N;
    for(int i = 0; i < N; i++) {
      ta = t0 + i * dt, tb = t0 + (i + 1) * dt;
      func(x, ta, para, f);
      for(int j = 0; j < nx; j++)
        xtemp[j] = x[j] + (tb - ta) * f[j];
      func(xtemp, tb, para, f);
      for(int j = 0; j < nx; j++)
        x[j] = 0.5 * (x[j] + xtemp[j] + (tb - ta) * f[j]);
    }
  };

  auto isothermal_bc_func = [&](const double *x, const double *para, double *f, double *df, double *f_out) {
    /*
    Assuming equilibrium, the unresolved inner layer is modeled by solving RANS boundary layer equation, in an overlapping
    layer between y = 0 and y = h
    check the equation in the paper "Wall-modeling in large eddy simulation: Length scales, grid resolution, and
    accuracy" by Soshi Kawai, and Johan Larsson
    the equation can be rewrite to ODE format
    du/dy = f1(u,T, muw, du0, T0, dT0, mu0, y, p, gam, R, Pr)
    dT/dy = f2(u,T, muw, du0, T0, dT0, mu0, y, p, gam, R, Pr)

    :param x: for isothermal wall the parameters we need to solve are [du_dy0, dT_dy0],
    :param para: parameters, we know, T0; y; p(y); u(y); T(y); gam; R; Pr; N: N+1 points for RK integrator

    :return: solve the ODE, we can get u(y) and T(y)
             f  = u(y,...) - u, T(y, ...) - T
             df = df/dx
    */
    /*
    For Isothermal: x[1] -> dT_dy|_w
    For Adiabatic:  x[1] -> T0
    For Isothermal: para[0] -> T0
    For Adiabatic:  para[0] -> dT_dy|_w
    */
    double du_dy0 = x[0], dT_dy0 = x[1];
    double T0  = para[0], y = para[1], p  = para[2], u = para[3], T = para[4],
           gam = para[5], R = para[6], Pr = para[7];

    // initial condition x0 = [u0,T0, du0/dp0, dT0/dp0, du0/dp1, dT0/dp1], here p0 = du_dy0, p1 = dT_dy0
    double x0[6] = {0.0, T0, 0.0, 0.0, 0.0, 0.0};
    double t[2]  = {0, y};

    double para1[7] = {du_dy0, T0, dT_dy0, p, gam, R, Pr};

    RK_solver(x0, para1, t);

    f[0]  = x0[0] - u;
    f[1]  = x0[1] - T;
    df[0] = x0[2];
    df[1] = x0[3];
    df[2] = x0[4];
    df[3] = x0[5];

    f_out[0] = x0[0];   // u
    f_out[1] = x0[1];   // T
    f_out[2] = para1[0]; // du_dy0
    f_out[3] = para1[1]; // T0
    f_out[4] = para1[2]; // dT_dy0
  };

  auto nonlinear_solver = [&](const double *para, double *x, double *f_out, const double eps = 1e-5, const int maxite = 100) {
    /*
    Nonlinear newton solver,
    :param x: initial guess, (muw * dudns) 1D array of 2 elements!!!
    :param func: solve func(x, para) = 0
    :param eps: stop criterion, |dx| < eps or |dx|/|x| < eps
    :param maxite: stop criterion max iteration
    :return x
    */
    double dx[2], dx_norm, x0[2] = {x[0], x[1]}, x0_norm = sqrt(x0[0] * x0[0] + x0[1] * x0[1]);
    double f[2], df[4], detf;
    for(int i = 0; i < maxite; i++) {
      isothermal_bc_func(x, para, f, df, f_out);
      detf  = df[0] * df[3] - df[1] * df[2];
      dx[0] = (df[3] * f[0] - df[2] * f[1]) / detf;
      dx[1] = (-df[1] * f[0] + df[0] * f[1]) / detf;
      x[0] -= dx[0];
      x[1] -= dx[1];
      dx_norm = sqrt(dx[0] * dx[0] + dx[1] * dx[1]);
      if(dx_norm < eps || dx_norm/sqrt(x0[0]*x0[0] + x0[1]*x0[1]) < eps)
        return;
    }

    std::cerr << "*** Error: Nonlinear solver diverges, |dx| is " << dx_norm << " |dx|/|x0| is " << dx_norm / x0_norm
              << " expect eps is " << eps << std::endl;
  };

  // ODE parameters
  double du_dy0 = u / y;
  dT_dy0 = (T - T0) / y; // guess
  x[0] = du_dy0, x[1] = dT_dy0;
  double para[8] = {T0, y, p, u, T, gam, R, Pr};

  nonlinear_solver(para, x, f_out);
  dT_dy0 = x[1];
}

//------------------------------------------------------------------------------

inline
Vec3D WallFcnODE::computeForce(Vec3D& normal, double d2wall[3], double *Vwall, double *V[3], int tag) {
  double delta, dT, rhow, muw, gamw;
  Vec3D du, uw;
  computeFaceValues(d2wall, Vwall, V, delta, du, dT, uw, rhow, muw, gamw, tag);
  muw          = muw * ooreynolds_mu;
  double kappa = muw * ooprandtl * gamw;
  double norm  = sqrt(normal * normal);
  Vec3D n      = (1.0 / norm) * normal;
  Vec3D t      = computeTangentVector(n, du);
  double T = third * (varFcn->computeTemperature(V[0], tag) +
                      varFcn->computeTemperature(V[1], tag) +
                      varFcn->computeTemperature(V[2], tag));
  double ut = du * t;
  // initialize isothermal and adiabatic
  bool isIsothermal = false;
  dT           = (-1.*0.5*prandtlT*ut*ut/gamw); //assumes adiabatic
  double Tw    = T - dT;
  if(codeWall == BcsWallData::ISOTHERMAL) {
    isIsothermal = true;
    Tw           = isoTemp;
    dT           = T - Tw;
  }
  double *Vwall_new = Vwall;
  varFcn->getV4FromTemperature(Vwall_new, Tw, tag);
  double pwall = Vwall_new[4];
  double p     = third * (varFcn->getPressure(V[0], tag) +
                          varFcn->getPressure(V[1], tag) +
                          varFcn->getPressure(V[2], tag));
  double dudns, dTdns = 0.0;
  computeNormalGradientLES(T, p, du, t, Tw, dTdns, delta, dudns, isIsothermal, tag);

  double mu_turb_term = rhow * vkcst * vkcst * delta * delta * dudns;
  Vec3D force = - mu_turb_term * dudns * norm * t;
  return force;
}

//------------------------------------------------------------------------------

inline
double WallFcnODE::computeHeatPower(Vec3D& normal, double d2wall[3], double *Vwall, double *V[3], int tag) {
  double delta, dT, rhow, muw, gamw;
  Vec3D du, uw;
  computeFaceValues(d2wall, Vwall, V, delta, du, dT, uw, rhow, muw, gamw, tag);
  muw          = muw * ooreynolds_mu;
  double kappa = muw * ooprandtl * gamw;
  double norm  = sqrt(normal * normal);
  Vec3D n      = (1.0 / norm) * normal;
  Vec3D t      = computeTangentVector(n, du);
  double T = third * (varFcn->computeTemperature(V[0], tag) +
                      varFcn->computeTemperature(V[1], tag) +
                      varFcn->computeTemperature(V[2], tag));
  double ut = du * t;
  // initialize isothermal and adiabatic
  bool isIsothermal = false;
  dT           = (-1.*0.5*prandtlT*ut*ut/gamw); //assumes adiabatic
  double Tw    = T - dT;
  if(codeWall == BcsWallData::ISOTHERMAL) {
    isIsothermal = true;
    Tw           = isoTemp;
    dT           = T - Tw;
  }
  double *Vwall_new = Vwall;
  varFcn->getV4FromTemperature(Vwall_new, Tw, tag);
  double pwall = Vwall_new[4];
  double p     = third * (varFcn->getPressure(V[0], tag) +
                          varFcn->getPressure(V[1], tag) +
                          varFcn->getPressure(V[2], tag));
  double dudns, dTdns = 0.0;
  computeNormalGradientLES(T, p, du, t, Tw, dTdns, delta, dudns, isIsothermal, tag);
  double hp = - kappa * dTdns * norm;
  return hp;
}

//------------------------------------------------------------------------------

inline
double WallFcnODE::computeInterfaceWork(Vec3D& normal, double d2wall[3], double *Vwall, double *V[3], int tag) {
  double delta, dT, rhow, muw, gamw;
  Vec3D du, uw;
  computeFaceValues(d2wall, Vwall, V, delta, du, dT, uw, rhow, muw, gamw, tag);
  muw          = muw * ooreynolds_mu;
  double kappa = muw * ooprandtl * gamw;
  double norm  = sqrt(normal * normal);
  Vec3D n      = (1.0 / norm) * normal;
  Vec3D t      = computeTangentVector(n, du);
  double T = third * (varFcn->computeTemperature(V[0], tag) +
                      varFcn->computeTemperature(V[1], tag) +
                      varFcn->computeTemperature(V[2], tag));
  double ut = du * t;
  // initialize isothermal and adiabatic
  bool isIsothermal = false;
  dT           = (-1.*0.5*prandtlT*ut*ut/gamw); //assumes adiabatic
  double Tw    = T - dT;
  if(codeWall == BcsWallData::ISOTHERMAL) {
    isIsothermal = true;
    Tw           = isoTemp;
    dT           = T - Tw;
  }
  double *Vwall_new = Vwall;
  varFcn->getV4FromTemperature(Vwall_new, Tw, tag);
  double pwall = Vwall_new[4];
  double p     = third * (varFcn->getPressure(V[0], tag) +
                          varFcn->getPressure(V[1], tag) +
                          varFcn->getPressure(V[2], tag));
  double dudns, dTdns = 0.0;
  computeNormalGradientLES(T, p, du, t, Tw, dTdns, delta, dudns, isIsothermal, tag);

  double mu_turb_term = rhow * vkcst * vkcst * delta * delta * dudns;
  double W = - mu_turb_term * dudns * norm * (uw * t);
  return W;
}

//------------------------------------------------------------------------------

inline
double WallFcnODE::computeDeltaPlus(Vec3D& normal, double d2wall[3], double *Vwall, double *V[3], int tag) {
  double delta, dT, rho, mu, gam;
  Vec3D du, u;
  computeFaceValues(d2wall, Vwall, V, delta, du, dT, u, rho, mu, gam, tag);
  mu           = mu * ooreynolds_mu;
  double kappa = mu * ooprandtl * gam;
  double norm  = sqrt(normal * normal);
  Vec3D n      = (1.0 / norm) * normal;
  Vec3D t      = computeTangentVector(n, du);
  double T = third * (varFcn->computeTemperature(V[0], tag) +
                      varFcn->computeTemperature(V[1], tag) +
                      varFcn->computeTemperature(V[2], tag));
  double ut = du * t;
  // initialize isothermal and adiabatic
  bool isIsothermal = false;
  dT           = (-1.*0.5*prandtlT*ut*ut/gam); //assumes adiabatic
  double Tw    = T - dT;
  if(codeWall == BcsWallData::ISOTHERMAL) {
    isIsothermal = true;
    Tw           = isoTemp;
    dT           = T - Tw;
  }
  double *Vwall_new = Vwall;
  varFcn->getV4FromTemperature(Vwall_new, Tw, tag);
  double pwall = Vwall_new[4];
  double mu_w  = viscoFcn->compute_mu(Tw, tag) * ooreynolds_mu;
  double p     = third * (varFcn->getPressure(V[0], tag) +
                          varFcn->getPressure(V[1], tag) +
                          varFcn->getPressure(V[2], tag));
  double rho_w = p / (gam * Tw);
  double dudns, dTdns = 0.0;
  computeNormalGradientLES(T, p, du, t, Tw, dTdns, delta, dudns, isIsothermal, tag);
  double tau_w = rho * vkcst * vkcst * delta * delta * dudns * dudns;
  double yp_w  = sqrt(rho_w * tau_w) / mu_w;
  double yp_l  = rho * sqrt(tau_w / rho_w) / mu;
  double yp_sl =  sqrt(rho * tau_w) / mu;

  double yp_mm_new = std::min((yp_w + yp_sl), (yp_l + yp_sl)) / 0.2e1 * delta;
  double yp_w_new = delta * yp_w;
  double yp_l_new = delta * yp_l;
  double dp = yp_mm_new;
  return dp;
}

//------------------------------------------------------------------------------

inline
double WallFcnODE::computeDeltaPlus(Vec3D& normal, double delta, Vec3D& uw, double *V, int tag) {
  double rho  = varFcn->getDensity(V, tag);
  Vec3D du    = varFcn->getVelocity(V, tag) - uw;
  double T    = varFcn->computeTemperature(V, tag);
  double mu   = viscoFcn->compute_mu(T, tag);
  double norm = sqrt(normal * normal);
  Vec3D n     = (1.0 / norm) * normal;
  Vec3D t     = computeTangentVector(n, du);
  double gam  = varFcn->specificHeatCstPressure(tag);
  mu           = mu * ooreynolds_mu;
  double kappa = mu * ooprandtl * gam;
  double Tw    = varFcn->computeTemperature(uw, tag);
  double dT    = T - Tw; //// only solves isothermal case. As opposed to adiabatic wherein Tw = T_adiabatic_wall.
  double p     = varFcn->getPressure(V, tag);
  double dudns, dTdns = 0.0;
  bool isIsothermal = true;
  computeNormalGradientLES(T, p, du, t, Tw, dTdns, delta, dudns, isIsothermal, tag);

  double dp, dp_w, dp_sl, dp_l;
  double yp = sqrt(rho * dudns / mu);
  double yp_new = delta * yp;
  dp = yp_new;
  return dp;
}

//------------------------------------------------------------------------------

#endif

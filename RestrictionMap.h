#ifndef _RESTRICTION_MAP_H_
#define _RESTRICTION_MAP_H_

#include <functional>
#include <vector>
#include <set>

#include <DistInfo.h>

class Domain;
class Timer;
class SubDomain;

template<class Scalar> class DistVec;
template<class Scalar, int dim> class DistSVec;
template<class MatrixType> class DistMat;
template<class Scalar> class DenseMat;

//------------------------------------------------------------------------------

class RestrictionMap {

  int numLocSub;
  DistInfo& origDistInfo;
  DistInfo restrictDistInfo;
  std::vector<std::vector<int>> restrictedToOrig, restrictedToOrigGrads;
  std::vector<std::vector<double>> restrictedWeights;
  const bool weighted;

  Timer *timer;
  Domain *domain;
  SubDomain **subDomain;

 private:
  void initialize();

 public:
  RestrictionMap(Domain *, bool, std::function<void(std::vector<int>&, std::vector<int>&)>,
                 std::function<void(std::vector<double>&)>);
  RestrictionMap(Domain *, std::vector<std::set<int>>&);
  ~RestrictionMap() {}

  DistInfo& getRestrictedDistInfo() {
    return restrictDistInfo;
  }

  void finalize();

  void restriction(DistVec<bool>&, DistVec<bool>&);

  template<int dim>
  void restriction(DistSVec<double, dim>&, DistSVec<double, dim>&);

  template<int dim>
  void restriction(DistMat<DenseMat<double>>&, DistMat<DenseMat<double>>&);

  template<int dim>
  void restriction(DistMat<DenseMat<double>>& Vin, DistMat<DenseMat<double>>& Vout, std::vector<double>& componentScaling);


};

//------------------------------------------------------------------------------

#endif

#ifndef _EMBCOROT_SOLVER_H_
#define _EMBCOROT_SOLVER_H_

#include <IoData.h>
#include <DistVector.h>
#include <NodeData.h>
#include <Vector3D.h>

class MatchNodeSet;
class Domain;
class Communicator;
class BCApplier;
class DistLevelSetStructure;

//------------------------------------------------------------------------------

class EmbeddedCorotSolver {

  int numLocSub;

  Vec<Vec3D> Xs0;
  int (*stElem)[3];
  int *nodesType;
  int *surfaceID;

  DistSVec<double, 3> *X0;

  double cg0[3], cgN[3];
  double n[3];
  double R[3][3];

  IoData& ioData;
  Domain *domain;
  Communicator *com;

  BCApplier *meshMotionBCs;

  enum SymmetryAxis {NONE, AXIS_X, AXIS_Y, AXIS_Z} SymAxis;
  int masterSurfaceID;

  double cm0[3];
  double cmN[3];

  std::string corotmatrix;

  void computeRotGradAndJac(Vec<Vec3D>&, double[3][3],
                            double[3], double[3], double[3][3]);
  void computeRotMat(double *, double[3][3]);
  void rotLocVec(double[3][3], double[3]);
  void solveRotMat(double[3][3], double[3]);
  void computeNodeRot(double[3][3], DistSVec<double, 3>&, double[3], double[3]);

  void printRotMat(double[3][3]);

  void readRotationMatrix();
  void updateCM(double[3]);

 public:

  EmbeddedCorotSolver(IoData&, Domain *, DistLevelSetStructure *, int = -1, DistSVec<double, 3> * = nullptr, std::string = "");
  ~EmbeddedCorotSolver();

  void resize2();

  void applyProjector(DistSVec<double, 3>&);
  void computeMeanDXForSlidingPlane(DistSVec<double, 3>&, double[3]);

  void setup(Vec<Vec3D>&);
  void computeCG(Vec<Vec3D>&, double[3]);
  void solveDeltaRot(Vec<Vec3D>&, double[3][3], double[3]);
  void solve(Vec<Vec3D>&, DistSVec<double, 3>&);
  // setup computes the rotation and CG that will fit best for restarting

  void updateMomentArm(Vec3D&, Vec3D&);
  void getDeltaXCM(double[3]);
  void getRotationMatrix(double[3][3]);

};

//------------------------------------------------------------------------------

#endif

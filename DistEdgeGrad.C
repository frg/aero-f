#include <DistEdgeGrad.h>

#include <Domain.h>
#include <SubDomain.h>
#include <DistVector.h>
#include <Vector.h>
#include <Vector3D.h>

//------------------------------------------------------------------------------

template<int Dim>
void DistEdgeGrad::compute(int config, DistSVec<double, 3>& X, DistSVec<double, Dim>& V, DistSVec<double, Dim>& dVdx,
                           DistSVec<double, Dim>& dVdy, DistSVec<double, Dim>& dVdz) {
  const int dim = V.dim();  // note: Dim may be equal to Eigen::Dynamic
  initialize(config, X, dim);
  const int block = 2 * dim;
  // slaves compute interpolated gradients and fill v6Pat send buffer
  #pragma omp parallel for
  for(int iSub = 0; iSub < numLocSub; ++iSub) {
    for(auto& it : remoteV6Data[iSub]) {
      SubRecInfo<double> sInfo = v6Pat->getSendBuffer(subDomain[iSub]->getSndChannel()[it.first]);
      for(int k = 0; k < it.second.size(); ++k) {
        V6NodeData& data = it.second[k].first;
        Vec3D& rij = it.second[k].second;
        Elem& elem = subDomain[iSub]->getElems()[data.tet];
        double *ddV_u = sInfo.data + block * k;
        double *ddV_f = ddV_u + dim;
        subEdgeGrad[iSub]->precomputeUpwindGradient(elem, X(iSub), V(iSub), rij, ddV_u, dim);
        subEdgeGrad[iSub]->precomputeFaceGradient(data, elem, dVdx(iSub), dVdy(iSub), dVdz(iSub), rij, ddV_f, dim);
      }
    }
  }
  // exchange data to masters
  v6Pat->exchange();
  // copy receive buffer data to subEdgeGrad objects
  #pragma omp parallel for
  for(int iSub = 0; iSub < numLocSub; ++iSub) {
    auto& v6remote = subEdgeGrad[iSub]->getV6RemoteData();
    for(auto& it : remoteEdgeMap[iSub]) {
      SubRecInfo<double> sInfo = v6Pat->recData(subDomain[iSub]->getRcvChannel()[it.first]);
      for(int k = 0; k < it.second.size(); ++k) {
        const double *data = sInfo.data + block * k;
        std::copy(data, data + block, v6remote[it.second[k]]);
      }
    }
  }
}

//------------------------------------------------------------------------------

template<int Dim>
void DistEdgeGrad::computeDerivative(int config, DistSVec<double, 3>& X, DistSVec<double, 3>& dX, DistSVec<double, Dim>& V,
                                     DistSVec<double, Dim>& dV, DistSVec<double, Dim>& dVdx, DistSVec<double, Dim>& dVdy,
                                     DistSVec<double, Dim>& dVdz, DistSVec<double, Dim>& ddVdx, DistSVec<double, Dim>& ddVdy,
                                     DistSVec<double, Dim>& ddVdz) {
  std::cerr << "*** Error: DistEdgeGrad::computeDerivative is not implemented\n";
  exit(-1);
}

//------------------------------------------------------------------------------

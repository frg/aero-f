#ifndef _MESH_MOTION_SOLVER_H_
#define _MESH_MOTION_SOLVER_H_

#include <Domain.h>
#include <DistVector.h>
#include <NodeData.h>
#include <vector>

class BCApplier;
class MatchNodeSet;
class CorotSolver;
class Communicator;
class StiffMat;
class Timer;
class TsParameters;

#ifndef _KSPPREC_TMPL_
  #define _KSPPREC_TMPL_
  template<int Dim, class Scalar2 = double> class KspPrec;
#endif

template<class ProblemDescriptor, class KspDataType> class NewtonSolver;
#ifndef _KSPSLVR_TMPL_
#define _KSPSLVR_TMPL_
template<class VecType, class MvpOp, class PrecOp, class IoOp, class ScalarT = double> class KspSolver;
#endif

//------------------------------------------------------------------------------

class MeshMotionSolver {

 public:
  MeshMotionSolver() {}
  virtual ~MeshMotionSolver() {}

  virtual void resize1(NodeData *) {}
  virtual void resize2() {}

  virtual int solve(DistSVec<double, 3>&, DistSVec<double, 3>&) = 0;
  virtual int solveAdjoint(DistSVec<double, 3>&, DistSVec<double, 3>&) = 0;
  virtual void setup(DistSVec<double, 3>&) = 0;
  virtual void applyProjector(DistSVec<double, 3>&) {}
  virtual void applyProjectorTranspose(DistSVec<double, 3>& dX) {}
  virtual void setAdjointFlagOn() {}
  virtual void apply(DistSVec<double, 3>&, DistSVec<double, 3>&) {}
  virtual void solveLinearSystem(int, DistSVec<double, 3>&, DistSVec<double, 3>&) {}
  virtual void setOperators(DistSVec<double, 3>&) {}
  virtual void getDeltaXCM(double dXCM[3]) {dXCM[0] = 0.0; dXCM[1] = 0.0; dXCM[2] = 0.0;}
  virtual bool getRotationMatrix(double rotMat[3][3]) { return false; }
  virtual void updateMomentArm(Vec3D& x00, Vec3D& x0) {}
  virtual void resetCorotSolver() {}
  virtual BCApplier *getMeshMotionBCs() { return nullptr; }

};

//------------------------------------------------------------------------------

class TetMeshMotionSolver : public MeshMotionSolver {

 public:
  typedef DistSVec<double, 3> SolVecType;
  typedef DistVec<double> VolVecType;

 protected:
  IoData& ioData;
  DefoMeshMotionData& data;
  MatchNodeSet **matchNodes;

  DistSVec<double, 3> *F0;
  DistSVec<double, 3> *dX0;
  DistSVec<double, 3> *Xwall;

  CorotSolver *cs;

  StiffMat *mvp;
  KspPrec<3> *pc;

  KspSolver<DistSVec<double, 3>, StiffMat, KspPrec<3>, Communicator> *ksp;
  NewtonSolver<TetMeshMotionSolver, KspData> *ns;

  Domain *domain;
  Communicator *com;
  Timer *timer;

  bool adjointFlag;

  BCApplier *meshMotionBCs;

  std::vector<std::vector<double>> database;

 public:
  TetMeshMotionSolver(IoData&, MatchNodeSet **, Domain *, std::string = "");
  ~TetMeshMotionSolver();

  DistInfo& getVecInfo() const {
    return domain->getNodeDistInfo();
  }
  Communicator *getCommunicator() {
    return domain->getCommunicator();
  }
  TsParameters *getTsParams() {
    return NULL;
  }
  BCApplier *getMeshMotionBCs() {
    return meshMotionBCs;
  }

  void resize1(NodeData *);
  void resize2();

  int solve(DistSVec<double, 3>&, DistSVec<double, 3>&);
  int solveAdjoint(DistSVec<double, 3>&, DistSVec<double, 3>&);

  void setup(DistSVec<double, 3>&);

  void applyProjector(DistSVec<double, 3>&);
  void applyProjectorTranspose(DistSVec<double, 3>& X);
  void apply(DistSVec<double, 3>&, DistSVec<double, 3>&);

  void solveLinearSystem(int, DistSVec<double, 3>&, DistSVec<double, 3>&);
  void setOperators(DistSVec<double, 3>&);
  void computeFunction(DistSVec<double, 3>&, DistSVec<double, 3>&, bool);
  double meritFunction(DistSVec<double, 3>& F) { return F.norm(); }
  void computeJacobian(DistSVec<double, 3>&, DistSVec<double, 3>&) {}

  bool lineSearchCondition(double restrial, double res, double alpha, double c1) {
    return (restrial < sqrt(1 - 2.0 * alpha * c1) * res);
  }
  void adjustStep(DistSVec<double, 3>& dX, double c) {
    dX *= c;
  }
  void incrementSolution(DistSVec<double, 3>&, DistSVec<double, 3>&, DistSVec<double, 3>&);
  void decrementSolution(DistSVec<double, 3>&, DistSVec<double, 3>&);
  void resetSolution(DistSVec<double, 3>&, DistSVec<double, 3>&);
  int checkSolution(DistSVec<double, 3>&);
  int checkFailSafe(DistSVec<double, 3>&);
  void setAdjointFlagOn() {
    adjointFlag = true;
  }

  void printf(int, const char *, ...);
  void fprintf(FILE *, const char *, ...);

  virtual void getDeltaXCM(double[3]);

  virtual bool getRotationMatrix(double[3][3]);

  virtual void updateMomentArm(Vec3D&, Vec3D&);

  void resetCorotSolver();

 private:
  void initializeDatabase();
  double getConstraintViolation();
  void updateMultipliers();

};

//------------------------------------------------------------------------------

#endif

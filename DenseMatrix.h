#ifndef _DENSE_MATRIX_H_
#define _DENSE_MATRIX_H_

#include <DenseMatrixOps.h>

#include <cstdio>
#include <cmath>
#include <iostream>
#include <fstream>
#include <vector>
#include <map>

#include <Eigen/Dense>
#include <Eigen/Sparse>

//------------------------------------------------------------------------------
// DenseMat = Full Matrix class, stores a dynamically sized mxn matrix in
//            row-major storage
// Beware: certain member functions only work for the square matrix case (nxn)
template<class Scalar>
class DenseMat {

#ifdef USE_EIGEN3_DenseMat
  typedef Eigen::Matrix<Scalar,Eigen::Dynamic,Eigen::Dynamic,Eigen::RowMajor> MatrixType;
  typedef Eigen::Matrix<Scalar,Eigen::Dynamic,1,Eigen::ColMajor> VectorType;
  MatrixType v;
  Eigen::PartialPivLU<MatrixType> LU;
  Eigen::LLT<MatrixType> LLT;
  Eigen::JacobiSVD<MatrixType> SVD;
#else
  int nrow;         // number of rows
  int ncolumn;      // number of columns
  Scalar *v;        // pointer to matrix data
#endif
  typedef typename Eigen::NumTraits<Scalar>::Real RealScalarType;

 public:
#ifdef USE_EIGEN3_DenseMat
  // data structures keeping track of where the nonzero blocks are located in this matrix
  Eigen::PermutationMatrix<Eigen::Dynamic> groupColsPermMat;   // collects the columns to group them by sparsity pattern
  std::vector<int> patLabelToNumVecs;                          // number of columns in each block
  std::vector<std::vector<bool>> patLabelToColPat;             // sparsity pattern for each block
  bool sparsified;                                             // have sparsity data structures been made?
  int rows_per_block;                                          // block height (usually dim)
  int num_row_blocks;                                          // numCol()/rows_per_block, usually the number of nodes

  template<bool Add = false>
  void applyToMatrix(Eigen::SparseMatrix<Scalar>&, DenseMat<Scalar>&);
#endif

  // constructors
  DenseMat();
  DenseMat(int, int);
  DenseMat(const DenseMat&);

  // destructor
  ~DenseMat();

  // operators
  Scalar *operator[](int) const;
  void operator=(const DenseMat&);
  void operator=(const Scalar&);
  void operator*=(const Scalar&);
  void operator+=(Scalar *);
  void operator+=(const DenseMat&);
  void operator-=(const DenseMat&);

  // data access
  int numRow() const {
#ifdef USE_EIGEN3_DenseMat
    return v.rows();
#else
    return nrow;
#endif
  }

  int numCol() const {
#ifdef USE_EIGEN3_DenseMat
    return v.cols();
#else
    return ncolumn;
#endif
  }

  int nnz() const;

  Scalar *data() const {
#ifdef USE_EIGEN3_DenseMat
    return const_cast<Scalar *>(v.data());
#else
    return v;
#endif
  }

  void setZero();
  void resize(int, int);
  void print(std::ostream& = std::cerr, int = 6, const char * = "");
  void printRow(std::ostream&, int);
  void readMatrixFromFile(const char *, bool = false);

  bool isSparsified();
  void block_sparsify(int, std::vector<std::vector<int>>&);
  void copySparsityInfo(DenseMat<Scalar>&, bool = false);

  // arithmetic operations: matrix arguments as DenseMats, vectors as pointers
  // to contiguous memory (container agnostic)
  template<bool Add = false>
  void applyToMatrix(DenseMat<Scalar>&, DenseMat<Scalar>&);       // A * B
  template<bool Add = false>
  void applyTransToMatrix(DenseMat<Scalar>&, DenseMat<Scalar>&);  // A^T * B
  template<bool Add = false>
  void applyToMatrixTrans(DenseMat<Scalar>&, DenseMat<Scalar>&);  // A * B^T
  template<bool Add = false>
  void applyTransToSelf(DenseMat<Scalar>&);                       // A^T * A
  template<bool Add = false>
  void applyTransToSelfSparse(DenseMat<Scalar>&);                 // A^T * A sparse helper
  template<bool Add = false>
  void applyToVector(Scalar *, Scalar *);                         // A * b
  template<bool Add = false>
  void applyTransToVector(Scalar *, Scalar *);                    // A^T * b
  template<bool Add = false>
  void addTransToSelf(DenseMat<Scalar>&);                         // A^T + A

  Scalar norm();                // Frobenius norm
  template<bool Factor = true>
  void lu(Scalar *);            // solve A * x = b w/ LU
  template<bool Factor = true>
  void cholesky(Scalar *);      // solve A * x = b for SPD A w/ Cholesky
  template<bool UT = false>
  void svd(Scalar *, Scalar * = nullptr);           // thin SVD, X = U * Sigma * V^T
  template<bool Factor = true>
  int svd_solve(Scalar *, RealScalarType);
  template<bool Factor = true>
  int transpose_svd_solve(Scalar *, Scalar *, RealScalarType);
  void invert();

};

//------------------------------------------------------------------------------
#ifdef USE_EIGEN3_DenseMat
template<class Scalar>
inline
DenseMat<Scalar>::DenseMat() : v(0,0) {
  sparsified = false;
}

//------------------------------------------------------------------------------

template<class Scalar>
inline
DenseMat<Scalar>::DenseMat(int nr, int nc) : v(nr, nc) {
  sparsified = false;
}

//------------------------------------------------------------------------------

template<class Scalar>
inline
DenseMat<Scalar>::DenseMat(const DenseMat& m) : v(m.v) {
  sparsified = false;
}

//------------------------------------------------------------------------------

template<class Scalar>
template<bool Add>
inline
void DenseMat<Scalar>::applyToMatrix(Eigen::SparseMatrix<Scalar>& b, DenseMat<Scalar>& c) {
  assert(v.cols() == b.rows());
  if(Add) {
    c.v.noalias() += v*b;
  }
  else {
    c.v = v*b;
  }
}

//------------------------------------------------------------------------------
#else
template<class Scalar>
inline
DenseMat<Scalar>::DenseMat() {
  nrow    = 0;
  ncolumn = 0;
  v = 0;
}

//------------------------------------------------------------------------------

template<class Scalar>
inline
DenseMat<Scalar>::DenseMat(int nr, int nc) {
  nrow    = nr;
  ncolumn = nc;
  if(nrow * ncolumn == 0) {
    v = new Scalar[1];
  }
  else {
    v = new Scalar[nrow * ncolumn];
  }
}

//------------------------------------------------------------------------------

template<class Scalar>
inline
DenseMat<Scalar>::DenseMat(const DenseMat& m) {
  nrow    = m.numRow();
  ncolumn = m.numCol();
  v = new Scalar[nrow * ncolumn];
  int i;
  for(i = 0; i < nrow * ncolumn; ++i) {
    v[i] = m.v[i];
  }
}
#endif
//------------------------------------------------------------------------------

template<class Scalar>
inline
DenseMat<Scalar>::~DenseMat() {
#ifndef USE_EIGEN3_DenseMat
  if(v) {
    delete [] v;
  }
#endif
}

//------------------------------------------------------------------------------

template<class Scalar>
inline
Scalar *DenseMat<Scalar>::operator[](int i) const {
#ifdef USE_EIGEN3_DenseMat
  return const_cast<Scalar*>(v.row(i).data());
#else
  return v + i * ncolumn;
#endif
}

//------------------------------------------------------------------------------

template<class Scalar>
inline
void DenseMat<Scalar>::operator=(const DenseMat<Scalar>& m) {
#ifdef USE_EIGEN3_DenseMat
  v = m.v;
#else
  if(m.v == v) {
    return;
  }
  if(nrow != m.numRow() || ncolumn != m.numCol()) {
    if(v) {
      delete [] v;
    }
    nrow = m.numRow();
    ncolumn = m.numCol();
    v = new Scalar[nrow * ncolumn];
  }
  // copy data
  int i;
  for(i = 0; i < nrow * ncolumn; ++i) {
    v[i] = m.v[i];
  }
#endif
}

//------------------------------------------------------------------------------

template<class Scalar>
inline
void DenseMat<Scalar>::operator=(const Scalar& c) {
#ifdef USE_EIGEN3_DenseMat
  v.setConstant(c);
#else
  int i;
  for(i = 0; i < nrow * ncolumn; ++i) {
    v[i] = c;
  }
#endif
}

//------------------------------------------------------------------------------

template<class Scalar>
inline
void DenseMat<Scalar>::operator*=(const Scalar& c) {
#ifdef USE_EIGEN3_DenseMat
  v *= c;
#else
  int i;
  for(i = 0; i < nrow * ncolumn; ++i) {
    v[i] *= c;
  }
#endif
}

//------------------------------------------------------------------------------
// add to main diagonal
template<class Scalar>
inline
void DenseMat<Scalar>::operator+=(Scalar *c) {
#ifdef USE_EIGEN3_DenseMat
  Eigen::Diagonal<MatrixType> diagonal = v.diagonal();
  Eigen::Map<VectorType> C(c, diagonal.rows(), 1);
  diagonal += C;
#else
  int i;
  int size = (nrow <= ncolumn) ? nrow : ncolumn;
  for(i = 0; i < size; ++i) {
    (*this)[i][i] += c[i];
  }
#endif
}

//------------------------------------------------------------------------------

template<class Scalar>
inline
void DenseMat<Scalar>::operator+=(const DenseMat<Scalar>& m) {
#ifdef USE_EIGEN3_DenseMat
  assert(v.rows() == m.numRow() && v.cols() == m.numCol());
  v += m.v;
#else
  assert(nrow == m.numRow() && ncolumn == m.numCol());
  int i;
  for(i = 0; i < nrow * ncolumn; ++i) {
    v[i] += m.v[i];
  }
#endif
}

//------------------------------------------------------------------------------

template<class Scalar>
inline
void DenseMat<Scalar>::operator-=(const DenseMat<Scalar>& m) {
#ifdef USE_EIGEN3_DenseMat
  assert(v.rows() == m.numRow() && v.cols() == m.numCol());
  v -= m.v;
#else
  assert(nrow == m.numRow() && ncolumn == m.numCol());
  int i;
  for(i = 0; i < nrow * ncolumn; ++i) {
    v[i] -= m.v[i];
  }
#endif
}

//------------------------------------------------------------------------------

template<class Scalar>
inline
void DenseMat<Scalar>::setZero() {
#ifdef USE_EIGEN3_DenseMat
  v.setZero();
#else
  int i;
  for(i = 0; i < nrow * ncolumn; ++i) {
    v[i] = 0.0;
  }
#endif
}

//------------------------------------------------------------------------------

template<class Scalar>
inline
void DenseMat<Scalar>::resize(int nr, int nc) {
#ifdef USE_EIGEN3_DenseMat
  v.resize(nr, nc);
#else
  if(nrow != nr || ncolumn != nc) {
    if(v) {
      delete [] v;
    }
    nrow = nr;
    ncolumn = nc;
    if(nrow * ncolumn == 0) {
      v = new Scalar[1];
    }
    else {
      v = new Scalar[nrow * ncolumn];
    }
  }
#endif
}

//------------------------------------------------------------------------------

template<class Scalar>
inline
void DenseMat<Scalar>::print(std::ostream& ostream, int precision, const char *msg) {
  int old_precision = ostream.precision(precision);
  ostream.setf(std::ios::scientific);
#ifdef USE_EIGEN3_DenseMat
  if(*msg) ostream << msg << std::endl;
  ostream << v << std::endl;
#else
  if(*msg) {
    ostream << msg << std::endl;
  }
  int i, j;
  for(i = 0; i < nrow; ++i) {
    printRow(ostream, i);
    ostream << std::endl;
  }
#endif
  ostream.precision(old_precision);
  ostream.unsetf(std::ios::scientific);
}

//------------------------------------------------------------------------------

template<class Scalar>
inline
void DenseMat<Scalar>::printRow(std::ostream& ostream, int i) {
#ifdef USE_EIGEN3_DenseMat
  ostream << v.row(i) << " ";
#else
  for(int j = 0; j < ncolumn; ++j) {
    ostream << (*this)[i][j] << " ";
  }
#endif
}

//------------------------------------------------------------------------------

template<class Scalar>
inline
void DenseMat<Scalar>::readMatrixFromFile(const char *file, bool isROM) {
  std::ifstream inFile(file);
  if(!inFile.is_open()) {
    fprintf(stderr, "*** Error: %s failed to open\n", file);
    exit(-1);
  }
  int nr, nc;
  inFile >> nr >> nc;
  if(!inFile) {
    fprintf(stderr, "*** Error: check file format (Line 1: rows cols; successive lines: 1 row of matrix) in %s\n", file);
    exit(-1);
  }
  if(isROM) {
    nc *= 2;
    nc += nr;
    nr = nc;
  }
  this->resize(nr, nc);
  int i, j;
  for(i = 0; i < nr; ++i) {
    for(j = 0; j < nc; ++j) {
      inFile >> (*this)[i][j];
      if(!inFile) {
        fprintf(stderr, "*** Error: readMatrixFromFile failed for %s\n", file);
        exit(-1);
      }
    }
  }
  inFile.close();
}

//------------------------------------------------------------------------------

template<class Scalar>
template<bool Add>
inline
void DenseMat<Scalar>::applyToMatrix(DenseMat<Scalar>& b, DenseMat<Scalar>& c) {
#ifdef USE_EIGEN3_DenseMat
  assert(v.cols() == b.numRow());
  if(Add) {
    c.v.noalias() += v*b.v;
  }
  else {
    c.v = v*b.v;
  }
#else
  assert(ncolumn == b.numRow());
  int i, j, k;
  for(i = 0; i < nrow; ++i) {
    for(j = 0; j < b.numCol(); ++j) {
      if(!Add) c[i][j] = 0.0;
      for(k = 0; k < ncolumn; ++k) {
        c[i][j] += (*this)[i][k] * b[k][j];
      }
    }
  }
#endif
}

//------------------------------------------------------------------------------

template<class Scalar>
template<bool Add>
inline
void DenseMat<Scalar>::applyTransToMatrix(DenseMat<Scalar>& b, DenseMat<Scalar>& c) {
#ifdef USE_EIGEN3_DenseMat
  assert(v.rows() == b.numRow());
  if(Add) {
    c.v.noalias() += v.transpose()*b.v;
  }
  else {
    c.v = v.transpose()*b.v;
  }
#else
  assert(nrow == b.numRow());
  int i, j, k;
  for(i = 0; i < ncolumn; ++i) {
    for(j = 0; j < b.numCol(); ++j) {
      if(!Add) c[i][j] = 0.0;
      for(k = 0; k < nrow; ++k) {
        c[i][j] += (*this)[k][i] * b[k][j];
      }
    }
  }
#endif
}

//------------------------------------------------------------------------------

template<class Scalar>
template<bool Add>
inline
void DenseMat<Scalar>::applyToMatrixTrans(DenseMat<Scalar>& b, DenseMat<Scalar>& c) {
#ifdef USE_EIGEN3_DenseMat
  assert(v.cols() == b.numCol());
  if(Add) {
    c.v.noalias() += v*b.v.transpose();
  }
  else {
    c.v = v*b.v.transpose();
  }
#else
  assert(ncolumn == b.numCol());
  int i, j, k;
  for(i = 0; i < nrow; ++i) {
    for(j = 0; j < b.numRow(); ++j) {
      if(!Add) c[i][j] = 0.0;
      for(k = 0; k < ncolumn; ++k) {
        c[i][j] += (*this)[i][k] * b[j][k];
      }
    }
  }
#endif
}

//------------------------------------------------------------------------------

template<class Scalar>
template<bool Add>
inline
void DenseMat<Scalar>::applyTransToSelf(DenseMat<Scalar>& b) {
#ifdef USE_EIGEN3_DenseMat
  if(Add) {
    if(sparsified) { applyTransToSelfSparse<Add>(b); }
    else { b.v.noalias() += v.transpose()*v; }
  }
  else {
    if(sparsified) { applyTransToSelfSparse<Add>(b); }
    else { b.v = v.transpose()*v; }
  }
#else
  int i, j, k;
  for(i = 0; i < ncolumn; ++i) {
    for(j = 0; j < ncolumn; ++j) {
      if(j >= i) {
        if(!Add) b[i][j] = 0.0;
        for(k = 0; k < nrow; ++k) {
          b[i][j] += (*this)[k][i] * (*this)[k][j];
        }
      }
      else {
        b[i][j] = b[j][i];
      }
    }
  }
#endif
}

//------------------------------------------------------------------------------

template<class Scalar>
template<bool Add>
inline
void DenseMat<Scalar>::applyTransToSelfSparse(DenseMat<Scalar>& b) {
#ifdef USE_EIGEN3_DenseMat
  if(!Add) { b.v.setZero(); }
  int iStartRow = 0;
  for(int i = 0; i < patLabelToNumVecs.size(); i++) {
    int iNumVecs = patLabelToNumVecs[i];
    int jStartCol = iStartRow;
    for(int j = i; j < patLabelToNumVecs.size(); j++) {
      int jNumVecs = patLabelToNumVecs[j];
      for(int k = 0; k < num_row_blocks; k++) {
        if(patLabelToColPat[i][k] && patLabelToColPat[j][k]) {
          b.v.block(iStartRow, jStartCol, iNumVecs, jNumVecs).noalias() +=
            v.block(k*rows_per_block, iStartRow, rows_per_block, iNumVecs).transpose() *
            v.block(k*rows_per_block, jStartCol, rows_per_block, jNumVecs);
        }
      }
      if(j != i) { // fill in the lower diagonal
        b.v.block(jStartCol, iStartRow, jNumVecs, iNumVecs).noalias() +=
          b.v.block(iStartRow, jStartCol, iNumVecs, jNumVecs).transpose();
      }
      jStartCol += jNumVecs;
    }
    iStartRow += iNumVecs;
  }
#else
  fprintf(stderr, "*** Warning: sparse applyTransToSelf routine is not supported when USE_EIGEN3_DenseMat is not set\n");
#endif
}

//------------------------------------------------------------------------------

template<class Scalar>
template<bool Add>
inline
void DenseMat<Scalar>::applyToVector(Scalar *b, Scalar *c) {
#ifdef USE_EIGEN3_DenseMat
  Eigen::Map<VectorType> B(b,v.cols(),1);
  Eigen::Map<VectorType> C(c,v.rows(),1);
  if(Add) {
    C.noalias() += v*B;
  }
  else {
    C = v*B;
  }
#else
  int i, j, k;
  for(i = 0; i < nrow; ++i) {
    if(!Add) c[i] = 0.0;
    for(j = 0; j < ncolumn; ++j) {
      c[i] += (*this)[i][j] * b[j];
    }
  }
#endif
}

//------------------------------------------------------------------------------

template<class Scalar>
template<bool Add>
inline
void DenseMat<Scalar>::applyTransToVector(Scalar *b, Scalar *c) {
#ifdef USE_EIGEN3_DenseMat
  Eigen::Map<VectorType> B(b,v.rows(),1);
  Eigen::Map<VectorType> C(c,v.cols(),1);
  if(Add) {
    C.noalias() += v.transpose()*B;
  }
  else {
    C = v.transpose()*B;
  }
#else
  int i, j;
  for(i = 0; i < ncolumn; ++i) {
    if(!Add) c[i] = 0.0;
    for(j = 0; j < nrow; ++j) {
      c[i] += (*this)[j][i] * b[j];
    }
  }
#endif
}

//------------------------------------------------------------------------------

template<class Scalar>
template<bool Add>
inline
void DenseMat<Scalar>::addTransToSelf(DenseMat<Scalar>& B) {
#ifdef USE_EIGEN3_DenseMat
  assert(v.rows() ==  v.cols());
  if(Add) {
    B.v.noalias() += v.transpose() + v;
  }
  else {
    B.v = v.transpose() + v;
  }
#else
  assert(nrow == ncolumn);
  int i, j;
  for(i = 0; i < ncolumn; ++i) {
    for(j = i; j < ncolumn; ++j) {
      double tmp = (*this)[i][j] + (*this)[j][i];
      (Add ? B[i][j] += tmp : B[i][j] = tmp);
      if(j != i) {
        (Add ? B[j][i] += tmp : B[j][i] = tmp);
      }
    }
  }
#endif
}

//------------------------------------------------------------------------------

template<class Scalar>
inline
Scalar DenseMat<Scalar>::norm() {
#ifdef USE_EIGEN3_DenseMat
  return v.norm();
#else
  Scalar frobNorm = 0;
  for(int i = 0; i < nrow * ncolumn; ++i) {
    frobNorm += pow(v[i], 2);
  }
  frobNorm = sqrt(frobNorm);
  return frobNorm;
#endif
}

//------------------------------------------------------------------------------

template<class Scalar>
template<bool Factor>
inline
void DenseMat<Scalar>::lu(Scalar *b) {
#ifdef USE_EIGEN3_DenseMat
  if(Factor) {
    LU.compute(v);
  }
  Eigen::Map<VectorType> B(b, v.rows(), 1);
  B = LU.solve(B).eval();
#else
  if(!Factor) {
    fprintf(stderr, "*** Warning: precomputing factors in DenseMat::lu is not supported when USE_EIGEN3_DenseMat is not set\n");
  }
  DenseMatrixFactor<Scalar, Eigen::Dynamic>::lu(v, b, nrow);
#endif
}

//------------------------------------------------------------------------------

template<class Scalar>
template<bool Factor>
inline
void DenseMat<Scalar>::cholesky(Scalar *b) {
#ifdef USE_EIGEN3_DenseMat
  if(Factor) {
    LLT.compute(v);
  }
  Eigen::Map<VectorType> B(b, v.rows(), 1);
  B = LLT.solve(B).eval();
#else
  if(!Factor) {
    fprintf(stderr, "*** Warning: precomputing factors in DenseMat::lu is not supported when USE_EIGEN3_DenseMat is not set\n");
  }
  DenseMatrixFactor<Scalar, Eigen::Dynamic>::cholesky(v, b, nrow);
#endif
}

//------------------------------------------------------------------------------
// Thresholded SVD-based solution to Ax = b. A must be square.
template<class Scalar>
template<bool Factor>
inline
int DenseMat<Scalar>::svd_solve(Scalar *b, RealScalarType threshold) {
#ifdef USE_EIGEN3_DenseMat
  if(v.cols() != v.rows()) {
    fprintf(stderr, "*** Error: DenseMat::svd_solve only implemented for square matrices!\n");
    exit(-1);
  }
  // Note: JacobiSVD fixes occasional FPE experienced with BDCSVD
  if(Factor) {
    SVD.compute(v, Eigen::ComputeThinU | Eigen::ComputeThinV);
  }
  SVD.setThreshold(threshold);
  Eigen::Map<VectorType> B(b, v.rows(), 1);
  B = SVD.solve(B).eval();
  return SVD.rank();
#else
  std::cerr << "*** Error: DenseMat::svd_solve is not implemented when USE_EIGEN3_DenseMat is undefined\n";
  exit(-1);
#endif
}

//------------------------------------------------------------------------------
// Least-squares solution to ||V^T x - b||. V thus has to be nonskinny.
template<class Scalar>
template<bool Factor>
inline
int DenseMat<Scalar>::transpose_svd_solve(Scalar *b, Scalar *x, RealScalarType threshold) {
#ifdef USE_EIGEN3_DenseMat
  if(v.rows() > v.cols()) {
    fprintf(stderr, "*** Error: DenseMat::transpose_svd_solve will likely have issues with a skinny matrix!\n");
    exit(-1);
  }
  // Note: JacobiSVD fixes occasional FPE experienced with BDCSVD
  if(Factor) {
    SVD.compute(v.transpose(), Eigen::ComputeThinU | Eigen::ComputeThinV);
  }
  SVD.setThreshold(threshold);
  Eigen::Map<VectorType> B(b, v.cols(), 1);
  Eigen::Map<VectorType> X(x, v.rows(), 1);
  X = SVD.solve(B).eval();
  return SVD.rank();
#else
  std::cerr << "*** Error: DenseMat::transpose_svd_solve is not implemented when USE_EIGEN3_DenseMat is undefined\n";
  exit(-1);
#endif
}

//------------------------------------------------------------------------------

template<class Scalar>
template<bool UT>
inline
void DenseMat<Scalar>::svd(Scalar *Sigma, Scalar *V) {
#ifdef USE_EIGEN3_DenseMat
  if(v.cols() > v.rows()) {
    fprintf(stderr, "*** Error: DenseMat::svd will likely have issues with a fat matrix!\n");
    exit(-1);
  }
  // Note: JacobiSVD fixes occasional FPE experienced with BDCSVD
  Eigen::JacobiSVD<MatrixType> SVD((UT ? v.template triangularView<Eigen::Upper>() : v), V ? (Eigen::ComputeThinU | Eigen::ComputeThinV) : (Eigen::ComputeThinU));
  v = SVD.matrixU();
  Eigen::Map<VectorType>(Sigma, std::min(v.rows(), v.cols()), 1) = SVD.singularValues();
  if(V) {
    Eigen::Map<MatrixType>(V, std::min(v.rows(), v.cols()), std::min(v.rows(), v.cols())) = SVD.matrixV().transpose();
  }
#else
  std::cerr << "*** Error: DenseMat::svd is not implemented when USE_EIGEN3_DenseMat is undefined\n";
  exit(-1);
#endif
}

//------------------------------------------------------------------------------

template<class Scalar>
inline
void DenseMat<Scalar>::invert() {
#ifdef USE_EIGEN3_DenseMat
  DenseMatrixFactor<Scalar, Eigen::Dynamic>::invert(v.data(), v.rows());
#else
  DenseMatrixFactor<Scalar, Eigen::Dynamic>::invert(v, nrow);
#endif
}

//------------------------------------------------------------------------------

template<class Scalar>
inline
bool DenseMat<Scalar>::isSparsified() {
#ifdef USE_EIGEN3_DenseMat
  return sparsified;
#else
  return false;
#endif
}

//------------------------------------------------------------------------------

template<class Scalar>
inline
void DenseMat<Scalar>::block_sparsify(int rows_per_block, std::vector<std::vector<int>>& sharedNodes) {
#ifdef USE_EIGEN3_DenseMat
  // identify how many sparsity patterns are present in the ROB, and in which cols, so
  // they can be grouped together to form a block-sparse ROB
  std::map<std::vector<bool>, int> colPatToLabel;
  std::map<int, std::vector<int>> patLabelToColInds;
  std::vector<int> colIndToPatLabel;
  patLabelToColPat.clear();
  patLabelToNumVecs.clear();
  int curPatLabel = -1;
  for(int iCol = 0; iCol < v.cols(); iCol++) {
    std::vector<bool> colPattern;
    // compute this column's sparsity pattern
    for(int iNode = 0; iNode < v.rows()/rows_per_block; iNode++) {

      // mark all shared nodes as potentially nonzero.  This prevents a need to
      // communicate with neighbor subdomains to check for nonzero elements
      bool isShared = false;
      for(int iNeigh = 0; iNeigh < sharedNodes.size(); iNeigh++) {
        for(int iSharedNode = 0; iSharedNode < sharedNodes[iNeigh].size(); iSharedNode++) {
          if(sharedNodes[iNeigh][iSharedNode] == iNode) {
            colPattern.push_back(true);
            isShared = true;
          }
        }
      }

      if(!isShared) {
        for(int iDim = 0; iDim < rows_per_block; iDim++) {
          if(v(iNode*rows_per_block + iDim, iCol) != 0) {
            colPattern.push_back(true);
            break;
          }
          if(iDim == rows_per_block - 1) { colPattern.push_back(false); }
        }
      }
    }

    // get the label for this col's sparsity pattern
    auto foundIt = colPatToLabel.find(colPattern);
    if(foundIt == colPatToLabel.end()) {
      curPatLabel++;
      colPatToLabel[colPattern] = curPatLabel;
      colIndToPatLabel.push_back(curPatLabel);
      patLabelToColPat.push_back(colPattern);
      patLabelToColInds[curPatLabel].push_back(iCol);
    }
    else {
      int patLabel = (*foundIt).second;
      colIndToPatLabel.push_back(patLabel);
      patLabelToColInds[patLabel].push_back(iCol);
    }
  }
  // form a permutation matrix to group columns w/ the same sparsity pattern together
  std::vector<int> permIndices;
  for(int iLabel = 0; iLabel <= curPatLabel; iLabel++) {
    permIndices.insert(permIndices.end(), patLabelToColInds[iLabel].begin(),
                                          patLabelToColInds[iLabel].end() );
    patLabelToNumVecs.push_back(patLabelToColInds[iLabel].size());
  }
  groupColsPermMat.resize(v.cols());
  for(int iCol = 0; iCol < v.cols(); iCol++) {
    groupColsPermMat.indices()[iCol] = permIndices[iCol];
  }
  v *= groupColsPermMat;

  // store some variables that help process the node-based block_sparse matrix
  this->rows_per_block = rows_per_block;
  num_row_blocks = v.rows()/rows_per_block;
  sparsified = true;
#else
  fprintf(stderr, "*** Warning: block-sparsity is not supported when USE_EIGEN3_DenseMat is not set\n");
  return;
#endif
}

//------------------------------------------------------------------------------

template<class Scalar>
inline
void DenseMat<Scalar>::copySparsityInfo(DenseMat<Scalar>& target, bool resetSparsityPatterns) {
#ifdef USE_EIGEN3_DenseMat
  target.patLabelToColPat.resize(patLabelToColPat.size());
  for(int iLabel = 0; iLabel < patLabelToColPat.size(); iLabel++) {
    target.patLabelToColPat[iLabel].clear();
    if(resetSparsityPatterns) {
      target.patLabelToColPat[iLabel].assign(patLabelToColPat[iLabel].size(), false);
    }
    else {
      target.patLabelToColPat[iLabel].assign(patLabelToColPat[iLabel].begin(), patLabelToColPat[iLabel].end());
    }
  }

  target.groupColsPermMat = groupColsPermMat;
  target.patLabelToNumVecs = patLabelToNumVecs;
  target.sparsified = sparsified;
  target.rows_per_block = rows_per_block;
  target.num_row_blocks = num_row_blocks;
  return;
#else
  fprintf(stderr, "*** Warning: block-sparsity is not supported when USE_EIGEN3_DenseMat is not set\n");
  return;
#endif
}

//------------------------------------------------------------------------------

template<class Scalar>
inline
int DenseMat<Scalar>::nnz() const {
#ifdef USE_EIGEN3_DenseMat
  if(!sparsified) { return numRow() * numCol(); }
  int nnz = 0;
  for(int patNum = 0; patNum < patLabelToNumVecs.size(); patNum++) {
    int num_cols = patLabelToNumVecs[patNum];
    int num_nnz_rows = 0;
    for(int iNode = 0; iNode < num_row_blocks; iNode++) {
      if(patLabelToColPat[patNum][iNode]) { num_nnz_rows++; }
    }
    nnz += num_cols * num_nnz_rows * rows_per_block;
  }
  return nnz;
#else
  fprintf(stderr, "*** Warning: block-sparsity is not supported when USE_EIGEN3_DenseMat is not set\n");
  return 0;
#endif
}

//------------------------------------------------------------------------------

#endif

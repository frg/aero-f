#ifndef _IO_DATA_H_
#define _IO_DATA_H_

#include <algorithm>
#include <cmath>
#include <cstdio>
#include <list>
#include <map>
#include <vector>

#include <RefVal.h>
#include <Types.h>
#include <Vector3D.h>
#include <parser/ParseTree.h>
#include <parser/Dictionary.h>
#include <USStandardAtmosphere.h>

struct AdaptiveMeshRefinementData;
class Assigner;
class ClassAssigner;
class Communicator;
class VarFcn;

//------------------------------------------------------------------------------

template<class DataType>
class ObjectMap {

 public:
  std::map<int, DataType *> dataMap;
  void setup(const char *name, ClassAssigner *);

  ObjectMap() {}
  ~ObjectMap() {
    for(typename std::map<int, DataType *>::iterator it = dataMap.begin(); it != dataMap.end(); ++it) {
      delete it->second;
    }
  }
};

//------------------------------------------------------------------------------

struct FluidRemapData {
  int oldID, newID;

  FluidRemapData();

  void setup(const char *, ClassAssigner * = 0);
  Assigner *getAssigner();
};

//------------------------------------------------------------------------------

struct OneDimensionalInputData {
  const char *file;
  double x0, y0, z0;
  ObjectMap<FluidRemapData> fluidRemap;

  OneDimensionalInputData();

  void setup(const char *, ClassAssigner * = 0);
  Assigner *getAssigner();
};

//------------------------------------------------------------------------------

struct InputData {
  enum OptimalPressureDimensionality {NON_DIMENSIONAL = 0, DIMENSIONAL = 1, NONE = 2} optPressureDim;

  const char *prefix;
  const char *geometryprefix;
  const char *connectivity;
  const char *geometry;
  const char *decomposition;
  const char *cpumap;
  const char *icgeometryprefix;
  const char *icconnectivity;
  const char *icgeometry;
  const char *icdecomposition;
  const char *iccpumap;
  const char *match;
  const char *d2wall;
  const char *d2bfwall;
  const char *fixes;
  const char *limiterphi;
  const char *edgeupwinddirections;
  const char *faceupwinddirections;
  const char *perturbed;
  const char *solutions;
  const char *positions;
  const char *embeddedpositions;
  const char *embeddedvelocity;
  const char *embeddedSurface;
  const char *triangleSet;
  const char *levelsets;
  const char *fluidId;
  const char *rstdata;
  const char *mshdata;
  const char *restart_file_package;
  const char *podFile;
  const char *optimalPressureFile;
  const char *matchStateFile;
  const char *optimalPressureDim;
  const char *strModesFile;
  const char *oneDimensionalSolution;
  const char *convergence_file;
  const char *exactInterfaceLocation;
  const char *reducedEigState;
  const char *wallsurfacedisplac;
  const char *shapederivatives;
  const char *inletdisk;
  const char *inletdisksensitivities;
  int solutionStep;

  // for nonlinear ROM
  const char *statesnapfile;
  const char *adaptivesnapfile;
  const char *unbalancedsnapfile;
  const char *superstatesnapfile;
  const char *projerrorsnapfile;
  const char *samplingsnapfile;
  const char *supersamplingsnapfile;
  const char *residualsnapfile;
  const char *surfacesnapfile;
  const char *surfacesnapfilevel;
  const char *initialfidfile;
  const char *reducedcoords;
  const char *neighborinds;
  const char *surfaceposition;
  const char *inpsampnodes;
  const char *inpsampweights;
  const char *inpsweptnodes;
  const char *snapreferencesolution;
  int snapReferenceStep;
  const char *interpICWeights;
  const char *multiSolutions;
  const char *parameters;
  double parametricDistanceExponent;
  int maxInterpolatedSolutions;

  ObjectMap<OneDimensionalInputData> oneDimensionalInput;

  const char *corotmatrix;
  const char *altitudeSamples;

  const char *romMatrixFile;
  const char *romConstraintMatrixFile;

  const char *gendispandvelFile;
  const char *trimFile;
  const char *errorAnalysisIntermediateMatrices;

  int read_64bit_int; // set to 1 if input mesh files use 64 bit integers for global node, face and element numbers
                      // for example when reading mesh files written by Aero-F during AMR simulation when code was
                      // configured with cmake -DUSE_64BIT_INT=TRUE (default is 0, in which case input mesh files should
                      // use 32 bit integers for the global node, face and element numbers)

  InputData();

  void setup(const char *, ClassAssigner * = 0);
};

//------------------------------------------------------------------------------

struct Probes {
  const static int MAXNODES = 3176;
  struct Node {
    Node() {
      id = -1;
      locationX = locationY = locationZ = -1.0e20;
      subId = localNodeId = -1;
      isLocationBased = false;
    }
    int id;
    int subId;
    int localNodeId;
    double locationX, locationY, locationZ;
    bool isLocationBased;
    void setup(const char *, ClassAssigner * = 0);
  };

  Node myNodes[MAXNODES];

  enum OutputSwept {OUTPUT_SWEPT_FALSE = 0, OUTPUT_SWEPT_TRUE = 1} outputswept;

  const char *prefix;
  const char *density;
  const char *pressure;
  const char *diffpressure;
  const char *temperature;
  const char *velocity;
  const char *displacement;
  const char *nutilde;

  Probes();

  void setup(const char *, ClassAssigner * = 0);
};

//------------------------------------------------------------------------------

struct LinePlot {

  LinePlot();

  Assigner *getAssigner();
  double x0, y0, z0;
  double x1, y1, z1;

  int numPoints;

  const char *density;
  const char *pressure;
  const char *temperature;
  const char *velocity;
};

//------------------------------------------------------------------------------

struct TransientData {

  const char *prefix;
  const char *solutions;
  const char *coarsesolutions;
  const char *finesolutions;
  const char *density;
  const char *tavdensity;
  const char *mach;
  const char *speed;
  const char *divergence;
  const char *wtmach;
  const char *wtspeed;
  const char *tavmach;
  const char *pressure;
  const char *diffpressure;
  const char *tavpressure;
  const char *tavpressurecoefficient;
  const char *hydrostaticpressure;
  const char *hydrodynamicpressure;
  const char *pressurecoefficient;
  const char *temperature;
  const char *tavtemperature;
  const char *totalpressure;
  const char *tavtotalpressure;
  const char *vorticity;
  const char *tavvorticity;
  const char *vorticitymag;
  const char *tavvorticitymag;
  const char *reynoldsstresseigenvalues;
  const char *nutturb;
  const char *kturb;
  const char *epsturb;
  const char *omegaturb;
  const char *eddyvis;
  const char *densityfine;
  const char *pressurefine;
  const char *temperaturefine;
  const char *velocityfine;
  const char *tavdensityfine;
  const char *tavpressurefine;
  const char *tavtemperaturefine;
  const char *tavvelocityfine;
  const char *reynoldsstress; //value is reported as a turbulence statistic with units velocity * velocity
  const char *reynoldsstressmodel; //value is reported as a instantaneous modeled quantity with units velocity * velocity
  const char *tavreynoldsstressmodel;
  const char *reynoldsstressfluctuation;
  const char *densityfluctuationrms;
  const char *pressurefluctuationrms;
  const char *temperaturefluctuationrms;
  const char *dplus;
  const char *sfric;
  const char *tavsfric;
  const char *csdles;
  const char *tavcsdles;
  const char *csdvms;
  const char *tavcsdvms;
  const char *mutOmu;
  const char *velocity;
  const char *tavvelocity;
  const char *meshvelocity;
  const char *displacement;
  const char *tavdisplacement;
  const char *flightDisplacement;
  const char *localFlightDisplacement;
  const char *bulkQuantities;
  const char *forces;
  const char *tavforces;
  const char *hydrostaticforces;
  const char *hydrodynamicforces;
  const char *generalizedforces;
  const char *lift;
  const char *ilift; //inviscid component of lift/drag (pressure lift)
  const char *vlift; //viscous component of lift/drag
  const char *tavlift;
  const char *hydrostaticlift;
  const char *hydrodynamiclift;
  const char *residuals;
  const char *fluxresiduals;
  const char *materialVolumes;
  const char *materialConservationScalars;
  const char *conservation;
  const char *podFile;
  const char *robProductFile;
  const char *rMatrixFile;
  const char *romFile;
  const char *gendispFile;
  const char *romInitialConditionFile;
  const char *philevel;
  const char *controlvolume;
  const char *macrocellid;
  const char *fluidid;
  const char *isactive;
  const char *localtimestep;
  const char *isoccluded;
  const char *hybridstatus;
  const char *hybridindicator;
  const char *d2wall;
  const char *d2wallgrad;
  const char *preferreddirection;
  const char *gap;
  const char *embeddedsurfacedisp;
  const char *embeddedsurfacevel;
  const char *embeddedsurfacecp;
  const char *embeddedsurfacecf;
  const char *embeddedsurfacetemp;
  const char *embeddedsurfacemach;
  const char *embeddedsurfaceheatflux;
  const char *embeddedsurfacedplus;
  const char *embeddedsurfacedelta;
  const char *embeddedsurfaceutau;
  const char *embeddedsurfacedensity;
  const char *inletdiskpressure;
  const char *inletdiskpressuredistortion;
  const char *inletdisktotalpressuredistortion;
  const char *cputiming;
  const char *aeroelasticEigenvalues;
  const char *gamData;
  const char *gamFData;
  const char *velocitynorm;
  const char *dSolutions;
  const char *populatedState;
  const char *dDensity;
  const char *dMach;
  const char *dPressure;
  const char *dMatchPressure;
  const char *dTemperature;
  const char *dTotalpressure;
  const char *dNutturb;
  const char *dKturb;
  const char *dEpsturb;
  const char *dEddyvis;
  const char *dVelocityScalar;
  const char *dVelocityVector;
  const char *dDisplacement;
  const char *dForces;
  const char *dModalForces;
  const char *dLiftDrag;
  const char *dLiftx;
  const char *dLifty;
  const char *dLiftz;
  const char *dMomentx;
  const char *dMomenty;
  const char *dMomentz;
  const char *dPressureDistortion;
  const char *surfaceheatflux;
  const char *heatfluxes;
  const char *sumpsimag;
  const char *sparseGrid;
  const char *bubbleRadius;
  const char *fixes;
  const char *limiterphi;
  const char *hdensity;
  const char *hvorticitymag;
  const char *hxvelocity;
  const char *hyvelocity;
  const char *hzvelocity;
  const char *hpressure;
  const char *hmach;

  int frequency;
  int forces_frequency;
  int width, precision; // used for formatting floating point ascii output
  double x0, y0, z0;
  double length;
  double surface;
  double surfaceBQ;
  bool isfavre;
  double frequency_dt; // set to -1.0 by default and used iff it is activated (> 0.0) by user
  enum ExactForceOutput {OFF_EXACTFORCEOUTPUT = 0, ON_EXACTFORCEOUTPUT = 1} exactforceoutput;
  enum HessianMode {NON_DIMENSIONAL = 0, DIMENSIONAL = 1} hessian_mode;
  enum Postprocess {OFF = 0, ON = 1} postprocess;
  enum PostprocessLoftingInterface {REAL = 0, SURROGATE = 1, HYBRID = 2} postprocess_lofting_interface;
  enum PostprocessLoftingType {GLOBAL = 0, LOCAL = 1} postprocess_lofting_type;
  enum PostprocessReconstruction {CONSTANT = 0, LINEAR = 1} postprocess_reconstruction;
  int postprocess_sides;
  double postprocess_lofting_factor;
  Probes probes;
  ObjectMap<LinePlot> linePlots;

  const char *altitudeSamplesROM;
  const char *hdmFile;
  const char *errorBoundPreprocessFile;
  const char *genvelFile;
  const char *dModalFdModeFile;
  const char *dModalFdCtrlSurfFile;

  enum UpdateMomentArm {FALSE = 0, TRUE = 1} updateMomentArm;
  enum OutputIntermediateErrorMatrices {FALSE_OUTPUT_INTERMEDIATE_MATRICES = 0, TRUE_OUTPUT_INTERMEDIATE_MATRICES = 1} outputIntermediateErrorMatrices;

  const char *refModalForceFile;

  TransientData();

  void setup(const char *, ClassAssigner * = 0);
};

//------------------------------------------------------------------------------

struct RestartData {

  enum Type {SINGLE = 0, DOUBLE = 1} type;

  const char *prefix;

  const char *solutions;
  const char *positions;
  const char *embeddedpositions;
  const char *embeddedvelocity;
  const char *d2wall;
  const char *levelsets;
  const char *cracking;
  const char *fluidId;
  const char *edgeupwinddirections;
  const char *faceupwinddirections;
  const char *fixes;
  const char *limiterphi;
  const char *data;
  const char *meshdata;
  const char *filepackage;
  const char *corotmatrix;
  const char *trimSolutionOffsetFile;
  const char *errorAnalysisIntermediateMatrices;

  int frequency;
  double frequency_dt; // set to -1.0 by default and used iff it is activated (> 0.0) by user

  RestartData();

  void setup(const char *, ClassAssigner * = 0);
};

//------------------------------------------------------------------------------

struct ROMOutputData {

  enum OutputVelocity {OUTPUT_VEL_FALSE = 0, OUTPUT_VEL_TRUE = 1} outputvelocity;
  enum OutputDeltaSnapshots {OUTPUT_DELTA_FALSE = 0, OUTPUT_DELTA_TRUE = 1} outputdeltasnapshots;
  enum OutputEnforcedBCs {OUTPUT_ENFORCED_FALSE = 0, OUTPUT_ENFORCED_TRUE = 1} outputenforcedbcs;
  enum OutputSwept {OUTPUT_SWEPT_FALSE = 0, OUTPUT_SWEPT_TRUE = 1} outputswept;
  enum OutputSweptHistory {OUTPUT_SWEPT_HIST_FALSE = 0, OUTPUT_SWEPT_HIST_TRUE = 1} outputswepthistory;
  enum OutputSubcycle {OUTPUT_SUBCYCLE_FALSE = 0, OUTPUT_SUBCYCLE_TRUE = 1} outputsubcycle;

  const char *prefix;

  const char *statevector;
  const char *residualvector;
  const char *reducedcoords;
  const char *neighborinds;
  const char *reducedresid;
  const char *reducedjac;
  const char *surfaceposition;
  const char *surfacevelocity;
  const char *sweptset;
  const char *dFluxNorm;

  int frequency;

  ROMOutputData();

  void setup(const char *, ClassAssigner * = 0);
};

//------------------------------------------------------------------------------

struct OutputData {

  int write_64bit_int; // set to 1 if output mesh files use 64 bit integers for global node, face and element numbers
                       // note #1: this option is only available when code is configured with -DUSE_64BIT_INT=TRUE
                       // note #2: sower currently does not support postprocessing 64 bit global numbers, but mesh files
                       //          can be used for restarting, provided InputData::read_64bit_int is set to 1

  TransientData transient;
  RestartData restart;
  ROMOutputData rom;

  OutputData();

  void setup(const char *, ClassAssigner * = 0);
};

//------------------------------------------------------------------------------

struct RestartParametersData {

  int iteration;

  double etime;
  double dt_nm1;
  double dt_nm2;
  double residual;
  double energy;

  int mesh_output_index;
  int mesh_refined_since_last_output;
  int max_node_pre_amr;
  int corrector_iteration;
  int step_offset;
  int snap_step;

  RestartParametersData();

  void setup(const char *, ClassAssigner * = 0);
};

//------------------------------------------------------------------------------

struct NonlinearRomFilesData {

  // State snapshot clusters
  const char *statePrefix;
  const char *stateSnapsName;
  const char *mapName;
  const char *indexName;
  const char *connName;
  const char *centersName;

  // State basis
  const char *stateBasisName;
  const char *quadStateBasisName;
  const char *stateRefName;
  const char *stateAvgName;
  const char *stateSingValsName;
  const char *quadStateSingValsName;
  const char *projErrorName;
  const char *reducedCoordNeuralNetName;
  const char *projReducedCoords;
  const char *rowClustersName;
  const char *basisRowClustersName;
  const char *quadStateBasisSizesName;

  // Residual basis
  const char *residualPrefix;
  const char *residualBasisName;
  const char *residualSingValsName;

  // Reduced mesh quantities
  const char *reducedMeshPrefix;
  const char *reducedMeshName;
  const char *reducedMeshNodesName;
  const char *sampleNodesName;
  const char *sampleNodesFullName;
  const char *gradientNodesName;
  const char *gradientNodesFullName;
  const char *sampleWeightsName;

  // Online and HROM quantities
  const char *reducedWallDistName;
  const char *reducedStateBasisName;
  const char *reducedQuadStateBasisName;
  const char *reducedStateRefName;
  const char *reducedFluidIdName;
  const char *reducedSnapshotsName;
  const char *reducedGnatMatrixName;
  const char *gnatMatrixName;

  NonlinearRomFilesData();

  void setup(const char *, ClassAssigner * = 0);
};

//------------------------------------------------------------------------------

struct NonlinearRomFileSystemData {
  // A nonlinear ROM database can consist of a very large number of files. To
  // make life easier for the user, this code introduces a "prefix" feature,
  // which tells aero-f to read and write database files using a built-in
  // naming convention. If a prefix and a name are both given for a file, the
  // name overrides the prefix.

  enum DuplicateSnapshots {DUPLICATE_SNAPSHOTS_FALSE = 0,
                           DUPLICATE_SNAPSHOTS_TRUE = 1} duplicateSnapshots;

  const char *prefix;
  const char *databaseName;
  const char *clusterName;

  int nClusters;

  NonlinearRomFilesData files;

  NonlinearRomFileSystemData();

  void setup(const char *, ClassAssigner * = 0);
};

//------------------------------------------------------------------------------

struct ProblemData {

  enum Type {UNSTEADY = 0, ACCELERATED = 1, AERO = 2, THERMO = 3, FORCED = 4,
             ROLL = 5, RBM = 6, LINEARIZED = 7, NLROMOFFLINE = 8, NLROMONLINE = 9,
             NLROMPOST = 10, SENSITIVITY_ANALYSIS = 11, EMBEDDED_FRAMEWORK = 12, SIZE = 13};
  bool type[SIZE];

  enum AllType {_STEADY_ = 0, _UNSTEADY_ = 1, _ACC_UNSTEADY_ = 2, _STEADY_AEROELASTIC_ = 3,
                _UNSTEADY_AEROELASTIC_ = 4, _ACC_UNSTEADY_AEROELASTIC_ = 5,
                _STEADY_THERMO_ = 6, _UNSTEADY_THERMO_ = 7, _STEADY_AEROTHERMOELASTIC_ = 8,
                _UNSTEADY_AEROTHERMOELASTIC_ = 9, _FORCED_ = 10, _ACC_FORCED_ = 11,
                _ROLL_ = 12, _RBM_ = 13, _UNSTEADY_LINEARIZED_ = 14, _UNSTEADY_LINEARIZED_AEROELASTIC_ = 15,
                _POD_CONSTRUCTION_ = 16, _INTERPOLATION_ = 17, _ROB_INNER_PRODUCT_ = 18,
                _GAM_CONSTRUCTION_ = 19, _AEROELASTIC_ANALYSIS_ = 20,
                _NONLINEAR_EIGENRESIDUAL_ = 21, _ROM_ = 22, _ROM_AEROELASTIC_ = 23,
                _SPARSEGRIDGEN_ = 24, _ONE_DIMENSIONAL_ = 25, _AERO_ACOUSTIC_ = 26,
                _NONLINEAR_ROM_PREPROCESSING_ = 27, _STEADY_NONLINEAR_ROM_ = 28,
                _UNSTEADY_NONLINEAR_ROM_ = 29, _ACC_UNSTEADY_NONLINEAR_ROM_ = 30,
                _FORCED_NONLINEAR_ROM_ = 31, _STEADY_NONLINEAR_ROM_POST_ = 32,
                _UNSTEADY_NONLINEAR_ROM_POST_ = 33,
                _SENSITIVITY_ANALYSIS_ = 34, _FORCED_LINEARIZED_ = 35,
                _FORCED_LINEARIZED_ROM_ = 36, _LINEARIZED_HDM_OUTPUT_ = 37,
                _LINEARIZED_ERROR_ANALYSIS_PREPROCESSING_ = 38, _FORCED_NONLINEAR_ROM_POST_ = 39} alltype;
  enum Mode {NON_DIMENSIONAL = 0, DIMENSIONAL = 1} mode;
  enum Prec {NON_PRECONDITIONED = 0, PRECONDITIONED = 1} prec;
  enum Framework {BODYFITTED = 0, EMBEDDED = 1, EMBEDDEDALE = 2} framework;
  int verbose;

  ProblemData();

  void setup(const char *, ClassAssigner * = 0);
};

//------------------------------------------------------------------------------

struct PreconditionData {

  double mach;
  double cmach;
  double k;
  double betav;
  double shockreducer;

  PreconditionData();

  void setup(const char *, ClassAssigner * = 0);
};

//------------------------------------------------------------------------------

struct ReferenceStateData {

  double mach;
  double velocity;
  double density;
  double pressure;
  double temperature;
  double reynolds_mu;
  double energy;
  double length;
  double dRe_mudMach;

  RefVal rv;

  double altitude;
  USStandardAtmosphere::Unit altitudeUnit;
  USStandardAtmosphere::Unit problemUnit;

  USStandardAtmosphere usStandardAtmosphere;

  ReferenceStateData();

  void setup(const char *, ClassAssigner * = 0);
};

//------------------------------------------------------------------------------

struct BcsFreeStreamData {

  enum Type {EXTERNAL = 0, INTERNAL = 1} type;
  int fluidModelID;

  double mach;
  double velocity;
  double density;
  double pressure;
  double temperature;
  double nutilde;
  double kenergy;
  double eps;
  double alpha;
  double beta;
  double altitude;
  int isPerturbed;
  int isFunction;
  int forceAlphaBeta;

  USStandardAtmosphere::Unit altitudeUnit;
  USStandardAtmosphere::Unit problemUnit;

  USStandardAtmosphere usStandardAtmosphere;

  enum VaryAltitude {FALSE = 0, TRUE = 1} varyAltitude;
  double flightPathAngle;
  double dt; // ignored in nonlinear case
  double flightPathVelocity; // velocity along the glideslope
  // normal vector of the vertical direction (i.e. in what direction altitude increases)
  double nX;
  double nY;
  double nZ;

  BcsFreeStreamData();

  Assigner *getAssigner();
  void setup(const char *, ClassAssigner * = 0);

  template<int dim>
  void getConservativeState(VarFcn *, double, double[dim]) const;
  double calculateAltitude(double, double * = 0) const;
};

//------------------------------------------------------------------------------
// 5 August 2019: Andrew McClellan; calculate the altitude according to a constant velocity
// descent; altitude is output in meters
inline
double BcsFreeStreamData::calculateAltitude(double t, double *xCM) const {
  double altCM = 0.0;
  if(xCM) {
    altCM = xCM[0]*nX + xCM[1]*nY + xCM[2]*nZ;
  }
  altCM /= usStandardAtmosphere.problem_Length_conversion;  // convert to meters
  return altitude + flightPathVelocity*sin(flightPathAngle)*t + altCM;
}

//------------------------------------------------------------------------------

struct MultiPhaseBcsData {

  enum Type {INLET = 0, OUTLET = 1} type;
  int initialFluidModelID;

  ObjectMap<BcsFreeStreamData> inlet;
  ObjectMap<BcsFreeStreamData> outlet;

  MultiPhaseBcsData();

  void setup(const char *, ClassAssigner * = 0);

  BcsFreeStreamData *operator[](int fluidModelID) const {
    switch(type) {
      case INLET : {
        auto it = std::find_if(inlet.dataMap.begin(), inlet.dataMap.end(),
                               [&](const std::pair<int, BcsFreeStreamData*>& p) {return p.second->fluidModelID == fluidModelID;});
        if(it != inlet.dataMap.end()) { return it->second; }
      } break;
      case OUTLET : {
        auto it = std::find_if(outlet.dataMap.begin(), outlet.dataMap.end(),
                               [&](const std::pair<int, BcsFreeStreamData*>& p) {return p.second->fluidModelID == fluidModelID;});
        if(it != outlet.dataMap.end()) { return it->second; }
      } break;
    }
    return nullptr;
  }
};

//------------------------------------------------------------------------------

struct WallFcnData {

  enum CutOff {NO = 0, YES = 1} cutoff;
  int maxits;
  double eps;
  double vsl_threshold;
  int ntrapz;

  WallFcnData();

  void setup(const char *, ClassAssigner * = 0);
};

//------------------------------------------------------------------------------

struct MaxwellSlipData {

  bool refVelOn;
  double sigma_acc;
  double zeroconstant;

  MaxwellSlipData();

  void setup(const char *, ClassAssigner * = 0);
};

//------------------------------------------------------------------------------

struct ExactRiemannWallData {

  enum Flux {ROE = 0, VANLEER = 1, HLLE = 2, HLLC = 3, ROTATED_RIEMANN = 4} flux; // NOTE: must be identical to SchemeData::Flux

  ExactRiemannWallData();

  void setup(const char *, ClassAssigner * = 0);
};

//------------------------------------------------------------------------------

struct BcsWallData {

  enum Type {ISOTHERMAL = 0, ADIABATIC = 1} type;
  enum Integration {AUTO = 0, WALL_FUNCTION = 1, ODE = 2, UNIVERSAL = 3, FULL = 4} integration;
  enum Method {STANDARD = 0, EXACT_RIEMANN_PROBLEM = 1} method;
  enum ShearStress {ORIGINAL = 0, MODIFIED = 1} shearStress;
  enum SlipModel {NONE = 0, MAXWELL = 1} slipModel;

  double temperature;
  double delta;
  double uniFactor;

  bool anyFullInt;
  bool anyWallFcn;

  WallFcnData wallFcnData;
  MaxwellSlipData maxwellSlipData;
  ExactRiemannWallData erwData;

  BcsWallData();

  void setup(const char *, ClassAssigner * = 0);
};

//------------------------------------------------------------------------------

struct BcsSymmetryData {

  enum Enforcement {WEAK = 0, STRONG = 1} enforcement;
  enum Priority {OUTLETS = 0, SYMMETRY = 1} priority;

  BcsSymmetryData();

  void setup(const char *, ClassAssigner * = 0);
};

//------------------------------------------------------------------------------

struct BcsHydroData {

  double depth;

  BcsHydroData();

  void setup(const char *, ClassAssigner * = 0);
};

//------------------------------------------------------------------------------

struct ActuatorDisk {

  enum VelocityReconstructionMethod {AVERAGE = 1, FIRSTORDER = 2, SECONDORDER = 3} velocityReconstructionMethod;
  enum ActuatorDiskMethod {SOURCETERM = 1, RIEMANNSOLVER= 2, SOURCETERMINCOMPLETE = 3} actuatorDiskMethod;
  enum SourceTermExpression {OLD = 1, CORRECTED = 2} sourceTermExpression;

  double pressureJump;

  ActuatorDisk();

  void setup(const char*,ClassAssigner * = 0);
};

//------------------------------------------------------------------------------

struct BoundaryData {

  enum Type {UNSPECIFIED = -1, DIRECTSTATE = 1, MASSFLOW = 2, POROUSWALL = 3, SYMMETRYPLANE = 4, ACTUATORDISK = 5,
             INLET = 6, OUTLET = 7, WALL = 8, MULTIPHASE = 9} type;
  enum vars {DENSITY = 0, VX = 1, VY = 2, VZ = 3, PRESSURE = 4, TEMPERATURE = 5, TOTALPRESSURE = 6, TOTALTEMPERATURE = 7,
             MDOT = 8, NUTILDE = 9, KENERGY = 10, EPSILON = 11, SIZE = 12};
  enum PorosityUpdateMethod {CONSTANT = 0, PAYNES_LAW = 1} porosityUpdateMethod;

  bool inVar[SIZE], outVar[SIZE];
  double density;
  double velocityX, velocityY, velocityZ;
  double pressure;
  double temperature;
  double totalPressure;
  double totalTemperature;
  double mdot;
  double nutilde;
  double kenergy;
  double epsilon;
  int integrationWF;
  double deltaWF;
  double porosity;
  int poreShape;
  double poissonRatio;
  double thickness;
  double weavingLength;
  double massFlow;

  ActuatorDisk actuatorDisk;
  BcsFreeStreamData inlet;
  BcsFreeStreamData outlet;
  MultiPhaseBcsData multiphase;

  BoundaryData();

  Assigner *getAssigner();

  template<int dim>
  void getInletDirectState(double[dim]) const;
};

//------------------------------------------------------------------------------

struct BcsData {

  BcsFreeStreamData inlet;
  BcsFreeStreamData outlet;
  BcsWallData wall;
  BcsSymmetryData symmetry;
  BcsHydroData hydro;
  ObjectMap<BoundaryData> bcMap;

  BcsData();

  void setup(const char *, ClassAssigner * = 0);

  bool bc_type(BoundaryData::Type t) const {
    return std::any_of(bcMap.dataMap.begin(), bcMap.dataMap.end(), [&](const std::pair<int, BoundaryData*>& p) {return p.second->type == t;});
  }
};

//------------------------------------------------------------------------------

struct GasModelData {

  enum Type {IDEAL = 0, STIFFENED = 1} type;

  double specificHeatRatio;
  double idealGasConstant;
  double pressureConstant;
  double specificHeatPressure;

  GasModelData();

  void setup(const char *, ClassAssigner * = 0);
};

//------------------------------------------------------------------------------

struct JWLModelData {

  enum Type {IDEAL = 0, JWL = 1} type;

  double omega; // = specificHeatRatio-1.0
  double idealGasConstant;
  double A1, R1, rhoref, A2, R2;

  JWLModelData();

  void setup(const char *, ClassAssigner * = 0);
};

//------------------------------------------------------------------------------

struct LiquidModelData {

  enum Type {COMPRESSIBLE = 0} type;

  enum YesNo {YES = 0, NO = 1};
  YesNo check;

  YesNo burnable;

  // the state equation is derived from a linearization of the bulk modulus wrt
  // pressure: K = k1 + k2 * P
  // the integration constant of the ODE is given by the couple (RHOrefwater,Prefwater)
  double specificHeat;
  double k1water;
  double k2water;
  double Bwater;
  double Prefwater;
  double RHOrefwater;

  // the state equation can be put in the form P=Pref+alpha*rho^beta
  // with Pref, alpha and beta derived from k1, k2 and the 'initial' couple
  double Pref;
  double alpha;
  double beta;

  LiquidModelData();

  void setup(const char *, ClassAssigner * = 0);
};

//------------------------------------------------------------------------------

struct ExcludedVolumeModelData {

  double R;    // mixture gas constant
  double b;    // specific co-volume, i.e. ratio of co-volume of the mixture to the mixture average molecular weight (b0/Mbar)

  ExcludedVolumeModelData();

  void setup(const char *, ClassAssigner * = 0);
};

//------------------------------------------------------------------------------

struct FluidModelData {

  enum Fluid {PERFECT_GAS = 0, LIQUID = 1, JWL = 2, STIFFENED_GAS = 3, EXCLUDED_VOLUME = 4, UNDEFINED = 5} fluid;
  double rhomin;
  double pmin;
  double nutmin;
  bool verif_clipping;

  GasModelData gasModel;
  JWLModelData jwlModel;
  LiquidModelData liquidModel;
  ExcludedVolumeModelData excludedVolumeModel;

  FluidModelData();

  Assigner *getAssigner();
  void setup(const char *, ClassAssigner * = 0);
};

//------------------------------------------------------------------------------

struct HerschelBulkleyData {

  double consistency;
  double flowIndex;
  double yieldStress;
  double yieldingViscosity;

  HerschelBulkleyData();

  void setup(const char *, ClassAssigner * = 0);
};

//------------------------------------------------------------------------------

struct ViscosityModelData {

  enum Type {CONSTANT = 0, SUTHERLAND = 1, PRANDTL = 2, HERSCHEL_BULKLEY = 3} type;
  enum ZeroViscosityRatioAtWall {OFF = 0, ON = 1} zeroViscosityRatioAtWall;

  double sutherlandReferenceTemperature;
  double sutherlandConstant;
  double dynamicViscosity;
  double bulkViscosity;
  double viscosityRatio;

  HerschelBulkleyData hb;

  ViscosityModelData();

  void setup(const char *, ClassAssigner * = 0);
};

//------------------------------------------------------------------------------

struct ThermalCondModelData {

  enum Type {CONSTANT_PRANDTL = 0, CONSTANT = 1} type;

  double prandtl;
  double conductivity;

  ThermalCondModelData();

  void setup(const char *, ClassAssigner * = 0);
};

//------------------------------------------------------------------------------

struct PorousMedia {

  double iprimex, iprimey, iprimez;
  double jprimex, jprimey, jprimez;
  double kprimex, kprimey, kprimez;
  double alphax, alphay, alphaz;
  double betax, betay, betaz;
  double idr, ldr;

  PorousMedia();

  void setup(const char *, ClassAssigner * = 0);
};

//------------------------------------------------------------------------------

struct InitialConditions {

  double mach;
  double velocity;
  double alpha, beta;
  double pressure;
  double density;
  double temperature;
  double nutilde;
  double kenergy;
  double epsilon;

  InitialConditions();

  void setup(const char *, ClassAssigner * = 0);
};

//------------------------------------------------------------------------------

struct VolumeData {

  enum Type {FLUID = 0, POROUS = 1} type;
  int fluidModelID;

  PorousMedia porousMedia;
  InitialConditions initialConditions;

  VolumeData();

  Assigner *getAssigner();
};

//------------------------------------------------------------------------------

struct SAModelData {

  enum Form {NOFT2 = 0, FV3 = 1, STANDARD = 2, NEGATIVE = 3} form;

  double cb1;
  double cb2;
  double cw2;
  double cw3;
  double cv1;
  double cv2;
  double sigma;
  double vkcst;
  double rlim;
  double c2;
  double c3;
  double ct3;
  double ct4;
  double cn1;
  double d2wall_eps;
  double mut_eps;

  SAModelData();

  void setup(const char *, ClassAssigner * = 0);
};

//------------------------------------------------------------------------------

struct DESModelData : public SAModelData {

  double cdes;

  DESModelData();

  void setup(const char *, ClassAssigner * = 0);
};

//------------------------------------------------------------------------------

struct KEModelData {

  double sigma_k;
  double sigma_eps;
  double sigma_eps1;
  double sigma_eps2;
  double c_mu;

  KEModelData();

  void setup(const char *, ClassAssigner * = 0);
};

//------------------------------------------------------------------------------

struct WallDistanceMethodData {

  enum Type {ITERATIVE = 0, NONITERATIVE = 1, HYBRID = 2, CLOSESTPOINT = 3} type;
  enum FrequencyAdaptation {OFF = 0, SA = 1, D2W = 2, BOTH = 3} frequencyadaptation;
  enum ComputeInactive {NO = 0, YES = 1} computeinactive;
  enum InactiveSign {POSITIVE = 0, NEGATIVE = 1} inactivesign;
  enum Sensitivities {ANALYTICAL = 0, FINITEDIFFERENCE = 1} sensitivities;

  int maxIts;
  double eps;
  double epsSA;
  double epsD2W;
  int maxSkip;
  int faceID;

  WallDistanceMethodData();

  void setup(const char *, ClassAssigner * = 0);
};

//------------------------------------------------------------------------------

struct TurbulenceModelData {

  enum Type {ONE_EQUATION_SPALART_ALLMARAS = 0, ONE_EQUATION_DES = 1, TWO_EQUATION_KE = 2} type;

  SAModelData sa;
  DESModelData des;
  KEModelData ke;
  WallDistanceMethodData d2wall;

  TurbulenceModelData();

  void setup(const char *, ClassAssigner * = 0);
};

//------------------------------------------------------------------------------

struct SmagorinskyLESData {

  double c_s;

  SmagorinskyLESData();

  void setup(const char *, ClassAssigner * = 0);
};

//------------------------------------------------------------------------------

struct VremanLESData {

  double c_s;
  double threshold;
  double eps;

  VremanLESData();

  void setup(const char *, ClassAssigner * = 0);
};

//------------------------------------------------------------------------------

struct WaleLESData {

  double c_w;

  WaleLESData();

  void setup(const char *, ClassAssigner * = 0);
};

//------------------------------------------------------------------------------

struct ClippingData {

  double cs_max;
  double pt_min;
  double pt_max;

  ClippingData();

  void setup(const char *, ClassAssigner * = 0);
};

//------------------------------------------------------------------------------

struct DynamicLESData {

  ClippingData clip;

  DynamicLESData();

  void setup(const char *, ClassAssigner * = 0);
};

//------------------------------------------------------------------------------

struct VMSLESData {

  double c_s_prime;
  int agglomeration_width;
  int agglomeration_depth;

  VMSLESData();

  void setup(const char *, ClassAssigner * = 0);
};

//------------------------------------------------------------------------------

struct LESModelData {

  enum Type {SMAGORINSKY = 0, DYNAMIC = 1, VMS = 2, DYNAMICVMS = 3, WALE = 4, VREMAN = 5} type;
  enum Delta {VOLUME = 0, SIDE = 1} delta;

  SmagorinskyLESData sma;
  VremanLESData vre;
  DynamicLESData dles;
  VMSLESData vms;
  WaleLESData wale;

  LESModelData();

  void setup(const char *, ClassAssigner * = 0);
};


//------------------------------------------------------------------------------

struct StochasticTurbulenceModelData {


  enum ValuePerturbation {OFF_VA = 0, ON_VA = 1} value_perturbation;
  enum VectorPerturbation {OFF_VE = 0, ON_VE = 1, PSI = 2} vector_perturbation;
  enum ScalePerturbation {OFF_S = 0, ON_S = 1} scale_perturbation;

  double delta_psi; // controls uncertainty perturbations of eigenvectors
  double mu_z;      // set mean for R.V. z, where mu_z in [0,1]
  double delta_z;   // set perturbation magnitude for R.V. z
  double mu_J;      // set mean for discrete R.V. J
  double p0;        // set probability for mean of discrete R.V. J
  double uq_urlx;
  double kfactor;
  double mu_k;
  double delta_k;
  double max_k;
  double mu_zeta;
  // double delta_zeta;
  double mu_eta;
  // double delta_eta;
  double mu_theta;
  // double delta_theta;
  double mu_iota;
  // double delta_iota;
  double delta_ev;
  bool writeOutput;
  bool readOutput;
  bool computeStochasticDrawWeights;
  bool useKWeight;
  double weight_C1;
  double weight_C2;
  double weight_C3;
  double weight_eul2;
  double weight_k;
  const char *outputFileName;


  StochasticTurbulenceModelData();

  void setup(const char *, ClassAssigner * = 0);
};

//------------------------------------------------------------------------------

struct EigenperturbationData {

  enum Eigenvalue {UNPERTURBEDVALUE = 0, C1 = 1, C2 = 2, C3 =3} eigenvalue;
  enum Eigenvector {UNPERTURBEDVECTOR = 0, MINIMUM = 1, MAXIMUM = 2} eigenvector;
  double uq_urlx;

  EigenperturbationData();

  void setup(const char *, ClassAssigner * = 0);
};

//------------------------------------------------------------------------------

struct TBFixData {

  double x0;
  double y0;
  double z0;
  double x1;
  double y1;
  double z1;

  TBFixData();

  void setup(const char *, ClassAssigner * = 0);
};

//------------------------------------------------------------------------------

struct TripDomainData {

  TBFixData bfix;

  TripDomainData();

  void setup(const char *, ClassAssigner * = 0);
};

//------------------------------------------------------------------------------

struct TurbulenceClosureData {

  enum Type {NONE = 0, EDDY_VISCOSITY = 1, LES = 2} type;

  double prandtlTurbulent;
  TurbulenceModelData tm;
  LESModelData les;
  TripDomainData tr;
  EigenperturbationData ep;
  StochasticTurbulenceModelData stm;

  TurbulenceClosureData();

  void setup(const char *, ClassAssigner * = 0);
};

//------------------------------------------------------------------------------

struct ProgrammedBurnData {

  int unburnedEOS, burnedEOS;
  double ignitionX0, ignitionY0, ignitionZ0;
  double e0;
  double cjDetonationVelocity;
  double cjPressure;
  double cjDensity;
  double cjEnergy;
  double ignitionTime;
  double factorB;
  double factorS;
  int ignited;
  int limitPeak;

  ProgrammedBurnData();

  void setup(const char *, ClassAssigner * = 0);
};

//------------------------------------------------------------------------------

struct SphereData {

  double cen_x, cen_y, cen_z, radius;
  int fluidModelID;
  InitialConditions initialConditions;
  ProgrammedBurnData programmedBurn;

  bool inside(double x, double y, double z, double eps) const {
    return std::sqrt((x-cen_x)*(x-cen_x) + (y-cen_y)*(y-cen_y) + (z-cen_z)*(z-cen_z)) <= (radius+eps);
  }

  SphereData();

  void setup(const char *, ClassAssigner * = 0);
  Assigner *getAssigner();
};

//------------------------------------------------------------------------------

struct PrismData {

  double cen_x, cen_y, cen_z, w_x, w_y, w_z;
  double X0, Y0, Z0, X1, Y1, Z1;
  int fluidModelID;
  InitialConditions initialConditions;

  bool inside(double x, double y, double z, double eps) const {
    return (x <= (X1+eps) && x >= (X0-eps) && y <= (Y1+eps) && y >= (Y0-eps) && z <= (Z1+eps) && z >= (Z0-eps));
  }

  PrismData();

  void setup(const char *, ClassAssigner * = 0);
  Assigner *getAssigner();
};

//------------------------------------------------------------------------------

struct LineData {
  double X0, Y0, Z0, X1, Y1, Z1;

  LineData();

  void setup(const char *, ClassAssigner * = 0);
};

//------------------------------------------------------------------------------

struct PlaneData {

  double cen_x, cen_y, cen_z, nx, ny, nz;
  int fluidModelID;
  InitialConditions initialConditions;

  PlaneData();

  void setup(const char *, ClassAssigner * = 0);
  Assigner *getAssigner();
};

//------------------------------------------------------------------------------

struct CylinderData {

  double cen_x, cen_y, cen_z; // center of circle at 1st end of cylinder
  double nx, ny, nz; // unit normal to circle at 1st end pointing in direction of 2nd end along axis
  double r, L; // radius and length
  int fluidModelID;
  InitialConditions initialConditions;

  bool inside(double x, double y, double z, double eps) const {
    double dx = x - cen_x, dy = y - cen_y, dz = z - cen_z;
    double s = nx*dx + ny*dy + nz*dz;
    if(s < -eps || s > L+eps) return false;
    else {
      double qx = dx - s*nx, qy = dy - s*ny, qz = dz - s*nz;
      return (sqrt(qx*qx + qy*qy + qz*qz) <= r+eps);
    }
  }

  CylinderData();

  void setup(const char *, ClassAssigner * = 0);
  Assigner *getAssigner();
};

//------------------------------------------------------------------------------

struct EllipsoidData {

  double cen_x, cen_y, cen_z;
  double a, b, c;
  double axis00, axis01, axis02, axis10, axis11, axis12, axis20, axis21, axis22;

  bool inside(double x, double y, double z, double eps) const {
    // note: currently, the three axes of symmetry are assumed to be aligned with the global x, y and z axes in this function
    return std::sqrt((x-cen_x)*(x-cen_x)/(a*a) + (y-cen_y)*(y-cen_y)/(b*b) + (z-cen_z)*(z-cen_z)/(c*c)) <= (1+eps);
  }

  EllipsoidData();

  void setup(const char *, ClassAssigner * = 0);
};

//------------------------------------------------------------------------------

struct TriangleSetData {

  int surfaceID;
  double eps;

  TriangleSetData();

  void setup(const char *, ClassAssigner * = 0);
};

//------------------------------------------------------------------------------

struct StarData {

  double cen_x, cen_y, cen_z; // center of circle at 1st end of cylinder
  double nx, ny, nz; // unit normal to circle at 1st end pointing in direction of 2nd end along axis
  double r, L; // radius and length
  int fluidModelID;
  InitialConditions initialConditions;

  bool inside(double x, double y, double z, double eps) const {
    double dx = x - cen_x, dy = y - cen_y, dz = z - cen_z;
    double s = nx*dx + ny*dy + nz*dz;
    if(s < -eps || s > L+eps) return false;
    else {
      double qx = dx - s*nx, qy = dy - s*ny, qz = dz - s*nz;
      return (sqrt(qx*qx + qy*qy + qz*qz) <= r+eps &&
             (fabs(qx) + fabs(qy) + fabs(qz) + sqrt(2*(fabs(qx*qy) + fabs(qy*qz) + fabs(qx*qz)))) >= r-eps);
    }
  }

  StarData();

  void setup(const char *, ClassAssigner * = 0);
  Assigner *getAssigner();
};

//------------------------------------------------------------------------------

struct ConeData {

  double cen0_x, cen0_y, cen0_z; // center of circle at 1st end of cone
  double cen1_x, cen1_y, cen1_z; // center of circle at 2nd end of cone
  double r0, r1; // radii of first and second circles
  int fluidModelID;
  InitialConditions initialConditions;

  bool inside(double x, double y, double z, double eps) const {
    double dx = x - cen0_x, dy = y - cen0_y, dz = z - cen0_z;
    double nx = cen1_x - cen0_x, ny = cen1_y - cen0_y, nz = cen1_z - cen0_z;
    double L = sqrt(nx*nx + ny*ny + nz*nz); nx /= L; ny /= L; nz /= L;
    double s = nx*dx + ny*dy + nz*dz;
    if(s < -eps || s > L+eps) return false;
    else {
      double qx = dx - s*nx, qy = dy - s*ny, qz = dz - s*nz;
      double r = r0 + s/L*(r1 - r0);
      return (sqrt(qx*qx + qy*qy + qz*qz) <= r+eps);
    }
  }

  ConeData();

  void setup(const char *, ClassAssigner * = 0);
  Assigner *getAssigner();
};

//------------------------------------------------------------------------------

struct WedgeData {

  double cen0_x, cen0_y, cen0_z; // center of first end of wedge
  double cen1_x, cen1_y, cen1_z; // center of second end of wedge
  double r0, r1; // radii of first and second circles (in-plane)
  double ux, uy, uz; // length of extrusion of wedge (currently only supports cardinal directions)
  int fluidModelID;
  InitialConditions initialConditions;

  bool inside(double x, double y, double z, double eps) const {
    double dx = x - cen0_x, dy = y - cen0_y, dz = z - cen0_z;
    double nx = cen1_x - cen0_x, ny = cen1_y - cen0_y, nz = cen1_z - cen0_z;
    double dx1, dx2, nx1, nx2;
    if(ux == 0 && uy == 0 && uz != 0) { // x-y plane
      dx1 = dx;
      dx2 = dy;
      nx1 = nx;
      nx2 = ny;
      if(z < cen0_z - uz/2 - eps || z > cen0_z + uz/2 + eps) return false;
    }
    else if(ux == 0 && uy != 0 && uz == 0) { // x-z plane
      dx1 = dx;
      dx2 = dz;
      nx1 = nx;
      nx2 = nz;
      if(y < cen0_y - uy/2 - eps || y > cen0_y + uy/2 + eps) return false;
    }
    else if(ux != 0 && uy == 0 && uz == 0) { // y-z plane
      dx1 = dy;
      dx2 = dz;
      nx1 = ny;
      nx2 = nz;
      if(x < cen0_x - ux/2 - eps || x > cen0_x + ux/2 + eps) return false;
    }
    else return false;

    double L = sqrt(nx1*nx1 + nx2*nx2); nx1 /= L; nx2 /= L;
    double s = nx1*dx1 + nx2*dx2;
    if(s < -eps || s > L+eps) return false;
    else {
      double qx1 = dx1 - s*nx1, qx2 = dx2 - s*nx2;
      double r = r0 + s/L*(r1 - r0);
      return (sqrt(qx1*qx1 + qx2*qx2) <= r+eps);
    }
  }

  WedgeData();

  void setup(const char *, ClassAssigner * = 0);
  Assigner *getAssigner();
};

//------------------------------------------------------------------------------

struct TriangularPrismData {

  double x0, y0, z0, x1, y1, z1, x2, y2, z2; // coordinates of the vertices of the base
  double nx, ny, nz; // unit vector direction in which the base is extruded to form the prism
  double L; // length of prism
  int fluidModelID;
  InitialConditions initialConditions;

  bool inside(double, double, double, double) const;

  TriangularPrismData();

  void setup(const char *, ClassAssigner * = 0);
  Assigner *getAssigner();
};

//------------------------------------------------------------------------------

struct CapsuleData {
  // two hemispheres connected by a cylinder
  double cen_x, cen_y, cen_z; // center of circle at 1st end of cylinder
  double nx, ny, nz; // unit normal to circle at 1st end pointing in direction of 2nd end along axis
  double r, L; // radius and length of cylinder (note: total length of capsule is L+2*r)
  int fluidModelID;
  InitialConditions initialConditions;

  bool inside(double x, double y, double z, double eps) const {
    double dx = x - cen_x, dy = y - cen_y, dz = z - cen_z;
    double s = nx*dx + ny*dy + nz*dz;
    if(s < 0) return (sqrt(dx*dx + dy*dy + dz*dz) <= r+eps);
    else if(s > L) {
      double qx = dx - L*nx, qy = dy - L*ny, qz = dz - L*nz;
      return (sqrt(qx*qx + qy*qy + qz*qz) <= r+eps);
    }
    else {
      double qx = dx - s*nx, qy = dy - s*ny, qz = dz - s*nz;
      return (sqrt(qx*qx + qy*qy + qz*qz) <= r+eps);
    }
  }

  CapsuleData();

  void setup(const char *, ClassAssigner * = 0);
  Assigner *getAssigner();
};

//------------------------------------------------------------------------------

struct MultiInitialConditionsData {

  ObjectMap<SphereData> sphereMap;
  ObjectMap<PrismData> prismMap;
  ObjectMap<PlaneData> planeMap;
  ObjectMap<CylinderData> cylinderMap;

  void setup(const char *, ClassAssigner * = 0);
};

//------------------------------------------------------------------------------

struct PointData {

  int fluidModelID;
  double x, y, z;
  InitialConditions initialConditions;

  PointData();

  void setup(const char *, ClassAssigner * = 0);
  Assigner *getAssigner();
};

//------------------------------------------------------------------------------

struct DummyPointData {

  int fluidModelID;

  DummyPointData();

  Assigner *getAssigner();
};

//------------------------------------------------------------------------------

struct EmbeddedInitialConditionsData {

  enum FindFarfieldNode {OFF = 0, ON = 1} findFarfieldNode;

  ObjectMap<PointData> pointMap;
  ObjectMap<DummyPointData> dummyPointMap;

  EmbeddedInitialConditionsData();

  void setup(const char *, ClassAssigner * = 0);
};

//------------------------------------------------------------------------------

struct SparseGridData {

  const char *tabulationFileName;
  int numberOfTabulations;
  int verbose;
  int minPoints;
  int maxPoints;
  double relAccuracy;
  double absAccuracy;
  double dimAdaptDegree;

  double range1min, range1max, mapBaseValue1;
  int numDomainDim1;
  double range2min, range2max, mapBaseValue2;
  int numDomainDim2;
  double range3min, range3max, mapBaseValue3;
  int numDomainDim3;
  double range4min, range4max, mapBaseValue4;
  int numDomainDim4;
  double range5min, range5max, mapBaseValue5;
  int numDomainDim5;
  double range6min, range6max, mapBaseValue6;
  int numDomainDim6;
  typedef double Range[2];
  Range *range;
  double *mapBaseValue;
  int *numDomainDim;

  int numOutputs;
  int numInputs;

  SparseGridData();

  void setup(const char *, ClassAssigner * = 0);
};

//------------------------------------------------------------------------------

struct MultiFluidData {

  enum Method {NONE = 0, GHOSTFLUID_FOR_POOR = 1, GHOSTFLUID_WITH_RIEMANN} method;
  enum InterfaceTracking {LINEAR = 0, GRADIENT = 1, HERMITE = 2};
  enum RiemannComputation {FE = 0, RK2 = 1, TABULATION2 = 2, TABULATION5 = 3} riemannComputation;
  enum TypePhaseChange {ASIS = 0, RIEMANN_SOLUTION = 1, EXTRAPOLATION = 2} typePhaseChange;
  enum CopyCloseNodes {FALSE = 0, TRUE = 1} copyCloseNodes;
  enum InterfaceType {FSF = 0, FF = 1, FSFandFF = 2} interfaceType;
  enum InterfaceTreatment {FIRSTORDER = 0, SECONDORDER = 1} interfaceTreatment;
  enum InterfaceExtrapolation {EXTRAPOLATIONFIRSTORDER = 0, EXTRAPOLATIONSECONDORDER = 1, AUTO = 2} interfaceExtrapolation;
  enum InterfaceLimiter {LIMITERNONE = 0, LIMITERALEX1 = 1} interfaceLimiter;
  enum LevelSetMethod {CONSERVATIVE = 0, HJWENO = 1, SCALAR = 2, PRIMITIVE = 3, TRIANGULATED = 4} levelSetMethod;
  enum RiemannNormal {REAL = 0, MESH = 1, LEGACYMESH = 2} riemannNormal;
  enum Prec {NON_PRECONDITIONED = 0, PRECONDITIONED = 1, SAME_AS_PROBLEM = 2} prec;

  int bandlevel;
  int frequency;
  int outputdiff;
  int riemannMaxIts;
  int testCase;

  double eps;
  double jwlRelaxationFactor;
  double riemannEps;

  MultiInitialConditionsData multiInitialConditions;
  SparseGridData sparseGrid;

  MultiFluidData();

  void setup(const char *, ClassAssigner * = 0);
};

//------------------------------------------------------------------------------

struct ThermalNonEquilibriumData {

  enum RelaxationModel {NONE = 0, LANDAU_TELLER = 1} rmodel;

  ThermalNonEquilibriumData();

  void setup(const char *, ClassAssigner * = 0);
};

//------------------------------------------------------------------------------

struct ChemicalNonEquilibriumData {

  enum KineticModel {NONE = 0, AIR = 1} kmodel;

  ChemicalNonEquilibriumData();

  void setup(const char *, ClassAssigner * = 0);
};


//------------------------------------------------------------------------------

struct EquationsData {

  enum Type {EULER = 0, NAVIER_STOKES = 1} type;

  // it is assumed that in a two-phase flow, fluidModel represents the surrounding fluid
  // whereas fluidModel2 represents the fluid in the bubbles. This means that the
  // uniform boundary conditions and the uniform initial conditions are computed
  // with the values given to characterize the first fluid!

  double gravity_x, gravity_y, gravity_z;

  ObjectMap<FluidModelData> fluidModelMap;
  ViscosityModelData viscosityModel;
  ThermalCondModelData thermalCondModel;
  TurbulenceClosureData tc;
  ThermalNonEquilibriumData th;
  ChemicalNonEquilibriumData ch;

  EquationsData();

  void setup(const char *, ClassAssigner * = 0);

  int numPhase() { return fluidModelMap.dataMap.size(); }
  FluidModelData& fluidModel(int i = 0) { return *(fluidModelMap.dataMap[i]); }
};

//------------------------------------------------------------------------------
    
struct SymmetryData {
  
  double nx, ny, nz;
  
  SymmetryData();
  
  void setup(const char *, ClassAssigner * = 0);
};

//----------------------------------------------------------------------------------------------------------------------------------
  
struct LeastSquaresData {

  enum Type {ORDINARY = 0, WEIGHTED = 1} type;
  int min_count; // if the number of edges connected to a node is less than min_count then the least squares nodal weights are
                 // not computed and set to zero (this is only relevant when the regularization parameter is not specified)

  LeastSquaresData();

  void setup(const char *, ClassAssigner * = 0);
};

//------------------------------------------------------------------------------

struct SchemeData {

  enum AdvectiveOperator {FINITE_VOLUME = 0, FE_GALERKIN = 1} advectiveOperator;
  enum Flux {ROE = 0, VANLEER = 1, HLLE = 2, HLLC = 3, ROTATED_RIEMANN = 4} flux;

  enum Reconstruction {CONSTANT = 0, LINEAR = 1} reconstruction;

  enum Limiter {NONE = 0, VANALBADA = 1, BARTH = 2, VENKAT = 3, P_SENSOR = 4, MICHALAK = 5, MODIFIED_VANALBADA = 6, WBAP = 7} limiter;
  enum PreComputeLimiter {PRE_COMPUTE_LIMITER_OFF = 0, PRE_COMPUTE_LIMITER_ON = 1} preComputeLimiter;
  enum SecondLimiter {SECOND_LIMITER_OFF = 0, SECOND_LIMITER_ON = 1} secondLimiter;
  enum Gradient {LEAST_SQUARES = 0, GALERKIN = 1, NON_NODAL = 2} gradient;
  enum Dissipation {SECOND_ORDER = 0, SIXTH_ORDER = 1} dissipation;

  double beta;
  double gamma;
  double xiu;
  double xic;
  double eps;
  double preComputeLimiterEps;

  double xirho;
  double xip;
  double vel_fac;

  int preComputeLimiterFreq;

  struct MaterialFluxData {
    Flux flux;
    Assigner *getAssigner();
  };
  struct RotatedRiemannData {
    enum UpwindDirection {VELOCITY_DIFFERENCE = 0, VELOCITY_AVERAGE = 1, TRIANGLE_SET = 2} upwindDirection;
    enum PreCompute {PRE_COMPUTE_OFF = 0, PRE_COMPUTE_ON = 1, PRE_COMPUTE_AUTO = 2} preCompute;
    // note: jacobian controls whether or not the contribution of the upwindDirection is included in the flux function Jacobian
    //       APPROXIMATE: the contribution of the upwindDirection is not included
    //       EXACT: the contribution of the upwindDirection is included
    //       AUTO (default): changed to EXACT when the matrix-vector product is exact; changed to APPROXIMATE when the
    //                       matrix-vector product is approximate or finite difference.
    enum ReferenceDirection {INLET_VELOCITY = 0, CUSTOM_DIRECTION = 1} referenceDirection;
    enum Jacobian {AUTO = 0, APPROXIMATE = 1, EXACT = 2} jacobian;
    double eps;
    double preComputeEps;
    int N;
    Flux flux1, flux2, flux3;
    TriangleSetData triangleSetData;
    SymmetryData directionData;

    RotatedRiemannData();
    void setup(const char *, ClassAssigner * = 0);
  };

  // We now allow different flux functions to be used for different materials.
  // The behavior is that if the flux is specified for a fluid id in this map,
  // then it is used.  Otherwise, the default (schemedata.flux) is used for
  // that material.
  ObjectMap<MaterialFluxData> fluxMap;
  RotatedRiemannData rotatedRiemann;
  LeastSquaresData leastSquaresData;

  int allowsFlux;

  // allowsFlux = 0 for levelset equation (the choice of flux for the levelset is hardcoded)
  SchemeData(int allowsFlux = 1, Reconstruction = LINEAR);

  void setup(const char *, ClassAssigner * = 0);
};

//------------------------------------------------------------------------------

struct CFixData {

  // nodal coordinates of first circle
  double x0;
  double y0;
  double z0;

  // nodal coordinates of second circle
  double x1;
  double y1;
  double z1;

  // radii of 1st and 2nd circle
  double r0;
  double r1;

  int failsafeN;
  enum {OFF = 0, ON = 1, ALWAYSON = 2} failsafe;

  CFixData();

  void setup(const char *, ClassAssigner * = 0);
};

//------------------------------------------------------------------------------

struct SFixData {

  double x0;
  double y0;
  double z0;
  double r;

  int failsafeN;
  enum {OFF = 0, ON = 1, ALWAYSON = 2} failsafe;

  SFixData();

  void setup(const char *, ClassAssigner * = 0);
};

//------------------------------------------------------------------------------

struct BFixData {

  double x0;
  double y0;
  double z0;
  double x1;
  double y1;
  double z1;

  int failsafeN;
  enum {OFF = 0, ON = 1, ALWAYSON = 2} failsafe;

  BFixData();

  void setup(const char *, ClassAssigner * = 0);
};

//------------------------------------------------------------------------------

struct DHFixData {

  double angle;
  int numLayers;
  double maxDist;

  DHFixData();

  void setup(const char *, ClassAssigner * = 0);
};

//------------------------------------------------------------------------------

struct AMRFixData {

  enum {OFF = 0, ALWAYSON = 1} failsafe;

  AMRFixData();

  void setup(const char *, ClassAssigner * = 0);
};

//------------------------------------------------------------------------------

struct SchemeFixData {

  static const int num = 10;

  enum Symmetry {NONE = 0, X = 1, Y = 2, Z = 3} symmetry;

  SFixData *spheres[num];
  BFixData *boxes[num];
  CFixData *cones[num];

  SFixData sfix1;
  SFixData sfix2;
  SFixData sfix3;
  SFixData sfix4;
  SFixData sfix5;
  SFixData sfix6;
  SFixData sfix7;
  SFixData sfix8;
  SFixData sfix9;
  SFixData sfix10;

  BFixData bfix1;
  BFixData bfix2;
  BFixData bfix3;
  BFixData bfix4;
  BFixData bfix5;
  BFixData bfix6;
  BFixData bfix7;
  BFixData bfix8;
  BFixData bfix9;
  BFixData bfix10;

  CFixData cfix1;
  CFixData cfix2;
  CFixData cfix3;
  CFixData cfix4;
  CFixData cfix5;
  CFixData cfix6;
  CFixData cfix7;
  CFixData cfix8;
  CFixData cfix9;
  CFixData cfix10;

  DHFixData dhfix;

  AMRFixData amrfix;

  SchemeFixData();

  void setup(const char *, ClassAssigner * = 0);
};

//------------------------------------------------------------------------------

struct BoundarySchemeData {

  enum Type {STEGER_WARMING = 0, GHIDAGLIA = 1} type;

  BoundarySchemeData();

  void setup(const char *, ClassAssigner * = 0);
};

//------------------------------------------------------------------------------

struct SchemesData {

  SchemeData ns;
  SchemeData tm;
  SchemeData tcm;
  SchemeData ls;
  SchemeFixData fixes;
  BoundarySchemeData bc;

  SchemesData();

  void setup(const char *, ClassAssigner * = 0);
};

//------------------------------------------------------------------------------

struct ExplicitData {

  enum Type {RUNGE_KUTTA_4 = 0, RUNGE_KUTTA_2 = 1, FORWARD_EULER = 2} type;

  ExplicitData();

  void setup(const char *, ClassAssigner * = 0);
};

//------------------------------------------------------------------------------

struct PcData {

  enum Type {IDENTITY = 0, JACOBI = 1, AS = 2, RAS = 3, ASH = 4, AAS = 5} type;
  enum Renumbering {NATURAL = 0, RCM = 1, SLOAN = 2} renumbering;

  int fill;

  PcData();

  void setup(const char *, ClassAssigner * = 0);
};

//------------------------------------------------------------------------------

struct KspData {

  enum Type {RICHARDSON = 0, CG = 1, GMRES = 2, GCR = 3, AUGMENTED_GMRES = 4} type;
  enum EpsFormula {CONSTANT = 0, EISENSTADT = 1} epsFormula;
  enum CheckFinalRes {NO = 0, YES = 1} checkFinalRes;
  // This flag enables the preconditioners to be constructed using either the
  // exact (H2) or approximate (H1) jacobian when the exact matrix-vector
  // product is specified. Prior to 4/24/2019 H1 was always used.
  // Note: as of 06/2019, we also support H2 also for the finite difference matrix-vector product.
  enum KspPrecJacobian {H1_PREC = 0, H2_PREC = 1, AUTO = 2} pcjacobian;
  enum MultipleLhs {MULTIPLE_LHS_OFF = 0, MULTIPLE_LHS_ON = 1} multipleLhs;

  int maxIts;
  int numVectors;
  int deflationSize;
  int typePrec;
  double eps;
  double absoluteEps;

  const char *output;
  const char *sparsematrix;

  PcData pc;

  KspData();

  void setup(const char *, ClassAssigner * = 0);
};

//------------------------------------------------------------------------------

struct KspFluidData {

  KspData ns;
  KspData tm;
  KspData tcm;
  KspData lsi;

  KspFluidData();

  void setup(const char *, ClassAssigner * = 0);
};

//------------------------------------------------------------------------------

struct LineSearchData {

  enum Type {NONE = 0, BACKTRACKING = 1} type;
  int maxIts;
  double rho;
  double c1;

  LineSearchData();

  void setup(const char *, ClassAssigner * = 0);
};

//------------------------------------------------------------------------------

template<class KspDataType>
struct NewtonData {

  enum FailSafe {NO = 0, YES = 1, ALWAYS = 2} failsafe;
  int maxIts;
  int fsMaxIts; // maximum number of failsafe iterations (default 5)
  int fsMinIts; // minimum number of failsafe iterations (default 0)
                // this can be used to to do some failsafe iterations before reducing the cfl and
                // repeating the entire time-step when CheckSolution is active
  double eps;
  double epsAbsRes, epsAbsInc;
  KspDataType ksp;
  LineSearchData lineSearch;
  const char *output;

  NewtonData();

  void setup(const char *, ClassAssigner * = 0);
};

//------------------------------------------------------------------------------

struct ImplicitData {

  enum Type {BACKWARD_EULER = 0, THREE_POINT_BDF = 1, FOUR_POINT_BDF = 2, RUNGE_KUTTA_2 = 3, RUNGE_KUTTA_3 = 4, SPATIAL_ONLY = 5} type;
  enum Startup {REGULAR = 0, MODIFIED = 1} startup;
  enum ModelCoupling {WEAK = 0, STRONG = 1} tmcoupling, tcmcoupling;
  enum Mvp {FD = 0, H1 = 1, H2 = 2} mvp;
  enum FiniteDifferenceOrder {FIRST_ORDER = 1, SECOND_ORDER = 2} fdOrder;
  NewtonData<KspFluidData> newton;

  // This flag was originally not visible from the input file but now (as of 12/21/2018) can be specified as this can be useful for debugging.
  // It governs the computation of the Jacobian of the flux function, a component of the 'H' matrix (from the MatrixVectorProduct).
  enum FluxFcnJacobian {FINITE_DIFFERENCE = 0, APPROXIMATE = 1, EXACT = 2, AUTO = 3} ffjacobian;

  // This flag can be used to activate precomputation of the exact jacobian in sparse matrix format which is then used to compute the exact
  // matrix vector-product and the H2 preconditioner (if specified).
  enum ExactImplementation {EXACT_IMPL_OLD = 0, EXACT_IMPL_NEW = 1} exactimpl;

  // This flag controls the time discretization at periodic surfaces, where the
  // reconstructed boundary states can be discretized fully implicitly leading
  // to a defect Jacobian, or left explicit so that the flux Jacobian remains exact
  enum PeriodicTreatment {IMPLICIT = 0, EXPLICIT = 1} periodic;

  double eps0;

  ImplicitData();

  void setup(const char *, ClassAssigner * = 0);
};

//------------------------------------------------------------------------------

struct CFLData {

  enum Strategy {DEFAULT = -1, RESIDUAL = 0, DIRECTION = 1, DFT = 2, HYBRID = 3, FIXEDUNSTEADY = 4, OLD = 5} strategy;
  enum BackwardCompat {OFF = 0, ON = 1} backward_compat;

  // global cfl parameters
  double cfl0;
  double cflCoef1;
  double cflCoef2;
  double cflMax;
  double cflMin;
  double dualtimecfl;

  // residual based parameters
  double ser;

  // direction based parameters
  double angle_growth;
  double angle_zero;

  // dft based parameters
  int dft_history;
  int dft_freqcutoff;
  double dft_growth;

  const char *output;

  CFLData();

  void setup(const char *, ClassAssigner * = 0);
};

//------------------------------------------------------------------------------

struct TsData {

  enum Type {EXPLICIT = 0, IMPLICIT = 1} type;
  enum TypeTimeStep {AUTO = 0, LOCAL = 1, GLOBAL = 2} typeTimeStep;
  enum Clipping {NONE = 0, ABS_VALUE = 1, FREESTREAM = 2, CUTOFF = 3} typeClipping;
  enum TimeStepCalculation {CFL = 0, ERRORESTIMATION = 1} timeStepCalculation;
  enum TimeStepSubcycle {TIMESTEPSUBCYCLE_OFF = 0, TIMESTEPSUBCYCLE_ON = 1} timeStepSubcycle;
  enum DualTimeStepping {OFF = 0, ON = 1} dualtimestepping;

  enum Prec {NO_PREC = 0, PREC = 1} prec;
  enum Form {DESCRIPTOR = 1, NONDESCRIPTOR = 0, HYBRID = 2} form;
  double viscousCst;
  double embeddedCfl, embeddedCflRef;

  int maxIts;
  double eps;
  double epsabs;
  double timestep;
  double timestepinitial;
  double maxTime;

  int residual;
  double errorTol;

  // Kept for back compatibility
  double cfl0;
  double cflCoef1;
  double cflCoef2;
  double cflMax;
  double cflMin;
  double ser;
  double dualtimecfl;

  int checksol;
  int checkvelocity;
  int checkpressure;
  int checkdensity;
  int checknutilde;
  int checkriemann;
  int checklinsolve;
  int checknewton;
  int deltapressurethreshold;
  int deltadensitythreshold;

  double programmedBurnShockSensor;
  double rapidPressureThreshold;
  double rapidDensityThreshold;

  const char *output;

  int minRestartIts;
  int minIts;

  ExplicitData expl;
  ImplicitData implicit;
  CFLData cfl;

  TsData();

  void setup(const char *, ClassAssigner * = 0);
};

//------------------------------------------------------------------------------

struct DGCLData {

  enum Normals {AUTO = 0, IMPLICIT_FIRST_ORDER_GCL = 1, IMPLICIT_SECOND_ORDER_GCL = 2,
                IMPLICIT_FIRST_ORDER_EZGCL = 3, IMPLICIT_SECOND_ORDER_EZGCL = 4, IMPLICIT_THIRD_ORDER_EZGCL = 5,
                IMPLICIT_CURRENT_CFG = 6, IMPLICIT_LATEST_CFG = 7, EXPLICIT_RK2 = 8
               } normals;
  enum Velocities {AUTO_VEL = 0, IMPLICIT_BACKWARD_EULER_VEL = 1, IMPLICIT_THREE_POINT_BDF_VEL = 2,
                   IMPLICIT_IMPOSED_VEL = 3, IMPLICIT_IMPOSED_BACKWARD_EULER_VEL = 4,
                   IMPLICIT_IMPOSED_THREE_POINT_BDF_VEL = 5, IMPLICIT_ZERO = 6, EXPLICIT_RK2_VEL = 7
                  } velocities;

  DGCLData();

  void setup(const char *, ClassAssigner * = 0);
};

//------------------------------------------------------------------------------

struct SensitivityAnalysisData {

  enum Method {DIRECT = 0, ADJOINT = 1} method;
  enum SensitivityComputation {ANALYTICAL = 0, SEMIANALYTICAL = 1, FINITEDIFFERENCE = 2} scFlag;
  enum Mvp {H2 = 0, FD = 1} mvp;
  enum Compatible3D {OFF_COMPATIBLE3D = 0, ON_COMPATIBLE3D = 1} comp3d;
  enum AngleRadians {OFF_ANGLERAD = 0, ON_ANGLERAD = 1} angleRad;
  enum SensitivityFSI {OFF_SENSITIVITYFSI  = 0, ON_SENSITIVITYFSI  = 1} sensFSI;
  enum SensitivityMesh {OFF_SENSITIVITYMESH = 0, ON_SENSITIVITYMESH = 1} sensMesh;
  enum SensitivityMach {OFF_SENSITIVITYMACH = 0, ON_SENSITIVITYMACH = 1} sensMach;
  enum SensitivityAOA {OFF_SENSITIVITYALPHA = 0, ON_SENSITIVITYALPHA = 1} sensAlpha;
  enum SensitivityYAW {OFF_SENSITIVITYBETA = 0, ON_SENSITIVITYBETA = 1} sensBeta;
  enum SensitivityLiftx {OFF_SENSITIVITYLIFTX = 0, ON_SENSITIVITYLIFTX = 1} sensLiftx;
  enum SensitivityLifty {OFF_SENSITIVITYLIFTY = 0, ON_SENSITIVITYLIFTY = 1} sensLifty;
  enum SensitivityLiftz {OFF_SENSITIVITYLIFTZ = 0, ON_SENSITIVITYLIFTZ = 1} sensLiftz;
  enum AdaptiveEpsFSI {OFF_ADAPTIVEEPSFSI = 0, ON_ADAPTIVEEPSFSI = 1} adaptiveEpsFSI;
  enum Debug {DEBUG_OFF = 0, DEBUG_ON = 1} debug;

  double machref;
  double alpharef;
  double betaref;
  double eps;

  const char *sensoutput;

  bool sparseFlag;
  int numShapeParameters;
  int maxOuterIts;

  KspData ksp;

  // toggle using structural/control surface modes as mesh shape parameters (i.e. mesh deformation already computed)
  enum UseModalInput {OFF_MODALINPUT = 0, ON_MODALINPUT = 1} useModalInput;

  SensitivityAnalysisData();

  void setup(const char *, ClassAssigner * = 0);
};

//------------------------------------------------------------------------------

struct MassData {
  // structure mass
  double mass;

  // moment of inertia tensor
  double Ixx;
  double Iyy;
  double Izz;
  double Ixy;
  double Ixz;
  double Iyz;

  // center of mass
  double XCM;
  double YCM;
  double ZCM;

  MassData();

  void setup(const char *, ClassAssigner * = 0);
};

//------------------------------------------------------------------------------

struct UzawaData {

  int maxIts;
  double eps;
  double beta;

  UzawaData();

  void setup(const char *, ClassAssigner * = 0);
};

//------------------------------------------------------------------------------

struct NeoHookeanData {

  double mu; // shear modulus

  NeoHookeanData();

  void setup(const char *, ClassAssigner * = 0);
};

//------------------------------------------------------------------------------

struct MooneyRivlinData {

  double mu1, mu2;

  MooneyRivlinData();

  void setup(const char *, ClassAssigner * = 0);
};

//------------------------------------------------------------------------------

struct ElementData {

  enum Type {QUASI_INCOMPRESSIBLE = 0} type;
  enum ConstitutiveModel {NEO_HOOKEAN = 0, MOONEY_RIVLIN = 1} constitutiveModel;
  int quadratureOrder;
  double epsilon;   // penalty parameter for quasi-incompressble element
  double threshold; // zero-pivot threshold used for computing Schur complement

  NeoHookeanData nhData;
  MooneyRivlinData mrData;

  ElementData();

  void setup(const char *, ClassAssigner * = 0);
};

//------------------------------------------------------------------------------

struct DefoMeshMotionData {

  enum Type {BASIC = 0, COROTATIONAL = 1, RIGID = 2} type;
  enum Element {LINEAR_FE = 0, NON_LINEAR_FE = 1, TORSIONAL_SPRINGS = 2, BALL_VERTEX = 3, NL_BALL_VERTEX = 4,
                NL_MATERIAL = 5} element;
  enum ConstraintMethod { PROJECTION = 0, ELIMINATION = 1} constraint_method;

  double volStiff;
  enum Mode {Recursive = 1, NonRecursive = 2} mode;
  int numIncrements, initStates;
  enum SlidingSurfaceTreatment {Default = 0, PrescribedAverage = 1} slidingSurfaceTreatment;

  NewtonData<KspData> newton;
  SymmetryData symmetry;
  MassData mass;
  UzawaData uzawa;
  ElementData elementData;

  DefoMeshMotionData();

  void setup(const char *, ClassAssigner * = 0);
};

//------------------------------------------------------------------------------

struct VelocityPoints {

  double time;
  double velocityX;
  double velocityY;
  double velocityZ;

  VelocityPoints();

  void setup(const char *, ClassAssigner * = 0);
};

//------------------------------------------------------------------------------

struct RigidMeshMotionData {

  static const int num = 10;

  enum Tag {MACH = 0, TIME = 1, VELOCITY = 2} tag;
  enum LawType {VELOCITYPOINTS = 0, CONSTANTACCELERATION = 1} lawtype;

  double vx;
  double vy;
  double vz;

  double ax;
  double ay;
  double az;

  VelocityPoints *vpts[num];
  VelocityPoints vpts1;
  VelocityPoints vpts2;
  VelocityPoints vpts3;
  VelocityPoints vpts4;
  VelocityPoints vpts5;
  VelocityPoints vpts6;
  VelocityPoints vpts7;
  VelocityPoints vpts8;
  VelocityPoints vpts9;
  VelocityPoints vpts10;

  double timestep;

  RigidMeshMotionData();

  void setup(const char *, ClassAssigner * = 0);
};

//------------------------------------------------------------------------------

struct AeroelasticData {

  enum Force {LAST = 0, AVERAGED = 1, LAST_KRIS = 2} force;
  double pressure;
  double displacementScaling;
  double forceScaling;
  double powerScaling;

  AeroelasticData();

  void setup(const char *, ClassAssigner * = 0);
};

//------------------------------------------------------------------------------

struct HeavingData {

  enum Domain {VOLUME = 0, SURFACE = 1} domain;

  double ax;
  double ay;
  double az;

  HeavingData();

  void setup(const char *, ClassAssigner * = 0);
};

//------------------------------------------------------------------------------

struct SpiralingData {

  enum Domain {VOLUME = 0, SURFACE = 1} domain;

  double xL;
  double x0;

  SpiralingData();

  void setup(const char *, ClassAssigner * = 0);
};

//------------------------------------------------------------------------------

struct PitchingData {

  enum Domain {VOLUME = 0, SURFACE = 1} domain;

  double alpha_in;
  double alpha_max;
  double alpha_slope;
  double x11;
  double y11;
  double z11;
  double x21;
  double y21;
  double z21;

  double beta_in;
  double beta_max;
  double beta_slope;
  double x12;
  double y12;
  double z12;
  double x22;
  double y22;
  double z22;

  PitchingData();

  void setup(const char *, ClassAssigner * = 0);
};

//------------------------------------------------------------------------------

struct DeformingData {
  enum Domain {VOLUME = 0, SURFACE = 1} domain;
  const char *positions;
  double amplification;
  DeformingData();

  void setup(const char *, ClassAssigner * = 0);
};

//------------------------------------------------------------------------------

struct PrescribedDeformingData {
  enum ExtrudeDirection {Y = 0, Z = 1} extrudedirection;
  double amplification;
  PrescribedDeformingData();

  void setup(const char *, ClassAssigner * = 0);
};

//------------------------------------------------------------------------------

struct UserDefinedData {
  enum Domain {VOLUME = 0, SURFACE = 1} domain;
  enum Type {FORCED = 0, CTRL = 1} type;
  const char *fileName;
  const char *routineName;

  UserDefinedData();

  void setup(const char *, ClassAssigner * = 0);
};

//------------------------------------------------------------------------------

struct RotationData {
  enum InfRadius {FALSE = 0, TRUE = 1} infRadius;
  double nx, ny, nz;
  double x0, y0, z0;
  double omega;

  RotationData();

  Assigner *getAssigner();
  void setup(const char *, ClassAssigner * = 0);
};

//------------------------------------------------------------------------------

struct Velocity {
  ObjectMap<RotationData> rotationMap;

  void setup(const char *, ClassAssigner * = 0);
};

//------------------------------------------------------------------------------

struct ForcedData {
  enum Type {HEAVING = 0, PITCHING = 1, VELOCITY = 2, DEFORMING = 3, PRESCRIBEDDEFORMING = 4,
             ACOUSTICBEAM = 5, SPIRALING = 6, ACOUSTICVISCOUSBEAM = 7, USERDEFINED = 8,
             HEAVINGPITCHING = 9} type;
  enum Smoothing {SMOOTHING_OFF = 0, SMOOTHING_ON = 1} smoothing;
  double frequency;
  double phase_shift;
  double smoothing_factor;
  double timestep;
  double tsoffset;

  HeavingData hv;
  SpiralingData sp;
  PitchingData pt;
  Velocity vel;
  DeformingData df;
  PrescribedDeformingData pd;
  UserDefinedData usd;

  ForcedData();

  void setup(const char *, ClassAssigner * = 0);
  bool isRigid() { return (type == HEAVING || type == PITCHING || type == VELOCITY || type == SPIRALING || type == HEAVINGPITCHING); }
};

//------------------------------------------------------------------------------

struct SurfaceData {
  enum ComputeForces {UNSPECIFIED = -1, FALSE = 0, TRUE = 1} computeForces;
  enum ComputeBulkQuantities {UNSPECIFIED_BQ = -1, FALSE_BQ = 0, TRUE_BQ = 1} computeBulkQuantities;
  enum ForceResults {NO = 0, YES = 1} forceResults;
  enum BulkQuantitiesResults {NO_BQ = 0, YES_BQ = 1} bulkQuantitiesResults;
  enum Type {DEFAULT = 0, ADIABATIC = 1, ISOTHERMAL = 2, PERIODIC = 3} type;

  enum ComputeHeatPower {UNSPECIFIED_HF = -1, FALSE_HF = 0, TRUE_HF = 1} computeHeatFluxes;
  enum HeatFluxResults {NO_HF = 0, YES_HF = 1} heatFluxResults;
  // the HF (Heat Flux) index ensures that there is no confusion with the force related data.
  double nx, ny, nz;
  double velocity;
  double temp;
  int sBit;
  int rotationID;
  int forceID;
  int bcID;
  int slaveID;  // for periodic BC
  double px, py, pz; // for periodic BC (periodic offset)
  int postprocess_sides;

  SurfaceData();

  Assigner *getAssigner();
  void setBit(int b) {
    sBit = b;
  }
};

//------------------------------------------------------------------------------

struct MovingLeastSquaresData {
  int dim;
  double supportRadius;

  void setup(const char *, ClassAssigner * = 0);

  MovingLeastSquaresData();
};

//------------------------------------------------------------------------------

struct EmbeddedFrameworkData {
  enum IntersectorName {PHYSBAM = 0, FRG = 1} intersectorName;
  enum StructureNormal {ELEMENT_BASED = 0, NODE_BASED = 1, PROXIMITY_BASED = 2} structNormal;
  enum StructureNormalAveraging {AREA = 0, ANGLE = 1} structNormalAvg;
  enum StructureNormalOrientation {EDGE = 0, FLUID_NORMAL = 1} structNormalOrient;
  enum EOSChange {NODAL_STATE = 0, RIEMANN_SOLUTION = 1} eosChange;
  enum ForceAlgorithm {CLOSEST_ELEMENT = 0, MLS = 1} forceAlg;
  enum RiemannNormal {STRUCTURE = 0, FLUID = 1} riemannNormal;
  enum PhaseChangeAlgorithm {AVERAGE = 0, LEAST_SQUARES = 1} phaseChangeAlg;
  enum UseDoublyIntersectedEdges {USE_DOUBLY_INTERSECTED_EDGES_NO = 0, USE_DOUBLY_INTERSECTED_EDGES_YES = 1} useDoublyIntersectedEdges;
  enum TypeHalfRiemannProblem {SURROGATE = 0, REAL = 1} typeHalfRiemannProblem;
  enum FirstLayerGradient {NIL = 0, COMPUTE = 1} firstLayerGradient;
  enum InterfaceLimiter {LIMITERNONE = 0, LIMITERALEX1 = 1} interfaceLimiter;
  enum Prec {NON_PRECONDITIONED = 0, PRECONDITIONED = 1, SAME_AS_PROBLEM = 3} prec;
  enum CrackingWithLevelSet {OFF = 0, ON = 1} crackingWithLevelset;
  enum ViscousInterfaceOrder {FIRST = 0, SECOND = 1} viscousInterfaceOrder;
  enum ViscousBoundaryCondition {WEAK = 0, STRONG = 1} viscousBoundaryCondition;
  enum SurrogateInterface {INTERSECTPOINT = 0, CLOSESTPOINT = 1} locationHalfRiemannProblem;
  enum DefinitionActiveInactive {NODE = 0, CONTROLVOLUME = 1} definitionActiveInactive;
  enum ThinStructure {NOTREATMENT = 0, PAD = 1} thinStructure;
  enum TurbulenceModelGhostTreatment {ZERO = 0, CONSTANT = 1, LINEAR = 2} turbulenceModelGhostTreatment;
  enum ComputeOffWallNode {NO = 0, YES = 1} computeOffWallNode;
  enum GhostPointPopulation {GLOBAL_GPP = 0, HALFSPACE = 1, QUADRANT = 2, LOCAL = 3, MIRRORING = 4} ghostPointPopulation;
  enum FillScope {BOUNDING_BOX = 0, ALWAYS = 1} fillScope;
  enum FillScopeType {GLOBAL_SCOPE = 0, LOCAL_SCOPE = 1} fillScopeType;
  enum RotatedRiemann {ROTATED_RIEMANN_OFF = 0, ROTATED_RIEMANN_ON = 1} rotatedRiemann;
  enum AveragedRiemann {AVERAGED_RIEMANN_OFF = 0, AVERAGED_RIEMANN_ON = 1} averagedRiemann;
  enum TangentFlux {INTERNAL = 0, BOUNDARY = 1} tangentFlux;
  enum TangentFluxType {FLUID_FLUID = 0, FLUID_STRUCTURE = 1} tangentFluxType;
  enum TangentialPorosity {TANGENTIAL_POROSITY_OFF = 0, TANGENTIAL_POROSITY_ON = 1} tangentialPorosity;
  enum RiemannAveraging {NO_RIEMANN_AVERAGING = 0, ALL = 1} riemannAveraging;
  enum RiemannAveragingStencil { CENTERED = 0, BIASED = 1} riemannAveragingStencil;
  enum RiemannAveragingWeight {UNIT = 0, SURFACEAREA = 1, EDGELENGTH = 2} riemannAveragingWeight;
  enum RiemannAveragingType {GLOBAL = 0, ELEMENTLOCAL = 1} riemannAveragingType;
  enum UpdatePolicy {ALWAYSUPDATE = 0, ERRORCONTROLLED = 1} updatePolicy;
  enum Wetted {WETTED = 0, UNWETTED = 1} wetted;
  enum UpdateRTreeFrequency {SUBCYCLINGEND = 0, SUBCYCLINGALL = 1} updateRTreeFrequency;
  enum NewtonInitialGuess {NEWTON_INITIAL_GUESS_OFF = 0, NEWTON_INITIAL_GUESS_ON = 1} newtonInitialGuess;
  enum WallFunctionNormal {LEGACY = 0, CLOSEST_POINT = 1} wallFunctionNormal;
  enum LocalGhostPopulationWeight {NON_DIMENSIONAL = 0, DIMENSIONAL = 1} localGhostPopulationWeight;
  enum ActiveNodesInitialization {FLOODFILL = 0, INTERSECTIONS = 1} activeNodesInitialization;
  int nLevelset;
  int qOrder; // order of quadrature rule used for EMBEDDED_SURFACE forceAlg
  int bbSize;
  int verbose;
  int updateSweptNodesNm;
  double bbSizeHybrid;
  double updateThreshold;
  double stabil_alpha;
  double contactThreshold;
  double newtonThreshold;
  double interfaceThickness;
  double ghostPointThreshold;
  double eps; // used to construct bounding box for SubDomain RTrees, default value is 10*std::numeric_limits<double>::epsilon()
  double ghostPointMirroringMVPEpsilon;
  double doublyIntersectedEdgeThreshold;

  MovingLeastSquaresData mls;

  EmbeddedInitialConditionsData embedIC;

  EmbeddedFrameworkData();

  void setup(const char *);
};

//------------------------------------------------------------------------------

struct EmbeddedALEFrameworkData {
  enum InitializeMeshVelocity {INITIALIZE_MESH_VELOCITY_OFF = 0, INITIALIZE_MESH_VELOCITY_ON = 1} initializeMeshVelocity;
  int masterSurfaceID;

  EmbeddedALEFrameworkData();

  void setup(const char *);
};

//------------------------------------------------------------------------------

struct HessianCriterionData {
  enum HessianConstruction {LEAST_SQUARES = 0} hessian_construction;
  enum SensorType {VELOCITY = 0, PRESSURE = 1, DENSITY = 2, VORTICITY = 3, LEVELSET = 4,
                   NU_TILDE = 5, MACH = 6, WALL_DISTANCE = 7, NUM_SENSOR_TYPES = 8} sensor_type; // note: NUM_SENSOR_TYPES must always be the last entry
  double threshold, coarsening_threshold;
  double p;

  HessianCriterionData();

  void setup(const char *, ClassAssigner * = 0);
};

//------------------------------------------------------------------------------

struct ValueCriterionData {
  enum HessianCriterionData::SensorType sensor_type;
  enum ThresholdType {UPPER_BOUND = 0, LOWER_BOUND = 1, CONTOUR = 2} threshold_type;
  enum ValueType {AVERAGE = 0, MINIMUM = 1, MAXIMUM = 2} value_type;
  double threshold, coarsening_threshold, contour_value;

  ValueCriterionData();

  void setup(const char *, ClassAssigner * = 0);
};

//------------------------------------------------------------------------------

struct SourceData {
  enum Type {POINT = 0, LINE = 1, PLANE = 2, ELLIPSOID = 3, TRIANGLE_SET = 4} type;
  enum CorotatingFrame {COROTATING_FRAME_OFF = 0, COROTATING_FRAME_ON = 1} corotating_frame;
  enum Predictor {PREDICTOR_OFF = 0, PREDICTOR_ON = 1} predictor;
  int predictor_level;

  PointData pointData;
  LineData lineData;
  PlaneData planeData;
  EllipsoidData ellipsoidData;
  TriangleSetData triangleSetData;

  SourceData();

  Assigner * getAssigner();
  void setup(const char *, ClassAssigner * = 0);
};

//------------------------------------------------------------------------------

struct GaussRuleData {
  int num_gauss_points;

  GaussRuleData();

  void setup(const char *, ClassAssigner * = 0);
};

//------------------------------------------------------------------------------

struct AdaptiveSimpsonsRuleData {
  int max_rec_depth;
  double epsilon;

  AdaptiveSimpsonsRuleData();

  void setup(const char *, ClassAssigner * = 0);
};

//------------------------------------------------------------------------------

struct PrincipalDirectionData {
  enum HessianCriterionData::SensorType sensor_type;

  PrincipalDirectionData();

  void setup(const char *, ClassAssigner * = 0);
};

//------------------------------------------------------------------------------

struct SecondDirectionData {

  enum Type {X = 0, Y = 1, Z = 2} type;

  SecondDirectionData();

  void setup(const char *, ClassAssigner * = 0);
};

//------------------------------------------------------------------------------

struct PreferredDirectionData {
  ObjectMap<SourceData> sources;
  enum Type {WALL_DISTANCE_GRADIENT = 0, SOURCE = 1, PRINCIPAL_DIRECTION = 2} type;
  enum UseSecondDirection {USE_SECOND_DIRECTION_OFF = 0, USE_SECOND_DIRECTION_ON = 1} use_second_direction;
  enum Discrete {DISCRETE_OFF = 0, DISCRETE_ON = 1} discrete;
                                            // When the discrete option is specified, the metric tensors at quadrature and bisection points
                                            // are computed by interpolation from the edge ends. When the discrete option is not specified
                                            // the metric tensors are always computed exactly unless (a) type = WALL_DISTANCE_GRADIENT,
                                            // or (b) stability_criterion = STABLE_ON and the stability criterion is violated.
  enum Stable {STABLE_OFF = 0, STABLE_ON = 1} stability_criterion;
  enum GeneralizedMidpoint {GENERALIZED_MIDPOINT_OFF = 0, GENERALIZED_MIDPOINT_ON = 1} generalized_midpoint;
  enum EdgeLengthIntegral {QUADRATURE = 0, ANALYTICAL = 1} edge_length_integral;
  enum QuadratureRule {TRAPEZOIDAL_RULE = 0, GAUSS_RULE = 1, SIMPSONS_RULE = 2, ADAPTIVE_SIMPSONS_RULE = 3} quadrature_rule;
  enum AnalyticalLaw {GEOMETRIC_LAW = 0, ARITHMETIC_LAW_H = 1, ARITHMETIC_LAW_LAMBDA = 2, ARITHMETIC_LAW_H_INVERSE = 3} analytical_law;
  enum DiscontinuousIntegrand {DISCONTINUOUS_INTEGRAND_OFF = 0, DISCONTINUOUS_INTEGRAND_ON = 1} discontinuous_integrand;
  enum SpatialFilter {NONE = 0, ARITHMETIC_MEAN = 1, GEOMETRIC_MEAN = 2, QUADRATIC_MEAN = 3, POWER_MEAN = 4, GAUSSIAN = 5,
                      LOG_EUCLIDEAN_GAUSSIAN = 6} spatial_filter;
  enum MetricIntersection {METRIC_INTERSECTION_OFF = 0, METRIC_INTERSECTION_ON = 1} metric_intersection;
  enum MetricInterpolation {LINEAR = 0, LOG_EUCLIDEAN = 1, SIMULTANEOUS_REDUCTION = 2} metric_interpolation, tz_metric_interpolation;
  enum SimultaneousReductionScheme {LINEAR_LAMBDA = 0, LINEAR_H_INVERSE = 1, LINEAR_H = 2, GEOMETRIC = 3} simultaneous_reduction_scheme,
                                                                                                          tz_simultaneous_reduction_scheme;
  double scaleFactor;
  double cutoff;
  double tol1, tol2;
  double spatial_filter_width;
  double power_mean_exponent;
  double metric_intersection_eps;
  double grid_size;
  double transition_zone_d, transition_zone_D;
  int maxit;

  GaussRuleData gaussRule;
  AdaptiveSimpsonsRuleData adaptiveSimpsonsRule;
  PrincipalDirectionData principalDirectionData;
  SecondDirectionData secondDirectionData;

  PreferredDirectionData();

  void setup(const char *, ClassAssigner * = 0);

  bool source_type(SourceData::Type t) const {
    return std::any_of(sources.dataMap.begin(), sources.dataMap.end(), [&](const std::pair<int, SourceData*>& p) {return p.second->type == t;});
  }
  bool corotating_frame() const {
    return (type == SOURCE && std::any_of(sources.dataMap.begin(), sources.dataMap.end(), [&](const std::pair<int, SourceData*>& p)
           {return p.second->corotating_frame == SourceData::COROTATING_FRAME_ON;}));
  }
  bool aabb_tree() const {
    return (type == SOURCE && std::any_of(sources.dataMap.begin(), sources.dataMap.end(), [&](const std::pair<int, SourceData*>& p)
           {return p.second->type == SourceData::TRIANGLE_SET;}));
  }
};

//------------------------------------------------------------------------------

struct BoundaryCriterionData {
  enum MinimumEdgeLengthTreatment {CLIPPED = 0, SMOOTHED = 1} min_edge_length_treatment;
  enum ActivateFirstLayer {OFF = 0, ON = 1} activate_first_layer;
  enum InnerZonePreRef {AUTO = 0, EDGE_LENGTH = 1, DELTA_PLUS = 2} inner_zone_pre_ref;
  double outerZone_d; // distance from wall to inner boundary of outer zone
  double outerZone_H; // maximum edge length in outer zone (not including outerZone_multiplier)
  double innerZone_D; // distance from wall to outer boundary of inner zone
  double innerZone_H; // maximum edge length in inner zone
  double innerZone_yplus;
  double outerZone_r; // ratio of outerZone_d to innerZone_H (only used when value is non-zero)
  double innerZone_R; // ratio of innerZone_D to innerZone_H (only used when value is non-zero)
  double hbyH; // ratio of minimum to maximum edge length in inner zone and sub-zones
  double outerZone_multiplier; // maximum edge length in outer zone: outerZone_H* = outerZone_multiplier*outerZone_H
  double coarsening_zone_scale_factor; // scaling factor applied to zone boundaries for evaluation of coarsening criterion
  double first_layer_activation_threshold; // only activate occluded first layer node if its distance to the wall is less than this threshold

  BoundaryCriterionData();

  void setup(const char *, ClassAssigner * = 0);

  double logr;
  std::vector<double> Hz, hz;
  void computeZoneParameters(double);
  int assignZone(double) const;
};

//------------------------------------------------------------------------------

inline
int BoundaryCriterionData::assignZone(double d2wmin) const {
  const int m = Hz.size();                        // number of zones
  const double& D_i = innerZone_D;                // distance from wall to outer boundary of inner zone
  const double d_o = (m > 2) ? outerZone_d : D_i; // distance from wall to inner boundary of outer zone
  if(d2wmin < D_i) return 0;
  else if(d2wmin >= d_o) return m-1;
  else {
    return std::max(0, std::min(m-1, (d2wmin > 1e-12) ? int(std::ceil(std::log(d2wmin/D_i)/logr)) : 0));
  }
}

//------------------------------------------------------------------------------

struct APrioriCriterionData {
  // local minimum edge length at point P[x,y,z] is defined as min_edge_length + abs(gx*(x-x0)) + abs(gy*(y-y0)) + abs(gz*(z-z0))
  double gx, gy, gz; // x, y and z components of the cartesian gradient
  double x0, y0, z0; // x, y and z components of the reference point

  APrioriCriterionData();

  void setup(const char *, ClassAssigner * = 0);
};

//------------------------------------------------------------------------------

struct GeometricCriterionData {
  ObjectMap<SourceData> sources;
  enum Progression {CONSTANT = 0, LINEAR = 1, QUADRATIC = 2, INTERSECTION_ONLY = 3} progression;
  enum DistanceMethod {EXACT = 0, APPROXIMATE = 1, HYBRID = 2, FAST = 3} distance_method;
  double a0, a1, a2, a3; // a0 is the mesh size on the source, a1 and a2 represent the linear and quadratic variations of
                         // mesh size with distance from source; i.e. h = (d<d0) ? a0 : a0 + a1*(d-d0) + a2*(d-d0)^2
  double d0, d1;

  GeometricCriterionData();

  void setup(const char *, ClassAssigner * = 0);

  bool corotating_frame() const {
    return std::any_of(sources.dataMap.begin(), sources.dataMap.end(), [&](const std::pair<int, SourceData*>& p)
           {return p.second->corotating_frame == SourceData::COROTATING_FRAME_ON;});
  }
  bool aabb_tree() const {
    return std::any_of(sources.dataMap.begin(), sources.dataMap.end(), [&](const std::pair<int, SourceData*>& p)
           {return p.second->type == SourceData::TRIANGLE_SET;});
  }
};

//------------------------------------------------------------------------------

struct FixesCriterionData {
  enum DoublyIntersectedEdgeFix {OFF = 0, ON = 1} doubly_intersected_edge_fix;
  int faceID;
  double doubly_intersected_edge_threshold;

  FixesCriterionData();

  void setup(const char *, ClassAssigner * = 0);
};

//------------------------------------------------------------------------------

struct IntersectionCriterionData {
  enum LoftingInterface {REAL = 0, SURROGATE = 1, HYBRID = 2} lofting_interface;
  int faceID;
  double y;
  double yplus;
  double y_min_max_ratio;
  double yplus_min_max_ratio;

  IntersectionCriterionData();

  void setup(const char *, ClassAssigner * = 0);
};

//------------------------------------------------------------------------------

struct RefinementDomainData {
  enum Type {NONE = 0, SPHERE = 1, PRISM = 2, CYLINDER = 3, CAPSULE = 4, CONE = 5, STAR = 6, WEDGE = 7, ELLIPSOID = 8,
             TRIANGULAR_PRISM = 9} type;
  enum Side {INTERIOR = 0, EXTERIOR = 1} side;

  SphereData sphereData;
  PrismData prismData;
  CylinderData cylinderData;
  StarData starData;
  CapsuleData capsuleData;
  ConeData coneData;
  WedgeData wedgeData;
  EllipsoidData ellipsoidData;
  TriangularPrismData triangularPrismData;

  RefinementDomainData();

  void setup(const char *, ClassAssigner * = 0);

  bool inside(Vec3D& v, double eps) const {
    bool result;
    switch(type) {
      default :
      case NONE : return true;
      case SPHERE : result = sphereData.inside(v[0], v[1], v[2], eps); break;
      case PRISM :  result = prismData.inside(v[0], v[1], v[2], eps); break;
      case CYLINDER : result = cylinderData.inside(v[0], v[1], v[2], eps); break;
      case CAPSULE : result = capsuleData.inside(v[0], v[1], v[2], eps); break;
      case CONE : result = coneData.inside(v[0], v[1], v[2], eps); break;
      case STAR : result = starData.inside(v[0], v[1], v[2], eps); break;
      case WEDGE : result = wedgeData.inside(v[0], v[1], v[2], eps); break;
      case ELLIPSOID : result = ellipsoidData.inside(v[0], v[1], v[2], eps); break;
      case TRIANGULAR_PRISM : result = triangularPrismData.inside(v[0], v[1], v[2], eps); break;
    }
    return (side == INTERIOR) ? result : !result;
  }
};

//------------------------------------------------------------------------------

struct SelectionCriterionData {
  enum Type {NONE = 0, HESSIAN = 1, VALUE = 2, BOUNDARY = 3, A_PRIORI = 4, FIXES = 5, GAP = 6, INTERSECTION = 7, NODELIST = 8, GEOMETRIC = 9} type;
  enum LocalMetric {EUCLIDEAN = 0, PREFERRED_DIRECTION = 1, GLOBAL = 2} local_metric;
  enum {OFF = 0, ON = 1} pre_refine, pre_coarsen,           // used to activate an adaptive mesh refinement step at t = 0
                         refine_inactive, coarsen_inactive; // used to disable adaptation for edges whose vertices are both inactive
  double min_edge_length; // minimium length of an edge created by refinement with this criterion
                          // i.e. edge can only be selected if its length is >= 2*min_edge_length
  double max_edge_length; // maximum length of an edge created by coarsening with this criterion
                          // i.e. edge can only be selected if its length is <= 0.5*max_edge_length
  double coarsening_domain_eps; // defines the characteristic size of the coarsening domain to be eps
                                // greater than that of the refinement domain
  int num_levels; // maximum number of levels of refinement per step
  int num_coarsening_levels; // if >= 0 this is the maximum number of levels of coarsening per step
                             // if < 0 the maximum number of levels of coarsening per step is num_levels
  int start_level;
  int start_coarsening_level;

  HessianCriterionData hessianData;
  ValueCriterionData valueData;
  BoundaryCriterionData boundaryData;
  APrioriCriterionData aprioriData;
  FixesCriterionData fixesData;
  BoundaryCriterionData gapData;
  IntersectionCriterionData intersectionData;
  GeometricCriterionData geometricData;

  RefinementDomainData refinementDomain;
  PreferredDirectionData preferredDirectionData;

  SelectionCriterionData();

  Assigner *getAssigner();
  void setup(const char *, ClassAssigner * = 0);
  int getNumCoarseningLevels() { return (num_coarsening_levels < 0) ? num_levels : num_coarsening_levels; }
  int getStartCoarseningLevel() { return (start_coarsening_level < 0) ? start_level : start_coarsening_level; }
};

//------------------------------------------------------------------------------

struct AdaptiveMeshRefinementData {
  ObjectMap<SelectionCriterionData> selectionCriteria;
  enum Mode {DIMENSIONAL = 0, NONDIMENSIONAL = 1} mode;
  enum BisectionStrategy {ARBITRARY = 0, ISOTROPIC = 1, ANISOTROPIC = 2} bisection_strategy;
  enum Metric {EUCLIDEAN = 0, PREFERRED_DIRECTION = 1} metric;
  enum InitializeRefinementTag {INITIALIZE_REFINEMENT_TAG_OFF = 0, INITIALIZE_REFINEMENT_TAG_ON = 1} initialize_refinement_tag;
  enum RenumberingVertexSets {ONLY_TYPE_0 = 0, INITIAL_LONGEST_EDGE = 1, LEAST_ADJACENT_ELEMENTS = 2, RANDOM = 3} renumbering_vertex_sets;
  enum RenumberingVertexOrdering {NATURAL = 0, SRN = 1, SRN2 = 2} renumbering_vertex_ordering;
  enum RenumberingAnnouncedEdge {ANNOUNCE_LONGEST_EDGE = 0, ANNOUNCE_REFINEMENT_EDGE = 1} renumbering_announced_edge;
  enum CoarseningPolicy {PARTIAL = 0, COMPLETE = 1, ALTERNATING = 2} coarsening_policy;
  enum CoarseningCriterion {ALL = 0, ANY = 1} coarsening_criterion;
  enum InterpolationBasis {CURRENT_EDGE = 0, REFERENCE_CELL = 1, CURRENT_EDGE_PATCH = 2} interpolation_basis;
  enum SortEdges {GLOBAL_NODE = 0, EDGE_LENGTH = 1, SORT_EDGES_AUTO = 2} sort_edges;
  // When consistent_numbering = CONSISTENT_NUMBERING_ON, the same convention is used for both isotropic and arbitrary/anisotropic bisections to number the sons;
  // either s1 or s2 can replace the father in the element set. For isotropic with CONSISTENT_NUMBERING_OFF, s1 always replaces the father which is
  // the convention proposed in the Bartels reference). The consistent convention is required for isotropic only if you want to coarsen the isotropic refinement
  // in a subsequent simulation using arbitrary/anisotropic bisection (or vice versa).
  enum ConsistentNumbering {CONSISTENT_NUMBERING_OFF = 0, CONSISTENT_NUMBERING_ON = 1} consistent_numbering;
  enum UniformityTreatment {ADAPTIVE = 0, NON_ADAPTIVE = 1} uniformity_treatment;
  enum UniformityThresholdType {MIN_AVG = 0, MIN_MAX = 1, AVG_MAX = 2} uniformity_threshold_type;
  enum Coarsening {COARSENING_OFF = 0, COARSENING_ON = 1} coarsening;
  enum Check {CHECK_OFF = 0, CHECK_ON = 1, CHECK_AUTO = 2} check_elem_volume, check_aspect_ratio;
  enum CoarseningIntersection {CI_OFF = 0, CI_ON = 1} coarsening_intersection; // if on, then only coarsen elements that are marked to be coarsened by all criteria
  enum Balancing {BALANCING_OFF = 0, BALANCING_ON = 1} balancing;
  enum BalancingCriterion {REGULAR = 0, NODE_MAX_MIN_RATIO = 1, ELEM_MAX_MIN_RATIO = 2} balancing_criterion;
  enum SilenceErrors {SILENCE_ERRORS_OFF = 0, SILENCE_ERRORS_ON = 1} silence_errors;
  enum PredictorCorrector {OFF = 0, ON = 1} predictor_corrector, predictor_corrector_coarsening;
  enum FindStatus {UPDATE = 1, FLOOD_FILL = 2} find_status;
  enum PrecomputeHessian {REFINEMENT_LEVEL = 0, REFINEMENT_STEP = 1} precompute_hessian;
  enum NormalDirection {X = 0, Y = 1, Z = 2} normal_direction;
  double eps, eps0;
  double min_elem_volume;
  double max_aspect_ratio;
  double balancing_threshold; // if the value associate with certain balancing criteria exceeds this threshold
                              // then repartitiong will occur
  double itr_factor;      // This parameter describes the ratio of inter-processor communication time compared to data redistri-
                          // bution time. It should be set between 0.000001 and 1000000.0. If ITR is set high, a repartitioning
                          // with a low edge-cut will be computed. If it is set low, a repartitioning that requires little data
                          // redistribution will be computed. Good values for this parameter can be obtained by dividing inter-
                          // processor communication time by data redistribution time. Otherwise, a value of 1000.0 is recommended.
  double imbalance_tol;
  double uniformity_threshold; // if the ratio of the minimum to average number of elements after balancing is less than this
                               // threshold, the the balancing is repeated with a smaller imbalance_tol
  double initial_uniformity_threshold; // if the ratio of the minimum to average number of elements before balancing is less than
                                       // this threshold, then a non-adaptive balancing method is used
  double longest_edge_threshold, longest_edge_relative_threshold;
  double coarsening_domain_eps;
  double node_rtree_eps;
  int frequency, coarsening_frequency, complete_coarsening_frequency;
  int max_refinement_iter, max_refinement_subiter, max_coarsening_iter;
  int refine3d;
  int balancing_frequency;
  int balancing_weight;   // element weight for mesh repartitioning: set to 0 for unweighted partitioning or non-zero to specify
                          // weight of intersected edge (note weight of non-intersected active edge is 1, weight of inactive edge
                          // is 0, and element weight is sum of edge weights)
  int verbose;            // parameter controlling screen output related to refinement (0: suppress all screen output, 1: minimal
                          // screen output, 2: more screen output, etc...)
  int coarsening_verbose; // parameter controlling screen output related to coarsening
  int balancing_verbose;  // parameter controlling screen output related to balancing
  int num_corrector_its;
  int debug_balancing;    // use to force re-partitioning at end of each balancing period, regardless of whether refinement occurred
  int use_unsteady_strategy;
  int compress_global_numbers;
  int initial_refinement_tag_type;
  int truncation_threshold; // loosely enforced upper bound on the number of nodes per subdomain; the maximum number of edges that can
                            // be pre-selected for refinement in each subdomain per refinement level is equal to this threshold minus
                            // the current number of nodes in the subdomain
  int recompute_wall_distance;
  int recompute_wall_distance_num_levels;
  int recompute_preferred_direction_num_levels;
  int recompute_yplus;
  int renumbering_threshold; // constant threshold used when initial longest edge or least adjacent elements renumbering is specified
  int_t max_num_elem;
  bool onthefly_restart;  // used to distinguish between a regular restart and a continuation after balancing or correcting

  RefinementDomainData refinementDomain;
  PreferredDirectionData preferredDirectionData;
  LeastSquaresData leastSquaresData;

  AdaptiveMeshRefinementData();

  void setup(const char *, ClassAssigner * = 0);

  bool reinitialize_preferred_direction(int level) {
    return (metric == PREFERRED_DIRECTION) && ((level == 0) || ((level < recompute_preferred_direction_num_levels) &&
           (preferredDirectionData.discrete == PreferredDirectionData::DISCRETE_ON ||
            preferredDirectionData.stability_criterion == PreferredDirectionData::STABLE_ON ||
            preferredDirectionData.type == PreferredDirectionData::WALL_DISTANCE_GRADIENT)));
  }

  // helper functions to facilitate use of multiple selection criteria
  // 1. criterion returns true if the type of any of the selection criteria matches the parameter t
  bool criterion(SelectionCriterionData::Type t) const {
    return std::any_of(selectionCriteria.dataMap.begin(), selectionCriteria.dataMap.end(),
           [&](const std::pair<int, SelectionCriterionData*>& p) {return p.second->type == t;});
  }
  // 2. none returns true if no selection criteria are specified
  bool none() const {
    return selectionCriteria.dataMap.empty() ||
           std::all_of(selectionCriteria.dataMap.begin(), selectionCriteria.dataMap.end(),
           [&](const std::pair<int, SelectionCriterionData*>& p) {return p.second->type == SelectionCriterionData::NONE;});
  }
  // 3. num_levels returns the maximum number of levels of all the selection criteria
  int num_levels() const {
   return std::max_element(selectionCriteria.dataMap.begin(), selectionCriteria.dataMap.end(),
          [&](const std::pair<int, SelectionCriterionData*>& a, const std::pair<int, SelectionCriterionData*>& b)
          {return a.second->num_levels < b.second->num_levels;})->second->num_levels;
  }
  int num_coarsening_levels() const {
    return std::max_element(selectionCriteria.dataMap.begin(), selectionCriteria.dataMap.end(),
          [&](const std::pair<int, SelectionCriterionData*>& a, const std::pair<int, SelectionCriterionData*>& b)
          {return a.second->getNumCoarseningLevels() < b.second->getNumCoarseningLevels();})->second->getNumCoarseningLevels();
  }
  // 4. max_start_level returns the maximum start level of all the selection criteria
  int max_start_level() const {
   return std::max_element(selectionCriteria.dataMap.begin(), selectionCriteria.dataMap.end(),
          [&](const std::pair<int, SelectionCriterionData*>& a, const std::pair<int, SelectionCriterionData*>& b)
          {return a.second->start_level < b.second->start_level;})->second->start_level;
  }
  int max_start_coarsening_level() const {
    return std::max_element(selectionCriteria.dataMap.begin(), selectionCriteria.dataMap.end(),
          [&](const std::pair<int, SelectionCriterionData*>& a, const std::pair<int, SelectionCriterionData*>& b)
          {return a.second->getStartCoarseningLevel() < b.second->getStartCoarseningLevel();})->second->getStartCoarseningLevel();
  }
  // 5. pre_refine returns true if any of the selection criteria have pre-refinement activated
  bool pre_refine() const {
    return std::any_of(selectionCriteria.dataMap.begin(), selectionCriteria.dataMap.end(),
           [&](const std::pair<int, SelectionCriterionData*>& p) {return p.second->pre_refine == SelectionCriterionData::ON;});
  }
  bool pre_coarsen() const {
    return (coarsening == COARSENING_ON) && std::any_of(selectionCriteria.dataMap.begin(), selectionCriteria.dataMap.end(),
           [&](const std::pair<int, SelectionCriterionData*>& p) {return p.second->pre_coarsen == SelectionCriterionData::ON;});
  }
  // 6. sensor_type returns true if the sensor type of any of the hessian or value selection criteria matches the parameter s
  bool sensor_type(HessianCriterionData::SensorType s) const {
    return std::any_of(selectionCriteria.dataMap.begin(), selectionCriteria.dataMap.end(),
           [&](const std::pair<int, SelectionCriterionData*>& p) {return (p.second->type == SelectionCriterionData::HESSIAN
           && p.second->hessianData.sensor_type == s) || (p.second->type == SelectionCriterionData::VALUE && p.second->valueData.sensor_type == s);});
  }
  // 7. hessian_sensor_type returns true if the sensor type of any of the hessian selection criteria matches the parameter s
  bool hessian_sensor_type(HessianCriterionData::SensorType s) const {
    return std::any_of(selectionCriteria.dataMap.begin(), selectionCriteria.dataMap.end(),
           [&](const std::pair<int, SelectionCriterionData*>& p) {return p.second->type == SelectionCriterionData::HESSIAN
           && p.second->hessianData.sensor_type == s;});
  }
  // 8. preferred_direction returns true if the local metric of any of the selection criteria is preferred direction
  //    or if the global metric is preferred direction
  bool preferred_direction() const {
    return std::any_of(selectionCriteria.dataMap.begin(), selectionCriteria.dataMap.end(),
           [&](const std::pair<int, SelectionCriterionData*>& p) {return (p.second->local_metric == SelectionCriterionData::PREFERRED_DIRECTION);})
           || (metric == PREFERRED_DIRECTION);
  }
  // 9. preferred_direction_type returns true if the preferred direction type of any of the local metrics matches the parameter t
  //    or if the preferred direction type of the global metric matches the parameter t
  bool preferred_direction_type(PreferredDirectionData::Type t) const {
    return std::any_of(selectionCriteria.dataMap.begin(), selectionCriteria.dataMap.end(),
           [&](const std::pair<int, SelectionCriterionData*>& p) {return (p.second->local_metric == SelectionCriterionData::PREFERRED_DIRECTION
           && p.second->preferredDirectionData.type == t);}) || (metric == PREFERRED_DIRECTION && preferredDirectionData.type == t);
  }
  // 10. corotating_frame returns true if any source has an attached corotating frame
  bool corotating_frame() const {
    return (metric == PREFERRED_DIRECTION && preferredDirectionData.corotating_frame())
           || std::any_of(selectionCriteria.dataMap.begin(), selectionCriteria.dataMap.end(), [&](const std::pair<int, SelectionCriterionData*>& p)
           {return (p.second->local_metric == SelectionCriterionData::PREFERRED_DIRECTION && p.second->preferredDirectionData.corotating_frame());})
           || std::any_of(selectionCriteria.dataMap.begin(), selectionCriteria.dataMap.end(), [&](const std::pair<int, SelectionCriterionData*>& p)
           {return (p.second->type == SelectionCriterionData::GEOMETRIC && p.second->geometricData.corotating_frame());});
  }
  // 11. aabb_frame returns true if any metric or selection criterion has a source with an axis-aligned bounding box tree
  bool aabb_tree() const {
    return (metric == PREFERRED_DIRECTION && preferredDirectionData.aabb_tree())
           || std::any_of(selectionCriteria.dataMap.begin(), selectionCriteria.dataMap.end(), [&](const std::pair<int, SelectionCriterionData*>& p)
           {return (p.second->local_metric == SelectionCriterionData::PREFERRED_DIRECTION && p.second->preferredDirectionData.aabb_tree());})
           || std::any_of(selectionCriteria.dataMap.begin(), selectionCriteria.dataMap.end(), [&](const std::pair<int, SelectionCriterionData*>& p)
           {return (p.second->type == SelectionCriterionData::GEOMETRIC && p.second->geometricData.aabb_tree());});
  }
};

//------------------------------------------------------------------------------

struct ClusteringData {

  enum UseExistingClusters {USE_EXISTING_CLUSTERS_FALSE = 0, USE_EXISTING_CLUSTERS_TRUE = 1} useExistingClusters;

  double percentOverlap;
  int maxIter;
  int randSeed;
  int minClusterSize;

  ClusteringData();

  void setup(const char *, ClassAssigner * = 0);
};

//------------------------------------------------------------------------------

struct FeatureSelectionData {

  const char *whateverprefix;

  enum ComputeFeatures {COMPUTE_FEATURES_FALSE = 0, COMPUTE_FEATURES_TRUE = 1} computeFeatures;
  enum NormalizeSnapshots {NORMALIZE_SNAPHOTS_FALSE = 0, NORMALIZE_SNAPHOTS_TRUE = 1} normalizeSnapshots;

  enum BoundaryDistance {BOUNDARY_DISTANCE_FALSE = 0, BOUNDARY_DISTANCE_TRUE = 1} boundaryDistance;
  enum MinEdgeLength {MIN_EDGE_LENGTH_FALSE = 0, MIN_EDGE_LENGTH_TRUE = 1} minEdgeLength;
  enum MaxEdgeLength {MAX_EDGE_LENGTH_FALSE = 0, MAX_EDGE_LENGTH_TRUE = 1} maxEdgeLength;
  enum AvgEdgeLength {AVG_EDGE_LENGTH_FALSE = 0, AVG_EDGE_LENGTH_TRUE = 1} avgEdgeLength;
  enum Hessian {HESSIAN_FALSE = 0, HESSIAN_TRUE = 1} hessian;
  enum FFT {FFT_FALSE = 0, FFT_TRUE = 1} fft;
  enum Polynomial {POLYNOMIAL_FALSE = 0, POLYNOMIAL_TRUE = 1} polynomial;

  int waveNumbers;
  int polyOrder;

  FeatureSelectionData();

  void setup(const char *, ClassAssigner * = 0);
};

//------------------------------------------------------------------------------

struct AdaptiveRomData {

  const char *geometryprefix;
  const char *connectivity;
  const char *geometry;
  const char *decomposition;
  const char *d2wall;

  const char *innerproductfilename;
  const char *unbalancedprefix;
  const char *supermeshprefix;

  bool onlyAMR;

  enum ComputeAROM {COMPUTE_AROM_FALSE = 0, COMPUTE_AROM_TRUE = 1} computeAROM;
  enum ComputeUnbalanced {COMPUTE_UNBALANCED_FALSE = 0, COMPUTE_UNBALANCED_TRUE = 1} computeUnbalanced;
  enum ComputeInnerProduct {COMPUTE_INNER_PRODUCT_FALSE = 0, COMPUTE_INNER_PRODUCT_TRUE = 1} computeInnerProduct;
  enum RecomputeInnerProduct {RECOMPUTE_INNER_PRODUCT_FALSE = 0, RECOMPUTE_INNER_PRODUCT_TRUE = 1} recomputeInnerProduct;
  enum InterpolateSupermesh {INTERPOLATE_SUPERMESH_FALSE = 0, INTERPOLATE_SUPERMESH_TRUE = 1} interpolateSupermesh;
  enum SubmeshInnerProduct {SUBMESH_INNER_PRODUCT_FALSE = 0, SUBMESH_INNER_PRODUCT_TRUE = 1} submeshInnerProduct;
  enum SubtractRefStateBeforeInnerProduct {SUBTRACT_FALSE = 0, SUBTRACT_TRUE = 1} subtractRefStateBeforeInnerProduct;
  enum UseSupermesh {USE_SUPERMESH_FALSE = 0, USE_SUPERMESH_TRUE = 1} useSupermesh;
  enum ApproxType {VELOCITY = 0, PRESSURE = 1, DENSITY = 2, STATE = 3} approxType;
  enum BasisComputation {IPM = 0, SVD = 1, RBM = 2} basisComputation;
  enum SubmeshMethod {INITIAL = 0, SUBMESH = 1} submeshMethod;

  int numLoadedMeshes;
  int referenceMesh;
  int shiftSnaps;

  AdaptiveRomData();

  void setup(const char *, ClassAssigner * = 0);
  void applyAdaptiveGeometryPrefix();
};

//------------------------------------------------------------------------------

struct SnapshotsData {

  enum NormalizeSnaps {NORMALIZE_FALSE = 0, NORMALIZE_TRUE = 1} normalizeSnaps;
  enum SubtractRefState {REF_STATE_NONE = 0, REF_STATE_INPUT = 1, REF_STATE_CENTER = 2,
                         REF_STATE_INITIAL = 3, REF_STATE_AVERAGE = 4} subtractRefState;

  SnapshotsData();

  void setup(const char *, ClassAssigner * = 0);
};

//------------------------------------------------------------------------------

struct AlternatingLeastSquaresData {

  enum LinearSolver {JACOBI_SVD = 0, CHOLESKY = 1} linearSolver;
  enum OrthogonalizeBasis {ORTHOGONALIZE_FALSE = 0, ORTHOGONALIZE_TRUE = 1} orthogonalizeBasis;

  int maxIts;
  int numVectors;
  double threshold;
  double lambda, mu; // regularization parameters for U and V
  double eps;

  AlternatingLeastSquaresData();

  void setup(const char *, ClassAssigner * = 0);
};

//------------------------------------------------------------------------------

struct QuadraticManifoldOfflineData {
  enum ComputeQuadraticManifold {COMPUTE_QUADRATIC_FALSE = 0, COMPUTE_QUADRATIC_TRUE = 1} compute_quadratic;
  enum UseQuadraticManifold {USE_QUADRATIC_FALSE = 0, USE_QUADRATIC_TRUE = 1} use_quadratic;
  enum ComputeQuadraticManifoldSize {COMPUTE_QUADRATIC_SIZE_FALSE = 0, COMPUTE_QUADRATIC_SIZE_TRUE = 1} computeQuadSize;
  enum ComputeQuadraticSingValTolerance {COMPUTE_Q_EPSILON_FALSE = 0, COMPUTE_Q_EPSILON_TRUE = 1} computeQuadEpsilon;
  enum RegularizationCriterion {LCURVE = 0, GCV = 1} regCriterion;
  enum RegularizationScope {GLOBAL_ALL = 0, GLOBAL_CPU = 1, LOCAL = 2} regScope;
  enum QuadraticManifoldSolver {TSVD = 0, EIGEN_SVD_SOLVE = 1, TIKHONOV = 2} quadSolver;

  int quadSize;
  double singValToleranceDelta; // Used for determining the size of H in H(q x q) + Vq
  double quadSingValTolerance;
  double quadSingValSamplingFraction;
  double hdmSamplingFraction;

  QuadraticManifoldOfflineData();

  void setup(const char *, ClassAssigner * = 0);
};

//------------------------------------------------------------------------------

struct DataCompressionData {

  enum ComputePOD {COMPUTE_POD_FALSE = 0, COMPUTE_POD_TRUE = 1} computePOD;
  enum PODMethod {SCALAPACK_SVD = 0, R_SVD = 1} podMethod;
  enum EmbeddedCompressionMethod {AUTO = 0, POD_METHOD = 1, ALS_METHOD = 2} embeddedCompressionMethod;
  enum GenerateRowClusteredBasis {GENERATE_ROW_CLUSTERED_BASIS_FALSE = 0,
                                  GENERATE_ROW_CLUSTERED_BASIS_TRUE = 1} generateRowClusteredBasis;
  enum UseGeneralManifold {USE_GENERAL_MANIFOLD_FALSE = 0, USE_GENERAL_MANIFOLD_TRUE = 1} useGenManOffline;

  int maxBasisSize;
  int minBasisSize;
  double maxEnergyRetained;
  double singValTolerance;
  double genManReductionFactor;

  QuadraticManifoldOfflineData qm;
  AlternatingLeastSquaresData als;

  DataCompressionData();

  void setup(const char *, ClassAssigner * = 0);
};

//------------------------------------------------------------------------------

struct PODData {

  SnapshotsData snapshots;
  DataCompressionData dataCompression;

  PODData();

  void setup(const char *, ClassAssigner * = 0);
};

//------------------------------------------------------------------------------

struct RelativeProjectionErrorData {

  enum RelativeProjectionError {PROJ_ERROR_OFF = 0, PROJ_ERROR_ON = 1} relProjError;

  int minDimension;
  int maxDimension;
  double energy;
  const char *clustEnergies;

  RelativeProjectionErrorData();

  void setup(const char *, ClassAssigner * = 0);
};

//------------------------------------------------------------------------------

struct ROBConstructionData {

  enum ProjectInitialCondition {PROJECT_IC_FALSE = 0, PROJECT_IC_TRUE = 1} projectIC;
  enum PreprocessForProjections {PROJECTIONS_FALSE = 0, PROJECTIONS_TRUE = 1} preprocessForProjections;

  ClusteringData clustering;
  PODData state;
  PODData residual;
  RelativeProjectionErrorData relativeProjectionError;
  int numNeighbors;

  ROBConstructionData();

  void setup(const char *, ClassAssigner * = 0);
};

//------------------------------------------------------------------------------

struct GappyPODData {

  enum GreedyData {RESIDUAL_ROB = 0, STATE_ROB = 1} greedyData;
  enum GreedyLSSolver {QR_GREEDY = 0, NORMAL_EQUATIONS_GREEDY = 1} greedyLsSolver;
  enum ComputeGNAT {COMPUTE_GNAT_FALSE = 0, COMPUTE_GNAT_TRUE = 1} computeGNAT;
  enum PseudoInvLSSolver {QR_PINV = 0, NORMAL_EQUATIONS_PINV = 1} pseudoInvLsSolver;

  int maxDimGreedy;
  int minDimGreedy;
  double dimGreedyFactor;
  int maxSampleNodes;
  int minSampleNodes;
  double sampleNodesFactor;

  int minNumberInlet;
  double minFractionSurface;
  double minFractionTargetRegion;
  SchemeFixData targetRegions;  // use fix regions to specify target areas

  GappyPODData();

  void setup(const char *, ClassAssigner * = 0);
};

//------------------------------------------------------------------------------

struct SamplingWeightingData {

  enum SamplingAlgorithm {NNLS = 0, PFP = 1} algorithm;
  enum SamplingSolver {CG = 0, QR = 1} solver;
  enum DataType {RESIDUAL = 0, JACOBIAN = 1} dataType;
  enum UseScaling {SCALING_FALSE = 0, SCALING_TRUE = 1} useRowScaling, useColScaling;
  enum UseCentering {CENTERING_FALSE = 0, CENTERING_TRUE = 1} useCentering;
  enum UseBalancing {BALANCE_FALSE = 0, BALANCE_TRUE = 1} useBalancing;
  enum DecomposeBCs {DECOMPOSE_FALSE = 0, DECOMPOSE_TRUE = 1} decomposeBCs;
  enum DecomposeRowClusters {DECOMPOSE_ROW_FALSE = 0, DECOMPOSE_ROW_TRUE = 1} decomposeRowClusts;
  enum UseGlobEps {USE_GLOB_EPS_FALSE = 0, USE_GLOB_EPS_TRUE = 1} useGlobEps;
  enum Projection {GALERKIN = 0, LSPG = 1} projection;

  double samplingTol;
  double maxItRatio;
  double maxSizeRatio;
  
  int maxNumResComps;

  const char *clustTols;

  SamplingWeightingData();

  void setup(const char *, ClassAssigner * = 0);
};

//------------------------------------------------------------------------------

struct ReducedMeshConstructionData {

  enum GenerateTrainingData {GENERATE_TRAINING_FALSE = 0, GENERATE_TRAINING_TRUE = 1} generateTraining;
  enum ConstructReducedMesh {CONSTRUCT_MESH_FALSE = 0, CONSTRUCT_MESH_TRUE = 1} constructMesh;
  enum NodeSamplingType {GAPPY_POD = 0, SAMPLING_WEIGHTING = 1} nodeSamplingType;
  enum IncludeLiftFaces {LIFT_FACES_NONE = 0, LIFT_FACES_ALL = 1} includeLiftFaces;
  enum UseInputSampSol {INPUT_SAMP_SOL_FALSE = 0, INPUT_SAMP_SOL_TRUE = 1} useInputSampSol;
  enum IncludePeriodicFaces {PERIODIC_FACES_NONE = 0, PERIODIC_FACES_ALL = 1} includePeriodicFaces;
  enum StackedTraining {STACKED_FALSE = 0, STACKED_TRUE = 1, SPATIAL_ONLY = 2} stackedTraining;
  enum AddFreeStream {ADD_FREESTREAM_FALSE = 0, ADD_FREESTREAM_TRUE = 1} addFreeStream;
  enum EnforceBcs {ENFORCE_BCS_FALSE = 0, ENFORCE_BCS_TRUE = 1} enforceBcs;
  enum SamplingStrategy {INPUT = 0, CLUSTER_CENTERS = 1} samplingStrategy;

  int minDimensionStateBasis;
  int maxDimensionStateBasis;
  double energyStateBasis;
  int minDimensionResBasis;
  int maxDimensionResBasis;
  double energyResBasis;
  const char *clustEnergies;
  int numNeighbors;

  GappyPODData gappyData;
  SamplingWeightingData samplingWeightingData;

  ReducedMeshConstructionData();

  void setup(const char *, ClassAssigner * = 0);
};

//------------------------------------------------------------------------------

struct NonlinearRomOfflineData {

  FeatureSelectionData fsd;
  AdaptiveRomData arom;
  ROBConstructionData rob;
  ReducedMeshConstructionData rmsh;

  NonlinearRomOfflineData();

  void setup(const char *, ClassAssigner * = 0);
};

//------------------------------------------------------------------------------

struct NonlinearRomOnlineData {

  enum Projection {GALERKIN = 0, LSPG = 1} projection;
  enum SystemApproximation {SYSTEM_APPROXIMATION_NONE = 0, GNAT = 1,
                            COLLOCATION = 2, SAMPLING_WEIGHTING = 3} systemApproximation;
  enum pseudoStatePopulation {REDUCED_COORDINATES = 0, SPATIAL_EXTRAPOLATION = 1} pseudoStatePopulation;
  enum LSSolver {QR = 0, NORMAL_EQUATIONS = 1} lsSolver;
  enum BasisType {DENSE = 0, ROW_CLUSTERED = 1, NEAREST_NEIGHBOR = 2} basisType;
  enum ReuseJac {REUSE_FALSE = 0, REUSE_TRUE = 1} reuseJac;
  enum ComponentScaling {COMPONENT_SCALING_FALSE = 0, COMPONENT_SCALING_TRUE = 1} componentScaling;
  enum AddFreeStream {ADD_FREESTREAM_FALSE = 0, ADD_FREESTREAM_TRUE = 1} addFreeStream;
  enum EnforceBcs {ENFORCE_BCS_FALSE = 0, ENFORCE_BCS_TRUE = 1} enforceBcs;
  enum ROBStorage {ROB_OLD = 0, ROB_NEW = 1} robStorage;
  enum DistanceComparisons {DISTANCE_COMPARISONS_OFF = 0,
                            DISTANCE_COMPARISONS_ON = 1} distanceComparisons;
  enum ApplyMask {APPLY_MASK_OFF = 0, APPLY_MASK_ON = 1, APPLY_MASK_LEGACY = 2} applyMask;
  enum ProjectState {PROJECT_STATE_OFF = 0, PROJECT_STATE_ON = 1} projectState;
  enum UseQuadraticManifold {USE_QUADRATIC_FALSE = 0, USE_QUADRATIC_TRUE = 1} quadratic;
  enum UseGeneralManifold {USE_GENERAL_MANIFOLD_FALSE = 0, USE_GENERAL_MANIFOLD_TRUE = 1} useGenManOnline;
  enum CacheLoadedClusters {CACHE_LOADED_CLUSTERS_FALSE = 0, CACHE_LOADED_CLUSTERS_TRUE = 1} cacheLoadedClusters;
  enum Verbose {VERBOSE_FALSE = 0, VERBOSE_TRUE = 1} verbose;

  int minDimension;
  int maxDimension;
  int quadManifoldDimension;
  int projectStateMaxIts;
  double energy;
  double projectStateThreshold;
  double projectStateWeight;
  double genManReductionFactor;

  const char *clustEnergies;

  int numNeighbors;
  int minNeighPerSnapFile;

  NonlinearRomOnlineData();

  void setup(const char *, ClassAssigner * = 0);
};

//------------------------------------------------------------------------------

struct PadeData {

  static const int num = 11;

  double freq[num];
  double freq1;
  double freq2;
  double freq3;
  double freq4;
  double freq5;
  double freq6;
  double freq7;
  double freq8;
  double freq9;
  double freq10;
  double freq11;

  int nPoints;
  int degNum;
  int degDen;

  PadeData();

  void setup(const char *, ClassAssigner * = 0);
};

//------------------------------------------------------------------------------

struct LinearizedData {

  enum PadeReconstruction {TRUE = 1, FALSE = 0} padeReconst;
  enum Type {DEFAULT = 0, ROM = 1, FORCED = 2} type;

  enum Domain {TIME = 0, FREQUENCY = 1} domain;
  enum InitialCondition {DISPLACEMENT = 0, VELOCITY = 1} initCond;
  enum GramSchmidt {TRUE_GS = 1, FALSE_GS = 0} doGramSchmidt;
  double amplification;
  double frequency;
  double stepsize;
  double freqStep;
  double eps;
  double epsEV;
  double tolerance;
  double refLength;
  const char *strModesFile;
  int modeNumber;
  int numPOD;
  int numStrModes;
  int maxItEV;
  static const int numFreq = 20;
  double gamFreq[numFreq];
  double gamFreq1;
  double gamFreq2;
  double gamFreq3;
  double gamFreq4;
  double gamFreq5;
  double gamFreq6;
  double gamFreq7;
  double gamFreq8;
  double gamFreq9;
  double gamFreq10;
  double gamFreq11;
  double gamFreq12;
  double gamFreq13;
  double gamFreq14;
  double gamFreq15;
  double gamFreq16;
  double gamFreq17;
  double gamFreq18;
  double gamFreq19;
  double gamFreq20;

  PadeData pade;

  enum InitialTimeStep {FULL = 0, SQUARED = 1, USER_DEFINED = 2} initialTimeStep;
  enum InitialStepScheme {BACKWARD_EULER = 0, RK2 = 1} initialStepScheme;
  // Old code assumed non-dimensional solution vectors, but state vector output is
  // dimensionalized for dimensional simulations. This allows samples to be either.
  enum AltitudeSampleMode {NON_DIMENSIONAL = 0, DIMENSIONAL = 1} altitudeSampleMode;

  // flag that controls orthogonalization of POD basis
  // previously this was controlled by TsData.Form, but implementation differences
  // required a change
  enum Orthogonalization {IDENTITY = 0, CONTROL_VOLUME = 1} orthogonalization;
  // flag that controls whether or not to compute snapshots and add to snapshots read
  // from disk
  enum ComputeSnapshots {FALSE_SNAP = 0, TRUE_SNAP = 1} computeSnapshots;
  // number of control inputs for error analysis preprocessing
  int nCtrlInputs;
  int timeHorizon;
  // flag that controls using full aerodynamic force or not (i.e. linear vs. linearized structure)
  // potentially useful for debugging
  enum TotalForce {OFF = 0, ON = 1} totalForce;

  // flag that controls addition of dForcedX * dX to modal force
  enum AddMeshDerivative {OFF_DFDX = 0, ON_DFDX = 1} addMeshDerivative;

  // number of control surface modes
  int nCtrlSurfModes;
  const char *ctrlSurfModesFile;

  // PROM training method in the Frequency domain
  // STANDARD indicates the current method (either numStrModes or nCtrlSurfModes must be non-zero)
  // CONTROL_FOCUSED works on the coupled fluid/structure system, using the control surface modes as
  // the forcing term in the harmonic expansion (but only produces a fluid PROM; both numStrModes and
  // nCtrlSurfModes must be non-zero)
  enum PROMTrainingMethod {STANDARD = 0, CONTROL_FOCUSED = 1} promTrainingMethod;

  // trigger use of trim correction to reference state
  enum UseTrim {NO_TRIM = 0, TRIM = 1} useTrim;
  // trigger interpretation of initial condition as being the absolute state or as relative to the
  // solution read in Input.Solution when UseTrim = On, i.e. does Input.Perturbed = W = Input.Solution + dW_Trim + dW
  // (absolute) or does Input.Perturbed = Input.Solution + dW = W - dW_Trim (relative)?
  // In the absolute case, any control surface deflection read in via ControlInterface::ctrlSurfaceDeflection
  // would be subtracted by the trim (i.e. it is interpreted as the absolute deflection). In the relative case,
  // the output of the control algorithm is assumed to be only outputting the perturbation to the trim.
  enum InitialConditionReference {ABSOLUTE = 0, RELATIVE = 1} initialConditionReference;

  // triggers normalization of snapshots prior to SVD
  // currently done after outputting snapshots, if requested
  enum NormalizeSnapshots {FALSE_NORMALIZE = 0, TRUE_NORMALIZE = 1} normalizeSnapshots;

  // trigger interpretation of Input.PODData as a file describing multiple ROBs to read and combine into one ROB
  enum ReadMultipleROBs {FALSE_READ_MULTIPLE_ROBS = 0, TRUE_READ_MULTIPLE_ROBS = 1} readMultipleROBs;
  // choose SVD or QR with column pivoting to orthogonalize multiple bases
  enum OrthogonalizationMethod {SVD = 0, QR = 1} orthogonalizationMethod;

  // choose whether or not to compute spectral radius of discretized error dynamics matrix
  enum ComputeSpectralRaidus {FALSE_COMPUTE_SPECTRAL_RADIUS = 0, TRUE_COMPUTE_SPECTRAL_RADIUS = 1} computeSpectralRadius;
  // tolerance at which to stop computing error analysis preprocessing matices (i.e., once the Frobenius norm of those matrices
  // has decayed sufficiently, stop computing them)
  double errMatTol;

  // When normalization is turned on, choose whether to normalize each vector individually
  // or to "normalize" using the average norm of a group of snapshots
  enum NormalizationType {STANDARD_NORM = 0, GROUP_NORM = 1} normalizationType;
  // file defining groups of modes that are to be "normalized" using the average of the snapshot
  // norms for those modes; only used for newly computed snapshots
  // first line gives number of groups, then 2 lines per group:
  // first line of a given group gives the number of modes in the group
  // second line gives the mode IDs, starting with 1. If using control surface modes
  // and the standard method of computing snapshots, control surface mode IDs start
  // at NumStrModes + 1. If the control focused method is being used and NumCtrlInputs
  // is nonzero, the control surface mode IDs start at NumCtrlInputs + 1.
  const char *modeGroupsFile;

  LinearizedData();

  void setup(const char *, ClassAssigner * = 0);
};

//------------------------------------------------------------------------------

struct Volumes {

  ObjectMap<VolumeData> volumeMap;

  void setup(const char *, ClassAssigner * = 0);
};

//------------------------------------------------------------------------------

struct Surfaces {

  ObjectMap<SurfaceData> surfaceMap;

  void setup(const char *);

  bool surface_type(SurfaceData::Type t) const {
    return std::any_of(surfaceMap.dataMap.begin(), surfaceMap.dataMap.end(), [&](const std::pair<int, SurfaceData*>& p) {return p.second->type == t;});
  }
};

//------------------------------------------------------------------------------

struct OneDimensionalInfo {
  enum CoordinateType {CARTESIAN = 0, CYLINDRICAL = 1, SPHERICAL = 2} coordType;
  enum VolumeType {CONSTANT_VOLUME = 0, REAL_VOLUME = 1} volumeType;
  enum Mode {NORMAL = 0, CONVTEST1 = 1, CONVTEST2 = 2} mode;
  enum ProblemMode {MULTIFLUID = 0, FSI = 1} problemMode;

  int numPoints;
  int fluidId2;
  int sourceTermOrder;

  double maxDistance;
  double interfacePosition;
  double density1, velocity1, pressure1, temperature1;
  double density2, velocity2, pressure2, temperature2;

  ProgrammedBurnData programmedBurn;

  OneDimensionalInfo();

  void setup(const char *);
};

//------------------------------------------------------------------------------

class IoData {

  char *cmdFileName;
  FILE *cmdFilePtr;


 public:

  Communicator *com;
  
  InputData input;
  OutputData output;
  PreconditionData prec;
  RestartParametersData restart;
  ProblemData problem;
  ReferenceStateData ref;
  BcsData bc;
  EquationsData eqs;
  MultiFluidData mf;
  SchemesData schemes;

  SensitivityAnalysisData sa;

  TsData ts;
  DGCLData dgcl;
  DefoMeshMotionData dmesh;
  RigidMeshMotionData rmesh;
  AeroelasticData aero;
  ForcedData forced;
  Surfaces surfaces;
  Velocity rotations;
  Volumes volumes;

  EmbeddedFrameworkData embed;
  EmbeddedALEFrameworkData embedale;
  OneDimensionalInfo oneDimensionalInfo;
  AdaptiveMeshRefinementData amr;

  // for nonlinear ROM
  NonlinearRomOfflineData romOffline;
  NonlinearRomOnlineData romOnline;
  NonlinearRomFileSystemData romDatabase;

  LinearizedData linearizedData;

 public:

  IoData(Communicator *);

  void readCmdLine(int, char **);
  void setupCmdFileVariables();
  void readCmdFile();
  void readRestartData();
  void applyGeometryPrefix();
  void applyMeshIndex(int);
  void removeMeshIndex();
  void applyClusterPrefix(int);
  int resetInputValues();
  int checkFileNames();
  int checkInputValues();
  int checkInputValuesAllEquationsOfState();
  int checkInputValuesProgrammedBurn();
  int checkProgrammedBurnLocal(ProgrammedBurnData&, InitialConditions&);
  int checkInputValuesCFL();
  int checkInputValuesAllInitialConditions();
  int checkInputValuesAmr();
  void nonDimensionalizeAllEquationsOfState();
  void nonDimensionalizeAllInitialConditions();
  void nonDimensionalizeForcedMotion();
  void nonDimensionalizeOneDimensionalProblem();
  void nonDimensionalizeRestartData();
  void nonDimensionalizeAmrParameters();
  int checkInputValuesNonDimensional();
  int checkInputValuesDimensional(std::map<int, SurfaceData *>&);
  int checkInputValuesEssentialBC();
  void checkInputValuesTurbulence();
  void checkInputValuesDefaultOutlet();
  int checkInputValuesEmbeddedFramework();
  int checkInputValuesSensitivityAnalysis();
  int checkBoundaryValues();
  int checkSolverValues(std::map<int, SurfaceData *>&);
  int checkInputValuesInitialConditions(InitialConditions&, int);
  int checkInputValuesEquationOfState(FluidModelData&, int);
  void nonDimensionalizeInitialConditions(InitialConditions&);
  void nonDimensionalizeFluidModel(FluidModelData&);
  void nonDimensionalizeViscosityModel(ViscosityModelData&);
  void nonDimensionalizeThermalCondModel(ThermalCondModelData&);
  int checkInputValuesSparseGrid(SparseGridData&);
  void printDebug();
  int setupOneDimensional();
};

//------------------------------------------------------------------------------

#ifdef TEMPLATE_FIX
  #include <IoData.C>
#endif

#endif

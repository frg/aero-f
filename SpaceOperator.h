#ifndef _SPACE_OPERATOR_H_
#define _SPACE_OPERATOR_H_

#include <DistEmbeddedBcData.h>
#include <NodeData.h>
#include <Types.h>

#include <complex>

class IoData;
class VarFcn;
class BcFcn;
class RecFcn;
class FluxFcn;
class FemEquationTerm;
class VolumicForceTerm;
class Domain;
class DistEdgeGrad;
class DistGeoState;
class Communicator;
class PostFcn;
class DistMovingLeastSquaresForceHandler;
class TriangleSet;

template<int dim> class DistBcData;
template<int dim> class DistTimeState;
template<class Scalar> class DistVec;
template<class Scalar, int dim> class DistSVec;
template<class MatrixType> class DistMat;
template<class Scalar, int dim> class MvpMat;
template<int dim> class GhostPoint;
template<int dim> class DistLESData;
template<int dim> class PostOperator;
template<int dim> class MatVecProd_dRdX;
template<int dim, class Scalar> class DistNodalGrad;

//------------------------------------------------------------------------------

template<int dim>
class SpaceOperator {

 protected:
  VarFcn *varFcn;
  DistBcData<dim> *bcData;
  DistEmbeddedBcData<dim> *ebcData;
  DistSVec<double, dim> *V;
  DistSVec<double, dim> *dU, *dV;
  DistVec<double> *dPstiff;

  BcFcn *bcFcn;
  FluxFcn **fluxFcn;
  RecFcn *recFcn;
  RecFcn *recFcnLS;

  DistNodalGrad<dim, double> *ngrad;
  DistNodalGrad<dim, std::complex<double>> *cngrad;
  DistEdgeGrad *egrad;
  DistNodalGrad<dimLS, double> *ngradLS;
  DistEdgeGrad *egradLS;
  FemEquationTerm *fet;
  DistLESData<dim> *les;
  VolumicForceTerm *volForce;

  DistMovingLeastSquaresForceHandler *mlsfh;
  DistMovingLeastSquaresForceHandler *mlsfhPressureDistortion;

  int order;
  int failsafe;
  int rshift;

  bool locAlloc;

  double pinfty; // TODO: add dpinfty for use in sensitivities (specifically force sensitivities in embedded)?

 public:
  DistGeoState *geoState;
  Domain *domain;
  Communicator *com;
  IoData& iod;

  SpaceOperator(IoData&, VarFcn *, DistBcData<dim> *, DistEmbeddedBcData<dim> *, DistGeoState *,
                Domain *, DistSVec<double, dim> *);
  SpaceOperator(const SpaceOperator<dim>&);
  ~SpaceOperator();

  void resize1(NodeData *);
  void resize2();

  void fsoInitialize();

  void setBcFcn(BcFcn *bf) {
    bcFcn = bf;
  }
  void setFluxFcn(FluxFcn **ff) {
    fluxFcn = ff;
  }
  void setFemEquationTerm(FemEquationTerm *fem) {
    fet = fem;
  }

  FluxFcn **createFluxFcn(int = 0);
  BcFcn *createBcFcn();
  RecFcn *createRecFcn();
  RecFcn *createRecFcnLS();
  FemEquationTerm *createFemEquationTerm();
  DistLESData<dim> *createLesData();
  VolumicForceTerm *createVolumicForceTerm();

  void fix(DistSVec<bool, 2>&);
  void resetTag();

  RecFcn *getRecFcn() {
    return recFcn;
  }

  DistNodalGrad<dim, double> *getDistNodalGrad() {
    return ngrad;
  }

  DistEdgeGrad *getDistEdgeGrad() {
    return egrad;
  }

  void updateFixes();

  int getSpaceOrder() {
    return order;
  }

  BcFcn *getBcFcn() {
    return bcFcn;
  }

  VarFcn *getVarFcn() {
    return varFcn;
  }

  DistMovingLeastSquaresForceHandler* getMovingLeastSquaresForceHandler() {
    return mlsfh;
  }

  DistMovingLeastSquaresForceHandler* getMovingLeastSquaresForceHandlerPressureDistortion() {
    return mlsfhPressureDistortion;
  }

  FemEquationTerm *getFemEquationTerm() {
    return fet;
  }

  VolumicForceTerm *getVolumicForceTerm() {
    return volForce;
  }

  DistVec<int> *getFluidId() {
    return (ebcData) ? ebcData->fluidId : nullptr;
  }

  DistEmbeddedBcData<dim> *getEmbeddedBcData() {
    return ebcData;
  }

  void computeResidual(DistSVec<double, dim>&, DistSVec<double, dim>&, DistTimeState<dim> *,
                       DistVec<bool> * = nullptr, DistVec<bool> * = nullptr);

  void computeDerivativeOfResidual(DistSVec<double, dim>&, double, DistSVec<double, dim>&,
                                   DistSVec<double, dim>&, DistTimeState<dim> *);

  void computeDerivativeOfResidual(MatVecProd_dRdX<dim>&, DistSVec<double, dim>&, DistSVec<double, dim>&,
                                   DistSVec<double, dim>&, bool, DistTimeState<dim> *);

  void computeTransposeDerivativeOfResidual(MatVecProd_dRdX<dim>&, DistSVec<double, dim>&,
                                            DistSVec<double, dim>&, DistSVec<double, 3>&);

  void computeResidualLS(DistSVec<double, dim> *, DistSVec<double, dimLS>&, DistSVec<double, dimLS>&);

  template<class Scalar, int neq>
  void computeJacobian(DistSVec<double, dim>&, DistMat<MvpMat<Scalar, neq>>&, DistTimeState<dim> *);

  template<class Scalar>
  void computeJacobianLS(DistSVec<double, dimLS>&, DistMat<MvpMat<Scalar, dimLS>>&);

  void precomputeH2(DistSVec<double, dim>&, DistSVec<double, 4 * dim>&);

  template<class Scalar, int neq>
  void applyH2(DistSVec<double, dim>&, DistMat<MvpMat<Scalar, neq>>&, DistSVec<double, 4 * dim>&,
               DistSVec<Scalar, dim>&, DistSVec<Scalar, neq>&, DistVec<bool> * = nullptr,
               DistVec<bool> * = nullptr);

  template<int neq>
  void applyH2(DistSVec<double, dim>&, DistMat<MvpMat<std::complex<double>, neq>>&, DistSVec<double, 4 * dim>&,
               DistSVec<std::complex<double>, dim>&, DistSVec<std::complex<double>, neq>&, DistVec<bool> * = nullptr,
               DistVec<bool> * = nullptr);

  template<int neq>
  void applyH2(DistSVec<double, dim>&, DistMat<MvpMat<float, neq>>&, DistSVec<double, 4 * dim>&,
               DistSVec<float, dim>&, DistSVec<float, neq>&, DistVec<bool> * = nullptr,
               DistVec<bool> * = nullptr);

  void applyBCsToSolutionVector(DistSVec<double, dim>&);

  void applyBCsToSolutionVectorFullMesh(DistSVec<double, dim>&);

  void applyBCsToTurbSolutionVector(DistSVec<double, dim>&);

  void applyBCsToResidual(DistSVec<double, dim>&, DistSVec<double, dim>&);

  void applyBCsToDerivativeOfResidual(DistSVec<double, dim>&, DistSVec<double, dim>&);

  void applyBCsToTransposeDerivativeOfResidual(MatVecProd_dRdX<dim>&, DistSVec<double, dim>&, DistSVec<double, 3>&);

  void applyBCsToTimeResidual(DistSVec<double, dim>&, DistSVec<double, dim>&);

  template<class Scalar, int neq>
  void applyBCsToJacobian(DistSVec<double, dim>&, DistMat<MvpMat<Scalar, neq>>&);

  void computeGradP(DistSVec<double, dim>&, DistSVec<double, 3>&);

  void computeDerivativeOfGradP(DistSVec<double, dim>&, DistSVec<double, dim>&, bool, DistSVec<double, 3>&);

  void computeDerivativeOfGradP(MatVecProd_dRdX<dim>&, DistSVec<double, dim>&, bool, DistSVec<double, 3>&);

  void computeTransposeDerivativeOfGradP(MatVecProd_dRdX<dim>&, DistSVec<double, 3>&, bool, DistSVec<double, 3>&,
                                         DistSVec<double, dim>&, bool = true);

  void computeForceLoad(DistSVec<double, dim>&, double (*)[3], int, PostFcn *, double *, int);
  void computeDerivativeOfForceLoad(DistSVec<double, dim>&, DistSVec<double, dim> *, double (*)[3], int, PostFcn *, double *, int, double);

  bool updateSweptNodes(DistSVec<double, dim>&, DistVec<double>&,
                        DistSVec<double, dim>&, double *, TriangleSet * = nullptr);

  void applyNewtonInitialGuess(DistSVec<double, dim>&, double, TriangleSet * = nullptr);

  void populateGhostPoints(DistSVec<double, dim>&);

  void extrapolatePhaseChange(DistSVec<double, dim>&, DistVec<double>&,
                              DistSVec<double, dim>&);

  void updatePeriodicBoundaries(DistSVec<double, dim>&);

  void setupMLSFH();

  void setupMLSFHPressureDistortion();

  void computeDerivativeOperators(Vec3D&, DistSVec<double, dim>&, DistSVec<double, 3>&,
                                  PostOperator<dim> *, MatVecProd_dRdX<dim>&, int);

  void computeDerivativeOperatorsLinearized(DistSVec<double, dim>&, DistSVec<double, 3>&,
                                            PostOperator<dim> *, MatVecProd_dRdX<dim>&);

};

//------------------------------------------------------------------------------

#endif

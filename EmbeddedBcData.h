#ifndef _EMBEDDED_BC_DATA_
#define _EMBEDDED_BC_DATA_

#include <IoData.h>
#include <Types.h>
#include <Vector.h>
#include <Vector3D.h>
#include <LevelSet/LevelSetStructure.h>

#include <vector>
#include <set>

class FluidSelector;

template<int dim> class ExactRiemannSolver;
template<int dim> class GhostPoint;

//------------------------------------------------------------------------------

template<int dim>
class EmbeddedBcData {
 public:
  LevelSetStructure *LSS;
  Vec<int> *fluidId;
  Vec<GhostPoint<dim>*> *ghostPoints;
  bool isViscous;
  ExactRiemannSolver<dim>& riemann;
  EmbeddedFrameworkData& embed;
  SVec<double, dim> *leftRiemannAverage, *rightRiemannAverage;
  Vec<double> *leftRiemannSum, *rightRiemannSum;
  std::vector<std::set<int>> *globalNeighbors;
  FluidSelector *fluidSelector;
  MultiFluidData& mf;
  SVec<double, dimLS> *Phi;

  EmbeddedBcData(LevelSetStructure *_LSS, Vec<int> *_fluidId, Vec<GhostPoint<dim>*> *_ghostPoints, bool _isViscous, ExactRiemannSolver<dim>& _riemann,
                 EmbeddedFrameworkData& _embed, SVec<double, dim> *_leftRiemannAverage, SVec<double, dim> *_rightRiemannAverage,
                 Vec<double> *_leftRiemannSum, Vec<double> *_rightRiemannSum, std::vector<std::set<int>> *_globalNeighbors, FluidSelector *_fluidSelector,
                 MultiFluidData& _mf, SVec<double, dimLS> *_Phi)
    : LSS(_LSS), fluidId(_fluidId), ghostPoints(_ghostPoints), isViscous(_isViscous), riemann(_riemann), embed(_embed),
      leftRiemannAverage(_leftRiemannAverage), rightRiemannAverage(_rightRiemannAverage), leftRiemannSum(_leftRiemannSum),
      rightRiemannSum(_rightRiemannSum), globalNeighbors(_globalNeighbors), fluidSelector(_fluidSelector), mf(_mf), Phi(_Phi) {}

   void assembleRiemannAverage(int, int, int, int_t, int, int, Vec3D&, double, double[dim], Vec3D, Vec3D, std::vector<int_t>&);
};

//------------------------------------------------------------------------------

template<int dim>
void
EmbeddedBcData<dim>::assembleRiemannAverage(int i, int j, int l, int_t global_n, int ID, int tag, Vec3D& surfaceNorm,
                                            double w, double Vn[dim], Vec3D Xi, Vec3D Xj, std::vector<int_t>& locToGlobNodeMap) {
  // populate contribution from node n to ghost-like weighted average state at node j assocated with real state at node i
  // note #1: this contribution is evaluted in the subdomain containing edge l (i.e. both nodes i and j are local)
  // note #2: node n may either be local or be in a neighboring subdomain in which case the quantites ID, tag and surfaceNorm
  //          are obtained by communication between subdomains, and i_equals_n is false
  // l is the local edge index for edge i-j
  // ID is the fluid ID of node n
  // tag is equal to 0 if edge n-j is not intersected, -1 if edge n-j is intersected and n is on the normal pointing side, or
  //     1 if edge n-j is intersected and n is on the other side
  // surfaceNorm is the outward pointing normal at the intersection point on edge n-j closest to n when edge n-j is intersected
  // Vn is the to be averaged state, for example fluid primitive state at node n, or the half Riemann solution
  // w is the weight associated with node n used to define the weighted average

  int IDi, diri, dir;
  double dot;
  bool i_equals_n = (locToGlobNodeMap[i] == global_n);
  if(embed.riemannAveragingType == EmbeddedFrameworkData::ELEMENTLOCAL && !i_equals_n &&
     (*globalNeighbors)[i].find(global_n) == (*globalNeighbors)[i].end()) return;

  if(!i_equals_n) {
    // criteria:
    // 1. i and n must have the same fluid ID
    // 2. i and n must both be on the same side of the wall (note: surface normals must be consistently defined)
    //    note: i and n are assumed to be on the same side if dot > 0 and dir == diri (see below)
    IDi = fluidId ? (*fluidId)[i] : 0;
    if(ID == IDi) {
      if(embed.definitionActiveInactive == EmbeddedFrameworkData::CONTROLVOLUME) {
        Vec3D surfaceNorm_i;
        LSS->nWall_node(i, surfaceNorm_i);
        dot = surfaceNorm_i * surfaceNorm;
        dir = diri = 1;
      }
      else {
        PartialLevelSetResult resij = LSS->getPartialLevelSetDataAtEdgeCenter(0.0, l, (i < j));
        diri = (resij.surfaceNorm * (Xj - Xi) <= 0) ? -1 : 1; // -1: i on the normal pointing side, 1: i on other side
        if(tag != 0) {
          dir = tag;
          dot = surfaceNorm * resij.surfaceNorm;
        }
        else { // n is on the same side as j if edge n-j is not intersected
          if(embed.averagedRiemann == EmbeddedFrameworkData::AVERAGED_RIEMANN_ON) // Riemann solution can only be computed when it is intersected
            return;
          PartialLevelSetResult res = LSS->getPartialLevelSetDataAtEdgeCenter(0.0, l, (j < i));
          dir = (res.surfaceNorm * (Xi - Xj) <= 0) ? -1 : 1; // -1: j on the normal pointing side, 1: j on other side
          dot = res.surfaceNorm * resij.surfaceNorm;         // < 0: normals are opposing, >= 0 normals are not opposing
        }
      }
    }
  }

  if(i_equals_n || ((ID == IDi) && (dir == diri) && (dot > 0))) {
    double *avg = (locToGlobNodeMap[i] < locToGlobNodeMap[j]) ? (*leftRiemannAverage)[l] : (*rightRiemannAverage)[l];
    double& sum = (locToGlobNodeMap[i] < locToGlobNodeMap[j]) ? (*leftRiemannSum)[l] : (*rightRiemannSum)[l];
    for(int k = 0; k < dim; ++k) avg[k] += w * Vn[k];
    sum += w;
  }
}

//------------------------------------------------------------------------------

#endif

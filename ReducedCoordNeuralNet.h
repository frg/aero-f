//
// Created by jbarnett on 6/3/22.
//

#ifndef AERO_F_REDUCEDCOORDNEURALNET_H
#define AERO_F_REDUCEDCOORDNEURALNET_H

#include "Vector.h"
#include "Communicator.h"
#include "Domain.h"
#include "IoData.h"
#include "DenseMatrix.h"
#include "Timer.h"

#include <memory>
#ifdef USE_TORCH
#include "torch/script.h"
#include "torch/csrc/autograd/autograd.h"
#endif

#ifdef USE_EIGEN3_DenseMat
#include <Eigen/Dense>
#include <Eigen/SVD>
typedef Eigen::Matrix<double,Eigen::Dynamic,Eigen::Dynamic,Eigen::RowMajor> MatrixType;
typedef Eigen::Matrix<double,Eigen::Dynamic,1,Eigen::ColMajor> VectorType;
#endif

class ReducedCoordNeuralNet {
private:
  Communicator *com;
  Timer *timer;
  IoData &ioData;
  int64_t input_size, output_size;
  bool is_loaded = false;
#ifdef USE_TORCH
  torch::jit::script::Module model;
#endif
public:
  ReducedCoordNeuralNet(IoData& _ioData, Domain *_domain);
  ~ReducedCoordNeuralNet();
  void loadModel(const char*);
  void evalModel(Vec<double>&, Vec<double>&);
  void evalModelGradient(Vec<double> &Qrom, DenseMat<double> &matOut);
  void evalModel(Vec<double>&, VectorType&);
  void evalModelGradient(Vec<double> &Qrom, MatrixType &matOut);
  long getInputSize() const {return input_size; }
  long getOutputSize() const {return output_size; }
};

#endif //AERO_F_REDUCEDCOORDNEURALNET_H

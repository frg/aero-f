#ifndef _DIST_INFO_H_
#define _DIST_INFO_H_

#include <vector>

class Communicator;

template<class Scalar> class DistVec;

//------------------------------------------------------------------------------

struct DistInfo {

  int numLocSub;
  std::vector<int> subLen;
  DistVec<bool> *masterFlag;
  DistVec<double> *invNdWeight; // inverse of the number of subs touching a node

  Communicator *com;

  DistInfo(int, Communicator *);
  ~DistInfo();

  void setLen(int iSub, int len) {
    subLen[iSub] = len;
  }

  int subSize(int iSub) const {
    return (subLen.empty()) ? 0 : subLen[iSub];
  }

  bool *getMasterFlag(int) const;

  double *getInvWeight(int) const;

  void finalize(bool);

  int localSize(int) const;

  int localSize() const;

  int globalSize() const;

};

//------------------------------------------------------------------------------

#endif

#ifndef _BC_DATA_H_
#define _BC_DATA_H_

#include <Face.h>

template<class Scalar, int dim> class SVec;

//------------------------------------------------------------------------------

template<int dim>
class BcData {

  SVec<double, Face::MaxNumNd * dim>& Uface;
  SVec<double, dim>& Unode;
  SVec<double, dim>& Uporouswall;
  SVec<double, dim*(dim-5)> *dUfaceSA;
  SVec<double, dim*(dim-5)> *dUnodeSA;

  SVec<double, Face::MaxNumNd * dim> *dUface;
  SVec<double, dim> *dUnode;
  Vec<bool>& wallFcnActVector;

 public:
  BcData(SVec<double, Face::MaxNumNd * dim>& uf, SVec<double, dim>& un, SVec<double, dim>& uporouswall, Vec<bool>& wallFcnActVector,
         SVec<double, dim*(dim-5)> *dufsa, SVec<double, dim*(dim-5)> *dunsa,
         SVec<double, Face::MaxNumNd * dim> *duf, SVec<double, dim> *dun)
    : Uface(uf), Unode(un), Uporouswall(uporouswall), dUfaceSA(dufsa), dUnodeSA(dunsa), dUface(duf), dUnode(dun), wallFcnActVector(wallFcnActVector) {
  }
  ~BcData() {}

  void fsoInitialize(SVec<double, Face::MaxNumNd * dim> *duf, SVec<double, dim> *dun) {
    dUface = duf;
    dUnode = dun;
  }

  SVec<double, Face::MaxNumNd * dim>& getFaceStateVector() const {
    return Uface;
  }
  SVec<double, dim>& getNodeStateVector() const {
    return Unode;
  }
  SVec<double, dim>& getPorousWallStateVector() const {
    return Uporouswall;
  }
  SVec<double, dim*(dim-5)> *getdFaceStateVectorSA() const {
    return dUfaceSA;
  }
  SVec<double, dim*(dim-5)> *getdNodeStateVectorSA() const {
    return dUnodeSA;
  }

  SVec<double, Face::MaxNumNd * dim> *getdFaceStateVector() const {
    return dUface;
  }
  SVec<double, dim>& getdNodeStateVector() const {
    return *dUnode;
  }
  Vec<bool>& getIsWallFcnActVector() const {
    return wallFcnActVector;
  }

};

//------------------------------------------------------------------------------

#endif

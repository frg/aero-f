#ifndef _AUGMENTED_THREE_FIELD_STRAIN_ENERGY_FUNCTION_H_
#define _AUGMENTED_THREE_FIELD_STRAIN_ENERGY_FUNCTION_H_

#include <AutoDiff/Function.h>
#include <AutoDiff/SpaceDerivatives.h>
#include <iostream>
#include <stdexcept>

// References
// [1] J.C. Simo, R.L. Taylor and K.S. Pister, "Variational and projection methods for the volume constraint in finite
//     deformation plasticity", Computer Methods in Applied Mechanics and Engineering, 51 (1985) 177-208.
// [2] J.C. Simo and R.L. Taylor, "Quasi-incompressible finite elasticity in principal stretches. Continuum basis and
//     numerical algorithms", Computer Methods in Applied Mechanics and Engineering, 8.5 (1991) 273-310.

//------------------------------------------------------------------------------

template<typename Scalar, 
         template <typename S> class ShapeFunctionTemplate,
         template <typename S> class ShapeFunctionTemplate2,
         template <typename S> class ShapeFunctionTemplate3,
         template <typename S> class ShapeFunctionTemplate4,
         typename QuadratureRule>
class AugmentedThreeFieldStrainEnergyFunction
 : public ScalarValuedFunction<ShapeFunctionTemplate<Scalar>::NumberOfGeneralizedCoordinates*ShapeFunctionTemplate<Scalar>::NumberOfValues
                               +ShapeFunctionTemplate2<Scalar>::NumberOfValues
                               +ShapeFunctionTemplate3<Scalar>::NumberOfValues,
                               Scalar,
                               ShapeFunctionTemplate<Scalar>::NumberOfGeneralizedCoordinates*ShapeFunctionTemplate<Scalar>::NumberOfValues
                               +2
                               +ShapeFunctionTemplate2<Scalar>::NumberOfValues
                               +ShapeFunctionTemplate3<Scalar>::NumberOfValues
                               +ShapeFunctionTemplate4<Scalar>::NumberOfValues+1,
                               2,
                               double>
{
  public:
    enum {
      NumberOfNodes  = ShapeFunctionTemplate<Scalar>::NumberOfValues,
      MaximumNumberOfMaterialConstants = 2,
      NumberOfNodes2 = ShapeFunctionTemplate2<Scalar>::NumberOfValues,
      NumberOfNodes3 = ShapeFunctionTemplate3<Scalar>::NumberOfValues,
      NumberOfNodes4 = ShapeFunctionTemplate4<Scalar>::NumberOfValues,
      NumberOfDimensions = ShapeFunctionTemplate<Scalar>::NumberOfGeneralizedCoordinates,
      NumberOfGeneralizedCoordinates = NumberOfNodes*NumberOfDimensions+NumberOfNodes2+NumberOfNodes3,
      NumberOfScalarConstants = NumberOfGeneralizedCoordinates+MaximumNumberOfMaterialConstants+NumberOfNodes4+1,
      NumberOfIntegerConstants = 2
    };

  private:
    Eigen::Matrix<double,NumberOfNodes,NumberOfDimensions> X;
    Eigen::Array<double,MaximumNumberOfMaterialConstants,1> c;
    Eigen::Matrix<double,NumberOfNodes2,1> Theta0;  // dilatation reference configuration
    Eigen::Matrix<double,NumberOfNodes3,1> p0;      // pressure reference configuration
    Eigen::Matrix<double,NumberOfNodes4,1> lambda;  // Lagrange multiplier
    double epsilon; // penalty parameter
    int deg;  // quadrature rule degree
    int wopt; // strain energy density function

  public:
    AugmentedThreeFieldStrainEnergyFunction(const Eigen::Array<double,NumberOfScalarConstants,1>& sconst,
                                            const Eigen::Array<int,NumberOfIntegerConstants,1>& iconst)
    {
      for(int inode = 0; inode < NumberOfNodes; ++inode) {
        X.row(inode) = Eigen::Map<Eigen::Matrix<double,NumberOfDimensions,1>>(const_cast<double*>(sconst.data())+NumberOfDimensions*inode);
      }
      c       = sconst.template segment<MaximumNumberOfMaterialConstants>(NumberOfDimensions*NumberOfNodes);
      Theta0  = sconst.template segment<NumberOfNodes2>(NumberOfDimensions*NumberOfNodes+MaximumNumberOfMaterialConstants);
      p0      = sconst.template segment<NumberOfNodes3>(NumberOfDimensions*NumberOfNodes+MaximumNumberOfMaterialConstants+NumberOfNodes2);
      lambda  = sconst.template segment<NumberOfNodes4>(NumberOfDimensions*NumberOfNodes+MaximumNumberOfMaterialConstants+NumberOfNodes2+NumberOfNodes3);
      epsilon = sconst[NumberOfDimensions*NumberOfNodes+MaximumNumberOfMaterialConstants+NumberOfNodes2+NumberOfNodes3+NumberOfNodes4];

      deg  = iconst[0];
      wopt = iconst[1];
    }

    Scalar operator() (const Eigen::Matrix<Scalar,NumberOfGeneralizedCoordinates,1>& q, Scalar t)
    {
      // q[0] = x translation of node 1
      // q[1] = y translation of node 1
      // q[2] = z translation of node 1
      // q[3] = x translation of node 2
      // q[4] = y translation of node 2
      // q[5] = z translation of node 2
      // etc...

      // return value: strain energy
      Scalar V = 0.0;

      using std::abs;
      using std::sqrt;
      using std::pow;

      // Set current configuration
      Eigen::Matrix<Scalar,NumberOfNodes,NumberOfDimensions> x;
      Eigen::Matrix<Scalar,NumberOfNodes2,1> Theta;
      Eigen::Matrix<Scalar,NumberOfNodes3,1> p;
      for(int inode = 0; inode < NumberOfNodes; ++inode) {
        x.row(inode) = X.row(inode).template cast<Scalar>() + q.template segment<NumberOfDimensions>(inode*NumberOfDimensions).transpose();
      }
      Theta = Theta0.template cast<Scalar>() + q.template segment<NumberOfNodes2>(NumberOfDimensions*NumberOfNodes);
      p     = p0.template cast<Scalar>()     + q.template segment<NumberOfNodes3>(NumberOfDimensions*NumberOfNodes+NumberOfNodes2);

      // shape function and derivatives
      ShapeFunctionTemplate2<double> N2;
      ShapeFunctionTemplate3<double> N3;
      ShapeFunctionTemplate4<double> N4;
      Jacobian<double, ShapeFunctionTemplate> dN(Eigen::Array<double,0,1>::Zero(), Eigen::Array<int,0,1>::Zero());

      // quadrature rule
      QuadratureRule qrule(deg);

      // local variables to be computed at each integration point
      Eigen::Matrix<double,NumberOfDimensions,1> xi;
      double weight;
      Eigen::Matrix<double,NumberOfNodes,NumberOfDimensions> dNdxi;
      Eigen::Matrix<double,NumberOfDimensions,NumberOfDimensions> J;
      Eigen::Matrix<double,NumberOfNodes,NumberOfDimensions> nGrad;
      Eigen::Matrix<Scalar,NumberOfDimensions,NumberOfDimensions> F, C, Ctilde;
      Scalar I1, I2, W, Theta_G, p_G;
      double lambda_G;

      // Loop over the integration points
      for(int ip = 0; ip < qrule.getN(); ++ip) {

        // get the integration point abscissa and weight
        qrule.getAbscissaAndWeight(ip, xi, weight);

        // compute shape function derivatives
        dNdxi = dN(xi, 0.);

        // compute the jacobian of the isoparametric mapping and the deformation gradient
        J = dNdxi.transpose()*X;
        nGrad = dNdxi*J.transpose().inverse();
        F = x.transpose().lazyProduct(nGrad);

        // interpolate the scalar fields
        Theta_G  = N2(xi,0.).template cast<Scalar>().dot(Theta);
        p_G      = N3(xi,0.).template cast<Scalar>().dot(p);
        lambda_G = N4(xi,0.).dot(lambda);

        // compute the dilational part of the right Cauchy-Green strain tensor
        C = F.transpose()*F;
        Scalar j = F.determinant();
        if(j <= 0) throw std::runtime_error("j <= 0");
        Ctilde = pow(j,-2/3.)*C;

        // evaluate the augmented strain energy density function at the integration point
        switch(wopt) {
          default :
          case 0 : I1 = Ctilde.trace(); W = c[0]/2*(I1 - 3); break; // neo-Hookean
          case 1 : I1 = Ctilde.trace(); I2 = 0.5*(I1*I1-(Ctilde*Ctilde).trace()); W = c[0]/2*(I1 - 3) + c[1]/2*(I2-3); break; // Mooney-Rivlin
        }

        V += (abs(J.determinant())*weight)*(W + p_G*(j-Theta_G) + epsilon*0.5*((Theta_G-1)*(Theta_G-1)) + lambda_G*(Theta_G-1));
      }

      return V;
    }

  public:
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW
};

//------------------------------------------------------------------------------

#endif

#ifndef _QUASI_INCOMPRESSIBLE_TETRA_P1_P0_H_
#define _QUASI_INCOMPRESSIBLE_TETRA_P1_P0_H_

#include <Deforming/MixedFiniteElement.h>
#include <Deforming/AugmentedThreeFieldStrainEnergyFunction.h>
#include <Deforming/Tet4LagrangePolynomial.h>
#include <Deforming/Constant.h>
#include <Deforming/QuadratureRule.h>

//------------------------------------------------------------------------------

template<typename S>
using TetraP1P0AugmentedThreeFieldStrainEnergyFunction 
      = AugmentedThreeFieldStrainEnergyFunction<S, Tet4LagrangePolynomialShapeFunction,
                                                ConstantShapeFunction, ConstantShapeFunction, ConstantShapeFunction,
                                                TetrahedralQuadratureRule<double,Eigen::Vector3d>>;

typedef MixedFiniteElement<TetraP1P0AugmentedThreeFieldStrainEnergyFunction> QuasiIncompressibleTetraP1P0;

//------------------------------------------------------------------------------

#endif

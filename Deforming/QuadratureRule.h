#ifndef _QUADRATURE_RULE_H_
#define _QUADRATURE_RULE_H_

//------------------------------------------------------------------------------

// multivariate quadrature rule
template<typename T, int dim, typename VecType = T[dim]>
class CubatureRule
{
  public:
    virtual int getN() = 0;
    virtual void getAbscissaAndWeight(int i, VecType& xi, T& weight) = 0;
};

//------------------------------------------------------------------------------

template<typename T, typename VecType = T[3]>
class TetrahedralQuadratureRule : public CubatureRule<T, 3, VecType>
{
    int n;
  public:
    TetrahedralQuadratureRule(int n);

    int getN();
    void getAbscissaAndWeight(int i, VecType& xi, T& weight);
};

//------------------------------------------------------------------------------

#ifdef TEMPLATE_FIX
  #include "QuadratureRule.C"
#endif
#endif

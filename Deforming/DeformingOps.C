#include <Deforming/DeformingOps.h>
#include <Deforming/QuasiIncompressibleTetraP1P0.h>
#include <Elem.h>
#include <IoData.h>
#include <Node.h>
#include <Vector.h>

//------------------------------------------------------------------------------

int DeformingOps::numStates(ElementData& data) {
  switch(data.type) {
    case ElementData::QUASI_INCOMPRESSIBLE :
      return QuasiIncompressibleTetraP1P0::numStates();
  }
  return 0;
}

//------------------------------------------------------------------------------

void DeformingOps::initStates(double *states, ElementData& data) {
  switch(data.type) {
    case ElementData::QUASI_INCOMPRESSIBLE : 
      QuasiIncompressibleTetraP1P0::initStates(states);
      break;
  }
}

//------------------------------------------------------------------------------

void DeformingOps::updateInternalVariables(Elem& E, double *states, SVec<double, 3>& U, ElementData& data) {
  int nd[4] = {0, 1, 2, 3};
  double u[12];
  for(int i = 0; i < 4; ++i) {
    u[3*i+0] = U[E.nodeNum(i)][0];
    u[3*i+1] = U[E.nodeNum(i)][1];
    u[3*i+2] = U[E.nodeNum(i)][2];
  }

  switch(data.type) {
    case ElementData::QUASI_INCOMPRESSIBLE : {
      QuasiIncompressibleTetraP1P0 e(nd, data);
      e.updateInternalVariables(u, states);
    } break;
  }
}

//------------------------------------------------------------------------------

void DeformingOps::downdateInternalVariables(Elem& E, double *states, SVec<double, 3>& U, ElementData& data) {
  int nd[4] = {0, 1, 2, 3};
  double u[12];
  for(int i = 0; i < 4; ++i) {
    u[3*i+0] = -U[E.nodeNum(i)][0];
    u[3*i+1] = -U[E.nodeNum(i)][1];
    u[3*i+2] = -U[E.nodeNum(i)][2];
  }

  switch(data.type) {
    case ElementData::QUASI_INCOMPRESSIBLE : {
      QuasiIncompressibleTetraP1P0 e(nd, data);
      e.updateInternalVariables(u, states);
    } break;
  }
}

//------------------------------------------------------------------------------

void DeformingOps::checkSolution(Elem& E, double *states, SVec<double, 3>& X, NodeSet& X0, ElementData& data) {
  double V;
  switch(data.type) {
    case ElementData::QUASI_INCOMPRESSIBLE : {
      QuasiIncompressibleTetraP1P0 e(E.nodeNum(), data);
      V = e.getStrainEnergy(X0, X, states, 0);
    } break;
  }
}

//------------------------------------------------------------------------------

double DeformingOps::getConstraintViolation(Elem& E, double *states, ElementData& data) {
  int nd[4] = {0, 1, 2, 3};
  double err;
  switch(data.type) {
    case ElementData::QUASI_INCOMPRESSIBLE : {
      QuasiIncompressibleTetraP1P0 e(nd, data);
      err = e.getConstraintViolation(states);
    } break;
  }
  return err;
}

//------------------------------------------------------------------------------

void DeformingOps::updateMultipliers(Elem& E, double *states, ElementData& data) {
  int nd[4] = {0, 1, 2, 3};
  switch(data.type) {
    case ElementData::QUASI_INCOMPRESSIBLE : {
      QuasiIncompressibleTetraP1P0 e(nd, data);
      e.updateMultipliers(states);
    } break;
  }
}

//------------------------------------------------------------------------------

#ifndef _MIXED_FINITE_ELEMENT_H_
#define _MIXED_FINITE_ELEMENT_H_

#include <Eigen/Core>

//------------------------------------------------------------------------------

class ElementData;
class NodeSet;
template<class Scalar> class DenseMat;
template<class Scalar, int dim> class SVec;

//------------------------------------------------------------------------------

template<template <typename S> class ScalarValuedFunctionTemplate>
class MixedFiniteElement
{
    enum {
      N = ScalarValuedFunctionTemplate<double>::NumberOfGeneralizedCoordinates,
      NumberOfNodes = ScalarValuedFunctionTemplate<double>::NumberOfNodes,
      NumberOfDimensions = ScalarValuedFunctionTemplate<double>::NumberOfDimensions
    };

  protected:
    int nn[NumberOfNodes];
    int materialType;
    int quadratureOrder;
    static const int nIV = ScalarValuedFunctionTemplate<double>::NumberOfNodes2+
                           ScalarValuedFunctionTemplate<double>::NumberOfNodes3; // number of internal variables
    static const int nLM = ScalarValuedFunctionTemplate<double>::NumberOfNodes4; // number of Lagrange multiplier variables for incompressible materials
    double epsilon;   // penalty parameter for incompressible materials
    double threshold; // zero-pivot threshold for LU or SVD factorization in Schur complement
    double mu1, mu2;  // material constants

  public:
    MixedFiniteElement(int *, ElementData&);

    double getStrainEnergy(NodeSet&, SVec<double, 3>&, double *, double);
    void getStiffAndForce(NodeSet&, SVec<double, 3>&, double *, double *, double *, double);

    int numNodes() const { return ScalarValuedFunctionTemplate<double>::NumberOfNodes; }
    int numDofs() const { return ScalarValuedFunctionTemplate<double>::NumberOfGeneralizedCoordinates - nIV; }

    static int numStates();
    static void initStates(double *);

    void initMultipliers(double *);
    double getConstraintViolation(double *);
    void updateMultipliers(double *);
    void updateInternalVariables(double *, double *);

    int getQuadratureOrder() const { return quadratureOrder; };

  private:
    void getConstants(NodeSet&, const double *,
                      Eigen::Array<typename ScalarValuedFunctionTemplate<double>::ScalarConstantType,
                      ScalarValuedFunctionTemplate<double>::NumberOfScalarConstants, 1>&,
                      Eigen::Array<int, ScalarValuedFunctionTemplate<double>::NumberOfIntegerConstants, 1>&) const;
    void getInputs(NodeSet&, SVec<double, 3>&, const double *,
                   Eigen::Matrix<double,ScalarValuedFunctionTemplate<double>::NumberOfGeneralizedCoordinates,1>&) const;
};

//------------------------------------------------------------------------------

#ifdef TEMPLATE_FIX
  #include "MixedFiniteElement.C"
#endif

#endif

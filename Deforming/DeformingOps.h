#ifndef _DEFORMING_OPS_H_
#define _DEFORMING_OPS_H_

class ElementData;
class Elem;
class NodeSet;
template<class Scalar, int dim> class SVec;

//------------------------------------------------------------------------------

class DeformingOps {

public:
  static int numStates(ElementData&);
  static void initStates(double *, ElementData&);
  static void updateInternalVariables(Elem&, double *, SVec<double, 3>&, ElementData&);
  static void downdateInternalVariables(Elem&, double *, SVec<double, 3>&, ElementData&);
  static void checkSolution(Elem&, double *, SVec<double, 3>&, NodeSet&, ElementData&);
  static double getConstraintViolation(Elem&, double *, ElementData&);
  static void updateMultipliers(Elem&, double *, ElementData&);

};

//------------------------------------------------------------------------------

#endif

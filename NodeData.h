#ifndef _NODE_DATA_H_
#define _NODE_DATA_H_

#include <array>
#include <utility>
#include <vector>
#include <tuple>
#include <Vector3D.h>

template<int N>
using GenNodeData = std::vector<std::tuple<int,std::array<int,N>,std::array<double,N>,int,double>>;

typedef std::vector<std::tuple<int,std::array<int,2>,std::array<double,2>,int,double,std::vector<Vec3D>>> NodeData;

#endif

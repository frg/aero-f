#ifndef _TS_RESTART_H_
#define _TS_RESTART_H_

#include <Types.h>

class IoData;
class RefVal;
class DistGeoState;
class Communicator;
class Domain;
struct Vec3D;

template<class Scalar> class Vec;
template<class Scalar> class DistVec;
template<int dim> class DistTimeState;
template<int dim> class TsOutput;
template<int dimLS> class LevelSet;
template<int dim> class DistEmbeddedBcData;
template<int dim, class Scalar> class DistNodalGrad;

struct Vec3D;

#ifndef _VECSET_TMPL_
  #define _VECSET_TMPL_
  template<class VecType, class Scalar = double> class VecSet;
#endif

template<int dim> class ErrorDynamicsBlockVec;

//------------------------------------------------------------------------------

class TsRestart {

  RefVal *refVal;

 public:
  int index;
  int iteration;
  double etime;
  double residual;
  double energy[2];

  int mesh_output_index;
  bool mesh_refined_since_last_output;
  int_t max_node_pre_amr;
  int corrector_iteration;
  int snap_step;

  int frequency;
  double frequency_dt, prtout;

  char *solutions[3];
  char *positions[3];
  char *levelsets[3];
  char *cracking[3];
  char *fluidId[3];
  char *data[3];
  char *d2wall[3];
  char *edgeupwinddirections[3];
  char *faceupwinddirections[3];
  char *limiterphi[3];
  char *fixes[3];

  char *structPos;
  char *structVel;
  char *meshData;

  char *corotmatrix;

  char *errorAnalysisIntermediateMatrices;

  bool deleteCharStar;

 public:
  TsRestart(IoData&, RefVal *);
  ~TsRestart();

  bool toWrite(int it, bool lastIt, double t);

  void writeRestartFileNames(const char *fn);

  static void readRestartFileNames(const char *, char *, char *, char *,
                                   char *, char *, char *, char *, char *,
                                   char *, Communicator *);

  void updatePrtout(double t);

  template<int dim, int dimLS>
  void writeToDisk(int, bool, int, double, double, DistTimeState<dim>&,
                   DistGeoState&, TsOutput<dim>&, LevelSet<dimLS> *,
                   DistEmbeddedBcData<dim> *, Domain *, DistNodalGrad<dim, double> *,
                   int, bool);

  template<int dim>
  void writeDataToDisk(DistTimeState<dim>&, TsOutput<dim>&, int, bool, char*);

  void writeStructPosToDisk(int, bool, Vec<Vec3D>&, Vec3D * = nullptr);

  void writeRotationMatrixToDisk(int, bool, bool, double[3][3]);

  template<int dim>
  void writeErrorAnalysisIntermediateMatricesToDisk(int, bool, double, Domain&, VecSet<ErrorDynamicsBlockVec<dim>>&);

};

//------------------------------------------------------------------------------

#endif

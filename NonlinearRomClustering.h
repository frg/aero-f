#ifndef _NONLINEAR_ROM_CLUSTERING_H_
#define _NONLINEAR_ROM_CLUSTERING_H_

#include <VectorSet.h>
#include <VectorSetAdaptive.h>
#include <Timer.h>
#include <Eigen/Dense>
#include <Eigen/SVD>

#include "ReducedCoordNeuralNet.h"

#include <vector>
#include <utility>
#include <string>

class IoData;
class Domain;
class Communicator;
class NonlinearRom;
class Connectivity;
class Timer;

template<class Scalar> class Vec;
template<class Scalar, int dim> class SVec;
//template<int dim> class ParallelRom;

#ifdef USE_EIGEN3_DenseMat
typedef Eigen::Matrix<double,Eigen::Dynamic,Eigen::Dynamic,Eigen::RowMajor> MatrixType;
typedef Eigen::Matrix<double,Eigen::Dynamic,1,Eigen::ColMajor> VectorType;
#endif

struct ClusteringData;

//------------------------------------------------------------------------------

template<class VecType>
class Clustering {

  ClusteringData& clusteringData;
  Domain *domain;
  Communicator *com;
  IoData& ioData;

  Connectivity *snapshotClusterMap;    // for each snapshot, lists clusters it is in (including overlapping snapshots)

  enum DistanceType {EUCLIDEAN = 0, EUCLIDEAN_BOUNDARY = 1, EUCLIDEAN_FAST = 2, ANGLES = 3} distType;
  bool kmeanspp;

  void initializeClusterCenters(VecSet<VecType>&);
  void computeClusterDistances(std::vector<std::vector<double>>&, std::vector<double>&, int&);
  void computeClusterCenters(VecSet<VecType>&);
  double euclideanFull(VecType&, VecType&);
  double angleFull(VecType&, VecType&);
  void addClusterOverlap(VecSet<VecType>&, double);

public:
  Clustering(IoData&, Domain *);
  ~Clustering();

  Connectivity *getSnapshotClusterMap();

  void cleanup();
  void deleteBasisCacheData();

  void kmeansWithBounds(VecSet<VecType>&);

  double distanceFull(VecType&, VecType&);
  void closestCenterFull(VecType&, int *, int * = nullptr);
  double distanceFast(Vec<double>&, int, int);
  void distanceFastQuad(double&, Vec<double>&, int, int);
  int closestCenterFast(Vec<double>&, int);

  void loadOnlineProjectionData(NonlinearRom *);
  void loadOnlineInitialConditionData(NonlinearRom *);

  void convertProjectionData();
  void convertInitialConditionData();
  void optimizeQuadProjectedState(Vec<double> &, const Vec<double> &, const Vec<double> &, double, int, bool = true);
  void optimizeNNProjectedState(Vec<double> &, const Vec<double> &, const Vec<double> &, int);

  // Use compute_quadratic manifold
  bool quadratic;
  bool useGenMan;
  Timer *timer;

  // adaptive member functions and objects
  Eigen::MatrixXd *innerProducts;
  void preprocessAffineForOnlineProjections();
  // void preprocessICForOnlineProjections();

  // clustering related quantities
  int nClusters;
  int thisCluster;             // for usage with local supermeshes
  int snapOffset;              // for usage with local supermeshes
  VecSet<VecType> *clusterCenters;  // cluster centers (centroids excluding overlapping snapshots)
  int *snapsInCluster;         // number of snaps in each cluster (including overlapping)
  int **clusterSnapshotMap;    // for each cluster, lists snapshots to include in the cluster (including overlapping snapshots)
  int *clusterIndex;           // original cluster association for each snapshot (before any overlap)
  int **clusterNeighbors;      // for each cluster, lists neighboring clusters
  int *clusterNeighborsCount;  // stores number of neighbors for each cluster

  std::vector<std::pair<std::string, int>> originalSnapshotLocation;  // map to snapshots on disk to avoid copy

  // memory caches to reduce excessive I/O
  VecSet<VecType> **allStateBases;
  std::vector<double> **allStateSVEnergy;
  std::vector<double> **allStateRowSVEnergy;
  std::vector<int> **allStateRowAssignments;
  VecSet<VecType> **allRefStates;
  VecSet<VecType> **quadStateBasis;
  VecType **average;
  std::vector<ReducedCoordNeuralNet> reducedCoordsNNs;

  // precomputed quantities for fast distance comparisons, switches, and
  // initial condition interpolation
  std::vector<std::vector<std::vector<std::vector<double>>>> basisBasisProducts;  // [iCluster][jCluster][iBasisV of iCluster][jBasisV of jCluster]
  std::vector<std::vector<std::vector<double>>> basisUrefProducts;                // [iCluster][jCluster reference][iBasisV of iCluster]
  std::vector<std::vector<std::vector<double>>> basisUcProducts;                  // [iCluster][jCluster center][iBasisV of iCluster]
  std::vector<std::vector<double>> ucUcProducts;                                  // [iCluster][jCluster]
  std::vector<std::vector<std::vector<double>>> basisUicProducts;                 // [iCluster][jIC][iBasisV of iCluster]
  std::vector<std::vector<std::vector<double>>> ucUicDist;                        // [iCluster][jIC][kIC]

  // Additional containers required for compute_quadratic manifold
  std::vector<std::vector<std::vector<std::vector<double>>>> quadQuadProducts;    // [iCluster][jCluster][iBasisH of iCluster][jBasisH of jCluster]
  std::vector<std::vector<std::vector<std::vector<double>>>> quadBasisProducts;   // [iCluster][jCluster][iBasisH of iCluster][jBasisV of jCluster]
  std::vector<std::vector<std::vector<double>>> quadUrefProducts;                 // [iCluster][jCluster reference][iBasisH of iCluster]
  std::vector<std::vector<std::vector<double>>> quadUcProducts;                   // [iCluster][jCluster center][iBasisH of iCluster]
  std::vector<std::vector<std::vector<double>>> quadUicProducts;                  // [iCluster][jIC][iBasisH of iCluster]

  #ifdef USE_EIGEN3_DenseMat
  std::vector<std::vector<MatrixType>> quadQuadProductsMat, basisBasisProductsMat, quadBasisProductsMat;
  std::vector<std::vector<VectorType>> quadUrefProductsMat, basisUrefProductsMat;
  std::vector<std::vector<VectorType>> quadUcProductsMat, basisUcProductsMat;
  std::vector<std::vector<VectorType>> quadUicProductsMat, basisUicProductsMat;
  #endif
};

//------------------------------------------------------------------------------

#endif

#ifndef _MAT_VEC_PROD_H_
#define _MAT_VEC_PROD_H_

#include <IoData.h>
#include <DistMatrix.h>
#include <MvpMatrix.h>
#include <Types.h>

#include <iostream>
#include <type_traits>
#include <array>
#include <unordered_map>

class Domain;
class SubDomain;

template<int dim> class DistTimeState;
template<int dim> class SpaceOperator;
template<class Scalar> class DistVec;
template<class Scalar, int dim> class DistSVec;
template<class MatrixType> class DistMat;
template<class Scalar, int dim> class MvpMat;
template<class Scalar> class DenseMat;
template<class T> class CommPattern;

#ifndef _MATVECPROD_TMPL_
  #define _MATVECPROD_TMPL_
  template<int dim, int neq, class Scalar2 = double> class MatVecProd;
#endif

#ifndef _MATVECPROD2_TMPL_
  #define _MATVECPROD2_TMPL_
  template<int dim, int neq, class Scalar2 = double> class MatVecProdFD;
  template<int dim, class Scalar, int neq, class Scalar2 = double> class MatVecProdH1;
  template<int dim, class Scalar, int neq, class Scalar2 = double> class MatVecProdH2;
  template<int dim, class Scalar, class Scalar2 = double> class MatVecProdLS;
#endif

//------------------------------------------------------------------------------

template<int dim, int neq, class Scalar2>
class MatVecProd {

 protected:
  // These template functions allow specialization of the matrix-vector product.
  // In particular, we differentiate the cases where dim and neq are different
  // for turbulent problems with weak coupling.
  template<int nn, typename std::enable_if<nn!=dim>::type * = nullptr>
  DistSVec<Scalar2, dim>& Pad(DistSVec<Scalar2, nn>&, DistSVec<Scalar2, dim> *);
  template<int nn, typename std::enable_if<nn==dim>::type * = nullptr>
  DistSVec<Scalar2, dim>& Pad(DistSVec<Scalar2, nn>&, DistSVec<Scalar2, dim> *);
  template<int nn, typename std::enable_if<nn!=dim>::type * = nullptr>
  DistSVec<Scalar2, nn>& Strip(DistSVec<Scalar2, dim>&, DistSVec<Scalar2, nn> *);
  template<int nn, typename std::enable_if<nn==dim>::type * = nullptr>
  DistSVec<Scalar2, nn>& Strip(DistSVec<Scalar2, dim>&, DistSVec<Scalar2, nn> *);

 public:
  MatVecProd() {}
  virtual ~MatVecProd() {}

  virtual DistTimeState<dim> *getTimeState() { return nullptr; }

  virtual void resize1() = 0;
  virtual void resize2() = 0;

  virtual void evaluate(DistVec<double>&, DistSVec<double, dim>&, DistSVec<double, dim>&) = 0;

  virtual void apply(DistSVec<Scalar2, neq>&, DistSVec<Scalar2, neq>&) = 0;

  virtual void applyTranspose(DistSVec<Scalar2, neq>&, DistSVec<Scalar2, neq>&) = 0;

  virtual void apply(DistMat<DenseMat<Scalar2>>&, DistMat<DenseMat<Scalar2>>&,
                     std::array<CommPattern<Scalar2> *, 3>&) = 0;

  // some MVPs use internal state to help accelerate multiplication by sparse vectors
  virtual void clearState() {};

};

//------------------------------------------------------------------------------

template<int dim, int neq, class Scalar2>
class MatVecProdFD : public MatVecProd<dim, neq, Scalar2> {

  DistSVec<double, dim> Qeps;
  DistSVec<double, dim> Feps, *Feps2;
  DistSVec<double, neq> Qepstmp;
  DistSVec<double, neq> Fepstmp, *Fepstmp2;

  DistVec<double> *ctrlVol;
  DistSVec<double, neq> Q;
  DistSVec<double, neq> F;

  SpaceOperator<dim> *spaceOp;
  DistTimeState<dim> *timeState;
  Domain *domain;
  IoData& ioData;

  using MatVecProd<dim, neq, Scalar2>::Pad;
  using MatVecProd<dim, neq, Scalar2>::Strip;

  double computeEpsilon(DistSVec<double, neq>&, DistSVec<Scalar2, neq>&);

  // sparsity mask for the MVP product vectors, when using row clustering
  std::unordered_map<DistVec<bool> *, DistVec<bool> *> prodMaskMap;

  DistVec<bool> *getProdMask(DistVec<bool> *);

 public:
  MatVecProdFD(DistTimeState<dim> *, SpaceOperator<dim> *, Domain *, IoData&);
  ~MatVecProdFD();

  DistTimeState<dim> *getTimeState() {
    return timeState;
  }

  void resize1();
  void resize2();

  void evaluate(DistVec<double>&, DistSVec<double, dim>&, DistSVec<double, dim>&);

  void apply(DistSVec<Scalar2, neq>&, DistSVec<Scalar2, neq>&);

  void applyTranspose(DistSVec<Scalar2, neq>&, DistSVec<Scalar2, neq>&) {
    fprintf(stderr, "*** Error: MatVecProdFD::applyTranspose cannot be implemented\n");
    exit(-1);
  }

  void apply(DistMat<DenseMat<Scalar2>>&, DistMat<DenseMat<Scalar2>>&,
             std::array<CommPattern<Scalar2> *, 3>&) {
    fprintf(stderr, "*** Error MatVecProdFD::apply is not implemented for DenseMat arguments\n");
    exit(-1);
  }

  void addMasksToMap(DistVec<bool> *, DistSVec<Scalar2, neq>&);

  void clearState();

};

//------------------------------------------------------------------------------
// Note: the H1 (and H2) application is performed without assembly of the
//       underlying matrices and only assembly of the product vector
template<int dim, class Scalar, int neq, class Scalar2>
class MatVecProdH1 : public MatVecProd<dim, neq, Scalar2> {

 protected:
  DistMat<MvpMat<Scalar, neq>> *A;

  int numLocSub;
  SubDomain **subDomain;

  SpaceOperator<dim> *spaceOp;
  DistTimeState<dim> *timeState;
  Domain *domain;
  IoData& ioData;

  bool rowMask;  // flag for HPROM MVP optimization

  virtual void createMvp(int);

  void evaluateInternal(DistVec<double>&, DistSVec<double, dim>&, Scalar = 1, double = 1);

  void applyInternal(DistSVec<Scalar2, neq>&, DistSVec<Scalar2, neq>&,
                     DistSVec<Scalar2, neq> * = nullptr, DistVec<bool> * = nullptr);

  void applyInternalTranspose(DistSVec<Scalar2, neq>&, DistSVec<Scalar2, neq>&, DistSVec<Scalar2, neq> * = nullptr);

  void applyInternal(DistMat<DenseMat<Scalar2>>&, DistMat<DenseMat<Scalar2>>&,
                     CommPattern<Scalar2> *);

 public:
  MatVecProdH1(DistTimeState<dim> *, SpaceOperator<dim> *, Domain *, IoData&, bool = true, bool = true, bool = false);
  virtual ~MatVecProdH1();

  DistMat<MvpMat<Scalar, neq>>& getMvpMatrix() {
    return *A;
  }
  DistTimeState<dim> *getTimeState() {
    return timeState;
  }

  void resize1();
  void resize2();

  void evaluate(DistVec<double>&, DistSVec<double, dim>&, DistSVec<double, dim>&);
  void evaluate(DistVec<double>&, DistSVec<double, dim>&, DistSVec<double, dim>&, Scalar, double);

  void apply(DistSVec<Scalar2, neq>&, DistSVec<Scalar2, neq>&);

  void applyTranspose(DistSVec<Scalar2, neq>&, DistSVec<Scalar2, neq>&);

  void apply(DistMat<DenseMat<Scalar2>>&, DistMat<DenseMat<Scalar2>>&,
             std::array<CommPattern<Scalar2> *, 3>&);

};

//------------------------------------------------------------------------------
// The MatVecProdH2 class enables matrix vector product based on the exact
// Jacobian matrix. The only difference from the H1 class is the handling of the
// edge FV flux Jacobians which are functions of the edge-reconstructed states.
template<int dim, class Scalar, int neq, class Scalar2>
class MatVecProdH2 : public MatVecProdH1<dim, Scalar, neq, Scalar2> {

  // flag controlling use of exact H2 matrix construction and application vs.
  // hybrid method that was previously implemented
  const bool newH2;

  // coefficients in the linearization of the reconstructed-limited primitive states,
  // ordered as aij, aji, bij, bji
  // see Lesoinne et al. A linearized method for the frequency analysis of three
  // dimensional fluid/structure interaction problems in all flow regimes, Comp. Meth. Appl. Mech. Eng.
  // vol. 190 (2001) pp 3121-3146
  DistSVec<double, 4 * dim> *aij;

  // temporary storage buffer used in matrix-vector product when dim is not equal to neq
  // note: the implementation for weak turbulence-model coupling is not optimal
  // because it uses a vector of local length dim for the H2 part (because the
  // reconstruction is for vectors of length dim, not neq)
  DistSVec<Scalar2, dim> *pExt;

  // temporary storage buffer for new H2 MVP
  DistMat<DenseMat<Scalar2>> *recPm;

  // sparsity mask for the recP vectors in the H2 MVP, when using row clustering
  std::unordered_map<DistVec<bool> *, DistVec<bool> *> recPMaskMap;
  std::unordered_map<DistVec<bool> *, DistVec<bool> *> prodMaskMap;

  // mask for part 1 of new H2 MVP relevant for HPROM simulations
  DistVec<int> *H2mask;

  DistVec<double> *ctrlVol;
  DistSVec<double, dim> *Q;

  using MatVecProdH1<dim, Scalar, neq, Scalar2>::A;
  using MatVecProdH1<dim, Scalar, neq, Scalar2>::numLocSub;
  using MatVecProdH1<dim, Scalar, neq, Scalar2>::subDomain;

  using MatVecProdH1<dim, Scalar, neq, Scalar2>::spaceOp;
  using MatVecProdH1<dim, Scalar, neq, Scalar2>::timeState;
  using MatVecProdH1<dim, Scalar, neq, Scalar2>::domain;
  using MatVecProdH1<dim, Scalar, neq, Scalar2>::ioData;

  using MatVecProdH1<dim, Scalar, neq, Scalar2>::evaluateInternal;
  using MatVecProdH1<dim, Scalar, neq, Scalar2>::applyInternal;
  using MatVecProdH1<dim, Scalar, neq, Scalar2>::applyInternalTranspose;

  using MatVecProdH1<dim, Scalar, neq, Scalar2>::rowMask;

  using MatVecProd<dim, neq, Scalar2>::Pad;

  void createMvp(int);

  DistVec<bool> *getRecPMask(DistVec<bool> *, DistSVec<Scalar2, 3*neq>&);
  DistVec<bool> *getProdMask(DistVec<bool> *, DistSVec<Scalar2, neq>&);

 public:
  MatVecProdH2(DistTimeState<dim> *, SpaceOperator<dim> *, Domain *, IoData&, bool = true, bool = true, bool = false, bool = false);
  ~MatVecProdH2();

  void resize1();
  void resize2();

  void evaluate(DistVec<double>&, DistSVec<double, dim>&, DistSVec<double, dim>&);
  void evaluate(DistVec<double>&, DistSVec<double, dim>&, DistSVec<double, dim>&, Scalar, double);

  void apply(DistSVec<Scalar2, neq>&, DistSVec<Scalar2, neq>&);

  void applyTranspose(DistSVec<Scalar2, neq>&, DistSVec<Scalar2, neq>&);

  void apply(DistMat<DenseMat<Scalar2>>&, DistMat<DenseMat<Scalar2>>&,
             std::array<CommPattern<Scalar2> *, 3>&);

  void clearState();

};

//------------------------------------------------------------------------------
// matrix-vector product for level-set based on MatVecProdH1
template<int dim, class Scalar, class Scalar2>
class MatVecProdLS : public MatVecProd<dimLS, dimLS, Scalar2> {

  int nLevelset;

  DistMat<MvpMat<Scalar, dimLS>> *A;

  int numLocSub;
  SubDomain **subDomain;

  SpaceOperator<dim> *spaceOp;
  DistTimeState<dim> *timeState;
  Domain *domain;

 public:
  MatVecProdLS(DistTimeState<dim> *, SpaceOperator<dim> *, Domain *, int);
  virtual ~MatVecProdLS();

  DistMat<MvpMat<Scalar, dimLS>>& getMvpMatrix() {
    return *A;
  }

  void resize1();
  void resize2();

  void evaluate(DistVec<double>&, DistSVec<double, dimLS>&, DistSVec<double, dimLS>&);

  void apply(DistSVec<Scalar2, dimLS>&, DistSVec<Scalar2, dimLS>&);

  void applyTranspose(DistSVec<Scalar2, dimLS>&, DistSVec<Scalar2, dimLS>&) {
    fprintf(stderr, "*** Error: MatVecProdLS::applyTranspose not implemented\n");
    exit(-1);
  }

  void apply(DistMat<DenseMat<Scalar2>>&, DistMat<DenseMat<Scalar2>>&,
             std::array<CommPattern<Scalar2> *, 3>&) {
    fprintf(stderr, "*** Error: MatVecProdLS::apply is not implemented for DenseMat arguments\n");
    exit(-1);
  }

};

//------------------------------------------------------------------------------

#endif


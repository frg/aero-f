#ifndef _BC_APPLIER_H_
#define _BC_APPLIER_H_

#include <IoData.h>

#include <list>
#include <map>

class Domain;
class MatchNodeSet;
class SurfaceData;

template<class MatrixType> class DistMat;
template<class Scalar> class DistVec;
template<class Scalar, int dim> class DistSVec;
template<class Scalar, int dim> class MvpMat;

//------------------------------------------------------------------------------

struct ProjData {
  int node;
  double n[3];
  ProjData(int nd, double nrm[3]) {
    node = nd;
    n[0] = nrm[0];
    n[1] = nrm[1];
    n[2] = nrm[2];
  }
  void apply(double u[3]) {
    double dd = n[0] * u[0] + n[1] * u[1] + n[2] * u[2];
    u[0] -= dd * n[0];
    u[1] -= dd * n[1];
    u[2] -= dd * n[2];
  }
  void rotateNormal(double dRot[3][3]) {
    double v[3] = {n[0], n[1], n[2]};
    for(int j = 0; j < 3; j++) {
      n[j] = dRot[j][0] * v[0] + dRot[j][1] * v[1] + dRot[j][2] * v[2];
    }
  }
};

//------------------------------------------------------------------------------

class BcFcnMeshMotion {
 public:

  BcFcnMeshMotion() {}
  ~BcFcnMeshMotion() {}

  void applyToSolutionVectorTerm(int, double *, double *, double (*)[3]);
  void applyToResidualTerm(int, double *, double *, double *, double (*)[3]);

  void applyToDiagonalTerm(int, double *, double *, double *, double (*)[3]);
  void applyToOffDiagonalTerm(int, double *, double (*)[3]);
};

//------------------------------------------------------------------------------

class BCApplier {

  Domain *domain;
  IoData& ioData;
  int numLocSub;
  DistVec<int> *nodeType;
  DistSVec<double, 9> *nodeFrame;
  int **dofType;
  std::map<int, SurfaceData *>& surfaceMap;
  std::list<ProjData> *proj;
  const DefoMeshMotionData::SlidingSurfaceTreatment& slidingSurfaceTreatment;
  BcFcnMeshMotion *bcFcn;

 public:
  BCApplier(int nLocSub, Domain *dom, IoData& ioData);
  ~BCApplier();

  int **getDofType() {
    return dofType;
  }

  // Adds a projection on a node.
  // WARNING: Projections should be on orthogonal planes to guarantee commutativity.
  void addProj(int iSub, int node, double n[3]);

  void rotateProjNormal(double dRot[3][3]);

  void applyP(DistSVec<double, 3>&);

  void applyPt(DistSVec<double, 3>& X) {
    applyP(X);
  }

  void applyD(DistSVec<double, 3>&);

  void applyDt(DistSVec<double, 3>& X) {
    applyD(X);
  }

  void applyD2(DistSVec<double, 3>&, double[3]);

  void applyPD(DistSVec<double, 3>& X) {
    applyD(X);
    applyP(X);
  }

  void applyPDt(DistSVec<double, 3>& X) {
    applyPt(X);
    applyDt(X);
  }

  void setNodeType(MatchNodeSet **);
  void setDofType(MatchNodeSet **);
  void print();

  void applyBCsToSolutionVector(DistSVec<double, 3>&, DistSVec<double, 3>&);
  void applyBCsToResidual(DistSVec<double, 3>&, DistSVec<double, 3>&, DistSVec<double, 3>&);
  void applyBCsToJacobian(DistSVec<double, 3>&, DistSVec<double, 3>&, DistMat<MvpMat<double, 3>>&);

};

//------------------------------------------------------------------------------

#endif

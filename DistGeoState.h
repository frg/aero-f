#ifndef _DIST_GEO_STATE_H_
#define _DIST_GEO_STATE_H_

#include <GeoData.h>
#include <NodeData.h>
#include <Face.h>

class DistLevelSetStructure;
class Domain;
class GeoState;
class Communicator;
class IoData;
class TimeData;
class TriangleSet;

struct Vec3D;

template<class Scalar> class DistVec;
template<class Scalar, int dim> class DistSVec;
template<class MatrixType> class DistMat;
template<class Scalar, int dim, int dim2> class RectangularSparseMat;

//------------------------------------------------------------------------------

class DistGeoState {

  int numLocSub;
  double lscale;
  double oolscale;

  GeoData data;

  IoData& ioData;
  Domain *domain;
  Communicator *com;
  GeoState **subGeoState;

  DistSVec<double, 3> *X;    // nodal positions
  DistSVec<double, 3> *Xn;   // nodal positions at time n
  DistSVec<double, 3> *Xnm1; // nodal positions at time n-1
  DistSVec<double, 3> *Xnm2; // nodal positions at time n-2
  DistSVec<double, 3> *Xdot;
  DistSVec<double, 3> *Xsave;

  DistVec<double> *ctrlVol; // note: in Aero-F this is TsDesc::A
  DistVec<double> *ctrlVol_n;
  DistVec<double> *ctrlVol_nm1;
  DistVec<double> *ctrlVol_nm2;
  DistVec<double> *ctrlVol_save;

  DistVec<double> *d2wall;
  DistVec<double> *d2wall_nmp; // wall distance at time n-p (p is the refinement period)
  DistVec<double> *d2bfwall;   // distance to body-fitted wall/s, required when embedded framework is used

  DistVec<double> *dd2wall;

  DistVec<Vec3D>  *edgeNorm;
  DistVec<double> *edgeNormVel;
  DistVec<Vec3D>  *edgeNorm_nm1;
  DistVec<double> *edgeNormVel_nm1;
  DistVec<Vec3D>  *edgeNorm_nm2;
  DistVec<double> *edgeNormVel_nm2;

  // There is one normal per face node (however for triangular nodes only one normal
  // is stored as they are all equal). The mapping to the face normals is stored with
  // each face (normNum in Face). The total number of face normals is stored in
  // the faceset (numFaceNorms in FaceSet).
  DistVec<Vec3D>  *faceNorm;
  DistVec<double> *faceNormVel;
  DistVec<Vec3D>  *faceNorm_nm1;
  DistVec<double> *faceNormVel_nm1;
  DistVec<Vec3D>  *faceNorm_nm2;
  DistVec<double> *faceNormVel_nm2;

  // upwinding directions for rotated Riemann flux
  DistSVec<double, 6> *edgeUpwindDirections;
  DistSVec<double, Face::MaxNumNd * 3> *faceUpwindDirections;
  TriangleSet *triangleSet;

  //---------------------- sensitivity analysis --------------------------------
  DistSVec<double, 3> *dX;
  DistSVec<double, 3> *dXdot;
  DistVec<double> *dCtrlVol;
  DistVec<Vec3D> *dEdgeNorm;
  DistVec<Vec3D> *dFaceNorm;
  DistVec<double> *dEdgeNormVel;
  DistVec<double> *dFaceNormVel;
  //----------------------------------------------------------------------------

 public:
  DistGeoState(IoData&, Domain *);
  ~DistGeoState();

  void resize1(NodeData *);
  void resize2(bool, TimeData&);

  void setup(IoData&, const char *, TimeData&, double * = 0, double * = 0);
  void setup1(IoData&, const char *);
  void setup2(IoData&, TimeData&, double * = 0, double * = 0);

  void compute(TimeData&, DistSVec<double, 3>&, bool = true);
  void interpolate(double, double, DistSVec<double, 3>&);
  void update();

  void reset(DistSVec<double, 3>&);

  void writeToDisk(char *);
  void writeWallDistanceToDisk(char *);
  void writeEdgeUpwindDirectionsToDisk(char *);
  void writeFaceUpwindDirectionsToDisk(char *);

  void initializePredictor();
  void initializeCorrector();

  GeoState& operator()(int i) const {
    return *subGeoState[i];
  }

  int getConfig() const {
    return data.config;
  }

  int getConfigSA() const {
    return data.configSA;
  }

  bool useN() const {
    return data.use_n;
  }

  bool useNm1() const {
    return data.use_nm1;
  }

  bool useNm2() const {
    return data.use_nm2;
  }

  bool useSave() const {
    return data.use_save;
  }

  DistSVec<double, 3>& getPositionVector() const {
    return *X;
  }

  DistSVec<double, 3>& getXn() const {
    return *Xn;
  }

  DistSVec<double, 3>& getXnm1() const {
    return *Xnm1;
  }

  DistSVec<double, 3>& getXnm2() const {
    return *Xnm2;
  }

  DistSVec<double, 3>& getXsave() const {
    return *Xsave;
  }

  DistSVec<double, 3>& getVelocityVector() const {
    return *Xdot;
  }

  DistVec<double>& getCtrlVol() const {
    return *ctrlVol;
  }

  DistVec<Vec3D>& getFaceNormal() const {
    return *faceNorm;
  }

  DistVec<double>& getFaceNormalVel() const {
    return *faceNormVel;
  }

  DistVec<Vec3D>& getEdgeNormal() const {
    return *edgeNorm;
  }

  DistVec<double>& getEdgeNormalVel() const {
    return *edgeNormVel;
  }

  DistVec<double>& getDistanceToWall() const {
    return *d2wall;
  }

  DistVec<double>& getdDistanceToWall() const {
    return *dd2wall;
  }

  DistVec<double> *getDistanceToWallnmp() const {
    return d2wall_nmp;
  }

  DistVec<double> *getDistanceToBodyFittedWall() const {
    return d2bfwall;
  }

  DistSVec<double, 3>& getdPositionVector() const {
    return *dX;
  }

  DistVec<double>& getdCtrlVol() const {
    return *dCtrlVol;
  }

  DistVec<Vec3D>& getdEdgeNormal() const {
    return *dEdgeNorm;
  }

  DistVec<double>& getdEdgeNormalVel() const {
    return *dEdgeNormVel;
  }

  DistSVec<double, 6> *getEdgeUpwindDirections() const {
    return edgeUpwindDirections;
  }

  DistSVec<double, Face::MaxNumNd * 3> *getFaceUpwindDirections() const {
    return faceUpwindDirections;
  }

  bool preComputeUpwindDirections();

  void setPositionVector(const DistSVec<double, 3>&);

  void setCtrlVol(const DistVec<double>&);

  void updateDistanceToWall(const DistVec<double>&);

  void computeUpwindDirections();
  void recomputeUpwindDirections(DistLevelSetStructure *);

  void fsoInitialize();

  void initializeDerivatives();

  void computeDerivatives(DistSVec<double, 3>&, DistSVec<double, 3>&);

  void computeDerivatives(DistMat<RectangularSparseMat<double, 3, 3>>&, DistMat<RectangularSparseMat<double, 3, 3>>&,
                          DistMat<RectangularSparseMat<double, 3, 1>>&);

  void computeTransposeDerivatives(DistMat<RectangularSparseMat<double, 3, 3>>&, DistMat<RectangularSparseMat<double, 3, 3>>&,
                                   DistMat<RectangularSparseMat<double, 3, 1>>&, DistSVec<double, 3>&);

  void computeDerivativeOperators(DistMat<RectangularSparseMat<double, 3, 3>>&, DistMat<RectangularSparseMat<double, 3, 3>>&,
                                  DistMat<RectangularSparseMat<double, 3, 1>>&);

};

//------------------------------------------------------------------------------

#endif

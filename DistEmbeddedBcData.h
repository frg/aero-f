#ifndef _DIST_EMBEDDED_BC_DATA_
#define _DIST_EMBEDDED_BC_DATA_

#include <EmbeddedBcData.h>
#include <NodeData.h>

#include <vector>
#include <set>

class Communicator;
class DistLevelSetStructure;
class Domain;
class DynamicNodalTransfer;
class FluidSelector;
class IoData;
class VarFcn;
class DistGeoState;

template<class Scalar> class DistVec;
template<int dim> class GhostPoint;
template<int dim> class DistExactRiemannSolver;
template<int dim> class GhostPointReflectionHandler;
template<int dim> class DistBcData;

//------------------------------------------------------------------------------

template<int dim>
class DistEmbeddedBcData {
  Domain *dom;
  DistBcData<dim> *bcData;

 public:
  DistLevelSetStructure *distLSS;
  DistVec<int> *fluidId;
  DistVec<GhostPoint<dim>*> *ghostPoints;
  bool isViscous;
  DistExactRiemannSolver<dim> *riemann;
  EmbeddedFrameworkData& embed;
  std::vector<EmbeddedBcData<dim>> embData;
  DistSVec<double, dim> *leftRiemannAverage, *rightRiemannAverage;
  DistVec<double> *leftRiemannSum, *rightRiemannSum;
  GhostPointReflectionHandler<dim> *gpReflecHandler;
  std::vector<std::vector<std::set<int>>> globalNeighbors;
  bool recomputeGlobalNeighbors;
  FluidSelector *fluidSelector;
  MultiFluidData& mf;
  DistSVec<double, dimLS> *Phi;

  DistEmbeddedBcData(IoData&, Domain *, VarFcn *, Communicator *, DynamicNodalTransfer *, DistVec<int> *, DistGeoState *, DistBcData<dim> *);
  ~DistEmbeddedBcData();

  void initialize();
  void resize1(NodeData *);
  void resize2();

  EmbeddedBcData<dim>& operator()(int);

  void updateFluidId();
  void writeToDisk(const char *);

};

//------------------------------------------------------------------------------

#endif

#ifndef _FEM_EQUATION_TERM_NS_H_
#define _FEM_EQUATION_TERM_NS_H_

#include <FemEquationTerm.h>

class IoData;
class VarFcn;
class NavierStokesTerm;

struct Vec3D;

//------------------------------------------------------------------------------

class FemEquationTermNS : public FemEquationTerm {

 protected:
  NavierStokesTerm *ns;
  int integrationType;

 public:
  FemEquationTermNS(IoData&, VarFcn *);
  ~FemEquationTermNS();

  void rstVar(IoData&);

  NavierStokesTerm *getNavierStokesTerm() {
    return ns;
  }

  bool doesSourceTermExist() {
    return false;
  }

  double computeViscosity(double *, int);

  void computeJacobianViscosity(double *, double *, int);

  double computeViscousTimeStep(double *, double *, int);

  double computeDerivativeOfViscousTimeStep(double *, double *, double *, double *, double, int);

  bool computeVolumeTerm(double, double[4][3], double[4][3], double[4], double *[4], int, int, int,
                         Vec3D&, double *, int, double, double *, double *, double *, double = 1.0);

  bool computeDerivativeOfVolumeTerm(double, double, double[4][3], double[4][3], double[4][3],
                                     double[4][3], double[4], double[4], double *[4], double *[4],
                                     double, int, int, int,
                                     Vec3D&, Vec3D&, double *, int, double, double,
                                     double *, double *, double *, double = 1.0, double = 0.0);

  void computeDerivativeOperatorsOfVolumeTerm(double[4][3], double[4][3], double[4], double *[4], int,
                                              int, double[3][5][4][3], double[3][5], double[5][4][3],
                                              double[5], double[5][4][3]);

  bool computeJacobianVolumeTerm(double, double[4][3], double[4][3], double[4], double *[4], int,
                                 int, int, Vec3D&, double *, int, double, double *, double *,
                                 double *, double = 1.0);

  void computeSurfaceTerm(int, double[3], double *[3], double *, int, Vec3D&, double *);

  void computeDerivativeOfSurfaceTerm(int, double[3], double *[3], double *[3], double *, double *,
                                      int, Vec3D&, Vec3D&, double, double *);

  void computeDerivativeOperatorsOfSurfaceTerm(int, double[3], double *[3], double *, int, Vec3D&,
                                               double[][3], double[][3], double[][3], double[][3]);

  void computeJacobianSurfaceTerm(int, double[3], double *[3], double *, int, Vec3D&, double *);

  void computeSurfaceTerm(int, double[4][3], double *[4], double *, int, Vec3D&, double *);

  void computeDerivativeOfSurfaceTerm(int, double[4][3], double[4][3], double *[4], double *[4], double *,
                                      double *, int, Vec3D&, Vec3D&, double, double *);

  void computeDerivativeOperatorsOfSurfaceTerm(int, double[4][3], double *[4], double *, int, Vec3D&,
                                               double[][4][3], double[][3], double[]);

  void computeJacobianSurfaceTerm(int, double[4][3], double *[4], double *, int, Vec3D&, double *);

  void computeMaxwellSlipSurfaceTerm(int, double[4][3], double *[4], double *, int, Vec3D&, double *);

};

//------------------------------------------------------------------------------

#endif

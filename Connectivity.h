#ifndef _CONNECTIVITY_H_
#define _CONNECTIVITY_H_

#include <Types.h>

#include <algorithm>
#include <cstdio>
#include <vector>

class ElemSet;
class FaceSet;
class EdgeSet;
class BinFileHandler;

//------------------------------------------------------------------------------

class Connectivity {

  int size;           // size of pointer (minus 1)
  int numtarget;      // size of target, number of connections
  int *pointer;       // pointer to target
  int *target;        // value of the connectivity
  float *weight;      // weights of pointer (or NULL)

 protected:
  Connectivity();

  void findPseudoDiam(int *, int *, int * = 0);
  int rootLS(int, int *, int *, int&, int * = 0);

 public:
  Connectivity(ElemSet *);
  Connectivity(FaceSet *);
  Connectivity(EdgeSet *);
  Connectivity(int, int *, int *);
  Connectivity(int, std::vector<int> *);
  Connectivity(BinFileHandler&);
  ~Connectivity();

  int *operator[](int i) {
    return target + pointer[i];
  }

  const int *operator[](int i) const {
    return target + pointer[i];
  }

  // Number of parts
  int csize() {
    return size;
  }

  // Number of connections
  int numConnect() {
    return numtarget;
  }

  int *ptr() {
    return pointer;
  }

  // Beginning of ith part
  int offset(int i) {
    return pointer[i];
  }

  // Returns a unique id for connection i to j
  int offset(int i, int j) {
    for(int k = pointer[i]; k < pointer[i + 1]; ++k) {
      if(target[k] == j) {
        return k;
      }
    }
    return -1;  // no connection found
  }

  // Number of connections for part i
  int num(int i) const {
    return (i >= size) ? 0 : pointer[i + 1] - pointer[i];
  }

  int num(int i, int *mask) {
    int res = 0;
    for(int j = pointer[i]; j < pointer[i + 1]; ++j) {
      if(mask[target[j]]) {
        ++res;
      }
    }
    return res;
  }

  int numNonZeroP() {
    int res = 0;
    for(int i = 0; i < size; ++i) {
      if(pointer[i + 1] != pointer[i]) {
        ++res;
      }
    }
    return res;
  }

  template<class Map>
  void renumberTargets(Map& tmap) {
    for(int i = 0; i < numtarget; ++i) {
      target[i] = tmap[target[i]];
    }
  }

  Connectivity *reverse(float * = 0);     // Creates t->s from s->t
  Connectivity *transcon(Connectivity *); // Creates s->u from s->t and t->u
  Connectivity *merge(Connectivity *);

  // Create a rooted level structure
  int *renumByComponent(int);
  int *renumSloan(int *, int&, int * = 0);
  int *renumRCM(int *, int&, int * = 0);

  void print(FILE * = stderr);
  void write(BinFileHandler&);

  void addEdge(int, int);
  void deleteEdge(int, int);

  void sortedInsertion(std::vector<set_t<int>>&);

};

//------------------------------------------------------------------------------

inline
void
Connectivity::sortedInsertion(std::vector<set_t<int>>& v) {
  v.resize(size);
  for(int i = 0; i < size; ++i) {
    const int& jstart = pointer[i];
    const int& jend = pointer[i + 1];
    std::sort(target + jstart, target + jend);
    v[i].reserve(jend - jstart);
    for(int j = jstart; j < jend; ++j) {
      v[i].insert(v[i].end(), target[j]);
    }
  }
}

//------------------------------------------------------------------------------

#endif

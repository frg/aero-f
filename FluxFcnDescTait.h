#ifndef _FLUX_FCN_DESC_TAIT_H_
#define _FLUX_FCN_DESC_TAIT_H_

#include <FluxFcnDesc.h>
#include <VarFcnBase.h>
#include <VarFcnTait.h>
#include <VarFcnTaitSA.h>
#include <VarFcnTaitKE.h>

class IoData;

//------------------------------------------------------------------------------

class FluxFcnTaitApprJacRoeEuler3D : public FluxFcnApprJacRoeEuler3D {

 public:
  FluxFcnTaitApprJacRoeEuler3D(int rs, double gg, IoData& ioData, VarFcnTait *varFcnTait) :
    FluxFcnApprJacRoeEuler3D(ioData, rs, gg, varFcnTait) {}
  ~FluxFcnTaitApprJacRoeEuler3D() {
    vf = 0;
  }

  void compute(double, double, double *, double *, double, double *, double *, double *, bool);
  void computeJacobians(double, double, double *, double *, double, double *, double *, double *, double *, FluxFcnBase::Type, bool);

};

//------------------------------------------------------------------------------

class FluxFcnTaitWallEuler3D : public FluxFcnWallEuler3D {

 public:
  FluxFcnTaitWallEuler3D(IoData& ioData, VarFcnTait *varFcnTait) :
    FluxFcnWallEuler3D(ioData, varFcnTait) {}
  ~FluxFcnTaitWallEuler3D() {
    vf = 0;
  }

  void compute(double, double, double *, double *, double, double *, double *, double *, bool);
  void computeJacobian(double, double, double *, double *, double, double *, double *, double *, FluxFcnBase::Type, bool);

};

//------------------------------------------------------------------------------

class FluxFcnTaitGhidagliaEuler3D : public FluxFcnGhidagliaEuler3D {

 public:
  FluxFcnTaitGhidagliaEuler3D(IoData& ioData, VarFcnTait *varFcnTait) :
    FluxFcnGhidagliaEuler3D(varFcnTait) {}
  ~FluxFcnTaitGhidagliaEuler3D() {
    vf = 0;
  }

  void compute(double, double, double *, double *, double, double *, double *, double *, bool);

};

//------------------------------------------------------------------------------

class FluxFcnTaitInternalInflowEuler3D : public FluxFcnInternalInflowEuler3D {

 public:
  FluxFcnTaitInternalInflowEuler3D(IoData& ioData, VarFcnTait *varFcnTait) :
    FluxFcnInternalInflowEuler3D(varFcnTait) {}
  ~FluxFcnTaitInternalInflowEuler3D() {
    vf = 0;
  }

  void compute(double, double, double *, double *, double, double *, double *, double *, bool);
  void computeJacobian(double, double, double *, double *, double, double *, double *, double *, FluxFcnBase::Type, bool);

};

//------------------------------------------------------------------------------

class FluxFcnTaitInternalOutflowEuler3D : public FluxFcnInternalOutflowEuler3D {

 public:
  FluxFcnTaitInternalOutflowEuler3D(IoData& ioData, VarFcnTait *varFcnTait) :
    FluxFcnInternalOutflowEuler3D(varFcnTait) {}
  ~FluxFcnTaitInternalOutflowEuler3D() {
    vf = 0;
  }

  void compute(double, double, double *, double *, double, double *, double *, double *, bool);
  void computeJacobian(double, double, double *, double *, double, double *, double *, double *, FluxFcnBase::Type, bool);

};

//------------------------------------------------------------------------------

class FluxFcnTaitApprJacRoeSA3D : public FluxFcnApprJacRoeSA3D {

 public:
  FluxFcnTaitApprJacRoeSA3D(int rs, double gg, IoData& ioData, VarFcnTaitSA *varFcnTait) :
    FluxFcnApprJacRoeSA3D(ioData, rs, gg, varFcnTait) {}
  ~FluxFcnTaitApprJacRoeSA3D() {
    vf = 0;
  }

  void compute(double, double, double *, double *, double, double *, double *, double *, bool);
  void computeJacobians(double, double, double *, double *, double, double *, double *, double *, double *, FluxFcnBase::Type, bool);

};

//------------------------------------------------------------------------------

class FluxFcnTaitWallSA3D : public FluxFcnWallSA3D {

 public:
  FluxFcnTaitWallSA3D(IoData& ioData, VarFcnTaitSA *varFcnTait) :
    FluxFcnWallSA3D(varFcnTait) {}
  ~FluxFcnTaitWallSA3D() {
    vf = 0;
  }

  void compute(double, double, double *, double *, double, double *, double *, double *, bool);
  void computeJacobian(double, double, double *, double *, double, double *, double *, double *, FluxFcnBase::Type, bool);

};

//------------------------------------------------------------------------------

class FluxFcnTaitGhidagliaSA3D : public FluxFcnGhidagliaSA3D {

 public:
  FluxFcnTaitGhidagliaSA3D(IoData& ioData, VarFcnTaitSA *varFcnTait) :
    FluxFcnGhidagliaSA3D(varFcnTait) {}
  ~FluxFcnTaitGhidagliaSA3D() {
    vf = 0;
  }

  void compute(double, double, double *, double *, double, double *, double *, double *, bool);

};

//------------------------------------------------------------------------------

class FluxFcnTaitRoeSAturb3D : public FluxFcnRoeSAturb3D {

 public:
  FluxFcnTaitRoeSAturb3D(double gg, IoData& ioData, VarFcnTaitSA *varFcnTait) :
    FluxFcnRoeSAturb3D(ioData, gg, varFcnTait) {}
  ~FluxFcnTaitRoeSAturb3D() {
    vf = 0;
  }

  void compute(double, double, double *, double *n, double nv, double *vl, double *vr, double *f, bool) {}
  void computeJacobians(double, double, double *, double *, double, double *, double *, double *, double *, FluxFcnBase::Type, bool);

};

//------------------------------------------------------------------------------

class FluxFcnTaitWallSAturb3D : public FluxFcnWallSAturb3D {

 public:
  FluxFcnTaitWallSAturb3D(IoData& ioData, VarFcnTaitSA *varFcnTait) :
    FluxFcnWallSAturb3D(varFcnTait) {}
  ~FluxFcnTaitWallSAturb3D() {
    vf = 0;
  }

  void compute(double, double, double *, double *n, double nv, double *vl, double *vr, double *f, bool) {}
  void computeJacobian(double, double, double *, double *, double, double *, double *, double *, FluxFcnBase::Type, bool);

};

//------------------------------------------------------------------------------

class FluxFcnTaitGhidagliaSAturb3D : public FluxFcnGhidagliaSAturb3D {

 public:
  FluxFcnTaitGhidagliaSAturb3D(IoData& ioData, VarFcnTaitSA *varFcnTait) :
    FluxFcnGhidagliaSAturb3D(varFcnTait) {}
  ~FluxFcnTaitGhidagliaSAturb3D() {
    vf = 0;
  }

  void compute(double, double, double *, double *n, double nv, double *vl, double *vr, double *f, bool) {}
  void computeJacobian(double, double, double *, double *, double, double *, double *, double *, FluxFcnBase::Type, bool);

};

//------------------------------------------------------------------------------

class FluxFcnTaitApprJacRoeKE3D : public FluxFcnApprJacRoeKE3D {

 public:
  FluxFcnTaitApprJacRoeKE3D(int rs, double gg, IoData& ioData, VarFcnTaitKE *varFcnTait) :
    FluxFcnApprJacRoeKE3D(ioData, rs, gg, varFcnTait) {}
  ~FluxFcnTaitApprJacRoeKE3D() {
    vf = 0;
  }

  void compute(double, double, double *, double *, double, double *, double *, double *, bool);
  void computeJacobians(double, double, double *, double *, double, double *, double *, double *, double *, FluxFcnBase::Type, bool);

};

//------------------------------------------------------------------------------

class FluxFcnTaitWallKE3D : public FluxFcnWallKE3D {

 public:
  FluxFcnTaitWallKE3D(IoData& ioData, VarFcnTaitKE *varFcnTait) :
    FluxFcnWallKE3D(varFcnTait) {}
  ~FluxFcnTaitWallKE3D() {
    vf = 0;
  }

  void compute(double, double, double *, double *, double, double *, double *, double *, bool);
  void computeJacobian(double, double, double *, double *, double, double *, double *, double *, FluxFcnBase::Type, bool);

};

//------------------------------------------------------------------------------

class FluxFcnTaitGhidagliaKE3D : public FluxFcnGhidagliaKE3D {

 public:
  FluxFcnTaitGhidagliaKE3D(IoData& ioData, VarFcnTaitKE *varFcnTait) :
    FluxFcnGhidagliaKE3D(varFcnTait) {}
  ~FluxFcnTaitGhidagliaKE3D() {
    vf = 0;
  }

  void compute(double, double, double *, double *, double, double *, double *, double *, bool);

};

//------------------------------------------------------------------------------

class FluxFcnTaitRoeKEturb3D : public FluxFcnRoeKEturb3D {

 public:
  FluxFcnTaitRoeKEturb3D(double gg, IoData& ioData, VarFcnTaitKE *varFcnTait) :
    FluxFcnRoeKEturb3D(ioData, gg, varFcnTait) {}
  ~FluxFcnTaitRoeKEturb3D() {
    vf = 0;
  }

  void compute(double, double, double *, double *n, double nv, double *vl, double *vr, double *f, bool) {}
  void computeJacobians(double, double, double *, double *, double, double *, double *, double *, double *, FluxFcnBase::Type, bool);

};

//------------------------------------------------------------------------------

class FluxFcnTaitWallKEturb3D : public FluxFcnWallKEturb3D {

 public:
  FluxFcnTaitWallKEturb3D(IoData& ioData, VarFcnTaitKE *varFcnTait) :
    FluxFcnWallKEturb3D(varFcnTait) {}
  ~FluxFcnTaitWallKEturb3D() {
    vf = 0;
  }

  void compute(double, double, double *, double *n, double nv, double *vl, double *vr, double *f, bool) {}
  void computeJacobian(double, double, double *, double *, double, double *, double *, double *, FluxFcnBase::Type, bool);

};

//------------------------------------------------------------------------------

class FluxFcnTaitGhidagliaKEturb3D : public FluxFcnGhidagliaKEturb3D {

 public:
  FluxFcnTaitGhidagliaKEturb3D(IoData& ioData, VarFcnTaitKE *varFcnTait) :
    FluxFcnGhidagliaKEturb3D(varFcnTait) {}
  ~FluxFcnTaitGhidagliaKEturb3D() {
    vf = 0;
  }

  void compute(double, double, double *, double *n, double nv, double *vl, double *vr, double *f, bool) {}
  void computeJacobian(double, double, double *, double *, double, double *, double *, double *, FluxFcnBase::Type, bool);

};

//------------------------------------------------------------------------------

#endif

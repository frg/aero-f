#ifndef _SPARSE_MATRIX_H_
#define _SPARSE_MATRIX_H_

#include <GenMatrix.h>

#include <string>

template<class Scalar> class Vec;
template<class Scalar, int Dim> class SVec;

#ifdef USE_EIGEN3
// #define USE_EIGEN3_SparseMat
#endif

#ifdef USE_EIGEN3_SparseMat
#include <Eigen/Sparse>
#endif

//------------------------------------------------------------------------------

template<class Scalar, int Dim>
class SparseMat : public GenMat<Scalar, Dim> {

  using GenMat<Scalar, Dim>::a;
  using GenMat<Scalar, Dim>::dim;

  int n;                    // number of unknown blocks
  int nnz;                  // number of non-zero blocks

  Vec<int> ia;              // pointer to rows
  Vec<int> ja;              // pointer to columns
  Vec<int> ju;              // pointer to U factors in LU decomposition

  Vec<int> renum;           // foward unknown renumbering (renum[old] = new)
  Vec<int> order;           // reverse unknown renumbering (order[new] = old)

#ifdef USE_EIGEN3_SparseMat
  // if fill = -1, will perform a complete LU using Eigen
  Eigen::SparseMatrix<Scalar> A;
  Eigen::SparseLU<Eigen::SparseMatrix<Scalar>> solver;
  Eigen::PermutationMatrix<Eigen::Dynamic> P;
#endif

 private:
  void rperm(int *);
  void cperm(int *);

 public:
  SparseMat(int, int, const Vec<int>&, const Vec<int>&, int);
  SparseMat(const SparseMat<Scalar, Dim>&);
  ~SparseMat() {}

  using GenMat<Scalar, Dim>::neq;
  using GenMat<Scalar, Dim>::neq2;

  SparseMat<Scalar, Dim>& operator=(const Scalar& x) {
    a = x;
    return *this;
  }

  void setZero() {
    a.setZero();
  }

  int numNonZeroBlocks() const {
    return nnz;
  }

  int numNonZeroBlocks(int i) const {
    // # non-zero blocks in row i
    const int& inew = renum[i];
    return ia[inew + 1] - ia[inew];
  }

  Scalar *coeff(int i, int j) {
    // search for col j in row i (ja[ia[i]]:ja[ia[i+1]] is sorted) -> cost = O(log(nnz[i]))
    // note: ok to query a non-allocated column (just returns 0)
    const int& inew = renum[i];
    const int& jnew = renum[j];
    int k = std::distance(ja.data() + ia[inew],
            std::lower_bound(ja.data() + ia[inew], ja.data() + ia[inew + 1], jnew));
    if(k >= ia[inew + 1] - ia[inew] || ja[ia[inew] + k] != jnew) {
      return 0;
    }
    else {
      return a.row(ia[inew] + k).data();
    }
  }

  const Vec<int>& colind() const {
    return ja;
  }

  const Vec<int>& rowptr() const {
    return ia;
  }

  void permute(int *);

  void symbolicILU(const int);

  void numericILU();

  template<class Scalar2>
  void luSolve(SVec<Scalar2, Dim>&, SVec<Scalar2, Dim>&);

  template<class Scalar2>
  void apply(SVec<Scalar2, Dim>&, SVec<Scalar2, Dim>&);

  void print(std::ostream&);

  void printRow(int, std::ostream&, int * = 0);

  void printMatrix(const std::string&);

};

//------------------------------------------------------------------------------

#endif

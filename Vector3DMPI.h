#ifndef _VECTOR3D_MPI_H_
#define _VECTOR3D_MPI_H_

struct Vec3DMPI {
  double v[3]; // position
  double y;    // nodal value
  double state[7]; // state
};

#endif
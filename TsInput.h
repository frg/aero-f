#ifndef TS_INPUT_H
#define TS_INPUT_H

#include <string>

class IoData;

//------------------------------------------------------------------------------

struct TsInput {

  char *solutions;
  char *positions;
  char *levelsets;
  char *podFile;
  char *wallsurfacedisplac;
  char *shapederivatives;
  char *corotmatrix;
  char *altitudeSamples;

  TsInput(IoData&);
  ~TsInput();

  static char *absolutePath(const std::string& rawPath, const std::string& prefix);

 private:

  // Disallow copy and assignment
  TsInput(const TsInput&);
  TsInput& operator=(const TsInput&);

};

//------------------------------------------------------------------------------

#endif

#ifndef _KSP_PREC_H_
#define _KSP_PREC_H_

#include <IoData.h>

#include <string>

class Domain;

template<class Scalar> class DistVec;
template<class Scalar, int Dim> class DistSVec;
template<class MatrixType> class DistMat;
template<class Scalar, int Dim> class SparseMat;
template<class Scalar, int Dim> class MvpMat;

template<class Scalar, class Scalar2> struct KspPrecAssembly;

#ifndef _KSPPREC_TMPL_
  #define _KSPPREC_TMPL_
  template<int Dim, class Scalar2 = double> class KspPrec;
#endif

#ifndef _KSPPREC2_TMPL_
  #define _KSPPREC2_TMPL_
  template<int Dim, class Scalar2 = double> class IdentityPrec;
  template<class Scalar, int Dim, class Scalar2 = double> class JacobiPrec;
  template<class Scalar, int Dim, class Scalar2 = double> class IluPrec;
#endif

//------------------------------------------------------------------------------

template<int Dim, class Scalar2>
class KspPrec {

 public:
  KspPrec() {}
  virtual ~KspPrec() {}

  virtual void resize1() = 0;
  virtual void resize2() = 0;
  virtual void setup() = 0;
  virtual void apply(DistSVec<Scalar2, Dim>&, DistSVec<Scalar2, Dim>&) = 0;

};

//------------------------------------------------------------------------------

template<int Dim, class Scalar2>
class IdentityPrec : public KspPrec<Dim, Scalar2> {

 public:
  IdentityPrec() {}
  ~IdentityPrec() {}

  void resize1() {}
  void resize2() {}
  void setup() {}
  void apply(DistSVec<Scalar2, Dim>& x, DistSVec<Scalar2, Dim>& Ix);

};

//------------------------------------------------------------------------------

template<class Scalar, int Dim, class Scalar2>
class KspPrecMat {

 protected:
  DistMat<SparseMat<Scalar, Dim>> *A;
  int dim;

  int numLocSub;
  Domain *domain;

  KspPrecAssembly<Scalar, Scalar2> *matPat;
  KspPrecAssembly<Scalar, Scalar> *matPat2;

 public:
  KspPrecMat(Domain *, int);
  virtual ~KspPrecMat();

  DistMat<SparseMat<Scalar, Dim>>& getPrecMatrix() {
    return *A;
  }

  virtual void resize();

  virtual void getData(DistMat<MvpMat<Scalar2, Dim>>&, bool = true, bool = false);
  virtual void getData2(DistMat<MvpMat<Scalar, Dim>>&, bool = true, bool = false);

};

//------------------------------------------------------------------------------

template<class Scalar, int Dim, class Scalar2>
class JacobiPrec : public KspPrec<Dim, Scalar2>, public KspPrecMat<Scalar, Dim, Scalar2> {

  using KspPrecMat<Scalar, Dim, Scalar2>::A;
  using KspPrecMat<Scalar, Dim, Scalar2>::dim;
  using KspPrecMat<Scalar, Dim, Scalar2>::numLocSub;
  using KspPrecMat<Scalar, Dim, Scalar2>::domain;

  void createDiagMat(int);

 public:
  JacobiPrec(Domain *, int);
  ~JacobiPrec() {}

  void resize1();
  void resize2();
  void setup();
  void apply(DistSVec<Scalar2, Dim>&, DistSVec<Scalar2, Dim>&);

};

//------------------------------------------------------------------------------

template<class Scalar, int Dim, class Scalar2>
class IluPrec : public KspPrec<Dim, Scalar2>, public KspPrecMat<Scalar, Dim, Scalar2> {

  const char *output;

  PcData::Type type;
  int renum, fill;
  bool H2;  // sparsity pattern flag relevant when KspData::pcjacobian == H2_PREC

  using KspPrecMat<Scalar, Dim, Scalar2>::A;
  using KspPrecMat<Scalar, Dim, Scalar2>::dim;
  using KspPrecMat<Scalar, Dim, Scalar2>::numLocSub;
  using KspPrecMat<Scalar, Dim, Scalar2>::domain;

  void createILU(int);

 public:
  IluPrec(PcData&, const char *, bool, Domain *, int);
  ~IluPrec() {}

  void resize1();
  void resize2();
  void setup();
  void apply(DistSVec<Scalar2, Dim>&, DistSVec<Scalar2, Dim>&);

  void getData(DistMat<MvpMat<Scalar2, Dim>>&, bool = true, bool = false);
  void getData2(DistMat<MvpMat<Scalar, Dim>>&, bool = true, bool = false);

};

//------------------------------------------------------------------------------

#endif

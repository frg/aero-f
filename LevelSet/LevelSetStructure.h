#ifndef _LEVEL_SET_STRUCTURE_H_
#define _LEVEL_SET_STRUCTURE_H_

#include <DistVector.h>
#include <IoData.h>
#include <Vector.h>
#include <Vector3D.h>
#include <NodeData.h>
#include <Elem.h>
#include "PHYSBAM_INTERFACE.h"

#include <string>
#include <vector>

class Domain;
class DistIntersectorPhysBAM;

using PhysBAM::IntersectionResult;

/** Storing the closest point on the interface */
struct ClosestPoint {
  int mode;         // -2: unknown, -1: far from interface, 0: face, 1: edge, 2: vertex.
  int triId;        // the closest triangle id
  int tracker[2];   // for mode=0: tracker[0] = tria Id; for mode=1: the two vertices; for mode=2: the vertex
  double xi1, xi2;  // local coordinates.
  int sign;
  Vec3D x;          // closest point on the surface
  Vec3D v;          // velocity at the closest point on the surface
  Vec3D normal;
  int triId_nodeId; // used to store which node is relevant if closest is to an edge or node
                    // (is not actually the node but an indicator of which one it is)

  ClosestPoint() {
    mode = -2;
  }
  bool known() {
    return mode != -2;
  }
  bool nearInterface() {
    return mode != -1;
  }
  void print() {
    fprintf(stderr, "mode: %d, triId: %d, xi1: %e, %e, sign: %d, normal %e %e %e, closest point: %e %e %e\n",
            mode, triId, xi1, xi2, sign, normal[0], normal[1], normal[2], x[0], x[1], x[2]);
  }
};
/** Structure used to return levelset information */
struct LevelSetResult {
  double alpha;
  double xi[3];
  int (&trNodes)[3];
  Vec3D& surfaceNorm;
  Vec3D gradPhi;
  Vec3D normVel; // NOTE: this is the velocity, NOT normal velocity.
  enum BoundaryData::Type structureType;
  int faceID;
  double wallTemperature;
  bool isWallFunction;
  int heatFluxType;
  double porosity;
  int poreShape;
  double poissonRatio;
  double thickness;
  double weavingLength;
  double actuatorDiskPressureJump;
  int actuatorDiskMethod;
  double gamma;
  bool isCorrectedMethod;
  int actuatorDiskReconstructionMethod;
  double *outflowConditions;

  static int trNodes_default[3];
  static Vec3D surfaceNorm_default;

  LevelSetResult();
  LevelSetResult(const IntersectionResult<double>&,
                 DistIntersectorPhysBAM&);
  LevelSetResult(ClosestPoint&, Vec3D&,
                 DistIntersectorPhysBAM&);
};

/** Alternative structure used to return only the most commonly used levelset information */
struct PartialLevelSetResult {
  double alpha;
  Vec3D& surfaceNorm;
  int faceID;
  enum BoundaryData::Type structureType;
  double porosity;

  PartialLevelSetResult(const IntersectionResult<double>&,
                        DistIntersectorPhysBAM&);
};


/** Storing derivative of the closest point on the interface */
struct DerivativeClosestPoint {
  Vec3D dx;      // derivative of ClosestPoint::x
  Vec3D dnormal; // derivative of ClosestPoint::normal
  Vec3D dv; //derivative of ClosestPoint::v;
};

/** Abstract class for finding levelset information */
class LevelSetStructure {
 protected:
  Vec<int>& status;
  Vec<bool>& is_swept;
  Vec<bool>& is_active;
  Vec<bool>& is_occluded;
  Vec<bool>& edge_intersects;
  Vec<bool>& edge_intersects_constraint;

  Vec<double> *distance;
  Vec<double> *ddistance;
  Vec<ClosestPoint> *closest;
  Vec<DerivativeClosestPoint> *dclosest;

  Vec<double> *min_distance; // signed
  Vec<double> *dmin_distance; // signed
  Vec<double> *max_distance; // signed
  Vec<double> *chi;
  Vec<double> *dchi;
  Vec<bool> *edge_jump; // a flag for if an edge goes from one side of a structure to another with both nodes being conatinsEMB
  Vec<bool> *is_ghost;
  Vec<Elem*> *reflected_elem_ptr;
  Vec<bool> *hybrid;

 public:
  LevelSetStructure(Vec<int>& status,
                    Vec<bool>& is_swept,
                    Vec<bool>& is_active,
                    Vec<bool>& is_occluded,
                    Vec<bool>& edge_intersects,
                    Vec<bool>& edge_intersects_constraint,
                    Vec<double> *distance,
                    Vec<double> *ddistance,
                    Vec<ClosestPoint> *closest,
                    Vec<DerivativeClosestPoint> *dclosest,
                    Vec<double> *min_distance,
                    Vec<double> *dmin_distance,
                    Vec<double> *max_distance,
                    Vec<double> *chi,
                    Vec<double> *dchi,
                    Vec<bool> *hybrid,
                    Vec<bool> *is_ghost,
                    Vec<Elem*> *reflected_elem_ptr,
                    Vec<bool> *edge_jump)
    : status(status),
      is_swept(is_swept),
      is_active(is_active),
      is_occluded(is_occluded),
      edge_intersects(edge_intersects),
      edge_intersects_constraint(edge_intersects_constraint),
      distance(distance),
      ddistance(ddistance),
      closest(closest),
      dclosest(dclosest),
      min_distance(min_distance),
      dmin_distance(dmin_distance),
      max_distance(max_distance),
      chi(chi),
      dchi(dchi),
      hybrid(hybrid),
      is_ghost(is_ghost),
      reflected_elem_ptr(reflected_elem_ptr),
      edge_jump(edge_jump) {
  }

  virtual ~LevelSetStructure() {}

  /* returns the normal and normal velocity at intersection between edge ni, nj and structure
   * If ni, nj is not an edge of the fluid mesh, result is undefined. */
  virtual LevelSetResult getLevelSetDataAtEdgeCenter(double, int, bool) = 0;
  virtual PartialLevelSetResult getPartialLevelSetDataAtEdgeCenter(double, int, bool) = 0;
  virtual bool InterfacePointDist(const int, double&) = 0;
  virtual void findIntersection(int, const Vec3D&, bool, IntersectionResult<double>&) = 0;
  virtual bool withCracking() const = 0;
  virtual bool isNearInterface(double, int) const = 0;
  virtual void getClosestPoint(int, Vec3D&, Vec3D&) const = 0;
  virtual void getClosestPointAndNormal(int, Vec3D&, Vec3D&, Vec3D&) const = 0; 
  virtual void getdClosestPoint(int, Vec3D&, Vec3D&) const = 0;

  void forceOccluded(double t, int n) const {
    is_swept[n] = true;
    is_occluded[n] = true;
  }

  int fluidModel(double t, int n) const {
    return status[n];
  }

  double distToInterface(double t, int n) const {
    return (*distance)[n];
  }

  double ddistToInterface(double t, int n) const {
    return (*ddistance)[n];
  }

  double minSignedDist(double t, int n) const {
    return (*min_distance)[n];
  }

  double dminSignedDist(double t, int n) const {
    return (*dmin_distance)[n];
  }

  double maxSignedDist(double t, int n) const {
    return (*max_distance)[n];
  }

  double indicatorChi(double t, int n) const {
    return chi ? (*chi)[n] : is_active[n];
  }

  double dIndicatorChi(double t, int n) const {
    return (*dchi)[n];
  }

  bool isSwept(double t, int n) const {
    return is_swept[n];
  }

  bool isActive(double t, int n) const {
    return is_active[n];
  }

  bool isHybrid(double t, int n) const {
    return (hybrid) ? (*hybrid)[n] : false;
  }

  bool isGhost(double t, int n) const {
    return (*is_ghost)[n];
  }

  Elem *getReflectedElemPtr(int n) {
    return (*reflected_elem_ptr)[n];
  }

  void setReflectedElemPtr(int n, Elem *elem_ptr) {
    (*reflected_elem_ptr)[n] = elem_ptr;
  }

  bool edgeJumps(double t, int l) const {
    return (*edge_jump)[l];
  }

  bool isOccluded(double t, int n) const {
    return is_occluded[n];
  }

  // edgeIntersectsStructure : True for all kind of structures
  bool edgeIntersectsStructure(double t, int eij) const {
    return (edge_intersects[eij]) || edge_intersects_constraint[eij];
  }

  // edgeIntersectsWall : True for Structure Type Wall and Symmetry Plane
  bool edgeIntersectsWall(double t, int eij) const {
    return (edge_intersects[eij]);
  }

  bool edgeIntersectsConstraint(double t, int eij) const {
    return edge_intersects_constraint[eij];
  }

  virtual bool edgeIntersectsWall2(double, int, double) const = 0;

  // for intersection with specified wall surface only
  virtual bool edgeIntersectsWall(double, int, int) const = 0;
  virtual bool edgeIntersectsWall2(double, int, double, int) const = 0;

  // function for Hybrid Nodes
  virtual void nWall_node(int, Vec3D&) = 0;
  virtual void dnWall_node(int, Vec3D&) = 0;
  virtual void vWall_edge(int, Vec3D&) = 0;
  virtual void dvWall_edge(int, Vec3D*&) = 0;
  virtual bool getTwall(double&) = 0;

  Vec<int>& getStatus() {
    return status;
  }

  virtual class CrackingSurface * getCrackingSurface() {
    return NULL;
  }

  virtual int numOfFluids() = 0;

  int numEdges() {
    return edge_intersects.size();
  }
  int numNodes() {
    return status.size();
  }

  virtual double getInterfaceThickness() const = 0;

  virtual int getSurfaceID(int) = 0;

  virtual int getFaceID(int) = 0;

};

class DistLevelSetStructure {
 protected:
  DistVec<int> *status;
  DistVec<bool> *is_swept;
  DistVec<bool> *is_active;
  DistVec<bool> *is_occluded;
  DistVec<bool> *edge_intersects;
  DistVec<bool> *edge_intersects_constraint;

  DistVec<double> *distance;
  DistVec<double> *ddistance;
  DistVec<ClosestPoint> *closest;
  DistVec<DerivativeClosestPoint> *dclosest;

  DistVec<double> *min_distance;
  DistVec<double> *dmin_distance;
  DistVec<double> *max_distance;
  DistVec<double> *chi;
  DistVec<double> *dchi;
  DistVec<bool> *hybrid;
  DistVec<bool> *is_ghost;
  DistVec<Elem*> *reflected_elem_ptr;
  DistVec<bool> *edge_jump;
  DistVec<bool> *bk_is_active; // for updating swept nodes

  int numLocSub;
  int numFluid;
  std::string nodeSetName;
  std::vector<int> faceIDs;
  double *bbMin, *bbMax;

 public:
  DistLevelSetStructure()
    : status(0), is_swept(0), is_active(0), is_occluded(0), edge_intersects(0), edge_intersects_constraint(0),
      distance(0), ddistance(0), closest(0), dclosest(0), min_distance(0), dmin_distance(0), max_distance(0),
      chi(0), dchi(0), hybrid(0), is_ghost(0), reflected_elem_ptr(0), edge_jump(0), bk_is_active(0) {
  }

  virtual ~DistLevelSetStructure() {
    delete status;
    delete is_swept;
    delete is_active;
    delete is_occluded;
    delete edge_intersects;
    delete edge_intersects_constraint;

    if(distance) delete distance;
    if(ddistance) delete ddistance;
    if(closest) delete closest;
    if(dclosest) delete dclosest;

    if(min_distance) delete min_distance;
    if(dmin_distance) delete dmin_distance;
    if(max_distance) delete max_distance;
    if(chi) delete chi;
    if(dchi) delete dchi;
    if(hybrid) delete hybrid;
    if(is_ghost) delete is_ghost;
    if(reflected_elem_ptr) delete reflected_elem_ptr;
    if(edge_jump) delete edge_jump;
    if(bk_is_active) delete bk_is_active;
  }

  virtual void resize(NodeData *) = 0;

  int numOfFluids() {
    return numFluid;
  }

  void setNumOfFluids(int nf) {
    numFluid = nf;
  }

  virtual void initialize(Domain *, IoData&, DistSVec<double, 3>&, DistVec<int> * = 0,
                          DistVec<int> * = 0) = 0;

  virtual void init(char *, char *, double) = 0;

  virtual LevelSetStructure& operator()(int) const = 0;

  DistVec<int>& getStatus() const {
    return *status;
  }

  DistVec<double>& getDistance() const {
    return *distance;
  }

  DistVec<bool>& getIsSwept() const {
    return *is_swept;
  }

  DistVec<bool>& getIsActive() const {
    return *is_active;
  }

  DistVec<double>& getChi() const {
    return *chi;
  }

  DistVec<bool>& getHybrid() const {
    return *hybrid;
  }

  DistVec<bool>& getIsOccluded() const {
    return *is_occluded;
  }

  DistVec<bool>& getIntersectedEdges() const {
    return *edge_intersects;
  }

  DistVec<bool>& getIntersectedConstraintEdge() const {
    return *edge_intersects_constraint;
  }

  DistVec<ClosestPoint>& getClosestPoints() const {
    return *closest;
  }

  const std::string& getNodeSetName() const {
    return nodeSetName;
  }

  const std::vector<int>& getFaceIDs() const {
    return faceIDs;
  }

  void getBoundingBoxPointers(double*& _bbMin, double*& _bbMax) {bbMin = _bbMin; bbMax = _bbMax;}

  virtual DistVec<int> *getStatus_nmp() const = 0;
  virtual DistVec<bool> *getIsOccluded_nmp() const = 0;

  virtual void setStatus(DistVec<int>&) = 0;
  virtual void setIsOccluded(DistVec<bool>&) = 0;
  virtual void setStructPosition(const Vec<Vec3D>&) = 0;
  virtual void setStructPosition_np1(const Vec<Vec3D>&) = 0;

  virtual void updateStructure(double *, double *, int, int (*)[3] = 0, bool = false) = 0;
  virtual int recompute(DistSVec<double, 3>&, double, double, double, int, bool = false, bool = false, bool = true) = 0;
  virtual int partialRecompute(double, double, double) = 0;
  virtual void getMaskVec(DistSVec<double, 3>&, DistVec<bool>&, bool = true) = 0;
  virtual Vec3D *getSurfaceNorms() = 0;
  virtual Vec<Vec3D>& getStructPosition() = 0;
  virtual Vec3D *getdStructPosition() = 0;
  virtual Vec<Vec3D>& getStructPosition_0() = 0;
  virtual Vec<Vec3D>& getStructPosition_n() = 0;
  virtual Vec<Vec3D>& getStructPosition_np1() = 0;
  virtual Vec3D *getStructVelocity() = 0;
  virtual Vec3D *getStructVelocity_n() = 0;
  virtual Vec3D *getStructVelocity_np1() = 0;

  virtual int getNumStructNodes() = 0;
  virtual int getNumStructElems() = 0;
  virtual int getNumQuadPoints() = 0;
  virtual int (*getStructElems())[3] = 0;
  virtual int *getNodesType() = 0;
  virtual enum BoundaryData::Type getElemType(int) = 0;
  virtual int *getSurfaceID() = 0;
  virtual int *getFaceID() = 0;
  virtual int getSurfaceID(int) = 0;
  virtual int getFaceID(int) = 0;

  virtual void initializePredictor() = 0;
  virtual void initializeCorrector() = 0;

  virtual int countActive() = 0;
  virtual int countOccluded() = 0;
  virtual int countSwept() = 0;

  virtual void perturbStructureWithdXs(double) = 0;
  virtual void perturbMinDistance(double) = 0;
  virtual void perturbHybridQuantities(double) = 0;
  virtual void setdXdtAsdXs(double, double, double) = 0;
  virtual void setdXs(int, double *, double *, double *) = 0;
  virtual void computeDerivatives(double, DistSVec<double, 3>&) = 0;

  virtual double project(Vec3D&, int, double&, double&) const = 0;

};

#endif

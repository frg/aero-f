#ifndef _GHOST_POINT_H_
#define _GHOST_POINT_H_

#include <Vector3D.h>
#include <VarFcn.h>

#include <array>
#include <iostream>
#include <map>
#include <stdexcept>
#include <tuple>
#include <vector>

//------------------------------------------------------------------------------

template<int dim>
class GhostPoint {
 private:
  VarFcn *varFcn;
  int numRef;   // number of reference directions
  Vec3D v1, v2; // reference directions used for classification

 public:
  struct GhostData {
    std::array<double, dim> Vg;
    std::array<double, dim> Ws;
    int ghostTag;
    int dir;
    Vec3D ghostNormal;
    GhostData(std::array<double, dim>& _Vg, std::array<double, dim>& _Ws, int _ghostTag, int _dir,
              Vec3D& _ghostNormal) : Vg(_Vg), Ws(_Ws), ghostTag(_ghostTag), dir(_dir), ghostNormal(_ghostNormal) {}
  };
  std::vector<GhostData> ghostData;

  struct GhostState {
    std::array<double, dim> Vg;       // averaged ghost point state (rho,u,v,w,T)
    std::array<double, dim> V;        // corresponding primitive state (rho,u,v,w,P)
    std::array<double, dim> dVg;      // increment in Vg used for sensitivities (drho,du,dv,dw,dT)
    std::array<double, dim> dV;       // increment in V used for sensitivities (drho,du,dv,dw,dP)
    std::array<double, dim> V_alpha;  // state mixed with an alpha
    std::array<double, dim> Vg_alpha; // state mixed with an alpha
    std::array<double, dim> Ws;       // sum of the weights
    GhostState() {
      for(int i=0; i<dim; ++i) Vg[i] = Ws[i] = dVg[i] = dV[i] = 0.0;
    }
    void interpolateWithOtherState(double alpha, double *V_real) {
      for(int i=0; i<dim; ++i) V_alpha[i] = (1 - alpha)*V[i] + alpha*V_real[i];
    }
    void getDerivativeOfInterpolateWithOtherState(double dalpha, double *V_real) {
      for(int i=0; i<dim; ++i) V_alpha[i] = dalpha * (V_real[i] - V[i]);
    }
    void interpolateWithOtherStateVg(double alpha, double *Vg_real) {
      for(int i=0; i<dim; ++i) Vg_alpha[i] = (1 - alpha)*Vg[i] + alpha*Vg_real[i];
    }
    void getDerivativeOfInterpolateWithOtherStateVg(double dalpha, double *Vg_real) {
      for(int i=0; i<dim; ++i) Vg_alpha[i] = dalpha * (Vg_real[i] - Vg[i]);
    }
  };
  std::map<std::tuple<int,int,int>, GhostState> ghostStates;

  GhostPoint(VarFcn *, int);

  void clear();
  void addNeighbor(double *, double *, int, int, Vec3D&);
  void computeReferenceDirections();
  void reduce();

  Vec3D& getFirstReferenceDirection() { return v1; }
  Vec3D& getSecondReferenceDirection() { return v2; }
  int getOffset(int, int, Vec3D&);
  double *getPrimitiveState(int, int, Vec3D&, double = 0.0, double * = 0);
  double *getdPrimitiveState(int, int, Vec3D&);
  double *getDerivativeOfPrimitiveStateWRTalpha(int, int, Vec3D&, double, double *);
  double *getWs(int, int, Vec3D&);
  double *getVg(int, int, Vec3D&, double = 0.0, double * = 0);
  double *getDerivativeOfVgWRTalpha(int, int, Vec3D&, double, double *);
  double *getdVg(int, int, Vec3D&);
  void   setdVg(int, int, Vec3D&, double *);

 private:
  GhostPoint(const GhostPoint&);
  // GhostPoint<dim>& operator=(const GhostPoint<dim>&);

  int getColor(Vec3D&);
  typename std::map<std::tuple<int,int,int>, GhostState>::iterator findGhostState(int, int, Vec3D&);

};

//------------------------------------------------------------------------------

template<int dim>
inline
int GhostPoint<dim>::getColor(Vec3D& normal) {
  int color;
  switch(numRef) {
    default: case 0: {
      color = 0;
    } break;
    case 1: {
      color = (normal*v1 > 0) ? 1 : 0;
    } break;
    case 2: {
      if(normal*v1 > 0) {
        color = (normal*v2 > 0) ? 3 : 1;
      }
      else {
        color = (normal*v2 > 0) ? 2 : 0;
      }
    } break;
  }
  return color;
}

//------------------------------------------------------------------------------

template<int dim>
inline
typename std::map<std::tuple<int,int,int>, typename GhostPoint<dim>::GhostState>::iterator GhostPoint<dim>::findGhostState(int tag, int dir, Vec3D& normal) {
  if(ghostStates.size() == 1) {
    return ghostStates.begin();
  }
  else {
    int color = getColor(normal);
    std::tuple<int,int,int> key{tag, dir, color};
    auto it = ghostStates.find(key);
    if(it == ghostStates.end()) {
      fprintf(stderr, "specified key: %d, %d, %d \n", tag, dir, color);
      fprintf(stderr, "# available keys: %ld \n", ghostStates.size());
      fprintf(stderr, "available keys: ");
      for(auto& t : ghostStates) {
        fprintf(stderr, "key: %d, %d, %d \n",std::get<0>(t.first), std::get<1>(t.first), std::get<2>(t.first));
      }
      fprintf(stderr, "supplied normal: %f %f %f \n", normal[0], normal[1], normal[2]);
      fprintf(stderr, "ghostPoint normal 1: %f %f %f \n", v1[0], v1[1], v1[2]);
      fprintf(stderr, "ghostPoint normal 2: %f %f %f \n", v2[0], v2[1], v2[2]);
      throw std::runtime_error("ghost point has no state corresponding to specified key");
    }
    else {
      return it;
    }
  }
}

//------------------------------------------------------------------------------

template<int dim>
inline
int GhostPoint<dim>::getOffset(int tag, int dir, Vec3D& normal) {
  auto it = findGhostState(tag, dir, normal);
  return std::distance(ghostStates.begin(), it);
}

//------------------------------------------------------------------------------

template<int dim>
inline
double *GhostPoint<dim>::getPrimitiveState(int tag, int dir, Vec3D& normal, double alpha, double *V_real) {
  auto it = findGhostState(tag, dir, normal);
  if(V_real) { // hybridNodes
    it->second.interpolateWithOtherState(alpha, V_real);
    return it->second.V_alpha.data();
  }
  return it->second.V.data();
}

//------------------------------------------------------------------------------

template<int dim>
inline
double *GhostPoint<dim>::getdPrimitiveState(int tag, int dir, Vec3D& normal) {
  auto it = findGhostState(tag, dir, normal);
  return it->second.dV.data();
}

//------------------------------------------------------------------------------

template<int dim>
inline
double *GhostPoint<dim>::getDerivativeOfPrimitiveStateWRTalpha(int tag, int dir, Vec3D& normal, double dalpha, double *V_real) {
  auto it = findGhostState(tag, dir, normal);
  it->second.getDerivativeOfInterpolateWithOtherState(dalpha, V_real);
  return it->second.V_alpha.data();
}

//------------------------------------------------------------------------------

template<int dim>
inline
double *GhostPoint<dim>::getWs(int tag, int dir, Vec3D& normal) {
  auto it = findGhostState(tag, dir, normal);
  return it->second.Ws.data();
}

//------------------------------------------------------------------------------

template<int dim>
inline
double *GhostPoint<dim>::getVg(int tag, int dir, Vec3D& normal, double alpha, double *Vg_real) {
  auto it = findGhostState(tag, dir, normal);
  if(Vg_real) { // hybridNodes
    it->second.interpolateWithOtherStateVg(alpha, Vg_real);
    return it->second.Vg_alpha.data();
  }
  return it->second.Vg.data();
}

//------------------------------------------------------------------------------

template<int dim>
inline
double *GhostPoint<dim>::getDerivativeOfVgWRTalpha(int tag, int dir, Vec3D& normal, double dalpha, double *Vg_real) {
  auto it = findGhostState(tag, dir, normal);
  it->second.getDerivativeOfInterpolateWithOtherStateVg(dalpha, Vg_real);
  return it->second.Vg_alpha.data();
}

//------------------------------------------------------------------------------

template<int dim>
inline
double *GhostPoint<dim>::getdVg(int tag, int dir, Vec3D& normal) {
  auto it = findGhostState(tag, dir, normal);
  return it->second.dVg.data();
}

//------------------------------------------------------------------------------

template<int dim>
inline
void GhostPoint<dim>::setdVg(int tag, int dir, Vec3D& normal, double *dVg) {
  auto it = findGhostState(tag, dir, normal);
  for(int i = 0; i < dim; ++ i) {
    it->second.dVg[i] = dVg[i];
    if(i != 4) it->second.dV[i] = dVg[i];
  }
  varFcn->getdV4fromdVg(it->second.Vg.data(), dVg, it->second.dV[4], tag);
}

//------------------------------------------------------------------------------

#endif

#ifndef __REINITIALIZE_DISTANCE_TO_WALL_H__
#define __REINITIALIZE_DISTANCE_TO_WALL_H__

#include <DistVector.h>
#include <IoData.h>

// note: the following macro can be used to make the wall distance to be consistent
//       with the intersector state even when subcycling occurs; currently this is
//       only tested with the ClosestPoint method
//#define CONSISTENT_WALL_DISTANCE

class Domain;
class SubDomain;
class DistGeoState;
class DistLevelSetStructure;
class LevelSetStructure;
class TriangleSet;

//------------------------------------------------------------------------------

class ReinitializeDistanceToWall {
  IoData& iod;
  Domain& dom;
  DistVec<double> d2wall;
  double d2wallmax;
  DistVec<int> sortedNodes, tag;
  int *nSortedNodes, *firstCheckedNode;
  int faceID;
  bool updateGeoState;

  bool usePredictors;
  bool computeInactive;
  std::vector<double> predictorTime;
  DistVec<double> *d2wnm1, *d2wnm2;
  DistVec<int> *predictorTag;
  DistVec<double> *sensitivSA;
  double scaleSA, scaleD2W;
  int nSkip;

  DistSVec<std::pair<int, double>, 3> *dd2wall_doppnode;
  DistVec<double> *dd2wall;
  enum MarchingMethodObjective {D2WALL, DD2WALL, DERIVATIVEOPERATORS};

  TriangleSet *triangleSet;

 private:
  void PseudoFastMarchingMethod(DistLevelSetStructure&, DistSVec<double, 3>&);
  void ClosestPointMethod(DistLevelSetStructure&, DistSVec<double, 3>&);
  void IterativeMethodUpdate(DistLevelSetStructure&, DistSVec<double, 3>&, MarchingMethodObjective = D2WALL);

  int UpdateDistanceError(DistLevelSetStructure&, DistGeoState&, const double);

 public:
  ReinitializeDistanceToWall(IoData&, Domain&, int = -1, bool = true);
  ~ReinitializeDistanceToWall();

  void resize(NodeData *);

  int ComputeWallFunction(DistLevelSetStructure&, DistGeoState&, const double, bool);

  void ComputeDerivativeOfWallFunction(DistLevelSetStructure&, DistGeoState&);

  void ComputeDerivativeOperatorsOfWallFunction(DistLevelSetStructure&, DistGeoState&);

  template<int dim>
  void ReinitializePredictors(DistSVec<double, dim>&);

  bool useFreqAdaptation() const {
    return usePredictors;
  }
  DistVec<double> *getd2walln() {
    return &d2wall;
  }
  DistVec<double> *getdd2walln() {
    return dd2wall;
  }
  DistVec<double> *getd2wallnm1() {
    return d2wnm1;
  }
  DistVec<double> *getd2wallnm2() {
    return d2wnm2;
  }
  DistVec<double> *getSASensitiv() {
    return sensitivSA;
  }

};

//------------------------------------------------------------------------------

#endif

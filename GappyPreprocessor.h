#ifndef _GAPPY_PREPROCESSOR_H_
#define _GAPPY_PREPROCESSOR_H_

#include <vector>
#include <set>

#include <VectorSet.h>

class IoData;
class Domain;
class Communicator;
class SubDomain;
class NonlinearRom;

template<class Scalar, int dim> class DistSVec;
template<class VecType> class Clustering;

struct GappyPODData;

//----------------------------------------------------------------------------

class GappyPreprocessor {

  Domain *domain;
  Communicator *com;
  int numLocSub;
  NonlinearRom *nlrom;

  GappyPODData& gappyData;

 private:
  template<int dim>
  void readGreedyData(int&, VecSet<DistSVec<double, dim>>&, std::vector<int>&,
                      std::vector<int>&, Clustering<DistSVec<double, dim>> *);

  void setupTargetRegions(int, int&, int&, int&, std::vector<std::set<int>>&,
                          std::vector<std::set<int>>&, std::vector<std::set<int>>&);

  template<int dim>
  void findMaxError(std::vector<std::set<int>>&, VecSet<DistSVec<double, dim>>&,
                    int, int&, int&, int&);

  template<int dim>
  void findMaxErrorMask(std::vector<std::set<int>>&, std::vector<std::set<int>>&,
                        std::vector<std::set<int>>&, int&, int&, int&, int, int, int,
                        VecSet<DistSVec<double, dim>>&, int, int&, int&, int&);

 public:
  GappyPreprocessor(IoData&, Domain *, NonlinearRom *);
  ~GappyPreprocessor() {}

  template<int dim>
  void solve(int&, int&, std::vector<std::set<int>>&, Clustering<DistSVec<double, dim>> *);

  void useInputSol(int&, int&, std::vector<std::set<int>>&);

  template<int dim>
  void computeGnatMatrix(int, int, std::vector<std::set<int>>&, std::vector<std::set<int>>&);

};

//----------------------------------------------------------------------------

#endif

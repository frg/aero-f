#ifndef _DIST_METRIC_SPACE_H_
#define _DIST_METRIC_SPACE_H_

#include <MetricSpace.h>
#include <GeometricQuery.h>

class Domain;
template<class Scalar> class DistVec;
template<class Scalar, int dim> class DistSVec;

//------------------------------------------------------------------------------

class DistMetricSpace {
  Domain *domain;
  GeometricQuery query;
  MetricSpace **metricSpace;

  DistVec<double> *dists;
  DistSVec<double, 3> *dirs;
  DistSVec<double, 9> *mets;

public:
  DistMetricSpace(const PreferredDirectionData&, Domain *, TriangleSet *);
  ~DistMetricSpace();

  DistVec<double> * getDistances() { return dists; }
  DistSVec<double, 3> * getDirections() { return dirs; }
  DistSVec<double, 9> * getMetricTensors() { return mets; }

  MetricSpace& operator[](int iSub) { return *(metricSpace[iSub]); }

  GeometricQuery& getSource() { return query; }

  void initializePoints(DistVec<double> *, DistSVec<double, 1> *,
                        DistSVec<double, 1> *, DistSVec<double, 1> *,
                        DistSVec<double, 9> *);
};

//------------------------------------------------------------------------------

#endif

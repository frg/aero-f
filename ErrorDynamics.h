#ifndef _ERROR_DYNAMICS_H
#define _ERROR_DYNAMICS_H

#include <Domain.h>
#include <Communicator.h>
#include <DistVector.h>
#include <VectorSet.h>

#include <string>
#include <random>
#include <vector>

class IoData;
class Domain;
class Communicator;

template<int dim> class PostOperator;
template<int dim> class SpaceOperator;
template<int dim> class DistTimeState;

#ifndef _MATVECPROD_TMPL_
#define _MATVECPROD_TMPL_
template<int dim, int neq, class Scalar2 = double> class MatVecProd;
#endif

#ifndef _MATVECPROD2_TMPL_
#define _MATVECPROD2_TMPL_
template<int dim, int neq, class Scalar2 = double> class MatVecProdFD;
template<int dim, class Scalar, int neq, class Scalar2 = double> class MatVecProdH1;
template<int dim, class Scalar, int neq, class Scalar2 = double> class MatVecProdH2;
template<int dim, class Scalar, class Scalar2 = double> class MatVecProdLS;
#endif

#ifndef _KSPPREC_TMPL_
#define _KSPPREC_TMPL_
template<int Dim, class Scalar2 = double> class KspPrec;
#endif

//------------------------------------------------------------------------------

struct ErrorDynamicsInfo {
  Communicator *com;
  DistInfo info;
  int nf;
  int ns;
  int ncs;
  int n;

  ErrorDynamicsInfo(Communicator *_com, const DistInfo& _distInfo, int _nf, int _ns, int _ncs) : com(_com), info(_distInfo), nf(_nf),
                                                                                                 ns(_ns), ncs(_ncs), n(_nf + 2 * _ns + ncs) {}
};

//------------------------------------------------------------------------------

template<int dim>
class ErrorDynamicsBlockVec {
 public:
  typedef ErrorDynamicsInfo InfoType;
  typedef double RealScalarType;

 private:
  mutable ErrorDynamicsInfo distInfo;
  DistSVec<double, dim> w;
  Vec<double> structHDM;
  Vec<double> xPROM;

  std::vector<int> localOffsets;

  void setLocalOffsets();

 public:
  // constructors --------------------------------------------------------------
  ErrorDynamicsBlockVec(const ErrorDynamicsInfo&);
  ErrorDynamicsBlockVec(const ErrorDynamicsInfo&, int);
  ErrorDynamicsBlockVec(const ErrorDynamicsBlockVec<dim>&);

  // assignment operators ------------------------------------------------------
  ErrorDynamicsBlockVec<dim>& operator=(const double&);
  ErrorDynamicsBlockVec<dim>& operator=(const ErrorDynamicsBlockVec<dim>&);
  ErrorDynamicsBlockVec<dim>& setZero();
  ErrorDynamicsBlockVec<dim>& setFromLocalData(Domain&, double *);

  // arithmetic operators ------------------------------------------------------
  ErrorDynamicsBlockVec<dim> operator+(const ErrorDynamicsBlockVec<dim>&) const;
  ErrorDynamicsBlockVec<dim>& operator+=(const ErrorDynamicsBlockVec<dim>&);
  ErrorDynamicsBlockVec<dim> operator-(const ErrorDynamicsBlockVec<dim>&) const;
  ErrorDynamicsBlockVec<dim>& operator-=(const ErrorDynamicsBlockVec<dim>&);
  ErrorDynamicsBlockVec<dim> operator*(const double&) const;
  ErrorDynamicsBlockVec<dim>& operator*=(const double&);
  double operator*(const ErrorDynamicsBlockVec<dim>&) const;
  double operator^(const ErrorDynamicsBlockVec<dim>&) const;
  void linAdd(const double&, const ErrorDynamicsBlockVec<dim>&);

  // accessors -----------------------------------------------------------------
  InfoType info() const {
    return distInfo;
  }
  DistSVec<double, dim>& getFluidBlock() {
    return w;
  }
  const DistSVec<double, dim>& getFluidBlock() const {
    return w;
  }
  Vec<double>& getStructHDMBlock() {
    return structHDM;
  }
  const Vec<double>& getStructHDMBlock() const {
    return structHDM;
  }
  Vec<double>& getPROMBlock() {
    return xPROM;
  }
  const Vec<double>& getPROMBlock() const {
    return xPROM;
  }

  // resize method -------------------------------------------------------------
  void resize(const ErrorDynamicsInfo&);

  // other methods -------------------------------------------------------------
  double sizeMB() const;
  double norm() const;
  ErrorDynamicsBlockVec<dim>& normalize();
  void print(std::string, Domain&, int, double);
  void print(const char *prefix, Domain& domain, int step, double tag) { print(std::string(prefix), domain, step, tag); }
  void readFromFile(std::string, Domain&, int, std::ifstream&);
  void readFromFile(const char *prefix, Domain& domain, int step, std::ifstream& iFile) { readFromFile(std::string(prefix), domain, step, iFile); }
  void applyControlVolAndInvDtInPlace(DistVec<double>&, double);
  void applyInvControlVolAndDtInPlace(DistVec<double>&, double);
  ErrorDynamicsBlockVec<dim> applyControlVolAndInvDt(DistVec<double>&, double);
  ErrorDynamicsBlockVec<dim> applyInvControlVolAndDt(DistVec<double>&, double);
  int localSize() const;
  void getLocalData(double *) const;
};

//------------------------------------------------------------------------------

template<int dim>
ErrorDynamicsBlockVec<dim> operator*(const double& val, const ErrorDynamicsBlockVec<dim>& vec) {
  return vec * val;
}

//------------------------------------------------------------------------------

template<int dim>
class ErrorDynamicsMatVecProd {
  IoData *ioData;
  Communicator *com;
  MatVecProdH2<dim, double, dim, double> *HOp;
  PostOperator<dim> *postOp;

  VecSet<DistSVec<double, dim>>& DE;
  VecSet<DistSVec<double, dim>>& DX;
  DistSVec<double, dim>& Uref;
  DistSVec<double, 3>& Xref;
  VecSet<DistSVec<double, 3>>& mX;
  VecSet<DistSVec<double, 3>> mXcs;
  VecSet<DistSVec<double, dim>> DXcs;
  VecSet<DistSVec<double, dim>> DEcs;
  DistSVec<double, 3> *dX;
  MatVecProd_dRdX<dim> *dRdXop;

  DenseMat<double> A;
  DenseMat<double> B;
  DenseMat<double> ctrlSurfBlock;
  DenseMat<double> CVelDispHDM;
  DenseMat<double> CROM;
  DenseMat<double> E;
  DenseMat<double> G;
  DenseMat<double> L;
  DenseMat<double> K;
  DenseMat<double> mass;
  DenseMat<double> stiffness;
  DenseMat<double> dForcedX;
  DenseMat<double> dForcedXcs;
  // computed as X^T*K*X_CS, i.e. the coupling between the control surfaces and the standard structural modes
  DenseMat<double> stiffnessCS;
  DenseMat<double> ctrlDispMat;
  DenseMat<double> ctrlMagMat;
  DenseMat<double> stateEstimatorHDM;
  DenseMat<double> stateEstimatorPROM;

  double dt;
  double iDt;
  int nF;
  int nS;
  int nCS;
  int n;
  bool isIdentity;
  bool isDiagonal;

  void loadMatrixData();
  void checkMatrixData();
  void dimensionalizeOperators(SpaceOperator<dim> *, DistTimeState<dim> *tState, DistVec<double>&);
  void applyMassInverse(DenseMat<double>&);
  void applyMassInverseToSubBlock(DenseMat<double>&, int, int, int, int);

 public:
  ErrorDynamicsMatVecProd(IoData *, Communicator *, MatVecProdH2<dim, double, dim, double> *, PostOperator<dim> *,
                          SpaceOperator<dim> *, DistTimeState<dim> *, VecSet<DistSVec<double, dim>>&,
                          VecSet<DistSVec<double, dim>>&, DistSVec<double, dim>&, DistSVec<double, 3>&,
                          VecSet<DistSVec<double, 3>>&, double *, DistVec<double>&, DenseMat<double>&,
                          VecSet<DistSVec<double, dim>>&, VecSet<DistSVec<double, dim>>&, VecSet<DistSVec<double, 3>>&,
                          DenseMat<double>&, DistSVec<double, 3> *, MatVecProd_dRdX<dim> *);

  void apply(ErrorDynamicsBlockVec<dim>&, ErrorDynamicsBlockVec<dim>&);
  void applyTranspose(ErrorDynamicsBlockVec<dim>&, ErrorDynamicsBlockVec<dim>&) {
    com->fprintf(stderr, "*** Error: ErrorDynamicsMatVecProd::applyTranspose not implemented\n");
    exit(-1);
  }
  void applyFullOpToMatrix(DistVec<double>&, VecSet<ErrorDynamicsBlockVec<dim>>&, VecSet<ErrorDynamicsBlockVec<dim>>&);
  //TODO: remove unnecessary accessors
  int romSize() {
    return nF;
  }
  int strcSize() {
    return nS;
  }
  int coupledROMSize() {
    return n;
  }
  DenseMat<double>& getStateDynamics() {
    return A;
  }
  const DenseMat<double>& getStateDynamics() const {
    return A;
  }
  DenseMat<double>& getCtrlMatrix() {
    return B;
  }
  const DenseMat<double>& getCtrlMatrix() const {
    return B;
  }
  DenseMat<double>& getOutputMatrixHDM() {
    return CVelDispHDM;
  }
  const DenseMat<double>& getOutputMatrixHDM() const {
    return CVelDispHDM;
  }
  DenseMat<double>& getOutputMatrixROM() {
    return CROM;
  }
  const DenseMat<double>& getOutputMatrixROM() const {
    return CROM;
  }
  DenseMat<double>& getCtrlJacDisp() {
    return E;
  }
  const DenseMat<double>& getCtrlJacDisp() const {
    return E;
  }
  DenseMat<double>& getCtrlJacMag() {
    return G;
  }
  const DenseMat<double>& getCtrlJacMag() const {
    return G;
  }
  DenseMat<double>& getStateEstGain() {
    return L;
  }
  const DenseMat<double>& getStateEstGain() const {
    return L;
  }
  DenseMat<double>& getCtrlGain() {
    return K;
  }
  const DenseMat<double>& getCtrlGain() const {
    return K;
  }
  DenseMat<double>& getMass() {
    return mass;
  }
  const DenseMat<double>& getMass() const {
    return mass;
  }
  DenseMat<double>& getStateEstimatorPROM() {
    return stateEstimatorPROM;
  }
  const DenseMat<double>& getStateEstimatorPROM() const {
    return stateEstimatorPROM;
  }
};

//------------------------------------------------------------------------------

template<int dim>
class ErrorDynamicsPrec {
  IoData *ioData;
  Domain& domain;
  KspPrec<dim, double> *pc;
  DenseMat<double> stateEstimatorPROM;
  double dt;

 public:
  ErrorDynamicsPrec(IoData *, Domain&, SpaceOperator<dim> *, MatVecProdH2<dim, double, dim, double> *, DistSVec<double, dim>&,
                    const DenseMat<double>&);
  ~ErrorDynamicsPrec();
  void apply(ErrorDynamicsBlockVec<dim>&, ErrorDynamicsBlockVec<dim>&);
};

//------------------------------------------------------------------------------

class ErrorDynamicsBoundsMatProd {
  IoData *ioData;
  Communicator *com;
  // assume performance variables depend only on rigid body state
  // and that performance variables same as output variables
  DenseMat<double>& HfStruct;
  DenseMat<double> Hz;
  DenseMat<double> HzHfStruct;
  DenseMat<double>& K;
  DenseMat<double> Hu;
  DenseMat<double> HuK;

  void loadMatrixData();
  void checkMatrixData();

 public:
  ErrorDynamicsBoundsMatProd(IoData *, Communicator *, DenseMat<double>&,
      DenseMat<double>&);
  int numRowHz() {
    return Hz.numRow();
  }
  int numRowHu() {
    return Hu.numRow();
  }

  template<int dim>
  void applyHzHfStruct(VecSet<ErrorDynamicsBlockVec<dim>>&, DenseMat<double>&);
  template<int dim>
  void applyHuK(VecSet<ErrorDynamicsBlockVec<dim>>&, DenseMat<double>&);
};

#endif //_ERROR_DYNAMICS_H

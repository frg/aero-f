#ifndef _VREMAN_TERM_H_
#define _VREMAN_TERM_H_

#include <LESTerm.h>
#include <IoData.h>

//------------------------------------------------------------------------------
/*
 * An eddy-viscosity subgrid-scale model for turbulent shear flow:
 * Algebraic theory and applications, A. W. Vreman
 * Compressible turbulent channel flow with impedance boundary conditions,
 * Carlo Scalo, Julien Bodart, and Sanjiva K. Lele
 */
class VremanTerm : public LESTerm {

  static constexpr double fourth = 1.0 / 4.0;

  double cs;
  double threshold;
  double eps;

 public:
  VremanTerm(IoData&);
  ~VremanTerm() {}

  double computeEddyViscosity(double *[4], double, double[3][3]);

  double computeDerivativeOfEddyViscosity(double *[4], double *[4], double, double,
                                          double[3][3], double[3][3]);

  void computeJacobianEddyViscosity(double *[4], double, double[3][3],
                                    double[4][3], double[4][5]);

  double computeTKE(double, double);

};

//------------------------------------------------------------------------------

inline
VremanTerm::VremanTerm(IoData& iod) : LESTerm(iod) {
  cs        = iod.eqs.tc.les.vre.c_s;
  threshold = iod.eqs.tc.les.vre.threshold;
  eps       = iod.eqs.tc.les.vre.eps;
}

//------------------------------------------------------------------------------

inline
double VremanTerm::computeEddyViscosity(double *V[4], double Delta, double duidxj[3][3]) {
  using std::sqrt; using std::pow;
  double rho = fourth * (V[0][0] + V[1][0] + V[2][0] + V[3][0]);
  double S1 = 0.0;
  for(int i = 0; i < 3; i++) {
    for(int j = 0; j < 3; j++) {
      S1 += duidxj[i][j] * duidxj[i][j];
    }
  }
  if(S1 > threshold) {
    double beta[3][3];
    for(int i = 0; i < 3; i++) {
      for(int j = 0; j < 3; j++) {
        beta[i][j] = 0.0;
        for(int m = 0; m < 3; m++) {
          beta[i][j] += duidxj[i][m] * duidxj[j][m];
        }
      }
    }
    double S2 = (beta[0][0] * beta[1][1] - beta[0][1] * beta[0][1] +
                 beta[0][0] * beta[2][2] - beta[0][2] * beta[0][2] +
                 beta[1][1] * beta[2][2] - beta[1][2] * beta[1][2]);
    // S2 >= 0.0, in case of machine error
    double Q = S2/S1;
    double sq;
    if(Q < 0) sq = 2/3.*sqrt(eps);
    else if(Q < eps) sq = 1/3.*1/eps*pow(Q, 1.5) + 2/3.*sqrt(eps);
    else sq = sqrt(Q);
    return rho * cs * Delta * Delta * sq;
  }
  else {
    return 0.0;
  }
}

//------------------------------------------------------------------------------

inline
double VremanTerm::computeDerivativeOfEddyViscosity(double *V[4], double *dV[4], double Delta,
                                                    double dDelta, double duidxj[3][3],
                                                    double dduidxj[3][3]) {
  std::cerr << "*** Warning: VremanTerm::computeDerivativeOfEddyViscosity is not implemented\n";
  return 0;
}

//------------------------------------------------------------------------------

inline
void VremanTerm::computeJacobianEddyViscosity(double *V[4], double Delta, double duidxj[3][3],
                                              double dp1dxj[4][3], double dmut[4][5]) {
  using std::sqrt; using std::pow;
  double rho = fourth * (V[0][0] + V[1][0] + V[2][0] + V[3][0]);
  double S1 = 0.0;
  for(int i = 0; i < 3; i++) {
    for(int j = 0; j < 3; j++) {
      S1 += duidxj[i][j] * duidxj[i][j];
    }
  }
  if(S1 > threshold) {
    double beta[3][3];
    for(int i = 0; i < 3; i++) {
      for(int j = 0; j < 3; j++) {
        beta[i][j] = 0.0;
        for(int m = 0; m < 3; m++) {
          beta[i][j] += duidxj[i][m] * duidxj[j][m];
        }
      }
    }
    double betaduidxj[3][3];
    for(int i = 0; i < 3; i++) {
      for(int j = 0; j < 3; j++) {
        betaduidxj[i][j] = 0.0;
        for(int m = 0; m < 3; m++) {
          betaduidxj[i][j] += beta[i][m] * duidxj[m][j];
        }
      }
    }
    double S2 = (beta[0][0] * beta[1][1] - beta[0][1] * beta[0][1] +
                 beta[0][0] * beta[2][2] - beta[0][2] * beta[0][2] +
                 beta[1][1] * beta[2][2] - beta[1][2] * beta[1][2]);
    // S2 >= 0.0, in case of machine error
    double Q = S2/S1;
    if(Q < 0) {
      for(int k = 0; k < 4; ++k) {
        for(int i = 0; i < 5; ++i) {
          dmut[k][i] = 0.0;
        }
      }
    }
    else {
      double cdd = cs * Delta * Delta;
      double sq, toto2; // toto2 = 2 * ∂[mut]/∂Q = 2 * rho * cdd * ∂[sq]/∂Q
      if(Q < eps) {
        sq = 1/3.*1/eps*pow(Q, 1.5) + 2/3.*sqrt(eps);
        toto2 = rho * cdd / eps * sqrt(Q);
      }
      else {
        sq = sqrt(Q);
        toto2 = rho * cdd / sq;
      }
      double toto1 = fourth * cdd * sq;
      double toto3 = 1 - S2/(S1*S1);
      // note: dmut is the derivative w.r.t the conservative state variables
      for(int k = 0; k < 4; ++k) {
        dmut[k][4] = 0.0;
        dmut[k][3] = toto2 * ((dp1dxj[k][0] * duidxj[2][0] + dp1dxj[k][1] * duidxj[2][1] + dp1dxj[k][2] * duidxj[2][2]) * toto3 -
                     (dp1dxj[k][0] * betaduidxj[2][0] + dp1dxj[k][1] * betaduidxj[2][1] + dp1dxj[k][2] * betaduidxj[2][2]) / S1) / V[k][0];
        dmut[k][2] = toto2 * ((dp1dxj[k][0] * duidxj[1][0] + dp1dxj[k][1] * duidxj[1][1] + dp1dxj[k][2] * duidxj[1][2]) * toto3 -
                     (dp1dxj[k][0] * betaduidxj[1][0] + dp1dxj[k][1] * betaduidxj[1][1] + dp1dxj[k][2] * betaduidxj[1][2]) / S1) / V[k][0];
        dmut[k][1] = toto2 * ((dp1dxj[k][0] * duidxj[0][0] + dp1dxj[k][1] * duidxj[0][1] + dp1dxj[k][2] * duidxj[0][2]) * toto3 -
                     (dp1dxj[k][0] * betaduidxj[0][0] + dp1dxj[k][1] * betaduidxj[0][1] + dp1dxj[k][2] * betaduidxj[0][2]) / S1) / V[k][0];
        dmut[k][0] = toto1 - (dmut[k][1] * V[k][1] + dmut[k][2] * V[k][2] + dmut[k][3] * V[k][3]);
      }
    }
  }
  else {
    for(int k = 0; k < 4; ++k) {
      for(int i = 0; i < 5; ++i) {
        dmut[k][i] = 0.0;
      }
    }
  }
}

//------------------------------------------------------------------------------

inline
double VremanTerm::computeTKE(double nut, double Delta) {
  double uTKE = nut / (Delta * cs);
  double k = (1.0 / 2.0) * uTKE * uTKE;
  // std::cout << "vre k = " << k << ";\n";
  return k;
}

//------------------------------------------------------------------------------

#endif

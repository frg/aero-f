#ifndef _FLUX_FCN_BASE_H_
#define _FLUX_FCN_BASE_H_

#include <VarFcnBase.h>
#include <LowMachPrec.h>

//------------------------------------------------------------------------------

class FluxFcnBase {

 public:
  enum Type {CONSERVATIVE = 0, PRIMITIVE = 1};

 protected:
  VarFcnBase *vf;
  SpatialLowMachPrec sprec;
  int rshift;

 public:
  FluxFcnBase(VarFcnBase *varFcn) : vf(varFcn), rshift(0) {}
  virtual ~FluxFcnBase() {
    vf = 0;
  }

  virtual void compute(double, double, double *, double *, double, double *, double *, double *, bool) {
    fprintf(stderr, "*** Error: FluxFcnBase::compute() is not implemented!\n");
    exit(-1);
  }
  virtual void computeJacobian(double, double, double *, double *, double, double *, double *, double *, Type, bool) {
    fprintf(stderr, "*** Error: FluxFcnBase::computeJacobian() is not implemented!\n");
    exit(-1);
  }
  virtual void computeJacobians(double, double, double *, double *, double, double *, double *, double *, double *, Type, bool) {
    fprintf(stderr, "*** Error: FluxFcnBase::computeJacobians() is not implemented!\n");
    exit(-1);
  }
  virtual void computeDerivative(double, double, double, double *, double *, double, double, double *, double *,
                                 double *, double *, double, double *, double *, bool) {
    fprintf(stderr, "*** Error: FluxFcnBase::computeDerivative() is not implemented!\n");
    exit(-1);
  }
  virtual void computeDerivativeOperators(double, double, double *, double, double *, double *, double *, double *, double *,
                                          double *, double *, bool) {
    fprintf(stderr, "*** Error: FluxFcnBase::computeDerivativeOperators() is not implemented!\n");
    exit(-1);
  }

  VarFcnBase *getVarFcnBase() const {
    return vf;
  }
  int getPrecTag() const {
    return sprec.getPrecTag();
  }
  int getRShift() const {
    return rshift;
  }

};

//------------------------------------------------------------------------------

template<int dim>
class FluxFcnFD : public FluxFcnBase {

 public:
  FluxFcnFD(VarFcnBase *vf) : FluxFcnBase(vf) {}
  ~FluxFcnFD() {
    vf = 0;
  }

  virtual void compute(double, double, double *, double *, double, double *, double *, double *, bool) {
    fprintf(stderr, "*** Error: FluxFcnFD::compute() not implemented!");
    exit(-1);
  }
  void computeJacobian(double, double, double *, double *, double, double *, double *, double *, FluxFcnBase::Type, bool);
  void computeJacobians(double, double, double *, double *, double, double *, double *, double *, double *, FluxFcnBase::Type, bool);

};

//------------------------------------------------------------------------------

template<int dim>
inline
void FluxFcnFD<dim>::computeJacobian(double length, double irey, double *upwDir, double *normal, double normalVel,
                                     double *VL, double *VR, double *jac, FluxFcnBase::Type typeJac, bool useLimiter) {
  const double eps0 = 1.e-6;
  double Veps[dim], flux[dim], fluxeps[dim], dfdV[dim * dim];
  compute(length, irey, upwDir, normal, normalVel, VL, VR, flux, useLimiter);
  int k;
  for(k = 0; k < dim; ++k) {
    Veps[k] = VL[k];
  }
  for(k = 0; k < dim; ++k) {
    double eps;
    if(fabs(Veps[k]) < 1.e-10) {
      eps = eps0;
    }
    else {
      eps = eps0 * Veps[k];
    }
    double inveps = 1.0 / eps;
    Veps[k] += eps;
    if(k != 0) {
      Veps[k - 1] = VL[k - 1];
    }
    compute(length, irey, upwDir, normal, normalVel, Veps, VR, fluxeps, useLimiter);
    for(int j = 0; j < dim; ++j) {
      dfdV[dim * j + k] = (fluxeps[j] - flux[j]) * inveps;
    }
  }
  if(typeJac == CONSERVATIVE) {
    vf->postMultiplyBydVdU(VL, dfdV, jac);
  }
  else {
    for(k = 0; k < dim * dim; ++k) {
      jac[k] = dfdV[k];
    }
  }
}

//------------------------------------------------------------------------------

template<int dim>
inline
void FluxFcnFD<dim>::computeJacobians(double length, double irey, double *upwDir, double *normal, double normalVel, double *VL,
                                      double *VR, double *jacL, double *jacR, FluxFcnBase::Type typeJac, bool useLimiter) {
  double n[3] = {normal[0], normal[1], normal[2]};
  computeJacobian(length, irey, upwDir, n, normalVel, VL, VR, jacL, typeJac, useLimiter);
  n[0] = -n[0];
  n[1] = -n[1];
  n[2] = -n[2];
  computeJacobian(length, irey, upwDir, n, -normalVel, VR, VL, jacR, typeJac, useLimiter);
  for(int k = 0; k < dim * dim; ++k) {
    jacR[k] = -jacR[k];
  }
}

//------------------------------------------------------------------------------

#endif

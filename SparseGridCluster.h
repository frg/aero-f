#ifndef _SPARSEGRIDCLUSTER_H_
#define _SPARSEGRIDCLUSTER_H_

class SparseGrid;
class Communicator;
struct SparseGridData;

class SparseGridCluster {
 private:
  int dim_;
  int numSparseGrids_;
  SparseGrid *sparseGrids_;

 public:
  SparseGridCluster();
  virtual ~SparseGridCluster();

  template<typename T>
  void generate(SparseGridData&, double *, void (T::*)(double *, double *, double *), T&,
                const char *, const double *, const double *, const Communicator *);

  void readFromFile(const int, const double *, const double *, const char *, int = 0);

  int interpolate(const int, double **, double **);
  int interpolateGradient(const int, double **, double **);
};

#endif

#ifndef _FEM_EQUATION_TERM_LES_H_
#define _FEM_EQUATION_TERM_LES_H_

#include <FemEquationTerm.h>
#include <FemEquationTermNS.h>
#include <DynamicSmagorinskyTerm.h>
#include <VMSLESTerm.h>

class IoData;
class VarFcn;
class NavierStokesTerm;
class LESTerm;

struct Vec3D;

//------------------------------------------------------------------------------

class FemEquationTermLES : public FemEquationTermTurb, public FemEquationTermNS {

 protected:
  LESTerm *les;

 public:
  FemEquationTermLES(IoData&, VarFcn *);
  virtual ~FemEquationTermLES();

  bool computeVolumeTerm(double, double[4][3], double[4][3], double[4], double *[4], int, int, int,
                         Vec3D&, double *, int, double, double *, double *, double *, double = 1.0);

  bool computeDerivativeOfVolumeTerm(double, double, double[4][3], double[4][3], double[4][3],
                                     double[4][3], double[4], double[4], double *[4], double *[4], double, int, int, int,
                                     Vec3D&, Vec3D&, double *, int, double, double,
                                     double *, double *, double *, double = 1.0, double = 0.0);

  bool computeJacobianVolumeTerm(double, double[4][3], double[4][3], double[4], double *[4], int,
                                 int, int, Vec3D&, double *, int, double, double *, double *,
                                 double *, double = 1.0);

  //TODO: are we sure that the LES model contribution to residual can be neglected
  //      when using a wall model for the body-fitted case? ("wall" surface is not at the wall so
  //      LES flux is not zero...) (see computeSurfaceTerm, computeJacobianSurfaceTerm for wallFcn case)

  virtual double computeMutOMu(double, double[4][3], double[4][3], double *[4], int, bool = false);

};

//------------------------------------------------------------------------------

class FemEquationTermSmagorinsky : public FemEquationTermLES {

 public:
  FemEquationTermSmagorinsky(IoData&, VarFcn *);
  ~FemEquationTermSmagorinsky() {}

};

//------------------------------------------------------------------------------

class FemEquationTermWALE : public FemEquationTermLES {

 public:
  FemEquationTermWALE(IoData&, VarFcn *);
  ~FemEquationTermWALE() {}

};

//------------------------------------------------------------------------------

class FemEquationTermVreman : public FemEquationTermLES {

 public:
  FemEquationTermVreman(IoData&, VarFcn *);
  ~FemEquationTermVreman() {}

};

//------------------------------------------------------------------------------

class FemEquationTermDynamicSmagorinsky : public FemEquationTermLES {

 protected:
  DynamicSmagorinskyTerm *dles;

 public:
  FemEquationTermDynamicSmagorinsky(IoData&, VarFcn *);
  ~FemEquationTermDynamicSmagorinsky() {}

  void setConstants(double cs[4], double Pr[4]) {
    dles->setConstants(cs, Pr);
  }

  double computeCsValue(double tetVol, double x[4][3]) {
    return dles->computeCsValue(tetVol, x);
  }

};

//------------------------------------------------------------------------------

class FemEquationTermVMSLES : public FemEquationTermLES {

 protected:
  VMSLESTerm *vms;

 public:
  FemEquationTermVMSLES(IoData&, VarFcn *);
  ~FemEquationTermVMSLES() {}

  void setVBar(double *vbar[4]) {
    vms->setVBar(vbar);
  }

  // NOTE: Unlike other LES models, the VMS model ONLY adds the SGS LES model
  //       contribution to the residual, and the NS contribution is expected to
  //       be accounted for elsewhere

  bool computeVolumeTerm(double, double[4][3], double[4][3], double[4], double *[4], int, int, int,
                         Vec3D&, double *, int, double, double *, double *, double *, double = 1.0);

  bool computeJacobianVolumeTerm(double, double[4][3], double[4][3], double[4], double *[4], int,
                                 int, int, Vec3D&, double *, int, double, double *, double *,
                                 double *, double = 1.0);

  void computeSurfaceTerm(int, double[3], double *[3], double *, int, Vec3D&, double *);

  void computeJacobianSurfaceTerm(int, double[3], double *[3], double *, int, Vec3D&, double *);

  void computeSurfaceTerm(int, double[4][3], double *[4], double *, int, Vec3D&, double *);

  void computeJacobianSurfaceTerm(int, double[4][3], double *[4], double *, int, Vec3D&, double *);

  double computeMutOMu(double, double[4][3], double[4][3], double *[4], int, bool = false);

};

//------------------------------------------------------------------------------

#endif

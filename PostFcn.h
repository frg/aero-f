#ifndef _POST_FCN_H_
#define _POST_FCN_H_

#include <Vector3D.h>

#include <limits>

class IoData;
class VarFcn;
class WallFcn;
class Communicator;
class NavierStokesTerm;
class SATerm;
class DESTerm;
class KEpsilonTerm;

//------------------------------------------------------------------------------

class PostFcn {

 public:
  enum SolutionType {FULL = 0, COARSE = 1, FINE = 2, LIMITERPHI = 3, SOSIZE = 4};

  enum ScalarType {DENSITY = 0, MACH = 1, PRESSURE = 2, TEMPERATURE = 3, TOTPRESSURE = 4,
                   VORTICITYMAG = 5, CSDLES = 6, CSDVMS = 7, SKINFRICTION = 8, PRESSURECOEFFICIENT = 9,
                   DENSITYFINE = 10, PRESSUREFINE= 11, TEMPERATUREFINE = 12, 
                   DENSITYSQ = 13, PRESSURESQ= 14, TEMPERATURESQ = 15, 
                   PRESSUREFAVRE = 16, TEMPERATUREFAVRE = 17,
                   KTURB = 18, EPSTURB = 19, EDDYVISCOSITY = 20, 
                   DELTAPLUS = 21, PSENSOR = 22, MUTOVERMU = 23, PHILEVEL = 24, PHILEVEL2 = 25, 
                   DIFFPRESSURE = 26, SPEED = 27, HYDROSTATICPRESSURE = 28, HYDRODYNAMICPRESSURE = 29,
                   WTMACH = 30, WTSPEED = 31, VELOCITY_NORM = 32, TEMPERATURENORMALDERIVATIVE = 33,
                   SURFACEHEATFLUX = 34, NUTTURB = 35, CONTROLVOLUME = 36, FLUIDID = 37,
                   HYBRIDSTATUS = 38, MINDISTANCE = 39, MAXDISTANCE = 40, D2WALL = 41,
                   MACROCELLID = 42, FLUXRESIDUALS = 43, ISACTIVE = 44, ISOCCLUDED = 45,
                   LOCALTIMESTEP = 46, HYBRIDINDICATOR = 47, DIVERGENCE = 48, FRICTIONVELOCITY = 49,
                   GAP = 50, FIXES = 51, OMEGATURB = 52, PRESSUREDISTORTION = 53, DENSITYFLUCTUATIONRMS = 54, 
                   PRESSUREFLUCTUATIONRMS = 55, TEMPERATUREFLUCTUATIONRMS = 56, SSIZE = 57};

  enum VectorType {VELOCITY = 0, VORTICITY = 1, DISPLACEMENT = 2, VELOCITYFINE = 3, 
                   VELOCITYFAVRE = 4, FLIGHTDISPLACEMENT = 5, LOCALFLIGHTDISPLACEMENT = 6, 
                   DIVERGENCEVEC = 7, MESH_VELOCITY = 8, D2WALLGRAD = 9, PREFERREDDIRECTION = 10, 
                   REYNOLDSSTRESSEIGENVALUES = 11, VSIZE = 12};

  enum TensorType {REYNOLDSSTRESSMODEL = 0, VELOCITYSQ = 1, VELOCITYFAVRESQ = 2,
                   REYNOLDSSTRESSFLUCTUATION = 3, TNSIZE = 4};

  enum ScalarAvgType {DENSITYAVG = 0, MACHAVG = 1, PRESSUREAVG = 2, TEMPERATUREAVG = 3,
                      TOTPRESSUREAVG = 4, VORTICITYMAGAVG = 5, CSDLESAVG = 6, CSDVMSAVG = 7,
                      SKINFRICTIONAVG = 8, PRESSURECOEFFICIENTAVG = 9, DENSITYFINEAVG = 10,
                      PRESSUREFINEAVG = 11, TEMPERATUREFINEAVG = 12, DENSITYSQAVG = 13,
                      PRESSURESQAVG = 14, TEMPERATURESQAVG = 15, 
                      PRESSUREFAVREAVG = 16, TEMPERATUREFAVREAVG = 17, AVSSIZE = 18};
                      
  enum VectorAvgType {VELOCITYAVG = 0, VORTICITYAVG = 1, DISPLACEMENTAVG = 2, VELOCITYFINEAVG = 3, 
                      VELOCITYFAVREAVG = 4, AVVSIZE = 5};

  enum TensorAvgType {REYNOLDSSTRESSMODELAVG = 0, VELOCITYSQAVG = 1, VELOCITYFAVRESQAVG = 2,
                      AVTNSIZE = 3};

  enum ScalarDerivativeType {DERIVATIVE_DENSITY = 0, DERIVATIVE_MACH = 1, DERIVATIVE_PRESSURE = 2,
                             DERIVATIVE_TEMPERATURE = 3, DERIVATIVE_TOTPRESSURE = 4, DERIVATIVE_NUT_TURB = 5,
                             DERIVATIVE_EDDY_VISCOSITY = 6, DERIVATIVE_VELOCITY_SCALAR = 7,
                             DERIVATIVE_SPATIAL_RES = 8, DERIVATIVE_K_TURB = 9, DERIVATIVE_EPS_TURB = 10,
                             DSSIZE = 11};

  enum VectorDerivativeType {DERIVATIVE_VELOCITY_VECTOR = 0, DERIVATIVE_DISPLACEMENT = 1, DVSIZE = 2};

  enum ScalarHessianType {HESSIAN_DENSITY = 0, HESSIAN_XVELOCITY = 1, HESSIAN_YVELOCITY = 2, HESSIAN_ZVELOCITY = 3,
                          HESSIAN_PRESSURE = 4, HESSIAN_VORTICITYMAG = 5, HESSIAN_MACH = 6, HSSIZE = 7};

 protected:
  VarFcn *varFcn;

 public:
  PostFcn(VarFcn *);
  virtual ~PostFcn() {}

  virtual void rstVar(IoData&) = 0;

  virtual double computeNodeScalarQuantity(ScalarType, double *, double[3], int, double *, bool, double[3]) = 0;

  virtual double computeDerivativeOfNodeScalarQuantity(ScalarDerivativeType, bool, double *, double *,
                                                       double *, double *, int) = 0;

  virtual double computeFaceScalarQuantity(ScalarType, double[4][3], Vec3D&, double[3], double *, double *[3],
                                           double *[4], int[3], int[4], bool) = 0;

  virtual void computeForce(double[4][3], double *[3], Vec3D&, double[3], double *, double *[3], double *[4],
                            double *, Vec3D&, Vec3D&, Vec3D&, Vec3D&, double[3][3], int[3], int[4],
                            bool, int = 0) = 0;

  virtual void computeDerivativeOfForce(double[4][3], double[4][3], double *[3], double *[3], Vec3D&,
                                        Vec3D&, double[3], double *, double *, double *[3], double *[3],
                                        double *[4], double *[4], bool, double *, Vec3D&, Vec3D&,
                                        Vec3D&, Vec3D&, double[3][3], double[3][3], int[3], int[4], bool, int = 0) = 0;

  virtual void computeDerivativeOperatorsOfForce(double[4][3], double *[3], Vec3D&, double[3], double *, double *[3], double *[4],
                                                 double *, double[3][3], int[3], int[4], int, double[3], double[3], double[3],
                                                 double[3][3], double[3][3], double[3][3], double[3][3],
                                                 double[3][3], double[3][3], double[3][3], double[3][3],
                                                 double[3][3], double[3][3], double[3][3], double[3][3],
                                                 double[3][3], double[3][3], double[3][3], double *[3],
                                                 double *[3], double *[3], double[3][4][3], double[3][3],
                                                 double *[3][4], double[3][3], bool) = 0;

  virtual void computeForceTransmitted(double[4][3], double *[3], Vec3D&, double[3], double *, double *[3],
                                       double *[4], double *, Vec3D&, Vec3D&, Vec3D&, Vec3D&, double[3][3],
                                       int[3], int[4], bool, int = 0) = 0;

  virtual void computeDerivativeOfForceTransmitted(double[4][3], double[4][3], double *[3], double *[3],
                                                   Vec3D&, Vec3D&, double[3], double *, double *, double *[3],
                                                   double *[3], double *[4], double *[4], bool,
                                                   double *, Vec3D&, Vec3D&, Vec3D&, Vec3D&, double[3][3],
                                                   double[3][3], int[3], int[4], bool, int = 0) = 0;

  virtual void computeDerivativeOperatorsOfForceTransmitted(double[4][3], double *[3], Vec3D&, double[3], double *, double *[3], double *[4],
                                                            double *, double[3][3], int[3], int[4], int, double[3], double[3][3],
                                                            double *[3][3], double[3][3][3], double[3][3][3], double[3],
                                                            double[3][3], double *[3][3], double[3][3][3], double[3][3][3],
                                                            double[3], double[3][3], double *[3][3], double[3][3][3],
                                                            double[3][3][3], double[3][4][3], double[3][3], double *[3][4],
                                                            double[3][3], bool) = 0;

  virtual double computeHeatPower(double[4][3], Vec3D&, double[3], double *, double *[3], double *[4],
                                  int[3], int[4], bool) = 0;

  virtual Vec3D computeViscousForce(double[4][3], Vec3D&, double[3], double *, double *[3], double *[4],
                                    int[3], int[4], bool) = 0;

  virtual Vec3D computeDerivativeOfViscousForce(double[4][3], double[4][3], Vec3D&, Vec3D&, double[3],
                                                double *, double *, double *[3], double *[3], double *[4],
                                                double *[4], bool, int[3], int[4], bool) = 0;

  virtual Vec3D computeViscousForceAtWall(double *, Vec3D&, double, Vec3D&, double[4][3], double *[4],
                                          int, bool, double = std::numeric_limits<double>::epsilon()) = 0;

  virtual double computeSkinFriction(Vec3D&, double, Vec3D&, double *[4], double *, int, bool) = 0;

  virtual bool doesFaceNeedGradientP1Function() {
    return false;
  }

  // embedded functions
  virtual double computeSkinFriction(Vec3D&, double, Vec3D&, Vec3D&, double, double, int, bool) = 0;

  virtual double computeDeltaPlusAtWall(Vec3D&, double, Vec3D&, Vec3D&, double, double, int) = 0;

  // virtual double computeDeltaAtWall(Vec3D&, double, Vec3D&, Vec3D&, double, double, int) = 0;

  virtual Vec3D computeViscousForceAtWall(Vec3D&, double, double, Vec3D&, double, Vec3D&, int, bool) = 0;

  virtual void computeViscousForceAtWallDerivative(Vec3D&, double, double, Vec3D&, double, Vec3D&, Vec3D[3], Vec3D&, int, bool) = 0;

};

//------------------------------------------------------------------------------

class PostFcnEuler : public PostFcn {

 protected:
  double mach;
  double pinfty;

  bool dimFlag;
  double dpinfty;
  double dPin;

 public:
  PostFcnEuler(IoData&, VarFcn *);
  virtual ~PostFcnEuler() {}

  void rstVar(IoData&);

  double computeNodeScalarQuantity(ScalarType, double *, double[3], int, double *, bool, double[3]);

  double computeDerivativeOfNodeScalarQuantity(ScalarDerivativeType, bool, double *, double *,
                                               double *, double *, int);

  void computeForce(double[4][3], double *[3], Vec3D&, double[3], double *, double *[3], double *[4],
                    double *, Vec3D&, Vec3D&, Vec3D&, Vec3D&, double[3][3], int[3], int[4], bool, int = 0);

  void computeDerivativeOfForce(double[4][3], double[4][3], double *[3], double *[3], Vec3D&, Vec3D&,
                                double[3], double *, double *, double *[3], double *[3], double *[4],
                                double *[4], bool, double *, Vec3D&, Vec3D&, Vec3D&, Vec3D&, double[3][3],
                                double[3][3], int[3], int[4], bool, int = 0);

  void computeDerivativeOperatorsOfForce(double[4][3], double *[3], Vec3D&, double[3], double *, double *[3], double *[4],
                                         double *, double[3][3], int[3], int[4], int, double[3], double[3], double[3],
                                         double[3][3], double[3][3], double[3][3], double[3][3],
                                         double[3][3], double[3][3], double[3][3], double[3][3],
                                         double[3][3], double[3][3], double[3][3], double[3][3],
                                         double[3][3], double[3][3], double[3][3], double *[3],
                                         double *[3], double *[3], double[3][4][3], double[3][3],
                                         double *[3][4], double[3][3], bool);

  void computeForceTransmitted(double[4][3], double *[3], Vec3D&, double[3], double *, double *[3], double *[4],
                               double *, Vec3D&, Vec3D&, Vec3D&, Vec3D&, double[3][3], int[3], int[4],
                               bool, int = 0);

  void computeDerivativeOfForceTransmitted(double[4][3], double[4][3], double *[3], double *[3], Vec3D&,
                                           Vec3D&, double[3], double *, double *, double *[3], double *[3],
                                           double *[4], double *[4], bool, double *, Vec3D&, Vec3D&,
                                           Vec3D&, Vec3D&, double[3][3], double[3][3], int[3], int[4],
                                           bool, int = 0);

  void computeDerivativeOperatorsOfForceTransmitted(double[4][3], double *[3], Vec3D&, double[3], double *, double *[3], double *[4],
                                                    double *, double[3][3], int[3], int[4], int, double[3], double[3][3],
                                                    double *[3][3], double[3][3][3], double[3][3][3], double[3],
                                                    double[3][3], double *[3][3], double[3][3][3], double[3][3][3],
                                                    double[3], double[3][3], double *[3][3], double[3][3][3],
                                                    double[3][3][3], double[3][4][3], double[3][3], double *[3][4],
                                                    double[3][3], bool);

  double computeHeatPower(double[4][3], Vec3D&, double[3], double *, double *[3], double *[4], int[3],
                          int[4], bool);

  double computeFaceScalarQuantity(ScalarType, double[4][3], Vec3D&, double[3], double *, double *[3],
                                   double *[4], int[3], int[4], bool) {
    fprintf(stderr, "*** Error: Calling a PostFcnEuler Function for Viscous Forces. Doesn't make sense!\n");
    exit(-1);
  }

  Vec3D computeViscousForce(double[4][3], Vec3D&, double[3], double *, double *[3], double *[4], int[3],
                            int[4], bool) {
    fprintf(stderr, "*** Error: Calling a PostFcnEuler Function for Viscous Forces. Doesn't make sense!\n");
    exit(-1);
  }

  Vec3D computeDerivativeOfViscousForce(double[4][3], double[4][3], Vec3D&, Vec3D&, double[3], double *,
                                        double *, double *[3], double *[3], double *[4], double *[4],
                                        bool, int[3], int[4], bool) {
    fprintf(stderr, "*** Error: Calling a PostFcnEuler Function for Viscous Forces. Doesn't make sense!\n");
    exit(-1);
  }

  Vec3D computeViscousForceAtWall(double *, Vec3D&, double, Vec3D&, double[4][3], double *[4], int, bool,
                                  double = std::numeric_limits<double>::epsilon()) {
    fprintf(stderr, "*** Error: Calling a PostFcn Function for Viscous Forces. Doesn't make sense!\n");
    exit(-1);
  }

  double computeSkinFriction(Vec3D&, double, Vec3D&, double *[4], double *, int, bool) {
    fprintf(stderr, "*** Error: Calling a PostFcn Function for Viscous Forces. Doesn't make sense!\n");
    exit(-1);
  }

  double computeSkinFriction(Vec3D&, double, Vec3D&, Vec3D&, double, double, int, bool) {
    fprintf(stderr, "Calling a PostFcn Function for Viscous Forces. Doesn't make sense!\n");
    exit(-1);
  }

  double computeDeltaPlusAtWall(Vec3D&, double, Vec3D&, Vec3D&, double, double, int) {
    fprintf(stderr, "Calling a PostFcn Function for Viscous Forces. Doesn't make sense!\n");
    exit(-1);
  }

  Vec3D computeViscousForceAtWall(Vec3D&, double, double, Vec3D&, double, Vec3D&, int, bool) {
    fprintf(stderr, "Calling a PostFcn Function for Viscous Forces. Doesn't make sense!\n");
    exit(-1);
  }

  void computeViscousForceAtWallDerivative(Vec3D&, double, double, Vec3D&, double, Vec3D&, Vec3D[3], Vec3D&, int, bool) {
    fprintf(stderr, "Calling a PostFcn Function for Viscous Forces. Doesn't make sense!\n");
    exit(-1);
  }

};

//------------------------------------------------------------------------------

class PostFcnNS : public PostFcnEuler {

 protected:
  static constexpr double fourth = 1.0 / 4.0;

  WallFcn *wallFcn;
  NavierStokesTerm *ns;

 public:
  PostFcnNS(IoData&, VarFcn *, NavierStokesTerm *);
  virtual ~PostFcnNS();

  void rstVar(IoData&);

  double computeFaceScalarQuantity(ScalarType, double[4][3], Vec3D&, double[3], double *, double *[3],
                                   double *[4], int[3], int[4], bool);

  void computeForce(double[4][3], double *[3], Vec3D&, double[3], double *, double *[3], double *[4],
                    double *, Vec3D&, Vec3D&, Vec3D&, Vec3D&, double[3][3], int[3], int[4], bool, int = 0);

  void computeDerivativeOfForce(double[4][3], double[4][3], double *[3], double *[3], Vec3D&, Vec3D&,
                                double[3], double *, double *, double *[3], double *[3], double *[4],
                                double *[4], bool, double *, Vec3D&, Vec3D&, Vec3D&, Vec3D&, double[3][3],
                                double[3][3], int[3], int[4], bool, int = 0);

  void computeDerivativeOperatorsOfForce(double[4][3], double *[3], Vec3D&, double[3], double *, double *[3], double *[4],
                                         double *, double[3][3], int[3], int[4], int, double[3], double[3], double[3],
                                         double[3][3], double[3][3], double[3][3], double[3][3],
                                         double[3][3], double[3][3], double[3][3], double[3][3],
                                         double[3][3], double[3][3], double[3][3], double[3][3],
                                         double[3][3], double[3][3], double[3][3], double *[3],
                                         double *[3], double *[3], double[3][4][3], double[3][3],
                                         double *[3][4], double[3][3], bool);

  void computeForceTransmitted(double[4][3], double *[3], Vec3D&, double[3], double *, double *[3], double *[4],
                               double *, Vec3D&, Vec3D&, Vec3D&, Vec3D&, double[3][3], int[3], int[4],
                               bool, int = 0);

  void computeDerivativeOfForceTransmitted(double[4][3], double[4][3], double *[3], double *[3], Vec3D&,
                                           Vec3D&, double[3], double *, double *, double *[3], double *[3],
                                           double *[4], double *[4], bool, double *, Vec3D&, Vec3D&,
                                           Vec3D&, Vec3D&, double[3][3], double[3][3], int[3], int[4],
                                           bool, int = 0);

  void computeDerivativeOperatorsOfForceTransmitted(double[4][3], double *[3], Vec3D&, double[3], double *, double *[3], double *[4],
                                                    double *, double[3][3], int[3], int[4], int, double[3], double[3][3],
                                                    double *[3][3], double[3][3][3], double[3][3][3], double[3],
                                                    double[3][3], double *[3][3], double[3][3][3], double[3][3][3],
                                                    double[3], double[3][3], double *[3][3], double[3][3][3],
                                                    double[3][3][3], double[3][4][3], double[3][3], double *[3][4],
                                                    double[3][3], bool);

  double computeHeatPower(double[4][3], Vec3D&, double[3], double *, double *[3], double *[4], int[3],
                          int[4], bool);

  Vec3D computeViscousForce(double[4][3], Vec3D&, double[3], double *, double *[3], double *[4], int[3],
                            int[4], bool);

  Vec3D computeDerivativeOfViscousForce(double[4][3], double[4][3], Vec3D&, Vec3D&, double[3], double *,
                                        double *, double *[3], double *[3], double *[4], double *[4],
                                        bool, int[3], int[4], bool);

  void computeDerivativeOperatorsOfViscousForce(double[4][3], Vec3D&, double[3], double *, double *[3], double *[4], int[3], int[4], double[3][4][3],
                                                double[3][3], double *[3][4], double[3][3], bool);

  Vec3D computeViscousForceAtWall(double *, Vec3D&, double, Vec3D&, double[4][3], double *[4], int, bool,
                                  double = std::numeric_limits<double>::epsilon());

  double computeSkinFriction(Vec3D&, double, Vec3D&, double *[4], double *, int, bool);

  bool doesFaceNeedGradientP1Function() {
    return ((wallFcn) ? false : true);
  }

  Vec3D computeViscousForceAtWall(Vec3D&, double, double, Vec3D&, double, Vec3D&, int, bool);

  void computeViscousForceAtWallDerivative(Vec3D&, double, double, Vec3D&, double, Vec3D&, Vec3D[3], Vec3D&, int, bool);

  double computeSkinFriction(Vec3D&, double, Vec3D&, Vec3D&, double, double, int, bool);

  double computeDeltaPlusAtWall(Vec3D&, double, Vec3D&, Vec3D&, double, double, int);

};

//------------------------------------------------------------------------------

class PostFcnSA : public PostFcnNS {

 protected:
  SATerm *sa;

 public:
  PostFcnSA(IoData&, VarFcn *, NavierStokesTerm *);
  ~PostFcnSA();

  void rstVar(IoData&);

  double computeNodeScalarQuantity(ScalarType, double *, double[3], int, double *, bool, double[3]);
  double computeDerivativeOfNodeScalarQuantity(ScalarDerivativeType, bool, double *, double *,
                                               double *, double *, int);

};

//------------------------------------------------------------------------------

class PostFcnDES : public PostFcnNS {

 protected:
  DESTerm *des;

 public:
  PostFcnDES(IoData&, VarFcn *, NavierStokesTerm *);
  ~PostFcnDES();

  void rstVar(IoData&);

  double computeNodeScalarQuantity(ScalarType, double *, double[3], int, double *, bool, double[3]);
  double computeDerivativeOfNodeScalarQuantity(ScalarDerivativeType, bool, double *, double *,
                                               double *, double *, int);

};

//------------------------------------------------------------------------------

class PostFcnKE : public PostFcnNS {

 protected:
  KEpsilonTerm *ke;

 public:
  PostFcnKE(IoData&, VarFcn *, NavierStokesTerm *);
  ~PostFcnKE();

  void rstVar(IoData&);

  double computeNodeScalarQuantity(ScalarType, double *, double[3], int, double *, bool, double[3]);
  double computeDerivativeOfNodeScalarQuantity(ScalarDerivativeType, bool, double *, double *,
                                               double *, double *, int);

};

//------------------------------------------------------------------------------

#endif

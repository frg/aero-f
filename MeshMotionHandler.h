#ifndef _MESH_MOTION_HANDLER_H_
#define _MESH_MOTION_HANDLER_H_

#include <IoData.h>
#include <DistVector.h>
#include <NodeData.h>
#include <FSI/DynamicNodalTransfer.h>
#include <MeshMotionSolver.h>

class Communicator;
class CorotSolver;
class DistLevelSetStructure;
class Domain;
class EmbeddedCorotSolver;
class VarFcn;

template<int dim> class PostOperator;

//------------------------------------------------------------------------------

class MeshMotionHandler {

 protected:
  double tscale;
  double oolscale;
  double dtf0;
  double Pin;

  Domain *domain;
  Communicator *com;

  DistSVec<double, 3> X0, dX, F;

 public:
  MeshMotionHandler(IoData&, Domain *);
  virtual ~MeshMotionHandler() {}

  virtual void resize1(NodeData *);
  virtual void resize2() {}

  virtual void cmdCom(bool *) {}
  virtual void getNumParam(int&, int&, double&) {}
  virtual void sendNumParam(int) {}
  virtual void getRelResidual(double&) {}

  virtual double updateStep1(bool *, int, double, DistSVec<double, 3>&, DistSVec<double, 3>&, double * = 0) { return 0.0; }
  virtual double updateStep2(bool *, int, double, DistSVec<double, 3>&, DistSVec<double, 3>&) { return 0.0; }
  virtual int getAlgNum() { return 0; }
  virtual double getStructureTimeStep() { return 0; }

  virtual void getDeltaXCM(double dXCM[3]) { dXCM[0] = 0.0; dXCM[1] = 0.0; dXCM[2] = 0.0; }

  virtual bool getRotationMatrix(double rotMat[3][3]) { return false; }

  virtual void sendForceSensitivity(DistSVec<double, 3> *, bool = true) {}
  virtual void updateDStep2(DistSVec<double, 3>&, DistSVec<double, 3>&, bool = false) {}

  virtual void updateMomentArm(Vec3D& x00, Vec3D& x0) {
    com->fprintf(stderr, "*** Warning: updateMomentArm not implemented for MeshMotionHandler.\n");
  }

};

//------------------------------------------------------------------------------

class RigidMeshMotionHandler {

 protected:
  RigidMeshMotionData::Tag typeTag;
  RigidMeshMotionData::LawType typeLaw;
  double tscale;

  Vec3D vin;
  double cin;

  Vec3D v;
  Vec3D a;
  Vec3D deltaX;

  int nvpts;
  double *timeVpts;
  Vec3D *velVpts;

  DistSVec<double, 3> Xr;

 public:

  RigidMeshMotionHandler(IoData&, VarFcn *, double *, Domain *);
  ~RigidMeshMotionHandler();

  void setupVelocityPoints(IoData&);
  void addRigidMotion(double, DistSVec<double, 3>&, DistSVec<double, 3>&, DistSVec<double, 3>&, Communicator *com = 0);

  DistSVec<double, 3>& getRelativePositionVector(double, DistSVec<double, 3>&);
  DistSVec<double, 3>& getFlightPositionVector(double, DistSVec<double, 3>&);

  const char *getTagName();
  double getTagValue(double);
  virtual void updateMomentArm(Vec3D&, Vec3D&);

};

//------------------------------------------------------------------------------

class AccMeshMotionHandler : public MeshMotionHandler, public RigidMeshMotionHandler {

  double dt;

 public:

  AccMeshMotionHandler(IoData&, VarFcn *, double *, Domain *);
  ~AccMeshMotionHandler() {}

  void resize1(NodeData *);

  double update(bool *, int, double, DistSVec<double, 3>&, DistSVec<double, 3>&);
  double updateStep1(bool *, int, double, DistSVec<double, 3>&, DistSVec<double, 3>&, double * = 0);
  double updateStep2(bool *, int, double, DistSVec<double, 3>&, DistSVec<double, 3>&);

  virtual void updateMomentArm(Vec3D& x00, Vec3D& x0) { RigidMeshMotionHandler::updateMomentArm(x00, x0); }

};

//------------------------------------------------------------------------------

class AeroMeshMotionHandler : public MeshMotionHandler {

 protected:

  AeroelasticData::Force forceComputation;
  enum TimeIntegrator {IMPLICIT_FIRST_ORDER, IMPLICIT_SECOND_ORDER} timeIntegrator;
  bool steady;
  int it0;
  double mppFactor;

  DistSVec<double, 3> *Fn;
  DistSVec<double, 3> *Fnp1;
  DistSVec<double, 3> *Favg;

  StructExc *strExc;

  MeshMotionSolver *mms;
  char *posFile;

 public:

  AeroMeshMotionHandler(IoData&, VarFcn *, MatchNodeSet **,
                        Domain *, std::string = "");
  ~AeroMeshMotionHandler();

  void resize1(NodeData *);
  void resize2();

  template<int dim>
  void setup(int *, double *, PostOperator<dim> *, DistSVec<double, 3>&,
             DistSVec<double, dim>&, DistSVec<double, 3> *);

  template<int dim>
  void resetOutputToStructure(PostOperator<dim> *, DistSVec<double, 3>&,
                              DistSVec<double, dim>&, DistSVec<double, 3> *);

  template<int dim>
  void updateOutputToStructure(double, double, PostOperator<dim> *,
                               DistSVec<double, 3>&, DistSVec<double, dim>&,
                               DistSVec<double, 3> *);

  int getModalMotion(DistSVec<double, 3>&);
  void negotiate();

  void cmdCom(bool *);
  void getNumParam(int&, int&, double&);
  void sendNumParam(int);
  void getRelResidual(double&);
  virtual double update(bool *, int, double, DistSVec<double, 3>&, DistSVec<double, 3>&);
  double updateStep1(bool *, int, double, DistSVec<double, 3>&, DistSVec<double, 3>&, double * = 0);
  double updateStep2(bool *, int, double, DistSVec<double, 3>&, DistSVec<double, 3>&);
  int getAlgNum();

  void getDeltaXCM(double dXCM[3]) { mms->getDeltaXCM(dXCM); }

  bool getRotationMatrix(double rotMat[3][3]) { return mms->getRotationMatrix(rotMat); }

  void sendForceSensitivity(DistSVec<double, 3> *, bool = true);
  void updateDStep2(DistSVec<double, 3>&, DistSVec<double, 3>&, bool = false);

  void updateMomentArm(Vec3D& x00, Vec3D& x0) { mms->updateMomentArm(x00, x0); }

  template<int dim>
  void initializeOutputToStructure(PostOperator<dim> *, DistSVec<double, dim>&, DistSVec<double, 3> *);
  void sendInitialForce();

};

//------------------------------------------------------------------------------

class AccAeroMeshMotionHandler : public AeroMeshMotionHandler, public RigidMeshMotionHandler {

 public:

  AccAeroMeshMotionHandler(IoData&, VarFcn *, double *, MatchNodeSet **,
                           Domain *);
  ~AccAeroMeshMotionHandler() {}

  void resize1(NodeData *);

  double update(bool *, int, double, DistSVec<double, 3>&, DistSVec<double, 3>&);
  double updateStep1(bool *, int, double, DistSVec<double, 3>&, DistSVec<double, 3>&, double * = 0);
  double updateStep2(bool *, int, double, DistSVec<double, 3>&, DistSVec<double, 3>&);

  void updateMomentArm(Vec3D& x00, Vec3D& x0);

};

//------------------------------------------------------------------------------

class DeformingMeshMotionHandler : public MeshMotionHandler {

  double dt;
  double omega;

  DistSVec<double, 3> *dXmax;

  MeshMotionSolver *mms;

 public:

  DeformingMeshMotionHandler(IoData&, Domain *);
  ~DeformingMeshMotionHandler();

  void resize1(NodeData *);
  void resize2();

  double update(bool *, int, double, DistSVec<double, 3>&, DistSVec<double, 3>&);
  double updateStep1(bool *, int, double, DistSVec<double, 3>&, DistSVec<double, 3>&, double * = 0);
  double updateStep2(bool *, int, double, DistSVec<double, 3>&, DistSVec<double, 3>&);

  DistSVec<double, 3>& getModes();

  void setup(DistSVec<double, 3>&);

  virtual void updateMomentArm(Vec3D& x00, Vec3D& x0) {
    com->fprintf(stderr, "*** Warning: updateMomentArm not implemented for DeformingMeshMotionHandler.\n");
  }

};

//------------------------------------------------------------------------------

class PitchingMeshMotionHandler : public MeshMotionHandler {

  double alpha_in;
  double alpha_max;
  double alpha_slope;

  double beta_in;
  double beta_max;
  double beta_slope;

  double dt;
  double ts;
  double omega;

  double x1[3];
  double x2[3];

  double y1[3];
  double y2[3];

  double u, v, w;
  double ix, iy, iz;
  double jx, jy, jz;

  MeshMotionSolver *mms;

 public:

  PitchingMeshMotionHandler(IoData&, Domain *);
  ~PitchingMeshMotionHandler();

  void resize1(NodeData *);
  void resize2();

  double update(bool *, int, double, DistSVec<double, 3>&, DistSVec<double, 3>&);
  double updateStep1(bool *, int, double, DistSVec<double, 3>&, DistSVec<double, 3>&, double * = 0);
  double updateStep2(bool *, int, double, DistSVec<double, 3>&, DistSVec<double, 3>&);

  //DistSVec<double, 3> getModes();

  void setup(DistSVec<double, 3>&);

  void updateMomentArm(Vec3D&, Vec3D&);

};

//------------------------------------------------------------------------------

class HeavingMeshMotionHandler : public MeshMotionHandler {

  double dt;
  double ts;
  double omega;

  double delta[3];

  MeshMotionSolver *mms;

 public:

  HeavingMeshMotionHandler(IoData&, Domain *);
  ~HeavingMeshMotionHandler();

  void resize1(NodeData *);
  void resize2();

  double update(bool *, int, double, DistSVec<double, 3>&, DistSVec<double, 3>&);
  double updateStep1(bool *, int, double, DistSVec<double, 3>&, DistSVec<double, 3>&, double * = 0);
  double updateStep2(bool *, int, double, DistSVec<double, 3>&, DistSVec<double, 3>&);

  //DistSVec<double, 3> getModes();

  void setup(DistSVec<double, 3>&);

  void updateMomentArm(Vec3D&, Vec3D&);

};

//------------------------------------------------------------------------------

class HeavingPitchingMeshMotionHandler : public MeshMotionHandler {

  double dt;
  double ts;
  double omega;

  // Heaving amplitude
  double delta[3];

  // Pitching parameters
  double alpha_in;
  double alpha_max;
  double alpha_slope;

  double beta_in;
  double beta_max;
  double beta_slope;

  double x1[3];
  double x2[3];

  double y1[3];
  double y2[3];

  double u, v, w;
  double ix, iy, iz;
  double jx, jy, jz;

  MeshMotionSolver *mms;

 public:

  HeavingPitchingMeshMotionHandler(IoData&, Domain *);
  ~HeavingPitchingMeshMotionHandler();

  void resize1(NodeData *);
  void resize2();
  void setup(DistSVec<double, 3>&);

  double update(bool *, int, double, DistSVec<double, 3>&, DistSVec<double, 3>&);
  double updateStep1(bool *, int, double, DistSVec<double, 3>&, DistSVec<double, 3>&, double * = 0);
  double updateStep2(bool *, int, double, DistSVec<double, 3>&, DistSVec<double, 3>&);

  void updateMomentArm(Vec3D&, Vec3D&);
};

//------------------------------------------------------------------------------

class SpiralingMeshMotionHandler : public MeshMotionHandler {

  double dt;
  double omega;

  double delta[3];

  MeshMotionSolver *mms;

 public:

  SpiralingMeshMotionHandler(IoData&, Domain *);
  ~SpiralingMeshMotionHandler();

  void resize1(NodeData *);
  void resize2();

  double update(bool *, int, double, DistSVec<double, 3>&, DistSVec<double, 3>&);
  double updateStep1(bool *, int, double, DistSVec<double, 3>&, DistSVec<double, 3>&, double * = 0);
  double updateStep2(bool *, int, double, DistSVec<double, 3>&, DistSVec<double, 3>&);

  //DistSVec<double, 3> getModes();

  void setup(DistSVec<double, 3>&);

  void updateMomentArm(Vec3D& x00, Vec3D& x0) {
    com->fprintf(stderr, "*** Warning: updateMomentArm not implemented for SpiralingMeshMotionHandler.\n");
  }

};

//------------------------------------------------------------------------------

class AccPitchingMeshMotionHandler : public PitchingMeshMotionHandler, public RigidMeshMotionHandler {

 public:

  AccPitchingMeshMotionHandler(IoData&, VarFcn *, double *, Domain *);
  ~AccPitchingMeshMotionHandler() {}

  void resize1(NodeData *);

  double update(bool *, int, double, DistSVec<double, 3>&, DistSVec<double, 3>&);
  double updateStep1(bool *, int, double, DistSVec<double, 3>&, DistSVec<double, 3>&, double * = 0);
  double updateStep2(bool *, int, double, DistSVec<double, 3>&, DistSVec<double, 3>&);

  void updateMomentArm(Vec3D& x00, Vec3D& x0) {
    com->fprintf(stderr, "*** Warning: updateMomentArm not implemented for AccPitchingMeshMotionHandler.\n");
  }

};

//------------------------------------------------------------------------------

class AccHeavingMeshMotionHandler : public HeavingMeshMotionHandler, public RigidMeshMotionHandler {

 public:

  AccHeavingMeshMotionHandler(IoData&, VarFcn *, double *, Domain *);
  ~AccHeavingMeshMotionHandler() {}

  void resize1(NodeData *);

  double update(bool *, int, double, DistSVec<double, 3>&, DistSVec<double, 3>&);
  double updateStep1(bool *, int, double, DistSVec<double, 3>&, DistSVec<double, 3>&, double * = 0);
  double updateStep2(bool *, int, double, DistSVec<double, 3>&, DistSVec<double, 3>&);

  void updateMomentArm(Vec3D& x00, Vec3D& x0) {
    com->fprintf(stderr, "*** Warning: updateMomentArm not implemented for AccHeavingMeshMotionHandler.\n");
  }

};


//------------------------------------------------------------------------------

class AccDeformingMeshMotionHandler : public DeformingMeshMotionHandler, public RigidMeshMotionHandler {

 public:

  AccDeformingMeshMotionHandler(IoData&, VarFcn *, double *, Domain *);
  ~AccDeformingMeshMotionHandler() {}

  void resize1(NodeData *);

  double update(bool *, int, double, DistSVec<double, 3>&, DistSVec<double, 3>&);
  double updateStep1(bool *, int, double, DistSVec<double, 3>&, DistSVec<double, 3>&, double * = 0);
  double updateStep2(bool *, int, double, DistSVec<double, 3>&, DistSVec<double, 3>&);

  void updateMomentArm(Vec3D& x00, Vec3D& x0) {
    com->fprintf(stderr, "*** Warning: updateMomentArm not implemented for AccDeformingMeshMotionHandler.\n");
  }

};


//------------------------------------------------------------------------------

class RigidRollMeshMotionHandler : public MeshMotionHandler {

  double alpha_in;
  double beta_in;

  double dt;
  double maxtime;

  Vec3D xo;
  Vec3D u;

  MeshMotionSolver *mms;

 private:

  double computeRotationAngle(double);

 public:

  RigidRollMeshMotionHandler(IoData&, Domain *);
  ~RigidRollMeshMotionHandler() {}

  double update(bool *, int, double, DistSVec<double, 3>&, DistSVec<double, 3>&);
  double updateStep2(bool *, int, double, DistSVec<double, 3>&, DistSVec<double, 3>&);

  void updateMomentArm(Vec3D& x00, Vec3D& x0) {
    com->fprintf(stderr, "*** Warning: updateMomentArm not implemented for RigidRollMeshMotionHandler.\n");
  }

};

//------------------------------------------------------------------------------

class EmbeddedMeshMotionHandler {

  double dts; // structure time-step.
  int it0;    // restart time-step index
  bool steady;
  double dtf0;

  Domain *domain;
  Communicator *com;

  DynamicNodalTransfer *dynNodalTransfer;
  DistLevelSetStructure *distLSS;

 public:
  EmbeddedMeshMotionHandler(IoData&, Domain *, DynamicNodalTransfer *, DistLevelSetStructure *);
  ~EmbeddedMeshMotionHandler();

  void setup(double *);
  double updateStep1(bool *, int, double, double * = 0);
  double updateStep2(bool *, int, double);
  int getAlgNum() {
    return dynNodalTransfer->getAlgorithmNumber();
  }
  double getStructureTimeStep() { return dts; }
  void sendInitialForce();

 private:
  void step1ForA6(bool *, int);
  void step1ForC0FEM(bool *, int);
  void step1ForC0XFEM(bool *, int);
  void step1ForC0XFEM3D(bool *, int);
  void step2ForPP(bool *, int);
  void step2ForA6(bool *, int);
  void step2ForC0(bool *, int);
  void step2ForC0XFEM3D(bool *, int);

  void updateMomentArm(Vec3D& x00, Vec3D& x0) {
    com->fprintf(stderr, "*** Warning: updateMomentArm not implemented for EmbeddedMeshMotionHandler.\n");
  }

};

//------------------------------------------------------------------------------

class EmbeddedALEMeshMotionHandler : public MeshMotionHandler {

 protected:

  bool initializeMeshVelocity;
  double dt;
  DistLevelSetStructure *distLSS;
  EmbeddedCorotSolver *cs;
  int algNum;

 public:

  EmbeddedALEMeshMotionHandler(IoData&, Domain *, DistLevelSetStructure *, std::string = "", int = 0);
  ~EmbeddedALEMeshMotionHandler();

  void resize1(NodeData *);
  void resize2();

  double update(bool *, int, double, DistSVec<double, 3>&, DistSVec<double, 3>&);
  double updateStep1(bool *, int, double, DistSVec<double, 3>&, DistSVec<double, 3>&, double * = 0);
  double updateStep2(bool *, int, double, DistSVec<double, 3>&, DistSVec<double, 3>&);

  void setup(DistSVec<double, 3>&, DistSVec<double, 3>&);

  void getDeltaXCM(double[3]);

  bool getRotationMatrix(double[3][3]);

  void updateMomentArm(Vec3D&, Vec3D&);

  int getAlgNum() { return algNum; }

};

//------------------------------------------------------------------------------

class RbmExtractor : public MeshMotionHandler {

  char *name_in;
  char *name_out1;
  char *name_out2;

  CorotSolver *cs;

 public:

  RbmExtractor(IoData&, Domain *);
  ~RbmExtractor() {}

  double update(bool *, int, double, DistSVec<double, 3>&, DistSVec<double, 3>&);
  double updateStep2(bool *, int, double, DistSVec<double, 3>&, DistSVec<double, 3>&);

  void updateMomentArm(Vec3D& x00, Vec3D& x0) {
    com->fprintf(stderr, "*** Warning: updateMomentArm not implemented for RbmExtractor.\n");
  }

};

//------------------------------------------------------------------------------

#endif

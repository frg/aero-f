#ifndef _TIMER_H_
#define _TIMER_H_

class Communicator;
class IoData;

//------------------------------------------------------------------------------

class Timer {

  enum TimerIndex {
    setup, rtree, run, total, fluid, timeStep, nodalGrad, fvTerm, feTerm, fvJac,
    feJac, les, h2Assembly, mvpEval, mvpApply,
    fluidPrecSetup, fluidPrecFactor, fluidPrecApply, fluidKsp, periodicBC,
    meshMetrics, structUpd, mesh, meshAssembly, meshPrecSetup, meshKsp,
    podConstr, snapsLinSolv, padeReconstr, correlMatrix, eigSolv, gramSchmidt,
    romSol, romConstr, romTimeInteg, waitrec,
    comm, localCom, globalCom, interCom, barrier, io, binread, binwrite, postScalar, postVector,
    levelSet, lsNodalWeightsAndGrad, lsFvTerm, lsKsp, lsPrecSetup, lsJac, lsreinitialization,
    eulerFSI, embedSetup, intersect, embedPhaseChange, embedforce, walldistance, gap, popGhost,
    amr, hessian, metric, selectionCri, refineMesh, coarsenMesh, updateMetric, assignGlNum, reinitRefTag, compressGlNum,
    resize, outputMesh, adaptiveRepart, balancing, reintersect, recomputewall,
    romOffline, generateSnaps, pod, als, projError, nodeSelection, reducedMesh, gnatPrepro,
    romOnline, residual, jacAction, romLinearSystem, romCholesky, romNeighbor,
    romNeighborDists, romNeighborFactor, romNeighborPrecompute, restriction, switchProj, applyMask, riemannAvg,
    sensAnalysis, sensOps, derivFlux, derivEfforts, sensLinSolv, innerProdL2, meshLoading, unbalancing,
    supermesh, quadManifoldGrad, quadManifoldEval, quadManifoldLoad, quadManifoldSwitch, quadManifoldDist,
    reducedCoordNNEval, reducedCoordNNJac, limiter, limiterJac, NUMTIMINGS};

  const int NUMEXTRATIMINGS;

  int numTimings;

  double t0, initialTime;

  int *counter;
  double *data;

  IoData *ioData;

  Communicator *com;

 public:

  Timer(Communicator *);
  ~Timer();

  void reset() { t0 = getTime(); }

  void getData(double&, int *&, double *&);
  void setData(double, int *, double *);

  double getTime();
  double getTimeSyncro();
  double getRunTime();

  void setIoData(IoData& _ioData);
  void setSetupTime();
  void setRunTime();
  double subtractSetupTime(double);

  // for fluid solution
  double addFluidSolutionTime(double);
  double addTimeStepTime(double);
  double addNodalGradTime(double);
  double addLimiterTime(double);
  double addLimiterJacTime(double);
  double addRiemannAveragesTime(double);
  double addFiniteVolumeTermTime(double);
  double addFiniteElementTermTime(double);
  double addFiniteVolumeJacTime(double);
  double addFiniteElementJacTime(double);
  double addLESTime(double);
  double addH2SetupTime(double);
  double addMvpEvalTime(double);
  double addMvpApplyTime(double);
  double addPrecSetupTime(double);
  double addPrecFactorTime(double);
  double addPrecApplyTime(double);
  double addKspTime(double);
  double addPeriodicBCTime(double);
  double addRTreeTime(double);

  // for com
  double addLocalComTime(double);
  double addGlobalComTime(double);
  double addInterComTime(double);
  double addBarrierTime(double);

  // for post-processing and i/o
  double addBinaryReadTime(double);
  double addBinaryWriteTime(double);
  double addPostScalarTime(double);
  double addPostVectorTime(double);

  // for level-set
  double addLevelSetSolutionTime(double);
  double addLSNodalWeightsAndGradTime(double);
  double addLSFiniteVolumeTermTime(double);
  double addLSKspTime(double);
  double addLSPrecSetupTime(double);
  double addLSFiniteVolumeJacTime(double);
  double addLSReinitializationTime(double);

  // for AMR
  double addAmrTime(double);
  double addHessianTime(double);
  double addMetricTime(double);
  double addSelectionCriTime(double);
  double addRefineMeshTime(double);
  double addCoarsenMeshTime(double);
  double addUpdateMetricTime(double);
  double addAssignGlNumTime(double);
  double addReinitRefTagTime(double);
  double addCompressGlNumTime(double, int);
  double addResizeTime(double);
  double addOutputMeshTime(double);
  double addAdaptiveRepartTime(double);
  double addBalancingTime(double);
  double addReIntersectTime(double);
  double addRecomputeWallDistanceTime(double);

  // for embedded
  double addEmbeddedSetupTime(double);
  double addIntersectionTime(double, bool);
  double addEmbedPhaseChangeTime(double);
  double addEmbeddedForceTime(double);
  double addWallDistanceTime(double);
  double addWallDistanceTimeAndCount(double);
  double addGapTime(double);
  double addPopGhostTime(double);
  double removeForceAndDispComm(double);

  // for nonlinear ROM
  double addRomOfflineTime(double);
  double addGenerateSnapshotsTime(double);
  double addPodTime(double);
  double addAlsTime(double);
  double addProjErrorTime(double);
  double addNodeSelectionTime(double);
  double addReducedMeshTime(double);
  double addGnatMatrixTime(double);
  double addResidualTime(double);
  double addJacActionTime(double);
  double addRomLinearSolverTime(double);
  double addRomCholeskyTime(double);
  double addRomNearestNeighborBasisTime(double);
  double addRomNearestNeighborDistTime(double);
  double addRomNearestNeighborFactorTime(double);
  double addRomNearestNeighborPrecomputeTime(double);
  double addRestrictionTime(double);
  double addSwitchProjTime(double);
  double addApplyMaskTime(double);
  double addInnerProdL2Time(double);
  double addMeshLoadingTime(double);
  double addUnbalancingTime(double);
  double addSupermeshTime(double);
  double addQuadManifoldGradTime(double);
  double addManifoldEvalTime(double);
  //quadManifoldLoad, quadManifoldSwitch, quadManifoldDist
  double addManifoldLoadTime(double);
  double addQuadManifoldSwitchTime(double);
  double addQuadManifoldDistTime(double);
  double addReducedCoordNNEvalTime(double);
  double addReducedCoordNNJacTime(double);

  // mesh motion
  double addWaitAndReceiveDisp(double);
  double addStructUpdTime(double);
  double addMeshSolutionTime(double);
  double addMeshAssemblyTime(double);
  double addMeshPrecSetupTime(double);
  double addMeshKspTime(double);
  double addMeshMetricsTime(double);

  // linearized
  double addPodConstrTime(double);
  double addSnapsLinSolvTime(double);
  double addPadeReconstrTime(double);
  double addCorrelMatrixTime(double);
  double addEigSolvTime(double);
  double addGramSchmidtTime(double);
  double addRomConstrTime(double);
  double addRomTimeIntegTime(double);

  // sensitivity analysis
  double addSensAnalysisTime(double);
  double addSensOpsTime(double);
  double addDerivFluxTime(double);
  double addDerivEffortsTime(double);
  double addSensLinSolvTime(double);

  double addExtraTime(int, double);

  void print(Timer *, FILE * = stdout);

};

//------------------------------------------------------------------------------

#endif

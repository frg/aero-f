#ifndef _FEM_EQUATION_TERM_RANS_H_
#define _FEM_EQUATION_TERM_RANS_H_

#include <FemEquationTerm.h>

class IoData;
class VarFcn;
class NavierStokesTerm;
class SATerm;
class DESTerm;
class KEpsilonTerm;

struct Vec3D;

//------------------------------------------------------------------------------

class FemEquationTermRANS : public FemEquationTermTurb, public FemEquationTerm {

 protected:
  NavierStokesTerm *ns;

 public:
  FemEquationTermRANS(IoData&, VarFcn *);
  virtual ~FemEquationTermRANS();

  NavierStokesTerm *getNavierStokesTerm() {
    return ns;
  }

  virtual bool doesSourceTermExist() {
    return true;
  }

};

//------------------------------------------------------------------------------

class FemEquationTermSAbase : public FemEquationTermRANS {

 protected:
  SATerm *sa;

 public:
  FemEquationTermSAbase(IoData&, VarFcn *);
  virtual ~FemEquationTermSAbase();

  void rstVar(IoData&);

};

//------------------------------------------------------------------------------

class FemEquationTermSA : public FemEquationTermSAbase {

 public:
  FemEquationTermSA(IoData&, VarFcn *);
  virtual ~FemEquationTermSA() {}

  double computeViscosity(double *, int);

  void computeJacobianViscosity(double *, double *, int);

  double computeViscousTimeStep(double *, double *, int);

  bool computeVolumeTerm(double, double[4][3], double[4][3], double[4], double *[4], int, int, int,
                         Vec3D&, double *, int, double, double *, double *, double *, double = 1.0);

  bool computeDerivativeOfVolumeTerm(double, double, double[4][3], double[4][3], double[4][3],
                                     double[4][3], double[4], double[4], double *[4], double *[4],
                                     double, int, int, int,
                                     Vec3D&, Vec3D&, double *, int, double, double,
                                     double *, double *, double *, double = 1.0, double = 0.0);

  void computeDerivativeOperatorsOfVolumeTerm(double[4][3], double[4][3], double[4], double *[4], int,
                                              int, double[3][6][4][3], double[3][6], double[6][4][3],
                                              double[6], double[6][4][3]);

  bool computeJacobianVolumeTerm(double, double[4][3], double[4][3], double[4], double *[4], int,
                                 int, int, Vec3D&, double *, int, double, double *, double *,
                                 double *, double = 1.0);

  void computeSurfaceTerm(int, double[3], double *[3], double *, int, Vec3D&, double *);

  void computeDerivativeOfSurfaceTerm(int, double[3], double *[3], double *[3], double *, double *,
                                      int, Vec3D&, Vec3D&, double, double *);

  void computeDerivativeOperatorsOfSurfaceTerm(int, double[3], double *[3], double *, int, Vec3D&,
                                               double[][3], double[][3], double[][3], double[][3]);

  void computeJacobianSurfaceTerm(int, double[3], double *[3], double *, int, Vec3D&, double *);

  void computeSurfaceTerm(int, double[4][3], double *[4], double *, int, Vec3D&, double *);

  void computeDerivativeOfSurfaceTerm(int, double[4][3], double[4][3], double *[4], double *[4], double *,
                                      double *, int, Vec3D&, Vec3D&, double, double *);

  void computeDerivativeOperatorsOfSurfaceTerm(int, double[4][3], double *[4], double *, int, Vec3D&,
                                               double[][4][3], double[][3], double[]);

  void computeJacobianSurfaceTerm(int, double[4][3], double *[4], double *, int, Vec3D&, double *);

  void computeDistanceDerivativeOfVolumeTerm(int, double[4][3], double[4][3], double[4], double *[4],
                                             int, double&);

  void computeBCsJacobianWallValues(int, Vec3D&, double[3], double *, double *, double *[3], int);

};

//------------------------------------------------------------------------------

class FemEquationTermSAmean : public FemEquationTermSAbase {

 public:
  FemEquationTermSAmean(IoData&, VarFcn *);
  virtual ~FemEquationTermSAmean() {}

  bool computeJacobianVolumeTerm(double, double[4][3], double[4][3], double[4], double *[4], int,
                                 int, int, Vec3D&, double *, int, double, double *, double *,
                                 double *, double = 1.0);

  void computeJacobianSurfaceTerm(int, double[3], double *[3], double *, int, Vec3D&, double *);

  void computeJacobianSurfaceTerm(int, double[4][3], double *[4], double *, int, Vec3D&, double *);

  bool doesSourceTermExist() {
    return false;
  }

};

//------------------------------------------------------------------------------

class FemEquationTermSAturb : public FemEquationTermSAbase {

 public:
  FemEquationTermSAturb(IoData&, VarFcn *);
  virtual ~FemEquationTermSAturb() {}

  bool computeJacobianVolumeTerm(double, double[4][3], double[4][3], double[4], double *[4], int,
                                 int, int, Vec3D&, double *, int, double, double *, double *,
                                 double *, double = 1.0);

  bool doesFaceTermExist(int) {
    return false;
  }

  bool doesFaceNeedGradientP1Function(int) {
    return false;
  }

};

//------------------------------------------------------------------------------

class FemEquationTermDES : public FemEquationTermSA {

 public:
  FemEquationTermDES(IoData&, VarFcn *);
  ~FemEquationTermDES() {}

};

//------------------------------------------------------------------------------

class FemEquationTermDESmean : public FemEquationTermSAmean {

 public:
  FemEquationTermDESmean(IoData&, VarFcn *);
  ~FemEquationTermDESmean() {}

};

//------------------------------------------------------------------------------

class FemEquationTermDESturb : public FemEquationTermSAturb {

 public:
  FemEquationTermDESturb(IoData&, VarFcn *);
  ~FemEquationTermDESturb() {}

};

//------------------------------------------------------------------------------

class FemEquationTermKEbase : public FemEquationTermRANS {

 protected:
  KEpsilonTerm *ke;

 public:
  FemEquationTermKEbase(IoData&, VarFcn *);
  virtual ~FemEquationTermKEbase();

  void rstVar(IoData&);

};

//------------------------------------------------------------------------------

class FemEquationTermKE : public FemEquationTermKEbase {

 public:
  FemEquationTermKE(IoData&, VarFcn *);
  ~FemEquationTermKE() {}

  double computeViscosity(double *, int);

  void computeJacobianViscosity(double *, double *, int);

  double computeViscousTimeStep(double *, double *, int);

  bool computeVolumeTerm(double, double[4][3], double[4][3], double[4], double *[4], int, int, int,
                         Vec3D&, double *, int, double, double *, double *, double *, double = 1.0);

  bool computeDerivativeOfVolumeTerm(double, double, double[4][3], double[4][3], double[4][3],
                                     double[4][3], double[4], double[4], double *[4], double *[4],
                                     double, int, int, int,
                                     Vec3D&, Vec3D&, double *, int, double, double,
                                     double *, double *, double *, double = 1.0, double = 0.0);

  void computeDerivativeOperatorsOfVolumeTerm(double[4][3], double[4][3], double[4], double *[4], int,
                                              int, double[3][7][4][3], double[3][7], double[7][4][3],
                                              double[7], double[7][4][3]);

  bool computeJacobianVolumeTerm(double, double[4][3], double[4][3], double[4], double *[4], int,
                                 int, int, Vec3D&, double *, int, double, double *, double *,
                                 double *, double = 1.0);

  void computeSurfaceTerm(int, double[3], double *[3], double *, int, Vec3D&, double *);

  void computeDerivativeOfSurfaceTerm(int, double[3], double *[3], double *[3], double *, double *,
                                      int, Vec3D&, Vec3D&, double, double *);

  void computeDerivativeOperatorsOfSurfaceTerm(int, double[3], double *[3], double *, int, Vec3D&,
                                               double[][3], double[][3], double[][3], double[][3]);

  void computeJacobianSurfaceTerm(int, double[3], double *[3], double *, int, Vec3D&, double *);

  void computeBCsJacobianWallValues(int, Vec3D&, double[3], double *, double *, double *[3], int);

};

//------------------------------------------------------------------------------

class FemEquationTermKEmean : public FemEquationTermKEbase {

 public:
  FemEquationTermKEmean(IoData&, VarFcn *);
  ~FemEquationTermKEmean() {}

  bool computeJacobianVolumeTerm(double, double[4][3], double[4][3], double[4], double *[4], int,
                                 int, int, Vec3D&, double *, int, double, double *, double *,
                                 double *, double = 1.0);

  void computeJacobianSurfaceTerm(int, double[3], double *[3], double *, int, Vec3D&, double *);

  bool doesSourceTermExist() {
    return false;
  }

};

//------------------------------------------------------------------------------

class FemEquationTermKEturb : public FemEquationTermKEbase {

 public:
  FemEquationTermKEturb(IoData&, VarFcn *);
  ~FemEquationTermKEturb() {}

  bool computeJacobianVolumeTerm(double, double[4][3], double[4][3], double[4], double *[4], int,
                                 int, int, Vec3D&, double *, int, double, double *, double *,
                                 double *, double = 1.0);

  bool doesFaceTermExist(int) {
    return false;
  }

  bool doesFaceNeedGradientP1Function(int) {
    return false;
  }

};

//------------------------------------------------------------------------------

#endif

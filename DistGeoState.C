#include <DistGeoState.h>

#include <IoData.h>
#include <Domain.h>
#include <SubDomain.h>
#include <GeoState.h>
#include <GeometricQuery.h>
#include <Vector3D.h>
#include <DistVector.h>
#include <Communicator.h>
#include <TimeData.h>
#include <TsRestart.h>
#include <FSI/DynamicNodalTransfer.h>
#include <LevelSet/LevelSetStructure.h>

#include <Eigen/Core>

#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <map>
#include <string>
#include <vector>

//------------------------------------------------------------------------------

DistGeoState::DistGeoState(IoData& iod, Domain *dom) : data(iod), ioData(iod), domain(dom), triangleSet(nullptr) {
  numLocSub = domain->getNumLocSub();
  com = domain->getCommunicator();
  lscale = ioData.ref.rv.tlength;
  oolscale = 1.0 / lscale;
  if(data.use_n) {
    Xn = new DistSVec<double, 3>(domain->getNodeDistInfo());
    ctrlVol_n = new DistVec<double>(domain->getNodeDistInfo());
    // initialize the values
    *Xn = 0.0;
    *ctrlVol_n = 0.0;
  }
  else {
    Xn = 0;
    ctrlVol_n = 0;
  }
  if(data.use_nm1) {
    Xnm1 = new DistSVec<double, 3>(domain->getNodeDistInfo());
    ctrlVol_nm1 = new DistVec<double>(domain->getNodeDistInfo());
    // initialize the values
    *Xnm1 = 0.0;
    *ctrlVol_nm1 = 0.0;
  }
  else {
    Xnm1 = 0;
    ctrlVol_nm1 = 0;
  }
  if(data.use_nm2) {
    Xnm2 = new DistSVec<double, 3>(domain->getNodeDistInfo());
    ctrlVol_nm2 = new DistVec<double>(domain->getNodeDistInfo());
    // initialize the values
    *Xnm2 = 0.0;
    *ctrlVol_nm2 = 0.0;
  }
  else {
    Xnm2 = 0;
    ctrlVol_nm2 = 0;
  }
  Xdot = new DistSVec<double, 3>(domain->getNodeDistInfo());
  if(data.use_save) { // true only if explicit with moving mesh
    ctrlVol_save = new DistVec<double>(domain->getNodeDistInfo());
    Xsave = new DistSVec<double, 3>(domain->getNodeDistInfo());
  }
  else {
    ctrlVol_save = 0;
    Xsave = 0;
  }
  d2wall = new DistVec<double>(domain->getNodeDistInfo());
  dd2wall = 0;
  d2wall_nmp = 0;
  if(strcmp(ioData.romOffline.arom.d2wall, "") != 0) {
    domain->readVectorFromFile(ioData.romOffline.arom.d2wall, 0, 0, *d2wall, &oolscale);
  }
  else if(ioData.input.restart_file_package[0] == 0 && ioData.input.d2wall[0] != 0) {
    char *name = new char[strlen(ioData.input.prefix) + strlen(ioData.input.d2wall) + 1];
    sprintf(name, "%s%s", ioData.input.prefix, ioData.input.d2wall);
    domain->readVectorFromFile(name, 0, 0, *d2wall, &oolscale);
    delete [] name;
  }
  else if(ioData.input.restart_file_package[0] != 0) {
    char dummy[256], tmp[256], name[256];
    sprintf(tmp, "%s%s", ioData.input.prefix, ioData.input.restart_file_package);
    TsRestart::readRestartFileNames(tmp, dummy, dummy, dummy, dummy, dummy, dummy, name, dummy, dummy, NULL);
    domain->readVectorFromFile(name, 0, 0, *d2wall, &oolscale);
  }
  else {
    *d2wall = 0.0;
  }
  if(ioData.problem.type[ProblemData::EMBEDDED_FRAMEWORK] && ioData.input.d2bfwall[0] != 0) {
    d2bfwall = new DistVec<double>(domain->getNodeDistInfo());
    char *name = new char[strlen(ioData.input.prefix) + strlen(ioData.input.d2bfwall) + 1];
    sprintf(name, "%s%s", ioData.input.prefix, ioData.input.d2bfwall);
    domain->readVectorFromFile(name, 0, 0, *d2bfwall, &oolscale);
    delete [] name;
  }
  else {
    d2bfwall = 0;
  }
  edgeNorm    = new DistVec<Vec3D>(domain->getEdgeDistInfo());
  edgeNormVel = new DistVec<double>(domain->getEdgeDistInfo());
  edgeNorm_nm1    = 0;
  edgeNormVel_nm1 = 0;
  edgeNorm_nm2    = 0;
  edgeNormVel_nm2 = 0;
  faceNorm    = new DistVec<Vec3D>(domain->getFaceNormDistInfo());
  faceNormVel = new DistVec<double>(domain->getFaceNormDistInfo());
  faceNorm_nm1    = 0;
  faceNormVel_nm1 = 0;
  faceNorm_nm2    = 0;
  faceNormVel_nm2 = 0;
  if(data.typeNormals == DGCLData::IMPLICIT_SECOND_ORDER_GCL) {
    edgeNorm_nm1    = new DistVec<Vec3D>(domain->getEdgeDistInfo());
    edgeNormVel_nm1 = new DistVec<double>(domain->getEdgeDistInfo());
    faceNorm_nm1    = new DistVec<Vec3D>(domain->getFaceNormDistInfo());
    faceNormVel_nm1 = new DistVec<double>(domain->getFaceNormDistInfo());
  }
  else if(data.typeNormals == DGCLData::IMPLICIT_SECOND_ORDER_EZGCL) {
    edgeNormVel_nm1 = new DistVec<double>(domain->getEdgeDistInfo());
    faceNormVel_nm1 = new DistVec<double>(domain->getFaceNormDistInfo());
  }
  else if(data.typeNormals == DGCLData::IMPLICIT_THIRD_ORDER_EZGCL) {
    edgeNormVel_nm1 = new DistVec<double>(domain->getEdgeDistInfo());
    edgeNormVel_nm2 = new DistVec<double>(domain->getEdgeDistInfo());
    faceNormVel_nm1 = new DistVec<double>(domain->getFaceNormDistInfo());
    faceNormVel_nm2 = new DistVec<double>(domain->getFaceNormDistInfo());
  }
  ctrlVol = new DistVec<double>(domain->getNodeDistInfo());
  X       = new DistSVec<double, 3>(domain->getNodeDistInfo());
  if(ioData.problem.type[ProblemData::LINEARIZED] && ioData.linearizedData.addMeshDerivative == LinearizedData::ON_DFDX) {
    dX = new DistSVec<double, 3>(domain->getNodeDistInfo());
  }
  else {
    dX = 0;
  }
  dXdot = 0;
  dCtrlVol = 0;
  dEdgeNorm = 0;
  dFaceNorm = 0;
  dEdgeNormVel = 0;
  dFaceNormVel = 0;
  edgeUpwindDirections = 0;
  faceUpwindDirections = 0;
  if(ioData.schemes.ns.rotatedRiemann.preCompute == SchemeData::RotatedRiemannData::PRE_COMPUTE_ON) {
    if(ioData.schemes.ns.flux == SchemeData::ROTATED_RIEMANN) {
      edgeUpwindDirections = new DistSVec<double, 6>(domain->getEdgeDistInfo());
      *edgeUpwindDirections = 0.0;
    }
    if(((ioData.bc.wall.method == BcsWallData::EXACT_RIEMANN_PROBLEM) && (ioData.bc.wall.erwData.flux == ExactRiemannWallData::ROTATED_RIEMANN)) ||
       (ioData.surfaces.surface_type(SurfaceData::PERIODIC) && (ioData.schemes.ns.flux == SchemeData::ROTATED_RIEMANN))) {
      faceUpwindDirections = new DistSVec<double, Face::MaxNumNd * 3>(domain->getFaceDistInfo());
      *faceUpwindDirections = 0.0;
    }
    if(ioData.schemes.ns.rotatedRiemann.upwindDirection == SchemeData::RotatedRiemannData::TRIANGLE_SET) {
      if(ioData.input.triangleSet[0] != 0) {
        computeUpwindDirections();
      }
    }
    else {
      if(ioData.input.edgeupwinddirections[0] != 0) {
        char *name = new char[strlen(ioData.input.prefix) + strlen(ioData.input.edgeupwinddirections) + 1];
        sprintf(name, "%s%s", ioData.input.prefix, ioData.input.edgeupwinddirections);
        domain->readVectorFromFile(name, *edgeUpwindDirections);
      }
      if(ioData.input.faceupwinddirections[0] != 0) {
        char *name = new char[strlen(ioData.input.prefix) + strlen(ioData.input.faceupwinddirections) + 1];
        sprintf(name, "%s%s", ioData.input.prefix, ioData.input.faceupwinddirections);
        domain->readVectorFromFile(name, *faceUpwindDirections);
      }
    }
  }
  subGeoState = new GeoState*[numLocSub];
  #pragma omp parallel for
  for(int iSub = 0; iSub < numLocSub; ++iSub) {
    subGeoState[iSub] = 0;
    if(d2bfwall || !ioData.problem.type[ProblemData::EMBEDDED_FRAMEWORK]) {
      Vec<double>& d2w = (!ioData.problem.type[ProblemData::EMBEDDED_FRAMEWORK]) ? (*d2wall)(iSub) : (*d2bfwall)(iSub);
      for(int i = 0; i < d2w.size(); ++i) {
        d2w[i] += ioData.bc.wall.delta;
      }
    }
  }
  // in case ReinitializeDistanceToWall::ComputeWallFunction is never called (e.g. no turbulence model)
  if(d2bfwall && ioData.problem.type[ProblemData::EMBEDDED_FRAMEWORK]) *d2wall = *d2bfwall;
}

//------------------------------------------------------------------------------

DistGeoState::~DistGeoState() {
  if(data.use_n && Xn) {
    delete Xn;
  }
  if(data.use_nm1 && Xnm1) {
    delete Xnm1;
  }
  if(data.use_nm2 && Xnm2) {
    delete Xnm2;
  }
  if(Xdot) {
    delete Xdot;
  }
  if(Xsave) {
    delete Xsave;
  }
  if(data.use_n && ctrlVol_n) {
    delete ctrlVol_n;
  }
  if(data.use_nm1 && ctrlVol_nm1) {
    delete ctrlVol_nm1;
  }
  if(data.use_nm2 && ctrlVol_nm2) {
    delete ctrlVol_nm2;
  }
  if(ctrlVol_save) {
    delete ctrlVol_save;
  }
  if(d2wall) {
    delete d2wall;
  }
  if(d2bfwall) {
    delete d2bfwall;
  }
  if(dd2wall) {
    delete dd2wall;
  }
  if(d2wall_nmp) {
    delete d2wall_nmp;
  }
  if(edgeNorm) {
    delete edgeNorm;
  }
  if(edgeNormVel) {
    delete edgeNormVel;
  }
  if(edgeNorm_nm1) {
    delete edgeNorm_nm1;
  }
  if(edgeNormVel_nm1) {
    delete edgeNormVel_nm1;
  }
  if(edgeNorm_nm2) {
    delete edgeNorm_nm2;
  }
  if(edgeNormVel_nm2) {
    delete edgeNormVel_nm2;
  }
  if(faceNorm) {
    delete faceNorm;
  }
  if(faceNormVel) {
    delete faceNormVel;
  }
  if(faceNorm_nm1) {
    delete faceNorm_nm1;
  }
  if(faceNormVel_nm1) {
    delete faceNormVel_nm1;
  }
  if(faceNorm_nm2) {
    delete faceNorm_nm2;
  }
  if(faceNormVel_nm2) {
    delete faceNormVel_nm2;
  }
  if(ctrlVol) {
    delete ctrlVol;
  }
  if(X) {
    delete X;
  }
  if(dX) {
    delete dX;
  }
  if(dXdot) {
    delete dXdot;
  }
  if(dCtrlVol) {
    delete dCtrlVol;
  }
  if(dEdgeNorm) {
    delete dEdgeNorm;
  }
  if(dFaceNorm) {
    delete dFaceNorm;
  }
  if(dEdgeNormVel) {
    delete dEdgeNormVel;
  }
  if(dFaceNormVel) {
    delete dFaceNormVel;
  }
  if(edgeUpwindDirections) {
    delete edgeUpwindDirections;
  }
  if(faceUpwindDirections) {
    delete faceUpwindDirections;
  }
  if(triangleSet) {
    delete triangleSet;
  }
  if(subGeoState) {
    #pragma omp parallel for
    for(int iSub = 0; iSub < numLocSub; ++iSub) {
      if(subGeoState[iSub]) {
        delete subGeoState[iSub];
      }
    }
    delete [] subGeoState;
  }
}

//------------------------------------------------------------------------------

void DistGeoState::resize1(NodeData *nodeData) {
  d2wall->resize(domain->getNodeDistInfo(), nodeData, 3, 0.);
  if(d2wall_nmp) d2wall_nmp->resize(domain->getNodeDistInfo(), nodeData);
  if(d2bfwall) d2bfwall->resize(domain->getNodeDistInfo(), nodeData);
  X->resize(domain->getNodeDistInfo(), nodeData);
  if(data.use_n) Xn->resize(domain->getNodeDistInfo(), nodeData);
  if(data.use_nm1) Xnm1->resize(domain->getNodeDistInfo(), nodeData);
  if(data.use_nm2) Xnm2->resize(domain->getNodeDistInfo(), nodeData);
  if(Xsave) Xsave->resize(domain->getNodeDistInfo(), nodeData);
}

//------------------------------------------------------------------------------

void DistGeoState::resize2(bool reinit, TimeData& timeData) {
  Xdot->resize(domain->getNodeDistInfo());
  edgeNorm->resize(domain->getEdgeDistInfo());
  edgeNormVel->resize(domain->getEdgeDistInfo());
  if(edgeNorm_nm1) edgeNorm_nm1->resize(domain->getEdgeDistInfo());
  if(edgeNormVel_nm1) edgeNormVel_nm1->resize(domain->getEdgeDistInfo());
  if(edgeNorm_nm2) edgeNorm_nm2->resize(domain->getEdgeDistInfo());
  if(edgeNormVel_nm2) edgeNormVel_nm2->resize(domain->getEdgeDistInfo());
  faceNorm->resize(domain->getFaceNormDistInfo());
  faceNormVel->resize(domain->getFaceNormDistInfo());
  if(faceNorm_nm1) faceNorm_nm1->resize(domain->getFaceNormDistInfo());
  if(faceNormVel_nm1) faceNormVel_nm1->resize(domain->getFaceNormDistInfo());
  if(faceNorm_nm2) faceNorm_nm2->resize(domain->getFaceNormDistInfo());
  if(faceNormVel_nm2) faceNormVel_nm2->resize(domain->getFaceNormDistInfo());
  ctrlVol->resize(domain->getNodeDistInfo());
  if(data.use_n) ctrlVol_n->resize(domain->getNodeDistInfo());
  if(data.use_nm1) ctrlVol_nm1->resize(domain->getNodeDistInfo());
  if(data.use_nm2) ctrlVol_nm2->resize(domain->getNodeDistInfo());
  if(ctrlVol_save) ctrlVol_save->resize(domain->getNodeDistInfo());
  if(reinit) {
    int ierr = domain->computeControlVolumes(lscale, *X, *ctrlVol);
    if(data.use_n) {
      int ierr = domain->computeControlVolumes(lscale, *Xn, *ctrlVol_n);
    }
    if(data.use_nm1) {
      int ierr = domain->computeControlVolumes(lscale, *Xnm1, *ctrlVol_nm1);
    }
    if(data.use_nm2) {
      int ierr = domain->computeControlVolumes(lscale, *Xnm2, *ctrlVol_nm2);
    }
    if(ctrlVol_save) {
      domain->computeControlVolumes(lscale, *Xsave, *ctrlVol_save);
    }
    double oodtnm1 = 1.0 / timeData.dt_nm1;
    double oodtnm2 = 1.0 / timeData.dt_nm2;
    if(data.typeVelocities == DGCLData::IMPLICIT_ZERO ||
       data.typeVelocities == DGCLData::EXPLICIT_RK2_VEL) {
      *Xdot = 0.0;
    }
    else {
      *Xdot = oodtnm1 * (*Xn - *Xnm1);
    }
    domain->computeNormalsGCL1(*Xnm1, *Xn, *Xdot, *edgeNorm, *edgeNormVel, *faceNorm, *faceNormVel);
    if(data.typeNormals == DGCLData::IMPLICIT_SECOND_ORDER_GCL) {
      domain->computeNormalsGCL1(*Xnm1, *Xn, *Xdot, *edgeNorm_nm1, *edgeNormVel_nm1,
                                 *faceNorm_nm1, *faceNormVel_nm1);
    }
    else if(data.typeNormals == DGCLData::IMPLICIT_SECOND_ORDER_EZGCL) {
      domain->computeNormalsEZGCL1(oodtnm1, *Xnm1, *Xn, *edgeNorm, *edgeNormVel_nm1,
                                   *faceNorm, *faceNormVel_nm1);
    }
    else if(data.typeNormals == DGCLData::IMPLICIT_THIRD_ORDER_EZGCL) {
      domain->computeNormalsEZGCL1(oodtnm2, *Xnm2, *Xnm1, *edgeNorm, *edgeNormVel_nm2,
                                   *faceNorm, *faceNormVel_nm2);
      domain->computeNormalsEZGCL1(oodtnm1, *Xnm1, *Xn, *edgeNorm, *edgeNormVel_nm1,
                                   *faceNorm, *faceNormVel_nm1);
    }
  }
  if(ioData.schemes.ns.rotatedRiemann.preCompute == SchemeData::RotatedRiemannData::PRE_COMPUTE_ON) {
    if(edgeUpwindDirections) {
      edgeUpwindDirections->resize(domain->getEdgeDistInfo());
      *edgeUpwindDirections = 0.0;
    }
    if(faceUpwindDirections) {
      faceUpwindDirections->resize(domain->getFaceDistInfo());
      *faceUpwindDirections = 0.0;
    }
    if(ioData.schemes.ns.rotatedRiemann.upwindDirection == SchemeData::RotatedRiemannData::TRIANGLE_SET) {
      if(ioData.input.triangleSet[0] != 0) {
        computeUpwindDirections();
      }
    }
    else {
      //if(edgeUpwindDirections) com->fprintf(stderr, "*** Warning: pre-computed edge upwind directions reset to zero.\n");
      //if(faceUpwindDirections) com->fprintf(stderr, "*** Warning: pre-computed face upwind directions reset to zero.\n");
    }
  }
}

//------------------------------------------------------------------------------

void DistGeoState::setup(IoData& ioData, const char *name, TimeData& timeData, double *_bbMin, double *_bbMax) {
  setup1(ioData, name);
  setup2(ioData, timeData, _bbMin, _bbMax);
}

//------------------------------------------------------------------------------

void DistGeoState::setup1(IoData& ioData, const char *name) {
  if(!data.use_n) {
    Xn = X;
    ctrlVol_n = ctrlVol;
  }
  if(!data.use_nm1) {
    Xnm1 = Xn;
    ctrlVol_nm1 = ctrlVol_n;
  }
  if(!data.use_nm2) {
    Xnm2 = Xnm1;
    ctrlVol_nm2 = ctrlVol_nm1;
  }
  if(!ioData.amr.onthefly_restart) {
    bool read_n = false;
    bool read_nm1 = false;
    bool read_nm2 = false;
    // This effectively `restarts' the geometry of the mesh, recovering the
    // last 3 positions.
    if(name[0] != 0) {
      if(ioData.problem.framework == ProblemData::EMBEDDED) {
        com->fprintf(stderr, "*** Warning: Initialization of mesh position is not supported for embedded framework.\n");
      }
      else {
        read_n = domain->readVectorFromFile(name, 0, 0, *Xn, &oolscale);
        if(data.use_nm1) {
          read_nm1 = domain->readVectorFromFile(name, 1, 0, *Xnm1, &oolscale);
        }
        if(data.use_nm2) {
          read_nm2 = domain->readVectorFromFile(name, 2, 0, *Xnm2, &oolscale);
        }
      }
    }
    if(!read_n) {
      domain->getReferenceMeshPosition(*Xn);
    }
    if(data.use_nm1 && !read_nm1) {
      *Xnm1 = *Xn;
    }
    if(data.use_nm2 && !read_nm2) {
      *Xnm2 = *Xnm1;
    }
  }
  data.config = 0;
  data.configSA = 0;
  int ierr = domain->computeControlVolumes(lscale, *Xn, *ctrlVol_n);
  if(data.use_n) {
    *X = *Xn;
    *ctrlVol = *ctrlVol_n;
  }
  if(data.use_nm1) {
    ierr = domain->computeControlVolumes(lscale, *Xnm1, *ctrlVol_nm1);
  }
  if(data.use_nm2) {
    ierr = domain->computeControlVolumes(lscale, *Xnm2, *ctrlVol_nm2);
  }
  if(!ioData.amr.onthefly_restart && name[0] != 0) {
    domain->reconstructRTree(this);
  }
}

//------------------------------------------------------------------------------

void DistGeoState::setup2(IoData& ioData, TimeData& timeData, double *_bbMin, double *_bbMax) {
  double oodtnm1 = 1.0 / timeData.dt_nm1;
  double oodtnm2 = 1.0 / timeData.dt_nm2;
  if(data.typeVelocities == DGCLData::IMPLICIT_ZERO ||
     data.typeVelocities == DGCLData::EXPLICIT_RK2_VEL) {
    *Xdot = 0.0;
  }
  else {
    *Xdot = oodtnm1 * (*Xn - *Xnm1);
  }
  if(ctrlVol_save) {
    *ctrlVol_save = *ctrlVol_n;
  }
  if(Xsave) {
    *Xsave = *Xn;
  }
  domain->computeNormalsGCL1(*Xnm1, *Xn, *Xdot, *edgeNorm, *edgeNormVel, *faceNorm, *faceNormVel);
  if(data.typeNormals == DGCLData::IMPLICIT_SECOND_ORDER_GCL) {
    domain->computeNormalsGCL1(*Xnm1, *Xn, *Xdot, *edgeNorm_nm1, *edgeNormVel_nm1,
                               *faceNorm_nm1, *faceNormVel_nm1);
  }
  else if(data.typeNormals == DGCLData::IMPLICIT_SECOND_ORDER_EZGCL) {
    domain->computeNormalsEZGCL1(oodtnm1, *Xnm1, *Xn, *edgeNorm, *edgeNormVel_nm1,
                                 *faceNorm, *faceNormVel_nm1);
  }
  else if(data.typeNormals == DGCLData::IMPLICIT_THIRD_ORDER_EZGCL) {
    domain->computeNormalsEZGCL1(oodtnm2, *Xnm2, *Xnm1, *edgeNorm, *edgeNormVel_nm2,
                                 *faceNorm, *faceNormVel_nm2);
    domain->computeNormalsEZGCL1(oodtnm1, *Xnm1, *Xn, *edgeNorm, *edgeNormVel_nm1,
                                 *faceNorm, *faceNormVel_nm1);
  }

  #pragma omp parallel for
  for(int iSub = 0; iSub < numLocSub; ++iSub) {
    if(!subGeoState[iSub]) {
      subGeoState[iSub] = new GeoState((*X)(iSub), (*ctrlVol)(iSub),
                                       (data.use_n ? (*ctrlVol_n)(iSub) : (*ctrlVol)(iSub)),
                                       (data.use_nm1 ? (*ctrlVol_nm1)(iSub) : (*ctrlVol)(iSub)),
                                       (data.use_nm2 ? (*ctrlVol_nm2)(iSub) : (*ctrlVol)(iSub)),
                                       (*d2wall)(iSub), (*edgeNorm)(iSub), (*faceNorm)(iSub),
                                       (*edgeNormVel)(iSub), (*faceNormVel)(iSub),
                                       (edgeUpwindDirections ? &(*edgeUpwindDirections)(iSub) : nullptr),
                                       (faceUpwindDirections ? &(*faceUpwindDirections)(iSub) : nullptr));
    }
  }
  if(!ioData.amr.onthefly_restart) {
    double bbMin[3] = {std::numeric_limits<double>::infinity(),
                       std::numeric_limits<double>::infinity(),
                       std::numeric_limits<double>::infinity()};
    double bbMax[3] = {-std::numeric_limits<double>::infinity(),
                       -std::numeric_limits<double>::infinity(),
                       -std::numeric_limits<double>::infinity()};
    #pragma omp parallel for
    for(int iSub = 0; iSub < numLocSub; ++iSub) {
      NodeSet& nodes = domain->getSubDomain()[iSub]->getNodes();
      for(int i = 0; i < nodes.size(); ++i) {
        for(int j = 0; j < 3; ++j) {
          bbMin[j] = std::min(bbMin[j], nodes[i][j]);
          bbMax[j] = std::max(bbMax[j], nodes[i][j]);
        }
      }
    }
    com->globalMin(3, bbMin);
    com->globalMax(3, bbMax);
    com->printf(2,
                "Control volume statistics: min=%.3e, max=%.3e, total=%.3e\n"
                "Mesh bounding box: (Xmin,Ymin,Zmin) = (%+.3e %+.3e %+.3e)\n"
                "                   (Xmax,Ymax,Zmax) = (%+.3e %+.3e %+.3e)\n",
                ctrlVol->min(), ctrlVol->max(), ctrlVol->sum(),
                bbMin[0], bbMin[1], bbMin[2], bbMax[0], bbMax[1], bbMax[2]);
    if(_bbMin && _bbMax) {
      for(int i = 0; i < 3; ++i) {
        _bbMin[i] = bbMin[i];
        _bbMax[i] = bbMax[i];
      }
    }
  }
}

//------------------------------------------------------------------------------

void DistGeoState::compute(TimeData& timeData, DistSVec<double, 3>& Xsdot, bool use_dgcl) {
  data.config += 1;
  // ctrlVol has the control volumes of Xnp1
  int ierr = domain->computeControlVolumes(lscale, *X, *ctrlVol);
  // Xdot
  DGCLData::Velocities typeVel = (use_dgcl) ? data.typeVelocities : DGCLData::IMPLICIT_IMPOSED_VEL;
  domain->computeVelocities(typeVel, timeData, Xsdot, *Xnm1, *Xn, *X, *Xdot);
  // normals -> edgeNorm, edgeNormVel, faceNorm, faceNormVel
  if(data.typeNormals == DGCLData::IMPLICIT_FIRST_ORDER_GCL ||
     data.typeNormals == DGCLData::IMPLICIT_SECOND_ORDER_GCL) {
    domain->computeNormalsGCL1(*Xn, *X, *Xdot, *edgeNorm, *edgeNormVel,
                               *faceNorm, *faceNormVel);
    if(data.typeNormals == DGCLData::IMPLICIT_SECOND_ORDER_GCL)
      domain->computeNormalsGCL2(timeData, *edgeNorm, *edgeNorm_nm1, *edgeNormVel,
                                 *edgeNormVel_nm1, *faceNorm, *faceNorm_nm1,
                                 *faceNormVel, *faceNormVel_nm1);
  }
  else if(data.typeNormals == DGCLData::IMPLICIT_FIRST_ORDER_EZGCL ||
          data.typeNormals == DGCLData::IMPLICIT_SECOND_ORDER_EZGCL ||
          data.typeNormals == DGCLData::IMPLICIT_THIRD_ORDER_EZGCL) {
    domain->computeNormalsEZGCL1(1.0 / timeData.dt_n, *Xn, *X, *edgeNorm, *edgeNormVel,
                                 *faceNorm, *faceNormVel);
    if(data.typeNormals == DGCLData::IMPLICIT_SECOND_ORDER_EZGCL)
      domain->computeNormalsEZGCL2(timeData, *edgeNormVel, *edgeNormVel_nm1,
                                   *faceNormVel, *faceNormVel_nm1);
    else if(data.typeNormals == DGCLData::IMPLICIT_THIRD_ORDER_EZGCL)
      domain->computeNormalsEZGCL3(timeData, *edgeNormVel, *edgeNormVel_nm1, *edgeNormVel_nm2,
                                   *faceNormVel, *faceNormVel_nm1, *faceNormVel_nm2);
  }
  else if(data.typeNormals == DGCLData::IMPLICIT_CURRENT_CFG) {
    domain->computeNormalsGCL1(*Xn, *Xn, *Xdot, *edgeNorm, *edgeNormVel,
                               *faceNorm, *faceNormVel);
  }
  else if(data.typeNormals == DGCLData::IMPLICIT_LATEST_CFG) {
    domain->computeNormalsGCL1(*X, *X, *Xdot, *edgeNorm, *edgeNormVel,
                               *faceNorm, *faceNormVel);
  }
  else if(data.typeNormals == DGCLData::EXPLICIT_RK2 && data.use_save) {
    // Xsave is temporarily used to compute the different configurations on which to compute the normals
    // and the normal velocities
    #pragma omp parallel for
    for(int iSub = 0; iSub < numLocSub; ++iSub) {
      Vec<Vec3D>& subEdgeNorm = (*edgeNorm)[iSub];
      for(int i = 0; i < subEdgeNorm.size(); ++i) subEdgeNorm[i] = 0.0;
    }
    *edgeNormVel = 0.0;
    #pragma omp parallel for
    for(int iSub = 0; iSub < numLocSub; ++iSub) {
      Vec<Vec3D>& subFaceNorm = (*faceNorm)[iSub];
      for(int i = 0; i < subFaceNorm.size(); ++i) subFaceNorm[i] = 0.0;
    }
    *faceNormVel = 0.0;
    *Xsave = 0.5 * (1.0 - 1.0 / sqrt(3.0)) * (*X) + 0.5 * (1.0 + 1.0 / sqrt(3.0)) * (*Xn);
    domain->computeNormalsConfig(*Xsave, *Xdot, *edgeNorm, *edgeNormVel, *faceNorm, *faceNormVel, false);
    *Xsave = 0.5 * (1.0 + 1.0 / sqrt(3.0)) * (*X) + 0.5 * (1.0 - 1.0 / sqrt(3.0)) * (*Xn);
    domain->computeNormalsConfig(*Xsave, *Xdot, *edgeNorm, *edgeNormVel, *faceNorm, *faceNormVel, true);
    #pragma omp parallel for
    for(int iSub = 0; iSub < numLocSub; ++iSub) {
      Vec<Vec3D>& subEdgeNorm = (*edgeNorm)[iSub];
      for(int i = 0; i < subEdgeNorm.size(); ++i) subEdgeNorm[i] *= 0.5;
    }
    *edgeNormVel *= 0.5;
    #pragma omp parallel for
    for(int iSub = 0; iSub < numLocSub; ++iSub) {
      Vec<Vec3D>& subFaceNorm = (*faceNorm)[iSub];
      for(int i = 0; i < subFaceNorm.size(); ++i) subFaceNorm[i] *= 0.5;
    }
    *faceNormVel *= 0.5;
    // save values of actual configuration Xnp1
    *Xsave = (*X);
    *ctrlVol_save = *ctrlVol;
    // return values X and ctrlVol on which to compute the fluxes!
    // for Eulerian fluxes, implicit/explicit formulation/coding requires normals and normal velocities
    // for viscous terms, it is always required to have Xconfig which is not necessarily Xnp1!
    (*X) += *Xn;
    (*X) *= 0.5;
    // now X has the configuration on which to compute the fluxes
    // but does not correspond to Xnp1 which will need to be restored
    // at end of iteration!
  }
  else {
    com->fprintf(stderr, "*** Error: Running a mesh moving simulation\n");
    com->fprintf(stderr, "***        but no update of the normals!!!!\n");
    com->fprintf(stderr, "*** Exiting\n");
    exit(1);
  }
}

//------------------------------------------------------------------------------

void DistGeoState::interpolate(double dt, double dtLeft, DistSVec<double, 3>& Xs) {
  double alpha = dt / (dt + dtLeft) - 1.0;
  (*X) = Xs + alpha * (Xs - *Xn);
}

//------------------------------------------------------------------------------

void DistGeoState::update() {
  if(data.use_save) {
    // X and ctrlVol had configuration on which fluxes were computed
    // Xnp1 and ctrlVol_np1 are restored to X and ctrlVol
    *X = *Xsave;
    *ctrlVol = *ctrlVol_save;
  }
  if(data.use_nm2) {
    *Xnm2 = *Xnm1;
    *ctrlVol_nm2 = *ctrlVol_nm1;
  }
  if(data.use_nm1) {
    *Xnm1 = *Xn;
    *ctrlVol_nm1 = *ctrlVol_n;
  }
  if(data.use_n) {
    *Xn = *X;
    *ctrlVol_n = *ctrlVol;
  }
}

//------------------------------------------------------------------------------

void DistGeoState::reset(DistSVec<double, 3>& Xmod) {
  if(Xn) *Xn = Xmod;
  if(Xnm1) *Xnm1 = Xmod;
}

//------------------------------------------------------------------------------

void DistGeoState::writeToDisk(char *name) {
  if(data.use_n) {
    domain->writeVectorToFile(name, 0, 0.0, *Xn, &lscale);
  }
  if(data.use_nm1) {
    domain->writeVectorToFile(name, 1, 0.0, *Xnm1, &lscale);
  }
  if(data.use_nm2) {
    domain->writeVectorToFile(name, 2, 0.0, *Xnm2, &lscale);
  }
}

//------------------------------------------------------------------------------

void DistGeoState::writeWallDistanceToDisk(char *name) {
  if(d2wall) {
    if(ioData.problem.framework == ProblemData::BODYFITTED) {
      com->fprintf(stdout, "Outputting wall distance without delta.\n"); com->barrier();
      for(int iSub = 0; iSub < numLocSub; ++iSub) {
        Vec<double>& d2w = (*d2wall)(iSub);
        for(int i = 0; i < d2w.size(); ++i) {
          d2w[i] -= ioData.bc.wall.delta;
        }
      }
      domain->writeVectorToFile(name, 0, 0.0, *d2wall, &lscale);
      for(int iSub = 0; iSub < numLocSub; ++iSub) {
        Vec<double>& d2w = (*d2wall)(iSub);
        for(int i = 0; i < d2w.size(); ++i) {
          d2w[i] += ioData.bc.wall.delta;
        }
      }
    }
    else {
      domain->writeVectorToFile(name, 0, 0.0, *d2wall, &lscale);
    }
  }
}

//------------------------------------------------------------------------------

void DistGeoState::writeEdgeUpwindDirectionsToDisk(char *name) {
  if(edgeUpwindDirections) {
    domain->writeVectorToFile(name, *edgeUpwindDirections);
  }
}

//------------------------------------------------------------------------------

void DistGeoState::writeFaceUpwindDirectionsToDisk(char *name) {
  if(faceUpwindDirections) {
    domain->writeVectorToFile(name, *faceUpwindDirections);
  }
}

//------------------------------------------------------------------------------

void DistGeoState::initializePredictor() {
  if(d2wall) {
    if(!d2wall_nmp) d2wall_nmp = new DistVec<double>(domain->getNodeDistInfo());
    else d2wall_nmp->resize(domain->getNodeDistInfo());
    *d2wall_nmp = *d2wall;
  }
}

//------------------------------------------------------------------------------

void DistGeoState::initializeCorrector() {
  if(d2wall) {
    *d2wall = *d2wall_nmp;
  }
}

//------------------------------------------------------------------------------

bool DistGeoState::preComputeUpwindDirections() {
  return ((ioData.schemes.ns.rotatedRiemann.preCompute == SchemeData::RotatedRiemannData::PRE_COMPUTE_ON) &&
          ((edgeUpwindDirections && edgeUpwindDirections->norm() == 0) || (faceUpwindDirections && faceUpwindDirections->norm() == 0)));
}

//------------------------------------------------------------------------------

void DistGeoState::setPositionVector(const DistSVec<double, 3>& _X) {
  *X = _X;
}

//------------------------------------------------------------------------------

void DistGeoState::setCtrlVol(const DistVec<double>& _ctrlVol) {
  *ctrlVol = _ctrlVol;
}

//------------------------------------------------------------------------------

void DistGeoState::updateDistanceToWall(const DistVec<double>& _d2wall) {
  if(d2bfwall) {
    #pragma omp parallel for
    for(int iSub = 0; iSub < numLocSub; ++iSub) {
      const Vec<double>& _d2w = _d2wall(iSub);
      const Vec<double>& d2bfw = (*d2bfwall)(iSub);
      Vec<double>& d2w = (*d2wall)(iSub);
      for(int i = 0; i < d2w.size(); ++i) {
        // in case embedded and body-fitted walls intersect (delta d2w should never be zero for body-fitted wall)
        d2w[i] = (d2bfw[i] == ioData.bc.wall.delta) ? d2bfw[i] : std::min(_d2w[i], d2bfw[i]);
      }
    }
  }
  else {
    *d2wall = _d2wall;
  }
}

//------------------------------------------------------------------------------

void DistGeoState::computeUpwindDirections() {
  if(ioData.schemes.ns.rotatedRiemann.upwindDirection == SchemeData::RotatedRiemannData::TRIANGLE_SET) {
    // step 1. initialize source
    if(!triangleSet) {
      char *fileName = new char[strlen(ioData.input.prefix) + strlen(ioData.input.triangleSet) + 1];
      sprintf(fileName, "%s%s", ioData.input.prefix, ioData.input.triangleSet);
      std::string nodeSetName;
      std::map<int, BoundaryData::Type> boundaryConditionsMap;
      std::vector<int> faceIDs;
      int numStNodes;
      int numStElems;
      Vec<Vec3D> Xs;
      int *surfaceID = nullptr;
      int (*stElem)[3] = nullptr;
      int *faceID = nullptr;
      int maxFaceID;
      EmbeddedStructure::readMeshFile(ioData, com, fileName, 1.0, nodeSetName, boundaryConditionsMap,
                                      faceIDs, numStNodes, numStElems, Xs, surfaceID, stElem, faceID, maxFaceID);
      triangleSet = new TriangleSet(ioData.schemes.ns.rotatedRiemann.triangleSetData.surfaceID,
                                    ioData.schemes.ns.rotatedRiemann.triangleSetData.eps);
      triangleSet->updateAxisAlignedBoundingBoxTree(numStElems, stElem, Xs, faceID);

      if(surfaceID) delete [] surfaceID;
      if(stElem) delete [] stElem;
      if(faceID) delete [] faceID;
      delete [] fileName;
    }

    // step 2. compute nodal directions
    DistVec<Eigen::Vector3d> dirs(domain->getNodeDistInfo());
    #pragma omp parallel for
    for(int iSub = 0; iSub < domain->getNumLocSub(); ++iSub) {
      Vec<Eigen::Vector3d>& d = dirs(iSub);
      NodeSet& nodes = domain->getSubDomain()[iSub]->getNodes();
      for(int i = 0; i < d.size(); ++i) {
        Eigen::Vector3d P(nodes[i][0], nodes[i][1], nodes[i][2]);
        d[i] = triangleSet->direction(P);
      }
    }

    // step 3. interpolate nodal directions to compute edge directions (consider intersected edges)
    #pragma omp parallel for
    for(int iSub = 0; iSub < domain->getNumLocSub(); ++iSub) {
      Vec<Eigen::Vector3d>& d = dirs(iSub);
      if(edgeUpwindDirections) {
        SVec<double, 6>& upwdir = (*edgeUpwindDirections)(iSub);
        EdgeSet& edges = domain->getSubDomain()[iSub]->getEdges();
        auto& edgePtr = edges.getPtr();
        for(int l = 0; l < edges.size(); ++l) {
          const int& i = edgePtr[l].first;
          const int& j = edgePtr[l].second;
          Eigen::Vector3d dij = (0.5*(d[i]+d[j])).normalized();
          for(int k = 0; k < 3; ++k) upwdir[l][k] = upwdir[l][k+3] = dij[k];
        }
      }
      if(faceUpwindDirections) {
        SVec<double, Face::MaxNumNd * 3>& upwdir = (*faceUpwindDirections)(iSub);
        FaceSet& faces = domain->getSubDomain()[iSub]->getFaces();
        for(int i = 0; i < faces.size(); ++i) {
          for(int j = 0; j < 3; ++j) {
            for(int k = 0; k < 3; ++k) {
              upwdir[i][3*j+k] = d[faces[i].nodeNum(j)][k];
            }
          }
        }
      }
    }
  }
}

//------------------------------------------------------------------------------

void DistGeoState::recomputeUpwindDirections(DistLevelSetStructure *distLSS) {
  if((edgeUpwindDirections || faceUpwindDirections) &&
     (ioData.schemes.ns.rotatedRiemann.preCompute == SchemeData::RotatedRiemannData::PRE_COMPUTE_ON) &&
     (ioData.schemes.ns.rotatedRiemann.upwindDirection == SchemeData::RotatedRiemannData::TRIANGLE_SET) &&
     (ioData.input.triangleSet[0] == 0) && distLSS) {

    int numStElems = distLSS->getNumStructElems();
    int (*stElem)[3] = distLSS->getStructElems();
    Vec<Vec3D>& Xs = distLSS->getStructPosition();
    int *faceID = distLSS->getFaceID();
    if(!triangleSet) triangleSet = new TriangleSet(ioData.schemes.ns.rotatedRiemann.triangleSetData.surfaceID,
                                                   ioData.schemes.ns.rotatedRiemann.triangleSetData.eps);
    triangleSet->updateAxisAlignedBoundingBoxTree(numStElems, stElem, Xs, faceID);

    computeUpwindDirections();
  }
}

//------------------------------------------------------------------------------

void DistGeoState::fsoInitialize() {
  dX = new DistSVec<double, 3>(domain->getNodeDistInfo());
  dXdot = new DistSVec<double, 3>(domain->getNodeDistInfo());
  dCtrlVol = new DistVec<double>(domain->getNodeDistInfo());
  dd2wall = new DistVec<double>(domain->getNodeDistInfo());
  dEdgeNorm = new DistVec<Vec3D>(domain->getEdgeDistInfo());
  dFaceNorm = new DistVec<Vec3D>(domain->getFaceNormDistInfo());
  dEdgeNormVel = new DistVec<double>(domain->getEdgeDistInfo());
  dFaceNormVel = new DistVec<double>(domain->getFaceNormDistInfo());

  #pragma omp parallel for
  for(int iSub = 0; iSub < numLocSub; ++iSub) {
    subGeoState[iSub]->fsoInitialize(&(*dX)(iSub), &(*dXdot)(iSub), &(*dCtrlVol)(iSub),
                                     &(*dd2wall)(iSub), &(*dEdgeNorm)(iSub), &(*dFaceNorm)(iSub),
                                     &(*dEdgeNormVel)(iSub), &(*dFaceNormVel)(iSub));
  }
}

//------------------------------------------------------------------------------

void DistGeoState::initializeDerivatives() {
  (*dX) = 0.0;
  (*dXdot) = 0.0;
  (*dCtrlVol) = 0.0;
  (*dd2wall) = 0.0;
  #pragma omp parallel for
  for(int iSub = 0; iSub < numLocSub; ++iSub) {
    Vec<Vec3D>& subdEdgeNorm = (*dEdgeNorm)[iSub];
    for(int i = 0; i < subdEdgeNorm.size(); ++i) subdEdgeNorm[i] = 0.0;
  }
  #pragma omp parallel for
  for(int iSub = 0; iSub < numLocSub; ++iSub) {
    Vec<Vec3D>& subdFaceNorm = (*dFaceNorm)[iSub];
    for(int i = 0; i < subdFaceNorm.size(); ++i) subdFaceNorm[i] = 0.0;
  }
  (*dEdgeNormVel) = 0.0;
  (*dFaceNormVel) = 0.0;
}

//------------------------------------------------------------------------------

void DistGeoState::computeDerivatives(DistSVec<double, 3>& Xsdot, DistSVec<double, 3>& dXsdot) {
  data.configSA += 1;
  if(data.typeNormals == DGCLData::IMPLICIT_FIRST_ORDER_GCL || data.typeNormals == DGCLData::IMPLICIT_LATEST_CFG) {
    domain->computeDerivativeOfNormals(*X, *dX, *edgeNorm, *dEdgeNorm, *edgeNormVel,
                                       *dEdgeNormVel, *faceNorm, *dFaceNorm, *faceNormVel, *dFaceNormVel);
  }
  else {
    com->fprintf(stderr, "*** Error: The derivative of the normals is not implemented for the specified GCL.\n");
    com->barrier(); exit(1);
  }
  domain->computeDerivativeOfControlVolumes(lscale, *X, *dX, *dCtrlVol);
}

//------------------------------------------------------------------------------

void DistGeoState::computeDerivatives(DistMat<RectangularSparseMat<double, 3, 3>>& dEdgeNormdX,
                                      DistMat<RectangularSparseMat<double, 3, 3>>& dFaceNormdX,
                                      DistMat<RectangularSparseMat<double, 3, 1>>& dCtrlVoldX) {
  data.configSA += 1;
  if(data.typeNormals == DGCLData::IMPLICIT_FIRST_ORDER_GCL || data.typeNormals == DGCLData::IMPLICIT_LATEST_CFG) {
    domain->computeDerivativeOfNormals(dEdgeNormdX, dFaceNormdX, *dX, *dEdgeNorm, *dEdgeNormVel, *dFaceNorm, *dFaceNormVel);
  }
  else {
    com->fprintf(stderr, "*** Error: The derivative of the normals is not implemented for the specified GCL.\n");
    com->barrier(); exit(1);
  }
  domain->computeDerivativeOfControlVolumes(dCtrlVoldX, *dX, *dCtrlVol);
}

//------------------------------------------------------------------------------

void DistGeoState::computeTransposeDerivatives(DistMat<RectangularSparseMat<double, 3, 3>>& dEdgeNormdX,
                                               DistMat<RectangularSparseMat<double, 3, 3>>& dFaceNormdX,
                                               DistMat<RectangularSparseMat<double, 3, 1>>& dCtrlVoldX, DistSVec<double, 3>& dX) {
  data.configSA += 1;
  domain->computeTransposeDerivativeOfControlVolumes(dCtrlVoldX, *dCtrlVol, dX);
  if(data.typeNormals == DGCLData::IMPLICIT_FIRST_ORDER_GCL || data.typeNormals == DGCLData::IMPLICIT_LATEST_CFG) {
    domain->computeTransposeDerivativeOfNormals(dEdgeNormdX, dFaceNormdX, *dEdgeNorm, *dFaceNorm, dX);
  }
  else {
    com->fprintf(stderr, "*** Error: The transpose derivative of the normals is not implemented for the specified GCL.\n");
    com->barrier(); exit(1);
  }
}

//------------------------------------------------------------------------------

void DistGeoState::computeDerivativeOperators(DistMat<RectangularSparseMat<double, 3, 3>>& dEdgeNormdX,
                                              DistMat<RectangularSparseMat<double, 3, 3>>& dFaceNormdX,
                                              DistMat<RectangularSparseMat<double, 3, 1>>& dCtrlVoldX) {
  if(data.typeNormals == DGCLData::IMPLICIT_FIRST_ORDER_GCL || data.typeNormals == DGCLData::IMPLICIT_LATEST_CFG) {
    domain->computeDerivativeOperatorsOfNormals(*X, dEdgeNormdX, dFaceNormdX);
  }
  else {
    com->fprintf(stderr, "*** Error: The derivative operators of the normals is not implemented for the specified GCL.\n");
    com->barrier(); exit(1);
  }
  domain->computeDerivativeOperatorsOfControlVolumes(*X, dCtrlVoldX);
}

//------------------------------------------------------------------------------

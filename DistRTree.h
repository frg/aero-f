/*
 *    DistRTree.h
 */

#ifndef _DIST_R_TREE_
#define _DIST_R_TREE_

#include <Domain.h>
#include <SubDomain.h>
#include <Communicator.h>
#include <Connectivity.h>

#include <RTree.h>
#include <Vector3D.h>
#include <Vector3DRTree.h>
#include <Elem.h>
#include <Node.h>

#include <map>
#include <set>
#include <utility>
#include <vector>
#include <cstdio>

#define FIND_ELEMS_TAG 1934671

class DistRTree {

  Domain *domain;
  Communicator *com;
  double *roots;

  int numCPU;
  int numGlobSub;
  int numLocSub;
  int numOtherSub;
  int thisCPU;
  int *thisGlobSubNum;
  int *otherGlobSubNum;
  bool nodeMode;

 public:

  std::vector<std::multimap<int,int>> *reassignedElems;
  std::vector<std::multimap<int,int>> *reassignedNodes;
  bool allowDuplicates;

  DistRTree(Domain *_domain, bool _nodeMode = false) : domain(_domain), com(_domain->getCommunicator()), nodeMode(_nodeMode) {
    numCPU = com->size();
    numGlobSub = domain->getNumGlobSub();
    numLocSub = domain->getNumLocSub();
    numOtherSub = numGlobSub-numLocSub;
    thisCPU = com->cpuNum();
    thisGlobSubNum = new int [numLocSub];
    otherGlobSubNum = new int [numOtherSub];
    reassignedElems = new std::vector<std::multimap<int,int>>(numLocSub);
    reassignedNodes = new std::vector<std::multimap<int,int>>(numLocSub);
    allowDuplicates = false;

    // gather the RTree roots across all CPUs
    SubDomain *sub;
    roots = new double [numGlobSub*6];
    for(int iSub = 0; iSub < numLocSub; ++iSub) {
      sub = domain->getSubDomain()[iSub];
      int globSubNum = sub->getGlobSubNum();
      thisGlobSubNum[iSub] = globSubNum;
      for(int i = 0; i < 6; ++i) {
        roots[globSubNum*6+i] = !nodeMode ? sub->getMyTree()->root->bbox[i] : sub->getNodeTree()->root->bbox[i];
      }
    }
    com->allGather<double>(roots,6);

    // find the global subdomain numbers NOT on this CPU
    int i = 0;
    bool flag = false;
    for(int iGlobSub = 0; iGlobSub < numGlobSub; ++iGlobSub) {
      for(int iLocSub = 0; iLocSub < numLocSub; ++iLocSub) {
        if(thisGlobSubNum[iLocSub] == iGlobSub) {
          flag = true;
          break;
        }
      }
      if(!flag) {
        otherGlobSubNum[i] = iGlobSub;
        i++;
      }
      flag = false;
    }
  }

  ~DistRTree() {
    if(roots) {
      delete [] roots;
      roots = 0;
    }
    if(thisGlobSubNum) {
      delete [] thisGlobSubNum;
      thisGlobSubNum = 0;
    }
    if(otherGlobSubNum) {
      delete [] otherGlobSubNum;
      otherGlobSubNum = 0;
    }
    if(reassignedElems) {
      delete reassignedElems;
      reassignedElems = 0;
    }
    if(reassignedNodes) {
      delete reassignedNodes;
      reassignedNodes = 0;
    }
  }

  //------------------------------------------------------------------------------

  // find subdomain connectivity given element assignments
  void findSubToSub(const GeoSource& geoSource, std::vector<std::set<int>>& neighb, Connectivity*& subToSub,
                    const std::vector<std::multimap<int,int>>& somethingElems) {
    // subdomains I'm sending to are called neighbors
    for(int iSub = 0; iSub < numLocSub; ++iSub) {
      for(auto& p : somethingElems[iSub]) {
        const int& sub = p.first;
        neighb[iSub].insert(sub);
      }
    }

    // subdomains I'm receiving from are also neighbors
    Connectivity *cpuToSub = geoSource.getCpuToSub();
    Connectivity *subToCpu = cpuToSub->reverse();
    int *sendCount = new int[numCPU];
    std::fill(sendCount, sendCount+numCPU, 0);
    std::vector<std::array<int,2>*> sendBuffer;
    for(int iSub = 0; iSub < numLocSub; ++iSub) {
      int subI = thisGlobSubNum[iSub];
      for(int subJ : neighb[iSub]) {
        int dest = (*subToCpu)[subJ][0];
        sendBuffer.push_back(new std::array<int,2>{{subI, subJ}});
        com->sendTo(dest, FIND_ELEMS_TAG, sendBuffer.back()->data(), 2);
        sendCount[dest]++;
      }
    }
    delete subToCpu;
    for(int i = 0; i < numCPU; ++i) com->reduce(1, &sendCount[i], MPI_SUM, i);
    for(int i = 0; i < sendCount[thisCPU]; ++i) {
      int recBuffer[2];
      com->recFrom(FIND_ELEMS_TAG, recBuffer, 2);
      for(int iSub = 0; iSub < numLocSub; ++iSub) {
        int subI = thisGlobSubNum[iSub];
        if(subI == recBuffer[1]) {
            neighb[iSub].insert(recBuffer[0]);
            break;
        }
      }
    }
    com->waitForAllReq();
    for(auto& p : sendBuffer) delete p;
    sendBuffer.clear();
    delete [] sendCount;

    // construct a subToSub connectivity
    int *ptr2 = new int[numGlobSub+1];
    std::fill(ptr2, ptr2+numGlobSub+1, 0);
    for(int iSub = 0; iSub < numLocSub; ++iSub) {
      int subI = thisGlobSubNum[iSub];
      ptr2[subI+1] = int(neighb[iSub].size());
    }
    for(int i = 0; i < numGlobSub; ++i) ptr2[i+1] += ptr2[i];
    int *tgt2 = new int[ptr2[numGlobSub]];
    for(int iSub = 0; iSub < numLocSub; ++iSub) {
      int globSubNum = thisGlobSubNum[iSub];
      std::copy(neighb[iSub].begin(), neighb[iSub].end(), tgt2+ptr2[globSubNum]);
    }
    subToSub = new Connectivity(numGlobSub, ptr2, tgt2);
  }

  //------------------------------------------------------------------------------

  // find element assignments, that is, which subdomain each element should be sent to
  void findReassignedElems(const GeoSource& geoSource,
                           const std::vector<ElemSet*>& elems, const std::vector<NodeSet*>& nodes) {
    for(int iSub = 0; iSub < numLocSub; ++iSub) {
      if(reassignedElems) (*reassignedElems)[iSub].clear();
      if(reassignedNodes) (*reassignedNodes)[iSub].clear();
    }

    // compute centroids of elements
    std::vector<NodeSet*> centroids(nodes.size());
    if(!nodeMode) {
      for(int iSub = 0; iSub < elems.size(); ++iSub) {
        centroids[iSub] = new NodeSet(elems[iSub]->size());
        for(int iElem = 0; iElem < elems[iSub]->size(); ++iElem) {
          centroids[iSub]->addNode(iElem,((*elems[iSub])[iElem].computeCentroid(*(nodes[iSub]))));
        }
      }
    }
    else {
      centroids = nodes;
    }

    // build candidateElems, which can be thought of as a first-pass attempt at building reassignedElems,
    // but only by using the roots of the RTrees. MPI communication is used later to "trim" candidateElems
    // into reassignedElems, by searching the whole RTrees.
    std::vector<std::multimap<int,int>> candidateElems(numLocSub);
    for(int iSub = 0; iSub < centroids.size(); ++iSub) {
      for(int iElem = 0; iElem < centroids[iSub]->size(); ++iElem) {
        Vec3D cent = (*centroids[iSub])[iElem];
        bool flag;
        if(!nodeMode) {
          flag = searchLocalElem(cent) == nullptr;
        }
        else {
          flag = searchLocalNode(cent) == nullptr;
        }
        if(flag) {
          for(int iGlobSub = 0; iGlobSub < numOtherSub; ++iGlobSub) {
            int tmp = otherGlobSubNum[iGlobSub];
            if(cent[0] >= roots[tmp*6+0] && cent[0] <= roots[tmp*6+1] &&
               cent[1] >= roots[tmp*6+2] && cent[1] <= roots[tmp*6+3] &&
               cent[2] >= roots[tmp*6+4] && cent[2] <= roots[tmp*6+5]) {
              candidateElems[iSub].insert(std::make_pair(tmp, iElem));
            }
          }
        }
      }
    }

    // find subdomain connectivity
    std::vector<std::set<int>> neighb(numLocSub);
    Connectivity *subToSub;
    findSubToSub(geoSource, neighb, subToSub, candidateElems);

    // construct communication patterns
    SubDTopo *subTopo = new SubDTopo(thisCPU, subToSub, geoSource.getCpuToSub());
    CommPattern<Vec3D> centPat(subTopo, com, CommPattern<Vec3D>::CopyOnSend, CommPattern<Vec3D>::NonSym);
    CommPattern<bool> boolPat(subTopo, com, CommPattern<bool>::CopyOnSend, CommPattern<bool>::NonSym);
    int packetSize = 1;
    for(int iSub = 0; iSub < numLocSub; ++iSub) {
      moveCents_Step1a(thisGlobSubNum[iSub], neighb[iSub], candidateElems[iSub], subTopo, centPat, boolPat, packetSize);
    }
    centPat.finalize();
    for(int iSub = 0; iSub < numLocSub; ++iSub) {
      moveCents_Step1b(thisGlobSubNum[iSub], neighb[iSub], candidateElems[iSub], subTopo, centPat, boolPat, packetSize);
    }
    boolPat.finalize();

    // move centroids
    for(int iSub = 0; iSub < numLocSub; ++iSub) {
      moveCents_Step2(thisGlobSubNum[iSub], neighb[iSub], candidateElems[iSub], subTopo, centPat, centroids[iSub]);
    }
    centPat.exchange();

    // search centroids
    for(int iSub = 0; iSub < numLocSub; ++iSub) {
      moveCents_Step3(thisGlobSubNum[iSub], neighb[iSub], candidateElems[iSub], subTopo, centPat, boolPat, (*reassignedNodes)[iSub]);
    }
    boolPat.exchange();

    // populate reassignedElems
    for(int iSub = 0; iSub < numLocSub; ++iSub) {
      moveCents_Step4(thisGlobSubNum[iSub], neighb[iSub], candidateElems[iSub], subTopo, (*reassignedElems)[iSub], boolPat);
    }

    // destruct
    if(!nodeMode) {
      for(auto p : centroids) {
        if(p) {
          delete p;
          p = 0;
        }
      }
    }
    if(subToSub) {
      delete subToSub;
      subToSub = 0;
    }
    if(subTopo) {
      delete subTopo;
      subTopo = 0;
    }
  }

  //------------------------------------------------------------------------------

  void moveCents_Step1a(const int& globSubNum,
                       const std::set<int>& neighb, const std::multimap<int,int>& candidateElems, SubDTopo *subTopo,
                       CommPattern<Vec3D>& centPat, CommPattern<bool>& boolPat, int packetSize) {
    for(int subJ : neighb) {
      int sndChannel = subTopo->getChannelID(globSubNum, subJ);
      int len = candidateElems.count(subJ);
      centPat.setLen(sndChannel, len);
    }
  }

  //------------------------------------------------------------------------------

  void moveCents_Step1b(const int& globSubNum,
                       const std::set<int>& neighb, const std::multimap<int,int>& candidateElems, SubDTopo *subTopo,
                       CommPattern<Vec3D>& centPat, CommPattern<bool>& boolPat, int packetSize) {
    for(int subJ : neighb) {
      int sndChannel = subTopo->getChannelID(globSubNum, subJ);
      int rcvChannel = subTopo->getChannelID(subJ, globSubNum);
      boolPat.setLen(sndChannel, centPat.getLen(rcvChannel));
    }
  }

  //------------------------------------------------------------------------------

  void moveCents_Step2(const int& globSubNum,
                       const std::set<int>& neighb, const std::multimap<int,int>& candidateElems, SubDTopo *subTopo,
                       CommPattern<Vec3D>& centPat, NodeSet *centroids) {
    for(int subJ : neighb) {
      int sndChannel = subTopo->getChannelID(globSubNum, subJ);
      SubRecInfo<Vec3D> sInfo = centPat.getSendBuffer(sndChannel);
      auto range = candidateElems.equal_range(subJ);
      int iData = 0;
      for(auto it = range.first; it != range.second; ++it) {
        sInfo.data[iData++] = (*centroids)[it->second];
      }
    }
  }

  //------------------------------------------------------------------------------

  void moveCents_Step3(const int& globSubNum,
                       const std::set<int>& neighb, const std::multimap<int,int>& candidateElems, SubDTopo *subTopo,
                       CommPattern<Vec3D>& centPat, CommPattern<bool>& boolPat, std::multimap<int,int>& reassignedNodes) {
    for(int subJ : neighb) {
      int sndChannel = subTopo->getChannelID(globSubNum, subJ);
      int rcvChannel = subTopo->getChannelID(subJ, globSubNum);
      SubRecInfo<Vec3D> sInfoCent = centPat.recData(rcvChannel);
      SubRecInfo<bool> sInfoBool = boolPat.getSendBuffer(sndChannel);
      for(int iData = 0; iData < sInfoBool.len; ++iData) {
        if(!nodeMode) {
          sInfoBool.data[iData] = (searchLocalElem(sInfoCent.data[iData]) != nullptr);
        }
        else {
          Vec3DRTree *tmp = searchLocalNode(sInfoCent.data[iData]);
          sInfoBool.data[iData] = (tmp != nullptr);
          if(tmp != nullptr) reassignedNodes.insert(std::make_pair(subJ, tmp->idxTag));
        }
      }
    }
  }

  //------------------------------------------------------------------------------

  void moveCents_Step4(const int& globSubNum,
                       const std::set<int>& neighb, const std::multimap<int,int>& candidateElems, SubDTopo *subTopo,
                       std::multimap<int,int>& reassignedElems, CommPattern<bool>& boolPat) {
    std::set<int> addedElems;
    for(int subJ : neighb) {
      int rcvChannel = subTopo->getChannelID(subJ, globSubNum);
      SubRecInfo<bool> sInfoBool = boolPat.recData(rcvChannel);
      auto range = candidateElems.equal_range(subJ);
      int iData = 0;
      for(auto it = range.first; it != range.second; ++it) {
        if(sInfoBool.data[iData]) {
          if(allowDuplicates || addedElems.find(it->second) == addedElems.end()) {
            reassignedElems.insert(std::make_pair(subJ, it->second));
            addedElems.insert(it->second);
          }
        }
        iData++;
      }
    }
  }

  //------------------------------------------------------------------------------

  // search the local trees for element centroid
  Elem *searchLocalElem(const Vec3D& centroid) {
    double bary[3];
    SubDomain *sub;
    Elem *elem = nullptr;
    for(int iSub = 0; iSub < numLocSub; ++iSub) {
      sub = domain->getSubDomain()[iSub];
      double eps = 10 * sub->getMyTree()->eps;
      elem = sub->getMyTree()->search<NodeSet,&Elem::isPointInside>(sub->getNodes(),centroid,bary,eps);
      if(elem) {
        break;
      }
    }
    return elem;
  }

  //------------------------------------------------------------------------------

  // search the local trees for node
  Vec3DRTree *searchLocalNode(const Vec3D& centroid) {
    double bary[3];
    SubDomain *sub;
    int idx = -1;
    Vec3DRTree *node = nullptr;
    for(int iSub = 0; iSub < numLocSub; ++iSub) {
      sub = domain->getSubDomain()[iSub];
      double eps = 10 * sub->getNodeTree()->eps;
      node = sub->getNodeTree()->search<NodeSet,&Vec3DRTree::isPointInside>(sub->getNodes(),centroid,bary,eps);
      if(node) {
        break;
      }
    }
    return node;
  }
};

#endif

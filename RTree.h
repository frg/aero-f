/*
 *    RTree.h
 */

#include <algorithm>
#include <cfloat>
#include <cstring>
#include <limits>
#include <stdexcept>

#include <Vector3D.h>


#pragma once

// The first parameter is the object
template<class T>
class RTree {

  struct Node {
    Node *child1, *child2, *parent;
    double bbox[6];
    T *obj;
  };

 public:

  Node *root;
  int size;
  double eps;

  RTree(double _eps) {
    root = NULL;
    size = 0;
    eps = _eps;
  }

  ~RTree() {
    destruct();
  }

  int getSize() {
    return size;
  }

  // Construct an identical copy of the tree including deep copies of all the objects
  // note: pointers to all of the object copies are stored in the container
  template<class Container, class Allocator>
  RTree *clone(Container& objects, Allocator& memObjs) {
    RTree *t = new RTree(eps);
    t->root = cloneInternal(objects, memObjs, root);
    t->size = size;
    return t;
  }

  // Build the tree from list of objects
  template<class PosVecType, void (T::*bbox)(PosVecType&, double *, double), class Container>
  void construct(PosVecType& X, Container& objects, int num) {
    root = (num > 0) ? constructInternal<PosVecType, bbox>(X, objects, num) : nullptr;
    size = num;
  }

  // Delete the tree
  void destruct() {
    if(root) {
      destructInternal(root);
      root = NULL;
      size = 0;
    }
  }

  // Delete and rebuild the tree from list of objects
  template<class PosVecType, void (T::*bbox)(PosVecType&, double *, double), class Container>
  void reconstruct(PosVecType& X, Container& objects, int num) {
    if(root) {
      destructInternal(root);
    }
    root = constructInternal<PosVecType, bbox>(X, objects, num);
    size = num;
  }

  // Search the tree for all objects overlapping a specified region starting at the root
  // here the region (loc) is an axis-aligned bounding box
  template<class PosVecType, bool (T::*overlap)(PosVecType&, double *), class Container>
  void search(PosVecType& X, double *loc, Container& result) {
    return search<PosVecType, overlap, Container>(root, X, loc, result);
  }

  // Search the subtree with root at n for objects overlapping a specified region
  // here the region (loc) is an axis-aligned bounding box
  template<class PosVecType, bool (T::*overlap)(PosVecType&, double *), class Container>
  void search(Node *n, PosVecType& X, double *loc, Container& result) {
    if(n == NULL) {
      return;
    }
    if(loc[1] < n->bbox[0] || loc[0] > n->bbox[1] ||
       loc[3] < n->bbox[2] || loc[2] > n->bbox[3] ||
       loc[5] < n->bbox[4] || loc[4] > n->bbox[5]) {
      return;
    }
    if(n->obj) {
      if(((n->obj)->*overlap)(X, loc)) {
        result.push_back(n->obj);
        return;
      }
      else {
        return;
      }
    }
    search<PosVecType, overlap, Container>(n->child1, X, loc, result);
    search<PosVecType, overlap, Container>(n->child2, X, loc, result);
  }

  // Search the tree for object containing a specified point starting at the root
  template<class PosVecType, bool (T::*inside)(PosVecType&, const Vec3D&, double *, double)>
  T *search(PosVecType& X, const Vec3D& loc, double *bary, double eps = 10.0*std::numeric_limits<double>::epsilon()) {
    return search<PosVecType,inside>(root, X, loc, bary, eps);
  }

  // Search the subtree with root at n for object containing a specified point
  template<class PosVecType, bool (T::*inside)(PosVecType&, const Vec3D&, double *, double)>
  T *search(Node *n, PosVecType& X, const Vec3D& loc, double *bary, double eps) {
    if(n == NULL) {
      return NULL;
    }
    if(loc[0] < n->bbox[0] || loc[0] > n->bbox[1] ||
       loc[1] < n->bbox[2] || loc[1] > n->bbox[3] ||
       loc[2] < n->bbox[4] || loc[2] > n->bbox[5]) {
      return NULL;
    }
    if(n->obj) {
      if(((n->obj)->*inside)(X, loc, bary, eps)) {
        return n->obj;
      }
      else {
        return NULL;
      }
    }
    if(T *p = search<PosVecType,inside>(n->child1, X, loc, bary, eps)) {
      return p;
    }
    return search<PosVecType,inside>(n->child2, X, loc, bary, eps);
  }

  // Search the tree for the (leaf) node which contains the specified object starting at the root
  Node *search(double ebb[6], T *object) {
    return search(root, ebb, object);
  }

  // Search the tree for the (leaf) node which contains the specified object
  Node *search(Node *n, double ebb[6], T *object) {
    if(n == NULL) {
      std::cerr << "*** Warning: encountered NULL node in RTree::search\n";
      return NULL;
    }
    if(ebb[0] < n->bbox[0] || ebb[1] > n->bbox[1] ||
       ebb[2] < n->bbox[2] || ebb[3] > n->bbox[3] ||
       ebb[4] < n->bbox[4] || ebb[5] > n->bbox[5]) {
      return NULL;
    }
    if(n->obj) {
      if(n->obj == object)
        return n;
      else
        return NULL;
    }
    if(Node *p = search(n->child1, ebb, object)) {
      return p;
    }
    return search(n->child2, ebb, object);
  }

  // Split the leaf node containing parent yielding two children containing e1 and e2
  template<class PosVecType, void (T::*bbox)(PosVecType&, double *, double)>
  void split(PosVecType& nodes, T *parent, T *e1, T *e2) {
    double ebb[6];
    (parent->*bbox)(nodes, ebb, eps);
    Node *leaf = search(root, ebb, parent);
    if(leaf == NULL) {
      std::cerr << "*** Warning: search failed to locate leaf Node in RTree::split\n";
      return;
    }
    for(int i = 0; i < 2; i++) {
      Node *nn = new Node;
      nn->parent = leaf;
      nn->child1 = NULL;
      nn->child2 = NULL;
      if(i == 0) {
        leaf->child1 = nn;
        nn->obj = e1;
        (e1->*bbox)(nodes, ebb, eps);
      }
      else {
        leaf->child2 = nn;
        nn->obj = e2;
        (e2->*bbox)(nodes, ebb, eps);
      }
      std::memcpy(nn->bbox, ebb, sizeof(ebb));
    }
    leaf->obj = NULL;
    size++;
  }

  // Insert new leaf node containing object into the tree requiring least bbox enlargement
  template<class PosVecType, void (T::*bbox)(PosVecType&, double *, double)>
  void insert(PosVecType& nodes, T *object) {
    double ebb[6];
    (object->*bbox)(nodes, ebb, eps);

    if(!root) {  // create a new root
      Node *nn = new Node;
      std::memset(nn, 0, sizeof(Node));
      nn->obj = object;
      std::memcpy(nn->bbox, ebb, sizeof(ebb));
      root = nn;
      size++;
      return;
    }

    double v1, v2, v1n, v2n;
    Node *n = root;
    while(!n->obj) {
      // enlarge bounding box of subtree
      for(int i = 0; i < 3; ++i) {
        n->bbox[i * 2] = std::min(n->bbox[i * 2], ebb[i * 2]);
        n->bbox[i * 2 + 1] = std::max(n->bbox[i * 2 + 1], ebb[i * 2 + 1]);
      }
      // enter subtree requiring least expansion
      Node *child1 = n->child1;
      Node *child2 = n->child2;
      int i = 0;
      v1 = (child1->bbox[i * 2 + 1] - child1->bbox[i * 2]);
      v2 = (child2->bbox[i * 2 + 1] - child2->bbox[i * 2]);
      v1n = (std::max(child1->bbox[i * 2 + 1], ebb[i * 2 + 1]) - std::min(child1->bbox[i * 2], ebb[i * 2]));
      v2n = (std::max(child2->bbox[i * 2 + 1], ebb[i * 2 + 1]) - std::min(child2->bbox[i * 2], ebb[i * 2]));
      for(i = 1; i < 3; ++i) {
        v1 *= (child1->bbox[i * 2 + 1] - child1->bbox[i * 2]);
        v2 *= (child2->bbox[i * 2 + 1] - child2->bbox[i * 2]);
        v1n *= (std::max(child1->bbox[i * 2 + 1], ebb[i * 2 + 1]) - std::min(child1->bbox[i * 2], ebb[i * 2]));
        v2n *= (std::max(child2->bbox[i * 2 + 1], ebb[i * 2 + 1]) - std::min(child2->bbox[i * 2], ebb[i * 2]));
      }
      n = (v2n-v2 < v1n-v1) ? n->child2 : n->child1;
    }

    // nn is new leaf paired with n, with new parent node parent
    Node *nn = new Node;
    std::memset(nn, 0, sizeof(Node));
    nn->obj = object;
    std::memcpy(nn->bbox, ebb, sizeof(ebb));

    Node *parent = new Node;
    parent->obj = NULL;
    parent->parent = n->parent;
    parent->child1 = n;
    parent->child2 = nn;
    for(int i = 0; i < 3; ++i) {
      parent->bbox[i * 2] = std::min(n->bbox[i * 2], ebb[i * 2]);
      parent->bbox[i * 2 + 1] = std::max(n->bbox[i * 2 + 1], ebb[i * 2 + 1]);
    }

    if(!n->parent) {
      root = parent;
    }
    else {
      if(n->parent->child1 == n)
        n->parent->child1 = parent;
      else
        n->parent->child2 = parent;
    }
    n->parent = parent;
    nn->parent = parent;
    size++;
  }

  // Delete leaf node containing object
  template<class PosVecType, void (T::*bbox)(PosVecType&, double *, double)>
  void remove(PosVecType& nodes, T *object) {
    double ebb[6];
    (object->*bbox)(nodes, ebb, eps);
    Node *leaf = search(root, ebb, object);
    if(leaf == NULL) {
      std::cerr << "*** Warning: search failed to locate leaf Node in RTree::remove\n";
      return;
    }
    if(leaf == root) {  // need to account for possibility of emptying the tree
      destruct();
      return;
    }
    Node *parent = leaf->parent;
    if(parent == root) {
      root = (parent->child1 == leaf) ? parent->child2 : parent->child1;
      root->parent = NULL;
      delete parent;
      delete leaf;
      size--;
      return;
    }
    Node *gparent = parent->parent;

    // restructure tree around the removed parent and leaf
    Node *newchild = (parent->child1 == leaf) ? parent->child2 : parent->child1;
    newchild->parent = gparent;
    if(gparent->child1 == parent)
      gparent->child1 = newchild;
    else
      gparent->child2 = newchild;

    // backpropagate bounding box changes
    Node *n = gparent;
    while(n != NULL) {
      for(int i = 0; i < 3; ++i) {
        n->bbox[i * 2] = std::min(n->child1->bbox[i * 2], n->child2->bbox[i * 2]);
        n->bbox[i * 2 + 1] = std::max(n->child1->bbox[i * 2 + 1], n->child2->bbox[i * 2 + 1]);
      }
      n = n->parent;
    }

    delete parent;
    delete leaf;
    size--;
  }

  // Replace the object of the (leaf) node containing e1 with e2
  template<class PosVecType, void (T::*bbox)(PosVecType&, double *, double)>
  void replace(PosVecType& nodes, T *e1, T *e2) {
    double ebb[6];
    (e1->*bbox)(nodes, ebb, eps);
    Node *leaf = search(root, ebb, e1);
    if(leaf == NULL) {
      std::cerr << "*** Warning: search failed to locate leaf Node in RTree::replace\n";
      return;
    }
    (e2->*bbox)(nodes, ebb, eps);
    std::memcpy(leaf->bbox, ebb, sizeof(ebb));
    leaf->obj = e2;

    // backpropagate bounding box changes
    Node *n = leaf->parent;
    while(n != NULL) {
      for(int i = 0; i < 3; ++i) {
        n->bbox[i * 2] = std::min(n->bbox[i * 2], ebb[i * 2]);
        n->bbox[i * 2 + 1] = std::max(n->bbox[i * 2 + 1], ebb[i * 2 + 1]);
      }
      n = n->parent;
    }
  }

 private:

  template<class PosVecType, void (T::*bbox)(PosVecType&, double *, double), class Container>
  Node *constructInternal(PosVecType& X, Container& objects, int num, Node *parent=NULL) {
    Node *nn = new Node;
    memset(nn, 0, sizeof(Node));
    std::vector<T *> list1, list2;
    {
      double boundingbox[6] = {FLT_MAX, -FLT_MAX, FLT_MAX, -FLT_MAX, FLT_MAX, -FLT_MAX};
      double ebb[6];
      for(int i = 0; i < num; ++i) {
        (objects[i]->*bbox)(X, ebb, eps);
        for(int j = 0; j < 3; ++j) {
          boundingbox[j * 2] = std::min(boundingbox[j * 2], ebb[j * 2]);
          boundingbox[j * 2 + 1] = std::max(boundingbox[j * 2 + 1], ebb[j * 2 + 1]);
        }
      }
      std::memcpy(nn->bbox, boundingbox, sizeof(boundingbox));
      nn->parent = parent;
      if(num == 1) {
        nn->obj = objects[0];
        if(parent != NULL) { objects.clear(); objects.shrink_to_fit(); }
        return nn;
      }
      double dx[3] = {boundingbox[1] - boundingbox[0],
                      boundingbox[3] - boundingbox[2],
                      boundingbox[5] - boundingbox[4]
                     };
      int dim = 0;
      if(dx[1] > dx[0] && dx[1] > dx[2]) {
        dim = 1;
      }
      else if(dx[2] > dx[0]) {
        dim = 2;
      }
      double tolerance = 10*std::numeric_limits<double>::epsilon() * std::max(1., dx[dim]);
      double threshold1 = boundingbox[dim * 2] + 0.5 * dx[dim] - tolerance;
      double threshold2 = boundingbox[dim * 2] + 0.5 * dx[dim] + tolerance;
      list1.reserve(num); list2.reserve(num);
      std::vector<T *> list3;
      for(int i = 0; i < num; ++i) {
        (objects[i]->*bbox)(X, ebb, eps);
        double value = (ebb[dim * 2] + ebb[dim * 2 + 1]) * 0.5;
        if(value < threshold1) {
          list1.push_back(objects[i]);
        }
        else if(value > threshold2) {
          list2.push_back(objects[i]);
        }
        else {
          list3.push_back(objects[i]);
        }
      }
      for(int i = 0; i < list3.size(); ++i) {
        if(list1.size() < list2.size()) {
          list1.push_back(list3[i]);
        }
        else {
          list2.push_back(list3[i]);
        }
      }
      if(parent != NULL) { objects.clear(); objects.shrink_to_fit(); }
      if(list1.size() == 0 || list2.size() == 0) {
        // note: if this occurs, try increasing tolerance
        throw std::runtime_error("RTree construction failed");
      }
    }
    nn->child1 = constructInternal<PosVecType, bbox>(X, list1, int(list1.size()), nn);
    nn->child2 = constructInternal<PosVecType, bbox>(X, list2, int(list2.size()), nn);
    return nn;
  }

  void destructInternal(Node *nn) {
    if(!nn->obj) {
      destructInternal(nn->child1);
      destructInternal(nn->child2);
    }
    delete nn;
  }

  template<class Container, class Allocator>
  Node *cloneInternal(Container& objects, Allocator& memObjs, Node *n, Node *parent=NULL) {
    if(n) {
      Node *nn = new Node;
      nn->child1 = cloneInternal(objects, memObjs, n->child1, nn);
      nn->child2 = cloneInternal(objects, memObjs, n->child2, nn);
      nn->parent = parent;
      if(n->obj) {
        nn->obj = n->obj->clone(memObjs);
        objects.push_back(nn->obj);
      }
      else nn->obj = NULL;
      std::memcpy(nn->bbox, n->bbox, sizeof(n->bbox));
      return nn;
    }
    return NULL;
  }
};

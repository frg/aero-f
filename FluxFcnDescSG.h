#ifndef _FLUX_FCN_DESC_SG_H_
#define _FLUX_FCN_DESC_SG_H_

#include <FluxFcnDesc.h>
#include <VarFcnBase.h>
#include <VarFcnSGEuler.h>
#include <VarFcnSGSA.h>
#include <VarFcnSGKE.h>

class IoData;

//------------------------------------------------------------------------------

class FluxFcnSGFDJacRoeEuler3D : public FluxFcnFDJacRoeEuler3D {

 public:
  FluxFcnSGFDJacRoeEuler3D(double gg, IoData& ioData, VarFcnSGEuler *varFcnSGEuler) :
    FluxFcnFDJacRoeEuler3D(ioData, gg, varFcnSGEuler) {}
  ~FluxFcnSGFDJacRoeEuler3D() {
    vf = 0;
  }

  void compute(double, double, double *, double *, double, double *, double *, double *, bool);
  void computeDerivative(double, double, double, double *, double *, double, double, double *, double *, double *,
                         double *, double, double *, double *, bool);

};

//------------------------------------------------------------------------------

class FluxFcnSGApprJacRoeEuler3D : public FluxFcnApprJacRoeEuler3D {

 public:
  FluxFcnSGApprJacRoeEuler3D(int rs, double gg, IoData& ioData, VarFcnSGEuler *varFcnSGEuler) :
    FluxFcnApprJacRoeEuler3D(ioData, rs, gg, varFcnSGEuler) {}
  ~FluxFcnSGApprJacRoeEuler3D() {
    vf = 0;
  }

  void compute(double, double, double *, double *, double, double *, double *, double *, bool);
  void computeJacobians(double, double, double *, double *, double, double *, double *, double *, double *, FluxFcnBase::Type, bool);
  void computeDerivative(double, double, double, double *, double *, double, double, double *, double *, double *,
                         double *, double, double *, double *, bool);

};

//------------------------------------------------------------------------------

class FluxFcnSGExactJacRoeEuler3D : public FluxFcnExactJacRoeEuler3D {

 public:
  FluxFcnSGExactJacRoeEuler3D(double gg, IoData& ioData, VarFcnSGEuler *varFcnSGEuler) :
    FluxFcnExactJacRoeEuler3D(ioData, gg, varFcnSGEuler) {}
  ~FluxFcnSGExactJacRoeEuler3D() {
    vf = 0;
  }

  void compute(double, double, double *, double *, double, double *, double *, double *, bool);
  void computeJacobians(double, double, double *, double *, double, double *, double *, double *, double *, FluxFcnBase::Type, bool);
  void computeDerivative(double, double, double, double *, double *, double, double, double *, double *, double *,
                         double *, double, double *, double *, bool);
  void computeDerivativeOperators(double, double, double *, double, double *, double *, double *, double *, double *,
                                  double *, double *, bool);

};

//------------------------------------------------------------------------------

class FluxFcnSGFDJacHLLEEuler3D : public FluxFcnFDJacHLLEEuler3D {

 public:
  FluxFcnSGFDJacHLLEEuler3D(double gg, IoData& ioData, VarFcnSGEuler *varFcnSGEuler) :
    FluxFcnFDJacHLLEEuler3D(ioData, gg, varFcnSGEuler) {}
  ~FluxFcnSGFDJacHLLEEuler3D() {
    vf = 0;
  }

  void compute(double, double, double *, double *, double, double *, double *, double *, bool);
  void computeDerivative(double, double, double, double *, double *, double, double, double *, double *, double *,
                         double *, double, double *, double *, bool);

};

//------------------------------------------------------------------------------

class FluxFcnSGApprJacHLLEEuler3D : public FluxFcnApprJacHLLEEuler3D {

 public:
  FluxFcnSGApprJacHLLEEuler3D(int rs, double gg, IoData& ioData, VarFcnSGEuler *varFcnSGEuler) :
    FluxFcnApprJacHLLEEuler3D(ioData, rs, gg, varFcnSGEuler) {}
  ~FluxFcnSGApprJacHLLEEuler3D() {
    vf = 0;
  }

  void compute(double, double, double *, double *, double, double *, double *, double *, bool);
  void computeJacobians(double, double, double *, double *, double, double *, double *, double *, double *, FluxFcnBase::Type, bool);
  void computeDerivative(double, double, double, double *, double *, double, double, double *, double *, double *,
                         double *, double, double *, double *, bool);

};

//------------------------------------------------------------------------------

class FluxFcnSGExactJacHLLEEuler3D : public FluxFcnExactJacHLLEEuler3D {

 public:
  FluxFcnSGExactJacHLLEEuler3D(int rs, double gg, IoData& ioData, VarFcnSGEuler *varFcnSGEuler) :
    FluxFcnExactJacHLLEEuler3D(ioData, rs, gg, varFcnSGEuler) {}
  ~FluxFcnSGExactJacHLLEEuler3D() {
    vf = 0;
  }

  void compute(double, double, double *, double *, double, double *, double *, double *, bool);
  void computeJacobians(double, double, double *, double *, double, double *, double *, double *, double *, FluxFcnBase::Type, bool);
  void computeDerivative(double, double, double, double *, double *, double, double, double *, double *, double *,
                         double *, double, double *, double *, bool);
  void computeDerivativeOperators(double, double, double *, double, double *, double *, double *, double *, double *,
                                  double *, double *, bool);

};

//------------------------------------------------------------------------------

class FluxFcnSGFDJacHLLCEuler3D : public FluxFcnFDJacHLLCEuler3D {

 public:
  FluxFcnSGFDJacHLLCEuler3D(double gg, IoData& ioData, VarFcnSGEuler *varFcnSGEuler) :
    FluxFcnFDJacHLLCEuler3D(ioData, gg, varFcnSGEuler) {}
  ~FluxFcnSGFDJacHLLCEuler3D() {
    vf = 0;
  }

  void compute(double, double, double *, double *, double, double *, double *, double *, bool);
  void computeDerivative(double, double, double, double *, double *, double, double, double *, double *, double *,
                         double *, double, double *, double *, bool);

};

//------------------------------------------------------------------------------

class FluxFcnSGApprJacHLLCEuler3D : public FluxFcnApprJacHLLCEuler3D {

 public:
  FluxFcnSGApprJacHLLCEuler3D(int rs, double gg, IoData& ioData, VarFcnSGEuler *varFcnSGEuler) :
    FluxFcnApprJacHLLCEuler3D(ioData, rs, gg, varFcnSGEuler) {}
  ~FluxFcnSGApprJacHLLCEuler3D() {
    vf = 0;
  }

  void compute(double, double, double *, double *, double, double *, double *, double *, bool);
  void computeJacobians(double, double, double *, double *, double, double *, double *, double *, double *, FluxFcnBase::Type, bool);
  void computeDerivative(double, double, double, double *, double *, double, double, double *, double *, double *,
                         double *, double, double *, double *, bool);

};

//------------------------------------------------------------------------------

class FluxFcnSGExactJacHLLCEuler3D : public FluxFcnExactJacHLLCEuler3D {

 public:
  FluxFcnSGExactJacHLLCEuler3D(int rs, double gg, IoData& ioData, VarFcnSGEuler *varFcnSGEuler) :
    FluxFcnExactJacHLLCEuler3D(ioData, rs, gg, varFcnSGEuler) {}
  ~FluxFcnSGExactJacHLLCEuler3D() {
    vf = 0;
  }

  void compute(double, double, double *, double *, double, double *, double *, double *, bool);
  void computeJacobians(double, double, double *, double *, double, double *, double *, double *, double *, FluxFcnBase::Type, bool);
  void computeDerivative(double, double, double, double *, double *, double, double, double *, double *, double *,
                         double *, double, double *, double *, bool);
  void computeDerivativeOperators(double, double, double *, double, double *, double *, double *, double *, double *,
                                  double *, double *, bool);

};

//------------------------------------------------------------------------------

class FluxFcnSGVanLeerEuler3D : public FluxFcnVanLeerEuler3D {

 public:
  FluxFcnSGVanLeerEuler3D(IoData& ioData, VarFcnSGEuler *varFcnSGEuler) :
    FluxFcnVanLeerEuler3D(varFcnSGEuler) {}
  ~FluxFcnSGVanLeerEuler3D() {
    vf = 0;
  }

  void compute(double, double, double *, double *, double, double *, double *, double *, bool);
  void computeJacobians(double, double, double *, double *, double, double *, double *, double *, double *, FluxFcnBase::Type, bool);
  void computeDerivative(double, double, double, double *, double *, double, double, double *, double *, double *,
                         double *, double, double *, double *, bool);

};

//------------------------------------------------------------------------------

class FluxFcnSGWallEuler3D : public FluxFcnWallEuler3D {

 public:
  FluxFcnSGWallEuler3D(IoData& ioData, VarFcnSGEuler *varFcnSGEuler) :
    FluxFcnWallEuler3D(ioData, varFcnSGEuler) {}
  ~FluxFcnSGWallEuler3D() {
    vf = 0;
  }

  void compute(double, double, double *, double *, double, double *, double *, double *, bool);
  void computeJacobian(double, double, double *, double *, double, double *, double *, double *, FluxFcnBase::Type, bool);
  void computeDerivative(double, double, double, double *, double *, double, double, double *, double *, double *,
                         double *, double, double *, double *, bool);
  void computeDerivativeOperators(double, double, double *, double, double *, double *, double *, double *, double *,
                                  double *, double *, bool);

};

//------------------------------------------------------------------------------

class FluxFcnSGPorousWallEuler3D : public FluxFcnPorousWallEuler3D {

 public:
  FluxFcnSGPorousWallEuler3D(IoData& ioData, VarFcnSGEuler *varFcnSGEuler) :
    FluxFcnPorousWallEuler3D(varFcnSGEuler) {}
  ~FluxFcnSGPorousWallEuler3D() {
    vf = 0;
  }

  void compute(double, double, double *, double *, double, double *, double *, double *, bool);

};

//------------------------------------------------------------------------------

class FluxFcnSGGhidagliaEuler3D : public FluxFcnGhidagliaEuler3D {

 public:
  FluxFcnSGGhidagliaEuler3D(IoData& ioData, VarFcnSGEuler *varFcnSGEuler) :
    FluxFcnGhidagliaEuler3D(varFcnSGEuler) {}
  ~FluxFcnSGGhidagliaEuler3D() {
    vf = 0;
  }

  void compute(double, double, double *, double *, double, double *, double *, double *, bool);

};

//------------------------------------------------------------------------------

class FluxFcnSGOutflowEuler3D : public FluxFcnOutflowEuler3D {

 public:
  FluxFcnSGOutflowEuler3D(IoData& ioData, VarFcnSGEuler *varFcnSGEuler) :
    FluxFcnOutflowEuler3D(varFcnSGEuler) {}
  ~FluxFcnSGOutflowEuler3D() {
    vf = 0;
  }

  void compute(double, double, double *, double *, double, double *, double *, double *, bool);
  void computeJacobian(double, double, double *, double *, double, double *, double *, double *, FluxFcnBase::Type, bool);
  void computeDerivative(double, double, double, double *, double *, double, double, double *, double *, double *,
                         double *, double, double *, double *, bool);
  void computeDerivativeOperators(double, double, double *, double, double *, double *, double *, double *, double *,
                                  double *, double *, bool);

};

//------------------------------------------------------------------------------

class FluxFcnSGInternalInflowEuler3D : public FluxFcnInternalInflowEuler3D {

 public:
  FluxFcnSGInternalInflowEuler3D(IoData& ioData, VarFcnSGEuler *varFcnSGEuler) :
    FluxFcnInternalInflowEuler3D(varFcnSGEuler) {}
  ~FluxFcnSGInternalInflowEuler3D() {
    vf = 0;
  }

  void compute(double, double, double *, double *, double, double *, double *, double *, bool);
  void computeJacobian(double, double, double *, double *, double, double *, double *, double *, FluxFcnBase::Type, bool);

};

//------------------------------------------------------------------------------

class FluxFcnSGDirectStateInflowEuler3D : public FluxFcnDirectStateInflowEuler3D {

 public:
  FluxFcnSGDirectStateInflowEuler3D(IoData& ioData, VarFcnSGEuler *varFcnSGEuler) :
    FluxFcnDirectStateInflowEuler3D(ioData, varFcnSGEuler) {}
  ~FluxFcnSGDirectStateInflowEuler3D() {
    vf = 0;
  }

  void compute(double, double, double *, double *, double, double *, double *, double *, bool);
  void computeJacobian(double, double, double *, double *, double, double *, double *, double *, FluxFcnBase::Type, bool);

};

//------------------------------------------------------------------------------

class FluxFcnSGMassFlowInflowEuler3D : public FluxFcnMassFlowInflowEuler3D {

 public:
  FluxFcnSGMassFlowInflowEuler3D(IoData& ioData, VarFcnSGEuler *varFcnSGEuler) :
    FluxFcnMassFlowInflowEuler3D(varFcnSGEuler) {}
  ~FluxFcnSGMassFlowInflowEuler3D() {
    vf = 0;
  }

  void compute(double, double, double *, double *, double, double *, double *, double *, bool);

};

//------------------------------------------------------------------------------

class FluxFcnSGInternalOutflowEuler3D : public FluxFcnInternalOutflowEuler3D {

 public:
  FluxFcnSGInternalOutflowEuler3D(IoData& ioData, VarFcnSGEuler *varFcnSGEuler) :
    FluxFcnInternalOutflowEuler3D(varFcnSGEuler) {}
  ~FluxFcnSGInternalOutflowEuler3D() {
    vf = 0;
  }

  void compute(double, double, double *, double *, double, double *, double *, double *, bool);
  void computeJacobian(double, double, double *, double *, double, double *, double *, double *, FluxFcnBase::Type, bool);

};

//------------------------------------------------------------------------------

class FluxFcnSGDirectStateOutflowEuler3D : public FluxFcnDirectStateOutflowEuler3D {

 public:
  FluxFcnSGDirectStateOutflowEuler3D(IoData& ioData, VarFcnSGEuler *varFcnSGEuler) :
    FluxFcnDirectStateOutflowEuler3D(ioData, varFcnSGEuler) {}
  ~FluxFcnSGDirectStateOutflowEuler3D() {
    vf = 0;
  }

  void compute(double, double, double *, double *, double, double *, double *, double *, bool);
  void computeJacobian(double, double, double *, double *, double, double *, double *, double *, FluxFcnBase::Type, bool);

};

//------------------------------------------------------------------------------

class FluxFcnSGMassFlowOutflowEuler3D : public FluxFcnMassFlowOutflowEuler3D {

 public:
  FluxFcnSGMassFlowOutflowEuler3D(IoData& ioData, VarFcnSGEuler *varFcnSGEuler) :
    FluxFcnMassFlowOutflowEuler3D(varFcnSGEuler) {}
  ~FluxFcnSGMassFlowOutflowEuler3D() {
    vf = 0;
  }

  void compute(double, double, double *, double *, double, double *, double *, double *, bool);

};

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
// turbulence
// NOTE : for the coupled solver, the varFcn correspond to the FluxFcn in the sense
//        that if varFcn = VarFcnSGSA3D, then fluxFcn = FluxFcnSGApprJacRoeSA3D
//        HOWEVER, for the segregated solver, they do not necessarily correspond since we consider
//        two different fluxes ff1 and ff2. Then there must be two varFcn (vf1 and vf2) corresponding
//        to each fluxFcn, and not one varFcn (corresponding to the physical case we are considering)
//        for both ff1 and ff2
//
// NOTE2: some FluxFcn do not really need varFcn, but they were given one varFcn still (easier to implement)
//        for instance all fluxFcn of type SAturb3D and KEturb3D do not need a real varFcn. Moreover,
//        there is no corresponding varFcn for these fluxes because the varFcn always consider at least
//        the Euler variables, but never just the turbulent variables.

class FluxFcnSGFDJacRoeSA3D : public FluxFcnFDJacRoeSA3D {

 public:
  FluxFcnSGFDJacRoeSA3D(double gg, IoData& ioData, VarFcnSGSA *varFcnSGSA) :
    FluxFcnFDJacRoeSA3D(ioData, gg, varFcnSGSA) {}
  ~FluxFcnSGFDJacRoeSA3D() {
    vf = 0;
  }

  void compute(double, double, double *, double *, double, double *, double *, double *, bool);
  void computeDerivative(double, double, double, double *, double *, double, double, double *, double *, double *,
                         double *, double, double *, double *, bool);

};

//------------------------------------------------------------------------------

class FluxFcnSGApprJacRoeSA3D : public FluxFcnApprJacRoeSA3D {

 public:
  FluxFcnSGApprJacRoeSA3D(int rs, double gg, IoData& ioData, VarFcnSGSA *varFcnSGSA) :
    FluxFcnApprJacRoeSA3D(ioData, rs, gg, varFcnSGSA) {}
  ~FluxFcnSGApprJacRoeSA3D() {
    vf = 0;
  }

  void compute(double, double, double *, double *, double, double *, double *, double *, bool);
  void computeJacobians(double, double, double *, double *, double, double *, double *, double *, double *, FluxFcnBase::Type, bool);
  void computeDerivative(double, double, double, double *, double *, double, double, double *, double *, double *,
                         double *, double, double *, double *, bool);

};

//------------------------------------------------------------------------------

class FluxFcnSGExactJacRoeSA3D : public FluxFcnExactJacRoeSA3D {

 public:
  FluxFcnSGExactJacRoeSA3D(double gg, IoData& ioData, VarFcnSGSA *varFcnSGSA) :
    FluxFcnExactJacRoeSA3D(ioData, gg, varFcnSGSA) {}
  ~FluxFcnSGExactJacRoeSA3D() {
    vf = 0;
  }

  void compute(double, double, double *, double *, double, double *, double *, double *, bool);
  void computeJacobians(double, double, double *, double *, double, double *, double *, double *, double *, FluxFcnBase::Type, bool);
  void computeDerivative(double, double, double, double *, double *, double, double, double *, double *, double *,
                         double *, double, double *, double *, bool);
  void computeDerivativeOperators(double, double, double *, double, double *, double *, double *, double *, double *,
                                  double *, double *, bool);

};

//------------------------------------------------------------------------------

class FluxFcnSGFDJacHLLESA3D : public FluxFcnFDJacHLLESA3D {

 public:
  FluxFcnSGFDJacHLLESA3D(double gg, IoData& ioData, VarFcnSGSA *varFcnSGSA) :
    FluxFcnFDJacHLLESA3D(ioData, gg, varFcnSGSA) {}
  ~FluxFcnSGFDJacHLLESA3D() {
    vf = 0;
  }

  void compute(double, double, double *, double *, double, double *, double *, double *, bool);
  void computeDerivative(double, double, double, double *, double *, double, double, double *, double *, double *,
                         double *, double, double *, double *, bool);

};

//------------------------------------------------------------------------------

class FluxFcnSGApprJacHLLESA3D : public FluxFcnApprJacHLLESA3D {

 public:
  FluxFcnSGApprJacHLLESA3D(int rs, double gg, IoData& ioData, VarFcnSGSA *varFcnSGSA) :
    FluxFcnApprJacHLLESA3D(ioData, rs, gg, varFcnSGSA) {}
  ~FluxFcnSGApprJacHLLESA3D() {
    vf = 0;
  }

  void compute(double, double, double *, double *, double, double *, double *, double *, bool);
  void computeJacobians(double, double, double *, double *, double, double *, double *, double *, double *, FluxFcnBase::Type, bool);
  void computeDerivative(double, double, double, double *, double *, double, double, double *, double *, double *,
                         double *, double, double *, double *, bool);

};

//------------------------------------------------------------------------------

class FluxFcnSGExactJacHLLESA3D : public FluxFcnExactJacHLLESA3D {

 public:
  FluxFcnSGExactJacHLLESA3D(int rs, double gg, IoData& ioData, VarFcnSGSA *varFcnSGSA) :
    FluxFcnExactJacHLLESA3D(ioData, rs, gg, varFcnSGSA) {}
  ~FluxFcnSGExactJacHLLESA3D() {
    vf = 0;
  }

  void compute(double, double, double *, double *, double, double *, double *, double *, bool);
  void computeJacobians(double, double, double *, double *, double, double *, double *, double *, double *, FluxFcnBase::Type, bool);
  void computeDerivative(double, double, double, double *, double *, double, double, double *, double *, double *,
                         double *, double, double *, double *, bool);
  void computeDerivativeOperators(double, double, double *, double, double *, double *, double *, double *, double *,
                                  double *, double *, bool);

};

//------------------------------------------------------------------------------

class FluxFcnSGFDJacHLLCSA3D : public FluxFcnFDJacHLLCSA3D {

 public:
  FluxFcnSGFDJacHLLCSA3D(double gg, IoData& ioData, VarFcnSGSA *varFcnSGSA) :
    FluxFcnFDJacHLLCSA3D(ioData, gg, varFcnSGSA) {}
  ~FluxFcnSGFDJacHLLCSA3D() {
    vf = 0;
  }

  void compute(double, double, double *, double *, double, double *, double *, double *, bool);
  void computeDerivative(double, double, double, double *, double *, double, double, double *, double *, double *,
                         double *, double, double *, double *, bool);

};

//------------------------------------------------------------------------------

class FluxFcnSGApprJacHLLCSA3D : public FluxFcnApprJacHLLCSA3D {

 public:
  FluxFcnSGApprJacHLLCSA3D(int rs, double gg, IoData& ioData, VarFcnSGSA *varFcnSGSA) :
    FluxFcnApprJacHLLCSA3D(ioData, rs, gg, varFcnSGSA) {}
  ~FluxFcnSGApprJacHLLCSA3D() {
    vf = 0;
  }

  void compute(double, double, double *, double *, double, double *, double *, double *, bool);
  void computeJacobians(double, double, double *, double *, double, double *, double *, double *, double *, FluxFcnBase::Type, bool);
  void computeDerivative(double, double, double, double *, double *, double, double, double *, double *, double *,
                         double *, double, double *, double *, bool);

};

//------------------------------------------------------------------------------

class FluxFcnSGExactJacHLLCSA3D : public FluxFcnExactJacHLLCSA3D {

 public:
  FluxFcnSGExactJacHLLCSA3D(int rs, double gg, IoData& ioData, VarFcnSGSA *varFcnSGSA) :
    FluxFcnExactJacHLLCSA3D(ioData, rs, gg, varFcnSGSA) {}
  ~FluxFcnSGExactJacHLLCSA3D() {
    vf = 0;
  }

  void compute(double, double, double *, double *, double, double *, double *, double *, bool);
  void computeJacobians(double, double, double *, double *, double, double *, double *, double *, double *, FluxFcnBase::Type, bool);
  void computeDerivative(double, double, double, double *, double *, double, double, double *, double *, double *,
                         double *, double, double *, double *, bool);
  void computeDerivativeOperators(double, double, double *, double, double *, double *, double *, double *, double *,
                                  double *, double *, bool);

};

//------------------------------------------------------------------------------

class FluxFcnSGWallSA3D : public FluxFcnWallSA3D {

 public:
  FluxFcnSGWallSA3D(IoData& ioData, VarFcnSGSA *varFcnSGSA) :
    FluxFcnWallSA3D(varFcnSGSA) {}
  ~FluxFcnSGWallSA3D() {
    vf = 0;
  }

  void compute(double, double, double *, double *, double, double *, double *, double *, bool);
  void computeJacobian(double, double, double *, double *, double, double *, double *, double *, FluxFcnBase::Type, bool);
  void computeDerivative(double, double, double, double *, double *, double, double, double *, double *, double *,
                         double *, double, double *, double *, bool);
  void computeDerivativeOperators(double, double, double *, double, double *, double *, double *, double *, double *,
                                  double *, double *, bool);

};

//------------------------------------------------------------------------------

class FluxFcnSGPorousWallSA3D : public FluxFcnPorousWallSA3D {

 public:
  FluxFcnSGPorousWallSA3D(IoData& ioData, VarFcnSGSA *varFcnSGSA) :
    FluxFcnPorousWallSA3D(varFcnSGSA) {}
  ~FluxFcnSGPorousWallSA3D() {
    vf = 0;
  }

  void compute(double, double, double *, double *, double, double *, double *, double *, bool);

};

//------------------------------------------------------------------------------

class FluxFcnSGGhidagliaSA3D : public FluxFcnGhidagliaSA3D {

 public:
  FluxFcnSGGhidagliaSA3D(IoData& ioData, VarFcnSGSA *varFcnSGSA) :
    FluxFcnGhidagliaSA3D(varFcnSGSA) {}
  ~FluxFcnSGGhidagliaSA3D() {
    vf = 0;
  }

  void compute(double, double, double *, double *, double, double *, double *, double *, bool);

};

//------------------------------------------------------------------------------

class FluxFcnSGOutflowSA3D : public FluxFcnOutflowSA3D {

 public:
  FluxFcnSGOutflowSA3D(IoData& ioData, VarFcnSGSA *varFcnSGSA) :
    FluxFcnOutflowSA3D(varFcnSGSA) {}
  ~FluxFcnSGOutflowSA3D() {
    vf = 0;
  }

  void compute(double, double, double *, double *, double, double *, double *, double *, bool);
  void computeJacobian(double, double, double *, double *, double, double *, double *, double *, FluxFcnBase::Type, bool);
  void computeDerivative(double, double, double, double *, double *, double, double, double *, double *, double *,
                         double *, double, double *, double *, bool);
  void computeDerivativeOperators(double, double, double *, double, double *, double *, double *, double *, double *,
                                  double *, double *, bool);

};

//------------------------------------------------------------------------------

class FluxFcnSGInternalInflowSA3D : public FluxFcnInternalInflowSA3D {

 public:
  FluxFcnSGInternalInflowSA3D(IoData& ioData, VarFcnSGSA *varFcnSGSA) :
    FluxFcnInternalInflowSA3D(varFcnSGSA) {}
  ~FluxFcnSGInternalInflowSA3D() {
    vf = 0;
  }

  void compute(double, double, double *, double *, double, double *, double *, double *, bool);
  void computeJacobian(double, double, double *, double *, double, double *, double *, double *, FluxFcnBase::Type, bool);

};

//------------------------------------------------------------------------------

class FluxFcnSGDirectStateInflowSA3D : public FluxFcnDirectStateInflowSA3D {

 public:
  FluxFcnSGDirectStateInflowSA3D(IoData& ioData, VarFcnSGSA *varFcnSGSA) :
    FluxFcnDirectStateInflowSA3D(ioData, varFcnSGSA) {}
  ~FluxFcnSGDirectStateInflowSA3D() {
    vf = 0;
  }

  void compute(double, double, double *, double *, double, double *, double *, double *, bool);
  void computeJacobian(double, double, double *, double *, double, double *, double *, double *, FluxFcnBase::Type, bool);

};

//------------------------------------------------------------------------------

class FluxFcnSGMassFlowInflowSA3D : public FluxFcnMassFlowInflowSA3D {

 public:
  FluxFcnSGMassFlowInflowSA3D(IoData& ioData, VarFcnSGSA *varFcnSGSA) :
    FluxFcnMassFlowInflowSA3D(varFcnSGSA) {}
  ~FluxFcnSGMassFlowInflowSA3D() {
    vf = 0;
  }

  void compute(double, double, double *, double *, double, double *, double *, double *, bool);

};

//------------------------------------------------------------------------------

class FluxFcnSGInternalOutflowSA3D : public FluxFcnInternalOutflowSA3D {

 public:
  FluxFcnSGInternalOutflowSA3D(IoData& ioData, VarFcnSGSA *varFcnSGSA) :
    FluxFcnInternalOutflowSA3D(varFcnSGSA) {}
  ~FluxFcnSGInternalOutflowSA3D() {
    vf = 0;
  }

  void compute(double, double, double *, double *, double, double *, double *, double *, bool);
  void computeJacobian(double, double, double *, double *, double, double *, double *, double *, FluxFcnBase::Type, bool);

};

//------------------------------------------------------------------------------

class FluxFcnSGDirectStateOutflowSA3D : public FluxFcnDirectStateOutflowSA3D {

 public:
  FluxFcnSGDirectStateOutflowSA3D(IoData& ioData, VarFcnSGSA *varFcnSGSA) :
    FluxFcnDirectStateOutflowSA3D(ioData, varFcnSGSA) {}
  ~FluxFcnSGDirectStateOutflowSA3D() {
    vf = 0;
  }

  void compute(double, double, double *, double *, double, double *, double *, double *, bool);
  void computeJacobian(double, double, double *, double *, double, double *, double *, double *, FluxFcnBase::Type, bool);

};

//------------------------------------------------------------------------------

class FluxFcnSGMassFlowOutflowSA3D : public FluxFcnMassFlowOutflowSA3D {

 public:
  FluxFcnSGMassFlowOutflowSA3D(IoData& ioData, VarFcnSGSA *varFcnSGSA) :
    FluxFcnMassFlowOutflowSA3D(varFcnSGSA) {}
  ~FluxFcnSGMassFlowOutflowSA3D() {
    vf = 0;
  }

  void compute(double, double, double *, double *, double, double *, double *, double *, bool);

};

//------------------------------------------------------------------------------

class FluxFcnSGRoeSAturb3D : public FluxFcnRoeSAturb3D {

 public:
  FluxFcnSGRoeSAturb3D(double gg, IoData& ioData, VarFcnSGSA *varFcnSGSA) :
    FluxFcnRoeSAturb3D(ioData, gg, varFcnSGSA) {}
  ~FluxFcnSGRoeSAturb3D() {
    vf = 0;
  }

  void compute(double, double, double *, double *n, double nv, double *vl, double *vr, double *f, bool) {}
  void computeJacobians(double, double, double *, double *, double, double *, double *, double *, double *, FluxFcnBase::Type, bool);

};

//------------------------------------------------------------------------------

class FluxFcnSGWallSAturb3D : public FluxFcnWallSAturb3D {

 public:
  FluxFcnSGWallSAturb3D(IoData& ioData, VarFcnSGSA *varFcnSGSA) :
    FluxFcnWallSAturb3D(varFcnSGSA) {}
  ~FluxFcnSGWallSAturb3D() {
    vf = 0;
  }

  void compute(double, double, double *, double *n, double nv, double *vl, double *vr, double *f, bool) {}
  void computeJacobian(double, double, double *, double *, double, double *, double *, double *, FluxFcnBase::Type, bool);

};

//------------------------------------------------------------------------------

class FluxFcnSGPorousWallSAturb3D : public FluxFcnPorousWallSAturb3D {

 public:
  FluxFcnSGPorousWallSAturb3D(IoData& ioData, VarFcnSGSA *varFcnSGSA) :
    FluxFcnPorousWallSAturb3D(varFcnSGSA) {}
  ~FluxFcnSGPorousWallSAturb3D() {
    vf = 0;
  }

  void compute(double, double, double *, double *n, double nv, double *vl, double *vr, double *f, bool) {}
  void computeJacobian(double, double, double *, double *, double, double *, double *, double *, FluxFcnBase::Type, bool);

};

//------------------------------------------------------------------------------

class FluxFcnSGGhidagliaSAturb3D : public FluxFcnGhidagliaSAturb3D {

 public:
  FluxFcnSGGhidagliaSAturb3D(IoData& ioData, VarFcnSGSA *varFcnSGSA) :
    FluxFcnGhidagliaSAturb3D(varFcnSGSA) {}
  ~FluxFcnSGGhidagliaSAturb3D() {
    vf = 0;
  }

  void compute(double, double, double *, double *n, double nv, double *vl, double *vr, double *f, bool) {}
  void computeJacobian(double, double, double *, double *, double, double *, double *, double *, FluxFcnBase::Type, bool);

};

//------------------------------------------------------------------------------

class FluxFcnSGOutflowSAturb3D : public FluxFcnOutflowSAturb3D {

 public:
  FluxFcnSGOutflowSAturb3D(IoData& ioData, VarFcnSGSA *varFcnSGSA) :
    FluxFcnOutflowSAturb3D(varFcnSGSA) {}
  ~FluxFcnSGOutflowSAturb3D() {
    vf = 0;
  }

  void compute(double, double, double *, double *n, double nv, double *vl, double *vr, double *f, bool) {}
  void computeJacobian(double, double, double *, double *, double, double *, double *, double *, FluxFcnBase::Type, bool);

};

//------------------------------------------------------------------------------

class FluxFcnSGInternalInflowSAturb3D : public FluxFcnInternalInflowSAturb3D {

 public:
  FluxFcnSGInternalInflowSAturb3D(IoData& ioData, VarFcnSGSA *varFcnSGSA) :
    FluxFcnInternalInflowSAturb3D(varFcnSGSA) {}
  ~FluxFcnSGInternalInflowSAturb3D() {
    vf = 0;
  }

  void compute(double, double, double *, double *n, double nv, double *vl, double *vr, double *f, bool) {}
  void computeJacobian(double, double, double *, double *, double, double *, double *, double *, FluxFcnBase::Type, bool);

};

//------------------------------------------------------------------------------

class FluxFcnSGDirectStateInflowSAturb3D : public FluxFcnDirectStateInflowSAturb3D {

 public:
  FluxFcnSGDirectStateInflowSAturb3D(IoData& ioData, VarFcnSGSA *varFcnSGSA) :
    FluxFcnDirectStateInflowSAturb3D(ioData, varFcnSGSA) {}
  ~FluxFcnSGDirectStateInflowSAturb3D() {
    vf = 0;
  }

  void compute(double, double, double *, double *n, double nv, double *vl, double *vr, double *f, bool) {}
  void computeJacobian(double, double, double *, double *, double, double *, double *, double *, FluxFcnBase::Type, bool);

};

//------------------------------------------------------------------------------

class FluxFcnSGMassFlowInflowSAturb3D : public FluxFcnMassFlowInflowSAturb3D {

 public:
  FluxFcnSGMassFlowInflowSAturb3D(IoData& ioData, VarFcnSGSA *varFcnSGSA) :
    FluxFcnMassFlowInflowSAturb3D(varFcnSGSA) {}
  ~FluxFcnSGMassFlowInflowSAturb3D() {
    vf = 0;
  }

  void compute(double, double, double *, double *n, double nv, double *vl, double *vr, double *f, bool) {}
  void computeJacobian(double, double, double *, double *, double, double *, double *, double *, FluxFcnBase::Type, bool);

};

//------------------------------------------------------------------------------

class FluxFcnSGInternalOutflowSAturb3D : public FluxFcnInternalOutflowSAturb3D {

 public:
  FluxFcnSGInternalOutflowSAturb3D(IoData& ioData, VarFcnSGSA *varFcnSGSA) :
    FluxFcnInternalOutflowSAturb3D(varFcnSGSA) {}
  ~FluxFcnSGInternalOutflowSAturb3D() {
    vf = 0;
  }

  void compute(double, double, double *, double *n, double nv, double *vl, double *vr, double *f, bool) {}
  void computeJacobian(double, double, double *, double *, double, double *, double *, double *, FluxFcnBase::Type, bool);

};

//------------------------------------------------------------------------------

class FluxFcnSGDirectStateOutflowSAturb3D : public FluxFcnDirectStateOutflowSAturb3D {

 public:
  FluxFcnSGDirectStateOutflowSAturb3D(IoData& ioData, VarFcnSGSA *varFcnSGSA) :
    FluxFcnDirectStateOutflowSAturb3D(ioData, varFcnSGSA) {}
  ~FluxFcnSGDirectStateOutflowSAturb3D() {
    vf = 0;
  }

  void compute(double, double, double *, double *n, double nv, double *vl, double *vr, double *f, bool) {}
  void computeJacobian(double, double, double *, double *, double, double *, double *, double *, FluxFcnBase::Type, bool);

};

//------------------------------------------------------------------------------

class FluxFcnSGMassFlowOutflowSAturb3D : public FluxFcnMassFlowOutflowSAturb3D {

 public:
  FluxFcnSGMassFlowOutflowSAturb3D(IoData& ioData, VarFcnSGSA *varFcnSGSA) :
    FluxFcnMassFlowOutflowSAturb3D(varFcnSGSA) {}
  ~FluxFcnSGMassFlowOutflowSAturb3D() {
    vf = 0;
  }

  void compute(double, double, double *, double *n, double nv, double *vl, double *vr, double *f, bool) {}
  void computeJacobian(double, double, double *, double *, double, double *, double *, double *, FluxFcnBase::Type, bool);

};

//------------------------------------------------------------------------------

class FluxFcnSGFDJacRoeKE3D : public FluxFcnFDJacRoeKE3D {

 public:
  FluxFcnSGFDJacRoeKE3D(double gg, IoData& ioData, VarFcnSGKE *varFcnSGKE) :
    FluxFcnFDJacRoeKE3D(ioData, gg, varFcnSGKE) {}
  ~FluxFcnSGFDJacRoeKE3D() {
    vf = 0;
  }

  void compute(double, double, double *, double *, double, double *, double *, double *, bool);
  void computeDerivative(double, double, double, double *, double *, double, double, double *, double *, double *,
                         double *, double, double *, double *, bool);

};

//------------------------------------------------------------------------------

class FluxFcnSGApprJacRoeKE3D : public FluxFcnApprJacRoeKE3D {

 public:
  FluxFcnSGApprJacRoeKE3D(int rs, double gg, IoData& ioData, VarFcnSGKE *varFcnSGKE) :
    FluxFcnApprJacRoeKE3D(ioData, rs, gg, varFcnSGKE) {}
  ~FluxFcnSGApprJacRoeKE3D() {
    vf = 0;
  }

  void compute(double, double, double *, double *, double, double *, double *, double *, bool);
  void computeJacobians(double, double, double *, double *, double, double *, double *, double *, double *, FluxFcnBase::Type, bool);

};

//------------------------------------------------------------------------------

class FluxFcnSGExactJacRoeKE3D : public FluxFcnExactJacRoeKE3D {

 public:
  FluxFcnSGExactJacRoeKE3D(double gg, IoData& ioData, VarFcnSGKE *varFcnSGKE) :
    FluxFcnExactJacRoeKE3D(ioData, gg, varFcnSGKE) {}
  ~FluxFcnSGExactJacRoeKE3D() {
    vf = 0;
  }

  void compute(double, double, double *, double *, double, double *, double *, double *, bool);
  void computeJacobians(double, double, double *, double *, double, double *, double *, double *, double *, FluxFcnBase::Type, bool);
  void computeDerivative(double, double, double, double *, double *, double, double, double *, double *, double *,
                         double *, double, double *, double *, bool);
  void computeDerivativeOperators(double, double, double *, double, double *, double *, double *, double *, double *,
                                  double *, double *, bool);

};

//------------------------------------------------------------------------------

class FluxFcnSGFDJacHLLEKE3D : public FluxFcnFDJacHLLEKE3D {

 public:
  FluxFcnSGFDJacHLLEKE3D(double gg, IoData& ioData, VarFcnSGKE *varFcnSGKE) :
    FluxFcnFDJacHLLEKE3D(ioData, gg, varFcnSGKE) {}
  ~FluxFcnSGFDJacHLLEKE3D() {
    vf = 0;
  }

  void compute(double, double, double *, double *, double, double *, double *, double *, bool);
  void computeDerivative(double, double, double, double *, double *, double, double, double *, double *, double *,
                         double *, double, double *, double *, bool);

};

//------------------------------------------------------------------------------

class FluxFcnSGApprJacHLLEKE3D : public FluxFcnApprJacHLLEKE3D {

 public:
  FluxFcnSGApprJacHLLEKE3D(int rs, double gg, IoData& ioData, VarFcnSGKE *varFcnSGKE) :
    FluxFcnApprJacHLLEKE3D(ioData, rs, gg, varFcnSGKE) {}
  ~FluxFcnSGApprJacHLLEKE3D() {
    vf = 0;
  }

  void compute(double, double, double *, double *, double, double *, double *, double *, bool);
  void computeJacobians(double, double, double *, double *, double, double *, double *, double *, double *, FluxFcnBase::Type, bool);
  void computeDerivative(double, double, double, double *, double *, double, double, double *, double *, double *,
                         double *, double, double *, double *, bool);

};

//------------------------------------------------------------------------------

class FluxFcnSGExactJacHLLEKE3D : public FluxFcnExactJacHLLEKE3D {

 public:
  FluxFcnSGExactJacHLLEKE3D(int rs, double gg, IoData& ioData, VarFcnSGKE *varFcnSGKE) :
    FluxFcnExactJacHLLEKE3D(ioData, rs, gg, varFcnSGKE) {}
  ~FluxFcnSGExactJacHLLEKE3D() {
    vf = 0;
  }

  void compute(double, double, double *, double *, double, double *, double *, double *, bool);
  void computeJacobians(double, double, double *, double *, double, double *, double *, double *, double *, FluxFcnBase::Type, bool);
  void computeDerivative(double, double, double, double *, double *, double, double, double *, double *, double *,
                         double *, double, double *, double *, bool);
  void computeDerivativeOperators(double, double, double *, double, double *, double *, double *, double *, double *,
                                  double *, double *, bool);

};

//------------------------------------------------------------------------------

class FluxFcnSGFDJacHLLCKE3D : public FluxFcnFDJacHLLCKE3D {

 public:
  FluxFcnSGFDJacHLLCKE3D(double gg, IoData& ioData, VarFcnSGKE *varFcnSGKE) :
    FluxFcnFDJacHLLCKE3D(ioData, gg, varFcnSGKE) {}
  ~FluxFcnSGFDJacHLLCKE3D() {
    vf = 0;
  }

  void compute(double, double, double *, double *, double, double *, double *, double *, bool);
  void computeDerivative(double, double, double, double *, double *, double, double, double *, double *, double *,
                         double *, double, double *, double *, bool);

};

//------------------------------------------------------------------------------

class FluxFcnSGApprJacHLLCKE3D : public FluxFcnApprJacHLLCKE3D {

 public:
  FluxFcnSGApprJacHLLCKE3D(int rs, double gg, IoData& ioData, VarFcnSGKE *varFcnSGKE) :
    FluxFcnApprJacHLLCKE3D(ioData, rs, gg, varFcnSGKE) {}
  ~FluxFcnSGApprJacHLLCKE3D() {
    vf = 0;
  }

  void compute(double, double, double *, double *, double, double *, double *, double *, bool);
  void computeJacobians(double, double, double *, double *, double, double *, double *, double *, double *, FluxFcnBase::Type, bool);
  void computeDerivative(double, double, double, double *, double *, double, double, double *, double *, double *,
                         double *, double, double *, double *, bool);

};

//------------------------------------------------------------------------------

class FluxFcnSGExactJacHLLCKE3D : public FluxFcnExactJacHLLCKE3D {

 public:
  FluxFcnSGExactJacHLLCKE3D(int rs, double gg, IoData& ioData, VarFcnSGKE *varFcnSGKE) :
    FluxFcnExactJacHLLCKE3D(ioData, rs, gg, varFcnSGKE) {}
  ~FluxFcnSGExactJacHLLCKE3D() {
    vf = 0;
  }

  void compute(double, double, double *, double *, double, double *, double *, double *, bool);
  void computeJacobians(double, double, double *, double *, double, double *, double *, double *, double *, FluxFcnBase::Type, bool);
  void computeDerivative(double, double, double, double *, double *, double, double, double *, double *, double *,
                         double *, double, double *, double *, bool);
  void computeDerivativeOperators(double, double, double *, double, double *, double *, double *, double *, double *,
                                  double *, double *, bool);

};

//------------------------------------------------------------------------------

class FluxFcnSGWallKE3D : public FluxFcnWallKE3D {

 public:
  FluxFcnSGWallKE3D(IoData& ioData, VarFcnSGKE *varFcnSGKE) :
    FluxFcnWallKE3D(varFcnSGKE) {}
  ~FluxFcnSGWallKE3D() {
    vf = 0;
  }

  void compute(double, double, double *, double *, double, double *, double *, double *, bool);
  void computeJacobian(double, double, double *, double *, double, double *, double *, double *, FluxFcnBase::Type, bool);
  void computeDerivative(double, double, double, double *, double *, double, double, double *, double *, double *,
                         double *, double, double *, double *, bool);
  void computeDerivativeOperators(double, double, double *, double, double *, double *, double *, double *, double *,
                                  double *, double *, bool);

};

//------------------------------------------------------------------------------

class FluxFcnSGPorousWallKE3D : public FluxFcnPorousWallKE3D {

 public:
  FluxFcnSGPorousWallKE3D(IoData& ioData, VarFcnSGKE *varFcnSGKE) :
    FluxFcnPorousWallKE3D(varFcnSGKE) {}
  ~FluxFcnSGPorousWallKE3D() {
    vf = 0;
  }

  void compute(double, double, double *, double *, double, double *, double *, double *, bool);

};

//------------------------------------------------------------------------------

class FluxFcnSGGhidagliaKE3D : public FluxFcnGhidagliaKE3D {

 public:
  FluxFcnSGGhidagliaKE3D(IoData& ioData, VarFcnSGKE *varFcnSGKE) :
    FluxFcnGhidagliaKE3D(varFcnSGKE) {}
  ~FluxFcnSGGhidagliaKE3D() {
    vf = 0;
  }

  void compute(double, double, double *, double *, double, double *, double *, double *, bool);

};

//------------------------------------------------------------------------------

class FluxFcnSGOutflowKE3D : public FluxFcnOutflowKE3D {

 public:
  FluxFcnSGOutflowKE3D(IoData& ioData, VarFcnSGKE *varFcnSGKE) :
    FluxFcnOutflowKE3D(varFcnSGKE) {}
  ~FluxFcnSGOutflowKE3D() {
    vf = 0;
  }

  void compute(double, double, double *, double *, double, double *, double *, double *, bool);
  void computeJacobian(double, double, double *, double *, double, double *, double *, double *, FluxFcnBase::Type, bool);
  void computeDerivative(double, double, double, double *, double *, double, double, double *, double *, double *,
                         double *, double, double *, double *, bool);
  void computeDerivativeOperators(double, double, double *, double, double *, double *, double *, double *, double *,
                                  double *, double *, bool);

};

//------------------------------------------------------------------------------

class FluxFcnSGDirectStateInflowKE3D : public FluxFcnDirectStateInflowKE3D {

 public:
  FluxFcnSGDirectStateInflowKE3D(IoData& ioData, VarFcnSGKE *varFcnSGKE) :
    FluxFcnDirectStateInflowKE3D(ioData, varFcnSGKE) {}
  ~FluxFcnSGDirectStateInflowKE3D() {
    vf = 0;
  }

  void compute(double, double, double *, double *, double, double *, double *, double *, bool);
  void computeJacobian(double, double, double *, double *, double, double *, double *, double *, FluxFcnBase::Type, bool);

};

//------------------------------------------------------------------------------

class FluxFcnSGMassFlowInflowKE3D : public FluxFcnMassFlowInflowKE3D {

 public:
  FluxFcnSGMassFlowInflowKE3D(IoData& ioData, VarFcnSGKE *varFcnSGKE) :
    FluxFcnMassFlowInflowKE3D(varFcnSGKE) {}
  ~FluxFcnSGMassFlowInflowKE3D() {
    vf = 0;
  }

  void compute(double, double, double *, double *, double, double *, double *, double *, bool);

};

//------------------------------------------------------------------------------

class FluxFcnSGDirectStateOutflowKE3D : public FluxFcnDirectStateOutflowKE3D {

 public:
  FluxFcnSGDirectStateOutflowKE3D(IoData& ioData, VarFcnSGKE *varFcnSGKE) :
    FluxFcnDirectStateOutflowKE3D(ioData, varFcnSGKE) {}
  ~FluxFcnSGDirectStateOutflowKE3D() {
    vf = 0;
  }

  void compute(double, double, double *, double *, double, double *, double *, double *, bool);
  void computeJacobian(double, double, double *, double *, double, double *, double *, double *, FluxFcnBase::Type, bool);

};

//------------------------------------------------------------------------------

class FluxFcnSGMassFlowOutflowKE3D : public FluxFcnMassFlowOutflowKE3D {

 public:
  FluxFcnSGMassFlowOutflowKE3D(IoData& ioData, VarFcnSGKE *varFcnSGKE) :
    FluxFcnMassFlowOutflowKE3D(varFcnSGKE) {}
  ~FluxFcnSGMassFlowOutflowKE3D() {
    vf = 0;
  }

  void compute(double, double, double *, double *, double, double *, double *, double *, bool);

};

//------------------------------------------------------------------------------

class FluxFcnSGRoeKEturb3D : public FluxFcnRoeKEturb3D {

 public:
  FluxFcnSGRoeKEturb3D(double gg, IoData& ioData, VarFcnSGKE *varFcnSGKE) :
    FluxFcnRoeKEturb3D(ioData, gg, varFcnSGKE) {}
  ~FluxFcnSGRoeKEturb3D() {
    vf = 0;
  }

  void compute(double, double, double *, double *n, double nv, double *vl, double *vr, double *f, bool) {}
  void computeJacobians(double, double, double *, double *, double, double *, double *, double *, double *, FluxFcnBase::Type, bool);

};

//------------------------------------------------------------------------------

class FluxFcnSGWallKEturb3D : public FluxFcnWallKEturb3D {

 public:
  FluxFcnSGWallKEturb3D(IoData& ioData, VarFcnSGKE *varFcnSGKE) :
    FluxFcnWallKEturb3D(varFcnSGKE) {}
  ~FluxFcnSGWallKEturb3D() {
    vf = 0;
  }

  void compute(double, double, double *, double *n, double nv, double *vl, double *vr, double *f, bool) {}
  void computeJacobian(double, double, double *, double *, double, double *, double *, double *, FluxFcnBase::Type, bool);

};

//------------------------------------------------------------------------------

class FluxFcnSGPorousWallKEturb3D : public FluxFcnPorousWallKEturb3D {

 public:
  FluxFcnSGPorousWallKEturb3D(IoData& ioData, VarFcnSGKE *varFcnSGKE) :
    FluxFcnPorousWallKEturb3D(varFcnSGKE) {}
  ~FluxFcnSGPorousWallKEturb3D() {
    vf = 0;
  }

  void compute(double, double, double *, double *n, double nv, double *vl, double *vr, double *f, bool) {}
  void computeJacobian(double, double, double *, double *, double, double *, double *, double *, FluxFcnBase::Type, bool);

};

//------------------------------------------------------------------------------

class FluxFcnSGGhidagliaKEturb3D : public FluxFcnGhidagliaKEturb3D {

 public:
  FluxFcnSGGhidagliaKEturb3D(IoData& ioData, VarFcnSGKE *varFcnSGKE) :
    FluxFcnGhidagliaKEturb3D(varFcnSGKE) {}
  ~FluxFcnSGGhidagliaKEturb3D() {
    vf = 0;
  }

  void compute(double, double, double *, double *n, double nv, double *vl, double *vr, double *f, bool) {}
  void computeJacobian(double, double, double *, double *, double, double *, double *, double *, FluxFcnBase::Type, bool);

};

//------------------------------------------------------------------------------

class FluxFcnSGOutflowKEturb3D : public FluxFcnOutflowKEturb3D {

 public:
  FluxFcnSGOutflowKEturb3D(IoData& ioData, VarFcnSGKE *varFcnSGKE) :
    FluxFcnOutflowKEturb3D(varFcnSGKE) {}
  ~FluxFcnSGOutflowKEturb3D() {
    vf = 0;
  }

  void compute(double, double, double *, double *n, double nv, double *vl, double *vr, double *f, bool) {}
  void computeJacobian(double, double, double *, double *, double, double *, double *, double *, FluxFcnBase::Type, bool);

};

//------------------------------------------------------------------------------

class FluxFcnSGDirectStateInflowKEturb3D : public FluxFcnDirectStateInflowKEturb3D {

 public:
  FluxFcnSGDirectStateInflowKEturb3D(IoData& ioData, VarFcnSGKE *varFcnSGKE) :
    FluxFcnDirectStateInflowKEturb3D(ioData, varFcnSGKE) {}
  ~FluxFcnSGDirectStateInflowKEturb3D() {
    vf = 0;
  }

  void compute(double, double, double *, double *n, double nv, double *vl, double *vr, double *f, bool) {}
  void computeJacobian(double, double, double *, double *, double, double *, double *, double *, FluxFcnBase::Type, bool);

};

//------------------------------------------------------------------------------

class FluxFcnSGMassFlowInflowKEturb3D : public FluxFcnMassFlowInflowKEturb3D {

 public:
  FluxFcnSGMassFlowInflowKEturb3D(IoData& ioData, VarFcnSGKE *varFcnSGKE) :
    FluxFcnMassFlowInflowKEturb3D(varFcnSGKE) {}
  ~FluxFcnSGMassFlowInflowKEturb3D() {
    vf = 0;
  }

  void compute(double, double, double *, double *n, double nv, double *vl, double *vr, double *f, bool) {}
  void computeJacobian(double, double, double *, double *, double, double *, double *, double *, FluxFcnBase::Type, bool);

};

//------------------------------------------------------------------------------

class FluxFcnSGDirectStateOutflowKEturb3D : public FluxFcnDirectStateOutflowKEturb3D {

 public:
  FluxFcnSGDirectStateOutflowKEturb3D(IoData& ioData, VarFcnSGKE *varFcnSGKE) :
    FluxFcnDirectStateOutflowKEturb3D(ioData, varFcnSGKE) {}
  ~FluxFcnSGDirectStateOutflowKEturb3D() {
    vf = 0;
  }

  void compute(double, double, double *, double *n, double nv, double *vl, double *vr, double *f, bool) {}
  void computeJacobian(double, double, double *, double *, double, double *, double *, double *, FluxFcnBase::Type, bool);

};

//------------------------------------------------------------------------------

class FluxFcnSGMassFlowOutflowKEturb3D : public FluxFcnMassFlowOutflowKEturb3D {

 public:
  FluxFcnSGMassFlowOutflowKEturb3D(IoData& ioData, VarFcnSGKE *varFcnSGKE) :
    FluxFcnMassFlowOutflowKEturb3D(varFcnSGKE) {}
  ~FluxFcnSGMassFlowOutflowKEturb3D() {
    vf = 0;
  }

  void compute(double, double, double *, double *n, double nv, double *vl, double *vr, double *f, bool) {}
  void computeJacobian(double, double, double *, double *, double, double *, double *, double *, FluxFcnBase::Type, bool);

};

//------------------------------------------------------------------------------

#endif


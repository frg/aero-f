#ifndef _TS_DESC_H_
#define _TS_DESC_H_

#include <Domain.h>
#include <PostOperator.h>
#include <TsRestart.h>
#include <DistVector.h>
#include <NodeData.h>
#include <Types.h>
#include <set>

struct Vec3D;

class RefVal;
class VarFcn;
class GeoSource;
class DistGeoState;
class DistMeshState;
class EmbeddedMeshMotionHandler;
class MeshMotionHandler;
class HeatTransferHandler;
class Timer;
class TsInput;
class ErrorHandler;
class DynamicNodalTransfer;
class ReinitializeDistanceToWall;
class ReinitializeGap;
class WallFcn;
class DistMeshAdapter;
class ProgrammedBurn;
class TsParameters;

template<int dim> class DistBcData;
template<int dim> class SpaceOperator;
template<int dim> class DistTimeState;
template<int dim> class FluidShapeOptimizationHandler;
template<int dim> class PostOperator;
template<int dimLS> class LevelSet;

//-------------- Base class for time integration descriptors -------------------

template<int dim>
class TsDesc : public ForceGenerator<dim> {

 public:

  typedef DistSVec<double, dim> SolVecType;
  typedef DistSVec<double, 3> PosVecType;
  typedef DistVec<double> VolVecType;

  TsParameters *data;
  Timer *timer;

 protected:

  SolVecType *V;
  SolVecType *R;
  DistSVec<double, 3> *gradP;

  bool *problemType;
  TsInput *input;
  TsOutput<dim> *output;
  TsRestart *restart;

  VarFcn *varFcn;
  RefVal *refVal;

  DistTimeState<dim> *timeState;
  DistBcData<dim> *bcData;
  DistGeoState *geoState;
  SpaceOperator<dim> *spaceOp;
  PostOperator<dim> *postOp;

  IoData& ioData;
  GeoSource& geoSource;
  Domain *domain;
  DistMeshAdapter *meshAdapter;

  Communicator *com;
  ErrorHandler *errorHandler;

  DistVec<int> *swept;
  RTree<Vec3D> *supernodes;

  double forceNorm;
  double *forceNorms;

  bool meshRefinedOrBalancedSinceLastOutput;
  bool meshRefinedOrBalancedSinceLastSnapshot;
  bool meshRefinedOrBalancedSinceLastRestartDump;
  bool meshRefinedSinceLastBalancing;
  bool meshRefinedSinceLastWallDistance;
  int meshOutputIndex;
  int meshSnapshotIndex;
  bool recomputeResidualAfterBalancingOrOntheflyRestart;
  bool initializeCorrectorSinceLastIntersection;
  int_t min_gl_deleted_index[3];

  DistEmbeddedBcData<dim> *ebcData;
  DistMeshState *preAmrMeshState;
  DistRestartState<dim> *preAmrRestartState;

  //---------------------- moved from LevelSetTsDesc ---------------------------
  ProgrammedBurn *programmedBurn;
  LevelSet<dimLS> *LS;
  DistSVec<double, dimLS> *Phi;  // conservative variables
  DistSVec<double, dimLS> *PhiV; // primitive variables
  double currentTime;
  //----------------------------------------------------------------------------

  //---------------------- moved from EmbeddedTsDesc ---------------------------
  DistVec<int> nodeTag;  // fluid id
  double (*Fs)[3];       // derivative force distribution on the structure surface
  double (*dFs)[3];      // force distribution on the structure surface
  bool FsComputed;       // whether Fs has been computed for this (fluid-)time step.
  double dtf;            // fluid time-step
  double dtfLeft;        // time until next structure time-step is reached.
  int globIt;            // current global(i.e. structure) iteration
  bool inSubCycling;     // is it in subcyling (i.e. itSc>1)
  bool inRedoTimestep;
  DistSVec<double, dim> *VWeights; // stores U*Weights for each node. Used in updating phase change.
  DistVec<double> *Weights;        // weights for each node. Used in updating phase change.
  ReinitializeDistanceToWall *wall_computer;
  ReinitializeGap *gap_computer;
  DynamicNodalTransfer *dynNodalTransfer;
  EmbeddedMeshMotionHandler *emmh;
  //----------------------------------------------------------------------------

  //---------------------- mesh motion & heat transfer -------------------------
  PosVecType *Xs;
  MeshMotionHandler *mmh;
  HeatTransferHandler *hth;
  //----------------------------------------------------------------------------

  WallFcn *wallFcn;
  Vec<Vec3D> *solidX;

  // minimum number of iterations for steady (used to force x iterations even for converged solution)
  int minIts;

 public:
  // objects for oversubscribed processes (multiple online meshes)
  int *iterationPtr;
  double *timePtr;
  bool clusterDone;

 public:
  TsDesc(IoData&, GeoSource&, Domain *);
  ~TsDesc();

  virtual void rstVar(IoData&);

  Domain *getDomain() {
    return domain;
  }

  SpaceOperator<dim> *getSpaceOperator() {
    return spaceOp;
  }
  DistGeoState *getGeoState() {
    return geoState;
  }
  DistTimeState<dim> *getTimeState() {
    return timeState;
  }
  Communicator *getCommunicator() {
    return com;
  }
  DistBcData<dim> *getBcData() {
    return bcData;
  }
  DistEmbeddedBcData<dim> *getEmbeddedBcData() {
    return ebcData;
  }
  void setInRedoTimestep(bool flag) {
    inRedoTimestep = flag;
  }
  int getInitialIteration() const {
    return restart->iteration;
  }
  double getInitialTime() const {
    return restart->etime;
  }
  int getCorrectorIteration() {
    return restart->corrector_iteration;
  }
  DistInfo& getVecInfo() const {
    return domain->getNodeDistInfo();
  }
  TsParameters *getTsParams() {
    return data;
  }
  ErrorHandler *getErrorHandler() {
    return errorHandler;
  }
  bool& getAppendMode() {
    return output->getAppendMode();
  }

  void printf(int, const char *, ...);
  void fprintf(FILE *, const char *, ...);
  static DistBcData<dim> *createBcData(IoData&, VarFcn *, Domain *, DistGeoState *);
  DistEmbeddedBcData<dim> *createEmbeddedBcData();
  MeshMotionHandler *createMeshMotionHandler();
  MeshMotionHandler *createEmbeddedALEMeshMotionHandler();
  HeatTransferHandler *createHeatTransferHandler();

  virtual int solveNonLinearSystem(DistSVec<double, dim>&, int, double, DistSVec<double, dim> * = nullptr) = 0;

  virtual bool formInitialCondition(DistSVec<double, dim>&) { return false; };
  virtual double computeResidualNorm(DistSVec<double, dim>&);
  void monitorInitialState(int, DistSVec<double, dim>&);
  bool monitorConvergence(int, DistSVec<double, dim>&);
  void setupTimeStepping(DistSVec<double, dim> *);
  double computeTimeStep(int, double&, DistSVec<double, dim>&, double, double);
  double computePositionVector(bool *, int, double);
  bool isPP();
  void outputPositionVectorToDisk(DistSVec<double, dim>&);
  void resetOutputToStructure(DistSVec<double, dim>&);
  void updateOutputToStructure(double, double, DistSVec<double, dim>&);
  void setCurrentTime(double, DistSVec<double, dim>&);
  void interpolatePositionVector(double, double);
  void computeMeshMetrics(int = -1);
  void updateStateVectors(DistSVec<double, dim>&, int = 0);
  virtual bool checkForLastIteration(int, double, double, DistSVec<double, dim>&);
  virtual bool checkForAmrRefinement(int, bool);
  bool checkForAmrCoarsening(int);
  virtual bool checkForAmrBalancing(int, bool, bool = true);
  void recomputeRestartResidual(bool);
  void setupOutputToDisk(bool *, int, double, DistSVec<double, dim>&);
  virtual void outputToDisk(bool *, int, int, int, double, double, DistSVec<double, dim>&);
  virtual void outputSnapshotsToDisk(IoData&, bool, int, int, double, DistSVec<double, dim>&);
  virtual void printTimer();
  void outputForces(bool *, int, int, int, double, double, DistSVec<double, dim>&);
  void outputToScreen(int, int, int, double);
  int checkSolution(DistSVec<double, dim>&, int * = nullptr);
  virtual void writeReducedCoordsToDisk(bool, int) {}
  virtual void writeReducedResidToDisk(bool, int) {}
  virtual void writeReducedJacToDisk(bool, int) {}
  virtual void projectState(DistSVec<double, dim>&, Vec<double>&) {}
  virtual void projectState(DistSVec<double, dim>&, int) {}
  virtual void incrementSolution() {}

  void computeDistanceToWall(double, DistSVec<double, dim>&, int, int, bool = false);

  void initializeAmrStep(DistSVec<double, dim>&, int, DistSVec<double, dim> *, DistSVec<double, dim> *, DistSVec<double, dim> *,
                         DistSVec<double, dim> *, DistSVec<double, dim> *);
  bool refineMesh(DistSVec<double, dim>&, NodeData *&, int, int, double, int, bool&, DistSVec<double, dim> *,
                  DistSVec<double, dim> *, DistSVec<double, dim> *, DistSVec<double, dim> *, DistSVec<double, dim> *, int, bool&);
  void initializePredictor(int&, double&, DistSVec<double, dim> *&, DistSVec<double, dim> *&, DistSVec<double, dim> *&, int&);
  void initializeCorrector(int, double, const DistSVec<double, dim>&, DistSVec<double, dim> *, DistSVec<double, dim> *, DistSVec<double, dim> *&, int);
  void finalizeAmrStep(int);
  virtual void adaptiveRepart(DistSVec<double, dim> *, DistSVec<double, dim> *, DistSVec<double, dim> *, DistSVec<double, dim> *,
                              DistSVec<double, dim> *, int);
  virtual void outputMesh(int, int, double);
  virtual void resize1(NodeData *, DistSVec<double, dim>&);
  virtual void resize2();
  bool getMeshRefinedSinceLastWallDistance() { return meshRefinedSinceLastWallDistance; }
  bool getRecomputeResidualAfterBalancingOrOntheflyRestart() { return recomputeResidualAfterBalancingOrOntheflyRestart; }
  int getMeshOutputIndex() { return meshOutputIndex; }

  void computeForceLoad(DistSVec<double, dim>&, double * = nullptr, int = 0);
  void getForcesAndMoments(std::map<int, int>&, DistSVec<double, dim>&, Vec3D *, Vec3D *, Vec3D&, int);
  void computeDerivativeOfForceLoad(DistSVec<double, dim>&, DistSVec<double, dim> *, double, double * = nullptr, int = 0);
  void getDerivativeOfForcesAndMoments(std::map<int, int>&, DistSVec<double, dim>&, DistSVec<double, dim> *, Vec3D *, Vec3D *, Vec3D&, double, int);

  void getDeltaXCM(double[3]);

  void outputRotationMatrixToDisk(bool);

  void updateMomentArm();

  void updateSweptSet();

  void moveMesh();

  void initializeOutputToStructure(DistSVec<double, dim>&);
  void sendInitialForce();

  virtual void fsoHandler(DistSVec<double, dim>&);

 protected:
  void fsoInitialize();
  void commonPart(DistSVec<double, dim>&, int, double);

  friend FluidShapeOptimizationHandler<dim>;

};

//------------------------------------------------------------------------------

#endif

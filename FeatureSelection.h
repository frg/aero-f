/*
 *    FeatureSelection.h
 */

#ifndef _FEATURE_SELECTION_
#define _FEATURE_SELECTION_

#include <Domain.h>
#include <SubDomain.h>
#include <Communicator.h>
#include <Connectivity.h>

#include <RTree.h>
#include <Vector3D.h>
#include <DistVector.h>
#include <Vector.h>
#include <VectorSet.h>
#include <Elem.h>
#include <Node.h>
#include <DistGeoState.h>
#include <VarFcn.h>

#include <Eigen/Core>
#include <map>
#include <set>
#include <utility>
#include <vector>
#include <cstdio>

#define MAX_WAVE_NUMBER 50
#define MAX_POLY_ORDER 5

template<int dim>
class FeatureSelection {

  IoData& ioData;
  Domain *domain;
  Communicator *com;
  VarFcn *varFcn;

  int numFeatures;
  int numNodes;
  Eigen::MatrixXd features;
  std::vector<int> offset;

 public:

  FeatureSelection(IoData& _ioData, Domain *_domain, DistGeoState *_distGeoState, VecSet<DistSVec<double, dim>>& _snapshots, VarFcn *_varFcn) :
      ioData(_ioData),
      domain(_domain),
      com(_domain->getCommunicator()),
      varFcn(_varFcn) {
    // count how many features
    numFeatures = 0;
    offset.push_back(0);
    ioData.romOffline.fsd.boundaryDistance ? offset.push_back(1) : offset.push_back(0);
    ioData.romOffline.fsd.minEdgeLength ? offset.push_back(1) : offset.push_back(0);
    ioData.romOffline.fsd.maxEdgeLength ? offset.push_back(1) : offset.push_back(0);
    ioData.romOffline.fsd.avgEdgeLength ? offset.push_back(1) : offset.push_back(0);
    ioData.romOffline.fsd.hessian ? offset.push_back(1) : offset.push_back(0);
    ioData.romOffline.fsd.fft ? offset.push_back(2*MAX_WAVE_NUMBER) : offset.push_back(0);
    ioData.romOffline.fsd.polynomial ? offset.push_back(MAX_POLY_ORDER+1) : offset.push_back(0);
    for(int i = 0; i < offset.size()-1; ++i) offset[i+1] += offset[i];
    numFeatures = offset.back();
    com->fprintf(stdout, "Feature computation will be done for %d features.\n", numFeatures);

    // count how many nodes
    int_t toto[3];
    domain->getMaxElementStatistics(toto);
    numNodes = toto[0]+1;
    features = Eigen::MatrixXd::Ones(numNodes, numFeatures) * std::numeric_limits<double>::lowest();

    // compute features
    if(ioData.romOffline.fsd.boundaryDistance) computeBoundaryDistance(_distGeoState);
    if(ioData.romOffline.fsd.minEdgeLength || ioData.romOffline.fsd.maxEdgeLength || ioData.romOffline.fsd.avgEdgeLength) computeEdgeLengths();
    if(ioData.romOffline.fsd.hessian) computeHessian(_snapshots);
    if(ioData.romOffline.fsd.fft || ioData.romOffline.fsd.polynomial) computeFit(_snapshots);
    char tmp[] = "features.csv";
    writeFeatures(tmp);
  }

  ~FeatureSelection() {

  }

  //------------------------------------------------------------------------------

  void writeFeatures(char *filename) {
    // gather data
    double *buff = new double [numNodes*numFeatures];
    Eigen::Map<Eigen::MatrixXd>(buff, features.rows(), features.cols()) = features;
    com->globalMax<double>(numNodes*numFeatures, buff);
    com->barrier();
    Eigen::MatrixXd features = Eigen::Map<Eigen::MatrixXd>(buff, numNodes, numFeatures);

    // normalize boundary distance
    double maxDistance = features.col(offset[0]).maxCoeff();
    features.col(offset[0]) /= maxDistance;

    // normalize edge lengths
    double maxEdgeLength = features.col(offset[2]).maxCoeff();
    for(int col = offset[1]; col < offset[3]+1; col++) {
      features.col(col) /= maxEdgeLength;
    }

    // write data
    if(com->cpuNum() == 0) {
      const static Eigen::IOFormat CSVFormat(Eigen::FullPrecision,Eigen::DontAlignCols,", ","\n");
      std::ofstream file(filename);
      if(file.is_open()) {
        file << features.format(CSVFormat);
        file.close();
      }
    }
    delete [] buff;
  }

  //------------------------------------------------------------------------------

  void computeBoundaryDistance(DistGeoState *distGeoState) {
    DistVec<double> distances = distGeoState->getDistanceToWall();
    for(int iSub = 0; iSub < domain->getNumLocSub(); ++iSub) {
      for(int iNode = 0; iNode < domain->getSubDomain()[iSub]->numNodes(); ++iNode) {
        int globNode = domain->getSubDomain()[iSub]->getNodeMap()[iNode];
        features(globNode, offset[0]) = distances[iSub][iNode];
      }
    }
  }

  //------------------------------------------------------------------------------

  void computeEdgeLengths() {
    SubDomain **subDomain = domain->getSubDomain();
    for(int iSub = 0; iSub < domain->getNumLocSub(); ++iSub) {

      Connectivity *NodeToEdge = subDomain[iSub]->getNodeToEdge();
      auto& edgeFlag = subDomain[iSub]->getEdges().getMasterFlag();
      auto& edgePtr = subDomain[iSub]->getEdges().getPtr();
      auto& nodes = subDomain[iSub]->getNodes();

      for(int iNode = 0; iNode < NodeToEdge->csize(); ++iNode) {
        // if(!edgeFlag[iNode]) continue;

        // compute length
        double minLength = std::numeric_limits<double>::max();
        double maxLength = std::numeric_limits<double>::min();
        double totLength = 0;
        for(int iEdge = 0; iEdge < NodeToEdge->num(iNode); ++iEdge) {
          const int& l = (*NodeToEdge)[iNode][iEdge];
          const int& i = edgePtr[l].first;
          const int& j = edgePtr[l].second;
          const Vec3D& nodei = nodes[i];
          const Vec3D& nodej = nodes[j];
          Vec3D dx = nodej - nodei;
          double edgeLength = dx.norm();
          if(edgeLength < minLength) minLength = edgeLength;
          if(edgeLength > maxLength) maxLength = edgeLength;
          totLength += edgeLength;
        }
        double avgLength = totLength / NodeToEdge->num(iNode);

        // populate data structures
        int globNode = subDomain[iSub]->getNodeMap()[iNode];
        if(ioData.romOffline.fsd.minEdgeLength) features(globNode, offset[1]) = minLength;
        if(ioData.romOffline.fsd.maxEdgeLength) features(globNode, offset[2]) = maxLength;
        if(ioData.romOffline.fsd.avgEdgeLength) features(globNode, offset[3]) = avgLength;
      }
    }
  }

  //------------------------------------------------------------------------------

  void computeHessian(VecSet<DistSVec<double, dim>>& snapshots) {
    // for(const std::pair<int, SelectionCriterionData*>& p : amr.selectionCriteria.dataMap) {
    //   HessianCriterionData& hcd = p.second->hessianData;
    //   int k = (int)hcd.sensor_type;
    //   // Compute Hessian times edge vector ( e'*H*e ): essentially second derivative along the edge
    //   const SVec<double, 1> &ddxk = *ddx[k], &ddyk = *ddy[k], &ddzk = *ddz[k];
    //   double H = std::abs( (ddxk[i][0] - ddxk[j][0]) * (X[j][0] - X[i][0]) +
    //    (ddyk[i][0] - ddyk[j][0]) * (X[j][1] - X[i][1]) +
    //    (ddzk[i][0] - ddzk[j][0]) * (X[j][2] - X[i][2]) );
    // }
  }

  //------------------------------------------------------------------------------

  void computeFit(VecSet<DistSVec<double, dim>>& snapshots) {
    int k = 2*ioData.romOffline.fsd.waveNumbers + ioData.romOffline.fsd.polyOrder+1;
    int n = snapshots.numVectors();
    double pi = atan(1)*4;

    // compute mapping
    Eigen::MatrixXd Phi = Eigen::MatrixXd::Zero(k, n);
    for(int j = 0; j < Phi.cols(); ++j) {
      double t = (double)j / (double)(Phi.cols()-1);
      for(int i = 0; i < ioData.romOffline.fsd.waveNumbers; ++i) {
        Phi(i, j) = sin((i+1)*pi*t);
      }
      for(int i = 0; i < ioData.romOffline.fsd.waveNumbers; ++i) {
        Phi(i+ioData.romOffline.fsd.waveNumbers, j) = cos((i+1)*pi*t);
      }
      for(int i = 0; i <= ioData.romOffline.fsd.polyOrder; ++i) {
        Phi(i+2*ioData.romOffline.fsd.waveNumbers, j) = pow(t, i);
      }
    }

    // compute Kernel
    Eigen::MatrixXd K = Phi.transpose() * Phi;
    com->barrier();

    // iterate through nodes
    Eigen::MatrixXd nodeTimeSeries(n, 1);
    Eigen::MatrixXd beta(n, 1);
    for(int iSub = 0; iSub < domain->getNumLocSub(); ++iSub) {
      for(int iNode = 0; iNode < domain->getSubDomain()[iSub]->numNodes(); ++iNode) {
        nodeTimeSeries.setZero();
        beta.setZero();
        // get labels
        for(int iSnap = 0; iSnap < n; ++iSnap) {
          nodeTimeSeries(iSnap, 0) = varFcn->getVelocity((snapshots[iSnap])(iSub)[iNode], 0).norm();
        }
        // normalize
        if(ioData.romOffline.fsd.normalizeSnapshots) {
          nodeTimeSeries.array() -= nodeTimeSeries.minCoeff();
          nodeTimeSeries.array() /= nodeTimeSeries.maxCoeff();
        }
        // compute gradient descent solution
        computeFitSingle(K, nodeTimeSeries, beta);
        // write solution to features matrix
        int globNode = domain->getSubDomain()[iSub]->getNodeMap()[iNode];
        Eigen::MatrixXd theta = Phi * beta;
        theta.transposeInPlace();
        int zeroPad = MAX_WAVE_NUMBER - ioData.romOffline.fsd.waveNumbers;
        features.block(globNode, offset[5], 1, ioData.romOffline.fsd.waveNumbers) =
          theta.block(0, 0, 1, ioData.romOffline.fsd.waveNumbers);
        features.block(globNode, offset[5] + ioData.romOffline.fsd.waveNumbers, 1, zeroPad).setZero();
        features.block(globNode, offset[5] + MAX_WAVE_NUMBER, 1, ioData.romOffline.fsd.waveNumbers) =
          theta.block(0, ioData.romOffline.fsd.waveNumbers, 1, ioData.romOffline.fsd.waveNumbers);
        features.block(globNode, offset[5] + MAX_WAVE_NUMBER + ioData.romOffline.fsd.waveNumbers, 1, zeroPad).setZero();
        features.block(globNode, offset[6], 1, ioData.romOffline.fsd.polyOrder+1) =
          theta.block(0, 2*ioData.romOffline.fsd.waveNumbers, 1, ioData.romOffline.fsd.polyOrder+1);
        features.block(globNode, offset[6] + ioData.romOffline.fsd.polyOrder+1, 1, MAX_POLY_ORDER-ioData.romOffline.fsd.polyOrder).setZero();
        std::cout << "CPU " << com->cpuNum() << " Fit finished for node " << globNode << std::endl;
      }
    }
  }

  //------------------------------------------------------------------------------

  void computeFitSingle(Eigen::MatrixXd& K, Eigen::MatrixXd& y, Eigen::MatrixXd& beta) {
    Eigen::MatrixXd diff;
    double rel = std::numeric_limits<double>::max();
    double tol = 1e-4;
    double lr = 1e-4;
    if(y.norm() != 0) {
      while(rel > tol) {
        diff = y - (K * beta);
        diff.array() *= lr;
        beta.array() += diff.array();
        rel = diff.norm() / beta.norm();
        // std::cout << "CPU " << com->cpuNum() << " Diff " << diff.norm() << std::endl;
      }
    }
  }

};

#endif

#ifndef _RED_MESH_CONSTRUCTION_H_
#define _RED_MESH_CONSTRUCTION_H_

#include <vector>
#include <set>

#include <VectorSet.h>

class IoData;
class Domain;
class Communicator;
class Timer;
class SubDomain;
class DistGeoState;
class NonlinearRom;

template<int dim> class ImplicitTsDesc;
template<int dim> class PeriodicBoundaryHandler;
template<class Scalar> class DistVec;
template<class Scalar, int dim> class DistSVec;
template<class VecType> class Clustering;
template<int dim> class DistEmbeddedBcData;
template<int dim> class NearestNeighborRom;

struct ReducedMeshConstructionData;

//------------------------------------------------------------------------------

class ReducedMeshConstruction {

  IoData& ioData;
  Domain *domain;
  int numLocSub;
  Communicator *com;
  Timer *timer;
  SubDomain **subDomain;
  DistGeoState *geoState;
  NonlinearRom *nlrom;

  ReducedMeshConstructionData& meshData;

  // NOTE: all node quantities exclude non-master nodes on each subdomain
  int locSampleNodes, globSampleNodes;
  std::vector<std::set<int>> sampleNodes;
  std::vector<std::set<int>> reducedMeshNodes;
  std::vector<std::set<int>> reducedMeshElems;
  std::vector<std::set<int>> reducedMeshFaces;

 private:
  template<int dim>
  void buildReducedMesh(ImplicitTsDesc<dim> *, int = -1);

  void exchangeTag(DistVec<int>&);

  template<int dim>
  void addPeriodicNodes(PeriodicBoundaryHandler<dim> *, DistVec<int>&, int&, const int);

  void addLiftNodes(DistVec<int>&, int&, const int);

  void finalizeReducedMesh(DistVec<int>&, int, int = -1);

  template<class Scalar>
  void gatherReducedMeshData(std::vector<Scalar>&, std::vector<Scalar>&);

  template<int dim>
  void outputOnReducedMesh(Clustering<DistSVec<double, dim>> *, NearestNeighborRom<dim> * = nullptr);

 public:
  ReducedMeshConstruction(IoData&, Domain *, DistGeoState *, NonlinearRom *);
  ~ReducedMeshConstruction() {}

  template<int dim>
  void solve(ImplicitTsDesc<dim> *, DistEmbeddedBcData<dim> *, Clustering<DistSVec<double, dim>> *,
             NearestNeighborRom<dim> * = nullptr);

  int sumNodes;
  int sumFaces;
  int sumElems;

};

//------------------------------------------------------------------------------

#endif

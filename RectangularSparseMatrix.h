#ifndef _RECTANGULAR_SPARSE_MATRIX_H_
#define _RECTANGULAR_SPARSE_MATRIX_H_

#include <Vector.h>
#include <Vector3D.h>

//------------------------------------------------------------------------------

template<class Scalar, int dim, int dim2>
class RectangularSparseMat {

  int n;                    // number of unknown blocks
  int nnz;                  // number of non-zero blocks

  Vec<int> ia;              // pointer to rows
  Vec<int> ja;              // pointer to columns

  SVec<Scalar, dim*dim2> a; // data stored in CSR format

  bool dense = false;

 private:
  int find(int, int);

 public:
  RectangularSparseMat(int, int, const Vec<int>&, const Vec<int>&);
  ~RectangularSparseMat() {}

  RectangularSparseMat<Scalar, dim, dim2>& operator=(const Scalar& x) {
    a = x;
    return *this;
  }
  RectangularSparseMat<Scalar, dim, dim2>& operator*= (const Scalar x) {
    a *= x;
    return *this;
  }

  void setDenseFlag(bool flag) {dense = flag;};

  void setZero() {
    a.setZero();
  }

  int numNonZeroBlocks() const {
    return nnz;
  }

  Scalar(*data())[dim * dim2] { return a.data(); }

  const Vec<int>& colind() const {
    return ja;
  }

  const Vec<int>& rowptr() const {
    return ia;
  }

  void addContrib(int, int *, Scalar *);
  void addContrib(int, int, Scalar *);
  void addContrib(int, int, Scalar);

  void apply(SVec<Scalar, dim>&, SVec<Scalar, dim2>&);
  void apply(SVec<Scalar, dim>&, Vec<Vec3D>&);
  void apply(Vec<Vec3D>&, SVec<Scalar, dim2>&);
  void apply(Vec<Scalar>&, SVec<Scalar, dim2>&);
  void apply(SVec<Scalar, dim>&, Vec<Scalar>&);

  void applyAndAdd(SVec<Scalar, dim>&, SVec<Scalar, dim2>&);
  void applyAndAdd(SVec<Scalar, dim>&, Vec<Vec3D>&);
  void applyAndAdd(Vec<Vec3D>&, SVec<Scalar, dim2>&);
  void applyAndAdd(Vec<Scalar>&, SVec<Scalar, dim2>&);
  void applyAndAdd(SVec<Scalar, dim>&, Vec<Scalar>&);

  void applyTranspose(SVec<Scalar, dim2>&, SVec<Scalar, dim>&);
  void applyTranspose(Vec<Vec3D>&, SVec<Scalar, dim>&);
  void applyTranspose(SVec<Scalar, dim2>&, Vec<Vec3D>&);
  void applyTranspose(Vec<Scalar>&, SVec<Scalar, dim>&);
  void applyTranspose(SVec<Scalar, dim2>&, Vec<Scalar>&);

  void applyTransposeAndAdd(SVec<Scalar, dim2>&, SVec<Scalar, dim>&);
  void applyTransposeAndAdd(Vec<Vec3D>&, SVec<Scalar, dim>&);
  void applyTransposeAndAdd(SVec<Scalar, dim2>&, Vec<Vec3D>&);
  void applyTransposeAndAdd(Vec<Scalar>&, SVec<Scalar, dim>&);
  void applyTransposeAndAdd(SVec<Scalar, dim2>&, Vec<Scalar>&);

  void applyTransposeAndSubtract(SVec<Scalar, dim2>&, SVec<Scalar, dim>&);

};

//------------------------------------------------------------------------------

#endif

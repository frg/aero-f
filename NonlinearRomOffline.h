#ifndef _NONLINEAR_ROM_OFFLINE_H_
#define _NONLINEAR_ROM_OFFLINE_H_

#include <ImplicitTsDesc.h>

#include <VectorSet.h>
#include <VectorSetAdaptive.h>

#include <vector>

class IoData;
class GeoSource;
class Domain;
class Communicator;
class Timer;
class NonlinearRom;

template<int dim> class NonlinearRomAdaptive;
template<int dim> class EmbeddedPOD;
template<class VecType> class Clustering;
template<class Scalar, int dim> class DistSVec;
template<class Scalar> class Vec;
template<class Scalar> class DenseMat;

struct NonlinearRomOfflineData;

//------------------------------------------------------------------------------

template<int dim>
class NonlinearRomOffline : public ImplicitTsDesc<dim> {

  Communicator *com;
  Timer *timer;

  EmbeddedPOD<dim> *embedpod;
  NonlinearRom *nlrom;
  NonlinearRomAdaptive<dim> *nlroma;
  NonlinearRomOfflineData& offlineData;

 public:
  Clustering<DistSVec<double, dim>> *clustering;
  Clustering<Vec<double>> *clusteringA;

 private:
  void POD(const char *);
  void PODadaptive(const char *);
  void normalizeSnapshots(VecSet<DistSVec<double, dim>>&, VecSet<Vec<double>> * = nullptr);
  void normalizeSnapshotsAdaptive(VecSet<Vec<double>>&);
  void projectionError();
  void computeQuadraticManifold();
  void outputClusteredSnapshots(VecSet<DistSVec<double, dim>>&);
  void outputClusteredSnapshots(VecSet<Vec<double>>&);
  int preprocessAMRsnapshots();
  void preprocessAffineForOnlineProjections();
  void preprocessManifoldForOnlineProjections();
  void preprocessICForOnlineProjections();
  void preprocessICForOnlineManifoldProjections();
  void preprocessICForOnlineProjectionsAdaptive();
  void preprocessForNearestNeighbors();
  void readInitialCondition(VecSet<DistSVec<double, dim>>&);
  void readInitialFluidID(VecSet<DistVec<bool>>&);
  void getRowMask(DistVec<double>&, DistVec<bool>&, int);
  void orderRowClusteredBases(VecSet<DistSVec<double, dim>>&, VecSet<DistSVec<double, dim>>&,
                              std::vector<double>&, int, int, std::vector<int>&);
  int selectBasisSizes(VecSet<DistSVec<double, dim>>&, std::vector<double>&, DataCompressionData&, double&);

 public:
  NonlinearRomOffline(IoData&, GeoSource&, Domain *);
  ~NonlinearRomOffline();

  void solve();
  int constructSampleMesh();
  int constructSampleMeshAdaptive();

  void imputeSnapshots(VecSet<DistSVec<double, dim>>&, VecSet<DistVec<bool>>&, DistSVec<double, dim>&);
  EmbeddedPOD<dim> *getEmbeddedPOD() {
    return embedpod;
  }

};

//------------------------------------------------------------------------------

#endif

#ifndef _DIST_EDGE_GRAD_H_
#define _DIST_EDGE_GRAD_H_

#include <EdgeGrad.h>
#include <IoData.h>

#include <vector>
#include <map>
#include <utility>

class Domain;
class SubDomain;

template<class T> class CommPattern;
template<class Scalar, int dim> class DistSVec;

struct Vec3D;

//------------------------------------------------------------------------------

class DistEdgeGrad {

  int lastConfig;
  int it0;
  int lastIt;
  int numLocSub;

  Domain *domain;
  SubDomain **subDomain;

  std::vector<EdgeGrad *> subEdgeGrad;

  // data structures for parallelizing V6 scheme across subdomain boundaries
  CommPattern<double> *v6Pat;
  std::vector<std::map<int, std::vector<std::pair<V6NodeData, Vec3D>>>> remoteV6Data;
  std::vector<std::map<int, std::vector<int>>> remoteEdgeMap;

  void findEdgeTetrahedra(DistSVec<double, 3>&);
  void findRemoteTetrahedra(DistSVec<double, 3>&);
  void finalizeRemote(int);

 public:
  DistEdgeGrad(IoData&, Domain *);
  ~DistEdgeGrad();

  EdgeGrad& operator()(int i) const {
    return *subEdgeGrad[i];
  }
  const std::map<int, std::vector<std::pair<V6NodeData, Vec3D>>>& getV6RemoteData(int i) const {
    return remoteV6Data[i];
  }

  void resize2();

  void initialize(int, DistSVec<double, 3>&, const int);

  template<int Dim>
  void compute(int, DistSVec<double, 3>&, DistSVec<double, Dim>&, DistSVec<double, Dim>&, DistSVec<double, Dim>&,
               DistSVec<double, Dim>&);

  template<int Dim>
  void computeDerivative(int, DistSVec<double, 3>&, DistSVec<double, 3>&, DistSVec<double, Dim>&, DistSVec<double, Dim>&,
                         DistSVec<double, Dim>&, DistSVec<double, Dim>&, DistSVec<double, Dim>&, DistSVec<double, Dim>&,
                         DistSVec<double, Dim>&, DistSVec<double, Dim>&);

};

//------------------------------------------------------------------------------

#endif

#ifndef _DIST_MESH_ADAPTER_H_
#define _DIST_MESH_ADAPTER_H_

#include <IoData.h>
#include <NodeData.h>
#include <Types.h>

#include <map>
#include <set>
#include <utility>
#include <vector>

struct AdaptiveMeshRefinementData;

class Communicator;
class DistLevelSetStructure;
class DistMeshState;
class DistMetricSpace;
class Domain;
class EmbeddedCorotSolver;
class GeometricQuery;
class GeoSource;
class MeshAdapter;
class SubDomain;
class SubDTopo;
class Timer;
class TriangleSet;
class VarFcn;
class WallFcn;
class Vec3D;

template<int dim> class DistEmbeddedBcData;
template<int dim> class DistRestartState;
template<class Scalar, int dim> class DistSVec;
template<class Scalar> class DistVec;
template<class T> class RTree;

class DistMeshAdapter {

  Domain *domain;
  int numGlobSub;
  int numLocSub;
  SubDomain **subDomain;
  SubDTopo *&subTopo;
  Timer *timer;
  Communicator *com;

  // resizeable connectivities
  std::vector<set_t<int>> *node_to_element;
  std::vector<set_t<int>> *node_to_face;
  std::vector<set_t<int>> *elem_to_face;

  MeshAdapter **meshAdapter;

  double cg_0[3];
  EmbeddedCorotSolver *cs;
  TriangleSet *triangleSet;

  DistMetricSpace *metricSpace;
  std::map<int, DistMetricSpace*> localMetrics;
  std::map<int, GeometricQuery*> sources;

  DistSVec<double, 1> *refScalar[HessianCriterionData::NUM_SENSOR_TYPES];
  DistSVec<double, 1> *ddx[HessianCriterionData::NUM_SENSOR_TYPES];
  DistSVec<double, 1> *ddy[HessianCriterionData::NUM_SENSOR_TYPES];
  DistSVec<double, 1> *ddz[HessianCriterionData::NUM_SENSOR_TYPES];

 public:
  DistMeshAdapter(IoData&, Domain *);
  ~DistMeshAdapter();

  void resize(NodeData *);

  TriangleSet *getTriangleSet() { return triangleSet; }
  DistMetricSpace *getMetricSpace() { return metricSpace; }
  DistSVec<double, 1> *getRefScalar(int i) { return refScalar[i]; }
  DistSVec<double, 1> *getDdx(int i) { return ddx[i]; }
  DistSVec<double, 1> *getDdy(int i) { return ddy[i]; }
  DistSVec<double, 1> *getDdz(int i) { return ddz[i]; }

  void initializeCorotatingFrame(IoData&, DistLevelSetStructure *);
  void updateCorotatingFrame(IoData&, DistLevelSetStructure *);
  void updateAxisAlignedBoundingBoxTree(IoData&, DistLevelSetStructure *, double);

  template<int dim>
  bool refineMesh(GeoSource&, IoData&, DistSVec<double, dim>&, DistSVec<double, 3>&, DistVec<double>&, NodeData *,
                  int_t&, DistEmbeddedBcData<dim> *, DistVec<double> *, DistVec<double> *, int, VarFcn *, int,
                  DistRestartState<dim>&, bool&, DistMeshState *, DistRestartState<dim> *, int_t[3], int&, int,
                  DistSVec<double, dimLS> *, bool, WallFcn *, RTree<Vec3D> *);

  template<int dim>
  void adaptiveRepart(IoData&, GeoSource&, DistRestartState<dim>&, NodeData *, DistEmbeddedBcData<dim> *, bool);

  template<int dim>
  void moveElems(IoData&, GeoSource&, DistRestartState<dim>&, NodeData *, std::vector<std::multimap<int,int>>&, bool);

  template<int dim>
  void precomputeHessian(IoData&, DistSVec<double, dim>&, DistSVec<double, 3>&, DistEmbeddedBcData<dim> *,
                         DistVec<double> *, VarFcn *, DistSVec<double, dimLS> *);
  void deleteHessian();

private:
  template<int dim>
  void initializePreferredDirection(IoData&, DistSVec<double, dim>&, DistSVec<double, 3>&, DistEmbeddedBcData<dim> *,
                                    DistVec<double> *, VarFcn *, int, int, DistSVec<double, dimLS> *);

  void updateMetricTensor(std::vector<std::set<int>>&);
  void assignGlobalNumbers(IoData&, int_t[3], std::multimap<int,std::pair<int,int>> *, std::multimap<int,std::pair<int,int>> *,
                           int_t[3]);
  void initializeRefinementTag(IoData&);
  void checkGlobalNumbers(int_t, int_t, int_t);
  void compressGlobalNumbers(int_t, int_t, int_t, int);
  void updateSubTopo(GeoSource&, std::vector<std::set<int>>&, bool);
  void updateSharedNodes(GeoSource&, const std::vector<std::set<int>>&, std::vector<std::set<int>> * = nullptr);
  void computeEdgeLengths(AdaptiveMeshRefinementData&, const std::vector<std::set<int>>&, std::vector<std::vector<double>>&);
  void checkSharedNodes();

};

//------------------------------------------------------------------------------

#endif
